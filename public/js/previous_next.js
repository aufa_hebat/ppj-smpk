$('#tab_button .next-tab').click(function () {
    // get current tab
    var currentTab = $("#tab_button .active");
    // get the next tab, if there is one (i.e. we are not at the end)
    var newTab = currentTab.next();
    console.log(newTab);
    // console.log(newTab);
    if(newTab.length > 0) {
        // remove active from old tab
        currentTab.removeClass('active');
        // add active to new tab
        newTab.addClass('show active').removeClass('fade');
    }
});

$('#tab_button .prev-tab').click(function () {
    var currentTab = $("#tab_button .active");
    var newTab = currentTab.prev();
    if(newTab.length > 0) {
        currentTab.removeClass('active');
        newTab.addClass('show active').removeClass('fade');
    }
});