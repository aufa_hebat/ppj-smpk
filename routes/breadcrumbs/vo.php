<?php

// Home > Pra-Kontrak > Perubahan Kerja
Breadcrumbs::register('contract.post.variation-order.index', function ($breadcrumbs) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Perubahan Kontrak'), route('contract.post.variation-order.index'));
});

// Home > Pra-Kontrak > Perubahan Kerja > Tambah
Breadcrumbs::register('contract.post.variation-order.create', function ($breadcrumbs) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Tambah Perubahan Kerja'), route('contract.post.variation-order.create'));
});

// Home > Pra-Kontrak > Perubahan Kerja > Butiran
Breadcrumbs::register('contract.post.variation-order.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Perubahan Kerja'), route('contract.post.variation-order.show', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Kemaskini
Breadcrumbs::register('contract.post.variation-order.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Kemaskini Perubahan Kerja'), route('contract.post.variation-order.edit', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > PPK
Breadcrumbs::register('contract.post.variation-order.ppk', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Tambah Permohonan Perubahan Kerja'), route('contract.post.variation-order.ppk', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Kemaskini PPK
Breadcrumbs::register('contract.post.variation-order.ppk_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Kemaskini Permohonan Perubahan Kerja'), route('contract.post.variation-order.ppk_edit', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > PK
Breadcrumbs::register('contract.post.variation-order.pk', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Tambah Perubahan Kerja'), route('contract.post.variation-order.pk', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Search BQ
Breadcrumbs::register('contract.post.variation-order.searchBQ', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Carian BQ'), route('contract.post.variation-order.searchBQ', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Search BQ PS KKK
Breadcrumbs::register('contract.post.variation-order.searchBQ-PS-KKK', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Carian BQ'), route('contract.post.variation-order.searchBQ-PS-KKK', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Aktif WPS
Breadcrumbs::register('contract.post.variation-order.active-wps', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Aktif WPS'), route('contract.post.variation-order.active-wps', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Kesilapan Keterangan Kerja Dan Kuantiti
Breadcrumbs::register('contract.post.variation-order.kkk', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Kesilapan Keterangan Kerja Dan Kuantiti'), route('contract.post.variation-order.kkk', $id));
});
 
// Home > Pra-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order.wps', function ($breadcrumbs, $vo, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order.wps', [$vo, $id]));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS Aktif
Breadcrumbs::register('contract.post.variation-order.wpsSelect', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS Aktif'), route('contract.post.variation-order.wpsSelect', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Pengiraan Semula
Breadcrumbs::register('contract.post.variation-order.ps', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Pengiraan Semula'), route('contract.post.variation-order.ps', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Batal WPS
Breadcrumbs::register('contract.post.variation-order.cancel-wps', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Aktif WPS'), route('contract.post.variation-order.cancel-wps', $id));
});



// Home > Pra-Kontrak > Perubahan Kerja > Kemaskini PPK
Breadcrumbs::register('contract.post.variation-order.ppk_show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Paparan Permohonan Perubahan Kerja'), route('contract.post.variation-order.ppk_show', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Butiran Aktif WPS
Breadcrumbs::register('contract.post.variation-order.show-active-wps', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Aktif WPS'), route('contract.post.variation-order.show-active-wps', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > PK
Breadcrumbs::register('contract.post.variation-order.show-pk', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Perubahan Kerja'), route('contract.post.variation-order.show-pk', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS Aktif
Breadcrumbs::register('contract.post.variation-order.show-wpsSelect', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran WPS Aktif'), route('contract.post.variation-order.show-wpsSelect', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order.show-wps', function ($breadcrumbs, $vo, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order.show-wps', [$vo, $id]));
});

// Home > Pra-Kontrak > Perubahan Kerja > Pengiraan Semula
Breadcrumbs::register('contract.post.variation-order.show-ps', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Pengiraan Semula'), route('contract.post.variation-order.show-ps', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Kesilapan Keterangan Kerja Dan Kuantiti
Breadcrumbs::register('contract.post.variation-order.show-kkk', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Kesilapan Keterangan Kerja Dan Kuantiti'), route('contract.post.variation-order.show-kkk', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Butiran Batal WPS
Breadcrumbs::register('contract.post.variation-order.show-cancel-wps', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Aktif WPS'), route('contract.post.variation-order.show-cancel-wps', $id));
});



// Home > Pra-Kontrak > Urus Perubahan Kerja
Breadcrumbs::register('contract.post.variation-order-result.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Perubahan Kerja'), route('contract.post.variation-order-result.index'));
});

// Home > Pra-Kontrak > Perubahan Kerja > Kemaskini
Breadcrumbs::register('contract.post.variation-order-result.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('Kemaskini Perubahan Kerja'), route('contract.post.variation-order-result.edit', $id));
});


// Home > Pra-Kontrak > Perubahan Kerja > Kemaskini Keputusan PPK
Breadcrumbs::register('contract.post.variation-order-result.ppk_amend', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('Kemaskini Keputusan Permohonan Perubahan Kerja (PPK)'), route('contract.post.variation-order-result.ppk_amend', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Keputusan Perubahan Kerja
Breadcrumbs::register('contract.post.variation-order-result.ppk_amend_result', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('Kemaskini Keputusan Perubahan Kerja'), route('contract.post.variation-order-result.ppk_amend_result', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Jawatankuasa Perubahan Kerja
Breadcrumbs::register('contract.post.variation-order-result.ppk_amend_approver', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('Kemaskini Jawatankuasa Perubahan Kerja'), route('contract.post.variation-order-result.ppk_amend_approver', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Butiran Keputusan PPK
Breadcrumbs::register('contract.post.variation-order-result.ppk_amend_show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('Butiran Keputusan Permohonan Perubahan Kerja (PPK)'), route('contract.post.variation-order-result.ppk_amend_show', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Keputusan Perubahan Kerja
Breadcrumbs::register('contract.post.variation-order-result.show-ppk-amend-result', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('BUtiran Keputusan Perubahan Kerja'), route('contract.post.variation-order-result.show-ppk-amend-result', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > Jawatankuasa Perubahan Kerja
Breadcrumbs::register('contract.post.variation-order-result.show-ppk-amend-approver', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order-result.index');
    $breadcrumbs->push(__('Butiran Jawatankuasa Perubahan Kerja'), route('contract.post.variation-order-result.show-ppk-amend-approver', $id));
});

// Home > Pra-Kontrak > Pelarasan Jumlah Harga Kontrak > 
Breadcrumbs::register('contract.post.variation-order-ppjhk.ppjhk', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Tambah Permohonan Pelarasan Jumlah Harga Kontrak'), route('contract.post.variation-order-ppjhk.ppjhk', $id));
});

// Home > Pra-Kontrak > Kemaskini Pelarasan Jumlah Harga Kontrak > 
Breadcrumbs::register('contract.post.variation-order-ppjhk.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Kemaskini Permohonan Pelarasan Jumlah Harga Kontrak'), route('contract.post.variation-order-ppjhk.edit', $id));
});

// Home > Pra-Kontrak > Butiran Pelarasan Jumlah Harga Kontrak > 
Breadcrumbs::register('contract.post.variation-order-ppjhk.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Permohonan Pelarasan Jumlah Harga Kontrak'), route('contract.post.variation-order-ppjhk.show', $id));
});

// Home > Pra-Kontrak > Kemaskini Pelarasan Jumlah Harga Kontrak > 
Breadcrumbs::register('contract.post.variation-order-ppjhk.amend', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Kemaskini Kuantiti Dan Harga Item'), route('contract.post.variation-order-ppjhk.amend', $id));
});

// Home > Pra-Kontrak > Kemaskini Pelarasan Jumlah Harga Kontrak > 
Breadcrumbs::register('contract.post.variation-order-ppjhk.show-amend', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('Butiran Kuantiti Dan Harga Item'), route('contract.post.variation-order-ppjhk.show-amend', $id));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order-wps.wps-active', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order-wps.wps-active', [$id]));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order-wps.wps-edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order-wps.wps-edit', [$id]));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order-wps.wps-select', function ($breadcrumbs, $vo, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order-wps.wps-select', [$vo, $id]));
});

// Home > Pasca-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order-wps.wps-show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order-wps.wps-show', [$id]));
});

// Home > Pra-Kontrak > Perubahan Kerja > WPS & Kos Prima
Breadcrumbs::register('contract.post.variation-order-wps.wps-show-select', function ($breadcrumbs, $vo, $id) {
    $breadcrumbs->parent('contract.post.variation-order.index');
    $breadcrumbs->push(__('WPS & Kos Prima'), route('contract.post.variation-order-wps.wps-show-select', [$vo, $id]));
});