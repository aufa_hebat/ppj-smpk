<?php

// Home > Pra-Kontrak > Pemantauan Projek
Breadcrumbs::register('acquisition.monitoring.index', function ($breadcrumbs) {
    $breadcrumbs->parent('contract.pre.index');
    $breadcrumbs->push(__('Pemantauan Projek'), route('acquisition.monitoring.index'));
});

// Home > Pra-Kontrak > Pemantauan Projek > Butiran
Breadcrumbs::register('acquisition.monitoring.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.monitoring.index');
    $breadcrumbs->push(__('Butiran Pemantauan Projek'), route('acquisition.monitoring.show', $id));
});

// Home > Pra-Kontrak > Pemantauan Projek > Kemaskini
Breadcrumbs::register('acquisition.monitoring.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.monitoring.index');
    $breadcrumbs->push(__('Kemaskini Pemantauan Projek'), route('acquisition.monitoring.edit', $id));
});
