<?php

/*
|--------------------------------------------------------------------------
| API Datatable Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API Datatable routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API Datatable!
|
 */

/**
 * Task.
 */
Route::get('tasks', 'TaskController')->name('tasks');

/*
 * New Tasklist
 */
Route::get('workflowtasks', 'WorkflowTaskController')->name('workflowtasks');
Route::get('workflowtasksTeam', 'WorkflowTaskTeamController')->name('workflowtasksTeam');

/*
 * Contract.
 */
Route::group([
    'namespace' => 'Contract',
    'prefix'    => 'contract',
    'as'        => 'contract.',
], function () {
    Route::group([
        'namespace' => 'Acquisition',
        'prefix'    => 'acquisition',
        'as'        => 'acquisition.',
    ], function () {
        Route::get('activity', 'ActivityController')->name('activity');
        Route::get('activity-financial', 'Activity\FinancialController')->name('activity.financial');
        Route::get('activity-physical', 'Activity\PhysicalController')->name('activity.physical');
        Route::get('monitoring', 'MonitoringController')->name('monitoring');
        Route::get('monitoring/site-visit', 'Monitoring\SiteVisitController')->name('monitoring.site-visit');
        Route::get('monitoring/payment-monitor', 'Monitoring\PaymentMonitorController')->name('monitoring.payment-monitor');
        Route::get('monitoring/contract-progress', 'Monitoring\ContractProgressController')->name('monitoring.contract-progress');
        Route::get('review', 'ReviewController')->name('review');
        Route::get('lawreview', 'LawReviewController')->name('lawreview');
        Route::get('reviews', 'ReviewsController')->name('reviews');
    });
    Route::get('acceptance-letter', 'AcceptanceLetterController')->name('acceptance-letter');
    //WHY ACQUISITION CAN'T ADD PARAM ?? 
    Route::get('acquisition', 'AcquisitionController')->name('acquisition');
    Route::get('approval-documents', 'ApprovalDocumentController')->name('approval-documents');
    Route::get('budget-code', 'BudgetCodeController')->name('budget-code');
    Route::get('box', 'BoxController')->name('box');
    Route::get('briefing', 'BriefingController')->name('briefing');
    Route::get('document', 'DocumentController')->name('document');
    Route::get('evaluation', 'EvaluationController')->name('evaluation');
    Route::get('extension', 'ExtensionController')->name('extension');
    Route::get('extension_approve', 'ExtensionApproveController')->name('extension_approve');
    Route::get('extension_edit', 'ExtensionEditController')->name('extension_edit');
    Route::get('pre', 'PreController')->name('pre');
    Route::get('post', 'PostController')->name('post');
    Route::get('bon', 'BonController')->name('bon');
    Route::get('release-bon', 'ReleaseBonController')->name('release-bon');
    Route::get('sale', 'SaleController')->name('sale');
    Route::get('saletoday', 'SaleTodayController')->name('saletoday');
    Route::get('saleprevious', 'SalePreviousController')->name('saleprevious');
    Route::get('salefuture', 'SaleFutureController')->name('salefuture');
    Route::get('salelasttoday', 'SaleLastTodayController')->name('salelasttoday');
    Route::get('site-visit', 'SiteVisitController')->name('site-visit');
    Route::get('ipc', 'IpcController')->name('ipc');
    Route::get('ipc-invoice', 'IpcInvoiceController')->name('ipc-invoice');
    Route::get('ipc-invoice-list', 'IpcInvoiceListController')->name('ipc-invoice-list');
    Route::get('termination', 'TerminationController')->name('termination');
    Route::get('warning', 'WarningController')->name('warning');
    Route::get('cancel', 'CancelController')->name('cancel');
    Route::get('variation-order', 'VariationOrderController')->name('variation-order');
    Route::get('variation-order-ppk', 'VariationOrderPpkController')->name('variation-order-ppk');
    Route::get('variation-order-wps', 'VariationOrderWpsController')->name('variation-order-wps');
    Route::get('variation-order-result-ppk', 'VariationOrderResultPpkController')->name('variation-order-result-ppk');
    Route::get('variation-order-ppjhk', 'VariationOrderPpjhkController')->name('variation-order-ppjhk');
    Route::get('sofa', 'SofaController')->name('sofa');
    Route::get('contract-completed', 'ContractCompletedController')->name('contract-completed');
    Route::get('release', 'ReleaseController')->name('release');

    // review log
    Route::get('review-log', 'ReviewLogController')->name('review-log');
    Route::get('review-log-sst', 'ReviewLogSSTController')->name('review-log-sst');
    Route::get('review-log-document-contract', 'ReviewLogDocumentContractController')->name('review-log-document-contract');
    Route::get('review-log-ipc', 'ReviewLogIPCController')->name('review-log-ipc');
    Route::get('review-log-eot', 'ReviewLogEOTController')->name('review-log-eot');
    Route::get('review-log-warning', 'ReviewLogWarningController')->name('review-log-warning');
    Route::get('review-log-reason', 'ReviewLogReasonController')->name('review-log-reason');
    Route::get('review-log-notice', 'ReviewLogNoticeController')->name('review-log-notice');
    Route::get('review-log-vo-ppk', 'ReviewLogVOPPKController')->name('review-log-vo-ppk');
    Route::get('review-log-vo-ppjhk', 'ReviewLogVOPPJHKController')->name('review-log-vo-ppjhk');
    Route::get('review-log-sofa', 'ReviewLogSOFAController')->name('review-log-sofa');
});

/*
 * Manage
 */
Route::group([
    'namespace' => 'Manage',
    'prefix'    => 'manage',
    'as'        => 'manage.',
], function () {
    Route::get('users', 'UserController')->name('user');
    Route::get('companies', 'CompanyController')->name('company');
    Route::get('owners', 'OwnerController')->name('owner');
    Route::get('certificates', 'CertificateController')->name('certificate');
    Route::get('variation-order', 'VariationOrderController')->name('variation-order');
    Route::get('approval', 'ApprovalController')->name('approval');
    Route::get('assessment', 'AssessmentController')->name('assessment');
    Route::get('document_acceptances', 'DocumentAcceptanceController')->name('document_acceptances');
    Route::get('document_acceptances_list', 'DocumentAcceptanceListController')->name('document_acceptances_list');
});

/*
 * Utility
 */
 Route::group([
    'namespace' => 'Utility',
    'prefix'    => 'utility',
    'as'        => 'utility.',
], function () {
    Route::get('authorities', 'AuthoritiesController')->name('authorities');
    Route::get('bank', 'BankController')->name('bank');
    Route::get('bon_type', 'BonTypeController')->name('bon_type');
    Route::get('budget_code', 'BudgetCodeController')->name('budget_code');
    Route::get('state', 'StateController')->name('state');
    Route::get('department', 'DepartmentController')->name('department');
    Route::get('division', 'DivisionController')->name('division');
    Route::get('section', 'SectionController')->name('section');
    Route::get('position', 'PositionController')->name('position');
    Route::get('scheme', 'SchemeController')->name('scheme');
    Route::get('grade', 'GradeController')->name('grade');
    Route::get('role', 'RoleController')->name('role');
    Route::get('location', 'LocationController')->name('location');
    Route::get('period_type', 'PeriodTypeController')->name('period_type');
    Route::get('liability_period', 'LiabilityPeriodController')->name('liability_period');
    Route::get('insurance', 'InsuranceController')->name('insurance');
    Route::get('honorary', 'HonoraryController')->name('honorary');
    Route::get('acquisition_type', 'AcquisitionTypeController')->name('acquisition_type');
    Route::get('acquisition_category', 'AcquisitionCategoryController')->name('acquisition_category');
    Route::get('allocation_resource', 'AllocationResourceController')->name('allocation_resource');
    Route::get('termination_type', 'TerminationTypeController')->name('termination_type');
    Route::get('vo_type', 'VariationOrderTypeController')->name('vo_type');
    Route::get('cidb_grade', 'CIDBGradeController')->name('cidb_grade');
    Route::get('cidb_category', 'CIDBCategoryController')->name('cidb_category');
    Route::get('cidb_khusus', 'CIDBKhususController')->name('cidb_khusus');
    Route::get('mof_area', 'MOFAreaController')->name('mof_area');
    Route::get('mof_khusus', 'MOFKhususController')->name('mof_khusus');
});
