<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route::get('/', 'IndexController')->name('index');

//Route::get('/welcome', 'WelcomeController')->name('welcome');
Route::get('/', 'WelcomeController')->name('welcome');

Route::group([
    'namespace' => 'Registration',
    'prefix'    => 'Registration',
    'as'        => 'registration.',
], function () {
    Route::resource('companies', 'CompanyController');
    Route::resource('certificates', 'CertificateController');
    Route::resource('owners', 'OwnerController');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'namespace' => 'User',
    'prefix'    => 'user',
    'as'        => 'user.',
], function () {
    Route::get('/', 'UserController@show')->name('show');
    Route::put('/', 'UserController@update')->name('update');
    Route::get('/password', 'PasswordController@show')->name('password.show');
    Route::put('/password', 'PasswordController@update')->name('password.update');
    Route::get('/logs', 'LogController')->name('logs');
    Route::get('/avatar', 'AvatarController@show')->name('avatar.show');
    Route::post('/avatar', 'AvatarController@store')->name('avatar.store');
});

Route::resource('/sale', 'SaleController');
Route::resource('/reports', 'ReportController');
Route::resource('/reporting', 'ReportingController');
Route::resource('/configurations', 'ConfigurationController');

Route::get('/language/{language}', 'LanguageController')->name('language');

Route::group([
    'namespace' => 'Manage',
    'prefix'    => 'manage',
    'as'        => 'manage.',
], function () {
    Route::resource('users', 'UserController')->except('store', 'update', 'destroy');
    Route::resource('companies', 'CompanyController');
    Route::resource('owners', 'OwnerController');
    Route::resource('certificates', 'CertificateController');
    Route::post('certificates/storeCIDB', 'CertificateController@storeCIDB');
    Route::resource('document_acceptances', 'DocumentAcceptanceController');
    Route::get('/document_acceptances/{document_acceptances}/list', 'DocumentAcceptanceController@list')->name('document_acceptances.list');
    Route::get('/document_acceptances/{document_acceptances}/shows', 'DocumentAcceptanceController@shows')->name('document_acceptances.shows');
    Route::get('/document_acceptances/{document_acceptances}/edits', 'DocumentAcceptanceController@edits')->name('document_acceptances.edits');

    Route::group([
        'namespace' => 'Committee',
        'prefix'    => 'committee',
        'as'        => 'committee.',
    ], function () {
        Route::resource('assessments', 'AssessmentController');
        Route::resource('approvals', 'ApprovalController');
    });
});
Route::group([
        'namespace' => 'Contract\Acquisition\Document',
        'prefix'    => 'doc',
        'as'        => 'doc.',
    ], function () {
        Route::resource('addenda', 'AddendaController');
    });

Route::group([
        'namespace' => 'Contract\Acquisition',
        'prefix'    => 'acq',
        'as'        => 'acq.',
    ], function () {
        Route::resource('document', 'DocumentController');
        Route::resource('acquisition', 'AcquisitionController');
        Route::resource('review', 'ReviewController');
        Route::resource('lawreview', 'LawReviewController');
    });

Route::group([
    'namespace' => 'Contract',
    'prefix'    => 'contract',
    'as'        => 'contract.',
], function () {
    Route::resource('pre', 'PreController');
    Route::resource('post', 'PostController');
});
Route::resource('/import', 'ImportController');

Route::group([
    'namespace' => 'Contract\Post',
    'prefix'    => 'extension',
    'as'        => 'extension.',
], function () {
    Route::resource('extension', 'ExtensionController');
});
