<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push(__('Utama'), route('home'));
});

// Home > Urus ACL
Breadcrumbs::register('manage.acl.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Urus Kawalan Akses'), route('manage.acl.index'));
});

// Home > Urus Pengguna
Breadcrumbs::register('manage.users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Urus Pengguna'), route('manage.users.index'));
});

// Home > Urus Pengguna > Butiran Pengguna
Breadcrumbs::register('manage.users.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.users.index');
    $breadcrumbs->push(__('Butiran Pengguna'), route('manage.users.show', $id));
});

// Home > Urus Pengguna > Kemaskini Pengguna
Breadcrumbs::register('manage.users.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.users.index');
    $breadcrumbs->push(__('Kemaskini Pengguna'), route('manage.users.edit', $id));
});

// Home > Urus Jawatan Kuasa Penilai
Breadcrumbs::register('manage.committee.assessments.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Urus Jawatan Kuasa Penilai'), route('manage.committee.assessments.index'));
});

// Home > Urus Jawatan Kuasa Penilai > Butiran Jawatan Kuasa Penilai
Breadcrumbs::register('manage.committee.assessments.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.committee.assessments.index');
    $breadcrumbs->push(__('Butiran Jawatan Kuasa Penilai'), route('manage.committee.assessments.show', $id));
});

// Home > Urus Jawatan Kuasa Penilai > Tambah Jawatan Kuasa Penilai
Breadcrumbs::register('manage.committee.assessments.create', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.committee.assessments.index');
    $breadcrumbs->push(__('Tambah Jawatan Kuasa Penilai'), route('manage.committee.assessments.create'));
});

// Home > Urus Jawatan Kuasa Penilai > Kemaskini Jawatan Kuasa Penilai
Breadcrumbs::register('manage.committee.assessments.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.committee.assessments.index');
    $breadcrumbs->push(__('Kemaskini Jawatan Kuasa Penilai'), route('manage.committee.assessments.edit', $id));
});

// Home > Urus Jawatan Kuasa Pembuka
Breadcrumbs::register('manage.committee.approvals.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Urus Jawatan Kuasa Pembuka'), route('manage.committee.approvals.index'));
});

// Home > Urus Jawatan Kuasa Pembuka > Butiran Jawatan Kuasa Pembuka
Breadcrumbs::register('manage.committee.approvals.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.committee.approvals.index');
    $breadcrumbs->push(__('Butiran Jawatan Kuasa Pembuka'), route('manage.committee.approvals.show', $id));
});

// Home > Urus Jawatan Kuasa Pembuka > Kemaskini Jawatan Kuasa Pembuka
Breadcrumbs::register('manage.committee.approvals.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.committee.approvals.index');
    $breadcrumbs->push(__('Kemaskini Jawatan Kuasa Pembuka'), route('manage.committee.approvals.edit', $id));
});

// Home > Urus Syarikat
Breadcrumbs::register('manage.companies.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Urus Syarikat'), route('manage.companies.index'));
});

// Home > Urus Syarikat > Tambah Syarikat
Breadcrumbs::register('manage.companies.create', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Tambah Syarikat'), route('manage.companies.create'));
});

// Home > Urus Syarikat > Butiran Syarikat
Breadcrumbs::register('manage.companies.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Butiran Syarikat'), route('manage.companies.show', $id));
});

// Home > Urus Syarikat > Kemaskini Syarikat
Breadcrumbs::register('manage.companies.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
});

// Home > Urus Syarikat
Breadcrumbs::register('manage.owners.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
});

// Home > Urus Syarikat > Butiran Syarikat
Breadcrumbs::register('manage.owners.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
    $breadcrumbs->push(__('Butiran Pemilik Syarikat'), route('manage.owners.show', $id));
});

// Home > Urus Syarikat > Kemaskini Syarikat
Breadcrumbs::register('manage.owners.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
    $breadcrumbs->push(__('Kemaskini Pemilik Syarikat'), route('manage.owners.edit', $id));
});

// Home > Urus Syarikat > Sijil Kelayakan
Breadcrumbs::register('manage.certificates.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
});

// Home > Urus Syarikat > Butiran Sijil Kelayakan
Breadcrumbs::register('manage.certificates.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
    $breadcrumbs->push(__('Butiran Sijil Kelayakan'), route('manage.certificates.show', $id));
});

// Home > Urus Syarikat > Kemaskini Sijil Kelayakan
Breadcrumbs::register('manage.certificates.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.companies.index');
    $breadcrumbs->push(__('Kemaskini Syarikat'), route('manage.companies.edit', $id));
    $breadcrumbs->push(__('Kemaskini Sijil Kelayakan'), route('manage.certificates.edit', $id));
});

// Home > Penerimaan Dokumen
Breadcrumbs::register('manage.document_acceptances.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Penerimaan Dokumen'), route('manage.document_acceptances.index'));
});

// Home > Senarai Penerimaan Dokumen
Breadcrumbs::register('manage.document_acceptances.list', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.document_acceptances.index', $id);
    $breadcrumbs->push(__('Senarai Penerimaan Dokumen'), route('manage.document_acceptances.list', $id));
});

// Home > Urus Syarikat > Butiran Penerimaan Dokumen
Breadcrumbs::register('manage.document_acceptances.shows', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.document_acceptances.index');
    $breadcrumbs->push(__('Butiran Penerimaan Dokumen'), route('manage.document_acceptances.shows', $id));
});

// Home > Urus Syarikat > Kemaskini Penerimaan Dokumen
Breadcrumbs::register('manage.document_acceptances.edits', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('manage.document_acceptances.index');
    $breadcrumbs->push(__('Kemaskini Penerimaan Dokumen'), route('manage.document_acceptances.edits', $id));
});

// Home > Pra-Kontrak
Breadcrumbs::register('contract.pre.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Pra-Kontrak');
    $breadcrumbs->push(__('Senarai Kelulusan Perolehan'), route('contract.pre.index'));
});

// Home > Pra-Kontrak > Butiran Pra-Kontrak
Breadcrumbs::register('contract.pre.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.pre.index');
    $breadcrumbs->push(__('Butiran Kelulusan Perolehan'), route('contract.pre.show', $id));
});

// Home > Pra-Kontrak > Kemaskini Pra-Kontrak
Breadcrumbs::register('contract.pre.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.pre.index');
    $breadcrumbs->push(__('Kemaskini Kelulusan Perolehan'), route('contract.pre.edit', $id));
});

// Home > Pra-Kontrak > Jana Pra-Kontrak
Breadcrumbs::register('contract.pre.create', function ($breadcrumbs) {
    $breadcrumbs->parent('contract.pre.index');
    $breadcrumbs->push(__('Tambah Kelulusan Perolehan'), route('contract.pre.create'));
});

// Home > Pasca-Kontrak
Breadcrumbs::register('contract.post.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Pasca-Kontrak'), route('contract.post.index'));
});

// Home > Pasca-Kontrak > Butiran Pasca-Kontrak
Breadcrumbs::register('contract.post.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Butiran Pasca-Kontrak'), route('contract.post.show', $id));
});

// Home > Pasca-Kontrak > Kemaskini Pasca-Kontrak
Breadcrumbs::register('contract.post.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Kemaskini Pasca-Kontrak'), route('contract.post.edit', $id));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension.index'));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Butiran Pasca-Kontrak
Breadcrumbs::register('extension.extension.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Butiran Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension.show', $id));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Tambah Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension.create', function ($breadcrumbs) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Tambah Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension.create'));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Kemaskini Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Kemaskini Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension.edit', $id));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Senarai Kemaskini Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension_edit.index', function ($breadcrumbs) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Senarai Kemaskini Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension_edit.index'));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Kemaskini Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension_edit.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Kemaskini Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension_edit.edit', $id));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Senarai Kelulusan Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension_show.index', function ($breadcrumbs) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Senarai Kelulusan Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension_show.index'));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Butiran Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension_show.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Butiran Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension_show.show', $id));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Senarai Kelulusan Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension_approve.index', function ($breadcrumbs) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Senarai Kelulusan Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension_approve.index'));
});

// Home > Sijil Kelambatan & Lanjutan Kontrak > Kelulusan Sijil Kelambatan & Lanjutan Kontrak
Breadcrumbs::register('extension.extension_approve.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('extension.extension.index');
    $breadcrumbs->push(__('Kelulusan Sijil Kelambatan & Lanjutan Kontrak'), route('extension.extension_approve.edit', $id));
});

// Home > Penamatan Kontrak
Breadcrumbs::register('termination.termination.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Penamatan Kontrak'), route('termination.termination.index'));
});

// Home > Penamatan Kontrak > Kemaskini Penamatan Kontrak
Breadcrumbs::register('termination.termination.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Kemaskini Penamatan Kontrak'), route('termination.termination.edit', $id));
});

// Home > Warning Letter
Breadcrumbs::register('termination.warning.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Surat Amaran'), route('termination.warning.index'));
});

// Home > Warning Letter > Baru
Breadcrumbs::register('termination.warning.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Tambah Surat Amaran'), route('termination.warning.create'));
});

// Home > Warning Letter > Kemaskini
Breadcrumbs::register('termination.warning.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Kemaskini Surat Amaran'), route('termination.warning.edit', $id));
});

// Home > Warning Letter > Butiran
Breadcrumbs::register('termination.warning.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Butiran Surat Amaran'), route('termination.warning.show', $id));
});

// Home > Surat Setuju Terima
Breadcrumbs::register('acceptance-letter.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Surat Setuju Terima'), route('acceptance-letter.index'));
});

// Home > Surat Setuju Terima > Butiran Surat Setuju Terima
Breadcrumbs::register('acceptance-letter.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acceptance-letter.index');
    $breadcrumbs->push(__('Butiran Surat Setuju Terima'), route('acceptance-letter.show', $id));
});

// Home > Surat Setuju Terima > Kemaskini Surat Setuju Terima
Breadcrumbs::register('acceptance-letter.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acceptance-letter.index');
    $breadcrumbs->push(__('Kemaskini Surat Setuju Terima'), route('acceptance-letter.edit', $id));
});

// Home > Urus Jualan
Breadcrumbs::register('sale.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Jualan'), route('sale.index'));
});

// Home > Urus Jualan > Butiran Jualan
Breadcrumbs::register('sale.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Butiran Jualan'), route('sale.show', $id));
});

// Home > Urus Jualan > Kemaskini Jualan
Breadcrumbs::register('sale.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Kemaskini Jualan'), route('sale.edit', $id));
});

// Home > Urus Jualan > Carian Jualan
Breadcrumbs::register('sale.search', function ($breadcrumbs) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Carian Jualan'), route('sale.search'));
});

// Home > Urus Jualan > Tambah Wakil
Breadcrumbs::register('sale.wakil', function ($breadcrumbs) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Tambah Wakil'), route('sale.wakil'));
});

// Home > Urus Jualan > Penjualan
Breadcrumbs::register('sale.store', function ($breadcrumbs) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Penjualan'), route('sale.store'));
});

// Home > Urus Jualan > Kemaskini Resit
Breadcrumbs::register('sale.receipt', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Kemaskini Rekod Penjualan'), route('sale.receipt', $id));
});

// Home > Urus Jualan > Update
Breadcrumbs::register('sale.update', function ($breadcrumbs) {
    $breadcrumbs->parent('sale.index');
    $breadcrumbs->push(__('Kemaskini Penjualan'), route('sale.update'));
});

// Home > Laporan
Breadcrumbs::register('reports.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Laporan'), route('reports.index'));
});

// Home > Laporan > Butiran Laporan
Breadcrumbs::register('reports.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reports.index');
    $breadcrumbs->push(__('Butiran Laporan'), route('reports.show', $id));
});

// Home > Taklimat
Breadcrumbs::register('briefing.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Taklimat'), route('briefing.index'));
});

// Home > Taklimat > Butiran Taklimat
Breadcrumbs::register('briefing.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('briefing.index');
    $breadcrumbs->push(__('Butiran Taklimat'), route('briefing.show', $id));
});

// Home > Taklimat > Jana Taklimat
Breadcrumbs::register('briefing.create', function ($breadcrumbs) {
    $breadcrumbs->parent('briefing.index');
    $breadcrumbs->push(__('Jana Taklimat'), route('briefing.create'));
});

// Home > Taklimat > Kemaskini Taklimat
Breadcrumbs::register('briefing.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('briefing.index');
    $breadcrumbs->push(__('Kemaskini Taklimat'), route('briefing.edit', $id));
});

// Home > Taklimat > Kemaskini Taklimat
Breadcrumbs::register('briefing.listing', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('briefing.index');
    $breadcrumbs->push(__('Senarai Kehadiran'), route('briefing.listing', $id));
});

// Home > Lawatan Tapak
Breadcrumbs::register('site-visit.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Lawatan Tapak'), route('site-visit.index'));
});

// Home > Lawatan Tapak > Butiran Lawatan Tapak
Breadcrumbs::register('site-visit.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('site-visit.index');
    $breadcrumbs->push(__('Butiran Lawatan Tapak'), route('site-visit.show', $id));
});

// Home > Lawatan Tapak > Jana Lawatan Tapak
Breadcrumbs::register('site-visit.create', function ($breadcrumbs) {
    $breadcrumbs->parent('site-visit.index');
    $breadcrumbs->push(__('Jana Lawatan Tapak'), route('site-visit.create'));
});

// Home > Lawatan Tapak > Kemaskini Lawatan Tapak
Breadcrumbs::register('site-visit.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('site-visit.index');
    $breadcrumbs->push(__('Kemaskini Lawatan Tapak'), route('site-visit.edit', $id));
});

// Home > Konfigurasi
Breadcrumbs::register('configurations.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Konfigurasi'), route('configurations.index'));
});

// Home > Konfigurasi > Butiran Konfigurasi
Breadcrumbs::register('configurations.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('configurations.index');
    $breadcrumbs->push(__('Butiran Konfigurasi'), route('configurations.show', $id));
});

// Home > Konfigurasi > Kemaskini Konfigurasi
Breadcrumbs::register('configurations.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('configurations.index');
    $breadcrumbs->push(__('Kemaskini Konfigurasi'), route('configurations.edit', $id));
});

// Home > Profile
Breadcrumbs::register('user.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Profil'), route('user.show'));
});

// Home > Profile > Avatar
Breadcrumbs::register('user.avatar.show', function ($breadcrumbs) {
    $breadcrumbs->parent('user.show');
    $breadcrumbs->push(__('Avatar'), route('user.avatar.show'));
});

// Home > Profile > Security
Breadcrumbs::register('user.password.show', function ($breadcrumbs) {
    $breadcrumbs->parent('user.show');
    $breadcrumbs->push(__('Sekuriti'), route('user.password.show'));
});

// Home > Profile > Logs
Breadcrumbs::register('user.logs', function ($breadcrumbs) {
    $breadcrumbs->parent('user.show');
    $breadcrumbs->push(__('Log'), route('user.logs'));
});

// Home > Kelulusan >Maklumat Perolehan
Breadcrumbs::register('acquisition.document.index', function ($breadcrumbs) {
    $breadcrumbs->parent('contract.pre.index');
    $breadcrumbs->push(__('Senarai Dokumen Perolehan'), route('acquisition.document.index', ['hashslug' => session('approval_hashslug')]));
});

// Home > Kelulusan >Maklumat Perolehan
Breadcrumbs::register('acquisition.document.create', function ($breadcrumbs) {
    $breadcrumbs->parent('acquisition.document.index');
    $breadcrumbs->push(__('Tambah Dokumen Perolehan'), route('acquisition.document.create'));
});

// Home > Kelulusan > Butiran Maklumat Perolehan
Breadcrumbs::register('acquisition.document.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.acquisition.index');
    $breadcrumbs->push(__('Butiran Dokumen Perolehan'), route('acquisition.document.show', $id));
});

// Home > Penyerahan Tugasan
Breadcrumbs::register('acquisition.review.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Penyerahan Tugasan'), route('acquisition.review.index'));
});

// Home > Pembatalan Perolehan
Breadcrumbs::register('acquisition.cancel.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Senarai Pembatalan Perolehan'), route('acquisition.cancel.index'));
});

// Home > Kelulusan > Butiran Maklumat Perolehan
Breadcrumbs::register('acquisition.review.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.acquisition.index');
    $breadcrumbs->push(__('Semakan'), route('acquisition.review.show', $id));
});

// Home > Kelulusan > Butiran Maklumat Perolehan
Breadcrumbs::register('acquisition.cancel.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.cancel.index');
    $breadcrumbs->push(__('Semakan'), route('acquisition.cancel.show', $id));
});

// Home > Kelulusan > Kemaskini Maklumat Perolehan
Breadcrumbs::register('acquisition.document.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.acquisition.index');
    $breadcrumbs->push(__('Kemaskini Dokumen Perolehan'), route('acquisition.document.edit', $id));
});

// Home > Kelulusan > Kemaskini Maklumat Perolehan
Breadcrumbs::register('acquisition.cancel.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('acquisition.cancel.index');
    $breadcrumbs->push(__('Kemaskini Dokumen Perolehan'), route('acquisition.cancel.edit', $id));
});

// Home > Buka Peti
Breadcrumbs::register('box.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Buka Peti'), route('box.index'));
});

// Home > Buka Peti > Kemaskini
Breadcrumbs::register('box.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('box.index');
    $breadcrumbs->push(__('Kemaskini'), route('box.edit', $id));
});

// Home > Buka Peti > Butiran
Breadcrumbs::register('box.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('box.index');
    $breadcrumbs->push(__('Butiran'), route('box.show', $id));
});

// Home > Keputusan Perolehan
Breadcrumbs::register('evaluation.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Keputusan Perolehan'), route('evaluation.index'));
});

// Home > Keputusan Perolehan > Kemaskini
Breadcrumbs::register('evaluation.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('evaluation.index');
    $breadcrumbs->push(__('Kemaskini'), route('evaluation.edit', $id));
});

// Home > Keputusan Perolehan > Butiran
Breadcrumbs::register('evaluation.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('evaluation.index');
    $breadcrumbs->push(__('Butiran'), route('evaluation.show', $id));
});

// Home > Kelulusan >Maklumat Perolehan
Breadcrumbs::register('doc.addenda.index', function ($breadcrumbs) {
    $breadcrumbs->parent('acquisition.acquisition.index');
    $breadcrumbs->push(__('Senarai Addenda'), route('doc.addenda.index'));
});
// Home  >Maklumat Perolehan
Breadcrumbs::register('acquisition.acquisition.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Senarai Dokumen Perolehan'), route('acquisition.acquisition.index'));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Sijil Bayaran Interim'), route('contract.post.ipc.index'));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.ipc.index');
    $breadcrumbs->push(__('Senarai Kemaskini Sijil Interim'), route('contract.post.ipc.edit', $id));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.ipc.index');
    $breadcrumbs->push(__('Butiran Sijil Bayaran Interim'), route('contract.post.ipc.show', $id));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.summary', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.ipc.index');
    $breadcrumbs->push(__('Kemaskini Sijil Bayaran Interim'), route('contract.post.ipc.summary', $id));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.ipc_invoice', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.ipc.index', $id);
    $breadcrumbs->push(__('Tambah Sijil Bayaran Interim'), route('contract.post.ipc.ipc_invoice', $id));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.ipc_invoice_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.ipc.index', $id);
    $breadcrumbs->push(__('Kemaskini Sijil Bayaran Interim'), route('contract.post.ipc.ipc_invoice_edit', $id));
});

// Home  >Sijil Bayaran Interim
Breadcrumbs::register('contract.post.ipc.ipc_show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.ipc.index', $id);
    $breadcrumbs->push(__('Kemaskini Sijil Bayaran Interim'), route('contract.post.ipc.ipc_show', $id));
});

// Home > Pasca-Kontrak > Kemaskini Sijil Siap Kerja
Breadcrumbs::register('contract.post.cpc.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Kemaskini Sijil Siap Kerja'), route('contract.post.cpc.edit', $id));
});

// Home > Pasca-Kontrak > Kemaskini Sijil Tidak Siap Kerja
Breadcrumbs::register('contract.post.cnc.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Kemaskini Sijil Tidak Siap Kerja'), route('contract.post.cnc.edit', $id));
});

// Home > Pasca-Kontrak > Kemaskini Sijil Siap Membaiki Kecacatan
Breadcrumbs::register('contract.post.cmgd.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Kemaskini Sijil Siap Membaiki Kecacatan'), route('contract.post.cmgd.edit', $id));
});

// Home  > Sofa > Senarai
Breadcrumbs::register('contract.post.sofa.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Bayaran Muktamad'), route('contract.post.sofa.index'));
});

// Home > Sofa > Kemaskini Sofa
Breadcrumbs::register('contract.post.sofa.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.sofa.index');
    $breadcrumbs->push(__('Kemaskini Bayaran Muktamad'), route('contract.post.sofa.edit', $id));
});

// Home > Sofa > Butiran Sofa
Breadcrumbs::register('contract.post.sofa.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.sofa.index');
    $breadcrumbs->push(__('Butiran Bayaran Muktamad'), route('contract.post.sofa.show', $id));
});

// Home > Pasca-Kontrak > Kemaskini Sijil Siap Kerja Separa
Breadcrumbs::register('contract.post.cpo.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.index');
    $breadcrumbs->push(__('Kemaskini Sijil Siap Kerja Separa'), route('contract.post.cpo.edit', $id));
});

// Home  > Completed Contract > Senarai
Breadcrumbs::register('contract.post.contract-completed.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Senarai Kontrak Selesai'), route('contract.post.contract-completed.index'));
});

// Home > Completed Contract > Butiran
Breadcrumbs::register('contract.post.contract-completed.showPerolehan', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.contract-completed.index');
    $breadcrumbs->push(__('Butiran Penuh Perolehan'), route('contract.post.contract-completed.showPerolehan', $id));
});

// Home > Utiliti
Breadcrumbs::register('manage.utility.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Senarai Utiliti'), route('manage.utility.index'));
});

// Home > Utiliti > Authorities
Breadcrumbs::register('utility.authorities.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Senarai Pihak Authorities'), route('utility.authorities.index'));
});

// Home > Utiliti > Bank
Breadcrumbs::register('utility.bank.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Senarai Bank'), route('utility.bank.index'));
});

// Home > Utiliti > Jenis Bon
Breadcrumbs::register('utility.bon_type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jenis Bon'), route('utility.bon_type.index'));
});

// Home > Utiliti > Kod Bajet
Breadcrumbs::register('utility.budget_code.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Kod Bajet'), route('utility.budget_code.index'));
});

// Home > Utiliti > Negeri
Breadcrumbs::register('utility.state.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Negeri'), route('utility.state.index'));
});

// Home > Utiliti > Jabatan
Breadcrumbs::register('utility.department.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jabatan'), route('utility.department.index'));
});

// Home > Utiliti > Bahagian
Breadcrumbs::register('utility.division.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Bahagian'), route('utility.division.index'));
});

// Home > Utiliti > Seksyen
Breadcrumbs::register('utility.section.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Seksyen'), route('utility.section.index'));
});

// Home > Utiliti > Jawatan
Breadcrumbs::register('utility.position.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jawatan'), route('utility.position.index'));
});

// Home > Utiliti > Skim
Breadcrumbs::register('utility.scheme.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Skim'), route('utility.scheme.index'));
});

// Home > Utiliti > Gred
Breadcrumbs::register('utility.grade.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Gred'), route('utility.grade.index'));
});

// Home > Utiliti > Kumpulan
Breadcrumbs::register('utility.role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Kumpulan'), route('utility.role.index'));
});

// Home > Utiliti > Lokasi
Breadcrumbs::register('utility.location.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Lokasi'), route('utility.location.index'));
});

// Home > Utiliti > Jenis Tempoh
Breadcrumbs::register('utility.period_type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jenis Tempoh'), route('utility.period_type.index'));
});

// Home > Utiliti > Tempoh Liability
Breadcrumbs::register('utility.liability_period.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Tempoh Liability'), route('utility.liability_period.index'));
});

// Home > Utiliti > Insuran
Breadcrumbs::register('utility.insurance.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Insuran'), route('utility.insurance.index'));
});

// Home > Utiliti > Gelaran
Breadcrumbs::register('utility.honorary.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Gelaran'), route('utility.honorary.index'));
});

// Home > Utiliti > Jenis Perolehan
Breadcrumbs::register('utility.acquisition_type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jenis Perolehan'), route('utility.acquisition_type.index'));
});

// Home > Utiliti > Kategori Perolehan
Breadcrumbs::register('utility.acquisition_category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Kategori Perolehan'), route('utility.acquisition_category.index'));
});

// Home > Utiliti > Sumber Peruntukan
Breadcrumbs::register('utility.allocation_resource.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Sumber Peruntukan'), route('utility.allocation_resource.index'));
});

// Home > Utiliti > Jenis Penamatan
Breadcrumbs::register('utility.termination_type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jenis Penamatan'), route('utility.termination_type.index'));
});

// Home > Utiliti > Jenis Perubahan Kerja
Breadcrumbs::register('utility.vo_type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Jenis Perubahan Kerja'), route('utility.vo_type.index'));
});

// Home > Utiliti > CIDB:Grade
Breadcrumbs::register('utility.cidb_grade.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('CIDB:Grade'), route('utility.cidb_grade.index'));
});

// Home > Utiliti > CIDB:Kategori
Breadcrumbs::register('utility.cidb_category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('CIDB:Kategori'), route('utility.cidb_category.index'));
});

// Home > Utiliti > CIDB:Pengkhususan
Breadcrumbs::register('utility.cidb_khusus.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('CIDB:Pengkhususan'), route('utility.cidb_khusus.index'));
});

// Home > Utiliti > MOF:Bidang
Breadcrumbs::register('utility.mof_area.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('MOF:Bidang'), route('utility.mof_area.index'));
});

// Home > Utiliti > MOF:Pengkhususan
Breadcrumbs::register('utility.mof_khusus.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('MOF:Pengkhususan'), route('utility.mof_khusus.index'));
});

// Home > Utiliti > MOF:Pengkhususan
Breadcrumbs::register('utility.voc.index', function ($breadcrumbs) {
    $breadcrumbs->parent('manage.utility.index');
    $breadcrumbs->push(__('Urusetia Perubahan Kerja (VOC)'), route('utility.voc.index'));
});


// Home > Pelepasan Bon
Breadcrumbs::register('contract.post.release.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Pelepasan Bon'), route('contract.post.release.index'));
});

// Home > Pelepasan Bon > Kemaskini
Breadcrumbs::register('contract.post.release.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.release.index');
    $breadcrumbs->push(__('Kemaskini'), route('contract.post.release.edit', $id));
});

// Home > Pelepasan Bon > Butiran
Breadcrumbs::register('contract.post.release.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('contract.post.release.index');
    $breadcrumbs->push(__('Butiran'), route('contract.post.release.show', $id));
});

// Home > Reporting > Prestasi
Breadcrumbs::register('reporting.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Laporan Prestasi'), route('reporting.index'));
});
// Home > Reporting > Prestasi
Breadcrumbs::register('reporting.prestasi', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reporting.index');
    $breadcrumbs->push(__('Laporan Prestasi'), route('reporting.prestasi', [$id,$id]));
});

// Home > Manual Pengguna
Breadcrumbs::register('user_manual.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(__('Manual Pengguna'), route('user_manual.index'));
});

collect(glob(base_path('/routes/breadcrumbs/*.php')))
    ->each(function ($path) {
        require $path;
    });
