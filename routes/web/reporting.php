<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::resource('/reporting', 'ReportingController');
    Route::get('/reporting/{month}/{year}/prestasi', 'ReportingController@prestasi')->name('reporting.prestasi');
});
