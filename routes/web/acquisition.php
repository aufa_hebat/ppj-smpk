<?php

Route::group([
    'namespace'  => 'Contract\Acquisition',
    'prefix'     => 'acquisition',
    'as'         => 'acquisition.',
    'middleware' => ['auth'],
], function () {
    Route::resource('document', 'DocumentController');
    Route::resource('acquisition', 'AcquisitionController');
    Route::resource('activity', 'ActivityController');
    Route::resource('monitoring', 'MonitoringController');
    Route::resource('review', 'ReviewController');
    Route::resource('lawreview', 'LawReviewController');
    Route::resource('cancel', 'CancelController');
    //Route::put('/updateprogresschart', 'MonitoringController@updateprogresschart')->name('monitoring.updateprogresschart');
});
Route::get('/acq_notice', 'ReportViewerController@acq_notice')->name('acq_notice');
Route::get('/acq_bq', 'ReportViewerController@acq_bq')->name('acq_bq');
