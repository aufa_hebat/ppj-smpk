<?php


Route::group([
    'namespace'  => 'Contract\Post',
    'prefix'     => 'post',
    'as'         => 'contract.post.',
    'middleware' => ['auth'],
], function () {
    Route::resource('release', 'ReleaseController');
});
