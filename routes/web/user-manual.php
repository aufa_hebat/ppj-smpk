<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::resource('/user_manual', 'UserManualController');    
});
