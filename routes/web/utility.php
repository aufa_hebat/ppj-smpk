<?php

Route::group([
    'namespace'  => 'Manage\Utility',
    'prefix'     => 'utility',
    'as'         => 'utility.',
    'middleware' => ['auth'],
], function () {
    Route::resource('authorities', 'AuthoritiesController');
    Route::resource('bank', 'BankController');
    Route::resource('bon_type', 'BonTypeController');
    Route::resource('budget_code', 'BudgetCodeController');
    Route::resource('state', 'StateController');
    Route::resource('department', 'DepartmentController');
    Route::resource('division', 'DivisionController');
    Route::resource('section', 'SectionController');
    Route::resource('position', 'PositionController');
    Route::resource('scheme', 'SchemeController');
    Route::resource('grade', 'GradeController');
    Route::resource('role', 'RoleController');
    Route::resource('location', 'LocationController');
    Route::resource('period_type', 'PeriodTypeController');
    Route::resource('liability_period', 'LiabilityPeriodController');
    Route::resource('insurance', 'InsuranceController');
    Route::resource('honorary', 'HonoraryController');
    Route::resource('acquisition_type', 'AcquisitionTypeController');
    Route::resource('acquisition_category', 'AcquisitionCategoryController');
    Route::resource('allocation_resource', 'AllocationResourceController');
    Route::resource('termination_type', 'TerminationTypeController');
    Route::resource('vo_type', 'VariationOrderTypeController');
    Route::resource('cidb_grade', 'CIDBGradeController');
    Route::resource('cidb_category', 'CIDBCategoryController');
    Route::resource('cidb_khusus', 'CIDBKhususController');
    Route::resource('mof_area', 'MOFAreaController');
    Route::resource('mof_khusus', 'MOFKhususController');
    Route::resource('voc', 'VocController');
});
