<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

 //Route::get('/', 'IndexController')->name('index');
 
 //Route::get('/welcome', 'WelcomeController')->name('welcome');

 Route::get('/', 'WelcomeController')->name('welcome');

Route::get('/mylogout', 'LogoutXController@logoutx')->name('mylogout');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/lasdjlaksdjasdj', 'ChooseRoleController@index')->name('lasdjlaksdjasdj');
Route::get('/lasdjlaksdjasdj2', 'ChooseRoleController@chooseRole')->name('lasdjlaksdjasdj2');
Route::post('/lasdjlaksdjasdj_post', 'ChooseRoleController@update')->name('lasdjlaksdjasdj_post');

Route::get('/language/{language}', 'LanguageController')->name('language');
