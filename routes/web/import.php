<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::post('/import/parse', 'ImportController@parse')->name('import.parse');

    Route::get('/importvo/csv', 'ImportVOController@csv')->name('importvo.csv');
    Route::post('/importvo/parse', 'ImportVOController@parse')->name('importvo.parse');
    Route::post('/importvo/process', 'ImportVOController@process')->name('importvo.process');
    Route::get('/importvo/adjust_csv', 'ImportVOController@adjust_csv')->name('importvo.adjust_csv');
    Route::post('/importvo/adjust_parse', 'ImportVOController@adjust_parse')->name('importvo.adjust_parse');
    Route::post('/importvo/adjust_process', 'ImportVOController@adjust_process')->name('importvo.adjust_process');
});
