<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::resource('box', 'BoxController');
    Route::get('/jadualPerolehan', 'ReportViewerController@jadualPerolehan')->name('jadualPerolehan');
    Route::get('/download/{file}', 'BoxController@downloadFile');
});
