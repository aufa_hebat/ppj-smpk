<?php

Route::group([
    'namespace'  => 'Contract\Post',
    'prefix'     => 'post',
    'as'         => 'contract.post.',
    'middleware' => ['auth'],
], function () {
    Route::resource('ipc', 'IpcController');
    Route::get('/ipc/{ipc}/summary', 'IpcController@summary')->name('ipc.summary');
    Route::get('/ipc/{ipc}/ipc_invoice', 'IpcController@ipc_invoice')->name('ipc.ipc_invoice');
    Route::get('/ipc/{ipc}/ipc_invoice_edit', 'IpcController@ipc_invoice_edit')->name('ipc.ipc_invoice_edit');
    Route::get('/ipc/{ipc}/ipc_show', 'IpcController@ipc_show')->name('ipc.ipc_show');
    Route::get('/sap_vendor/{ssm}/{route}/{id}', 'IpcController@sap_vendor')->name('sap_vendor');
    Route::post('/sap', 'IpcController@sap')->name('sap');
    Route::post('/update_vendor', 'IpcController@update_vendor')->name('update_vendor');
    Route::post('/destroy_invoice/{inv}', 'IpcController@destroy_invoice')->name('ipc.destroy_invoice');
});

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/ipc_cert', 'ReportViewerController@ipc_cert')->name('ipc_cert');
    Route::get('/ipc_summary', 'ReportViewerController@ipc_summary')->name('ipc_summary');
    Route::get('/ipc_status', 'ReportViewerController@ipc_status')->name('ipc_status');
    Route::get('/ipc_butiran', 'ReportViewerController@ipc_butiran')->name('ipc_butiran');
    Route::get('/ipc_first_checklist', 'ReportViewerController@ipc_first_checklist')->name('ipc_first_checklist_report');
    Route::get('/ipc_current_checklist', 'ReportViewerController@ipc_current_checklist')->name('ipc_current_checklist_report');
    Route::get('/ipc_final_checklist', 'ReportViewerController@ipc_final_checklist')->name('ipc_final_checklist_report');
});
