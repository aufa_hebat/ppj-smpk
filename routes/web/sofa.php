<?php


Route::group([
    'namespace'  => 'Contract\Post',
    'prefix'     => 'post',
    'as'         => 'contract.post.',
    'middleware' => ['auth'],
], function () {
    Route::resource('sofa', 'SofaController');
});

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/sofaRpt', 'ReportViewerController@sofaRpt')->name('sofaRpt')->middleware('auth');
    Route::get('/akuanRpt', 'ReportViewerController@akuanRpt')->name('akuanRpt')->middleware('auth');
});
