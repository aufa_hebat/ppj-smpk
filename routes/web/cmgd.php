<?php

Route::group([
    'namespace' => 'Contract\Post',
    'prefix'    => 'post',
    'as'        => 'contract.post.',
], function () {
    Route::resource('cmgd', 'CmgdController');
});

Route::get('cmgdCertificate', 'ReportViewerController@cmgdCertificate')->name('cmgdCertificate');
