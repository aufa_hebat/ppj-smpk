<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/taskclaim', 'TaskController@claim')->name('task.claim');
});
