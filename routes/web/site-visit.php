<?php

Route::resource('/site-visit', 'SiteVisitController')->middleware('auth');
Route::resource('/contract-progress', 'ContractProgressController')->middleware('auth');
