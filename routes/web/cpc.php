<?php

Route::group([
    'namespace' => 'Contract\Post',
    'prefix'    => 'post',
    'as'        => 'contract.post.',
], function () {
    Route::resource('cpc', 'CpcController');
});

Route::get('cpcCertificate', 'ReportViewerController@cpcCertificate')->name('cpcCertificate');
