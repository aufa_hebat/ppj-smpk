<?php

Route::group([
    'namespace' => 'Registration',
    'prefix'    => 'registration',
    'as'        => 'registration.',
], function () {
    Route::resource('companies', 'CompanyController');
    Route::resource('certificates', 'CertificateController');
    Route::resource('owners', 'OwnerController');
    Route::delete('/companies/delete/{id}', 'CompanyController@delete')->name('companies.delete');
    Route::delete('/certificates/delete_ssm/{id}', 'CertificateController@delete_ssm')->name('certificates.delete_ssm');
    Route::delete('/certificates/delete_cidb/{id}', 'CertificateController@delete_cidb')->name('certificates.delete_cidb');
    Route::delete('/certificates/delete_bpku/{id}', 'CertificateController@delete_bpku')->name('certificates.delete_bpku');
    Route::delete('/certificates/delete_spkk/{id}', 'CertificateController@delete_spkk')->name('certificates.delete_spkk');
    Route::delete('/certificates/delete_mof/{id}', 'CertificateController@delete_mof')->name('certificates.delete_mof');
    Route::delete('/certificates/delete_bumi/{id}', 'CertificateController@delete_bumi')->name('certificates.delete_bumi');
    Route::delete('/certificates/delete_kdn/{id}', 'CertificateController@delete_kdn')->name('certificates.delete_kdn');
});
