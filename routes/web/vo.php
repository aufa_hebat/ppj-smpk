<?php


Route::group([
    'namespace' => 'Contract\Post',
    'prefix'    => 'post',
    'as'        => 'contract.post.',
], function () {
    Route::resource('variation-order', 'VariationOrderController');
    Route::get('/variation-order/{vo}/ppk', 'VariationOrderController@ppk')->name('variation-order.ppk');
    Route::get('/variation-order/{vo}/ppk_edit', 'VariationOrderController@ppk_edit')->name('variation-order.ppk_edit');
    Route::get('/variation-order/{vo}/active-wps', 'VariationOrderController@activeWps')->name('variation-order.active-wps');
    Route::get('/variation-order/{vo}/pk', 'VariationOrderController@pk')->name('variation-order.pk');
    Route::get('/variation-order/{vo}/kkk', 'VariationOrderController@kkk')->name('variation-order.kkk');
    Route::get('/variation-order/{vo}/ps', 'VariationOrderController@ps')->name('variation-order.ps');
    Route::get('/variation-order/{vo}/searchBQ', 'VariationOrderController@searchBQ')->name('variation-order.searchBQ');
    Route::post('/variation-order/{id}/selectBQ', 'VariationOrderController@selectBQ')->name('variation-order.selectBQ');
    Route::get('/variation-order/{voTypeId}/searchBQPSKKK', 'VariationOrderController@searchBQPSKKK')->name('variation-order.searchBQ-PS-KKK');
    Route::post('/variation-order/{voTypeId}/selectBQPSKKK', 'VariationOrderController@selectBQPSKKK')->name('variation-order.selectBQ-PS-KKK');
    Route::get('/variation-order/{vo}/{id}/wps', 'VariationOrderController@wps')->name('variation-order.wps');
    Route::get('/variation-order/{vo}/wpsSelect', 'VariationOrderController@editSelectWps')->name('variation-order.wpsSelect');   
    Route::get('/variation-order/{vo}/cancel-wps', 'VariationOrderController@cancelWps')->name('variation-order.cancel-wps'); 
    
    Route::get('/variation-order/{vo}/ppk_show', 'VariationOrderController@ppk_show')->name('variation-order.ppk_show');
    Route::get('/variation-order/{vo}/show-active-wps', 'VariationOrderController@showActiveWps')->name('variation-order.show-active-wps');
    Route::get('/variation-order/{vo}/show-pk', 'VariationOrderController@showPk')->name('variation-order.show-pk');
    Route::get('/variation-order/{vo}/show-wpsSelect', 'VariationOrderController@showSelectWps')->name('variation-order.show-wpsSelect'); 
    Route::get('/variation-order/{vo}/{id}show-wps', 'VariationOrderController@showWps')->name('variation-order.show-wps'); 
    Route::get('/variation-order/{vo}/show-ps', 'VariationOrderController@showPs')->name('variation-order.show-ps');
    Route::get('/variation-order/{vo}/show-kkk', 'VariationOrderController@showKkk')->name('variation-order.show-kkk');
    Route::get('/variation-order/{vo}/show-cancel-wps', 'VariationOrderController@showCancelWps')->name('variation-order.show-cancel-wps');
    

    Route::resource('variation-order-result', 'VariationOrderResultController');
    Route::get('/variation-order-result/{vo}/ppk_amend', 'VariationOrderResultController@ppk_amend')->name('variation-order-result.ppk_amend');
    Route::get('/variation-order-result/{vo}/ppk_amend_result', 'VariationOrderResultController@ppkAmendResult')->name('variation-order-result.ppk_amend_result');
    Route::get('/variation-order-result/{vo}/ppk_amend_approver', 'VariationOrderResultController@ppkAmendApprover')->name('variation-order-result.ppk_amend_approver');
    
    Route::get('/variation-order-result/{vo}/ppk_amend_show', 'VariationOrderResultController@ppkAmendShow')->name('variation-order-result.ppk_amend_show');
    Route::get('/variation-order-result/{vo}/show-ppk-amend-result', 'VariationOrderResultController@showPpkAmendResult')->name('variation-order-result.show-ppk-amend-result');
    Route::get('/variation-order-result/{vo}/show-ppk-amend-approver', 'VariationOrderResultController@showPpkAmendApprover')->name('variation-order-result.show-ppk-amend-approver');    
//    Route::get('/variation-order/{vo}/ppk_amend_result', 'VariationOrderController@ppkAmendResult')->name('variation-order.ppk_amend_result');
//    Route::get('/variation-order/{vo}/ppk_amend_approver', 'VariationOrderController@ppkAmendApprover')->name('variation-order.ppk_amend_approver');

    Route::resource('/variation-order-ppjhk', 'VariationOrderPpjhkController');
    Route::get('/variation-order-ppjhk/{vo}/ppjhk', 'VariationOrderPpjhkController@ppjhk')->name('variation-order-ppjhk.ppjhk');
    Route::get('/variation-order-ppjhk/{vo}/amend', 'VariationOrderPpjhkController@amend')->name('variation-order-ppjhk.amend');
    Route::get('/variation-order-ppjhk/{vo}/show-amend', 'VariationOrderPpjhkController@showAmend')->name('variation-order-ppjhk.show-amend');


    Route::get('/variation-order-wps/{vo}/wps-active', 'VariationOrderWpsController@wpsActive')->name('variation-order-wps.wps-active');
    Route::get('/variation-order-wps/{vo}/wps-create', 'VariationOrderWpsController@wpsCreate')->name('variation-order-wps.wps-create');
    Route::get('/variation-order-wps/{vo}/wps-edit', 'VariationOrderWpsController@wpsEdit')->name('variation-order-wps.wps-edit');
    Route::get('/variation-order-wps/{vo}/{id}/wps-select', 'VariationOrderWpsController@wpsSelect')->name('variation-order-wps.wps-select');
    Route::get('/variation-order-wps/{vo}/wps-show', 'VariationOrderWpsController@wpsShow')->name('variation-order-wps.wps-show');
    Route::get('/variation-order-wps/{vo}/{id}/wps-show-select', 'VariationOrderWpsController@wpsShowSelect')->name('variation-order-wps.wps-show-select');
});
Route::get('/vo_kkk', 'ReportViewerController@vo_kkk')->name('vo_kkk');
Route::get('/vo_ppk', 'ReportViewerController@vo_ppk')->name('vo_ppk');
Route::get('/vo_ppk_result', 'ReportViewerController@vo_ppk_result')->name('vo_ppk_result');
Route::get('/vo_ppjhk', 'ReportViewerController@vo_ppjhk')->name('vo_ppjhk');


