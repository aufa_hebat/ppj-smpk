<?php

Route::get('/download/{approval}/{acquisition}/{type}/{file}', 'DownloadController@downloadFile')
    ->name('download.approval.acquisition')
    ->middleware('auth');

Route::get('/download/{approval}/{acquisition}/{module}/{no}/{type}/{file}', 'DownloadController@downloadVoFile')
    ->name('download.approval.acquisition')
    ->middleware('auth');    

Route::get('/download/{approval}/{acquisition}/{module}/{no}/{file}', 'DownloadController@downloadIpcFile')
    ->name('download.approval.acquisition')
    ->middleware('auth');  

Route::get('/downloadx/acquisition_activity_sample', 'DownloadController@acquisitionActivitySample')
    ->name('download.approval.acquisition')
    ->middleware('auth');

Route::get('/downloadx/acquisition_progress_sample', 'DownloadController@acquisitionProgressSample')
    ->name('download.approval.acquisition')
    ->middleware('auth');

//Add normal download storage/15/ipc_saps.sql
Route::get('/storage/{folder}/{file}', 'DownloadController@downloadStorage')
    ->name('download.approval.acquisition')
    ->middleware('auth');