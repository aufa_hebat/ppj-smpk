<?php

Route::group([
    'namespace'  => 'Contract\Acquisition\Document',
    'prefix'     => 'doc',
    'as'         => 'doc.',
    'middleware' => ['auth'],
], function () {
    Route::resource('addenda', 'AddendaController');
});

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/addenda', 'ReportViewerController@addenda')->name('addenda');
    Route::get('/addenda_attach', 'ReportViewerController@addenda_attach')->name('addenda_attach');
});
