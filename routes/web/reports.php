<?php

Route::resource('/reports', 'ReportController')->middleware('auth');

Route::middleware('auth')
    ->namespace('Report')
    ->as('report.')
    ->prefix('report')
    ->group(function () {
        Route::namespace('Acquisition')
            ->as('acq.')
            ->prefix('acq')
            ->group(function () {
                Route::get('bq', 'BillOfQuantityController')->name('bq');
                Route::get('eot', 'EotController')->name('schedule');
                Route::get('notice', 'NoticeController')->name('notice');
                Route::get('schedule', 'ScheduleController')->name('schedule');
            });

        Route::namespace('IPC')
            ->as('ipc.')
            ->prefix('ipc')
            ->group(function () {
                Route::get('cert', 'CertController')->name('cert');
                Route::get('summary', 'SummaryController')->name('summary');
                Route::get('status', 'StatusController')->name('status');
            });

        Route::namespace('Certificate')
            ->as('certificate.')
            ->prefix('certificate')
            ->group(function () {
                Route::get('cmgd', 'CmgdController')->name('cmgd');
                Route::get('cnc', 'CncController')->name('cnc');
                Route::get('cpc', 'CpcController')->name('cpc');
                Route::get('cpo', 'CpoController')->name('cpo');
            });

        Route::namespace('Sale')
            ->as('sale.')
            ->prefix('sale')
            ->group(function () {
                Route::get('form', 'FormController')->name('form');
                Route::get('list', 'ListController')->name('list');
                Route::get('folder', 'FolderController')->name('folder');
                Route::get('folder-verify', 'FolderVerifyController')->name('folder-verify');
            });

        Route::namespace('VO')
            ->as('vo.')
            ->prefix('vo')
            ->group(function () {
                Route::get('kkk', 'KKKController')->name('kkk');
            });
            
        Route::namespace('Monitoring')
            ->as('monitoring.')
            ->prefix('monitoring')
            ->group(function () {
                Route::get('site-visit-per-hashslug', 'SiteVisitPerHashslugController')->name('site-visit-per-hashslug');
            });    

        Route::get('acceptance-letter', 'AcceptanceLetterController')->name('acceptance-letter');
        Route::get('brief-attendance', 'BriefAttendanceController')->name('brief-attendance');
    });
