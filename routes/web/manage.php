<?php

Route::group([
    'namespace'  => 'Manage',
    'prefix'     => 'manage',
    'as'         => 'manage.',
    'middleware' => ['auth'],
], function () {
    Route::resource('users', 'UserController')
        ->middleware('permission:urus-pengguna_index')
        ->except('store', 'destroy');
    Route::resource('companies', 'CompanyController')
        ->middleware('permission:urus-syarikat_index');
    Route::resource('owners', 'OwnerController')
        ->middleware('permission:urus-pemilik_index');
    Route::resource('certificates', 'CertificateController')
        ->middleware('permission:urus-sijil_index');
    Route::resource('acl', 'AclController')
        ->middleware('permission:acl_index')
        ->except('store', 'update', 'destroy');
    Route::resource('utility', 'UtilityController')
        ->except('store', 'update', 'destroy');
    Route::delete('/companies/delete/{id}', 'CompanyController@delete')->name('companies.delete');
    Route::delete('/certificates/delete_ssm/{id}', 'CertificateController@delete_ssm')->name('certificates.delete_ssm');
    Route::delete('/certificates/delete_cidb/{id}', 'CertificateController@delete_cidb')->name('certificates.delete_cidb');
    Route::delete('/certificates/delete_bpku/{id}', 'CertificateController@delete_bpku')->name('certificates.delete_bpku');
    Route::delete('/certificates/delete_spkk/{id}', 'CertificateController@delete_spkk')->name('certificates.delete_spkk');
    Route::delete('/certificates/delete_mof/{id}', 'CertificateController@delete_mof')->name('certificates.delete_mof');
    Route::delete('/certificates/delete_bumi/{id}', 'CertificateController@delete_bumi')->name('certificates.delete_bumi');
    Route::delete('/certificates/delete_kdn/{id}', 'CertificateController@delete_kdn')->name('certificates.delete_kdn');
    Route::resource('document_acceptances', 'DocumentAcceptanceController');
    Route::get('/document_acceptances/{document_acceptances}/list', 'DocumentAcceptanceController@list')->name('document_acceptances.list');
    Route::get('/document_acceptances/{document_acceptances}/shows', 'DocumentAcceptanceController@shows')->name('document_acceptances.shows');
    Route::get('/document_acceptances/{document_acceptances}/edits', 'DocumentAcceptanceController@edits')->name('document_acceptances.edits');

    Route::group([
        'namespace' => 'Committee',
        'prefix'    => 'committee',
        'as'        => 'committee.',
    ], function () {
        Route::resource('assessments', 'AssessmentController');
        Route::resource('approvals', 'ApprovalController');
    });
});
