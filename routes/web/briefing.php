<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::resource('/briefing', 'BriefingController');
    Route::get('/briefing/{briefing}/listing', 'BriefingController@listing')->name('briefing.listing');
    Route::get('/brief_attendance', 'ReportViewerController@brief_attendance')->name('brief_attendance');
});
