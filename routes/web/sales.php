<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::resource('/sale', 'SaleController');
    Route::post('/sale/search', 'SaleController@search')->name('sale.search');
    Route::post('/sale/wakil', 'SaleController@wakil')->name('sale.wakil');
    Route::get('/sale/{sale}/receipt', 'SaleController@receipt')->name('sale.receipt');
    Route::get('/sale_form', 'ReportViewerController@sale_form')->name('sale_form');
    Route::get('/sale_list', 'ReportViewerController@sale_list')->name('sale_list');
    Route::get('/sale_folder', 'ReportViewerController@sale_folder')->name('sale_folder');
    Route::get('/sale_folder_verify', 'ReportViewerController@sale_folder_verify')->name('sale_folder_verify');
});
