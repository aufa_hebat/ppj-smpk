<?php

Route::group([
    'namespace'  => 'Contract\Post',
    'prefix'     => 'extension',
    'as'         => 'extension.',
    'middleware' => ['auth'],
], function () {
    Route::resource('extension', 'ExtensionController')->middleware('permission:perlanjutan-kontrak_index');
    Route::resource('extension_edit', 'ExtensionEditController')->middleware('permission:perlanjutan-kontrak_edit');
    Route::resource('extension_show', 'ExtensionShowController')->middleware('permission:perlanjutan-kontrak_show');
    Route::resource('extension_approve', 'ExtensionApproveController')->middleware('permission:perlanjutan-kontrak_index');
});
Route::get('/extension_report', 'ReportViewerController@extension')
    ->name('extension_report')
    ->middleware('auth')->middleware('permission:perlanjutan-kontrak_index');

Route::get('/extension_report_work', 'ReportViewerController@extension_work')
    ->name('extension_report_work')
    ->middleware('auth')->middleware('permission:perlanjutan-kontrak_index');

Route::get('/extension_letter_report_work', 'ReportViewerController@extension_letter_work')
    ->name('extension_letter_report_work')
    ->middleware('auth')->middleware('permission:perlanjutan-kontrak_index');

Route::get('/extension_letter_report_bekalan_perkhidmatan', 'ReportViewerController@extension_letter_bekalan_perkhidmatan')
    ->name('extension_letter_report_bekalan_perkhidmatan')
    ->middleware('auth')->middleware('permission:perlanjutan-kontrak_index');
