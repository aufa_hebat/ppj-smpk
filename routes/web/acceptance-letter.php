<?php

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::resource('/acceptance-letter', 'AcceptanceLetterController');
    Route::get('/acceptance_letter_report', 'ReportViewerController@acceptance_letter')->name('acceptance_letter_report');
});
