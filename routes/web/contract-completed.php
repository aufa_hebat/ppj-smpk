<?php


Route::group([
    'namespace'  => 'Contract\Post',
    'prefix'     => 'post',
    'as'         => 'contract.post.',
    'middleware' => ['auth'],
], function () {
    Route::resource('contract-completed', 'ContractCompletedController');
    Route::get('showPerolehan/{id}', 'ContractCompletedController@showPerolehan')->name('contract-completed.showPerolehan');
});

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/sofaRpt', 'ReportViewerController@sofaRpt')->name('sofaRpt')->middleware('auth');
});
