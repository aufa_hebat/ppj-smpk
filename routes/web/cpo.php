<?php

Route::group([
    'namespace' => 'Contract\Post',
    'prefix'    => 'post',
    'as'        => 'contract.post.',
], function () {
    Route::resource('cpo', 'CpoController');
});

//Route::get('cncCertificate', 'ReportViewerController@cncCertificate')->name('cncCertificate');
