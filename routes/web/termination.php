<?php

Route::group([
    'namespace'  => 'Contract\Post',
    'prefix'     => 'termination',
    'as'         => 'termination.',
    'middleware' => ['auth'],
], function () {
    Route::resource('warning', 'WarningController');
    Route::resource('termination', 'TerminationController');
    Route::post('/termination/reasonTerminate', 'TerminationController@reasonTerminate')->name('reasonTerminate');
    Route::post('/termination/noticeTerminate', 'TerminationController@noticeTerminate')->name('noticeTerminate');
    Route::post('/termination/others', 'TerminationController@othersTerminate')->name('others');
    Route::post('/termination/upload-reason/{id}', 'TerminationController@uploadReason')->name('upload-reason');
    Route::post('/termination/upload-termination/{id}', 'TerminationController@uploadTermination')->name('upload-termination');
});

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('/warning', 'ReportViewerController@warning')->name('warning')->middleware('auth');
    Route::get('/terminate_reason', 'ReportViewerController@terminateReason')->name('terminate_reason')->middleware('auth');
    Route::get('/terminate_notice', 'ReportViewerController@terminateNotice')->name('terminate_notice')->middleware('auth');
});
