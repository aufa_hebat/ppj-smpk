<?php

Route::group([
    'namespace' => 'Contract\Post',
    'prefix'    => 'post',
    'as'        => 'contract.post.',
], function () {
    Route::resource('cnc', 'CncController');
});

Route::get('cncCertificate', 'ReportViewerController@cncCertificate')->name('cncCertificate');
