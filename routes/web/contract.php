<?php


Route::group([
    'namespace'  => 'Contract',
    'prefix'     => 'contract',
    'as'         => 'contract.',
    'middleware' => ['auth'],
], function () {
    Route::resource('pre', 'PreController');
    Route::resource('post', 'PostController');
});
