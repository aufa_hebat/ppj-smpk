<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group([
    'namespace' => 'Auth',
    'prefix'    => 'auth',
    'as'        => 'auth.',
], function () {
    Route::post('login', 'LoginController')->name('login');
    Route::post('logout', 'LogoutController')->name('logout');
    Route::post('register', 'RegisterController')->name('register');
    Route::post('forgot/password', 'ForgotPasswordController')->name('forgot.password');
});
