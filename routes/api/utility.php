<?php

Route::group([
    'namespace' => 'Utility',
    'prefix'    => 'utility',
    'as'        => 'utility.',
], function () {
    Route::resource('authorities', 'AuthoritiesController')->except('create', 'edit', 'show');
    Route::resource('bank', 'BankController')->except('create', 'edit', 'show');
    Route::resource('bon_type', 'BonTypeController')->except('create', 'edit', 'show');
    Route::resource('budget_code', 'BudgetCodeController')->except('create', 'edit', 'show');
    Route::resource('state', 'StateController')->except('create', 'edit', 'show');
    Route::resource('department', 'DepartmentController')->except('create', 'edit', 'show');
    Route::resource('division', 'DivisionController')->except('create', 'edit', 'show');
    Route::resource('section', 'SectionController')->except('create', 'edit', 'show');
    Route::resource('position', 'PositionController')->except('create', 'edit', 'show');
    Route::resource('scheme', 'SchemeController')->except('create', 'edit', 'show');
    Route::resource('grade', 'GradeController')->except('create', 'edit', 'show');
    Route::resource('role', 'RoleController')->except('create', 'edit', 'show');
    Route::resource('location', 'LocationController')->except('create', 'edit', 'show');
    Route::resource('period_type', 'PeriodTypeController')->except('create', 'edit', 'show');
    Route::resource('liability_period', 'LiabilityPeriodController')->except('create', 'edit', 'show');
    Route::resource('insurance', 'InsuranceController')->except('create', 'edit', 'show');
    Route::resource('honorary', 'HonoraryController')->except('create', 'edit', 'show');
    Route::resource('acquisition_type', 'AcquisitionTypeController')->except('create', 'edit', 'show');
    Route::resource('acquisition_category', 'AcquisitionCategoryController')->except('create', 'edit', 'show');
    Route::resource('allocation_resource', 'AllocationResourceController')->except('create', 'edit', 'show');
    Route::resource('termination_type', 'TerminationTypeController')->except('create', 'edit', 'show');
    Route::resource('vo_type', 'VariationOrderTypeController')->except('create', 'edit', 'show');
    Route::resource('cidb_grade', 'CIDBGradeController')->except('create', 'edit', 'show');
    Route::resource('cidb_category', 'CIDBCategoryController')->except('create', 'edit', 'show');
    Route::resource('cidb_khusus', 'CIDBKhususController')->except('create', 'edit', 'show');
    Route::resource('mof_area', 'MOFAreaController')->except('create', 'edit', 'show');
    Route::resource('mof_khusus', 'MOFKhususController')->except('create', 'edit', 'show');
    Route::resource('voc', 'VocController')->except('create', 'edit', 'show');
});
