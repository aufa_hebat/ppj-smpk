<?php


Route::namespace('Report')
    ->as('report.')
    ->prefix('report')
    ->group(function () {
        Route::post('sync', 'SyncController')->name('sync');
    });
