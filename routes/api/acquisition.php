<?php

Route::group([
    'namespace' => 'Contract\Acquisition',
    'prefix'    => 'contract/acquisition',
    'as'        => 'contract.acquisition.',
], function () {
    Route::resource('activity', 'ActivityController')->except('create', 'edit', 'show');
    Route::resource('briefing', 'BriefingController')->except('create', 'edit', 'show');
    Route::resource('bon', 'BonController')->except('create', 'edit', 'show');
    Route::resource('date-time', 'DatetimesController')->except('create', 'edit', 'show');
    Route::resource('semakdate', 'SemakanDateController')->except('create', 'edit', 'show');
    Route::resource('details', 'DetailController')->except('create', 'edit', 'show');
    Route::resource('document', 'DocumentController')->except('create', 'edit', 'show');
    Route::resource('experience', 'ExperienceController')->except('create', 'edit', 'show');
    Route::resource('insurance', 'InsuranceController')->except('create', 'edit', 'show');
    Route::resource('location', 'LocationController')->except('create', 'edit', 'show');
    Route::resource('price', 'PriceController')->except('create', 'edit', 'show');
    Route::resource('quantity', 'QuantityController')->except('create', 'edit', 'show');
    Route::resource('visit', 'VisitController')->except('create', 'edit', 'show');
    Route::resource('deposit', 'DepositController')->except('create', 'edit', 'show');
    Route::resource('contract-document', 'ContractDocumentController')->except('create', 'edit', 'show');
    Route::resource('cancel', 'CancelController')->except('create', 'edit', 'show');
    Route::resource('cpc', 'CpcController')->except('create', 'edit', 'show');
    Route::resource('cnc', 'CncController')->except('create', 'edit', 'show');
    Route::resource('cmgd', 'CmgdController')->except('create', 'edit', 'show');
    Route::resource('sub_contract', 'SubContractController')->except('create', 'edit', 'show');
    Route::resource('cpo', 'CpoController')->except('create', 'edit', 'show');
    Route::resource('sofa', 'SofaController')->except('create', 'edit', 'show');
    Route::resource('adjustment', 'AdjustmentController')->except('create', 'edit', 'show');  

    Route::post('bq-paging/{id}', 'QuantityController@bqPaging')->name('bq-paging'); 
    Route::post('updateacqcancel/{id}', 'CancelController@updateAcqCancel')->name('update-acq-cancel'); 
});
