<?php

Route::group([
    'namespace' => 'Contract',
    'prefix'    => 'contract',
    'as'        => 'contract.',
], function () {
    Route::resource('pre', 'PreController')->except('create', 'edit', 'show');
    Route::resource('briefing', 'BriefingController')->except('create', 'edit', 'show');
    Route::resource('site-visit', 'SiteVisitController')->except('create', 'edit', 'show');
    Route::delete('/site-visit/delete/{id}', 'SiteVisitController@delete')->name('site-visit.delete');
    Route::post('upload-site/{id}', 'SiteVisitController@upload')->name('upload-site');
    Route::resource('acceptance-letter', 'AcceptanceLetterController')->except('create', 'edit', 'show');
    Route::post('upload-sst/{id}', 'AcceptanceLetterController@upload')->name('upload-sst');
    Route::resource('extension', 'ExtensionController')->except('create', 'edit', 'show');
    Route::post('upload-eot/{id}', 'ExtensionController@upload')->name('upload-eot');
    Route::delete('/extension/delete/{id}', 'ExtensionController@delete')->name('extension.delete');
    Route::resource('extension_approve', 'ExtensionApproveController')->except('create', 'edit', 'show');
    Route::resource('extension_bon', 'ExtensionBonController')->except('create', 'edit', 'show');
    Route::resource('extension_insurance', 'ExtensionInsuranceController')->except('create', 'edit', 'show');
    Route::resource('warning', 'WarningController')->except('create', 'edit', 'show');
    Route::post('upload-warn/{id}', 'WarningController@upload')->name('upload-warn');
    
    Route::resource('variation-order', 'VariationOrderController')->except('create', 'edit', 'show');
    Route::get('/variation-order/{vo}/sync', 'VariationOrderController@sync')->name('variation-order.sync');
    Route::resource('variation-order-vo', 'VariationOrderVOController')->except('create', 'edit', 'show');
    Route::resource('variation-order-wps', 'VariationOrderWPSController')->except('create', 'edit', 'show');
    Route::resource('variation-order-active-wps', 'VariationOrderActiveWPSController')->except('create', 'edit', 'show');    
    Route::resource('variation-order-cancel-wps', 'VariationOrderCancelWPSController')->except('create', 'edit', 'show');    
    Route::resource('variation-order-pq', 'VariationOrderPQController')->except('create', 'edit', 'show');
    Route::resource('variation-order-kkk', 'VariationOrderKKKController')->except('create', 'edit', 'show');
    Route::post('create-vo-element/{id}', 'VariationOrderVOController@createVoElement')->name('create-vo-element');
    Route::post('create-vo-item/{id}', 'VariationOrderVOController@createVoItem')->name('create-vo-item');
    Route::post('create-vo-subitem/{id}', 'VariationOrderVOController@createVoSubItem')->name('create-vo-subitem');
    Route::post('edit-vo-element/{id}', 'VariationOrderVOController@editVoElement')->name('edit-vo-element');
    Route::post('edit-vo-item/{id}', 'VariationOrderVOController@editVoItem')->name('edit-vo-item');
    Route::post('edit-vo-subitem/{id}', 'VariationOrderVOController@editVoSubItem')->name('edit-vo-subitem');
    Route::get('/delete-vo-subitem/{id}', 'VariationOrderVOController@deleteVoSubItem')->name('delete-vo-subitem');
    Route::get('/delete-vo-item/{id}', 'VariationOrderVOController@deleteVoItem')->name('delete-vo-item');
    Route::get('/delete-vo-element/{id}', 'VariationOrderVOController@deleteVoElement')->name('delete-vo-element');

    Route::post('create-vo-wps-subitem/{id}', 'VariationOrderVOController@createVoWpsSubItem')->name('create-vo-wps-subitem');
    Route::post('submit-wps/{id}', 'VariationOrderVOController@submitWps')->name('submit-wps');

    Route::post('create-vo-pk-subitem/{id}', 'VariationOrderVOController@createVoPkSubItem')->name('create-vo-pk-subitem');

    Route::get('vo-select-wps/{id}', 'VariationOrderWPSController@editSelectWps')->name('vo-select-wps');
    Route::post('active-wps-result/{id}', 'VariationOrderActiveWPSController@result')->name('active-wps-result');    
    Route::post('ppk-approver/{id}', 'VariationOrderController@ppkApprover')->name('ppk-approver');  
    Route::put('ppk-meeting/{id}', 'VariationOrderController@ppkMeeting')->name('ppk-meeting');  

    Route::post('upload-ppk/{id}', 'VariationOrderController@uploadPpk')->name('upload-ppk');
    Route::post('upload-ppk-result/{id}', 'VariationOrderController@uploadPpkResult')->name('upload-ppk-result');
    
    Route::resource('variation-order-ppjhk', 'VariationOrderPpjhkController')->except('create', 'edit', 'show');
    Route::put('/variation-order-ppjhk/{vo}/amend', 'VariationOrderPpjhkController@amend')->name('variation-order-ppjhk.amend');

    Route::post('upload-ppjhk/{id}', 'VariationOrderPpjhkController@uploadPpjhk')->name('upload-ppjhk');
});
