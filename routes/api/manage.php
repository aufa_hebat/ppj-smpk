<?php

/*
 * @todo protect routes, allow for self-consume API
 */
Route::group([
    'namespace' => 'Manage',
    'prefix'    => 'manage',
    'as'        => 'manage.',
], function () {
    Route::resource('users', 'UserController')->except('create', 'edit', 'show');
    Route::get('finduserbydepartment/{id}', 'UserController@finduserbydepartment')->name('finduserbydepartment');
    Route::get('finduserbyid/{id}', 'UserController@finduserbyid')->name('finduserbyid');
    Route::resource('companies', 'CompanyController');
    Route::resource('owners', 'OwnerController');
    Route::resource('certificates', 'CertificateController');
    Route::get('acl', 'AclController@index')->name('acl.index');
    Route::put('acl', 'AclController@update')->name('acl.update');
    Route::resource('document_acceptances', 'DocumentAcceptanceController');
    Route::get('/document_acceptances/{document_acceptances}/list', 'DocumentAcceptanceController@list')->name('document_acceptances.list');
    Route::get('/document_acceptances/{document_acceptances}/shows', 'DocumentAcceptanceController@shows')->name('document_acceptances.shows');
    Route::get('/document_acceptances/{document_acceptances}/edits', 'DocumentAcceptanceController@edits')->name('document_acceptances.edits');
});
