<?php


Route::group([
    'namespace' => 'Contract\Acquisition',
    'prefix'    => 'contract/acquisition',
    'as'        => 'contract.acquisition.',
], function () {
    Route::resource('monitoring', 'ActivityController')->except('create', 'edit', 'show');

    Route::group([
        'namespace' => 'Monitoring',
        'prefix'    => 'monitor',
        'as'        => 'monitor.',
    ], function () {
        Route::resource('site-visit', 'SiteVisitController')->except('create', 'edit', 'show');
        Route::resource('payment-monitor', 'PaymentMonitorController')->except('create', 'edit', 'show');
        Route::resource('contract-progress', 'ContractProgressController')->except('create', 'edit', 'show');
        Route::get('contractprogress/{id}', 'ContractProgressController@show')->name('showcontractprogress');
        Route::get('site-visit/{id}', 'SiteVisitController@show')->name('showsitevisit');
    });
});
