<?php

Route::group([
    'namespace' => 'Contract\Ipc',
    'prefix'    => 'contract/ipc',
    'as'        => 'contract.ipc.',
], function () {
    Route::resource('sijil', 'SijilController')->except('create', 'edit', 'show');
    Route::resource('invoice', 'InvoiceController')->except('create', 'edit', 'show');
    Route::delete('/sijil/delete_attach/{id}', 'SijilController@delete_attach')->name('sijil.delete_attach');
});
