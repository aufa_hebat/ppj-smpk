<?php

Route::group([
    'namespace' => 'Contract\Acquisition',
    'prefix'    => 'contract/acquisition',
    'as'        => 'contract.acquisition.',
], function () {
    Route::resource('approval', 'Approval\ApprovalController')->except('create', 'edit', 'show');
    Route::group([
        'namespace' => 'Approval',
        'prefix'    => 'approval',
        'as'        => 'approval.',
    ], function () {
        Route::resource('details', 'DetailController')->except('create', 'edit', 'show');
        Route::resource('qualifications', 'QualificationController')->except('create', 'edit', 'show');
        Route::resource('finance', 'FinanceController')->except('create', 'edit', 'show');
        Route::resource('budget-codes', 'BudgetCodeController')->except('create', 'edit', 'show');
        Route::post('upload-documents', 'UploadDocumentController@upload')->name('upload-documents');
        Route::delete('upload-documents/{id}/{doc}/destroy', 'UploadDocumentController@destroy')->name('destroy-documents');
    });
});
