<?php


$ssts = \App\Models\Acquisition\Sst::all()->reject(function ($sst) {
    return $sst->is_more_than_fifty_percent_paid;
});

$exceeds = \App\Models\Acquisition\Activity::whereIn('sst_id', $ssts->pluck('id'))
    ->where('actual_completion', '>', '50')
    ->get();
