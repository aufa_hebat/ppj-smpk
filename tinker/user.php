<?php

 departments()->where('code', '!=', \App\Models\Department::ALL)->each(function ($department) {
     sections()->where('category', $department->code)->each(function ($section) use ($department) {
         sections()->where('code', $section->code)->each(function ($sec) use ($department, $section) {
             $user = factory(\App\Models\User::class)->create();
             $user->position_id = positions()->random()->id;
             $user->department_id = $department->id;
             $user->section_id = $sec->id;
             $user->scheme_id = schemes()->random()->id;
             $user->grade_id = grades()->random()->id;
             $user->save();
         });
     });
 });
