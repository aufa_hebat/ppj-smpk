<?php


$schema           = \DB::connection(config('database.default'))->getDoctrineConnection()->getSchemaManager();
$taskable_columns = [];
// we don't need to those already not null, since it's handle
// by the validation. so we just need to check those nullable
// fields, and need to determine, which fields is mandatory
// to mark task as done.
$columns = collect($schema->listTableColumns('acquisitions'))
    ->reject(function ($column) {
        return $column->getNotNull();
    })
    ->reject(function ($column) {
        return in_array($column->getName(), ['id', 'hashslug', 'deleted_at', 'created_at', 'updated_at']);
    });
$columns = collect($columns->keys()->toArray());

// if taskable columns is set, concat columns defined
// by the user to validate the fields either it's empty or not
if (sizeof($taskable_columns) > 0) {
    $columns = $columns->concat($taskable_columns);
}

$columns->toArray();
