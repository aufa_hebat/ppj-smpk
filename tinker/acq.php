<?php


$s = \App\Models\Acquisition\Sst::contractsSixMonthToEnd()->get()->groupBy('department_id')->mapWithKeys(function ($acq) {
    return [$acq->first()->department->name => $acq->count()];
});
