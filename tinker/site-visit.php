<?php


use App\Models\Acquisition\Monitoring\SiteVisit;

$hashslug = '2vybXEOaYlAg3kko8nLe38m7jqDN610n';

$query = SiteVisit::whereHas('sst', function ($query) use ($hashslug) {
    $query->where('hashslug', $hashslug);
})->get();
