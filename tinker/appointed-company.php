<?php

$a = \App\Models\Acquisition\AppointedCompany::withDetails()
    ->get()
    ->groupBy('acquisition.department_id')
    ->mapWithKeys(function ($acq) {
        return [$acq->first()->acquisition->department->name => $acq->count()];
    });
