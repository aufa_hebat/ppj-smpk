<?php

namespace Tests\Unit;

use Tests\TestCase;

/**
 * @todo test has helper,
 * @todo test expected result if key exist,
 * @todo test expected result if key not exist.
 */
class MediaCollectionHelperTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('config:clear');
    }

    /** @test */
    public function it_has_media_collection_helper()
    {
        $this->assertTrue(function_exists('media_collection_name'));
    }

    /** @test */
    public function it_can_generate_correct_key_if_key_exist()
    {
        $expected = 'all-media-123';
        $actual   = media_collection_name('default', 123);
        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function it_can_generate_correct_key_if_key_not_exist()
    {
        $expected = 'default-123';
        $actual   = media_collection_name('key.not.exist', 123);
        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function it_has_media_collection_config_file()
    {
        $this->assertTrue(file_exists(config_path('media-collections.php')));
    }

    /** @test */
    public function it_has_default_key()
    {
        $this->assertTrue(isset(config('media-collections')['default']));
        $this->assertNotNull(config('media-collections.default'));
        $this->assertTrue(('all-media' == config('media-collections.default')));
    }
}
