<?php

namespace Tests\Unit;

use Tests\TestCase;

class HelperTest extends TestCase
{
    /** @test */
    public function it_has_generate_sequence_helper()
    {
        $this->assertTrue(function_exists('generate_sequence'));
    }

    /** @test */
    public function it_has_abbrv_helper()
    {
        $this->assertTrue(function_exists('abbrv'));
    }

    /** @test */
    public function it_has_generate_reference_helper()
    {
        $this->assertTrue(function_exists('generate_reference'));
    }

    /** @test */
    public function it_has_str_slug_fqcn_helper()
    {
        $this->assertTrue(function_exists('str_slug_fqcn'));
    }

    /** @test */
    public function it_has_hashids_helper()
    {
        $this->assertTrue(function_exists('hashids'));
    }

    /** @test */
    public function it_has_user_helper()
    {
        $this->assertTrue(function_exists('user'));
    }
}
