<?php

namespace Tests\Unit;

use Tests\TestCase;

class SapServiceTest extends TestCase
{
    /** @test */
    public function it_has_sap_service_class()
    {
        $this->assertTrue(class_exists(\App\Services\SapService::class));
    }

    /** @test */
    public function it_has_sap_service_helper()
    {
        $this->assertTrue(function_exists('sap'));
    }
}
