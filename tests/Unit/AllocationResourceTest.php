<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\ReferenceTableTrait;

class AllocationResourceTest extends TestCase
{
    use RefreshDatabase, ReferenceTableTrait;

    public $model    = \App\Models\Acquisition\AllocationResource::class;
    public $table    = 'allocation_resources';
    public $helper   = 'allocation_resources';
    public $seeder   = 'AllocationResourceTableSeeder';
    public $seed_key = 'name';
    public $seeds    = [
        'Perbadanan Putrajaya',
        'Kementerian Wilayah Persekutuan',
        'Lain-Lain',
    ];
}
