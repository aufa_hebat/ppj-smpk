<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\ReferenceTableTrait;

class LocationTest extends TestCase
{
    use RefreshDatabase, ReferenceTableTrait;

    public $model    = \App\Models\Location::class;
    public $table    = 'locations';
    public $helper   = 'locations';
    public $seeder   = 'LocationSeeder';
    public $seed_key = 'name';
    public $seeds    = [
        'Presint 1',
        'Presint 2',
        'Presint 3',
        'Presint 4',
        'Presint 5',
        'Presint 6',
        'Presint 7',
        'Presint 8',
        'Presint 9',
        'Presint 10',
        'Presint 11',
        'Presint 12',
        'Presint 13',
        'Presint 14',
        'Presint 15',
        'Presint 16',
        'Presint 17',
        'Presint 18',
        'Presint 19',
        'Presint 20',
        'Keseluruhan Putrajaya',
    ];
}
