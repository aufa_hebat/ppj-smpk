<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\ReferenceTableTrait;

class BudgetCodeTest extends TestCase
{
    use RefreshDatabase, ReferenceTableTrait;

    public $model    = \App\Models\BudgetCode::class;
    public $table    = 'budget_codes';
    public $helper   = 'budget_code';
    public $seeder   = 'BudgetCodeSeeder';
    public $seed_key = 'category';
    public $seeds    = [
        'Business Area',
        'Cost Centre - Fun Centre',
        'Functional Area',
        'Fund',
        'Funded Programme',
        'GL Account',
    ];
}
