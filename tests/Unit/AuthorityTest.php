<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\ReferenceTableTrait;

class AuthorityTest extends TestCase
{
    use RefreshDatabase, ReferenceTableTrait;

    public $model    = \App\Models\Acquisition\Approval\Authority::class;
    public $table    = 'authorities';
    public $helper   = 'authorities';
    public $seeder   = 'AuthoritySeeder';
    public $seed_key = 'name';
    public $seeds    = [
        'Naib Presiden Jabatan Undang-Undang',
        'Naib Presiden Jabatan Lanskap dan Taman',
        'Naib Presiden Jabatan Perkhidmatan Korporat',
        'Naib Presiden Jabatan Perancangan Bandar',
        'Naib Presiden Jabatan Audit dan dan Kualiti Ansurans',
        'Naib Presiden Jabatan Perkhidmatan Bandar',
        'Naib Presiden Jabatan Kejuruteraan & Penyelengaraan',
        'Naib Presiden Jabatan Kewangan',
        'Presiden Perbadanan Putrajaya',
        'Kementerian Kewangan Malaysia',
        'Kementerian Wilayah Persekutuan',
    ];
}
