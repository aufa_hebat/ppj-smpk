<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\ReferenceTableTrait;

class CIDBCodeTest extends TestCase
{
    use RefreshDatabase, ReferenceTableTrait;

    public $model    = \App\Models\CIDBCode::class;
    public $table    = 'cidb_codes';
    public $helper   = 'cidb_codes';
    public $seeder   = 'CIDBCodeSeeder';
    public $seed_key = 'type';
    public $seeds    = [
        'grade',
        'category',
        'khusus',
    ];
}
