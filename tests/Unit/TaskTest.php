<?php

namespace Tests\Unit;

use Tests\TestCase;

class TaskTest extends TestCase
{
    /** @test */
    public function it_has_helper()
    {
        $this->assertTrue(function_exists('task'));
    }

    /** @test */
    public function it_has_service()
    {
        $this->assertTrue(class_exists('\App\Services\TaskService'));
    }
}
