<?php

namespace Tests\Unit;

use Tests\TestCase;

class NotifyTest extends TestCase
{
    /** @test */
    public function it_has_helper()
    {
        $this->assertTrue(function_exists('notify'));
    }

    /** @test */
    public function it_has_service()
    {
        $this->assertTrue(class_exists('\App\Services\NotificationService'));
    }
}
