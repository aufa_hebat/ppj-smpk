<?php

namespace Tests\Unit;

use Tests\TestCase;

class DashboardServiceTest extends TestCase
{
    /** @test */
    public function it_has_dashboard_service_class()
    {
        $this->assertTrue(class_exists(\App\Services\DashboardService::class));
    }

    /** @test */
    public function it_has_dashboard_service_class_helper()
    {
        $this->assertTrue(function_exists('dashboard'));
    }
}
