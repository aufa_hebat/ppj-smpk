<?php

namespace Tests\Unit;

use Tests\TestCase;

class MoneyTest extends TestCase
{
    /** @test */
    public function it_has_money_utility()
    {
        $this->assertTrue(class_exists(\App\Utilities\Money::class));
    }

    /** @test */
    public function it_has_money_helper()
    {
        $this->assertTrue(function_exists('money'));
    }

    /** @test */
    public function it_can_convert_to_common_format_with_thousand_separator_from_machine_format()
    {
        $expected = '1,000.23';
        $value    = 100023;
        $given    = money()->toCommon($value, true);
        $this->assertEquals($expected, $given);
    }

    /** @test */
    public function it_can_convert_to_common_format_from_machine_format()
    {
        $expected = '1000.23';
        $value    = 100023;
        $given    = money()->toCommon($value);
        $this->assertEquals($expected, $given);
    }

    /** @test */
    public function it_can_convert_to_human_format_from_machine_format()
    {
        $expected = 'RM 1,000.23';
        $value    = 100023;
        $given    = money()->toHuman($value);
        $this->assertEquals($expected, $given);
    }

    /** @test */
    public function it_can_convert_to_human_format_with_thousand_separator_from_machine_format()
    {
        $expected = 'RM 1,000.23';
        $value    = 100023;
        $given    = money()->toHuman($value, true);
        $this->assertEquals($expected, $given);
    }

    /** @test */
    public function it_can_convert_to_moneyphp_from_common_format()
    {
        $value    = '1,000.23';
        $expected = 100023;
        $given    = money()->toMachine($value);
        $this->assertEquals($expected, $given);
    }

    /** @test */
    public function it_can_convert_to_moneyphp_from_human_format()
    {
        $value    = 'RM 1,000.23';
        $expected = 100023;
        $given    = money()->toMachine($value);
        $this->assertEquals($expected, $given);
    }
}
