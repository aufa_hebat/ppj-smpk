<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoggedInPageTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'DepartmentSeeder']);
        $this->artisan('db:seed', ['--class' => 'PositionTableSeeder']);
        $this->artisan('db:seed', ['--class' => 'SchemeTableSeeder']);
        $this->artisan('db:seed', ['--class' => 'GradeTableSeeder']);
    }

    /** @test */
    public function get_status_code_200()
    {
        $user = factory(\App\Models\User::class)->create();
        $this->actingAs($user);
        $this->assertAuthenticated();
    }
}
