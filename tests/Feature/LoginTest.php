<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function get_status_code_200()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    /** @test */
    public function user_can_login()
    {
        $user     = factory(\App\Models\User::class)->create(['email' => 'developer@ppj.gov.my']);
        $response = $this->post('/login', [
            'email'    => 'developer@ppj.gov.my',
            'password' => 'secret',
        ]);

        $response->assertStatus(302);

        $this->assertAuthenticated();
    }

    /** @test */
    public function user_cannot_login()
    {
        $user = factory(\App\Models\User::class)->create();

        $response = $this->post('/login', [
            'email'    => $user->email,
            'password' => 'wrong-password',
        ]);

        $response->assertSessionHasErrors(['email']);
    }
}
