<?php

namespace Tests\Traits;

use Illuminate\Support\Facades\Schema;

trait ReferenceTableTrait
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:fresh');
        $this->artisan('db:seed', ['--class' => $this->seeder]);
    }

    /** @test */
    public function it_has_model()
    {
        $this->assertTrue(class_exists($this->model));
    }

    /** @test */
    public function it_has_table()
    {
        $this->assertTrue(Schema::hasTable($this->table));
    }

    /** @test */
    public function it_has_helper()
    {
        $this->assertTrue(function_exists($this->helper));
    }

    /** @test */
    public function it_has_seeded()
    {
        $data = $this->seeds;

        foreach ($data as $datum) {
            $this->assertDatabaseHas($this->table, [
                $this->seed_key => $datum,
            ]);
        }
    }
}
