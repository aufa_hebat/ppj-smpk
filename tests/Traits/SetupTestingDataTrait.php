<?php

namespace Tests\Traits;

use Illuminate\Foundation\Testing\DatabaseMigrations;

trait SetupTestingDataTrait
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('config:clear');
        $this->runDatabaseMigrations();
        $this->artisan('db:seed');
        $this->seedDevelopmentSeeder();
    }
}
