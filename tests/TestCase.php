<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function reloadAll()
    {
        $this->artisan('reload:all');
    }

    public function reloadAllWithPreSeedData()
    {
        $this->artisan('reload:all', ['--dev' => true]);
    }

    public function reloadDb()
    {
        $this->artisan('reload:db');
    }

    public function reloadCache()
    {
        $this->artisan('reload:cache');
    }

    public function seedDevelopmentSeeder()
    {
        $this->artisan('db:seed', ['--class' => 'DevelopmentSeeder']);
    }
}
