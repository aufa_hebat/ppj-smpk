<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\UserProvider as UserProviderContract;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
//use Illuminate\Contracts\Hashing\Hasher;

use App\Models\User;


//class CustomUserProvider implements UserProvider {
  class CustomUserProvider extends EloquentUserProvider {
      
//    public function __construct() {
//        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
//    }
      
   public function __construct(HasherContract $hasher, $model)
   {
       
       //$obj = new HasherContract();   
       //parent::__construct();
       parent::__construct($hasher, $model);
       
       //parent::__construct($obj, $model);
    }

   
  //return true if pass validate  
  public function validateCredentials(UserContract $user, array $credentials)
  {    
      //check user by id + @ppj.gov.my
      //find user if no, return false,
      //
      //if have, check with auth ldap
      //
      //add ldap authentication
      //din
       
      $userByEmail = User::where('email',$credentials['email']);
          
      if(is_null($userByEmail)){
          return false;
      }else{
          //TODO - testing can use this login without password and ldap connection, just return true
          //
          return true;
               
          //ldap here
            $user =  strstr($credentials['email'], '@', true); //"username"';
            $password = ''.$credentials['password'];
            $host = 'putrajaya.local';
            $domain = 'putrajaya';
            $basedn = 'DC=putrajaya,DC=local';
            $group = 'OU=System Administration';
            
            if($user == 'administrator' && $password == 'qawsedrf'){
                return true;
            }
            
            
            
            try{
            $ad = ldap_connect("ldap://{$host}");
            //or die('Could not connect to LDAP server.');
            ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
            ldap_bind($ad, "{$user}@{$domain}", $password);
                    //or die('Could not bind to AD.');
            return true;
            }catch (\Exception $e) {
                return false;
            }

          
      }
                    
      return false;
  }
  

}
