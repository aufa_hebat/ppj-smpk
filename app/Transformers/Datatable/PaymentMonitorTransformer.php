<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Monitoring\PaymentMonitor;
use League\Fractal\TransformerAbstract;

class PaymentMonitorTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Monitoring\SiteVisit $site_visit
     *
     * @return array
     */
    public function transform(PaymentMonitor $payment_monitor)
    {
        //$collection_name = $site_visit->getMediaCollectionName();

        return [
            //'hashslug'             => $payment_monitor->hashslug,
            'id'                  => $payment_monitor->id,
            'payment_at'                 => $payment_monitor->payment_at->format(config('datetime.dashboard')),
            'payment_percent' =>    $payment_monitor->payment_percent,
            //'remarks'              => $site_visit->remarks,
            //'attachment'           => $site_visit->getLastMediaUrl($collection_name),
            //'attachment_name'      => str_limit(optional($site_visit->getLastMedia($collection_name))->name, 100),
            //'percentage_completed' => $site_visit->percentage_completed,
            //'visited_by'           => $site_visit->visitedBy->name,
            //'visited_at'           => $site_visit->visited_at->format(config('datetime.dashboard')),
        ];
    }
}
