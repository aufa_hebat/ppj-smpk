<?php

namespace App\Transformers\Datatable;

use App\Models\WorkflowTask;
use League\Fractal\TransformerAbstract;

class WorkflowTaskTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\WorkflowTask $task
     *
     * @return array
     */
    //public function transform(WorkflowTask $task)
    public function transform(WorkflowTask $task)
    {
        return [
            //TODO transform into datatable
            //acquisition_ref => $task->
            'id'        => $task->id,
            'flow_name' => $task->flow_name,
            'url'       => $task->url,
            'flow_desc' => $task->flow_desc,

            //'flow_name' => $task->hashslug,
            //'url'       => $task->hashslug

            /*
            'hashslug'  => $task->hashslug,
            'section'   => $task->workflow->section,
            'name'      => optional($task->taskable)->taskable_name,
            'label'     => optional($task->taskable)->taskable_label,
            // should frontend to do the rendering
            'is_done'   => true == $task->is_done ? '<div class="badge badge-success">Selesai</div>' : '<div class="badge badge-danger">Belum</div>',
            'done_at'   => optional($task->done_at)->format(config('datetime.datetimepicker.date_time')),
            */
        ];
    }
}
