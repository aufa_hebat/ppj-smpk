<?php

namespace App\Transformers\Datatable;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'hashslug'   => $user->hashslug,
            'name'       => $user->name,
            'email'      => $user->email,
            'position'   => $user->position->name,
            'department' => $user->department->name,
            'section'    => $user->section->name,
            'scheme'     => $user->scheme->name,
            'grade'      => $user->grade->name,
            'org_atasan'      => $user->supervisor->name,
        ];
    }
}
