<?php

namespace App\Transformers\Datatable;

use App\Models\Task;
use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Task $task
     *
     * @return array
     */
    public function transform(Task $task)
    {
        return [
            'hashslug' => $task->hashslug,
            'section'  => $task->workflow->section,
            'name'     => optional($task->taskable)->taskable_name,
            'label'    => optional($task->taskable)->taskable_label,
            // should frontend to do the rendering
            'is_done' => true == $task->is_done ? '<div class="badge badge-success">Selesai</div>' : '<div class="badge badge-danger">Belum</div>',
            'done_at' => optional($task->done_at)->format(config('datetime.datetimepicker.date_time')),
        ];
    }
}
