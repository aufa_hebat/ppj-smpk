<?php

namespace App\Transformers\Datatable;

use App\Models\VO\VO;
use League\Fractal\TransformerAbstract;

class VOTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\VariationOrder $vo
     *
     * @return array
     */
    public function transform(VO $vo)
    {
        return [
            'hashslug'  => $vo->hashslug,
            'no'        => $vo->no,
            'title'     => $vo->sst->acquisition->title,
            'reference' => $vo->sst->acquisition->reference,
        ];
    }
}
