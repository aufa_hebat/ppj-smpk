<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Acquisition;
use League\Fractal\TransformerAbstract;

class EvaluationTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Sale $sale
     *
     * @return array
     */
    public function transform(Acquisition $acq)
    {
        return $acq->toArray();
    }
}
