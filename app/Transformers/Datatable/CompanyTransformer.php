<?php

namespace App\Transformers\Datatable;

use App\Models\Company\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Acquisition $acquisition
     *
     * @return array
     */
    public function transform(Company $company)
    {
        // return $company->toArray();
        $status = 'Aktif';
        // if($company->status_id == 0){
        //     $status = 'Aktif';
        // }

        return [
            'hashslug'     => $company->hashslug,
            'company_name' => $company->company_name,
            'email'        => $company->email,
            'status'       => $status,
        ];
    }
}
