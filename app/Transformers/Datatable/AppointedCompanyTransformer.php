<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\AppointedCompany;
use League\Fractal\TransformerAbstract;

class AppointedCompanyTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\AppointedCompany $appointed_company
     *
     * @return array
     */
    public function transform(AppointedCompany $appointed_company)
    {
        $sst_hashslug = "";
        $status = '';

        if(!empty($appointed_company->acquisition->sst)){
            $sst_hashslug = $appointed_company->acquisition->sst->hashslug;

            if($appointed_company->acquisition->sst->status == ''){
                $status = '';
            }else{
                $status = 'none';
            }
        }


        return [
            'hashslug'          => $appointed_company->hashslug,
            'sst_hashslug'      => $sst_hashslug,
            'acquisition_title' => $appointed_company->acquisition->title,
            'company_name'      => $appointed_company->company->company_name,
            'reference'         => $appointed_company->acquisition->reference,
            'type_acq'          => $appointed_company->acquisition->approval->acquisition_type_id,
            'created_at'        => optional($appointed_company->created_at)->format(config('datetime.datetimepicker.date')),
            'display'           => $status,
        ];
    }
}
