<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Sst;
use League\Fractal\TransformerAbstract;

class SstTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\User $user
     *
     * @return array
     */
    public function transform(Sst $sst)
    {
        //return $sst->toArray();

        return [
            'hashslug'          => $sst->hashslug,
            'no_kontrak'        => $sst->acquisition->reference,
            'tajuk'             => $sst->acquisition->title,
            'syarikat_terpilih' => $sst->company->company_name,
            'created_at'        => optional($sst->created_at)->format(config('datetime.datetimepicker.date')),
        ];
    }
}
