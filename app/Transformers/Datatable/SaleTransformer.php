<?php

namespace App\Transformers\Datatable;

use App\Models\Sale;
use League\Fractal\TransformerAbstract;

class SaleTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Sale $sale
     *
     * @return array
     */
    public function transform(Sale $sale)
    {
        return $sale->toArray();
    }
}
