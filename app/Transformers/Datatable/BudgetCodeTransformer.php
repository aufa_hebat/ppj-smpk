<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Approval\Financial;
use League\Fractal\TransformerAbstract;

class BudgetCodeTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Approval\Financial $financial
     *
     * @return array
     */
    public function transform(Financial $financial)
    {
        return [
            'hashslug'            => $financial->hashslug,
            'business_area'       => $financial->business,
            'functional_area'     => $financial->functional,
            'cost_center'         => $financial->cost,
            'fund'                => $financial->fun,
            'funded_programme'    => $financial->funded,
            'gl_account'          => $financial->account,
            'business_area_id'    => $financial->business_area_id,
            'functional_area_id'  => $financial->functional_area_id,
            'cost_center_id'      => $financial->cost_center_id,
            'fund_id'             => $financial->fund_id,
            'funded_programme_id' => $financial->funded_programme_id,
            'gl_account_id'       => $financial->gl_account_id,
        ];
    }
}
