<?php

namespace App\Transformers\Datatable;

use League\Fractal\TransformerAbstract;
use Spatie\MediaLibrary\Media;

class MediaTransformer extends TransformerAbstract
{
    /**
     * @param \Spatie\MediaLibrary\Media $media
     *
     * @return array
     */
    public function transform(Media $media)
    {
        return [
            'hashslug'  => $media->hashslug,
            'file_name' => $media->file_name,
        ];
    }
}
