<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Acquisition;
use League\Fractal\TransformerAbstract;

class BoxTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Sale $sale
     *
     * @return array
     */
    public function transform(Acquisition $acq)
    {
        if (null != $acq->extended_closed_sale_at) {
            $acq->closed_at = $acq->extended_closed_sale_at;
        } else {
            $acq->closed_at = $acq->closed_at;
        }

        return [
            'hashslug'  => $acq->hashslug,
            'reference' => $acq->reference,
            'title'     => $acq->title,
            'closed_at' => optional($acq->closed_at)->format(config('datetime.datetimepicker.date')),
        ];
    }
}
