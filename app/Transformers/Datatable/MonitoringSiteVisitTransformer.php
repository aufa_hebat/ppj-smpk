<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Monitoring\SiteVisit;
use League\Fractal\TransformerAbstract;

class MonitoringSiteVisitTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Monitoring\SiteVisit $site_visit
     *
     * @return array
     */
    public function transform(SiteVisit $site_visit)
    {
        $collection_name = $site_visit->getMediaCollectionName();

        return [
            'hashslug'             => $site_visit->hashslug,
            'name'                 => $site_visit->name,
            'remarks'              => $site_visit->remarks,
            'attachment'           => $site_visit->getLastMediaUrl($collection_name),  
            'attachment_name'      => str_limit(optional($site_visit->getLastMedia($collection_name))->name, 10),
            'percentage_completed' => $site_visit->percentage_completed,
            'visited_by'           => $site_visit->visitedBy->name,
            'visited_at'           =>  (!empty($site_visit->visited_at))? $site_visit->visited_at->format('d/m/Y'):null,
            'visit_date'           => (!empty($site_visit->visit_date))? $site_visit->visit_date->format('d/m/Y'):null ,
            //'visited_at'           => $site_visit->visited_at->format(config('datetime.dashboard')),
            //'visited_at_original'  => $site_visit->visited_at->format(config('datetime.datetimepicker.date')),
           
        ];
    }
}
