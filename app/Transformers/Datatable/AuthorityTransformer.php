<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Approval\Authority;
use League\Fractal\TransformerAbstract;

class AuthorityTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Approval $approval
     *
     * @return array
     */
    public function transform(Authority $authority)
    {
//        return $approval->toArray();
        return [
            'id'         => $authority->id,
            'code'       => $authority->code,
            'name'       => $authority->getActualNameAttributes(),
            'updated_at' => optional($authority->updated_at)->format(config('datetime.datetimepicker.date_time')),
        ];
    }
}
