<?php

namespace App\Transformers\Datatable;

use App\Models\Company\Owner;
use League\Fractal\TransformerAbstract;

class OwnerTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Acquisition $acquisition
     *
     * @return array
     */
    public function transform(Owner $owner)
    {
        return $owner->toArray();
    }
}
