<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Sst;
use League\Fractal\TransformerAbstract;

class IpcTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Sst $sst
     *
     * @return array
     */
    public function transform(Sst $sst)
    {
        return [
            'hashslug'          => $sst->hashslug,
            'no_kontrak'        => $sst->acquisition->reference,
            'tajuk'             => $sst->acquisition->title,
            'syarikat_terpilih' => $sst->company->company_name,
            'tarikh_mula'       => optional($sst->start_working_date)->format(config('datetime.datetimepicker.date')),
            'date_update'       => optional($sst->updated_at)->format(config('datetime.datetimepicker.date')),
        ];
    }
}
