<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Eot;
use League\Fractal\TransformerAbstract;

class EotApproveTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Eot $eot
     *
     * @return array
     */
    public function transform(Eot $eot)
    {
        if (! empty($eot->eot_approve)) {
            $extended_time_approved = $eot->eot_approve->period . ' ' . $eot->eot_approve->period_type->name;
            $approval               = $eot->eot_approve->approval_label;
        } else {
            $extended_time_approved = '-';
            $approval               = '-';
        }

        return [
            'hashslug'                => $eot->hashslug,
            'bil'                     => $eot->bil,
            'approval'                => $approval,
            'extended_time_requested' => $eot->period . ' ' . $eot->period_type->name,
            'extended_time_approved'  => $extended_time_approved,
            'created_at'              => optional($eot->created_at)->format(config('datetime.datetimepicker.date_time')),
            'updated_at'              => optional($eot->updated_at)->format(config('datetime.datetimepicker.date_time')),
        ];
    }
}
