<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Approval;
use League\Fractal\TransformerAbstract;

class AcquisitionApprovalTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Approval $approval
     *
     * @return array
     */
    public function transform(Approval $approval)
    {
//        return $approval->toArray();
        return [
            'hashslug'   => $approval->hashslug,
            'reference'  => $approval->reference,
            'title'      => $approval->title,
            'created_at' => optional($approval->created_at)->format(config('datetime.datetimepicker.date_time')),
            'updated_at' => optional($approval->updated_at)->format(config('datetime.datetimepicker.date_time')),
        ];
    }
}
