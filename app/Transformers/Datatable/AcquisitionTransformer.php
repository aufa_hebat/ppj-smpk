<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Acquisition;
use League\Fractal\TransformerAbstract;

class AcquisitionTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Acquisition\Acquisition $acquisition
     *
     * @return array
     */
    public function transform(Acquisition $acquisition)
    {
        return [
            'hashslug'          => $acquisition->hashslug,
            'reference'         => $acquisition->reference,
            'title'             => $acquisition->title,
            'briefing_location' => $acquisition->briefing_location,
            'briefing_at'       => optional($acquisition->briefing_at)->format(config('datetime.datetimepicker.date_time')),
            'visit_location'    => $acquisition->visit_location,
            'visit_date_time'   => optional($acquisition->visit_date_time)->format(config('datetime.datetimepicker.date')),
            'created_at'        => optional($acquisition->created_at)->format(config('datetime.datetimepicker.date_time')),
            'updated_at'        => optional($acquisition->updated_at)->format(config('datetime.datetimepicker.date_time')),
            'display'    => $acquisition->status_task,
            'department' => $acquisition->approval->department->name
        ];
    }
}
