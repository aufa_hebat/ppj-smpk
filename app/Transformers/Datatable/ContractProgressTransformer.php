<?php

namespace App\Transformers\Datatable;

use App\Models\Acquisition\Monitoring\ContractProgress;
use League\Fractal\TransformerAbstract;

class ContractProgressTransformer extends TransformerAbstract
{
    public function transform(ContractProgress $cp)
    {
        return [
            'id' => $cp->id,
            'on_plan_date' => $cp->on_plan_date,
            'on_plan_date_percent' => $cp->on_plan_date_percent,
            'on_site_date_percent' => $cp->on_site_date_percent,
            'on_payment_percent' => $cp->on_payment_percent,
        ];
    }
}
