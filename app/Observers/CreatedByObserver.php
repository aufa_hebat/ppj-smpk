<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

/**
 * See events.
 *
 * @href https://laravel.com/docs/5.5/eloquent#events
 * Available metthods: retrieved, creating, created, updating, updated,
 * saving, saved, deleting, deleted, restoring, restored
 */
class CreatedByObserver
{
    /**
     * Listen to the creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function creating(Model $model)
    {
        if (Schema::hasColumn($model->getTable(), $model->getCreatedByKeyName())) {
            $model->{$model->getCreatedByKeyName()} = user()->id;
        }
    }
}
