<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use App\Models\Acquisition\Approval;

/**
 * See events.
 *
 * @href https://laravel.com/docs/5.5/eloquent#events
 * Available metthods: retrieved, creating, created, updating, updated,
 * saving, saved, deleting, deleted, restoring, restored
 */
class ReferenceObserver
{
    /**
     * Listen to the creating event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function creating(Model $model)
    {
        if (Schema::hasColumn($model->getTable(), 'reference') && is_null($model->reference)) {
            $apprv = Approval::orderBy('id', 'DESC')->first();
            //$model->reference = generate_reference($model->referencePrefix(), $model::count() ?? 0);
            $model->reference = generate_reference($model->referencePrefix(), $apprv->id ?? 0);
        }
    }
}
