<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class TaskObserver
{
    /**
     * Listen to the created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function created(Model $model)
    {
        if (method_exists($model, 'getWorkflowSlugName')) {
            $this->createTask($model);
        }
    }

    /**
     * Listen to the updated event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function updated(Model $model)
    {
        $this->markAsDone($model);
    }

    /**
     * Listen to the created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    private function createTask(Model $model)
    {
        $user_id = isset($model->user) ? $model->user->id : user()->id;
        task($model->getWorkflowSlugName())
            ->user($user_id)
            ->create($model);
    }

    /**
     * Listen to the created event.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    private function markAsDone(Model $model)
    {
        if (isset($model->task_is_done) && true == $model->task_is_done) {
            task($model->getWorkflowSlugName())
                ->user($model->user_id)
                ->markAsDone($model);
        }
    }
}
