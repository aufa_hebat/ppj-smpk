<?php

namespace App\Listeners\Task;

use App\Events\Workflow\CreateTasks;

class InitialiseTask
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param CreateTasks $event
     */
    public function handle(CreateTasks $event)
    {
        $acquisition = $event->acquisition;
        \App\Models\Workflow::where($event->workflow_field, $event->workflow_value)->each(function ($workflow) use ($acquisition) {
            \App\Models\Task::create([
                'acquisition_approval_id' => $acquisition->acquisition_approval_id,
                'acquisition_id'          => $acquisition->id,
                'department_id'           => $acquisition->department_id,
                'workflow_id'             => $workflow->id,
                'user_id'                 => $acquisition->user_id,
            ]);
        });
    }
}
