<?php

namespace App\Macros;

class Carbon
{
    public static function registerMacros()
    {
        \Carbon::macro('intlFormat', function ($datetime, $formatter = \IntlDateFormatter::NONE) {
            return (new \IntlDateFormatter(
                config('app.locale'),
                \IntlDateFormatter::GREGORIAN,
                $formatter
            ))->format(\Carbon\Carbon::parse($datetime, config('app.timezone')));
        });
    }
}
