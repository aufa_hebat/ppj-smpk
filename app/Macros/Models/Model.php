<?php

namespace App\Macros\Models;

use Illuminate\Database\Eloquent\Builder;

class Model
{
    public static function registerMacros()
    {
        Builder::macro('hashslug', function ($hashslug) {
            return self::where('hashslug', $hashslug);
        });

        Builder::macro('findByHashSlug', function ($hashslug) {
            return self::hashslug($hashslug)->firstOrFail();
        });

        Builder::macro('hashslugOrId', function ($identifier) {
            return self::hashslug($identifier)->orWhere('id', $identifier);
        });

        Builder::macro('findByHashSlugOrId', function ($identifier) {
            return self::hashslugOrId($identifier)->firstOrFail();
        });

        Builder::macro('slug', function ($slug) {
            return self::where('slug', $slug);
        });

        Builder::macro('findBySlug', function ($slug) {
            return self::slug($slug)->firstOrFail();
        });
    }
}
