<?php

namespace App\Services;

use App\Models\Acquisition\Sst;

class ContractProgressChartService
{
    /**
     * [$labels description].
     *
     * @var array
     */
    public $labels = [];

    /**
     * [$labels description].
     *
     * @var array
     */
    public $onsite = [];

    /**
     * [$labels description].
     *
     * @var array
     */
    public $onplan = [];

    /**
     * [$labels description].
     *
     * @var array
     */
    public $finance = [];
    
     /**
     * [$labels description].
     *
     * @var array
     */
    public $onSiteFinance = [];
    
    
    /**
     * The sst.
     *
     * @var
     */
    protected $sst;

    /**
     * Create a new ChartService instance.
     */
    public function __construct(Sst $sst)
    {
        $this->sst = $sst;
    }

    /**
     * Create an instance of ChartService.
     *
     * @return App\Services\ChartService
     */
    public static function make(Sst $sst)
    {
        return new self($sst);
    }

    /**
     * Handle the processing.
     */
    public function handle()
    {
        $this->getData();
        
        return [
            'scales' => [
                'xAxes' => [
                    [
                        'display'    => true,
                        'scaleLabel' => [
                            'display'     => false,
                            'labelString' => 'Month',
                        ],
                    ],
                ],
                'yAxes' => [
                    [
                        'type'       => 'linear',
                        'display'    => true,
                        'scaleLabel' => [
                            'display'     => false,
                            'labelString' => 'Value',
                        ],
                        'id' => 'y-physical',
                         'ticks' => [
                            'min' => 0,
                            'max' => 100,
                        ],
                    ],
                    
                ],
            ],
            'datasets' => [
                [
                    'label'           => 'On Plan %',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(54, 162, 235)',
                    'borderColor'     => 'rgb(54, 162, 235)',
                    'data'            => $this->onplan,
                    'yAxisID'         => 'y-physical',
                ],
                [
                    'label'           => 'On Site %',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(54, 162, 125)',
                    'borderColor'     => 'rgb(54, 162, 125)',
                    'data'            => $this->onsite,
                    'yAxisID'         => 'y-physical',
                ],
                [
                    'label'           => 'Pembayaran %',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor'     => 'rgb(255, 99, 132)',
                    'data'            => $this->finance,
                    'yAxisID'         => 'y-physical',
                ],
//                [
//                    'label'           => 'On Site Pembayaran %',
//                    'fill'            => false,
//                    'backgroundColor' => 'rgb(153, 102, 255)',
//                    'borderColor'     => 'rgb(153, 102, 255)',
//                    'data'            => $this->onSiteFinance,
//                    'yAxisID'         => 'y-physical',
//                ],
            ],
            'labels' => $this->labels,
        ];
    }

    private function getData()
    {
        
        $contractProgresses =  \App\Models\Acquisition\Monitoring\ContractProgress::where('acquisition_id', $this->sst->acquisition->id)
                ->orderBy('on_plan_date','ASC')
                ->get();
               
          //add latest date, just appear until latest date of sap payment
         $countZ = count($contractProgresses);
         if($countZ > 0){       
         $latestSapPaymentDate = $contractProgresses->first()->on_plan_date;
             
         $isDoneSapPayment = false;
        
         
        foreach ($contractProgresses as $key => $value) {
                      
            $this->labels[] = $value->on_plan_date->format('d-M-Y');
            if($value->on_plan_date_percent == 0){
               $this->onplan[] = null;
            }else{
                $this->onplan[] = $value->on_plan_date_percent ?? 0;
                //$this->finance[] = $value->on_plan_date_percent ?? 0;
            }
           
            $this->onsite[] = !empty($value->siteVisit) ? $value->siteVisit->percentage_completed : null;
            

           
           
            $sap_added_amount = 0.00;
                  
            foreach ($this->sst->ipcInvoice as $key => $valueIpc) {          
                 if($latestSapPaymentDate <= $valueIpc->sap_date){  
                     $latestSapPaymentDate = $valueIpc->sap_date;
                 }
                 //dd($value->on_plan_date);
                if($valueIpc->status == 4 && $valueIpc->sap_date!=null && $valueIpc->sap_date <= $value->on_plan_date->endOfDay()){
                  
                    $sap_added_amount += $valueIpc->demand_amount;
                      //dd('masuk'. $sap_added_amount);
                }       
            }
            
           // dd($sap_added_amount);
                   
            $sap_payment_percent = 0.00;
                     
            if($sap_added_amount > 0){
                
               $sap_payment_percent = ($sap_added_amount/$this->sst->ins_work_amount) * 100;
               
            }else{
                //dd('uit:'.$value->on_plan_date_percent);
            }
                 
              if( $value->on_plan_date > $latestSapPaymentDate){
                  //dd($value->on_plan_date);
                  //dd('uit:'.$value->on_plan_date_percent);
                  $isDoneSapPayment = true;               
              }
              
              if($isDoneSapPayment){               
                 $this->finance[] = null;
              }
              else{
                 // dd(round($sap_payment_percent, 2));
                  //dd('uit:'.$value->on_plan_date_percent);
                  if($value->on_plan_date_percent == '98'){
                      //dd('haha'.$sap_added_amount);
                  }
                  $this->finance[] = round($sap_payment_percent, 2);
              } 
            
        
            
        }
            
        }
        //dd($this->finance);
        //dd($latestSapPaymentDate);
    } //end get data


}
