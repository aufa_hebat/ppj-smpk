<?php

namespace App\Services;

class RateService
{
    public static function make()
    {
        return new self();
    }

    public function bon($type, $amount, $periodType, $period)
    {
        $percentage = 0;
        switch ($type) {
            case 'Kerja':
                $percentage = 0.05;
                break;
            case 'Bekalan':
                $percentage = $this->calculatePercentage($type, $amount, $periodType, $period);
                break;
            case 'Perkhidmatan':
                $percentage = $this->calculatePercentage($type, $amount, $periodType, $period);
                break;                
            default:
                $percentage = $this->calculatePercentage($type, $amount, $periodType, $period);
                // $percentage = 0.05;
                break;
        }

        if($periodType == 4 && $period >= 2){
            $amountPerYear = $amount/$period;
        }else if($periodType == 3 && $period >= 24){
            $amountPerYear = ($amount/round(($period/12)));
        }else if($periodType == 2 && $period >= 104){
            $amountPerYear = ($amount/round(($period/52)));
        }else if($periodType == 1 && $period >= 730){
            $amountPerYear = ($amount/round(($period/365)));
        }else{
            $amountPerYear = $amount;
        }

        return $percentage * $amountPerYear;
        //return $percentage * $amount;
    }

    private function calculatePercentage($type, $amount, $periodType, $period){
        $percentageDefault = 0.0;
        $percentage = 0.0;
        
        if($type == 'Bekalan' || $type == 'Perkhidmatan' || $type == 'Penyelenggaraan'){

            //check percentage default
            $percentageDefault = $this->ratePercentageByAmount($amount);

            //if tempoh > 2 tahun
            if($periodType == 4 && $period >= 2){
                $amountPerYear = $amount/$period;
            }else if($periodType == 3 && $period >= 24){
                $amountPerYear = ($amount/round(($period/12)));
            }else if($periodType == 2 && $period >= 104){
                $amountPerYear = ($amount/round(($period/52)));
            }else if($periodType == 1 && $period >= 730){
                $amountPerYear = ($amount/round(($period/365)));
            }else{
                $amountPerYear = $amount;
            }
            
            $percentage = $this->ratePercentageByAmount($amountPerYear);
            if($percentage == 0.0){
                $percentage = $percentageDefault;
            }
        }

        return $percentage;
    }

    private function ratePercentageByAmount($amount){
        if($amount >= 20000000 && $amount < 50000000){
            $percentage = 0.025;
        }else if($amount >= 50000000){
            $percentage = 0.05;
        }else{
            $percentage = 0.0;
        }
        
        return $percentage;
    }
}
