<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use App\Models\VO\PPJHK\Ppjhk;

/**
 * Jasper Service
 * JasperService::make($ipc)->handle();.
 */
class JasperService
{
    private $rpt_bq_endpoint;
    private $acq;

    public function __construct($type, $category, $hashslug)
    {
        $this->rpt_endpoint    = config('jasper.endpoint');
        $this->hashslug        = $hashslug;
        $this->type            = $type;
        $this->category        = $category;
        $this->http            = new Client();
    }

    public function getHashslug()
    {
        return $this->hashslug;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getRptEndpoint()
    {
        if(empty($this->rpt_endpoint)){
            $this->rpt_endpoint = "http://10.10.1.30/api/report/";
        }

        return $this->rpt_endpoint;
    }

    public static function make($type, $category, $hashslug)
    {
        return new self($type, $category, $hashslug);
    }

    public function handle()
    {
        $hashslug       = $this->getHashslug();
        $type           = $this->getType();
        $category       = $this->getCategory();
        $respnd = [];

        if(!empty($hashslug)){
            if($type == 'bq') {
                $response = $this->http->request('GET', $this->getRptEndpoint().'/'.$type.'/'.$category.'/'.$hashslug);
                array_push($respnd, [
                    'data' => $response->getBody()->getContents(),
                ]);
            }elseif($type == 'bq_full'){
                $response = $this->http->request('GET', $this->getRptEndpoint().'/'.$type.'/'.$category.'/'.$hashslug);
                array_push($respnd, [
                    'data' => $response->getBody()->getContents(),
                ]);                
            }elseif($type == 'vo'){
                $response = $this->http->request('GET', $this->getRptEndpoint().'/'.$type.'/'.$category.'/'.$hashslug);
                array_push($respnd, [
                    'data' => $response->getBody()->getContents(),
                ]);
            }elseif($type == 'ppjhk'){
                $ppjhk = Ppjhk::findByHashSlug($hashslug);
                $netAmount = $ppjhk->elements->pluck('net_amount')->sum() +  $ppjhk->cancels->pluck('amount')->sum();

                $amountWords = $this->getAmountInWord($netAmount);

                $response = $this->http->request('GET', $this->getRptEndpoint().'/'.$type.'/'.$category.'/'.$hashslug.'/'.money()->toCommon($netAmount ?? "0", 2).'/'.$amountWords);
                array_push($respnd, [
                    'data' => $response->getBody()->getContents(),
                ]);

                //echo $amountWords . " | " . money()->toHuman($netAmount ?? "0", 2);
            }
                                                                 


        }else{
            //Do Nothing
        }

        return $respnd;
    }

    public function getAmountInWord($netAmount){

        $amountInWord = "";

        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
        $amaun = str_replace("-","",money()->toCommon($netAmount ?? "0" , 2));
        $format = explode('.',$amaun);
        $formats = substr($amaun,-2);
        $partringgit = str_replace(',', '', $format[0]);
        $partsen = $format[1];
        $partkosongsen = (int)$formats;

        if($partkosongsen == '.00'){
            $amountInWord =  $f->format($partringgit) . " Sahaja";
        }
        else{
            $amountInWord = $f->format($partringgit) . " Dan " . $f->format($partkosongsen) . " Sen Sahaja";
        }

        return $amountInWord;
    }
}
