<?php

namespace App\Services;

use App\Notifications\Notification;

class NotificationService
{
    use \App\Traits\MailTrait;

    public function send()
    {
        if ($this->user) {
            $this->user->notify(new Notification(
                $this->subject,
                $this->content,
                $this->link,
                $this->link_label,
                $this->data,
                $this->attachments,
                $this->cc,
                $this->bcc,
                $this->template
            ));
        } else {
            throw new \App\Exceptions\NoUserSpecifiedException('No User Specified.', 1);
        }
    }
}
