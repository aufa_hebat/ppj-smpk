<?php

namespace App\Services;

class WorkflowService
{
    protected $workflow;
    protected $slug;

    public function __construct($slug)
    {
        $this->slug     = $slug;
        $this->workflow = \App\Models\Workflow::whereSlug($slug)->firstOrFail();
    }

    public static function make($slug)
    {
        return new self($slug);
    }

    /**
     * Get All Tasks based on workflow slug.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getTasks()
    {
        return \App\Models\Workflow::whereSlug($this->slug)->get();
    }

    /**
     * Get All Tasks are done based on workflow slug.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDoneTasks()
    {
        return \App\Models\Workflow::whereSlug($this->slug)
            ->whereHas('tasks', function ($query) {
                $query->whereIsDone(true);
            })->get();
    }

    /**
     * Get All Tasks are not done based on workflow slug.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNotDoneTasks()
    {
        return \App\Models\Workflow::whereSlug($this->slug)
            ->whereHas('tasks', function ($query) {
                $query->whereIsDone(false);
            })->get();
    }
}
