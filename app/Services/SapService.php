<?php

namespace App\Services;

use App\Models\Acquisition\Ipc;
use App\Models\IpcSap;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;

/**
 * SAP Service
 * SapService::make($ipc)->handle();.
 */
class SapService
{
    private $sap_endpoint;
    private $ipc;

    public function __construct(Ipc $ipc)
    {
        $this->sap_endpoint = config('sap.endpoint');
        $this->ipc          = $ipc;
        $this->http         = new Client();
    }

    public function getIpc()
    {
        return $this->ipc;
    }

    public function getSapEndpoint()
    {
        return $this->sap_endpoint;
    }

    public static function make(Ipc $ipc)
    {
        return new self($ipc);
    }

    public function handle()
    {
        $ipc      = $this->getIpc();
        $get_data = json_encode($this->getData());
        IpcSap::create([
            'user_id' => user()->id,
            'ipc_id'  => $ipc->id,
            'data'    => $get_data,
        ]);
        $respnd = [];
        if(!empty($ipc) && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0){
            foreach($this->getData() as $invc){                                                             
                $response = $this->http->request('POST', $this->getSapEndpoint(),
                    [
                        'json' => $invc,
                    ]
                );
                array_push($respnd, [
                    'data' => $response->getBody()->getContents(),
                ]);
            }
        }else{
            $respnd = $this->http->request('POST', $this->getSapEndpoint(),
                [
                    'json' => $this->getData(),
                ]
            );
            array_push($respnd, [
                'data' => $response->getBody()->getContents(),
            ]);
        }
//        return $response->getBody()->getContents();
        return $respnd;
    }

    /**
     * @todo all should be store as array, use json_encode & json_decode as necessary
     */
    private function getData()
    {
        $ipc = $this->getIpc();
        $invoice        = (!empty($ipc))? $ipc->ipcInvoice:null;

        if(!empty($ipc) && $invoice != null && $invoice->count() > 0){
            $ttl_inv = $invoice->count();
            $temp = null;
            foreach ($invoice as $k=>$inv) {
                $vendor_sst_id  = '';
                $line1          = '{}';
                $line2          = '{}';
                $line3          = '{}';
                $line4          = '{}';
                $line5          = '{}';
                $line6          = '{}';
                $line7          = '{}';
                $line8          = '{}';
                
                $cost           = (! empty($inv->financial->costCenter)      && ! empty($inv->financial->costCenter->code))     ? $inv->financial->costCenter->code     : '';
                $gla            = (! empty($inv->financial->glAccount)       && ! empty($inv->financial->glAccount->code))      ? $inv->financial->glAccount->code      : '';
                $ba             = (! empty($inv->financial->businessArea)    && ! empty($inv->financial->businessArea->code))   ? $inv->financial->businessArea->code   : '';
                $fa             = (! empty($inv->financial->functionalArea)  && ! empty($inv->financial->functionalArea->code)) ? $inv->financial->functionalArea->code : '';
                $fund           = (! empty($inv->financial->fund)            && ! empty($inv->financial->fund->code))           ? $inv->financial->fund->code           : '';
                $funded         = (! empty($inv->financial->fundedProgramme) && ! empty($inv->financial->fundedProgramme->code) && 'KWP' == $ba) ? $inv->financial->fundedProgramme->code : '';
                
                if($ba == 'KWM'){
                    $vendor_sst_id = $inv->company->kwm_vendor_id ?? '' ;
                }elseif($ba == 'KWP'){
                    $vendor_sst_id = $inv->company->kwp_vendor_id ?? '' ;
                }
                
                $vo             = 0; 
                $adv            = 0;
                $adv_balik      = 0;
                $wjp            = 0;
                $wjp_lepas      = 0;
                $credit         = 0;
                $lad            = 0;
                $material       = 0;
                $material_tahan = 0;
                $total_inv      = 0;
                
                if($inv->status_contractor == 1){
                    
//                    $vo             = ($ttl_inv > 1) ? round($ipc->working_change_amount / $ttl_inv)  :($ipc->working_change_amount   ?? 0); 
//                    $adv            = ($ttl_inv > 1) ? round($ipc->advance_amount / $ttl_inv)         :($ipc->advance_amount          ?? 0);
//                    $adv_balik      = ($ttl_inv > 1) ? round($ipc->give_advance_amount / $ttl_inv)    :($ipc->give_advance_amount     ?? 0);
//                    $wjp            = ($ttl_inv > 1) ? round($ipc->wjp_amount / $ttl_inv)             :($ipc->wjp_amount              ?? 0);
//                    $wjp_lepas      = ($ttl_inv > 1) ? round($ipc->wjp_lepas_amount / $ttl_inv)       :($ipc->wjp_lepas_amount        ?? 0);
//                    $credit         = ($ttl_inv > 1) ? round($ipc->credit / $ttl_inv)                 :($ipc->credit                  ?? 0);
//                    $lad            = ($ttl_inv > 1) ? round($ipc->compensation_amount / $ttl_inv)    :($ipc->compensation_amount     ?? 0);
//                    $material       = ($ttl_inv > 1) ? round($ipc->material_on_site_amount / $ttl_inv):($ipc->material_on_site_amount ?? 0);
//                    $material_tahan = ($ttl_inv > 1) ? round($ipc->material_tahanan_amount / $ttl_inv):($ipc->material_tahanan_amount ?? 0);
//                    $total_inv      = $inv->invoice + $vo + $material - $material_tahan;
                    
                    $vo             = $ipc->working_change_amount   ?? 0; 
                    $adv            = $ipc->advance_amount          ?? 0;
                    $adv_balik      = $ipc->give_advance_amount     ?? 0;
                    $wjp            = $ipc->wjp_amount              ?? 0;
                    $wjp_lepas      = $ipc->wjp_lepas_amount        ?? 0;
                    $credit         = $ipc->credit                  ?? 0;
                    $lad            = $ipc->compensation_amount     ?? 0;
                    $material       = $ipc->material_on_site_amount ?? 0;
                    $material_tahan = $ipc->material_tahanan_amount ?? 0;
                    $total_inv      = $inv->invoice + $vo + $material - $material_tahan;
                    
                    $post_inv       = $inv->invoice + $adv - $adv_balik - $wjp + $wjp_lepas - $credit - $lad + $vo + $material - $material_tahan;
                }else{
                    $credit         = $ipc->credit ?? 0;
                    $total_inv      = $inv->invoice;
                    $post_inv       = $inv->invoice - $credit;
                }
                
//                $curr_date      = Carbon::now()->subDays(30)->format('Y-m-d H:i:s');
                $curr_date      = Carbon::now()->format('Y-m-d H:i:s');
                
                if (!empty($adv) || $adv != null || $adv != '0') {//send line for advance
                    $line1 = '{"ip_LIFNR":"' . $vendor_sst_id . '"' .
                            ',"bschl":"39"' .
                            ',"umsks":"D"' .
                            ',"waers":"RM"' .
                            ',"dmbtr":' . money()->toCommon($adv) .
                            ',"zuonr":""' .
                            ',"sgtxt":"' . $ipc->short_text . '"' .
                            ',"kostl":""' .
                            ',"hkont":""' .
                            ',"gsber":"' . $ba . '"' .
                            ',"fkber":""' .
                            ',"geber":""' .
                            ',"mwskz":"IZ"' .
                            ',"aufnr":""' .
                            ',"buzei":""' .
                            '}';
                }
                if (!empty($adv_balik) || $adv_balik != null || $adv_balik != '0') {//send line for return advance
                    $line2 = '{"ip_LIFNR":"' . $vendor_sst_id . '"' .
                            ',"bschl":"39"' .
                            ',"umsks":"D"' .
                            ',"waers":"RM"' .
                            ',"dmbtr":-' . money()->toCommon($adv_balik ?? 0) .
                            ',"zuonr":""' .
                            ',"sgtxt":"' . $ipc->short_text . '"' .
                            ',"kostl":""' .
                            ',"hkont":""' .
                            ',"gsber":"' . $ba . '"' .
                            ',"fkber":""' .
                            ',"geber":""' .
                            ',"mwskz":"IZ"' .
                            ',"aufnr":""' .
                            ',"buzei":""' .
                            '}';
                }
                if (!empty($wjp_lepas) || $wjp_lepas != null || $wjp_lepas != '0') {//send line for wjp lepas
                    $line3 = '{"ip_LIFNR":"' . $vendor_sst_id . '"' .
                            ',"bschl":"29"' .
                            ',"umsks":"R"' .
                            ',"waers":"RM"' .
                            ',"dmbtr":' . money()->toCommon($wjp_lepas) .
                            ',"zuonr":""' .
                            ',"sgtxt":"' . $ipc->short_text . '"' .
                            ',"kostl":""' .
                            ',"hkont":""' .
                            ',"gsber":"' . $ba . '"' .
                            ',"fkber":""' .
                            ',"geber":""' .
                            ',"mwskz":"IZ"' .
                            ',"aufnr":""' .
                            ',"buzei":""' .
                            '}';
                }
                if (!empty($wjp) || $wjp != null || $wjp != '0') {//send line for wjp
                    $line4 = '{"ip_LIFNR":"' . $vendor_sst_id . '"' .
                            ',"bschl":"39"' .
                            ',"umsks":"R"' .
                            ',"waers":"RM"' .
                            ',"dmbtr":-' . money()->toCommon($wjp) .
                            ',"zuonr":""' .
                            ',"sgtxt":"' . $ipc->short_text . '"' .
                            ',"kostl":""' .
                            ',"hkont":""' .
                            ',"gsber":"' . $ba . '"' .
                            ',"fkber":""' .
                            ',"geber":""' .
                            ',"mwskz":"IZ"' .
                            ',"aufnr":""' .
                            ',"buzei":""' .
                            '}';
                }
                if (!empty($credit) || $credit != null || $credit != '0') {//send line for credit
                    $line5 = '{"ip_LIFNR":""' .
                            ',"bschl":"50"' .
                            ',"umsks":""' .
                            ',"waers":"RM"' .
                            ',"dmbtr":-' . money()->toCommon($credit) .
                            ',"zuonr":""' .
                            ',"sgtxt":"' . $ipc->short_text . '"' .
                            ',"kostl":"' . $cost . '"' .
                            ',"hkont":"' . $gla . '"' .
                            ',"gsber":"' . $ba . '"' .
                            ',"fkber":"' . $fa . '"' .
                            ',"geber":"' . $fund . '"' .
                            ',"mwskz":"IZ"' .
                            ',"aufnr":"' . $funded . '"' .
                            ',"buzei":""' .
                            '}';
                }
                if (!empty($lad) || $lad != null || $lad != '0') {//send line for lad
                $line6 = '{"ip_LIFNR":""' .
                        ',"bschl":"50"' .
                        ',"umsks":""' .
                        ',"waers":"RM"' .
                        ',"dmbtr":-' . money()->toCommon($lad) .
                        ',"zuonr":""' .
                        ',"sgtxt":"' . $ipc->short_text . '"' .
                        ',"kostl":"' . $cost . '"' .
                        ',"hkont":"' . $gla . '"' .
                        ',"gsber":"' . $ba . '"' .
                        ',"fkber":"' . $fa . '"' .
                        ',"geber":"' . $fund . '"' .
                        ',"mwskz":"IZ"' .
                        ',"aufnr":"' . $funded . '"' .
                        ',"buzei":""' .
                        '}';
                }
                $line7 = '{"ip_LIFNR":"' . $vendor_sst_id . '"' .
                        ',"bschl":"31"' .
                        ',"umsks":""' .
                        ',"waers":"RM"' .
                        ',"dmbtr":-' . money()->toCommon($post_inv) .
                        ',"zuonr":""' .
                        ',"sgtxt":"' . $ipc->long_text . '"' .
                        ',"kostl":""' .
                        ',"hkont":""' .
                        ',"gsber":"' . $ba . '"' .
                        ',"fkber":""' .
                        ',"geber":""' .
                        ',"mwskz":"IZ"' .
                        ',"buzei":""' .
                        ',"aufnr":""' .
                        '}';

                $line8 = '{"ip_LIFNR":""' .
                        ',"bschl":"40"' .
                        ',"umsks":""' .
                        ',"waers":"RM"' .
                        ',"dmbtr":' . money()->toCommon($total_inv) .
                        ',"zuonr":""' .
                        ',"sgtxt":"' . $ipc->long_text . '"' .
                        ',"kostl":"' . $cost . '"' .
                        ',"hkont":"' . $gla . '"' .
                        ',"gsber":"' . $ba . '"' .
                        ',"fkber":"' . $fa . '"' .
                        ',"geber":"' . $fund . '"' .
                        ',"mwskz":"IZ"' .
                        ',"buzei":""' .
                        ',"aufnr":"' . $funded . '"' .
                        '}';
                $json = '{"parkInvoiceLineItems":[' .
                        $line1 . ',' . $line2 . ',' . $line3 . ',' . $line4 . ',' . $line5 . ',' . $line6 . ','
                        . $line7 . ',' . $line8
                        . '],'
                        . '"ip_HEADER_TEXT":"' . $ipc->short_text . '"'
                        . ',"ip_BUKRS":"1000"'
                        . ',"ip_LIFNR":"' . $vendor_sst_id . '"'
                        . ',"ip_INVOICE":"' . $inv->invoice_no . '"'
                        . ',"ip_DATE":"' . $curr_date . '"'
                        . ',"ip_REF":"' . $inv->ref_text . '"'
                        . '}';
                $rep_json = preg_replace('/{},/', '', $json);
                if($ttl_inv > 1){
                    if($temp == null){
                        $temp = '['.$rep_json.'';
                    }else{
                        $temp = $temp .','. $rep_json;
                    }
                    if($ttl_inv == ($k+1)){
                        $temp = $temp.']';
                    }
                }else{
                    $temp = '['.$rep_json.']';
                }
            }
//            dd(json_decode($temp));
//            return json_decode($rep_json);
                return json_decode($temp);
        }
    }
}
