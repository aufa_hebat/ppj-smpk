<?php

namespace App\Services;

use App\Models\Acquisition\Sst;

/**
 * ChartService Service.
 */
class ProjectMonitoringChartService
{
    /**
     * [$labels description].
     *
     * @var array
     */
    public $labels = [];

    /**
     * [$labels description].
     *
     * @var array
     */
    public $onsite = [];

    /**
     * [$labels description].
     *
     * @var array
     */
    public $onplan = [];

    /**
     * [$labels description].
     *
     * @var array
     */
    public $finance = [];
    /**
     * The sst.
     *
     * @var
     */
    protected $sst;

    /**
     * Create a new ChartService instance.
     */
    public function __construct(Sst $sst)
    {
        $this->sst = $sst;
    }

    /**
     * Create an instance of ChartService.
     *
     * @return App\Services\ChartService
     */
    public static function make(Sst $sst)
    {
        return new self($sst);
    }

    /**
     * Handle the processing.
     */
    public function handle()
    {
        $this->getPhysicalData();
        $this->getFinancialData();

        return [
            'scales' => [
                'xAxes' => [
                    [
                        'display'    => true,
                        'scaleLabel' => [
                            'display'     => false,
                            'labelString' => 'Month',
                        ],
                    ],
                ],
                'yAxes' => [
                    [
                        'type'       => 'linear',
                        'display'    => true,
                        'scaleLabel' => [
                            'display'     => false,
                            'labelString' => 'Value',
                        ],
                        'id' => 'y-physical',
                    ],
                    [
                        'type'      => 'linear',
                        'display'   => true,
                        'position'  => 'right',
                        'id'        => 'y-finance',
                        'gridLines' => [
                            'drawOnChartArea' => false,
                        ],
                        'ticks' => [
                            'min' => 0,
                            'max' => $this->getMaxAmount(),
                        ],
                    ],
                ],
            ],
            'datasets' => [
                [
                    'label'           => 'On Plan %',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(54, 162, 235)',
                    'borderColor'     => 'rgb(54, 162, 235)',
                    'data'            => $this->onplan,
                    'yAxisID'         => 'y-physical',
                ],
                [
                    'label'           => 'On Site %',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(54, 162, 125)',
                    'borderColor'     => 'rgb(54, 162, 125)',
                    'data'            => $this->onsite,
                    'yAxisID'         => 'y-physical',
                ],
                [
                    'label'           => 'Kemajuan Pembayaran',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor'     => 'rgb(255, 99, 132)',
                    'data'            => $this->finance,
                    'yAxisID'         => 'y-finance',
                ],
            ],
            'labels' => $this->labels,
        ];
    }

    private function getPhysicalData()
    {
        foreach ($this->sst->activities as $key => $value) {
            // why  this line have ?? $this->labels[] = $value->scheduled_ended_at->format('d-M-Y');
            $this->onplan[] = $value->scheduled_completion ?? 0;
            $this->onsite[] = $value->actual_completion    ?? 0;
        }
    }

    private function getFinancialData()
    {
        $amountSum = null;
        foreach ($this->sst->ipc as $ipc) {
           
            
            if ($ipc->status == 4) {
//                foreach ($ipc->ipcInvoice as $ipcInvoice) {
//                    foreach ($ipcInvoice->ipcBq as $ipcBq) {
//                        if(!is_null($ipcBq->adjust_amount)){
//                           $this->finance[] = money()->toCommon($ipcBq->adjust_amount);
//                        }
//                    }
//                }
                
                //column demand amount $given_adv = $give_adv - $return_adv;
              $amountSum = $amountSum + $ipc->demand_amount;
                
                
            }
            //if(!is_null($amountSum)){
                $this->finance[] = $amountSum;
            //}
        }
    }

    private function getMaxAmount()
    {
        $total  = $this->sst->acquisition->appointed->sum->offered_price;
        $amount = money()->toCommon($total);

        return round($amount, 0);
    }
}
