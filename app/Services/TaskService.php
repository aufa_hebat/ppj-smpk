<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

class TaskService
{
    public $slug;
    public $workflow;
    public $user;

    public function __construct($slug)
    {
        $this->slug($slug);
        $this->workflow($slug);
    }

    public static function make($slug)
    {
        return new self($slug);
    }

    public function slug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function user($identifier)
    {
        $this->user = \App\Models\User::findByHashSlugOrId($identifier);

        return $this;
    }

    public function workflow($slug)
    {
        $this->workflow = \App\Models\Workflow::findBySlug($slug);

        return $this;
    }

    /**
     * Create a new task.
     *
     * tasks('slug')->user($identifer)->create($model);
     *
     * @param int    $taskable_id
     * @param object $taskable_type
     *
     * @return \App\Models\Task
     */
    public function create(Model $model)
    {
        return $this->task = \App\Models\Task::create([
            'user_id'       => $this->user->id,
            'workflow_id'   => $this->workflow->id,
            'taskable_id'   => $model->id,
            'taskable_type' => fqcn($model),
        ]);
    }

    /**
     * Mark a task as done.
     *
     * tasks('slug')->user($identifer)->markAsDone($model);
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return bool
     */
    public function markAsDone(Model $model)
    {
        return \App\Models\Task::query()
            ->whereWorkflowId($this->workflow->id)
            ->whereUserId($this->user->id)
            ->markAsDone($model->id, fqcn($model));
    }
}
