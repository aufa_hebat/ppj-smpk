<?php

namespace App\Services;

use App\Models\Acquisition\AppointedCompany;

class AppointedCompanyService
{
    public $appointedCompany;

    public function __construct(AppointedCompany $appointedCompany)
    {
        $this->appointedCompany = $appointedCompany;
    }

    public static function make(AppointedCompany $appointedCompany)
    {
        return new self($appointedCompany);
    }

    public function getInsuranTanggunganAwam($acqCat, $offeredPrice){
        $insTA = 0;
        if($acqCat == 'Sebut Harga' || $acqCat == 'Sebut Harga B'){
            if($offeredPrice <= 5000000){
                $insTA = 1000000;
            }else if($offeredPrice > 5000000 && $offeredPrice <= 10000000){
                $insTA = 2500000;
            }else if($offeredPrice > 10000000 && $offeredPrice <= 20000000){
                $insTA = 5000000;
            }else if($offeredPrice > 20000000 && $offeredPrice <= 50000000){
                $insTA = 10000000;
            }
        }else if($acqCat == 'Tender Terhad' || $acqCat == 'Tender Terbuka'){
            if($offeredPrice > 50000000 && $offeredPrice <= 500000000){
                $insTA = 20000000;
            }else if($offeredPrice > 500000000 && $offeredPrice <= 2000000000){                
                $insTA = 50000000;
            }else if($offeredPrice > 2000000000 && $offeredPrice <= 5000000000){
                $insTA = 100000000;
            }else if($offeredPrice > 5000000000){
                $insTA = 2000000000;
            }
        }
        return $insTA;
    }

    public function getInsuranPampasanPekerja($appointed){

        return getPriceWithoutWps($appointed) * 0.1;
    }

    public function getPriceWithoutWps($appointed){

        if($appointed->acquisition->bqElements->count() > 0){
            foreach($appointed->acquisition->bqElements as $element){
                $isProSumFound = common()->checkProSum($element->element);
                if($isProSumFound){
                    $amountWithoutWPS = $appointed->offered_price - $element->amount;                    
                }
            }
            if($amountWithoutWPS == 0){
                $amountWithoutWPS = $appointed->offered_price;
            }
        }else{
            $amountWithoutWPS = $appointed->offered_price;
        }
        return $amountWithoutWPS;
    }
}