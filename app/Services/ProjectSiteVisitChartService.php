<?php

namespace App\Services;

use App\Models\Acquisition\Sst;

class ProjectSiteVisitChartService
{
    /**
     * The site labels.
     *
     * @var
     */
    protected $labels = [];

    /**
     * The site visits data.
     *
     * @var
     */
    protected $data_site_visits = [];

    /**
     * The physical progress data.
     *
     * @var
     */
    protected $data_physical_progress = [];

    /**
     * The site visits.
     *
     * @var
     */
    protected $site_visits;

    /**
     * The activities.
     *
     * @var
     */
    protected $activities;

    /**
     * Create a new ChartService instance.
     */
    public function __construct(Sst $sst)
    {
        $this->activities  = $sst->activities;
        $this->site_visits = $sst->siteVisits;
    }

    /**
     * Create an instance of ChartService.
     *
     * @return App\Services\ChartService
     */
    public static function make(Sst $sst)
    {
        return new self($sst);
    }

    public function handle()
    {
        $this->getActivitiesData();
        $this->getSiteVisitData();

        return [
            'scales' => [
                'xAxes' => [
                    [
                        'display'    => true,
                        'scaleLabel' => [
                            'display'     => false,
                            'labelString' => 'Date',
                        ],
                    ],
                ],
                'yAxes' => [
                    [
                        'display'    => true,
                        'scaleLabel' => [
                            'display'     => false,
                            'labelString' => 'Value',
                        ],
                    ],
                ],
            ],
            'datasets' => [
                [
                    'label'           => 'Lawatan Penilaian Tapak',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(54, 162, 235)',
                    'borderColor'     => 'rgb(54, 162, 235)',
                    'data'            => $this->data_site_visits,
                ],
                [
                    'label'           => 'Kemajuan Progres Fizikal',
                    'fill'            => false,
                    'backgroundColor' => 'rgb(54, 162, 125)',
                    'borderColor'     => 'rgb(54, 162, 125)',
                    'data'            => $this->data_physical_progress,
                ],
            ],
            'labels' => $this->labels,
        ];
    }

    private function getSiteVisitData()
    {
        foreach ($this->site_visits as $key => $value) {
            $this->data_site_visits[] = $value->percentage_completed;
        }
    }

    private function getActivitiesData()
    {
        foreach ($this->activities as $key => $value) {
            $this->labels[]                 = ($value->scheduled_ended_at !=null ?  $value->scheduled_ended_at->format('d-M-Y'):null);
            $this->data_physical_progress[] = $value->scheduled_completion ?? 0;
        }
    }
}
