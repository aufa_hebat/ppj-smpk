<?php

namespace App\Services;

class DashboardService
{
    public static function make()
    {
        return new self();
    }

    public function home()
    {
        return \App\Models\Report\Figure::category('home')->get();
    }

    public function users()
    {
        return \App\Models\Report\Figure::category('user')->get();
    }

    public function companies()
    {
        return \App\Models\Report\Figure::category('company')->get();
    }

    public function acquisitions()
    {
        return \App\Models\Report\Figure::category('acquisition')->get();
    }

    public function sales()
    {
        return \App\Models\Report\Figure::category('sale')->get();
    }

    public function monitoring()
    {
        return \App\Models\Report\Figure::category('project-monitoring')->get();
    }
}
