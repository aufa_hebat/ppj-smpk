<?php

namespace App\Services;

class ChartService
{
    public static function make()
    {
        return new self();
    }

    public function acquisitions()
    {
        $byDepartment = \App\Processors\Charts\AcquisitionProcessor::make()->acquisitionByDepartment();

        return [
            'acquisition-by-department' => $byDepartment,
        ];
    }

    public function monitoring()
    {
        $byDepartment = \App\Processors\Charts\ProjectMonitoringProcessor::make()->contractCreatedByDepartment();

        $awardedCompaniesByDepartment = \App\Processors\Charts\ProjectMonitoringProcessor::make()->awardedCompaniesByDepartment();

        return [
            'project-monitoring-by-department' => $byDepartment,
            'awarded-companies-by-department'  => $awardedCompaniesByDepartment,
        ];
    }
}
