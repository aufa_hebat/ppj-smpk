<?php

namespace App\Services;

use App\Models\Acquisition\Ipc;
use App\Models\BillOfQuantity\Element;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Review;
use Illuminate\Support\Carbon;

/**
 * SAP Service
 * SapService::make($ipc)->handle();.
 */
class IpcService
{
    private $ipc;
    
    private $inv_total = null;
    private $cnt_ipc   = null;
    
    //List of all IPC
    private $allIpc    = null;
    private $notCurIpc = null;
    private $prevIpc   = null;
    private $reviewdoc = null;
    private $appointed = null;
    private $bqElement = null;
    private $prov_sum  = 0;
    private $prima_cost  = 0;

    public function __construct(Ipc $ipc)
    {
        $this->ipc          = $ipc;
        $this->allIpc       = Ipc::where('sst_id', $ipc->sst_id)->where('deleted_at',null)->orderBy('id','asc')->get();
        //$this->notCurIpc    = Ipc::where('sst_id', $ipc->sst_id)->where('id',$this->ipc->id)->where('deleted_at',null)->orderBy('id','asc')->get();
        $this->notCurIpc    = Ipc::where('sst_id', $ipc->sst_id)->where('id','<',$this->ipc->id)->where('deleted_at',null)->orderBy('id','asc')->get();
        //$this->prevIpc      = Ipc::where('sst_id', $ipc->sst_id)->where('id','<',$this->ipc->id)->where('deleted_at',null)->orderBy('id','asc')->get();
        $this->reviewdoc    = Review::where('type', 'Dokumen Kontrak')->where('acquisition_id',$ipc->sst->acquisition_id)->where('status','Selesai')->where('deleted_at',null)->get();
        $this->appointed    = AppointedCompany::where('acquisition_id', $ipc->sst->acquisition_id)->where('company_id', $ipc->sst->company_id)->where('deleted_at', null)->with('company')->first();
        $this->bqElement    = Element::where('acquisition_id', $ipc->sst->acquisition_id)->where('deleted_at', null);
        $this->prov_sum     = $this->bqElement
                                ->where(function($q) {
                                    $q->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
                                    ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%');
                                })->pluck('amount_adjust')->sum();
        $this->prima_cost   = $this->bqElement
                                ->where(function($q) {
                                    $q->where('element','like','%'. 'PRIME' . '%' . ' COST' . '%')
                                    ->orWhere('element','like', '%' . 'KOS' . '%' . 'PRIMA'.'%');
                                })->pluck('amount_adjust')->sum();
    }
    
    public function getReviewDoc($val){
        $reviewD = null;
        if($this->reviewdoc != null){
            foreach($this->reviewdoc as $doc){
                if($doc->document_contract_type != $val){
                    continue;
                }else{
                    $reviewD = $doc;
                }
            }
        }
        return $reviewD;
    }
    
    public function getLastIpcLad(){
        //get last in list
        $beforeIpc = null;
        
        $allcompensation_amountNotZero = [];
        foreach ($this->allIpc as $value){
            if($value->compensation_amount != '' && $value->compensation_amount != 0){
                array_push($allcompensation_amountNotZero,$value);
            }
        }
        
        array_push($allcompensation_amountNotZero,$this->ipc);
        foreach ($allcompensation_amountNotZero as $value) {
           if($value->id == $this->ipc->id){            
               continue;
           }else{
               $beforeIpc = $value;
           }
        }
        
        return $beforeIpc;
        //loop object
        /*
        $last_ipc           = Ipc::where('sst_id', $ipc->sst_id)->
                where('id','<',$ipc->id)->where('deleted_at', null)->where('compensation_amount', '!=', 0)->
                where('compensation_amount', '!=', '')->orderBy('id','desc')->first() ?? 0;
        */
    }
    
    public function getIpc()
    {
        return $this->ipc;
    }
    
    public function getSst()
    {
        $ipc = $this->getIpc();
        
        return $ipc->sst;
    }
    
    public function getAllIpc()
    {
        $ipc        = $this->getIpc();
        if($this->cnt_ipc == null){
            $this->cnt_ipc      = Ipc::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get();
        }
        return $this->cnt_ipc;
    }

    public function getPreIpcNo()
    {
        $ipc        = $this->getIpc();
        $pre_ipcno  = $ipc->ipc_no - 1;
        
        return $pre_ipcno;
    }
    
    public function getTotalInvMain()
    {
        $ipc            = $this->getIpc();
        if($this->inv_total == null){
          $this->inv_total      = $ipc->ipcInvoice->where('deleted_at', null)->where('status_contractor',1)->pluck('invoice')->sum() ?? 0;
        }
        return $this->inv_total;
    }
    
    public function getTotalInvSub()
    {
        $ipc            = $this->getIpc();
        $inv_total      = $ipc->ipcInvoice->where('deleted_at', null)->where('status_contractor',2)->pluck('invoice')->sum() ?? 0;
        
        return $inv_total;
    }
    
    public function getAppointed()
    {
        return $this->appointed;
    }
    
    public static function make(Ipc $ipc)
    {
        return new self($ipc);
    }
    
    public function getAdvAmount()
    {
        $ipc            = $this->getIpc();
        $count_ipc      = $this->getAllIpc();
        $message        = null;
        $adv            = 0;
        $isAllow        = true;
        $check_advance  = $this->getReviewDoc('Wang Pendahuluan');
        
        if(empty($ipc->sst) || (empty($ipc->sst) && ! empty($ipc->sst->deposit)) || (!empty($ipc->sst) && !empty($ipc->sst->deposit) && $ipc->sst->deposit->qualified_amount < 1)){
            $isAllow    = false;
            $message    = 'Tiada wang pendahuluan disimpan dalam dokumen kontrak';
        }
        
        if(!empty($ipc->sst) && ! empty($ipc->sst->deposit) && !empty($ipc->sst->deposit->qualified_amount) && $ipc->sst->deposit->qualified_amount > 0 && (empty($check_advance) || $check_advance == null ) ){
            $isAllow    = false;
            $message    = 'Semakan wang pendahuluan belum selesai';
        }
        
        if(1 != $count_ipc->count()){
            $isAllow    = false;
            $message    = 'IPC ini bukan yang pertama';
        } 
        
        if($isAllow && 0 == $ipc->advance_amount){
            $adv = $ipc->sst->deposit->qualified_amount ?? 0;
        }elseif($isAllow && 0 != $ipc->advance_amount){
            $adv = $ipc->advance_amount ?? 0;
        }
        
        return $adv;
    }
    
    public function getAdvBalikAmount()
    {
        $ipc            = $this->getIpc();
        $count_ipc      = $this->getAllIpc();
        $give_adv       = 0;
        $given_adv      = 0;
        $isAllow        = true;
        $message        = null;
        $show_adv       = null;
        
        if(empty($ipc->sst) || (empty($ipc->sst) && ! empty($ipc->sst->deposit)) || (!empty($ipc->sst) && !empty($ipc->sst->deposit) && $ipc->sst->deposit->qualified_amount < 1)){
            $isAllow    = false;
            $message    = 'Tiada wang pendahuluan disimpan dalam dokumen kontrak';
        }
        
        if($count_ipc->count() < 2){
            $isAllow    = false;
            $message    = 'IPC ini yang pertama';
        } 
        
        if(0 == $this->getPreIpcNo()){
            $isAllow    = false;
            $message    = 'Tiada IPC dibuat sebelum ni';
        }
        
        if($isAllow){
            $contract_sum   = $this->appointed->offered_price ?? 0;
            $advance        = $this->getAllIpc()->pluck('advance_amount')->sum() ?? 0;
            $return_adv     = $this->notCurIpc->pluck('give_advance_amount')->sum() ?? 0;
            // $prov_sum       = $this->bqElement
            //         ->where(function($q) {
            //             $q->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
            //             ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%');
            //         })->pluck('amount_adjust')->sum()   ?? 0;
            // $prima_cost     = $this->bqElement
            //         ->where(function($q) {
            //             $q->where('element','like','%'. 'PRIME' . '%' . ' COST' . '%')
            //             ->orWhere('element','like', '%' . 'KOS' . '%' . 'PRIMA'.'%');
            //         })->pluck('amount_adjust')->sum()     ?? 0;
            $prime          = $this->prima_cost + $this->prov_sum;
            $inv_total      = $this->getTotalInvMain();
            
            $give_adv       = round((((2 * $advance) / ($contract_sum - $prime)) * ($inv_total - (0.25 * ($contract_sum - $prime)))), 2);

            $return = $give_adv + $return_adv;

            if ($advance >= $return) {//check if bayaran balik > than advance
                $given_adv = $give_adv;
            }else{
                $given_adv = $advance - $return_adv;
            }

            if($ipc->last_ipc == 'y'){
                $given_adv = $advance - $return_adv;
            }

            if($given_adv < 0){// resest for the rest adv return after minus all
                $given_adv = 0;
            }

            
            $show_adv       = "wang pendahuluan dilepaskan => = (((2 * wang pendahuluan) / (jumlah nilai kontrak - (jumlah prima + jumlah wang peruntukan))) * (jumlah inbois kontraktor utama - (0.25 * (jumlah nilai kontrak - (jumlah prima + jumlah wang peruntukan))))) = (((2 * ".money()->toCommon($advance,2).") / (".money()->toCommon($contract_sum,2)." - (".money()->toCommon($this->prima_cost,2)." + ".money()->toCommon($this->prov_sum,2)."))) * (".money()->toCommon($inv_total,2)." - (0.25 * (".money()->toCommon($contract_sum,2)." - (".money()->toCommon($this->prima_cost,2)." + ".money()->toCommon($this->prov_sum,2).")))))";
        }
        
        return ['tarik_adv' => $given_adv, 'formula' => $show_adv];
    }
    
    public function getMaterialTahanAmount()
    {
        $ipc                = $this->getIpc();
        $material_tahan     = 0;
        $get_prev_mat       = null;
        if($this->notCurIpc != null){
            foreach($this->notCurIpc as $not){
                if($not->material_on_site_amount != 0 || $not->material_tahanan_amount != 0){
                    $get_prev_mat = $not;
                }else{
                    continue;
                }
            }
        }
        
        
        if (!empty($this->getTotalInvMain()) && ('0' != $ipc->material_tahanan_amount || null != $ipc->material_tahanan_amount)) {
            $material_tahan = $ipc->material_tahanan_amount;
        } elseif (!empty($this->getTotalInvMain()) && ('' == $ipc->material_tahanan_amount || '0' == $ipc->material_tahanan_amount) && 0 != $this->getPreIpcNo()) {
            if (!empty($get_prev_mat) && '0' != $get_prev_mat->material_on_site_amount) {
                $material_tahan = $get_prev_mat->material_on_site_amount;
            }
        }
        
        return $material_tahan;
    }
    
    public function getWjpAmount()
    {
        $ipc            = $this->getIpc();
        $count_ipc      = $this->getAllIpc();
        $get_wjp        = 0;
        $check_wjp      = $this->getReviewDoc('Bon Pelaksanaan');
        $wjp_formula    = null;
        $isAllow        = true;
        $message        = null;
       
        if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->bon) &&  !empty($ipc->sst->bon->bon_type) && 4 == $ipc->sst->bon->bon_type && (empty($check_wjp) || $check_wjp == null)){
            $isAllow    = false;
            $message    = 'Semakan Bon masih belum selesai';
            
        }
        if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->bon) &&  !empty($ipc->sst->bon->bon_type) && 4 != $ipc->sst->bon->bon_type){
//        if(empty($ipc->sst->bon) || (!empty($ipc->sst->bon) && empty($ipc->sst->bon->bon_type)) ||  (!empty($ipc->sst->bon) && empty($ipc->sst->bon->bon_type) && 4 != $ipc->sst->bon->bon_type)){
            $isAllow    = false;
            $message    = 'Bon Pelaksanaan yang dipilih bukan wang jaminan pelaksanaan';
            
        }
        if($count_ipc->count() < 1) {
            $isAllow    = false;
            $message    = 'Bilangan IPC yang dikeluarkan 0 atau -ve';
            
        }
        if('n' != $ipc->last_ipc) {
            $isAllow    = false;
            $message    = 'Jenis ipc yang dipilh adalah ipc terakhir';
            
        }
        
        if($isAllow){
            $total_wjp      = $ipc->sst->bon->money_amount ?? 0;
            $prev_wjp       = $this->notCurIpc->pluck('wjp_amount')->sum() ?? 0;
            $inv_total      = $this->getTotalInvMain();
            $pay_wjp        = 0.1 * ($inv_total + $ipc->material_on_site_amount - $this->getMaterialTahanAmount());
            
            $pay_total = $pay_wjp + $prev_wjp;
            //get supposly wjp for current
            if ($total_wjp >= $pay_total) {//get new pay wjp for this time
                $get_wjp = $pay_wjp;
                $wjp_formula = "wang tahanan => ( 0.1 * (jumlah inbois + jumlah material on site - jumlah penarikan material on site) ) = ( 0.1 * (".money()->toCommon($inv_total,2)." + ".money()->toCommon($ipc->material_on_site_amount,2)." - ".money()->toCommon($this->getMaterialTahanAmount(),2)." ) )";
            } else {//get remaining pay wjp
                $get_wjp = $total_wjp - $prev_wjp;
                $wjp_formula = "wang tahanan => ( jumlah wang tahanan pelaksanaan - jumlah wang tahanan ) = ( ".money()->toCommon($total_wjp,2)." - ".money()->toCommon($prev_wjp,2)." )";
            }
            if($get_wjp < 0){// resest for the rest wjp after minus
                $get_wjp = 0;
            }
            if(0 != $ipc->wjp_amount && $ipc->wjp_amount == $get_wjp){// check kemaskini ipc yg ada wjp
                $get_wjp = $ipc->wjp_amount;
                $wjp_formula = "wang tahanan => ( 0.1 * (jumlah inbois + jumlah material on site - jumlah penarikan material on site) ) = ( 0.1 * (".money()->toCommon($inv_total,2)." + ".money()->toCommon($ipc->material_on_site_amount,2)." - ".money()->toCommon($this->getMaterialTahanAmount(),2)." ) )";
            }
            
        }
        
        return ['wjp' => $get_wjp, 'formula' => $wjp_formula];
    }
    
    public function getWjpLepasAmount()
    {
        $ipc            = $this->getIpc();
        $curr_wjp       = $this->getAllIpc()->pluck('wjp_amount')->sum() ?? 0;
        $curr_wjp_lepas = $this->notCurIpc->pluck('wjp_lepas_amount')->sum() ?? 0;
        $curr           = (!empty($ipc->evaluation_at))? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->evaluation_at):Carbon::now();
        $cpc_date       = (! empty($ipc->sst->cpc) && ! empty($ipc->sst->cpc->signature_date))? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->sst->cpc->signature_date):null;
        $formula        = null;
        $wjp_lepas      = 0;
        $new_lepas1     = $curr_wjp - $curr_wjp_lepas;
        $new_lepas2     = $curr_wjp * 0.5;
        
        
        if ('y' == $ipc->last_ipc && $curr_wjp > 0 && ('0' == $ipc->wjp_lepas_amount || $new_lepas1 != $ipc->wjp_lepas_amount) ) {//if last ipc and cpc xbuka lagi(tapi dah filter kat view last ipc open after cmgd)
            $wjp_lepas  = $curr_wjp - $curr_wjp_lepas;
            $formula    = "wang tahanan dilepaskan => (jumlah wang tahanan - jumlah wang tahanan dilepaskan) = ( ".money()->toCommon($curr_wjp,2)." - ".money()->toCommon($curr_wjp_lepas,2)." )";
        }elseif(! empty($cpc_date) && ! empty($curr) &&  $curr >= $cpc_date && 'n' == $ipc->last_ipc && $curr_wjp > 0 && $new_lepas2 != $ipc->wjp_lepas_amount){//for the first time nak lepas wjp
            $wjp_lepas  = $curr_wjp * 0.5;
            $formula    = "wang tahanan dilepaskan => (0.5 * jumlah wang tahanan) = ( 0.5 * ".money()->toCommon($curr_wjp,2).")";
        }elseif($ipc->wjp_lepas_amount > 0){//for kemaskini ipc
            $wjp_lepas  = $ipc->wjp_lepas_amount;
            $formula    = "wang tahanan dilepaskan => (0.5 * jumlah wang tahanan) = ( 0.5 * ".money()->toCommon($curr_wjp,2).")";
        }
        return ['wjp' => $wjp_lepas, 'formula' => $formula];
    }
    
    public function getLadAmount()
    {
        $ipc                = $this->getIpc();
        $curr               = (!empty($ipc->evaluation_at))? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->evaluation_at):Carbon::now();
        $cpc_date           = (! empty($ipc->sst->cpc) && ! empty($ipc->sst->cpc->signature_date))? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->sst->cpc->signature_date):null;
    
        $sst_end            = (! empty($ipc->sst) && ! empty($ipc->sst->end_working_date)) ? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->sst->end_working_date) : null;
        $eot_end            = (! empty($ipc->sst) && ! empty($ipc->sst->eots) && ! empty($ipc->sst->eots->pluck('eot_approve')->first())) ? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->sst->eots->pluck('eot_approve')->pluck('approved_end_date')->first()) : null;
        $contract_value     = $this->appointed->offered_price ?? 0;
        $cpo_value          = (! empty($ipc->sst->cpo)) ? $ipc->sst->cpo->cpo_amount : 0;
        $rate_blr           = money()->toCommon($ipc->sst->document_blr ?? 0);                                                                                                                                         
//        $last_ipc           = Ipc::where('sst_id', $ipc->sst_id)->where('id','<',$ipc->id)->where('deleted_at', null)->where('compensation_amount', '!=', 0)->where('compensation_amount', '!=', '')->orderBy('id','desc')->first() ?? 0;
        $last_ipc           = $this->getLastIpcLad();
//        dd($last_ipc);
        $check_cpo_review   = $this->getReviewDoc('CPO');
        $check_cnc_review   = $this->getReviewDoc('CNC');
        $cnc_date           = (! empty($ipc->sst->cnc) && !empty($check_cnc_review) && $check_cnc_review != null)? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->sst->cnc->signature_date):null;
        $cpo_date           = (! empty($ipc->sst->cpo) && !empty($check_cpo_review) && $check_cpo_review != null)? Carbon::createFromFormat('Y-m-d H:i:s', $ipc->sst->cpo->agreement_date):null;
        $lad_value          = 0;
        $lad_day            = 0;
        $lad_rate           = 0;
        $lad_cpo_per_day    = 0;
        $lad_clean_per_day  = 0;
        $cnt_cpo_day        = 1;
        $cnt_clean_day      = 1;
        $lad_formula        = null;
        //sst date must greater than tarikh penilaian
//        dd($check_cnc_review);
        if(!empty($sst_end) && $sst_end != null && ($curr > $sst_end || $ipc->penalty != null) && ((!empty($check_cpo_review) && $check_cpo_review != null) || (!empty($check_cnc_review) && $check_cnc_review != null) ))
        {
            //count lad per day
            if (!empty($cpo_value) && $cpo_value > 0) {
                $lad_cpo_per_day = round((($rate_blr/100) * ( $cpo_value/365 )));
            } else {
                $lad_clean_per_day = round((($rate_blr/100) * ( $contract_value/365 )));
            }
            //count lad per day
            
            //count day lad
            if(!empty($cpo_date) && $cpo_date != null){
                if(!empty($last_ipc) && $last_ipc != null) {
                    $cnt_cpo_day = $curr->diffInDays($last_ipc->evaluation_at);
                }elseif(!empty($eot_end) && $eot_end != null){
                    $cnt_cpo_day = $curr->diffInDays($eot_end);
                }else{
                    $cnt_cpo_day = $curr->diffInDays($cpo_date);
                }
            }
            if(!empty($cnc_date) && $cnc_date != null){
                if(!empty($last_ipc) && $last_ipc != null) {
                    $cnt_clean_day = $curr->diffInDays($last_ipc->evaluation_at);
                }elseif(!empty($eot_end) && $eot_end != null){
                    $cnt_clean_day = $curr->diffInDays($eot_end);
                }else{
                    $cnt_clean_day = $curr->diffInDays($cnc_date);
                }
            }
            if(empty($cpo_date) && $cpo_date == null && empty($cnc_date) && $cnc_date == null && !empty($cpc_date) && $cpc_date != null){
                $cnt_clean_day = $curr->diffInDays($sst_end);
            }
            
            $lad_day     = $cnt_cpo_day + $cnt_clean_day;
            $lad_rate    = $lad_clean_per_day ?? $lad_cpo_per_day;
            $lad_value   = ($lad_cpo_per_day * $cnt_cpo_day) + ($lad_clean_per_day * $cnt_clean_day); 
            $lad_formula = "ganti rugi =>( (kadar sehari * jumlah hari lewat) + (kadar sehari selepas cpo * jumlah hari lewat selepas cpo) ) = ( ( ".money()->toCommon($lad_clean_per_day,2)." * ".money()->toCommon($cnt_clean_day,2)." ) + ( ".money()->toCommon($lad_cpo_per_day,2).' * '.money()->toCommon($cnt_cpo_day,2).' ) )';
        }
        return ['value'=>$lad_value, 'day'=>$lad_day, 'rate'=>$lad_rate, 'formula'=> $lad_formula];

    }
    
    public function getDemandAmount()
    {
        $ipc        = $this->getIpc();
        $lad        = $this->getLadAmount();
        $advance    = $this->getAdvAmount();
        $adv_balik  = $this->getAdvBalikAmount();
        $wjp        = $this->getWjpAmount();
        $wjp_lepas  = $this->getWjpLepasAmount();
        $lad_val    = 0;
        
        if($lad["value"] == 0 || $lad["value"] == null){
            $lad_val = $ipc->compensation_amount ?? 0;
        }else{
            $lad_val = $lad["value"];
        }
        $demand = $this->getTotalInvSub() + $this->getTotalInvMain() + $ipc->material_on_site_amount - $this->getMaterialTahanAmount() 
                + $advance - $adv_balik["tarik_adv"] - $wjp["wjp"]
                + $wjp_lepas["wjp"] - $ipc->credit - $lad_val + 
                $ipc->working_change_amount;
        
        $formula = 'jumlah yang perlu dibayar => jumlah inbois kontraktor utama + jumlah inbois subkontraktor + jumlah material on site - jumlah penarikan material on site + jumlah wang pendahuluan - jumlah tahanan wang pendahuluan - jumlah wang tahanan + jumlah wang tahanan dilepaskan - jumlah nota kredit - jumlah wang ganti rugi + jumlah perubahan kerja <br>'
                   .' = ( '.money()->toCommon($this->getTotalInvSub(),2).' + '.money()->toCommon($this->getTotalInvMain(),2).' + '.money()->toCommon($ipc->material_on_site_amount,2).' - '.money()->toCommon($this->getMaterialTahanAmount(),2).' + '.money()->toCommon($advance,2).' - '.money()->toCommon($adv_balik["tarik_adv"],2).' - '.money()->toCommon($wjp["wjp"],2).' + '.money()->toCommon($wjp_lepas["wjp"],2).' - '.money()->toCommon($ipc->credit,2).' - '.money()->toCommon($lad_val,2).' + '.money()->toCommon($ipc->working_change_amount,2).' )';
        
        return ['demand' => $demand,'formula' => $formula];
    }
    
    public function getNewContractValue()
    {
        $ipc        = $this->getIpc();
        $check_ipc  = null;
        if(!empty($this->allIpc)){
            foreach($this->allIpc as $all){
                if($all->new_contract_amount != null && $all->new_contract_amount != 0){
                    $check_ipc = $all;
                }else{
                    continue;
                }
            }
        }
        $new_value  = null;
        if(!empty($check_ipc) && $check_ipc->count() > 0){
            $new_value = $check_ipc->latest()->first()->new_contract_amount;
        } 
        
        return $new_value;
    }

    public function getVoCancel(){
        $vo_cancel = null;
        if(!empty($this->ipc->sst->ppjhk_active) && $this->ipc->sst->ppjhk_active->count() > 0){
            foreach($this->ipc->sst->ppjhk_active as $ppjhk){
                if($ppjhk->status == '1'){
                    if(!empty($ppjhk->cancels) && !empty($ppjhk->cancels->where('ppjhk_status',1)->where('status',1)) && $ppjhk->cancels->where('ppjhk_status',1)->where('status',1)->count() > 0){
                        $vo_cancel = $ppjhk->cancels->where('ppjhk_status',1)->where('status',1)->sum('amount');
                    }
                }
            }
        }
        return $vo_cancel;
    }

    public function getPreVo(){
        $prev_vo = null;
        if($this->notCurIpc != null){
            foreach($this->notCurIpc as $not){
                if($prev_vo != null){
                    $prev_vo = $prev_vo + $not->working_change_amount;
                }else{
                    $prev_vo = $not->working_change_amount;
                }
            }
        }
        return $prev_vo;
    }
}
