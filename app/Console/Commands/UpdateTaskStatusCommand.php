<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateTaskStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:update {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tasks based on given model FQCN.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->argument('model');

        if (! class_exists($model)) {
            $this->error('No model found');

            return;
        }

        $this->info('Total tasks: ' . \App\Models\Task::where('taskable_type', $model)->count());
        $this->info('Total tasks is done: ' . \App\Models\Task::where('taskable_type', $model)->whereIsDone(true)->count());
        $this->info('Total tasks is not done: ' . \App\Models\Task::where('taskable_type', $model)->whereIsDone(false)->count());

        // \App\Models\Task::where('taskable_type', $model)->whereIsDone(false)->get();
    }
}
