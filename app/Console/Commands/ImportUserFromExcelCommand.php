<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportUserFromExcelCommand extends Command
{
    public $fields = [
        'nama',
        'gelatan_jawatan',
        'ringkasan_gelaran_jawatan',
        'skim',
        'gred_jawatan',
        'jabatanagensi',
        'bahagian',
        'seksyenunit',
        'no_telefon_bimbit',
        'no._tel._pejabat_603_8887_xxxx',
        'no._faks',
        'alamat_emel',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:users {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import users from an excel file';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file_name = storage_path('app/data/' . $this->argument('file'));

        if (! file_exists($file_name)) {
            $this->error('File not exist');

            return;
        }

        $this->info('Importing users from ' . $this->argument('file'));

        \Excel::filter('chunk')
            ->load($file_name)
            ->chunk(20, function ($users) {
                $json = str_replace(
                    ['\u00a0', 'no._tel._pejabat_603_8887_xxxx', 'no._faks'],
                    ['', 'phone_office', 'phone_fax'],
                    json_encode($users->toArray()));
                $users = json_decode($json);

                foreach ($users as $staff) {
                    $user = \App\Models\User::updateOrCreate([
                        'email' => $staff->alamat_emel,
                    ], [
                        'name'     => title_case($staff->nama),
                        'password' => bcrypt('password'),
                    ]);

                    $department = \App\Models\Department::where('code', $staff->kod_jabatan)->first();

                    if ($department) {
                        $user->department_id = $department->id;
                    }

                    $scheme = \App\Models\Scheme::where('name', $staff->skim)->first();

                    if ($scheme) {
                        $user->scheme_id = $scheme->id;
                    }

                    $grade = \App\Models\Grade::where(
                        'name', str_replace($staff->skim, '', $staff->gred_jawatan)
                    )->first();

                    if ($grade) {
                        $user->grade_id = $grade->id;
                    }

                    $position = \App\Models\Position::where('name', trim($staff->gelatan_jawatan))->first();

                    if ($position) {
                        $user->position_id = $position->id;
                    }

                    $user->save();

                    if ($user->grade_id > 44) {
                        $user->syncRoles(['user', 'administrator']);
                    } else {
                        $user->syncRoles(['user']);
                    }
                    
                    //add for admin
                    if($user->name == 'administrator@ppj.gov.my'){
                         $user->syncRoles(['administrator']);
                    }

                    // FAILED at Serialization level.
                    // @todo Refactor this
                    if (! empty($staff->phone_office)) {
                        // $user()->phones()->create([
                        //     'phone_type_id' => \CleaniqueCoders\Profile\Models\PhoneType::OFFICE,
                        //     'phone_number'  => '6038887' . $staff->phone_office,
                        // ]);
                    }

                    if (! empty($staff->phone_fax)) {
                        // $user()->phones()->create([
                        //     'phone_type_id' => \CleaniqueCoders\Profile\Models\PhoneType::OTHER,
                        //     'phone_number'  => preg_replace('/[^0-9]/im', '', $staff->phone_fax),
                        // ]);
                    }
                }
            }, 'UTF-8');
    }
}
