<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class ReloadAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reload:admin_acl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reload Admin Configuration Role Setting';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //ROLE
        //administrator
        //developer
        //pengesah
        //penyedia
        //penyemak
        //urusetia
        //user
        //ketuajabatan
        //kaunter
        
       // bawak all user yang terlibat, add role
        \App\Models\User::where('email', '=', 'administrator@ppj.gov.my')->get()->each->syncRoles(['administrator']);
        \App\Models\User::where('email', '=', 'developer@ppj.gov.my')->get()->each->syncRoles(['developer']);
        
        
        \App\Models\User::where('email', '=', 'shukri@ppj.gov.my')->get()->each->syncRoles(['ketuajabatan']);
        \App\Models\User::where('email', '=', 'suhaila@ppj.gov.my')->get()->each->syncRoles(['pengesah']);
        \App\Models\User::where('email', '=', 'aminudin@ppj.gov.my')->get()->each->syncRoles(['pengesah'],['penyemak']);
        \App\Models\User::where('email', '=', 'sarah_m@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'syuhadaz@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'saiful.deen@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'khairunnisa@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'roziah@ppj.gov.my')->get()->each->syncRoles(['pengesah']);
        \App\Models\User::where('email', '=', 'rohani@ppj.gov.my')->get()->each->syncRoles(['pengesah'],['penyemak']);
        \App\Models\User::where('email', '=', 'muhammad.fadzli@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['penyedia']);
        \App\Models\User::where('email', '=', 'zuraida@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'm_sufian@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'athira@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'nor.aisyam@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'noor.aishah@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['penyedia']);
        \App\Models\User::where('email', '=', 'm_noor@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['penyedia']);
        \App\Models\User::where('email', '=', 'faiz@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'affendy@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'sitisarah@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'mazian@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'baizura@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter'],['urusetia']);
        \App\Models\User::where('email', '=', 'irffan.yusof@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']);
        \App\Models\User::where('email', '=', 'noryana@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']); 
        \App\Models\User::where('email', '=', 'ahmad.faizal@ppj.gov.my')->get()->each->syncRoles(['urusetia']);
        \App\Models\User::where('email', '=', 'ahmad.fazila@ppj.gov.my')->get()->each->syncRoles(['penyemak'],['kaunter']); 
        \App\Models\User::where('email', '=', 'kamaru@ppj.gov.my')->get()->each->syncRoles(['pengesah']); 
        \App\Models\User::where('email', '=', 'arni@ppj.gov.my')->get()->each->syncRoles(['penyemak']);  
        \App\Models\User::where('email', '=', 'intan@ppj.gov.my')->get()->each->syncRoles(['penyedia']);
        \App\Models\User::where('email', '=', 'jalani@ppj.gov.my')->get()->each->syncRoles(['pengesah']);
        \App\Models\User::where('email', '=', 'mohdnor@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'mohdnor@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'halina.majmin@ppj.gov.my')->get()->each->syncRoles(['penyedia']);
        \App\Models\User::where('email', '=', 'suhara@ppj.gov.my')->get()->each->syncRoles(['urusetia']);  
        \App\Models\User::where('email', '=', 'hilmi@ppj.gov.my')->get()->each->syncRoles(['pengesah']);   
        \App\Models\User::where('email', '=', 'nur.azlina@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'zulie@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'azura@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
        \App\Models\User::where('email', '=', 'nasron@ppj.gov.my')->get()->each->syncRoles(['penyemak']);
     
        
    }

    private function syncUserRoles()
    {
        /*
        \App\Models\User::whereHas('grade', function ($query) {
            $query->where('name', '=<', 44);
        })->get()->each->syncRoles(['user']);

        \App\Models\User::whereHas('grade', function ($query) {
            $query->where('name', '>', 44);
        })->get()->each->syncRoles(['user', 'administrator']);
         * */
         
    }
}
