<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncFigureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:figure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync all information to figure reporting tables';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        
        
        //DIN delete all data from figure table
        \App\Models\Report\Figure::truncate();
        
        $this->syncHome();
        $this->syncUsers();
        $this->syncCompany();
        $this->syncAcquisition();
        $this->syncSale();
        $this->syncMonitoring();
    }

    private function syncHome()
    {
        $data = [
            [
                'category' => 'home',
                'header'   => date('Y'),
                'icon'     => '',
                'content'  => date('d'),
                'footer'   => date('M'),
            ],
        ];

        $this->syncData('home', $data);
    }

    private function syncUsers()
    {
        $lastLoggedIn = \App\Models\User::lastLoggedIn();
        $newestUser   = \App\Models\User::newestUser();

        $data = [
            [
                'category' => 'user',
                'header'   => __('Pengguna'),
                'icon'     => '',
                //'content'  => \App\Models\User::count(),
                //add user only has roles is counted
                'content'  => \App\Models\User::whereHas('roles')->count(),
                'footer'   => __('Jumlah'),
            ],
            [
                'category' => 'user',
                'header'   => __('Log Masuk Terbaru'),
                'icon'     => '',
                'content'  => '' == $lastLoggedIn['name'] ? 'Tiada Maklumat' : $lastLoggedIn['name'],
                'footer'   => '-' == $lastLoggedIn['datetime'] ? 'Tiada Maklumat' : __('pada ') . $lastLoggedIn['datetime'],
            ],
            [
                'category' => 'user',
                'header'   => __('Pengguna Terbaru'),
                'icon'     => '',
                'content'  => $newestUser->name,
                'footer'   => __('daftar pada ') . $newestUser->created_at->format(config('datetime.dashboard')),
            ],
        ];

        $this->syncData('user', $data);
    }

    private function syncCompany()
    {
        $newestCompany = \App\Models\Company\Company::where('company_name','!=',null)->where('deleted_at',null)->latest()->first();
        $company_name  = $newestCompany ? $newestCompany->company_name : '-';
        $created_at    = $newestCompany ? __('didaftar pada ') . $newestCompany->created_at->format(config('datetime.dashboard')) : '';

        $data = [
            [
                'category' => 'company',
                'header'   => __('Syarikat Berdaftar'),
                'icon'     => '',
                'content'  => \App\Models\Company\Company::where('company_name','!=',null)->where('deleted_at',null)->count(),
                'footer'   => __('Jumlah'),
            ],
            [
                'category' => 'company',
                'header'   => __('Syarikat Terbaru'),
                'icon'     => '',
                'content'  => $company_name,
                'footer'   => $created_at,
            ],
        ];

        $this->syncData('company', $data);
    }

    private function syncAcquisition()
    {
        $data = [
            [
                'category' => 'acquisition',
                'header'   => __('Kelulusan Perolehan'),
                'icon'     => '',
                'content'  => \App\Models\Acquisition\Approval::whereYear('created_at', date('Y'))->count(),
                'footer'   => __('Jumlah bagi tahun ' . date('Y')),
            ],
            [
                'category' => 'acquisition',
                'header'   => __('Perolehan'),
                'icon'     => '',
                'content'  => \App\Models\Acquisition\Acquisition::whereYear('created_at', date('Y'))->count(),
                'footer'   => __('Jumlah bagi tahun ' . date('Y')),
            ],
            [
                'category' => 'acquisition',
                'header'   => __('Kelulusan Perolehan'),
                'icon'     => '',
                'content'  => \App\Models\Acquisition\Approval::count(),
                'footer'   => __('Jumlah Keseluruhan'),
            ],
            [
                'category' => 'acquisition',
                'header'   => __('Perolehan'),
                'icon'     => '',
                'content'  => \App\Models\Acquisition\Acquisition::count(),
                'footer'   => __('Jumlah Keseluruhan'),
            ],
        ];

        $this->syncData('acquisition', $data);
    }

    private function syncSale()
    {
        $data = [
            [
                'category' => 'sale',
                'header'   => __('Jualan Keseluruhan'),
                'icon'     => '',
                'content'  => money()->toHuman(\App\Models\Acquisition\Acquisition::totalSalesAmount()),
                'footer'   => __('Jumlah'),
            ],
            [
                'category' => 'sale',
                'header'   => __('Jualan bagi ' . date('Y')),
                'icon'     => '',
                'content'  => money()->toHuman(\App\Models\Acquisition\Acquisition::totalSalesAmount(date('Y'))),
                'footer'   => __('Jumlah'),
            ],
            [
                'category' => 'sale',
                'header'   => __('Jualan bagi ' . date('M/Y')),
                'icon'     => '',
                'content'  => money()->toHuman(\App\Models\Acquisition\Acquisition::totalSalesAmount(date('Y'), date('m'))),
                'footer'   => __('Jumlah'),
            ],
        ];

        $this->syncData('sale', $data);
    }

    private function syncMonitoring()
    {
        //TODO DIN Monitoring : Need to modified
        $exceeds    = \App\Processors\Charts\ProjectProgressProcessor::make()->getPaymentProgressMoreThanFiftyThanPhysicalProgressPercent();
        $six_months = \App\Processors\Charts\ProjectProgressProcessor::make()->contractsSixMonthToEnd();
        
        //Senarai On-going 
        $contractsOnGoing = \App\Processors\Charts\ProjectProgressProcessor::make()->contractsOnGoing();
        
        //TODO Senarai Kontrak blm tandatangan contractsNotSignedYet()
        $contractsNotSignedYet = \App\Processors\Charts\ProjectProgressProcessor::make()->contractsNotSignedYet();
        
        //TODO Senarai kontrak telah tamat, sofa blm sedia - check kat table EOT if ada lanjutan  contractsDoneButNotSofaYet()
        $contractsDoneButNotSofaYet = \App\Processors\Charts\ProjectProgressProcessor::make()->contractsDoneButNotSofaYet();
        
        //TODO Senarai projek blm award contractsNotAwardYet()
        $contractsNotAwardYet = \App\Processors\Charts\ProjectProgressProcessor::make()->contractsNotAwardYet();
        
        //TODO - query directly to easy seperate value for role
        
        
        $data       = [
            [
                'category'      => 'project-monitoring',
                'header'        => '',
                'icon'          => '',
                'content_color' => 'primary',
                'content'       => $six_months->count(),
                'footer'        => 'Jumlah Kontrak akan Tamat dalam Tempoh 6 Bulan',
                //'data'          => $six_months,
                'data'          => null,
            ],
            /*
            [
                'category'      => 'project-monitoring',
                'header'        => '',
                'icon'          => '',
                'content_color' => 'primary',
                'content'       => $exceeds->count(),
                'footer'        => 'Jumlah Progres Bayaran Melebihi 50% dari Progres Kemajuan Fizikal',
                //'data'          => $exceeds,
                'data'          => null,
            ], */
            [
                'category'      => 'project-monitoring',
                'header'        => '',
                'icon'          => '',
                'content_color' => 'primary',
                'content'       => $contractsOnGoing->count(),
                'footer'        => 'Kontrak Sedang Berjalan',
                //'data'          => $contractsOnGoing,
                'data' => null,
            ],
            
            [
                'category'      => 'project-monitoring',
                'header'        => '',
                'icon'          => '',
                'content_color' => 'primary',
                'content'       => $contractsNotSignedYet->count(),
                'footer'        => 'Senarai Perolehan Belum Di Tandatangan',
                //'footer'        => user()->department->id,
                //'data'          => $contractsNotSignedYet,
                'data'          => null,
                
            ],
            [
                'category'      => 'project-monitoring',
                'header'        => '',
                'icon'          => '',
                'content_color' => 'primary',
                'content'       => $contractsDoneButNotSofaYet->count(),
                'footer'        => 'Kerja Telah Siap Tetapi Belum Di SOFA',
                //'data'          => $contractsDoneButNotSofaYet,
                'data'          => null,
            ],
            [
                'category'      => 'project-monitoring',
                'header'        => '',
                'icon'          => '',
                'content_color' => 'primary',
                'content'       => $contractsNotAwardYet->count(),
                'footer'        => 'Kontrak Belum Di Award Lagi',
                //'data'          => $contractsNotAwardYet,
                'data'          => null,
            ],
        ];

        $this->syncData('project-monitoring', $data);
    }

    private function syncData($category, $data)
    {
        foreach ($data as $datum) {
            \App\Models\Report\Figure::updateOrCreate(
                collect($datum)->only('category', 'header', 'icon', 'content_color', 'footer')->toArray(),
                collect($datum)->only('content', 'data')->toArray()
            );
        }
    }
}
