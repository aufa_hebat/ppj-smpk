<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportCompBaruCommand extends Command
{
    public $fields    = ['no_ssm', 'no_cidb', 'no_mof', 'syarikat', 'alamat', 'alamat2', 'poskod', 'daerah', 'negeri', 'no_tel_pej', 'no_faks', 'no_tel', 
        'email', 'pemilik1', 'pemilik2', 'ic_pemilik1', 'ic_pemilik2', 'gred'];
    public $companies = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:compBaru {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Company Baru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file_name = storage_path('app/data/' . $this->argument('file'));

        if (! file_exists($file_name)) {
            $this->error('File not exist');

            return;
        }

        \Excel::filter('chunk')->load($file_name)->chunk(20, function ($reader) {
            $reader->each(function ($sheet) {
                $sheet->each(function ($row) {
                    
                    $comp = \App\Models\Company\Company::updateOrCreate([
                        'company_name'      => strtoupper(str_replace("'", '`', $row->syarikat)),
                        'ssm_no'            => $row->no_ssm,
                        'cidb_no'           => $row->no_cidb,
                        'mof_no'            => $row->no_mof,
                        'email'             => $row->email,
                    ]);
                    if($comp){
                        \App\Models\Company\Company::find($comp->id)->addresses()->create([
                            'primary'   => strtoupper(str_replace("'", '`', $row->alamat)), 
                            'secondary' => strtoupper(str_replace("'", '`', $row->alamat2)), 
                            'postcode'  => str_replace("'", '`', $row->poskod), 
                            'city'      => strtoupper(str_replace("'", '`', $row->daerah)),
                            'state'     => strtoupper($row->negeri),
                        ]);

                        if (! empty($row->no_tel)) {
                            \App\Models\Company\Company::find($comp->id)->phones()->updateOrCreate(['phone_type_id' => '3', 'phone_number' => $row->no_tel]);
                        }
                        if (! empty($row->no_faks)) {
                            \App\Models\Company\Company::find($comp->id)->phones()->updateOrCreate(['phone_type_id' => '4', 'phone_number' => $row->no_faks]);
                        }
                        if (! empty($row->no_tel_pej)) {
                            \App\Models\Company\Company::find($comp->id)->phones()->updateOrCreate(['phone_type_id' => '1', 'phone_number' => $row->no_tel_pej]);
                        }
                        
                        if(!empty($row->pemilik1)){
                            \App\Models\Company\Owner::create(['name' => $row->pemilik1, 'ic_number' => $row->ic_pemilik1, 'company_id' => $comp->id]);
                        }
                        if(!empty($row->pemilik2)){
                            \App\Models\Company\Owner::create(['name' => $row->pemilik2, 'ic_number' => $row->ic_pemilik2, 'company_id' => $comp->id]);
                        }
                        
                        if (! empty($row->gred)) {
                            $grd             = new \App\Models\Company\CIDBGrade;
                            $grd->company_id = $comp->id;
                            $grd->grades     = $row->gred;
                            $grd->user_id    = 1;
                            $grd->save();
                        }
                    }
                    
                });
            });
        });
        

    }
}
