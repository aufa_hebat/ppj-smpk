<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SeedTestDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed test data';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('db:seed', ['--class' => 'TestSeeder']);
    }
}
