<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeReferenceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:ref {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new reference setup - model, migration, seeder and test';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('make:model', [
            'name'        => $this->argument('name'),
            '--migration' => true,
        ]);

        $this->call('make:seeder', [
            'name' => class_basename($this->argument('name')) . 'TableSeeder',
        ]);

        $this->call('make:test', [
            'name'   => class_basename($this->argument('name')) . 'Test',
            '--unit' => true,
        ]);
    }
}
