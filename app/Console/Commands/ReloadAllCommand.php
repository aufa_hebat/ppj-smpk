<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReloadAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reload:all
                                {--t|test : Seed test data}
                                {--d|dev : Seed development data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reload all caches and database.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('reload:cache');
        $this->call('passport:install', ['--force' => true]);
        $this->call('storage:link');
        $this->call('reload:db');
        $this->call('import:users', ['file' => 'Senarai-Kakitangan-PPJ.xlsx']);
        $this->call('command:compBaru', ['file' => 'Senarai-Syarikat-Existing.xlsx']);
        $this->call('reload:acl');

        if ($this->option('test')) {
            $this->call('seed:test');
        }

        if ($this->option('dev')) {
            $this->call('seed:dev');
        }

        if ($this->option('test') || $this->option('dev')) {
            $this->call('sync:figure');
            $this->call('sync:chart');
        }

        $this->call('reload:cache');
    }
}
