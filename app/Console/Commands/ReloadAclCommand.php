<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class ReloadAclCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reload:acl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reload ACL Configuration';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tables = config('permission.table_names');
        Schema::disableForeignKeyConstraints();
        foreach ($tables as $key => $table) {
            \DB::table($table)->truncate();
        }
        Schema::enableForeignKeyConstraints();

        $this->call('db:seed', [
            '--class' => 'RolesAndPermissionsSeeder',
        ]);

        $this->syncUserRoles();
    }

    private function syncUserRoles()
    {
        /*
        \App\Models\User::whereHas('grade', function ($query) {
            $query->where('name', '=<', 44);
        })->get()->each->syncRoles(['user']);

        \App\Models\User::whereHas('grade', function ($query) {
            $query->where('name', '>', 44);
        })->get()->each->syncRoles(['user', 'administrator']);
         * */
         
    }
}
