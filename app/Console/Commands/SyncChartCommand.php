<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncChartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:chart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync all information to chart reporting tables';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //DIN : delete all data from db first before sync new data
        \App\Models\Report\Chart::truncate();
        
        $this->syncAcquisition();
        $this->syncProjectMonitoring();
    }

    private function syncAcquisition()
    {
        $this->syncAcquisitionByDepartment();
    }

    private function syncProjectMonitoring()
    {
        $this->syncProjectContractsSixMonthToEnd();
        $this->syncProjectAwardedCompaniesByDepartment();
    }

    private function syncProjectContractsSixMonthToEnd()
    {
        /**
         * Deparment Doughnut Chart.
         */
        $departments = [];
        $colors      = [];

        //$data = \App\Models\Acquisition\Sst::contractsSixMonthToEnd()
        
        $data = \App\Models\Acquisition\Sst::where('end_working_date', '>=', now())
            ->where('end_working_date', '<=', now()->addMonths(6))
            ->get()
            ->groupBy('department_id')
            ->mapWithKeys(function ($acq) {
                return [$acq->first()->department->name => $acq->count()];
            });

        $data->each(function ($datum, $key) use (&$departments, &$colors) {
            $colors[] = departments()->where('name', $key)->first()->color;
            $departments[] = $key;
        });
        
        if(count($data) == 0){
           return null;
        }

        return $this->syncData(
            'project-contract-six-month-to-end',
            'doughnut',
            'Jumlah Kontrak akan Tamat dalam Tempoh 6 Bulan Mengikut Jabatan',
            $departments,
            [
                [
                    'data'            => $data->values()->all(),
                    'backgroundColor' => $colors,
                    'label'           => 'Dataset Perolehan Mengikut Jabatan',
                ],
            ],
            'bottom'
        );
    }

    private function syncProjectAwardedCompaniesByDepartment()
    {
        try {
        $departments = [];
        $colors      = [];

        $data = \App\Models\Acquisition\AppointedCompany::withDetails()
            ->get()
            ->groupBy('acquisition.department_id')
            ->mapWithKeys(function ($acq) {
                return [$acq->first()->acquisition->department->name => $acq->count()];
            });
            
        if(count($data) == 0){
           return null;
        }
            
        $data->each(function ($datum, $key) use (&$departments, &$colors) {
            $colors[] = departments()->where('name', $key)->first()->color;
            $departments[] = $key;
        });

        
        return $this->syncData(
            'project-awarded-company-by-department',
            'doughnut',
            'Jumlah Perolehan Yang Dianugerahkan Mengikut Jabatan',
            $departments,
            [
                [
                    'data'            => $data->values()->all(),
                    'backgroundColor' => $colors,
                    'label'           => 'Dataset Perolehan Yang Dianugerahkan Mengikut Jabatan',
                ],
            ],
            'bottom'
        );
        } catch (Exception $e) {
            return null;
        }
    }

    private function syncAcquisitionByDepartment()
    {
        $departments = [];
        $colors      = [];

        $data = \App\Models\Acquisition\Acquisition::chartGroupByDepartment();
        
        if(count($data) == 0){
           return null;
        }

        $data->each(function ($datum, $key) use (&$departments, &$colors) {
            $colors[] = departments()->where('name', $key)->first()->color;
            $departments[] = $key;
        });

        return $this->syncData(
            'acquisition-by-department',
            'doughnut',
            'Perolehan Mengikut Jabatan',
            $departments,
            [
                [
                    'data'            => $data->values()->all(),
                    //'data'            => $data,
                    'backgroundColor' => $colors,
                    'label'           => 'Dataset Perolehan Mengikut Jabatan',
                ],
            ],
            'bottom'
        );
    }

    private function syncData($category, $type, $title, $labels, $datasets, $legend_position)
    {
        $data = [
            'type' => $type,
            'data' => [
                'datasets' => $datasets,
                'labels'   => $labels,
            ],
            'options' => [
                'responsive' => true,
                'legend'     => [
                    'position' => $legend_position,
                ],
                'title' => [
                    'display' => true,
                    'text'    => title_case($title),
                ],
                'animation' => [
                    'animateScale'  => true,
                    'animateRotate' => true,
                ],
            ],
        ];
        \App\Models\Report\Chart::updateOrCreate([
            'category'        => $category,
            'type'            => $type,
            'title'           => $title,
            'labels'          => $labels,
            'legend_position' => $legend_position,
        ], [
            'datasets' => $datasets,
            'data'     => $data,
        ]);
    }
}
