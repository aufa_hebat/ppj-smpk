<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportCompanyFromExcelCommand extends Command
{
    public $fields    = ['vendor', 'name_1'];
    public $companies = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:companies {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import companies form an excel file';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file_name = storage_path('app/data/' . $this->argument('file'));

        if (! file_exists($file_name)) {
            $this->error('File not exist');

            return;
        }

        \Excel::filter('chunk')->load($file_name)->chunk(20, function ($reader) {
            $reader->each(function ($sheet) {
                $sheet->each(function ($row) {
                });
            });
        });
        // check file existence
        // check sheets
        // check columns
        // Cl.  Vendor  Cty Name 1  Name 2  District    PO Box  PostalCode  Rg  Street  Address Telephone 1 Telephone 2 Fax Number
    }
}
