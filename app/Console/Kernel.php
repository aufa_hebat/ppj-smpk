<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ImportCompBaruCommand::class,
        Commands\ImportCompanyFromExcelCommand::class,
        Commands\ImportUserFromExcelCommand::class,

        Commands\MakeJwtTokenCommand::class,
        Commands\MakeReferenceCommand::class,

        Commands\ReloadAllCommand::class,
        Commands\ReloadCacheCommand::class,
        Commands\ReloadDbCommand::class,
        Commands\ReloadAclCommand::class,

        Commands\SeedDevelopmentDataCommand::class,
        Commands\SeedTestDataCommand::class,

        Commands\SyncAllCommand::class,
        Commands\SyncChartCommand::class,
        Commands\SyncFigureCommand::class,

        Commands\UpdateTaskStatusCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
