<?php

namespace App\Traits;

trait HasCreatedTrait
{
    /**
     * Get created by key name. Default is user_id.
     *
     * @return string
     */
    public function getCreatedByKeyName(): string
    {
        if (! isset($this->created_by_key_name)) {
            return 'user_id';
        }

        return $this->created_by_key_name;
    }
}
