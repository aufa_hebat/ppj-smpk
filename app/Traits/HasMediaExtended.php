<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * HasMediaExtended Trait.
 */
trait HasMediaExtended
{
    use HasMediaTrait;

    public function addDocument(Request $request, string $name)
    {
        if ($request->hasFile($name) && $request->file($name)->isValid()) {
            return $this->addMediaFromRequest($name)->toMediaCollection($this->getMediaCollectionName());
        }

        return null;
    }

    public function addDocuments(Request $request, array $names)
    {
        return $this->addAllMediaFromRequest($names)
            ->each
            ->toMediaCollection($this->getMediaCollectionName());
    }

    /**
     * Get the last media item of a media collection.
     *
     * @param string $collectionName
     * @param array  $filters
     *
     * @return Media|null
     */
    public function getLastMedia(string $collectionName = 'default', array $filters = [])
    {
        $media = $this->getMedia($collectionName, $filters);

        return $media->sortByDesc('created_at')->first();
    }

    /*
     * Get the url of the image for the given conversionName
     * for first media for the given collectionName.
     * If no profile is given, return the source's url.
     */
    public function getLastMediaUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        $media = $this->getLastMedia($collectionName);

        if (! $media) {
            return '';
        }

        return $media->getUrl($conversionName);
    }

    /**
     * Get Media Collection Key.
     *
     * @return string key used in `media-collections` config
     */
    public function getMediaCollectionKey()
    {
        if (! isset($this->media_collection_key)) {
            return 'default';
        }

        return $this->media_collection_key;
    }

    /**
     * Get Media Collection Identifier Key.
     *
     * @return string key used as identifier, default is hashslug field if the field is exist
     */
    public function getMediaIdentifierKey()
    {
        if (! isset($this->media_identifier_key)) {
            return (isset($this->hashslug)) ? 'hashslug' : 'id';
        }

        return $this->media_identifier_key;
    }

    /**
     * Get Media Identifier Value.
     *
     * @return mix Can be string or integer
     */
    public function getMediaIdentifier()
    {
        return $this->{$this->getMediaIdentifierKey()};
    }

    /**
     * Get Media Collection Name.
     *
     * @return string Get media collection name
     */
    public function getMediaCollectionName()
    {
        return media_collection_name($this->getMediaCollectionKey(), $this->getMediaIdentifier());
    }

    /**
     * Get Documents.
     *
     * @return \Spatie\MediaLibrary\Media
     */
    public function documents()
    {
        return $this->getMedia($this->getMediaCollectionName());
    }
}
