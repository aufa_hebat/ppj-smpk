<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

trait GetTotalCompletionTrait
{
    /**
     * Get Total Contracts created by Department.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDepartment(Builder $query, Request $request = null)
    {
        if ($request && null !== $request->department_id) {
            // where acquisition department = request->department_id
            $query->whereHas('acquisition.user.department', function ($query) use ($request) {
                $query->where('id', $request->department_id);
            });
        }

        return $query;
    }

    /**
     * Get Total of Contracts that 6 months to end by department.
     * Project end date <= current adte + 6 months && project end date >= current date.
     *
     * @todo    Department ID based on user id if not defined. If user has permission / role to view all sst, exclude the query condition
     * @todo    Need to consider actual project completion date. Actual project completion date can be retrieve from EOT, VO.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeContractsSixMonthToEnd(Builder $query, Request $request = null)
    {
        $query->department($request);

        // @todo Get end date from EOT / VO if either one of it exist, else use from SST
        $query
            ->where('end_working_date', '>=', now())
            ->where('end_working_date', '<=', now()->addMonths(6));

        return $query;
    }

    /**
     * Get Payment Progress exceeds the Physical Progress.
     *
     * @todo    Get from Acquisition Activities and IPC
     * @todo    Consider to move this to services
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaymentProgressExceedPhysicalProgress(Builder $query, Request $request = null)
    {
        $query->department($request);
        // Physical Progress: Get latest completed progress date
        // Financial Progress: Get latest completed payment date
        return $query;
    }

    /**
     * NO OF EXPIRED CONTRACTS WITH ACCOUNT NOT CLOSE, upon selecting a Jabatan will display list by Bahagian.
     *
     * @todo    TBC what is account
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExpiredContractsWithAccountNotClose(Builder $query, Request $request = null)
    {
        return $query->department($request);
    }

    /**
     * Group by department.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGroupByDepartment(Builder $query, Request $request = null)
    {
        if ($request && null !== $request->department_id) {
            $query->whereHas('acquisition.user.department', function ($query) use ($request) {
                $query->groupBy('id')
                    ->having('id', $request->department_id);
            });
        }

        return $query;
    }

    /**
     * No of Contracts WHERE PHYSICAL PROGRESS EXCEEDS PAYMENT PROGRESS BY 50%. group by Jabatan, upon selecting a Jabatan will display list by Bahagian.
     *
     * @todo    Get amount where ipc percentage is more than 50%
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request [description]
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaymentProgressExceedPhysicalProgressByFiftyPercent(Builder $query, Request $request = null)
    {
        $query->paymentProgressExceedPhysicalProgress($query, $request);
        // @todo get amount where ipc percentage is more than 50%
        return $query;
    }

    public function getTotalScheduledCompletedAttribute()
    {
        if (0 === $this->activities->count()) {
            return 0;
        }

        return round((
            $this->activities->sum->scheduled_completion / ($this->activities->count() * 100)
        ) * 100, 2);
    }

    public function getTotalActualCompletedAttribute()
    {
        if (0 === $this->activities->count()) {
            return 0;
        }

        return round((
            $this->activities->sum->actual_completion / ($this->activities->count() * 100)
        ) * 100, 2);
    }
}
