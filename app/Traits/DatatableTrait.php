<?php

namespace App\Traits;

/**
 * DatatableTrait Trait.
 */
trait DatatableTrait
{
    public function scopeDatatable($query)
    {
        return $query->latest();
    }
}
