<?php

namespace App\Traits;

trait HasMakeStatic
{
    /**
     * Empty Construct.
     */
    public function __construct()
    {
    }

    /**
     * Create Static Instance.
     *
     * @return $this static instance
     */
    public static function make()
    {
        return new self();
    }
}
