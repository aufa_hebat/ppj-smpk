<?php

namespace App\Traits;

trait MailTrait
{
    public $user;
    public $subject;
    public $content;
    public $link;
    public $link_label;
    public $data;
    public $template = 'mail.notification';
    public $cc;
    public $bcc;
    public $attachments;

    public function __construct($identifier)
    {
        $this->user = \App\Models\User::findByHashSlugOrId($identifier);
    }

    abstract public function send();

    public static function make($identifier)
    {
        return new self($identifier);
    }

    public function subject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function message($content)
    {
        $this->content = $content;

        return $this;
    }

    public function link($link)
    {
        $this->link = $link;

        return $this;
    }

    public function link_label($link_label)
    {
        $this->link_label = $link_label;

        return $this;
    }

    public function data($data)
    {
        $this->data = $data;

        return $this;
    }

    public function template($template)
    {
        $this->template = $template;

        return $this;
    }

    public function attachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function cc(array $cc)
    {
        $this->cc = $cc;

        return $this;
    }

    public function bcc(array $bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }
}
