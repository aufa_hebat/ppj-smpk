<?php

namespace App\Traits;

/**
 * ReferenceTrait Trait.
 */
trait ReferenceTrait
{
    public function referencePrefix()
    {
        if (isset($this->reference_prefix)) {
            return $this->reference_prefix;
        }

        return config('document.prefix');
    }
}
