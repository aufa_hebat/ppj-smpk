<?php

namespace App\Traits\Relationships;

use Illuminate\Support\Str;

trait TaskableTrait
{
    abstract public function getTaskableLabelAttribute(): string;

    abstract public function getTaskableNameAttribute(): string;

    abstract public function getWorkflowSlugName(): string;

    public function getTableName(): string
    {
        if (! isset($this->table)) {
            return Str::lower(Str::plural(class_basename(fqcn($this))));
        }

        return $this->table;
    }

    public function getTaskableColumns()
    {
        $schema = \DB::connection(config('database.default'))
            ->getDoctrineConnection()
            ->getSchemaManager();
        $taskable_columns = isset($this->taskable_columns) ? $this->taskable_columns : [];

        // we don't need to those already not null, since it's handle
        // by the validation. so we just need to check those nullable
        // fields, and need to determine, which fields is mandatory
        // to mark task as done.
        $columns = collect($schema->listTableColumns($this->getTableName()))
            ->reject(function ($column) {
                return $column->getNotNull();
            })
            ->reject(function ($column) {
                return in_array($column->getName(), ['id', 'hashslug', 'deleted_at', 'created_at', 'updated_at']);
            });

        $columns = collect($columns->keys()->toArray());

        // if taskable columns is set, concat columns defined
        // by the user to validate the fields either it's empty or not
        if (sizeof($taskable_columns) > 0) {
            $columns = $columns->concat($taskable_columns);
        }

        return $columns->toArray();
    }

    public function getIsTableFilledAttribute()
    {
        foreach ($this->getTaskableColumns() as $column) {
            if (isset($this->{$column}) && blank($this->{$column})) {
                return false;
            }
        }

        return true;
    }

    /**
     * Use for after checking current model's table.
     * You may apply your logic inside here to determine
     * either the task is done or no.
     *
     * @return bool
     */
    public function getDependencyTasksIsDoneAttribute(): bool
    {
        return true;
    }

    /**
     * Get task status based data filled in the table then user defined logic, if necessary.
     *
     * Task status is done by default based on columns in table that has been filled.
     * If all columns defined have been filled, then it's consider the task is done.
     *
     * One the checking on table task status is done, then it will check dependency task that is done.
     * The dependency task accessor need to be implement by
     *
     * @return [type] [description]
     */
    public function getTaskIsDoneAttribute(): bool
    {
        return ($this->is_table_filled) ? $this->dependency_tasks_is_done : false;
    }

    /**
     * Get all of the class's tasks.
     */
    public function tasks()
    {
        return $this->morphMany(\App\Models\Task::class, 'taskable');
    }
}
