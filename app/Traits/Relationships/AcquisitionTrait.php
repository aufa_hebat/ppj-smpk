<?php

namespace App\Traits\Relationships;

use App\Models\Acquisition\Detail as Acquisition;

trait AcquisitionTrait
{
    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class, 'acquisition_id');
    }
}
