<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'report_type';  
}
