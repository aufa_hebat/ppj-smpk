<?php

namespace App\Models\Addendum;

use App\Models\Acquisition\Acquisition;
use Illuminate\Database\Eloquent\Model;

class Addendum extends Model
{
    protected $table    = 'addendums';
    protected $fillable = [];

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
