<?php

namespace App\Models\Addendum;

use App\Models\Document;
use App\Traits\Relationships\AcquisitionTrait;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use AcquisitionTrait;

    protected $table    = 'addendum_items';
    protected $fillable = [];

    public function addendum()
    {
        return $this->belongsTo(Addendum::class);
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'addenda');
    }
}
