<?php

namespace App\Models\Singleton;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\EotDetail;
use App\Models\Acquisition\AppointedCompany;

class SingletonEOT
{
   
    static function save(Request $request) {
        
        $appointed_company = AppointedCompany::where('id', $request->appointed_company_id)->first();

        if (! empty($request->original_end_date)) {
            $original_end_date = Carbon::createFromFormat('d/m/Y', $request->original_end_date);
        } else {
            $original_end_date = Carbon::now();
        }

        if (! empty($request->extended_end_date)) {
            $extended_end_date = Carbon::createFromFormat('d/m/Y', $request->extended_end_date);
        } else {
            $extended_end_date = Carbon::now();
        }

        $sst               = AppointedCompany::where([
            ['company_id', '=', $appointed_company->company_id],
            ['acquisition_id', '=', $appointed_company->acquisition_id],
        ])->first();

        $eot = Eot::create([
            'bil'                  => $request->bil,
            'appointed_company_id' => $request->appointed_company_id,
            'sst_id'               => $sst->acquisition->sst->id,
            'acquisition_id'       => $appointed_company->acquisition->id,
            'user_id'              => user()->id,
            'department_id'        => $request->department_id,
            'period'               => $request->period,
            'period_type_id'       => $request->period_type_id,
            'original_end_date'    => $original_end_date,
            'extended_end_date'    => $extended_end_date,
            'section'              => $request->section,
        ]);

        for ($a = 1; $a < count($request->baru); $a = $a + 1) {
            $eot_detail = EotDetail::create([
                'eot_id'          => $eot->id,
                'user_id'         => user()->id,
                'reasons'         => $request->baru[$a]['reason'],
                'clause'          => $request->baru[$a]['clauses'],
                'periods' => $request->baru[$a]['period'],
                'period_type_id' => $request->baru[$a]['period_type'],
            ]);
        }
        
      return $eot;
    }
    
}
