<?php

namespace App\Models\Singleton;

use Illuminate\Http\Request;
use App\Models\Acquisition\Approval;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Category;
use Illuminate\Support\Carbon;

class SingletonAcquisition
{
   
    static function save(Request $request) {
       
        $approval = Approval::findByHashSlug($request->hashslug);
        $cnt = (counter()->counter_acquisition ?? 0) + 1;
        counter()->update(['counter_acquisition' => $cnt]);

        $getRef = user()->getAcquisitionReferenceCode($cnt, $approval->year, $request->acquisition_category_id);

        $acq                          = new Acquisition();
        $acq->acquisition_approval_id = Approval::findByHashSlug($request->acquisition_approval_id)->id;
        $acq->acquisition_category_id = $request->acquisition_category_id ?? 2;
        $acq->reference               = $getRef;
        $acq->title                   = (!empty($request->title))? str_replace("'", '`', strtoupper($request->title)): Approval::findByHashSlug($request->acquisition_approval_id)->title;
        $acq->document_price          = money()->toMachine($request->document_price ?? 0);

        $acq->advertised_at  = (! empty($request->advertised_at)) ? Carbon::createFromFormat('d/m/Y', $request->advertised_at) : null;
        $acq->start_sale_at  = (! empty($request->start_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->start_sale_at) : null;
        $acq->closed_at      = (! empty($request->closed_at)) ? Carbon::createFromFormat('d/m/Y', $request->closed_at) : null;
        $acq->closed_sale_at = (! empty($request->closed_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->closed_sale_at) : null;

        /*
         * Experiences
         */
        $acq->experience_status   = (isset($request->experience_status)) ? true : false;
        $acq->experience_value    = money()->toMachine($request->experience_value ?? 0);
        $acq->experience_duration = $request->experience_duration;

        /*
         * Briefing
         */
        $acq->briefing_status   = (isset($request->briefing_status)) ? true : false;
        $acq->briefing_at       = (! empty($request->briefing_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->briefing_at) : null;
        $acq->briefing_location = $request->briefing_location;

        /*
         * Site Visit
         */
        $acq->visit_status    = (isset($request->visit_status)) ? true : false;
        $acq->visit_date_time = (! empty($request->visit_date_time)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->visit_date_time) : null;
        $acq->visit_location  = $request->visit_location;
        $acq->status_id       = \App\Models\Status::ACTIVE;

        if ('Sebut Harga B' == Category::find($acq->acquisition_category_id)->name || 'Sebut Harga' == Category::find($acq->acquisition_category_id)->name || 'Tender Terbuka' == Category::find($acq->acquisition_category_id)->name || 'Tender Terhad' == Category::find($acq->acquisition_category_id)->name) {
            $acq->document_sale_location = 'Kaunter Jualan Sebut Harga Blok C Aras 1 Bahagian Perolehan dan Ukur Bahan, Jabatan Kewangan Perbadanan Putrajaya';
        }

        $acq->status_task            = config('enums.flow1');
        $acq->user_id                = user()->id;
        $acq->department_id          = user()->department_id;
        $acq->executor_department_id = user()->executor_department_id;
        $acq->section_id             = user()->section_id;
        $acq->save();
        
      return $acq;
    }
    
}
