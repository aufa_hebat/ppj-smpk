<?php

namespace App\Models\Singleton;

use Illuminate\Http\Request;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Review;
use App\Models\Acquisition\Ipc;
use Illuminate\Support\Carbon;

class SingletonIpc
{
   
    static function save(Request $request) {
        
            $sst                = Sst::find($request->sst_id);
            $checks             = Sst::where('id',$request->sst_id)->where('deleted_at',null)->whereDate('end_working_date','<',Carbon::createFromFormat('d/m/Y', $request->evaluation_at)->format('Y-m-d') . '%')->first();
            $checks_eot         = (!empty($sst->eots) && !empty($sst->eots->where('deleted_at',null)) && !empty($sst->eots->where('deleted_at',null)->last()))? $sst->eots->where('deleted_at',null)->last()->pluck('extended_end_date')->first():null;
            $check_semakan      = Review::where('eot_id','!=',null)->where('acquisition_id',$sst->acquisition_id)->where('deleted_at',null)->where('status','Selesai')->orderBy('id','desc')->first();
            //kene check cpc dah kluar ke x n eot ade ke x
            if(empty($sst->cpcs) && ((empty($checks) && $checks == null) ||
                  (empty($checks_eot) || (!empty($checks_eot) && $request->evaluation_at <= Carbon::createFromFormat('Y-m-d H:i:s', $checks_eot)->format('d/m/Y') && !empty($check_semakan) && $check_semakan->count() > 0)) )
              ){
                $date_penilaian = (!empty($request->evaluation_at))? Carbon::createFromFormat('d/m/Y', $request->evaluation_at):null;
                $penalty        = null;
            }else{  
                $cnt_ipc        = Ipc::where('sst_id',$request->sst_id)->where('deleted_at',null)->where('penalty','!=',null)->count();
                $date_penilaian = (!empty($checks_eot))? $checks_eot:$checks->end_working_date;
                $penalty        = (!empty($cnt_ipc) && $cnt_ipc > 1)? "PENULTIMATE".$cnt_ipc:"PENULTIMATE";
            }
            //check previous ipc ada vo ke x
            $cnt_vo             = Ipc::where('sst_id',$request->sst_id)->where('deleted_at',null)->where('new_contract_amount','!=',0)->orderBy('ipc_no','desc')->first();
            $new_amt            = 0;
            if(!empty($cnt_vo) && $cnt_vo->count() > 0){
                $new_amt        = $cnt_vo->new_contract_amount;
            }
            // 
            
            $ipc = Ipc::create([
                'user_id'             => user()->id,
                'sst_id'              => $request->sst_id ?? '0',
                'ipc_no'              => $request->ipc_no,
                'short_text'          => $request->short_text ?? null,
                'last_ipc'            => $request->last_ipc ?? 'n',
                'evaluation_at'       => $date_penilaian,
                'long_text'           => $request->long_text ?? null,
                'penalty'             => $penalty,
                'new_contract_amount' => $new_amt,
                'status'              => 1,
            ]);
        
      return $ipc;
    }
    
}
