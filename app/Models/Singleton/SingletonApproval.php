<?php

namespace App\Models\Singleton;

use Illuminate\Http\Request;
use App\Models\Acquisition\Approval;

class SingletonApproval
{
   
   static function save(Request $request) {
     $approval = Approval::create([
            'user_id'             => user()->id,
            'department_id'       => null !== optional(user()->department)->id ? optional(user()->department)->id : Department::ALL_ID, // All
            'acquisition_type_id' => $request->acquisition_type_id,
            'period_type_id'      => $request->period_type_id,
            'title'               => str_replace("'", '`', strtoupper($request->title)),
            'description'         => str_replace("'", '`', $request->description),
            'year'                => $request->year,
            'file_reference'      => $request->file_reference,
            'period_length'       => $request->period_length,
            'est_cost_project'    => money()->toMachine('0'),
        ]);
      return $approval;
    }
   
}
