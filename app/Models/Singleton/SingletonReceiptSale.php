<?php

namespace App\Models\Singleton;

use Illuminate\Http\Request;
use App\Models\Sale;
use App\Models\Company\Company;
use App\Models\Acquisition\Acquisition;
use App\Models\WorkflowTask;
use Illuminate\Support\Carbon;

class SingletonReceiptSale
{
    static function create(Request $request){
        $new_sal = null;
        $ic_number = str_replace("-","",$request->ic_number);
        
        if (! empty($request->sale)) {
            foreach ($request->sale as $jual) {
                if (! empty($jual['status']) && isset($jual['status']) && $jual['acq'] != null) {
                    $check_sal = Sale::where('company_id', $jual['comp'])->where('acquisition_id', $jual['acq'])->where('deleted_at',null)->count();
                    
                    if ($check_sal < 1) {
                        $new_sal                      = new Sale();
                        $new_sal->user_id             = user()->id;
                        $new_sal->acquisition_id      = $jual['acq'];
                        $new_sal->company_id          = $jual['comp'];
                        $new_sal->ic_number           = $ic_number;
                        $new_sal->agent_letter_status = $jual['letter'] ?? "";
                        $new_sal->officer_id          = user()->id;
                        $new_sal->jabatan_code        = user()->department_id;
                        $new_sal->buyer_status        = $jual['buyer'];
                        $new_sal->sales_date          = new Carbon();
                        $new_sal->save();
            
                        $workflow = WorkflowTask::where('acquisition_id', $jual['acq'])->where('deleted_at',null)->first();
                        $workflow->flow_desc = 'Jualan : Sila kemaskini nombor resit.';
                        $workflow->flow_location = 'Jualan';
                        $workflow->url = '/sale/'.$ic_number.'/receipt';
                        $workflow->update();

                        $acq = Acquisition::find($jual['acq']);

                        $acq->status_task = config('enums.flow6');
                        $acq->update();

                        $company = Company::find($jual['comp']);

                        audit($new_sal, __('Jualan bagi perolehan ' .$acq->reference. ' : ' .$acq->title. ' telah berjaya dibeli oleh ' .$company->company_name. '.'));

                    }
                }
            }
        }
        return $new_sal;
    }
    
    static function update(Request $request,$id) {
        $selling = null;
        if (! empty($request->receipt)) {
            foreach ($request->receipt as $b => $recpt) {
                if (!empty($recpt['id'])) {
                    $selling = Sale::find($recpt['id']);
                    $acquisition = Acquisition::find($selling->acquisition_id);
                    if(!empty($recpt['resit']) && $recpt['resit'] != "" && $recpt['resit'] != null && $recpt['resit'] != $selling->receipt_no){
                        if (1 == $recpt['edit']) {
                            $get_recpt          = Sale::where('acquisition_id', $recpt['acq'])->orderBy('series_no', 'desc')->where('deleted_at',null)->first();
                            $count_recpt        = (! empty($get_recpt->series_no)) ? ($get_recpt->series_no + 1) : 1;
                            $selling->series_no = $count_recpt;
                        }
                        $selling->receipt_no = $recpt['resit'];
                        $selling->reason_buy = $recpt['reason_buy'] ?? null;
                        $selling->updated_at = new Carbon();
                        $selling->update();

                        

                        if($acquisition->status_task == 'Jualan' || $acquisition->status_task == 'Buka Peti'){

                            $workflow = WorkflowTask::where('acquisition_id', $selling->acquisition_id)->where('deleted_at',null)->first();
                            $workflow->flow_desc = 'Buka Peti : Sila teruskan dengan buka peti perolehan.';
                            $workflow->flow_location = 'Buka Peti';
                            $workflow->is_claimed = 0;
                            $workflow->user_id = null;
                            $workflow->role_id = 6;
                            $workflow->url = '/box/'.$selling->acquisition->hashslug.'/edit';
                            $workflow->update();
                            
                            $acquisition->status_task = config('enums.flow7');
                            $acquisition->update();
                        }

                        audit($selling, __('Resit jualan bagi perolehan ' .$selling->acquisition->reference. ' : ' .$selling->acquisition->title. ' telah berjaya dikemaskini.'));
                        
                    }
                    //kalau salah no resit n nak buang terus
//                    if((empty($recpt['resit']) || $recpt['resit'] == "" || $recpt['resit'] == null) && $recpt['edit'] == 2){
//                        
//                        $sell = Sale::find($recpt['id']);
//                        $sell->deleted_by = user()->id;
//                        $sell->update();
//                        Sale::find($recpt['id'])->delete();
//                    }
                    if ($request->hasFile('document'.$recpt['id'])) {
                        
                        $box_requirements = [
                            'request_hDocumentId' => null,
                            'request_hasFile'     => $request->hasFile('document'.$recpt['id']),
                            'request_file'        => $request->file('document'.$recpt['id']),
                            'acquisition'         => $acquisition,
                            'doc_type'            => 'JUALAN/'.$recpt['id'],
                            'doc_id'              => $recpt['id'],
                        ];
                        common()->upload_document($box_requirements);

                        audit($selling, __('Lampiran resit jualan bagi perolehan ' .$selling->acquisition->reference. ' : ' .$selling->acquisition->title. ' telah berjaya dimuatnaik.'));
                    }
                }
            }
        }
        
      return $selling;
    }
    
}
