<?php

namespace App\Models\CSV;

use Illuminate\Database\Eloquent\Model;

class Preview extends Model
{
    protected $table    = 'csv_data';
    protected $fillable = ['file', 'header', 'data'];
}
