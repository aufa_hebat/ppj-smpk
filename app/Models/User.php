<?php

namespace App\Models;

use App\Traits\HasDatatable;
use App\Traits\HasMediaExtended;
use App\Traits\HasSlugExtended;
use App\Traits\HasThumbnail;
use App\Traits\LogsActivityExtended;
use CleaniqueCoders\Profile\Traits\HasProfile;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMediaConversions
//class User extends \Eloquent implements Authenticatable,HasMediaConversions
{
    use HasApiTokens, HasProfile, HasMediaExtended,
        HasThumbnail, HasSlugExtended,
        LogsActivityExtended, Notifiable, SoftDeletes,
        HasDatatable,HasRoles;
    
    
    //Din override HasRoles , need to support can selected role after login , not done
    //Now try to assignRole to User
    //$user->assignRole('writer');

    /**
     * Guarded Field.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Create Slug From.
     *
     * @var array
     */
    protected $slug_from = ['name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'executor_department_id', 'department_id', 'section_id', 'supervisor_id', 'position_id', 'scheme_id', 'grade_id', 'phone_no','honourary',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that show in datatable.
     *
     * @var array
     */
    protected $datatable = [
        'name', 'email', 'hashslug', 'executor_department_id', 'department_id', 'section_id', 'supervisor_id', 'position_id', 'scheme_id', 'grade_id', 'phone_no',
    ];
    
     /**
     * Get roles as string via method.
     *
     * @return string
     */
    public function currentRole()
    {
        //DIN test
        //foreach ($this->roles as $value){
            //$this->removeRole($value);
       // }
       // return title_case(implode(', ', $this->roles->pluck('name')->toArray()));
        return $this->current_role_login;
    }

    /**
     * Get roles as string via method.
     *
     * @return string
     */
    public function rolesToString()
    {
        //DIN test
        //foreach ($this->roles as $value){
            //$this->removeRole($value);
       // }
       // return title_case(implode(', ', $this->roles->pluck('name')->toArray()));
        return $this->current_role_login;
    }

    /**
     * Get roles as a string via accessor.
     *
     * @return string
     */
    public function getRolesToStringAttribute()
    {
        return $this->rolesToString();
    }

    public function getAcquisitionReferenceCode($counter = null, $year = null, $type = null)
    {
        if (empty($year)) {
            $year = date('Y');
        }

        if (empty($counter)) {
            ((optional($this->department->counter)->counter_acquisition) + 1);
        }

        if (empty($this->department)) {
            return 'PPJ/' . $year;
        }
        if (! empty($type)) {
            if (1 == $type) {
                $type = 'SHB(S)';
            } elseif (2 == $type) {
                $type = 'SH(S)';
            } elseif (3 == $type) {
                $type = 'T';
            } elseif (4 == $type) {
                $type = 'TT';
            } elseif (5 == $type) {
                $type = 'RB';
            } elseif (6 == $type) {
                $type = 'RT';
            } elseif (7 == $type) {
                $type = 'RFP';
            } elseif (8 == $type) {
                $type = 'P';
            } else {
                $type = '';
            }
        }

        return 'PPJ/' . $this->department->code . '/' . $type . '/' . $counter . '/' . $year;
    }

    public function scopeDetails($query)
    {
        return $query->with(
            [
                'executor_department', 'department',
                'section', 'scheme', 'grade', 'position',
                'roles' => function ($query) {
                    return $query->select('id', 'name');
                },
            ]
        );
    }

    /**
     * Get last user logged in.
     *
     * @todo Do refactor this.
     *
     * @param [type] $query [description]
     *
     * @return [type] [description]
     */
    public function scopeLastLoggedIn($query)
    {
        $latest = config('activitylog.activity_model')::where('subject_type', get_class($this))
            //->where('description', 'User successfully logged in.')
              ->where('description', 'Pengguna telah berjaya log masuk.')  
            ->latest()
            ->first();

        if (empty($latest)) {
            return [
                'name'     => '-',
                'datetime' => '-',
            ];
        }
        $user = $query->find($latest->subject_id);

        return [
            'name'     => $user->name,
            'datetime' => $latest->created_at->format(config('datetime.dashboard')),
        ];
    }
    
    //add accessors for name, make in capital for all http://laravel.at.jeffsbox.eu/laravel-5-eloquent-accessors-mutators
    public function getNameAttribute($value)
    {
        return strtoupper($value);
    }

    public function scopeNewestUser($query)
    {
        return $query->latest()->first();
    }

    public function executor_department()
    {
        return $this->belongsTo(\App\Models\Department::class, 'executor_department_id');
    }

    public function bahagian()
    {
        return $this->belongsTo(\App\Models\Section::class, 'executor_department_id');
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class)->withDefault();
    }

    public function section()
    {
        return $this->belongsTo(\App\Models\Section::class)->withDefault();
    }

    public function scheme()
    {
        return $this->belongsTo(\App\Models\Scheme::class)->withDefault();
    }

    public function grade()
    {
        return $this->belongsTo(\App\Models\Grade::class)->withDefault();
    }

    public function position()
    {
        return $this->belongsTo(\App\Models\Position::class)->withDefault();
    }

    public function supervisor()
    {
        return $this->belongsTo(\App\Models\User::class, 'supervisor_id')->withDefault();
    }

    public function tasks()
    {
        return $this->hasMany(\App\Models\Task::class, 'done_by');
    }
    
     /**
     * Determine if the user may perform the given permission.
     *
     * @param string|Permission $permission
     *
     * @return bool
     *
     * @throws \Spatie\Permission\Exceptions\GuardDoesNotMatch
     */
    
    public function hasPermissionTo($permission): bool
    {
        //dd($permission);
        if (is_string($permission)) {
            $permission = app(\Spatie\Permission\Contracts\Permission::class)->findByName($permission, $this->getDefaultGuardName());
        }

        if (is_int($permission)) {
            $permission = app(\Spatie\Permission\Contracts\Permission::class)->findById($permission, $this->getDefaultGuardName());
        }

        if (! $this->getGuardNames()->contains($permission->guard_name)) {
            throw GuardDoesNotMatch::create($permission->guard_name, $this->getGuardNames());
        }

        //by pass here
       //dd($this);
       //dd($permission->roles);
           
       //DIN bypass menu , support only current login only   
       //TODO - change to session 
       $currentRole = $this->current_role_login; 
       $isHas = false;
        
       foreach ($permission->roles as $role){
           if($role->name == $currentRole){
              $isHas = true;
              break;
           }
       }     
       return $isHas;
        //return $this->permissions->contains('id', $permission->id);
    }
}
