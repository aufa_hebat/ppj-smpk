<?php

namespace App\Models;

use App\Models\Acquisition\Ipc;
use Illuminate\Database\Eloquent\Model;

class IpcSap extends Model
{
    protected $table   = 'ipc_saps';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ipc()
    {
        return $this->belongsTo(Ipc::class);
    }
}
