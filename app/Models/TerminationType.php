<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TerminationType extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'termination_types';
}
