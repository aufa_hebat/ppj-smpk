<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailScheduler extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'email_schedulers';
}
