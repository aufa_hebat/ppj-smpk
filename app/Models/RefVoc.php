<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefVoc extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'ref_vocs';  
    
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function position()
    {
        return $this->belongsTo(\App\Models\Position::class);
    }
}
