<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    protected $guarded = ['id'];

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function getDepartmentCodeAttribute()
    {
        return $this->department->code;
    }
}
