<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $guarded = ['id'];

    public function bahagian()
    {
        return $this->belongsTo(self::class, 'category', 'code');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'category', 'code');
    }
}
