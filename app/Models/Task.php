<?php

namespace App\Models;

use App\Traits\HasDatatable;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasDatatable;

    protected $guarded = ['id'];
    protected $dates   = ['done_at'];
    protected $casts   = [
        'is_done' => 'boolean',
    ];

    /**
     * Get all of the owning workflowable models.
     */
    public function taskable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function workflow()
    {
        return $this->belongsTo(\App\Models\Workflow::class, 'workflow_id');
    }

    public function done_by()
    {
        return $this->belongsTo(\App\Models\User::class, 'done_by');
    }

    public function scopeTask($query, $identifier)
    {
        return $query->hashslugOrId($identifier);
    }

    public function scopeIsDone($query, $is_done = true)
    {
        return $query->where('is_done', $is_done);
    }

    public function scopeDoneBy($query, $done_by = null)
    {
        return $query->where('done_by', (is_null($done_by) ? user()->id : $done_by));
    }

    public function scopeTaskWorkflow($query, $workflow_id)
    {
        return $query->where('workflow_id', $workflow_id);
    }

    public function scopeMarkAsDone($query, $taskable_id, $taskable_type)
    {
        return $query->where('taskable_id', $taskable_id)
            ->where('taskable_type', $taskable_type)
            ->isDone(false)
            ->update([
                'is_done' => true,
                'done_at' => now(),
                'done_by' => optional(user())->id,
            ]);
    }

    public function scopeMarkAsNotDone($query, $taskable_id, $taskable_type)
    {
        return $query
            ->where('taskable_id', $taskable_id)
            ->where('taskable_type', $taskable_type)
            ->isDone()
            ->update([
                'is_done' => false,
                'done_at' => null,
                'done_by' => null,
            ]);
    }

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'workflow', 'done_by'
        );
    }

    public function scopeWithUserTask($query, $user_id)
    {
        return $query->with('taskable')->whereUserId($user_id);
    }

    public function scopeDatatable($query)
    {
        return $query
            ->withDetails()
            ->withUserTask(user()->id);
    }
}
