<?php

namespace App\Models\CIDB;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'cidb_categories';

    public function approval()
    {
        return $this->belongsTo(Approval::class);
    }
}
