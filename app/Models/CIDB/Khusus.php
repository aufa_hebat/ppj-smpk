<?php

namespace App\Models\CIDB;

use Illuminate\Database\Eloquent\Model;

class Khusus extends Model
{
    protected $table = 'cidb_khusus';

    public function approval()
    {
        return $this->belongsTo(Category::class);
    }
}
