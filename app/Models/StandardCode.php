<?php

namespace App\Models;

use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Model;

class StandardCode extends Model
{
    protected $guarded = [];

    protected $table = 'standard_codes';

    public function company()
    {
        return $this->hasMany(Company::class);
    }
}
