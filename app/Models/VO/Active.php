<?php

namespace App\Models\VO;

use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_actives';
    protected $fillable = ['user_id', 'department_id', 'status', 'sst_id', 'bq_element_id', 'bq_item_id', 'vo_id', 'comment', 'amount'];

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'department',
            'sst', 'sst.acquisition',
            'sst.acquisition.approval',
            'sst.acquisition.appointed'
        );
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function vo()
    {
        return $this->belongsTo(VO::class, 'vo_id');
    }

    public function element()
    {
        return $this->belongsTo(\App\Models\BillOfQuantity\Element::class, 'bq_element_id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Models\BillOfQuantity\Item::class, 'bq_item_id');
    }
    
}
