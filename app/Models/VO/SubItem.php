<?php

namespace App\Models\VO;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_sub_items';

    public function vo_item()
    {
        return $this->belongsTo(\App\Models\VO\Item::class);
    }

    public function vo_element()
    {
        return $this->belongsTo(\App\Models\VO\Element::class);
    }

    public function vo_type()
    {
        return $this->belongsTo(\App\Models\VO\Type::class);
    }
    
    public function vo()
    {
        return $this->belongsTo(VO::class, 'vo_id');
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }
    
    public function ipcBqVo()
    {
        return $this->hasMany(App\Models\Acquisition\IpcBq::class, 'bq_subitem_id')->where('status','V')->where('deleted_at',null);
    }

    public function bqSubItem()
    {
        return $this->belongsTo(\App\Models\BillOfQuantity\SubItem::class, 'bq_subitem_id');
    }
}
