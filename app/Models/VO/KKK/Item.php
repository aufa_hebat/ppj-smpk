<?php

namespace App\Models\VO\KKK;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_kkk_items';

    public function vo_kkk_sub_items()
    {
        return $this->hasMany(\App\Models\VO\KKK\SubItem::class, 'vo_kkk_item_id');
    }
}
