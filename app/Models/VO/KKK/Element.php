<?php

namespace App\Models\VO\KKK;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_kkk_elements';

    public function vo_kkk_items()
    {
        return $this->hasMany(\App\Models\VO\KKK\Item::class, 'vo_kkk_element_id');
    }
}
