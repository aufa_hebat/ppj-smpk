<?php

namespace App\Models\VO\KKK;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_kkk_sub_items';
}
