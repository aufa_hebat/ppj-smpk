<?php

namespace App\Models\VO;

use App\Traits\HasDatatable;
use App\Traits\HasMediaExtended;
use App\Traits\LogsActivityExtended;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VO extends Model
{
    use HasMediaExtended, LogsActivityExtended,
        SoftDeletes, HasDatatable;

    protected $guarded = ['id'];
    protected $table   = 'vo';

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'department',
            'sst', 'sst.acquisition',
            'sst.acquisition.approval',
            'sst.acquisition.appointed'
        );
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function vo_pk_elements()
    {
        return $this->hasMany(\App\Models\VO\PK\Element::class, 'vo_id');
    }

    public function vo_pk_items()
    {
        return $this->hasMany(\App\Models\VO\PK\Item::class, 'vo_id');
    }

    public function vo_pk_sub_items()
    {
        return $this->hasMany(\App\Models\VO\PK\SubItem::class, 'vo_id');
    }

    public function vo_pq_elements()
    {
        return $this->hasMany(\App\Models\VO\PQ\Element::class, 'vo_id');
    }

    public function vo_pq_items()
    {
        return $this->hasMany(\App\Models\VO\PQ\Item::class, 'vo_id');
    }

    public function vo_pq_sub_items()
    {
        return $this->hasMany(\App\Models\VO\PQ\SubItem::class, 'vo_id');
    }

    public function vo_kkk_elements()
    {
        return $this->hasMany(\App\Models\VO\KKK\Element::class, 'vo_id');
    }

    public function vo_kkk_items()
    {
        return $this->hasMany(\App\Models\VO\KKK\Item::class, 'vo_id');
    }

    public function vo_kkk_sub_items()
    {
        return $this->hasMany(\App\Models\VO\KKK\SubItem::class, 'vo_id');
    }

    public function types()
    {
        return $this->hasMany(Type::class, 'vo_id');
    }

    public function actives()
    {
        return $this->hasMany(Active::class, 'vo_id');
    }

    public function getActiveWpsStatusAttribute()
    {
        return $this->actives->where('status', 1)->count() > 0 ? true : false;
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'VO/' . $this->no . '/VO_PPK');
    }

    public function doc_ppk()
    {
        return $this->hasOne(\App\Models\Document::class, 'document_id')->where('document_types', 'VO/' . $this->no . '/PPK_RPT')->where('deleted_at',null);
    }

    public function doc_ppk_result()
    {
        return $this->hasOne(\App\Models\Document::class, 'document_id')->where('document_types', 'VO/' . $this->no . '/PPK_RSLT_RPT')->where('deleted_at',null);
    }

    public function approvers()
    {
        return $this->hasMany(Approver::class, 'vo_id')->orderby('role', 'asc');
    }

    public function vo_elements()
    {
        return $this->hasMany(\App\Models\VO\Element::class, 'vo_id');
    }

    public function vo_items()
    {
        return $this->hasMany(\App\Models\VO\Item::class, 'vo_id');
    }
    
    public function vo_subItems()
    {
        return $this->hasMany(\App\Models\VO\SubItem::class, 'vo_id');
    } 

    public function cancels()
    {
        return $this->hasMany(Cancel::class, 'vo_id');
    }
}
