<?php

namespace App\Models\VO\WPS;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_wps_sub_items';
}
