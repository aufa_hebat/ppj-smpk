<?php

namespace App\Models\VO\WPS;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_wps_items';
}
