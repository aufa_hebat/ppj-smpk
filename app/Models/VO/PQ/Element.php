<?php

namespace App\Models\VO\PQ;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_pq_elements';
}
