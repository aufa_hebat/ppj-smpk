<?php

namespace App\Models\VO\PQ;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_pq_sub_items';

    public function vo()
    {
        return $this->belongsTo(\App\Models\VO\VO::class);
    }
}
