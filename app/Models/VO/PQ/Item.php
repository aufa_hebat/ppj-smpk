<?php

namespace App\Models\VO\PQ;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_pq_items';
}
