<?php

namespace App\Models\VO;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_elements';

    public function vo_type()
    {
        return $this->belongsTo(\App\Models\VO\Type::class);
    }
    
    public function vo()
    {
        return $this->belongsTo(VO::class, 'vo_id');
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function items()
    {
        return $this->hasMany(\App\Models\VO\Item::class, 'vo_element_id');
    }
    
    public function subItems()
    {
        return $this->hasMany(\App\Models\VO\SubItem::class, 'vo_element_id');
    }  

    public function actives()
    {
        return $this->belongsTo(\App\Models\VO\Active::class, 'vo_active_id');
    }

}
