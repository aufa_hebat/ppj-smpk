<?php

namespace App\Models\VO;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_items';

    public function vo_element()
    {
        return $this->belongsTo(\App\Models\VO\Element::class);
    }

    public function vo_type()
    {
        return $this->belongsTo(\App\Models\VO\Type::class);
    }
    
    public function vo()
    {
        return $this->belongsTo(VO::class, 'vo_id');
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function subItems()
    {
        return $this->hasMany(\App\Models\VO\SubItem::class, 'vo_item_id');
    } 

    public function bqElements()
    {
        return $this->belongsTo(\App\Models\BillOfQuantity\Element::class, 'bq_element_id');
    }
}
