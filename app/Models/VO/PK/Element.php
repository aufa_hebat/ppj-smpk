<?php

namespace App\Models\VO\PK;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_pk_elements';
}
