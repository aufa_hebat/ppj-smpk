<?php

namespace App\Models\VO\PK;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_pk_sub_items';
}
