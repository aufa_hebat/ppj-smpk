<?php

namespace App\Models\VO;

use Illuminate\Database\Eloquent\Model;

class Approver extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'vo_approvers';

    public function scopeWithDetails($query)
    {
        return $query->with(
            'sst', 'sst.acquisition',
            'sst.acquisition.approval',
            'sst.acquisition.appointed'
        );
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function vo()
    {
        return $this->belongsTo(VO::class, 'vo_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function position()
    {
        return $this->belongsTo(\App\Models\Position::class);
    }
    
}
