<?php

namespace App\Models\VO;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $table   = 'vo_types';

    public function variation_order_type()
    {
        return $this->belongsTo(\App\Models\VariationOrderType::class);
    }
    
    public function vo()
    {
        return $this->belongsTo(VO::class, 'vo_id');
    }

    public function elements()
    {
        return $this->hasMany(\App\Models\VO\Element::class, 'vo_type_id');
    }

    public function items()
    {
        return $this->hasMany(\App\Models\VO\Item::class, 'vo_type_id');
    }
    
    public function subItems()
    {
        return $this->hasMany(\App\Models\VO\SubItem::class, 'vo_type_id');
    }    
}
