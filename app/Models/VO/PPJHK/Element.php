<?php

namespace App\Models\VO\PPJHK;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'ppjhk_elements';

    public function ppjhk()
    {
        return $this->belongsTo(Ppjhk::class, 'ppjhk_id');
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function ppjhkItems()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\Item::class, 'ppjhk_element_id');
    }
    
    public function ppjhkSubItems()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\SubItem::class, 'ppjhk_element_id');
    }  

    public function voElement()
    {
        return $this->belongsTo(\App\Models\VO\Element::class);
    }
    
    public function ipcVo()
    {
        return $this->hasMany(\App\Models\Acquisition\IpcVo::class, 'vo_cancel_id')->where('status','V')->where('deleted_at',null);
    }
}
