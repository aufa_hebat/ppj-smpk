<?php

namespace App\Models\VO\PPJHK;

use Illuminate\Database\Eloquent\Model;

class SubItem extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'ppjhk_sub_items';

    public function ppjhkItem()
    {
        return $this->belongsTo(\App\Models\VO\PPJHK\Item::class);
    }

    public function ppjhkElement()
    {
        return $this->belongsTo(\App\Models\VO\PPJHK\Element::class);
    }
    
    public function ppjhk()
    {
        return $this->belongsTo(Ppjhk::class, 'ppjhk_id');
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function voElement()
    {
        return $this->belongsTo(\App\Models\VO\Element::class);
    }

    public function voItem()
    {
        return $this->belongsTo(\App\Models\VO\Item::class);
    }

    public function voSubItem()
    {
        return $this->belongsTo(\App\Models\VO\SubItem::class);
    }
}
