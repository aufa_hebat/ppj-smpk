<?php

namespace App\Models\VO\PPJHK;

use App\Traits\HasDatatable;
use App\Traits\HasMediaExtended;
use App\Traits\LogsActivityExtended;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ppjhk extends Model
{
    use HasMediaExtended, LogsActivityExtended,
        SoftDeletes, HasDatatable;

    protected $guarded = ['id'];
    protected $table   = 'ppjhk';

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'department',
            'sst', 'sst.acquisition',
            'sst.acquisition.approval',
            'sst.acquisition.appointed'
        );
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }
    
    public function elements()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\Element::class, 'ppjhk_id')->where('deleted_at',null);
    }

    public function items()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\Item::class, 'ppjhk_id')->where('deleted_at',null);
    }
    
    public function subItems()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\SubItem::class, 'ppjhk_id')->where('deleted_at',null);
    }

    public function doc_ppjhk()
    {
        return $this->hasOne(\App\Models\Document::class, 'document_id')->where('document_types', 'PPJHK/' . $this->no . '/PPJHK_RPT')->where('deleted_at',null);
    }

    public function cancels(){
        return $this->hasMany(\App\Models\VO\Cancel::class, 'ppjhk_id')->where('deleted_at',null);
    }
}
