<?php

namespace App\Models\VO\PPJHK;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'ppjhk_items';

    public function ppjhkElement()
    {
        return $this->belongsTo(\App\Models\VO\PPJHK\Element::class);
    }
    
    public function ppjhk()
    {
        return $this->belongsTo(VO::class, 'ppjhk_id');
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function ppjhkSubItems()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\SubItem::class, 'ppjhk_item_id');
    } 

    public function voElement()
    {
        return $this->belongsTo(\App\Models\VO\Element::class);
    }

    public function voItem()
    {
        return $this->belongsTo(\App\Models\VO\Item::class);
    }
}
