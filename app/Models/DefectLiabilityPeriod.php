<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DefectLiabilityPeriod extends Model
{
    const SIX_MONTH          = 1;
    const TWELVE_MONTH       = 2;
    const SIX_MONTH_VALUE    = 6;
    const TWELVE_MONTH_VALUE = 12;
    protected $guarded       = ['id'];
}
