<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetCode extends Model
{
    protected $guarded = ['id'];

    protected function getNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }
}
