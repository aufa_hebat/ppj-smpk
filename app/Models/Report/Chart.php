<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'report_charts';
    protected $casts   = [
        'labels'   => 'array',
        'datasets' => 'array',
        'data'     => 'array',
    ];

    public function scopeCategory($query, $name)
    {
        return $query->where('category', $name);
    }

    public static function getChartData($category)
    {
        $data = self::category($category)
            ->select('data')
            ->first();

        if (! $data) {
            return null;
        }

        return $data->pluck('data')->toArray()[0];
    }
}
