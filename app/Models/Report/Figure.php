<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class Figure extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'report_figures';
    protected $casts   = [
        'data' => 'array',
    ];

    public function scopeCategory($query, $name)
    {
        return $query->where('category', $name);
    }
}
