<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonType extends Model
{
    protected $guarded = ['id'];
}
