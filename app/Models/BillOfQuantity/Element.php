<?php

namespace App\Models\BillOfQuantity;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\IpcBq;
use App\Models\VO\Active;
use Illuminate\Database\Eloquent\Model;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Element extends Model 
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;
    
    protected $table = 'bq_elements';
    protected $fillable = ['hashslug', 'acquisition_id', 'no', 'element', 'description', 'amount', 'amount_adjust', 'created_by', 'status', 'updated_by', 'paging_start', 'paging_end'];
    
    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class, 'acquisition_id');
    }
    
    public function bqItems()
    {
        return $this->hasMany(Item::class, 'bq_element_id')->where('deleted_at',null);
    }
    
    public function bqSubItems()
    {
        return $this->hasMany(SubItem::class, 'bq_element_id')->where('deleted_at',null);
    }
    
    public function ipcBq()
    {
        return $this->hasMany(IpcBq::class, 'bq_element_id')->where('status','N')->where('deleted_at',null);
    }

    public function voActives()
    {
        return $this->hasMany(Active::class, 'bq_element_id')->where('status', 1)->where('deleted_at',null);
    }
}
