<?php

namespace App\Models\BillOfQuantity;

use App\Models\Acquisition\Acquisition;
use App\Models\BillOfQuantity\Element;
use App\Models\Acquisition\IpcBq;
use Illuminate\Database\Eloquent\Model;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;
    
    protected $table = 'bq_items';
    protected $fillable = ['hashslug', 'acquisition_id', 'bq_no_element', 'no', 'item', 'description', 'amount', 'amount_adjust', 'created_by', 'status', 'updated_by', 'bq_element_id', 'paging_start', 'paging_end'];
    
    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class, 'acquisition_id');
    }

    public function bqElement()
    {
        return $this->belongsTo(Element::class, 'bq_element_id');
    }

    public function bqSubItems()
    {
        return $this->hasMany(SubItem::class, 'bq_item_id')->where('deleted_at',null);
    }
    
    public function ipcBq()
    {
        return $this->hasMany(IpcBq::class, 'bq_item_id')->where('status','N')->where('deleted_at',null);
    }

}
