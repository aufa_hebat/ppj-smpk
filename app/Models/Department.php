<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    const ALL          = 'ALL';
    const ALL_ID       = 1;
    const KEWANGAN     = 'JW';
    protected $guarded = ['id'];

    public function counter()
    {
        return $this->hasOne(\App\Models\Counter::class);
    }

    public function getLabelAttribute()
    {
        return $this->code . ' : ' . $this->name;
    }

    public function scopeFinance($query)
    {
        return $query->where('code', self::KEWANGAN);
    }

    public function scopeExceptAll($query)
    {
        return $query->where('code', '!=', self::ALL);
    }
}
