<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $guarded = ['id'];
    
     //add accessors for name, make in capital for all http://laravel.at.jeffsbox.eu/laravel-5-eloquent-accessors-mutators
    public function getNameAttribute($value)
    {
        return strtoupper($value);
    }
}
