<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    /**
     * Table fields.
     */
    const CATEGORY = 'category';
    const SECTION  = 'section';
    const NAME     = 'name';

    /**
     * Sections.
     */
    const KPN = 'Kelulusan Perolehan';
    const DPN = 'Dokumen Perolehan';
    const BFG = 'Taklimat';
    const ATD = 'Lawatan Tapak';
    const SLS = 'Jualan';

    protected $guarded = ['id'];
    protected $append  = ['label'];

    public function tasks()
    {
        return $this->hasMany(\App\Models\Task::class, 'workflow_id');
    }

    public function scopePre($query)
    {
        return $query->where('category', 'Pra-Kontrak');
    }

    public function scopePost($query)
    {
        return $query->where('category', 'Post-Kontrak');
    }

    public function scopeModule($query, $section)
    {
        return $query->where('section', $section);
    }

    public function scopeName($query, $name)
    {
        return $query->where('name', $name);
    }

    public function getLabelAttribute()
    {
        return $this->category . ': ' . $this->section . ' - ' . $this->name;
    }
}
