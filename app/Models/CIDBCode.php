<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CIDBCode extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'cidb_codes';

    public function getNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }
}
