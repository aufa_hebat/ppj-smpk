<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/*
 * Description of WorkflowTask
 *
 * @author MITSB
 */

use App\Traits\HasDatatable;
use Illuminate\Database\Eloquent\Model;

class WorkflowTask extends Model
{
    //put your code here
    use HasDatatable;
    protected $guarded = ['id'];
    protected $table   = 'workflow_task';
}
