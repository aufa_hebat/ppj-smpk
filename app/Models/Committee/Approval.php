<?php

namespace App\Models\Committee;

use App\Models\Acquisition\Acquisition;
use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $table   = 'approvals';
    protected $guarded = ['id'];
    protected $dates   = ['appointment_date'];
    
     //add accessors for name, make in capital for all http://laravel.at.jeffsbox.eu/laravel-5-eloquent-accessors-mutators
    public function getNameAttribute($value)
    {
        return strtoupper($value);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class)->withDefault();
    }

    public function position()
    {
        return $this->belongsTo(\App\Models\Position::class)->withDefault();
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }
}
