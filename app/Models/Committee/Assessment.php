<?php

namespace App\Models\Committee;

use Illuminate\Database\Eloquent\Model;
use App\Models\Acquisition\Acquisition;
use App\Models\User;

class Assessment extends Model
{
    protected $table   = 'assessments';
    protected $guarded = ['id'];

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class)->withDefault();
    }

    public function position()
    {
        return $this->belongsTo(\App\Models\Position::class)->withDefault();
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }
    
    public function get_user(){
        $users = User::where('id', $this->name)
                ->OrderBy('name', 'ASC')
                ->get()->first();
        
        return $users;
    }
    
}
