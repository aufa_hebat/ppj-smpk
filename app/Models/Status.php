<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const ACTIVE       = 1;
    const CANCELLED    = 2;
    const TERMINATED   = 3;
    protected $guarded = ['id'];
    protected $table   = 'status';
}
