<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeriodType extends Model
{
    const DAY   = 1;
    const WEEK  = 2;
    const MONTH = 3;
    const YEAR  = 4;

    protected $guarded = ['id'];
    protected $table   = 'period_types';
}
