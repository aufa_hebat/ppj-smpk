<?php

namespace App\Models;

use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Model;

class SapVendorMapping extends Model
{
    protected $table   = 'sap_vendor_mappings';
    protected $guarded = ['id'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'ssm_no', 'ssm_no');
    }
}
