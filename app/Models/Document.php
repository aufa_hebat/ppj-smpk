<?php

namespace App\Models;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'documents';
    protected $guarded = ['id'];

    protected $fillable = ['document_id', 'document_types', 'document_path', 'document_name', 'mime'];
}
