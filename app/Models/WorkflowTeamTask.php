<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of WorkflowTeamTask.
 *
 * @author MITSB
 */
class WorkflowTeamTask extends \Illuminate\Database\Eloquent\Model
{
    //put your code here
    protected $guarded = ['id'];
    protected $table   = 'workflow_team_task';
    public $timestamps = false;
}
