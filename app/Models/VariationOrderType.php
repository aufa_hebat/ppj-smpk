<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariationOrderType extends Model
{
    protected $guarded = ['id'];
}
