<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class MOFBidang extends Model
{
    protected $table = 'company_mof';

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function syarikatMOFKhusus()
    {
        return $this->hasMany(MOFKhusus::class, 'areas_id');
    }
}
