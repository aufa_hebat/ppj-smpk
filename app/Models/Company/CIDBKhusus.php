<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class CIDBKhusus extends Model
{
    protected $table = 'company_cidb_khusus';

    public function syarikatcidbKategori()
    {
        return $this->belongsTo(CIDBCategory::class);
    }
}
