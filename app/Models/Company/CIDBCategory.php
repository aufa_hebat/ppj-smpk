<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class CIDBCategory extends Model
{
    protected $table = 'company_cidb_categories';

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function cidbspecial()
    {
        return $this->hasMany(CIDBKhusus::class, 'categories_id');
    }

    public function syarikatCIDBKhusus()
    {
        return $this->hasMany(CIDBKhusus::class, 'categories_id');
    }
}
