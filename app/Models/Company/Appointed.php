<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Appointed extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
