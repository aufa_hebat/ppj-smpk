<?php

namespace App\Models\Company;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use CleaniqueCoders\Profile\Traits\HasProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use DatatableTrait, ReferenceTrait, HasProfile;

    protected $table = 'company_owners';

    protected $guarded = ['id'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
