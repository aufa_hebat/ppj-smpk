<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class MOFKhusus extends Model
{
    protected $table = 'company_mof_khusus';

    public function syarikatMOFBidang()
    {
        return $this->belongsTo(MOFBidang::class, 'areas_id');
    }
}
