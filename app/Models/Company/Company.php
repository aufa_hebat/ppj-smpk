<?php

namespace App\Models\Company;

use App\Models\Bank;
use App\Models\StandardCode;
use App\Models\Document;
use App\Models\Status;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use CleaniqueCoders\Profile\Traits\HasProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use DatatableTrait, ReferenceTrait, HasProfile;

    protected $table   = 'companies';
    protected $guarded = ['id'];
    protected $appends = [
        'status_label',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with('owners', 'agents', 'cidbgrade', 'cidbcategory', 'cidbcategory.cidbspecial', 'mofcourse', 'mofcourse.syarikatMOFKhusus', 'status');
    }

    public function owners()
    {
        return $this->hasMany(Owner::class, 'company_id');
    }

    public function agents()
    {
        return $this->hasMany(Representative::class);
    }

    public function state()
    {
        return $this->hasOne(StandardCode::class, 'code_short', 'state')->where('code', '=', 'KOD_NEGERI');
    }

    public function bank()
    {
        return $this->hasMany(Bank::class, 'id', 'bank_name');
    }

    public function cidbgrade()
    {
        return $this->hasMany(CIDBGrade::class, 'company_id');
    }

    public function syarikatCIDBGreds()
    {
        return $this->hasMany(CIDBGrade::class, 'company_id');
    }

    public function cidbcategory()
    {
        return $this->hasMany(CIDBCategory::class, 'company_id');
    }

    public function syarikatCIDBKategori()
    {
        return $this->hasMany(CIDBCategory::class, 'company_id');
    }

    public function mofcourse()
    {
        return $this->hasMany(MOFBidang::class, 'company_id');
    }

    public function syarikatMOFBidang()
    {
        return $this->hasMany(MOFBidang::class, 'company_id');
    }

    public function vendors()
    {
        return $this->hasMany(SapVendorMapping::class, 'ssm_no', 'ssm_no');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function getStatusLabelAttribute()
    {
        return optional($this->status)->name;
    }

    public function gst()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'GST');
    }

    public function ssm()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'SSM');
    }

    public function cidb()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'CIDB');
    }

    public function bpku()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'BPKU');
    }

    public function spkk()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'SPKK');
    }

    public function mof()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'MOF');
    }

    public function bumiputra()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'Bumiputra');
    }

    public function kdn()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'KDN');
    }

    public function setSsmNoAttribute($value)
    {
        $this->attributes['ssm_no'] = strtoupper($value);
    }

    public function getSsmNoAttribute($value)
    {
        return strtoupper($value);
    }
}
