<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class CIDBGrade extends Model
{
    protected $table = 'company_cidb_grades';

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
