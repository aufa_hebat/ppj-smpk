<?php

namespace App\Models\Company;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use CleaniqueCoders\Profile\Traits\HasProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BPKU extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes, HasProfile;

    protected $table   = 'companies';
    protected $guarded = ['id'];
}
