<?php

namespace App\Models;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Box;
use App\Models\Company\Company;
use App\Models\Company\Owner;
use App\Models\Company\Representative;
use App\Models\Document;
use Illuminate\Database\Eloquent\Model;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;
    
    protected $table = 'sales';

    public function scopeWithDetails($query)
    {
        return $query->with('acquisition', 'company', 'box');
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function box()
    {
        return $this->hasOne(Box::class);
    }
    
    public function document()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'JUALAN/'.$this->id)->where('deleted_at',null);
    }
    
    public function sale_owner(){
        return $this->hasMany(Owner::class, 'ic_number', 'ic_number');
    }
    
    public function sale_agent(){
        return $this->hasMany(Representative::class, 'ic_number', 'ic_number');
    }
    
}
