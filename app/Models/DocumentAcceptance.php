<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document;

class DocumentAcceptance extends Model
{
    protected $table = 'document_acceptances';

    protected $guarded = ['id'];

    
    public function doc_acceptance()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'Penerimaan Dokumen');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
