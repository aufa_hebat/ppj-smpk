<?php //

namespace App\Models\Acquisition;

use App\Models\Company\Company;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppointedCompany extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table = 'appointed_company';

    protected $fillable = ['acquisition_id', 'company_id', 'offered_price'];

    protected $dates = [
        'termination_at',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with('acquisition', 'company', 'acquisition.approval');
    }

    public function eot()
    {
        return $this->hasMany(Eot::class);
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function getBonAmountAttribute()
    {
        $sale_id = $this->acquisition->sales->where('company_id', $this->attributes['company_id'])->where('deleted_at', null)->pluck('id')->first();
        $box = $this->acquisition->boxes->where('sale_id', $sale_id)->where('deleted_at', null)->first();

        return rate('bon', $this->acquisition->approval->type->name, $this->attributes['offered_price'], $box->period_type->id, $box->period);
    }

    public function terminationType()
    {
        return $this->belongsTo(\App\Models\TerminationType::class);
    }
}
