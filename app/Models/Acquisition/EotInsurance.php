<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class EotInsurance extends Model
{
    protected $guarded = ['id'];
    protected $casts   = [
        'public_received_date'          => 'datetime:Y-m-d',
        'public_start_date'             => 'datetime:Y-m-d',
        'public_expired_date'           => 'datetime:Y-m-d',
        'public_proposed_date'          => 'datetime:Y-m-d',
        'public_approval_proposed_date' => 'datetime:Y-m-d',
        'public_approval_received_date' => 'datetime:Y-m-d',

        'work_received_date'          => 'datetime:Y-m-d',
        'work_start_date'             => 'datetime:Y-m-d',
        'work_expired_date'           => 'datetime:Y-m-d',
        'work_proposed_date'          => 'datetime:Y-m-d',
        'work_approval_proposed_date' => 'datetime:Y-m-d',
        'work_approval_received_date' => 'datetime:Y-m-d',

        'compensation_received_date'          => 'datetime:Y-m-d',
        'compensation_start_date'             => 'datetime:Y-m-d',
        'compensation_expired_date'           => 'datetime:Y-m-d',
        'compensation_proposed_date'          => 'datetime:Y-m-d',
        'compensation_approval_proposed_date' => 'datetime:Y-m-d',
        'compensation_approval_received_date' => 'datetime:Y-m-d',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with(
            'sst', 'compensation_insurance',
            'public_insurance', 'work_insurance',
            'documents_publicLiability',
            'documents_compensation',
            'documents_work'
        );
    }

    public function eot()
    {
        return $this->belongsTo(Eot::class, 'eot_id');
    }

    public function compensation_insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'compensation_insurance_name');
    }

    public function public_insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'public_insurance_name');
    }

    public function work_insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'work_insurance_name');
    }

    public function documents_publicLiability()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')
            ->where('document_types', 'EOT_INSURANCE_PUBLIC');
    }

    public function documents_work()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')
            ->where('document_types', 'EOT_INSURANCE_WORK');
    }

    public function documents_compensation()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')
            ->where('document_types', 'EOT_INSURANCE_COMPENSATION');
    }
}
