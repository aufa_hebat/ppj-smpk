<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Cnc extends Model
{
    protected $guarded = ['id'];

    protected $dates = [
        'signature_date',
        'test_award_date',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'CNC');
    }
}
