<?php

namespace App\Models\Acquisition;

use App\Models\Company\Company;
use App\Models\Document;
use App\Models\Company\Owner;
use App\Models\PeriodType;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\GetTotalCompletionTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sst extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes,
        \App\Traits\Relationships\TaskableTrait,
        GetTotalCompletionTrait;

    const NUMBER_OF_PROPOSED_DAYS = 14;

    protected $table = 'ssts';

    protected $guarded = ['id'];

    protected $dates = [
        'start_working_date',
        'end_working_date',
        'termination_at',
    ];

    protected $casts = [
        'ins_pli_start_date' => 'datetime:Y-m-d',
        'ins_pli_end_date'   => 'datetime:Y-m-d',

        'ins_work_start_date' => 'datetime:Y-m-d',
        'ins_work_end_date'   => 'datetime:Y-m-d',

        'ins_ci_start_date' => 'datetime:Y-m-d',
        'ins_ci_end_date'   => 'datetime:Y-m-d',

        'sofa_signature_date' => 'datetime:Y-m-d',
        'letter_date'         => 'datetime:Y-m-d',
        'vp_sign_date'        => 'datetime:Y-m-d',
        'al_issue_date'       => 'datetime:Y-m-d',
        'al_contractor_date'  => 'datetime:Y-m-d',
    ];

    protected $appends = [
        'taskable_name', 'taskable_label',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'acquisition', 'acquisition.appointed',
            'company', 'owner',
            'bon', 'insurance', 'deposit', 'document',
            'acquisition.approval', 'acquisition.approval.type',
            'acquisition.approval.cidbQualifications'
        );
    }

    public function scopeWithIpcDetails($query)
    {
        return $query->with(
            'ipc',
            'ipc.ipcInvoice',
            'ipc.ipcInvoice.ipcBq',
            'ipc.ipcMaterial'
        );
    }

    public function scopeWithCertificates($query)
    {
        return $this->with('cmgd', 'cnc', 'cpc', 'cpo');
    }

    public function scopeWithVariationOrders($query)
    {
        return $query->with(
            'variationOrders',
            'variationOrders.categories',
            'variationOrders.categories.elements'
        );
    }

    public function scopeWithEots()
    {
        return $this->with(
            'eots',
            'eots.eot_approve',
            'eots.eot_bon',
            'eots.eot_details',
            'eots.eot_insurance'
        );
    }

    public function scopeWithSiteVisits($query)
    {
        return $query->with('siteVisits');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class)->withDefault();
    }

    public function company()
    {
        return $this->belongsTo(Company::class)->withDefault();
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class, 'company_owner_id')->withDefault();
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function period_type()
    {
        return $this->belongsTo(PeriodType::class, 'period_type_id')->withDefault();
    }

    public function bon()
    {
        return $this->hasOne(Bon::class);
    }

    public function insurance()
    {
        return $this->hasOne(Insurance::class);
    }

    public function deposit()
    {
        return $this->hasOne(Deposit::class);
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'SST');
    }
    
     public function document2()
    {
        return $this->hasOne(\App\Models\Acquisition\Document::class);
    }

    public function ipc()
    {
        return $this->hasMany(Ipc::class)->where('deleted_at',null);
    }

    public function ipcInvoice()
    {
        return $this->hasMany(IpcInvoice::class)->where('deleted_at',null)->orderby('sap_date', 'desc');
    }

    public function ipcBq()
    {
        return $this->hasMany(IpcBq::class)->where('deleted_at',null);
    }

    public function ipcMaterial()
    {
        return $this->hasMany(IpcMaterial::class)->where('deleted_at',null);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class)->withDefault();
    }

    public function terminationType()
    {
        return $this->belongsTo(\App\Models\TerminationType::class);
    }

    public function subContract()
    {
        return $this->hasMany(SubContract::class);
    }

    public function cmgd()
    {
        return $this->hasOne(\App\Models\Acquisition\Cmgd::class);
    }

    public function cnc()
    {
        return $this->hasOne(\App\Models\Acquisition\Cnc::class);
    }

    public function cpc()
    {
        return $this->hasOne(\App\Models\Acquisition\Cpc::class);
    }

    public function cpo()
    {
        return $this->hasOne(\App\Models\Acquisition\Cpo::class);
    }

    public function release()
    {
        return $this->hasOne(\App\Models\Acquisition\Release::class);
    }

    public function variationOrders()
    {
        return $this->hasMany(\App\Models\VO\VO::class, 'sst_id');
    }
    
    public function ppjhk_active()
    {
        return $this->hasMany(\App\Models\VO\PPJHK\Ppjhk::class, 'sst_id')->where('deleted_at',null)->where('status',1);
    }

    public function eots()
    {
        return $this->hasMany(\App\Models\Acquisition\Eot::class);
    }

    public function siteVisits()
    {
        return $this->hasMany(\App\Models\Acquisition\Monitoring\SiteVisit::class);
    }

    public function sofa()
    {
        return $this->hasOne(\App\Models\Acquisition\Sofa::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'SST');
    }

    public function getHasDocumentsAttribute()
    {
        return $this->documents->count() > 0;
    }

    // $sst->contracts_six_month_to_end
    public function getContractsSixMonthToEndAttribute()
    {
        return $this->contractsSixMonthToEnd()->count();
    }

    // $sst->payment_progress_exceed_physical_progress
    public function getPaymentProgressExceedPhysicalProgressAttribute()
    {
        return $this->paymentProgressExceedPhysicalProgress()->get();
    }

    // $sst->expired_contracts_with_account_not_close
    public function getExpiredContractsWithAccountNotCloseAttribute()
    {
        return $this->expiredContractsWithAccountNotClose()->get();
    }

    public function getHasBonDocumentsAttribute()
    {
        return ! empty($this->bon) && $this->bon->has_documents;
    }

    public function getHasInsuranceDocumentsAttribute()
    {
        return ! empty($this->insurance) && $this->insurance->has_documents;
    }

    public function getTotalPaidAttribute()
    {
        return $this->ipc->sum->ipc_invoice_amount;
    }

    public function getTotalIpcAttribute()
    {
        return $this->ipc->count();
    }

    public function getTotalPaymentAmountAttribute()
    {
        if (0 === $this->acquisition->appointed->count()) {
            return 0;
        }

        return $this->acquisition->appointed->sum->offered_price;
    }

    public function getPaymentPaidPercentageAttribute()
    {
        if (0 === $this->total_payment_amount) {
            return 0;
        }

        return ($this->total_paid / $this->total_payment_amount) * 100;
    }

    public function getIsFullyPaidAttribute()
    {
        if (0 === $this->total_payment_amount) {
            return false;
        }

        return $this->total_paid == $this->total_payment_amount;
    }

    public function getIsPartiallyPaidAttribute()
    {
        if (0 === $this->total_payment_amount) {
            return false;
        }

        return $this->total_paid < $this->total_payment_amount;
    }

    public function getIsMoreThanFiftyPercentPaidAttribute()
    {
        return $this->payment_paid_percentage >= 50;
    }

    public function getIsNotPayAnythingYetAttribute()
    {
        return 0 == $this->total_paid;
    }

    public function getTaskableNameAttribute(): string
    {
        return $this->acquisition->taskable_name;
    }

    public function getTaskableLabelAttribute(): string
    {
        return $this->contract_no;
    }

    public function getWorkflowSlugName(): string
    {
        return 'post-kontrak-surat-setuju-terima-sst-surat-setuju-terima-sst';
    }
}
