<?php

namespace App\Models\Acquisition\Approval;

use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'authorities';

    public function getNameAttribute()
    {
        if (isset($this->attributes['code']) && isset($this->attributes['name'])) {
            return $this->attributes['code'] . ' - ' . $this->attributes['name'];
        }

        return '';
    }

    public function getActualNameAttributes()
    {
        return $this->attributes['name'];
    }
}
