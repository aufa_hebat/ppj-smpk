<?php

namespace App\Models\Acquisition\Approval;

use App\Models\BudgetCode;
use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'acquisition_approval_financials';

    public function getBusinessAttribute()
    {
        return $this->businessArea->name;
    }

    public function getFunctionalAttribute()
    {
        return $this->functionalArea->name;
    }

    public function getCostAttribute()
    {
        return $this->costCenter->name;
    }

    public function getFunAttribute()
    {
        return $this->fund->name;
    }

    public function getFundedAttribute()
    {
        return $this->fundedProgramme->name;
    }

    public function getAccountAttribute()
    {
        return $this->glAccount->name;
    }

    public function businessArea()
    {
        return $this->belongsTo(BudgetCode::class, 'business_area_id')->withDefault();
    }

    public function functionalArea()
    {
        return $this->belongsTo(BudgetCode::class, 'functional_area_id')->withDefault();
    }

    public function costCenter()
    {
        return $this->belongsTo(BudgetCode::class, 'cost_center_id')->withDefault();
    }

    public function fund()
    {
        return $this->belongsTo(BudgetCode::class, 'fund_id')->withDefault();
    }

    public function fundedProgramme()
    {
        return $this->belongsTo(BudgetCode::class, 'funded_programme_id')->withDefault();
    }

    public function glAccount()
    {
        return $this->belongsTo(BudgetCode::class, 'gl_account_id')->withDefault();
    }
}
