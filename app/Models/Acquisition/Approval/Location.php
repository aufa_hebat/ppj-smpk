<?php

namespace App\Models\Acquisition\Approval;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'acquisition_approval_locations';

    public function location()
    {
        return $this->belongsTo(\App\Models\Location::class);
    }

    public function getNameAttribute()
    {
        return optional($this->location)->name;
    }
}
