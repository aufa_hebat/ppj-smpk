<?php

namespace App\Models\Acquisition\Approval;

use App\Models\Acquisition\Approval;
use App\Models\MOFCode;
use Illuminate\Database\Eloquent\Model;

class MOFQualification extends Model
{
    const ATAU = 0;
    const DAN  = 1;

    protected $guarded = ['id'];
    protected $table   = 'acquisition_approval_mof';

    public function acquisitionApproval()
    {
        return $this->belongsTo(Approval::class, 'acquisition_approval_id');
    }

    public function code()
    {
        return $this->belongsTo(MOFCode::class, 'mof_code_id');
    }
}
