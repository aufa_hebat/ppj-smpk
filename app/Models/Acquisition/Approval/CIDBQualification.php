<?php

namespace App\Models\Acquisition\Approval;

use App\Models\Acquisition\Approval;
use App\Models\CIDBCode;
use Illuminate\Database\Eloquent\Model;

class CIDBQualification extends Model
{
    const ATAU = 0;
    const DAN  = 1;

    protected $guarded = ['id'];
    protected $table   = 'acquisition_approval_cidb';

    public function acquisitionApproval()
    {
        return $this->belongsTo(Approval::class, 'acquisition_approval_id');
    }

    public function code()
    {
        return $this->belongsTo(CIDBCode::class, 'cidb_code_id');
    }
}
