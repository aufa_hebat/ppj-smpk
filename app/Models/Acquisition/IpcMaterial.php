<?php

namespace App\Models\Acquisition;

use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpcMaterial extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'ipc_materials';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ipc()
    {
        return $this->belongsTo(Ipc::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }
}
