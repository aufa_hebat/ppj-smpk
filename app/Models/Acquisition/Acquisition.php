<?php

namespace App\Models\Acquisition;

use App\Models\Acquisition\Review;
use App\Models\Addendum\Addendum;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Status;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\GetTotalCompletionTrait;
use App\Traits\HasMediaExtended;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Acquisition extends Model implements HasMedia, Auditable
{
    use DatatableTrait, ReferenceTrait,
        SoftDeletes, HasMediaExtended,
        GetTotalCompletionTrait,
        \App\Traits\Relationships\TaskableTrait,
        \OwenIt\Auditing\Auditable;

    const ALL = 'ALL';

    protected $dates = [
        'advertised_at',
        'briefing_at',
        'closed_at',
        'closed_sale_at',
        'start_sale_at',
        'send_acquisition_at',
        'visit_date_time',
        'cancelled_at',
    ];

    protected $guarded = ['id'];

    protected $appends = [
        'taskable_name', 'taskable_label',
    ];

    /**
     * Fields that should not be empty.
     */
    protected $taskable_columns = [
        'title', 'reference', 'department_id', 'acquisition_category_id', 'acquisition_approval_id', 'user_id',
    ];
    
    public function scopeWithDetails($query)
    {
        return $query->with('user', 'approval', 'briefings', 'status');
    }

    public function scopeWithBillOfQuantity($query)
    {
        return $query->with('bqElements', 'bqItems', 'bqSubItems');
    }

    public function scopeWithAddendums($query)
    {
        return $query->with('addenda', 'addenda.items');
    }

    public function scopeChartGroupByDepartment($query)
    {
        
        return $query->get()->groupBy('department_id')->mapWithKeys(function ($acq) {
            return [$acq->first()->department->name => $acq->count()];
        });
       // return $query->get()->groupBy('department_id');
    }

    public function scopeTotalSalesAmount($query, $year = null, $month = null)
    {
        if (! is_null($year)) {
            $query = $query->whereYear('created_at', $year);
        }

        if (! is_null($month)) {
            $query = $query->whereMonth('created_at', $month);
        }

        return $query->has('sales')->sum('total_document_price');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'acquisition_approval_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'acquisition_id')->where('type','Acquisitions')->where('deleted_at',null);
    }

    public function addenda()
    {
        return $this->hasMany(Addendum::class, 'acquisition_id');
    }

    public function briefings()
    {
        return $this->hasMany(Briefing::class)->where('deleted_at',null);
    }

    public function referencePrefix()
    {
        return 'PPJ/' . $this->department_code . '/SH(S)';
    }

    public function boxes()
    {
        return $this->hasMany(Box::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class)->whereNotNull('receipt_no')->where('deleted_at',null);
    }

    public function evaluation()
    {
        return $this->hasOne(Evaluation::class);
    }

    public function appointed()
    {
        return $this->hasMany(AppointedCompany::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'acquisition_category_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function bqElements()
    {
        return $this->hasMany(Element::class)->where('deleted_at',null);
    }

    public function bqItems()
    {
        return $this->hasMany(Item::class)->where('deleted_at',null);
    }

    public function bqSubItems()
    {
        return $this->hasMany(SubItem::class)->where('deleted_at',null);
    }

    public function committeeApprovals()
    {
        return $this->hasMany(\App\Models\Committee\Approval::class);
    }

    public function tasks()
    {
        return $this->hasMany(\App\Models\Task::class);
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function sst()
    {
        return $this->hasOne(Sst::class)->where('deleted_at',null);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function getDepartmentCodeAttribute()
    {
        if (! empty($this->department) && ! empty($this->department->code)) {
            return $this->department->code;
        }

        return self::ALL;
    }

    public function getLabelAttribute()
    {
        return $this->reference . ' : ' . $this->title;
    }

    public function getTaskableNameAttribute(): string
    {
        return $this->title ?? '';
    }

    public function getTaskableLabelAttribute(): string
    {
        return $this->reference;
    }

    public function getWorkflowSlugName(): string
    {
        return 'pra-kontrak-dokumen-perolehan-maklumat-kelulusan-perolehan';
    }
    
    public function doc_sitevisit()
    {
        return $this->hasOne(\App\Models\Document::class, 'document_id')->where('document_types', 'ACQ_VISIT')->where('deleted_at',null);
    }
    
    public function history_semakan_ipc(){
        return $this->hasMany(Review::class,'acquisition_id')->where('type','IPC')->where('ipc_id','!=',null)->where('deleted_at',null);
    }

    public function history_semakan_sst(){
        return $this->hasMany(Review::class,'acquisition_id')->where('type','SST')->where('deleted_at',null);
    }

    public function history_semakan_kontrak(){
        return $this->hasMany(Review::class,'acquisition_id')->where('type','Dokumen Kontrak')->where('deleted_at',null);
    }

    public function doc_cancel_acquisition()
    {
        return $this->hasOne(\App\Models\Document::class, 'document_id')->where('document_types', 'ACQ_CANCEL')->where('deleted_at',null);
    }
}
