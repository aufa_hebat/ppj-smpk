<?php

namespace App\Models\Acquisition\Monitoring;

use App\Traits\DatatableTrait;
use Illuminate\Database\Eloquent\Model;

class ContractProgress extends Model
{
    use DatatableTrait;
    
    protected $table   = 'contract_progress';
    protected $guarded = ['id'];

    protected $dates = [
        'on_plan_date',
    ];

    public function acquisition()
    {
        return $this->belongsTo(\App\Models\Acquisition\Acquisition::class);
    }
    
    public function siteVisit(){
        return $this->belongsTo(\App\Models\Acquisition\Monitoring\SiteVisit::class, 'site_visit_id');
    }
    
    public function paymentMonitor(){
        return $this->belongsTo(\App\Models\Acquisition\Monitoring\PaymentMonitor::class);
    }
    
    public function siteVisitData(){
        return $this->hasOne(\App\Models\Acquisition\Monitoring\SiteVisit::class, 'site_visit_id');
    }
    
    
    
}
