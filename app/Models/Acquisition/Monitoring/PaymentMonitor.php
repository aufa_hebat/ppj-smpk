<?php

namespace App\Models\Acquisition\Monitoring;

use App\Traits\DatatableTrait;
use Illuminate\Database\Eloquent\Model;

class PaymentMonitor extends Model
{
    use DatatableTrait;
    
    protected $table   = 'payment_monitor';
    protected $guarded = ['id'];

    protected $dates = [
        'payment_at',
    ];

    public function acquisition()
    {
        return $this->belongsTo(\App\Models\Acquisition\Acquisition::class);
    }
    
   
    
    

}
