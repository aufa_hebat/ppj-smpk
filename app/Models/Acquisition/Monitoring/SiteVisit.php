<?php

namespace App\Models\Acquisition\Monitoring;

use App\Traits\DatatableTrait;
use App\Traits\HasMediaExtended;
use App\Traits\HasThumbnail;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class SiteVisit extends Model implements HasMediaConversions
{
    use DatatableTrait, HasMediaExtended, HasThumbnail;

    protected $guarded = ['id'];

    protected $dates = [
        'visited_at',
        'visit_date',
    ];

    public function scopeDatatable($query)
    {
        return $query
            ->latest()
            ->withDetails();
    }

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'department',
            'acquisition', 'acquisitionApproval',
            'sst', 'visitedBy'
        );
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function acquisition()
    {
        return $this->belongsTo(\App\Models\Acquisition\Acquisition::class);
    }

    public function acquisitionApproval()
    {
        return $this->belongsTo(\App\Models\Acquisition\Approval::class);
    }

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function visitedBy()
    {
        return $this->belongsTo(\App\Models\User::class, 'visited_by');
    }
    
   
}
