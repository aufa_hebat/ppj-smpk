<?php

namespace App\Models\Acquisition;

use App\Models\PeriodType;
use App\Models\Sale;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Box extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table = 'acquisition_boxes';

    protected $fillable = [
        'user_id',
        'sale_id',
        'no',
        'period',
        'period_type_id',
        'is_stated',
        'amount',
    ];

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function period_type()
    {
        return $this->belongsTo(PeriodType::class, 'period_type_id');
    }
}
