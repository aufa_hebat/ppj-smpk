<?php

namespace App\Models\Acquisition;

use App\Models\Company\Company;
use App\Models\Company\Owner;
use App\Models\Company\Representative;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;

class AcquisitionBriefing extends Model
{
    use DatatableTrait, ReferenceTrait;

    protected $table   = 'acquisition_briefings';
    protected $guarded = ['id'];

    public function scopeWithDetails($query)
    {
        return $query->with('company', 'acquisition', 'owners');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class,'company_owner_id');
    }
    
    public function agent()
    {
        return $this->belongsTo(Representative::class,'company_owner_id');
    }
}
