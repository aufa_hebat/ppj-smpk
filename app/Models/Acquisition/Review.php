<?php

namespace App\Models\Acquisition;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\VO\PPJHK\Ppjhk;
use App\Models\VO\VO;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\Ipc;

class Review extends Model
{
    protected $table = 'acquisition_reviews';

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class, 'acquisition_id');
    }

    public function eot()
    {
        return $this->belongsTo(EOT::class, 'eot_id');
    }

    public function ipc()
    {
        return $this->belongsTo(IPC::class, 'ipc_id');
    }

    public function ppk()
    {
        return $this->belongsTo(VO::class, 'ppk_id');
    }

    public function ppjhk()
    {
        return $this->belongsTo(Ppjhk::class, 'ppjhk_id');
    }

    public function request()
    {
        return $this->belongsTo(User::class, 'requested_by');
    }

    public function approve()
    {
        return $this->belongsTo(User::class, 'approved_by');
    }

    public function task()
    {
        return $this->belongsTo(User::class, 'task_by');
    }

    public function lawtask()
    {
        return $this->belongsTo(User::class, 'law_task_by');
    }

    public function create()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
