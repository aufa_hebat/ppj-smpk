<?php

namespace App\Models\Acquisition;

use App\Traits\HasDatatable;
use App\Traits\HasMediaExtended;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Activity extends Model implements HasMedia
{
    use HasDatatable, HasMediaExtended;

    protected $guarded               = ['id'];
    protected $table                 = 'acquisition_activities';
    protected $media_collection_name = 'acquisition-activity';

    protected $dates = [
        'scheduled_started_at',
        'scheduled_ended_at',
        'current_started_at',
        'current_ended_at',
        'actual_started_at',
        'actual_ended_at',
    ];

    public function sst()
    {
        return $this->belongsTo(\App\Models\Acquisition\Sst::class);
    }

    public function getDeficitAttribute()
    {
        return $this->actual_completion - $this->scheduled_completion;
    }
}
