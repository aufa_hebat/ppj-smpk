<?php

namespace App\Models\Acquisition;

use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Model;

class SubContract extends Model
{
    protected $table = 'acquisition_sub_contracts';

    protected $fillable = [
        'sst_id',
        'user_id',
        'hashslug',
        'company_id',
        'amount',
        'description',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class)->withDefault();
    }

    public function invoices()
    {
        return $this->hasMany(IpcInvoice::class, 'company_id', 'company_id')->where('sst_id', $this->sst_id);
    }
}
