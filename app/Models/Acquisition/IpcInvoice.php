<?php

namespace App\Models\Acquisition;

use App\Models\Acquisition\Approval\Financial;
use App\Models\BudgetCode;
use App\Models\Company\Company;
use App\Models\Document;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpcInvoice extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'ipc_invoices';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function ipc()
    {
        return $this->belongsTo(Ipc::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function ipcBq()
    {
        return $this->hasMany(IpcBq::class)->where('deleted_at',null);
    }

    public function financial()
    {
        return $this->belongsTo(Financial::class, 'acquisition_approval_financial_id');
    }
    
    public function glAccount()
    {
        return $this->belongsTo(BudgetCode::class, 'acquisition_approval_financial_id')->withDefault();
    }

    public function getTotalBqAdjustAmountAttribute()
    {
        if (! $this->ipcBq) {
            return 0;
        }

        return $this->ipcBq->sum->adjust_amount;
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'INVOICE/'.$this->id)->where('deleted_at',null);
    }
}
