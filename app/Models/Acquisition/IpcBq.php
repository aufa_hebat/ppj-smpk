<?php

namespace App\Models\Acquisition;

use App\Models\BillOfQuantity\SubItem;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\Element;
use App\Models\VO\SubItem as WpsSub;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpcBq extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'ipc_bqs';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function ipcInvoice()
    {
        return $this->belongsTo(IpcInvoice::class);
    }

    public function ipcBqSub()
    {
        return $this->belongsTo(SubItem::class, 'bq_subitem_id');
    }
    
    public function ipcBqItem()
    {
        return $this->belongsTo(Item::class, 'bq_item_id');
    }
    
    public function ipcBqElement()
    {
        return $this->belongsTo(Element::class,'bq_element_id');
    }
    
    public function ipcBqWpsSub()
    {
        return $this->belongsTo(WpsSub::class,'bq_subitem_id');
    }
}
