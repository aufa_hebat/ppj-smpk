<?php

namespace App\Models\Acquisition;

use App\Models\Acquisition\Approval\Authority;
use App\Models\Acquisition\Approval\CIDBQualification;
use App\Models\Acquisition\Approval\MOFQualification;
use App\Models\Acquisition\Approval\Financial;
use App\Models\Acquisition\Approval\Location;
use App\Models\Department;
use App\Models\PeriodType;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\GetTotalCompletionTrait;
use App\Traits\HasMediaExtended;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Approval extends Model implements HasMedia, Auditable
{
    use DatatableTrait, HasMediaExtended,
        ReferenceTrait, SoftDeletes,
        GetTotalCompletionTrait,
        \App\Traits\Relationships\TaskableTrait,
        \OwenIt\Auditing\Auditable;

    const ALL = 'ALL';

    protected $table = 'acquisition_approvals';

    protected $guarded = ['id'];

    protected $media_collection_key = 'acquisition.approval';

    protected $dates = [
        'prepared_at', 'verified_at', 'approved_at',
    ];

    protected $appends = [
        'taskable_name', 'taskable_label',
    ];

    /**
     * Fields that should not be empty.
     */
    protected $taskable_columns = [
        'title', 'year', 'description', 'reference',
        'file_reference', 'period_length',
        'department_id', 'acquisition_type_id',
        'period_type_id', 'authority_id', 'user_id',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with('user', 'type', 'department', 'period_type', 'allocation_resource');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function allocation_resource()
    {
        return $this->belongsTo(AllocationResource::class, 'allocation_resource_id')->withDefault();
    }

    public function authority()
    {
        return $this->belongsTo(Authority::class, 'authority_id')->withDefault();
    }

    public function financeOfficer()
    {
        return $this->belongsTo(User::class, 'finance_officer_id')->withDefault();
    }

    public function acquisitions()
    {
        return $this->hasMany(Acquisition::class, 'acquisition_approval_id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'acquisition_type_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function period_type()
    {
        return $this->belongsTo(PeriodType::class, 'period_type_id');
    }

    public function financials()
    {
        return $this->hasMany(Financial::class, 'acquisition_approval_id');
    }

    public function locations()
    {
        return $this->hasMany(Location::class, 'acquisition_approval_id');
    }

    public function cidbQualifications()
    {
        return $this->hasMany(CIDBQualification::class, 'acquisition_approval_id');
    }

    public function mofQualifications()
    {
        return $this->hasMany(MOFQualification::class, 'acquisition_approval_id');
    }

    public function referencePrefix()
    {
        return 'PPJ/KELULUSAN/' . $this->department_code;
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function getLocationsToStringAttribute()
    {
        $locations = [];
        $this->locations->each(function ($location) use (&$locations) {
            $locations[] = $location->location->name;
        });

        return implode(', ', $locations);
    }

    public function getDepartmentCodeAttribute()
    {
        if (! empty($this->user->department) && ! empty($this->user->department->code)) {
            return $this->user->department->code;
        }

        return self::ALL;
    }

    public function getTaskableNameAttribute(): string
    {
        return $this->title;
    }

    public function getTaskableLabelAttribute(): string
    {
        return $this->reference;
    }

    public function getWorkflowSlugName(): string
    {
        return 'pra-kontrak-kelulusan-perolehan-maklumat-kelulusan';
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'ACQ_APRV');
    }
}
