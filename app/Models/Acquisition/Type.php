<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    const PROVIDER    = 'Bekalan';
    const WORK        = 'Kerja';
    const SERVICE     = 'Perkhidmatan';
    const MAINTENANCE = 'Penyelenggaraan';

    protected $guarded = ['id'];
    protected $table   = 'acquisition_types';
}
