<?php

namespace App\Models\Acquisition;

use App\Models\Department;
use App\Models\PeriodType;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;

class Eot extends Model
{
    use DatatableTrait, ReferenceTrait;

    const ALL           = 1;
    const PARTIAL       = 2;
    const ALL_LABEL     = 'Keseluruhan';
    const PARTIAL_LABEL = 'Sebahagian';

    protected $guarded = ['id'];

    protected $casts = [
        'original_end_date' => 'datetime:Y-m-d',
        'extended_end_date' => 'datetime:Y-m-d',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with(
            'appointed_company', 'period_type',
            'eot_approve', 'eot_bon',
            'eot_insurance', 'eot_details',
            'department'
        );
    }

    public function appointed_company()
    {
        return $this->belongsTo(AppointedCompany::class, 'appointed_company_id');
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class, 'sst_id');
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class, 'acquisition_id');
    }

    public function period_type()
    {
        return $this->belongsTo(PeriodType::class, 'period_type_id');
    }

    public function eot_details()
    {
        return $this->hasMany(EotDetail::class);
    }

    public function eot_approve()
    {
        return $this->hasOne(EotApprove::class);
    }

    public function eot_bon()
    {
        return $this->hasOne(EotBon::class);
    }

    public function eot_insurance()
    {
        return $this->hasOne(EotInsurance::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function getSectionLabelAttribute()
    {
        return (self::ALL == $this->section) ? self::ALL_LABEL : self::PARTIAL_LABEL;
    }

    public function getPeriodLabelAttribute()
    {
        return $this->period . ' ' . $this->period_type->name;
    }

    public function support_documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'SOKONGAN');
    }

    public function executif_documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'EKSEKUTIF');
    }

    public function president_documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'KERTAS_TIMBANGAN');
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'KEPUTUSAN_EOT');
    }
}
