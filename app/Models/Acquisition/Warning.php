<?php

namespace App\Models\Acquisition;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warning extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'warnings';
    protected $guarded = ['id'];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function terminationType()
    {
        return $this->belongsTo(\App\Models\TerminationType::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'WARN');
    }
}
