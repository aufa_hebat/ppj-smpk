<?php

namespace App\Models\Acquisition;

use App\Models\Addendum;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'acquisitions';

    public function approval()
    {
        return $this->belongsTo(Approval::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function addendums()
    {
        return $this->hasMany(Addendum::class);
    }
}
