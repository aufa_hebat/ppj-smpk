<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;
use App\Models\PeriodType;

class EotDetail extends Model
{
    protected $guarded  = ['id'];
    protected $table    = 'eot_details';
    protected $fillable = ['user_id', 'eot_id', 'reasons', 'clause', 'periods', 'period_type_id'];

    public function eot()
    {
        return $this->belongsTo(Eot::class, 'eot_id');
    }
    
    public function period_type()
    {
        return $this->belongsTo(PeriodType::class, 'period_type_id');
    }
}
