<?php

namespace App\Models\Acquisition;

use App\Models\Company\Representative;
use App\Models\Company\Company;
use App\Models\Company\Owner;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Briefing extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'acquisition_briefings';

    public function scopeWithDetails($query)
    {
        return $query->with('user', 'company', 'acquisition', 'owner', 'briefing_staff', 'visit_staff', 'agent');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function briefing_staff()
    {
        return $this->belongsTo(User::class,'briefing_staff_id');
    }

    public function visit_staff()
    {
        return $this->belongsTo(User::class,'visit_staff_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class, 'company_owner_id')->withDefault();
    }

    public function agent()
    {
        return $this->belongsTo(Representative::class, 'company_owner_id')->withDefault();
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }
    
    public function staff_briefing()
    {
        return $this->belongsTo(User::class, 'briefing_staff_id');
    }
    
    public function staff_visit()
    {
        return $this->belongsTo(User::class, 'visit_staff_id');
    }
}
