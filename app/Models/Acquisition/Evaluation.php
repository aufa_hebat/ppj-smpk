<?php

namespace App\Models\Acquisition;

use App\Models\Document;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluation extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table = 'acquisition_evaluations';

    protected $fillable = ['acquisition_id', 'meeting_no', 'meeting_date'];

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'EVALUATION');
    }
}
