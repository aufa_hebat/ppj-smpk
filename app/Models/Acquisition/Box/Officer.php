<?php

namespace App\Models\Acquisition\Box;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    protected $table = 'box_officer';

    public function acquisition()
    {
        return $this->belongsTo(Detail::class);
    }
}
