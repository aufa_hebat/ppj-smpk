<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Acceptance extends Model
{
    protected $table   = 'acquisition_acceptances';
    protected $guarded = ['id'];
}
