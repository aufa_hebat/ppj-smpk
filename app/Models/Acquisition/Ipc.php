<?php

namespace App\Models\Acquisition;

use App\Models\Acquisition\Review;
use App\Models\Document;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ipc extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'ipcs';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function doc_material()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/MATERIAL/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_perkeso()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/PERKESO/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_levi()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/LEVI/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_sijil()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/SIJIL_IPC/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_summary()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/SUMMARY_IPC/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_status()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/STATUS_IPC/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_detail()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/DETAIL_IPC/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_sofa()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/SOFA_IPC/'.$this->id)->where('deleted_at',null);
    }
    
    public function doc_statuori()
    {
        return $this->hasOne(Document::class, 'document_id')->where('document_types', 'IPC/STATUORI_IPC/'.$this->id)->where('deleted_at',null);
    }

    public function doc_acceptance()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'Penerimaan Dokumen');
    }

    public function scopeWithDetails($query)
    {
        return $query->with(
            'user', 'sst',
            'ipcInvoice', 'ipcMaterial'
        );
    }

    public function ipcInvoice()
    {
        return $this->hasMany(IpcInvoice::class)->where('deleted_at',null);
    }

    public function ipcMaterial()
    {
        return $this->hasMany(IpcMaterial::class)->where('deleted_at',null);
    }
    
    public function ipcVo()
    {
        return $this->hasMany(IpcVo::class)->where('deleted_at',null);
    }
    
    public function history_semakan(){
        return $this->hasMany(Review::class)->where('type','IPC')->where('ipc_id',$this->id)->where('deleted_at',null);
    }

    public function getIpcInvoiceAmountAttribute()
    {
        if (0 == $this->ipcInvoice->count()) {
            return 0;
        }

        return $this->ipcInvoice->sum->total_bq_adjust_amount;
    }

}
