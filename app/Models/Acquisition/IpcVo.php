<?php

namespace App\Models\Acquisition;

use App\Models\VO\PPJHK\Element;
use App\Models\VO\PPJHK\Item;
use App\Models\VO\PPJHK\SubItem;
use App\Models\VO\Cancel;
use App\Models\User;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpcVo extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'ipc_vos';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }
    
    public function ipc()
    {
        return $this->belongsTo(Ipc::class);
    }

    public function ipcVoSub()
    {
        return $this->belongsTo(SubItem::class, 'ppjhk_subitem_id');
    }
    
    public function ipcVoItem()
    {
        return $this->belongsTo(Item::class, 'ppjhk_item_id');
    }
    
    public function ipcVoElement()
    {
        return $this->belongsTo(Element::class, 'ppjhk_element_id');
    }
}
