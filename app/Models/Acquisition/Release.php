<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Release extends Model
{

    protected $table   = 'releases';
    protected $guarded = ['id'];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }
}
