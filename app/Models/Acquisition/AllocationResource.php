<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class AllocationResource extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'allocation_resources';
}
