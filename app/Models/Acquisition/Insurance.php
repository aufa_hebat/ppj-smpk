<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $table = 'acquisition_insurances';

    protected $fillable = [
        'user_id',
        'sst_id',
        'hashslug',

        'public_insurance_name',
        'public_policy_no',
        'public_policy_amount',
        'public_received_date',
        'public_start_date',
        'public_expired_date',
        'public_proposed_date',
        'public_approval_proposed_date',
        'public_approval_received_date',

        'work_insurance_name',
        'work_policy_no',
        'work_policy_amount',
        'work_received_date',
        'work_start_date',
        'work_expired_date',
        'work_proposed_date',
        'work_approval_proposed_date',
        'work_approval_received_date',

        'compensation_insurance_name',
        'compensation_policy_no',
        'compensation_policy_amount',
        'compensation_received_date',
        'compensation_start_date',
        'compensation_expired_date',
        'compensation_proposed_date',
        'compensation_approval_proposed_date',
        'compensation_approval_received_date',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function compensation_insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'compensation_insurance_name');
    }

    public function public_insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'public_insurance_name');
    }

    public function work_insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'work_insurance_name');
    }

    public function documents_publicLiability()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'INSURANCE_PUBLIC');
    }

    public function documents_work()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'INSURANCE_WORK');
    }

    public function documents_compensation()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'INSURANCE_COMPENSATION');
    }

    public function getHasPublicLiabilityDocumentsAttribute()
    {
        return $this->documents_publicLiability->count() > 0;
    }

    public function getHasWorkDocumentsAttribute()
    {
        return $this->documents_work->count() > 0;
    }

    public function getHasCompensationDocumentsAttribute()
    {
        return $this->documents_compensation->count() > 0;
    }

    public function getHasDocumentsAttribute()
    {
        return $this->has_public_liability_documents || $this->has_work_documents || $this->has_compensation_documents;
    }
}
