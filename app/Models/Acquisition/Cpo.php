<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Cpo extends Model
{
    protected $fillable = [
        'sst_id',
        'user_id',
        'hashslug',

        'agreement_date',
        'partial_date',
        'assessment_date',
        'cpo_start_date',
        'cpo_end_date',

        'reference_no',
        'clause',
        'part_of_work_title',

        'estimation_amount',
        'cpo_amount',
        'lad_per_day',
    ];

    protected $dates = [
        'agreement_date',
        'partial_date',
        'assessment_date',
        'cpo_start_date',
        'cpo_end_date',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'CPO');
    }
}
