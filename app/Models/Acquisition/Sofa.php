<?php

namespace App\Models\Acquisition;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sofa extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'sofas';
    protected $guarded = ['id'];

    protected $dates = [
        'signature_date',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'SOFA');
    }

    public function getHasDocumentsAttribute()
    {
        return $this->documents->count() > 0;
    }
}
