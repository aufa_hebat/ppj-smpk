<?php

namespace App\Models\Acquisition;

use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Termination extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $table   = 'terminations';
    protected $guarded = ['id'];

    protected $dates = [
        'notice_at', 'termination_at',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'TERMINATE');
    }
}
