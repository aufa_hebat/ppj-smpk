<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Cmgd extends Model
{
    protected $fillable = [
        'sst_id',
        'user_id',
        'hashslug',

        'reference_no',
        'site_visit_date',
        'signature_date',
        'mgd_date',
        'clause',
    ];

    protected $dates = [
        'site_visit_date',
        'signature_date',
        'mgd_date',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'CMGD');
    }
}
