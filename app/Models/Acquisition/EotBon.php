<?php

namespace App\Models\Acquisition;

use App\Models\Bank;
use App\Models\StandardCode;
use Illuminate\Database\Eloquent\Model;

class EotBon extends Model
{
    protected $guarded = ['id'];
    protected $casts   = [
        'bank_received_date'               => 'datetime:Y-m-d',
        'bank_start_date'                  => 'datetime:Y-m-d',
        'bank_expired_date'                => 'datetime:Y-m-d',
        'bank_proposed_date'               => 'datetime:Y-m-d',
        'bank_approval_proposed_date'      => 'datetime:Y-m-d',
        'bank_approval_received_date'      => 'datetime:Y-m-d',
        'insurance_received_date'          => 'datetime:Y-m-d',
        'insurance_start_date'             => 'datetime:Y-m-d',
        'insurance_expired_date'           => 'datetime:Y-m-d',
        'insurance_proposed_date'          => 'datetime:Y-m-d',
        'insurance_approval_proposed_date' => 'datetime:Y-m-d',
        'insurance_approval_received_date' => 'datetime:Y-m-d',
        'cheque_submitted_date'            => 'datetime:Y-m-d',
        'cheque_received_date'             => 'datetime:Y-m-d',
    ];

    protected $dates = [
        'money_received_at',
    ];

    public function eot()
    {
        return $this->belongsTo(Eot::class, 'eot_id');
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'EOT_BON');
    }

    public function type()
    {
        return $this->belongsTo(StandardCode::class, 'bon_type', 'code_short')->where('code', 'BON_TYPE');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_name');
    }

    public function insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'insurance_name');
    }

    public function cheque_bank()
    {
        return $this->belongsTo(Bank::class, 'cheque_name');
    }

    public function getHasDocumentsAttribute()
    {
        return $this->documents->count() > 0;
    }
}
