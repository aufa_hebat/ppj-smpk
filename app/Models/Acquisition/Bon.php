<?php

namespace App\Models\Acquisition;

use App\Models\Bank;
use App\Models\StandardCode;
use Illuminate\Database\Eloquent\Model;

class Bon extends Model
{
    protected $table = 'acquisition_bons';

    protected $fillable = [
        'sst_id',
        'user_id',
        'hashslug',

        'bon_type',
        'bank_name',
        'bank_no',
        'bank_amount',
        'bank_received_date',
        'bank_start_date',
        'bank_expired_date',
        'bank_proposed_date',
        'bank_approval_proposed_date',
        'bank_approval_received_date',

        'insurance_name',
        'insurance_no',
        'insurance_amount',
        'insurance_received_date',
        'insurance_start_date',
        'insurance_expired_date',
        'insurance_proposed_date',
        'insurance_approval_proposed_date',
        'insurance_approval_received_date',

        'cheque_name',
        'cheque_no',
        'cheque_resit_no',
        'cheque_submitted_date',
        'cheque_received_date',

        'money_amount',
        'money_received_at',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function type()
    {
        return $this->belongsTo(StandardCode::class, 'bon_type', 'code_short')->where('code', 'BON_TYPE');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_name');
    }

    public function insurance()
    {
        return $this->belongsTo(\App\Models\Insurance::class, 'insurance_name');
    }

    public function cheque_bank()
    {
        return $this->belongsTo(Bank::class, 'cheque_name');
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'BON');
    }

    public function getHasDocumentsAttribute()
    {
        return $this->documents->count() > 0;
    }
}
