<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const QUOTATION_LABEL          = 'Sebut Harga';
    const QUOTATION_B_LABEL        = 'Sebut Harga B';
    const OPEN_TENDER_LABEL        = 'Tender Terbuka';
    const CLOSE_TENDER_LABEL       = 'Tender Terhad';
    const NUMBER_OF_QUOTATION_DAYS = 60;
    const NUMBER_OF_TENDER_DAYS    = 60;

    protected $guarded = ['id'];
    protected $table   = 'acquisition_categories';
}
