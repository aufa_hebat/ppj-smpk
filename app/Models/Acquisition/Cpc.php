<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Cpc extends Model
{
    protected $fillable = [
        'sst_id',
        'user_id',
        'hashslug',

        'dlp_start_date',
        'dlp_expiry_date',
        'signature_date',
        'test_award_date',
        'clause_1',
        'clause_2',
    ];

    protected $dates = [
        'dlp_start_date',
        'dlp_expiry_date',
        'signature_date',
        'test_award_date',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }

    public function documents()
    {
        return $this->hasMany(\App\Models\Document::class, 'document_id')->where('document_types', 'CPC');
    }
}
