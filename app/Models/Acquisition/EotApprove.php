<?php

namespace App\Models\Acquisition;

use App\Models\Document;
use App\Models\PeriodType;
use Illuminate\Database\Eloquent\Model;

class EotApprove extends Model
{
    const APPROVED_LABEL = 'Lulus';
    const FAILED_LABEL   = 'Gagal';

    protected $guarded = ['id'];
    protected $casts   = [
        'meeting_date'      => 'datetime:Y-m-d',
        'approved_end_date' => 'datetime:Y-m-d',
    ];

    public function scopeWithDetails($query)
    {
        return $query->with('eot', 'period_type', 'documents');
    }

    public function getPeriodLabelAttribute()
    {
        return $this->period . ' ' . $this->period_type->name;
    }

    public function getApprovalLabelAttribute()
    {
        return (1 == $this->approval) ? self::APPROVED_LABEL : self::FAILED_LABEL;
    }

    public function eot()
    {
        return $this->belongsTo(Eot::class, 'eot_id');
    }

    public function period_type()
    {
        return $this->belongsTo(PeriodType::class, 'period_type_id');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'document_id')->where('document_types', 'EOT');
    }
}
