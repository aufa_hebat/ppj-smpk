<?php

namespace App\Models\Acquisition;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'acquisition_documents';

    protected $fillable = [
        'sst_id',
        'user_id',
        'hashslug',

        'revenue_stamp_date',
        'preparation_period',
        'proposed_date',
        'submitted_date',
        'base_lending_rate',
        'LAD_rate',
        'late_days',
    ];

    public function sst()
    {
        return $this->belongsTo(Sst::class);
    }
}
