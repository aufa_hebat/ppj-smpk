<?php

namespace App\Models\Acquisition;

use App\Models\Company\Company;
use App\Models\Acquisition\Review;
use App\Traits\DatatableTrait;
use App\Traits\ReferenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use DatatableTrait, ReferenceTrait, SoftDeletes;

    protected $dates = [
        'sales_date',
    ];

    public function scopeWithDetails($query)
    {
        return $query;
    }

    public function acquisition()
    {
        return $this->belongsTo(Acquisition::class);
    }

    public function briefing()
    {
        return $this->belongsTo(Briefing::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function box()
    {
        return $this->hasOne(Box::class);
    }
    
    public function reviews()
    {
        return $this->hasMany(Review::class, 'acquisition_id')->where('type','Acquisitions')->where('status','Selesai')->where('deleted_at',null);
    }

    public function scopeTotalSales($query, $year = null, $month = null)
    {
        if (! is_null($year)) {
            $query = $query->whereYear('created_at', $year);
        }

        if (! is_null($month)) {
            $query = $query->whereMonth('created_at', $month);
        }

        $total = 0;

        $query
            ->with('acquisition')
            ->get()
            ->each(function ($sale) use (&$total) {
                $total += (! is_null($sale->acquisition) ? $sale->acquisition->total_document_price : 0);
            });

        return money()->toHuman($total);
    }
}
