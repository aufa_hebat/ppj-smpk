<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MOFCode extends Model
{
    protected $guarded = ['id'];
    protected $table   = 'mof_codes';

    public function getNameAttribute()
    {
        return $this->attributes['code'] . ' - ' . $this->attributes['name'];
    }
}
