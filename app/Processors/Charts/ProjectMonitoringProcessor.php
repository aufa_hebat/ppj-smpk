<?php

namespace App\Processors\Charts;

class ProjectMonitoringProcessor extends BaseChartProcessor
{
    public static function make()
    {
        return new self();
    }

    // No of Contracts WHERE PAYMENT PROGRESS EXCEEDS PHYSICAL PROGRESS. group by Jabatan, upon selecting a Jabatan will display list by Bahagian.
    public function contractPaymentProgressExceedPhysicalProgressGroupByDepartment()
    {
        return \App\Models\Report\Chart::getChartData('project-payment-progress-exceed-physical-progress-group-by-department');
    }

    public function contractCreatedByDepartment()
    {
        return \App\Models\Report\Chart::getChartData('project-contract-six-month-to-end');
    }

    public function awardedCompaniesByDepartment()
    {
        return \App\Models\Report\Chart::getChartData('project-awarded-company-by-department');
    }
}
