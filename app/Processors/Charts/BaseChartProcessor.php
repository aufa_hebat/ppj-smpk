<?php

namespace App\Processors\Charts;

class BaseChartProcessor
{
    public function setup($type, $title, $labels, $datasets, $legend_position = 'bottom')
    {
        return [
            'type' => $type,
            'data' => [
                'datasets' => $datasets,
                'labels'   => $labels,
            ],
            'options' => [
                'plugins' => [
                    'datalabels' => [
                        'backgroundColor' => 'function(context) {return context.dataset.backgroundColor;}',
                        'borderRadius'    => 4,
                        'color'           => 'white',
                        'font'            => [
                           'weight' => 'bold',
                        ],
                        'formatter' => 'Math.round',
                    ],
                ],
                'responsive' => true,
                'legend'     => [
                    'position' => $legend_position,
                ],
                'title' => [
                    'display' => true,
                    'text'    => title_case($title),
                ],
                'animation' => [
                    'animateScale'  => true,
                    'animateRotate' => true,
                ],
            ],
        ];
    }

    public static function make()
    {
        return new self();
    }
}
