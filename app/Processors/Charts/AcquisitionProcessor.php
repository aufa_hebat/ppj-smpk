<?php

namespace App\Processors\Charts;

class AcquisitionProcessor extends BaseChartProcessor
{
    public static function make()
    {
        return new self();
    }

    public function acquisitionByDepartment()
    {
        return \App\Models\Report\Chart::getChartData('acquisition-by-department');
    }
}
