<?php

namespace App\Processors\Charts;

class ProjectProgressProcessor extends BaseChartProcessor
{
    //DIN : TODO - Query By Role , Ketua Jabatan, Dan so on
    // TODO - Please not include contract was batal and tammat
    
    public static function make()
    {
        return new self();
    }

    // Pie chart. No of Contracts WHERE PHYSICAL PROGRESS EXCEEDS PAYMENT PROGRESS BY 50%. group by Jabatan, upon selecting a Jabatan will display list by Bahagian.
    public function getPaymentProgressMoreThanFiftyThanPhysicalProgressPercent()
    {
//        $ssts = \App\Models\Acquisition\Sst::all()->reject(function ($sst) {
//            return $sst->is_more_than_fifty_percent_paid;
//        });
//
//        $exceeds = \App\Models\Acquisition\Activity::with('sst', 'sst.acquisition')->whereIn('sst_id', $ssts->pluck('id'))
//            ->latest()
//            ->where('actual_completion', '>', '50')
//            ->groupBy('sst_id')
//            ->get();
//
//        if (! $exceeds) {
//            return collect();
//        }
//
//        return $exceeds->mapWithKeys(function ($activity) {
//            return [
//                $activity->sst->acquisition->reference => route('acquisition.monitoring.show', $activity->sst->hashslug),
//            ];
//        });
        
        //find SST , Lawatan tapak latest , then get all invoice bahagi , if 50% set into list
        //TIPU
        return \App\Models\Acquisition\Acquisition::doesnthave('sst')->get();   
        
        
    }

    
    //ALL BACK TO BASIC FOR MAIN PAGE
    public function contractsSixMonthToEnd()
    {
        return \App\Models\Acquisition\Sst::contractsSixMonthToEnd()->get();
                /*
                ->mapWithKeys(function ($sst) {
            return [
                $sst->acquisition->reference => route('acquisition.monitoring.show', $sst->hashslug),
            ];
        }); */
        
        //DIN : TODO NEW QUERY
        /* if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                //$query = Approval::where('deleted_at',null)->latest()->get();
                 return \App\Models\Acquisition\Sst::
                        where('end_working_date', '>=', now())
                        ->where('end_working_date', '<=', now()->addMonths(6));
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                          where('department_id', user()->department_id)
                       // -> whereHas('document2',function($q) {
                             // $q->where('revenue_stamp_date', null); 
                           //})
                        ->where('end_working_date', '>=', now())
                        ->where('end_working_date', '<=', now()->addMonths(6));
               
                
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                       // ->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id); 
                           })
                        ->where('end_working_date', '>=', now())
                        ->where('end_working_date', '<=', now()->addMonths(6));
            }
            else {   
                //user biasa , section
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                        //->where('section_id','=',user()->section_id)
                        //->where('deleted_at',null)->latest()->get();
                
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id)
                                ->where('section_id', user()->section_id); 
                           })
                        ->where('end_working_date', '>=', now())
                        ->where('end_working_date', '<=', now()->addMonths(6));
            }
        }else{
            return null;
        }
        */
        
        
        
        
        //real query : return \App\Models\Acquisition\Sst::where('end_working_date', '>=', now())->where('end_working_date', '<=', now()->addMonths(6));
        
        
    }
    
    //DONE Senarai project On-going ,test tp pening
    public function contractsOnGoing()
    {
        //DONE query contract sedang berjalan , tarikh mula kerja in sst, tiada penamatan dan tiada CPC
        return \App\Models\Acquisition\Sst::whereDate('start_working_date','<=',now())->where('sofa_signature_date',null);
        
        
         if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                //$query = Approval::where('deleted_at',null)->latest()->get();
                 return \App\Models\Acquisition\Sst::
                        whereDate('start_working_date','<=',now())->where('sofa_signature_date',null);
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                          where('department_id', user()->department_id)
                       // -> whereHas('document2',function($q) {
                             // $q->where('revenue_stamp_date', null); 
                           //})
                        ->whereDate('start_working_date','<=',now())->where('sofa_signature_date',null);
               
                
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                       // ->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id); 
                           })
                        ->whereDate('start_working_date','<=',now())->where('sofa_signature_date',null);
            }
            else {   
                //user biasa , section
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                        //->where('section_id','=',user()->section_id)
                        //->where('deleted_at',null)->latest()->get();
                
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id)
                                ->where('section_id', user()->section_id); 
                           })
                       ->whereDate('start_working_date','<=',now())->where('sofa_signature_date',null);
            }
        }else{
            return null;
        }
          
    }
    
    //DONE Senarai Kontrak blm tandatangan
    public function contractsNotSignedYet()
    {
        //DONE Ada SST , tiada tarikh Dokumen Kontrak acquisition_documents -> revenue_stamp_date tarik mathi not null
         return \App\Models\Acquisition\Sst::doesnthave('document2')->orWhereHas('document2',function($q) {
            $q->where('revenue_stamp_date', null); 
        })->get();    
       
       
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                //$query = Approval::where('deleted_at',null)->latest()->get();
                 return \App\Models\Acquisition\Sst::doesnthave('document2')->orWhereHas('document2',function($q) {
                      $q->where('revenue_stamp_date', null); 
                    })->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                          where('department_id', user()->department_id)
                       // -> whereHas('document2',function($q) {
                             // $q->where('revenue_stamp_date', null); 
                           //})
                        ->doesnthave('document2')->orWhereHas('document2',function($q) {
                                     $q->where('revenue_stamp_date', null); 
                          })->get();
               
                
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                       // ->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id); 
                           })
                        ->doesnthave('document2')->orWhereHas('document2',function($q) {
                                     $q->where('revenue_stamp_date', null); 
                          })->get();
            }
            else {   
                //user biasa , section
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                        //->where('section_id','=',user()->section_id)
                        //->where('deleted_at',null)->latest()->get();
                
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id)
                                ->where('section_id', user()->section_id); 
                           })
                       ->doesnthave('document2')->orWhereHas('document2',function($q) {
                                     $q->where('revenue_stamp_date', null); 
                          })->get();
            }
        }else{
            return null;
        }
       
       
    }
    
    //TODO 
    public function contractsDoneButNotSofaYet()
    { 
        //TODO - Tarikh CPC signiture date dah ada, tp Sofa date belum create , Ada table Sofa
         return \App\Models\Acquisition\Sst::whereHas('cpc',function($q) {
            $q->whereNotNull('signature_date'); 
       })->get();    
        
        
        
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                //$query = Approval::where('deleted_at',null)->latest()->get();
                 return \App\Models\Acquisition\Sst::whereHas('cpc',function($q) {
                            $q->whereNotNull('signature_date'); 
                     })->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                          where('department_id', user()->department_id)
                       // -> whereHas('document2',function($q) {
                             // $q->where('revenue_stamp_date', null); 
                           //})
                        ->whereHas('cpc',function($q) {
                           $q->whereNotNull('signature_date'); 
                         })->get();
               
                
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                       // ->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id); 
                           })
                        ->whereHas('cpc',function($q) {
                           $q->whereNotNull('signature_date'); 
                         })->get();
            }
            else {   
                //user biasa , section
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                        //->where('section_id','=',user()->section_id)
                        //->where('deleted_at',null)->latest()->get();
                
                return \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id)
                                ->where('section_id', user()->section_id); 
                           })
                       ->whereHas('cpc',function($q) {
                           $q->whereNotNull('signature_date'); 
                         })->get();
            }
        }else{
            return null;
        }
        
        
    }
    
    //Senarai projek blm award
     public function contractsNotAwardYet()
    {    
        return \App\Models\Acquisition\Acquisition::doesnthave('sst')->get();
         
         
           if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                //$query = Approval::where('deleted_at',null)->latest()->get();
                 return \App\Models\Acquisition\Acquisition::doesnthave('sst')->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('deleted_at',null)->latest()->get();
                 return \App\Models\Acquisition\Acquisition::
                          where('department_id', user()->department_id)
                       // -> whereHas('document2',function($q) {
                             // $q->where('revenue_stamp_date', null); 
                           //})
                        ->doesnthave('sst')->get();
               
                
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                       // ->where('deleted_at',null)->latest()->get();
                return \App\Models\Acquisition\Acquisition::
                    where('department_id', user()->department_id)
                        ->where('executor_department_id', user()->executor_department_id)
                       ->doesnthave('sst')->get();
            }
            else {   
                //user biasa , section
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                        //->where('section_id','=',user()->section_id)
                        //->where('deleted_at',null)->latest()->get();
                
                return \App\Models\Acquisition\Acquisition::
                    where('department_id', user()->department_id)
                        ->where('executor_department_id', user()->executor_department_id)
                        ->where('section_id',user()->section_id)
                       ->doesnthave('sst')
                        ->get();
            }
        }else{
            return null;
        }
         
    }
        
    
}
