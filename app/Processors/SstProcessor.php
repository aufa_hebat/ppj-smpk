<?php

namespace App\Processors;

use App\Models\Acquisition\Category;
use App\Models\Acquisition\Sale;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Type;
use App\Models\DefectLiabilityPeriod as LiabilityPeriod;
use App\Models\PeriodType;
use Carbon\Carbon;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Review;

class SstProcessor
{
    public $sst;

    public function __construct(Sst $sst)
    {
        $this->sst = $sst;
    }

    public static function make(Sst $sst)
    {
        return new self($sst);
    }

    /**
     * Tempoh Kerja + (Tempoh Liabiliti Kecacatan,DLP) + 3 bulan + 14 hari.
     *
     * @return \Carbon\Carbon
     */
    public function getExpiredDate()
    {
        $sst = $this->sst;

        $expiry_date = $sst->start_working_date;

        $sale = Sale::query()
            ->where('acquisition_id', $sst->acquisition_id)
            ->where('company_id', $sst->company_id)
            ->with('box')
            ->first();

        // tempoh kerja
        if (PeriodType::DAY == $sale->box->period_type_id) {
            $expiry_date->addDays($sale->box->period);
        } elseif (PeriodType::WEEK == $sale->box->period_type_id) {
            $expiry_date->addWeeks($sale->box->period);
        } elseif (PeriodType::MONTH == $sale->box->period_type_id) {
            $expiry_date->addMonths($sale->box->period);
        } elseif (PeriodType::YEAR == $sale->box->period_type_id) {
            $expiry_date->addYears($sale->box->period);
        }

        // Tempoh Liabiliti Kecacatan, DLP
        if (LiabilityPeriod::SIX_MONTH == $sst->defects_liability_period) {
            $expiry_date->addMonths(LiabilityPeriod::SIX_MONTH_VALUE);
        } elseif (LiabilityPeriod::TWELVE_MONTH == $sst->defects_liability_period) {
            $expiry_date->addMonths(LiabilityPeriod::TWELVE_MONTH_VALUE);
        }
        
        return $expiry_date;
    }

    /**
     * Tarikh SST Dikeluarkan + 14 hari.
     *
     * @return \Carbon\Carbon
     */
    public function getProposedDate()
    {
        return $this->sst->letter_date->addDays(Sst::NUMBER_OF_PROPOSED_DAYS);
    }

    /**
     * OLD - Tarikh Terima SST dari Kontraktor + 90 Hari (Sebut Harga Kerja), 120 hari (Tender).
     * Tarikh Terima SST dari Kontraktor + 120 Hari.
     *
     * @return \Carbon\Carbon;
     */
    public function getDocProposedDate()
    {
        $sst = $this->sst;

        //$proposed_date = $sst->letter_date;
        //din change to al_contractor_date
        $proposed_date = $sst->al_contractor_date;

        /*if (Category::QUOTATION_LABEL == $sst->acquisition->category->name || Category::QUOTATION_B_LABEL == $sst->acquisition->category->name) {
            $proposed_date->addDays(Category::NUMBER_OF_QUOTATION_DAYS);
        } elseif (Category::OPEN_TENDER_LABEL == $sst->acquisition->category->name || Category::CLOSE_TENDER_LABEL == $sst->acquisition->category->name) {
            $proposed_date->addDays(Category::NUMBER_OF_TENDER_DAYS);
        }*/

        if($proposed_date != null && $proposed_date != ''){
            $proposed_date->addDays(120);
        }        

        return $proposed_date;
    }

    /**
     * Get Total WJP.
     *
     * @param int $deposit_amount
     *
     * @return array
     */
    public function getTotalWjp(int $deposit_amount)
    {
        $sst = $this->sst;

        $jumlah    = 0;
        $qualified = 0;
        $percent   = 0;
        $qualified = 0;
        $limit     = 0;

        if (Type::WORK == $sst->acquisition->approval->type->name) {
            if (Category::QUOTATION_LABEL == $sst->acquisition->category->name || Category::QUOTATION_B_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.25;
                $limit     = 10000000;
                $qualified = 10000000;
            }

            if (Category::OPEN_TENDER_LABEL == $sst->acquisition->category->name || Category::CLOSE_TENDER_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.25;
                $limit     = 1000000000;
                $qualified = 1000000000;
            }
        } elseif (Type::PROVIDER == $sst->acquisition->approval->type->name) {
            if (Category::QUOTATION_LABEL == $sst->acquisition->category->name || Category::QUOTATION_B_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.25;
                $limit     = 10000000;
                $qualified = 10000000;
            }

            if (Category::OPEN_TENDER_LABEL == $sst->acquisition->category->name || Category::CLOSE_TENDER_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.25;
                $limit     = 1000000000;
                $qualified = 1000000000;
            }
        } elseif (Type::SERVICE == $sst->acquisition->approval->type->name) {
            if (Category::QUOTATION_LABEL == $sst->acquisition->category->name || Category::QUOTATION_B_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.15;
                $limit     = 5000000;
                $qualified = 5000000;
            }

            if (Category::OPEN_TENDER_LABEL == $sst->acquisition->category->name || Category::CLOSE_TENDER_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.15;
                $limit     = 500000000;
                $qualified = 500000000;
            }
        } elseif (Type::MAINTENANCE == $sst->acquisition->approval->type->name) {
            if (Category::QUOTATION_LABEL == $sst->acquisition->category->name || Category::QUOTATION_B_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.25;
                $limit     = 10000000;
                $qualified = 10000000;
            }

            if (Category::OPEN_TENDER_LABEL == $sst->acquisition->category->name || Category::CLOSE_TENDER_LABEL == $sst->acquisition->category->name) {
                $percent   = 0.25;
                $limit     = 1000000000;
                $qualified = 1000000000;
            }
        }

        return $this->wjpCalculator($deposit_amount, $percent, $limit, $qualified);
    }

    private function wjpCalculator($deposit_amount, $percent, $limit, $qualified)
    {
        $jumlah    = $deposit_amount * $percent;
        //$qualified = $jumlah;

        if ($jumlah > $limit) {
            //$qualified = $qualified;
            $qualified = $limit;
            $jumlah = $limit;
        }else{
            $qualified = $jumlah;
        }

        return [
            'deposit_total_amount'     => $jumlah,
            'deposit_qualified_amount' => $qualified,
        ];
    }

    /**
     * Tarikh Luput Jaminan Bank 
            < Formula Tender Kerja = Tempoh Kerja + Tempoh DLP + 12 bulan> , 
            < Formula Sebutharga Kerja =Tempoh Kerja + DLP + 12 bulan>, 
            < Formula Sebutharga Penyelenggaraan=Tempoh Kerja + 12 bulan>
     *
     * @return \Carbon\Carbon
     */
    public function getExpiredDateBon($expiry_date)
    {
        $sst = $this->sst;

        //$expiry_date = $this->getExpiredDate();


        // Tender & Sebutharga: Kerja
        if($sst->acquisition->approval->acquisition_type_id == 'Kerja'){
            if($sst->acquisition->acquisition_category_id == config('enums.Tender Terbuka') || $sst->acquisition->acquisition_category_id == config('enums.Tender Terhad') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga')){
                $expiry_date->addMonths(12);
            }
        }
        else if ($sst->acquisition->approval->acquisition_type_id == config('enums.Penyelenggaraan')) {
            if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga')){
                $expiry_date->addMonths(12);
            }
        }

        return $expiry_date;
    }

    /**
     * Tarikh Luput Jaminan Insuran
        < Formula Tender Kerja = Tarikh Bermula Jaminan Insuran+  Tempoh Kerja + (Tempoh Liabiliti Kecacatan,DLP) + 3 bulan + 14 hari> 
        < Formula Sebutharga Kerja = Tarikh Bermula Jaminan Insuran+  1 tahun>
     *
     * @return \Carbon\Carbon
     */
    public function getExpiredDateInsuran($expiry_date)
    {
        $sst = $this->sst;
        
        //$expiry_date = $this->getExpiredDate();


        // Tender & Sebutharga: Kerja
        if($sst->acquisition->approval->acquisition_type_id == config('enums.Kerja')){
            if($sst->acquisition->acquisition_category_id == config('enums.Tender Terbuka') || $sst->acquisition->acquisition_category_id == config('enums.Tender Terhad') ){
                $expiry_date->addMonths(3);
                $expiry_date->addDays(14);
            }

            if( $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga')){
                $expiry_date->addMonths(12);
            }
        }
        
        return $expiry_date;
    }

    /**
     * Deposit Rules
        1. Tempoh kerja lebih 3 Bulan Sahaja
        2. Sebut Harga RM200k keatas
        3. Polisi dan Bon dikemukakan
        4. Di pohon tidak lebih 1 BULAN dari LOA dikeluarkan
        5. LOA ditandatangani
     *
     * @return \Carbon\Carbon
     */
    public function getDepositRules()
    {
        $sst = $this->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $bonApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Bon Pelaksanaan')
                                        ->where('type','Dokumen Kontrak')->first();
        $insuranceApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Insurans')
                                        ->where('type','Dokumen Kontrak')->first();

        $sstApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('type','SST')->first();
        
        $isAllow = true;
        $reason  = null;

        // 1. Tempoh kerja lebih 3 Bulan Sahaja
        if(!($sst->start_working_date->diffInMonths($sst->end_working_date) > 3)){
            $isAllow = false;
            $reason  = "Tempoh kerja lebih 3 Bulan";
        }
        // 2. Sebut Harga RM200k keatas
        else if(!($appointed->offered_price > 200000)){
            $isAllow = false;
            $reason  = "Sebut Harga kurang dari RM200k";
        }
        // 3. Polisi dan Bon dikemukakan
        //else if(empty($bonApproved) || empty($insuranceApproved)){
        else if(empty($bonApproved)){
            $isAllow = false;
            //$reason  = "Polisi dan Bon tidak selesai";
            $reason  = "Bon tidak selesai";
        }
        // 4. Di pohon tidak lebih 1 BULAN dari LOA dikeluarkan
        // else if(!(Carbon::now()->diffInMonths($sst->letter_date) < 1)){
        else if((!empty($sst->vp_sign_date))&& !($sst->vp_sign_date->diffInMonths($sst->letter_date) < 1)){
            $isAllow = false;
            $reason  = "Tarikh surat di sign vp must be within 1 month from letter date";
        }
        // 5. LOA ditandatangani
        else if(empty($sstApproved)){
            $isAllow = false;
            $reason  = "LOA tidak selesai";
        }

        return ['isAllow' => $isAllow, 'reason' => $reason];
    }

    public function getDocumentContractRules()
    {
        $sst = $this->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $bonApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Bon Pelaksanaan')
                                        ->where('type','Dokumen Kontrak')->first();
        $insuranceApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Insurans')
                                        ->where('type','Dokumen Kontrak')->first();
        $depositApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Wang Pendahuluan')
                                        ->where('type','Dokumen Kontrak')->first();

        $sstApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('type','SST')->first();
        
        $isAllow = true;

        // Samada perolehan mempunyai wang pendahuluan atau tidak
        if(!empty($sst->deposit)){
            // Polisi, Wang Pendahuluan dan Bon dikemukakan
            // if(empty($bonApproved) || empty($insuranceApproved) || empty($depositApproved)){
            if(empty($bonApproved) || empty($depositApproved)){
                $isAllow = false;
            }
        } else {
            // Polisi dan Bon dikemukakan
            // if(empty($bonApproved) || empty($insuranceApproved)){
            if(empty($bonApproved)){
                $isAllow = false;
            }
        }

        return ['isAllow' => $isAllow];
    }

    /**
     * Kiraan Insuran Tanggungan Awam
       1. Jika Jenis Sebut Harga dan Harga Kontrak < 50k = 10k
       2. Jika Jenis Sebut Harga dan Harga Kontrak di antara 50k - 100k = 25k
       3. Jika Jenis Sebut Harga dan Harga Kontrak di antara 100k - 200k = 50k
       4. Jika Jenis Sebut Harga dan Harga Kontrak di antara 200k - 500k = 100k
       5. Jika Jenis Tender dan Harga Kontrak di antara 500k - 5j = 200k
       6. Jika Jenis Tender dan Harga Kontrak di antara 5j - 20j = 500k
       7. Jika Jenis Tender dan Harga Kontrak di antara 20j - 50j = 1j
       8. Jika Jenis Tender dan Harga Kontrak > 50j = 20j
     */
     public function getInsuranTanggunganAwam($acqCat, $offeredPrice){
         $insTA = 0;
         if($acqCat == 'Sebut Harga' || $acqCat == 'Sebut Harga B'){
            if($offeredPrice <= 5000000){
                $insTA = 1000000;
            }else if($offeredPrice > 5000000 && $offeredPrice <= 10000000){
                $insTA = 2500000;
            }else if($offeredPrice > 10000000 && $offeredPrice <= 20000000){
                $insTA = 5000000;
            }else if($offeredPrice > 20000000 && $offeredPrice <= 50000000){
                $insTA = 10000000;
            }
         }else if($acqCat == 'Tender Terhad' || $acqCat == 'Tender Terbuka'){
            if($offeredPrice > 50000000 && $offeredPrice <= 500000000){
                $insTA = 20000000;
            }else if($offeredPrice > 500000000 && $offeredPrice <= 2000000000){                
                $insTA = 50000000;
            }else if($offeredPrice > 2000000000 && $offeredPrice <= 5000000000){
                $insTA = 100000000;
            }else if($offeredPrice > 5000000000){
                $insTA = 2000000000;
            }
         }
         return $insTA;
     }

    public function getInsuranPampasanPekerja($appointed){

        return getPriceWithoutWps($appointed) * 0.1;
    }

    public function getPriceWithoutWps($appointed){

        if($appointed->acquisition->bqElements->count() > 0){
            foreach($appointed->acquisition->bqElements as $element){
                $isProSumFound = common()->checkProSum($element->element);
                if($isProSumFound){
                    $amountWithoutWPS = $appointed->offered_price - $element->amount;                    
                }
            }
            if($amountWithoutWPS == 0){
                $amountWithoutWPS = $appointed->offered_price;
            }
        }else{
            $amountWithoutWPS = $appointed->offered_price;
        }
        return $amountWithoutWPS;
    }
}
