<?php


/*
 * Get Company Owners
 * company_owners()
 */
if (! function_exists('company_owners')) {
    function company_owners()
    {
        return \App\Models\Company\Owner::orderBy('name')->get();
    }
}

/*
 * Get Companies
 * companies()
 */
if (! function_exists('companies')) {
    function companies()
    {
        return \App\Models\Company\Company::where('company_name','!=',null)->orderBy('company_name')->get();
    }
}

/*
 * Get Company Owners Exist in A Company
 * company_owners_in_a_company()
 */
if (! function_exists('company_owners_in_a_company')) {
    function company_owners_in_a_company($company_id)
    {
        return \App\Models\Company\Owner::where('company_id', $company_id)->orderBy('name')->get();
    }
}

/*
 * Get Company Owners Exist in A Company
 * company_owners_in_a_company()
 */
if (! function_exists('company_owners_in_a_company_with_mof')) {
    function company_owners_in_a_company_with_mof($company_id)
    {
        return \App\Models\Company\Owner::where('company_id', $company_id)->where('mof_cert', true)->orderBy('name')->get();
    }
}

/*
 * Get Company Owners Exist in A Company
 * company_owners_in_a_company()
 */
if (! function_exists('company_owners_in_a_company_with_cidb')) {
    function company_owners_in_a_company_with_cidb($company_id)
    {
        return \App\Models\Company\Owner::where('company_id', $company_id)->where('cidb_cert', true)->orderBy('name')->get();
    }
}
