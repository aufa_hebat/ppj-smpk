<?php

use Illuminate\Support\Facades\Cache;

/*
 * generate sequence
 * @return sequence based on length supplied
 */
if (! function_exists('generate_sequence')) {
    function generate_sequence($input = 1)
    {
        return str_pad($input, config('document.sequence_length'), '0', STR_PAD_LEFT);
    }
}

/*
 * Get Abbreviation fo the given text
 */
if (! function_exists('abbrv')) {
    function abbrv($value, $unique_characters = true)
    {
        $removeNonAlphanumeric = preg_replace('/[^A-Za-z0-9 ]/', '', $value);
        $removeVowels          = str_replace(
            ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', ' '],
            '',
            $removeNonAlphanumeric);

        $uppercase = strtoupper($removeVowels);

        if ($unique_characters) {
            $split             = str_split($uppercase);
            $unique_characters = [];
            foreach ($split as $character) {
                if (! in_array($character, $unique_characters)) {
                    $unique_characters[] = $character;
                }
            }

            return implode('', $unique_characters);
        }

        return $uppercase;
    }
}

/*
 * Get Reference
 *
 * @todo Use Cache to Speed up the Query
 */
if (! function_exists('generate_reference')) {
    function generate_reference($prefix = 'DOCUMENT REFERENCE', $count = false)
    {
        $reference_number[] = $prefix;
        $reference_number[] = $count + 1;
        $reference_number[] = \Carbon\Carbon::now()->format('Y');

        return implode('/', $reference_number);
    }
}

/*
 * Get Media Collection Name
 */
if (! function_exists('media_collection_name')) {
    function media_collection_name($key, $identifier)
    {
        return (config('media-collections.' . $key) ?? 'default') . '-' . $identifier;
    }
}

/*
 * Hashids Helper
 */
if (! function_exists('hashids')) {
    function hashids($salt = null, $length = null, $alphabet = null)
    {
        $salt     = is_null($salt) ? config('hashids.salt') : config('hashids.salt') . $salt;
        $length   = is_null($length) ? config('hashids.length') : $length;
        $alphabet = is_null($alphabet) ? config('hashids.alphabet') : $alphabet;
        $salt     = \Illuminate\Support\Facades\Hash::make($salt);

        return \App\Services\Hashids::make($salt, $length, $alphabet);
    }
}

/*
 * Get Fully Qualified Class Name (FQCN)
 */
if (! function_exists('fqcn')) {
    function fqcn($object)
    {
        return get_class($object);
    }
}

/*
 * Get Slug Name for Fully Qualified Class Name (FQCN)
 */
if (! function_exists('str_slug_fqcn')) {
    function str_slug_fqcn($object)
    {
        return strtolower(str_replace('\\', '-', fqcn($object)));
    }
}

/*
 * Audit Trail
 */
if (! function_exists('audit')) {
    function audit($model, $message, $causedBy = null)
    {
        if (is_null($causedBy)) {
            activity()
                ->performedOn($model)
                ->causedBy(user())
                ->log($message);
        } else {
            activity()
                ->performedOn($model)
                ->causedBy($causedBy)
                ->log($message);
        }
    }
}

/*
 * user() helper
 */
if (! function_exists('user')) {
    function user()
    {
        foreach (config('auth.guards') as $key => $value) {
            if (Auth::guard($key)->check()) {
                return Auth::guard($key)->user();
            }
        }

        return null;
    }
}

/*
 * roles() helper
 */
if (! function_exists('roles')) {
    function roles($guard = 'web')
    {
        return Cache::remember('roles.' . $guard, 10, function () use ($guard) {
            return config('permission.models.role')::with('permissions')->where('guard_name', $guard)->get();
        });
    }
}

/*
 * permissions() helper
 */
if (! function_exists('permissions')) {
    function permissions($guard)
    {
        return Cache::remember('permissions.' . $guard, 10, function () use ($guard) {
            return config('permission.models.permission')::with('roles')->where('guard_name', $guard)->get();
        });
    }
}

/*
 * Minify given HTML Content
 */
if (! function_exists('minify')) {
    function minify($value)
    {
        $replace = [
            '/<!--[^\[](.*?)[^\]]-->/s' => '',
            "/<\?php/"                  => '<?php ',
            "/\n([\S])/"                => '$1',
            "/\r/"                      => '',
            "/\n/"                      => '',
            "/\t/"                      => '',
            '/ +/'                      => ' ',
        ];

        if (false !== strpos($value, '<pre>')) {
            $replace = [
                '/<!--[^\[](.*?)[^\]]-->/s' => '',
                "/<\?php/"                  => '<?php ',
                "/\r/"                      => '',
                "/>\n</"                    => '><',
                "/>\s+\n</"                 => '><',
                "/>\n\s+</"                 => '><',
            ];
        }

        return preg_replace(array_keys($replace), array_values($replace), $value);
    }
}

/*
 * Get Available Locales
 */
if (! function_exists('locales')) {
    function locales()
    {
        return collect(explode(',', config('app.locales')))->reject(function ($locale) {
            return ! file_exists(resource_path('lang/' . $locale));
        });
    }
}

/*
 * MoneyPHP Helper
 */
if (! function_exists('money')) {
    function money()
    {
        return \App\Utilities\Money::make();
    }
}

/*
 * Get Finance Users - From department Kewangan + Grade 41 and above
 */
if (! function_exists('finance_users')) {
    function finance_users($grade = 41)
    {
        return \App\Models\User::whereHas('department', function ($query) {
            $query->finance();
        })->whereHas('grade', function ($query) use ($grade) {
            $query->where('name', '>=', $grade);
        })->get();
    }
}

/*
 * Dashboard Service Helper
 */
if (! function_exists('dashboard')) {
    function dashboard()
    {
        return \App\Services\DashboardService::make();
    }
}

/*
 * Chart Service Helper
 */
if (! function_exists('chart')) {
    function chart()
    {
        return \App\Services\ChartService::make();
    }
}

/*
 * Rate Service Helper
 */
if (! function_exists('rate')) {
    function rate($category, $type, $amount, $periodType, $period)
    {
        return \App\Services\RateService::make()->$category($type, $amount, $periodType, $period);
    }
}

/*
 * Logo URL Generator Helper
 */
if (! function_exists('logo')) {
    function logo($print = false)
    {
        return ($print) ? public_path('images/logo.png') : asset('images/logo.png');
    }
}

/*
 * Document Upload Helper
 */
if (! function_exists('upload_document')) {
    function common()
    {
        return \App\Utilities\Common::make();
    }
}

/*
 * Determine Label Class Helper
 */
if (! function_exists('label_class')) {
    function label_class($first, $second)
    {
        return $first < $second ? 'danger' : 'success';
    }
}

/*
 * Get Random Color
 */
if (! function_exists('random_color')) {
    function random_color()
    {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }
}

/*
 * Get Random Colors
 */
if (! function_exists('random_colors')) {
    function random_colors($number_of_colors)
    {
        $colors = [];
        for ($i = 0; $i < $number_of_colors; ++$i) {
            $colors[] = random_color();
        }

        return $colors;
    }
}

/*
 * Get Role by Name
 */
if (! function_exists('role')) {
    function role($name)
    {
        return Cache::remember('role.' . $name, 10, function () use ($name) {
            return config('permission.models.role')::with('permissions')->where('name', $name)->first();
        });
    }
}

/*
 * Get Permission by Name
 */
if (! function_exists('permission')) {
    function permission($name)
    {
        return Cache::remember('permission.' . $name, 10, function () use ($name) {
            return config('permission.models.permission')::with('roles')->where('name', $name)->first();
        });
    }
}

/*
 * parse date use locale
 */
if (! function_exists('locale_date')) {
    function locale_date($datetime, $formatter = IntlDateFormatter::NONE)
    {
        return \Illuminate\Support\Carbon::intlFormat($datetime, $formatter);
    }
}

collect(glob(__DIR__ . '/*.php'))
    ->each(function ($path) {
        if (basename($path) !== basename(__FILE__)) {
            require $path;
        }
    });
