<?php


/**
 * Business Workflow Helpers.
 */
if (! function_exists('task')) {
    function task($slug)
    {
        return \App\Services\TaskService::make($slug);
    }
}
