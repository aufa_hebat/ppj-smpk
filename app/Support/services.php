<?php


use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\AppointedCompany;

/*
 * SAP Service Helper
 */
if (! function_exists('sap')) {
    function sap(Ipc $ipc)
    {
        return \App\Services\SapService::make($ipc);
    }
}

if (! function_exists('ipc')) {
    function ipc(Ipc $ipc)
    {
        return \App\Services\IpcService::make($ipc);
    }
}

/*
 * Jasper Service Helper
 */
if (! function_exists('jasper')) {
    function jasper($type, $cat, $hashslug)
    {
        return \App\Services\JasperService::make($type, $cat, $hashslug);
    }    
}

/*
 * Appointed Company Service Helper
 */
 if (! function_exists('appointed')) {
    function appointed(AppointedCompany $appointedCompany)
    {
        return \App\Services\AppointedCompanyService::make($appointedCompany);
    }    
}