<?php


use App\Models\Acquisition\Sst;

if (! function_exists('sst')) {
    function sst(Sst $sst)
    {
        return \App\Processors\SstProcessor::make($sst);
    }
}
