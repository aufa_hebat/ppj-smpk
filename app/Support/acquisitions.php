<?php


use Illuminate\Support\Facades\Cache;

/*
 * Get Acquisition Types
 * acq_types()
 */
if (! function_exists('acq_types')) {
    function acq_types()
    {
        return Cache::rememberForever('acq_types', function () {
            return \App\Models\Acquisition\Type::all();
        });
    }
}

/*
 * Get Acquisition Categories
 * acq_categories()
 */
if (! function_exists('acq_categories')) {
    function acq_categories()
    {
        return Cache::rememberForever('acq_categories', function () {
            return \App\Models\Acquisition\Category::all();
        });
    }
}

/*
 * Get Acquisition List
 * acquisition()
 */
if (! function_exists('acquisition')) {
    function acquisition()
    {
        return \App\Models\Acquisition\Acquisition::all();
    }
}
