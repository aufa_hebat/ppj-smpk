<?php

/*
 * Get Standard Code
 * standard_code('CIDB_GRED')
 */
if (! function_exists('standard_code')) {
    function standard_code($code)
    {
        return \App\Models\StandardCode::where('code', $code)->get();
    }
}

/*
 * Get Budget Code
 * budget_code('Fund')
 * Available category - Business Area, Cost Centre - Fun Centre, Functional Area, Fund, Funded Programme, GL Account
 */
if (! function_exists('budget_code')) {
    function budget_code($category)
    {
        return \App\Models\BudgetCode::where('category', $category)->get();
    }
}

/*
 * Get Banks
 * banks()
 */
if (! function_exists('banks')) {
    function banks()
    {
        return \App\Models\Bank::all();
    }
}

/*
 * Get CIDB Codes
 * cidb_codes($type)
 */
if (! function_exists('cidb_codes')) {
    function cidb_codes($type)
    {
        return \App\Models\CIDBCode::where('type', $type)->orderby('code','asc')->get();
    }
}

/*
 * Get MOF Codes
 * mof_codes($type)
 */
 if (! function_exists('mof_codes')) {
    function mof_codes($type)
    {
        return \App\Models\MOFCode::where('type', $type)->orderby('code','asc')->get();
    }
}

/*
 * Get Approval Authorities
 * authorities()
 */
if (! function_exists('authorities')) {
    function authorities()
    {
        return \App\Models\Acquisition\Approval\Authority::all();
    }
}

/*
 * Get Allocation Resources
 * allocation_resources()
 */
if (! function_exists('allocation_resources')) {
    function allocation_resources()
    {
        return \App\Models\Acquisition\AllocationResource::all();
    }
}

/* Get All Users
 * users()
 */
if (! function_exists('users')) {
    function users()
    {
        return \App\Models\User::all();
    }
}

/*
 * Get Locations
 * locations()
 */
if (! function_exists('locations')) {
    function locations()
    {
        return \App\Models\Location::all();
    }
}

/*
 * Get Department Codes
 * departments()
 */
if (! function_exists('departments')) {
    function departments()
    {
        return \App\Models\Department::all();
    }
}

/*
 * Get Sections
 * sections()
 */
if (! function_exists('sections')) {
    function sections()
    {
        return \App\Models\Section::all();
    }
}

/*
 * Get Schemes
 * schemes()
 */
if (! function_exists('schemes')) {
    function schemes()
    {
        return \App\Models\Scheme::all();
    }
}

/*
 * Get Schemes
 * grades()
 */
if (! function_exists('grades')) {
    function grades()
    {
        return \App\Models\Grade::all();
    }
}

/*
 * Get Positions
 * positions()
 */
if (! function_exists('positions')) {
    function positions()
    {
        return \App\Models\Position::all();
    }
}

/*
 * Get Period Types
 * period_types()
 */
if (! function_exists('period_types')) {
    function period_types()
    {
        return \App\Models\PeriodType::all();
    }
}

/*
 * Get bon types
 */
if (! function_exists('bon_types')) {
    function bon_types()
    {
        return \App\Models\BonType::all();
    }
}


if (! function_exists('bon_types_bekalan_perkhidmatan')) {
    function bon_types_bekalan_perkhidmatan()
    {
        return \App\Models\BonType::where('name','!=','Wang Jaminan Pelaksanaan')->get();
    }
}

/*
 * Get insurances
 */
if (! function_exists('insurances')) {
    function insurances()
    {
        //TODO - 
        return \App\Models\Insurance::where('name','ETIQA GENERAL TAKAFUL BERHAD')->get();
        //ori return \App\Models\Insurance::all();
    }
}

/*
 * Get Positions
 * positions()
 */
 if (! function_exists('termination_types')) {
     function termination_types()
     {
         return \App\Models\TerminationType::all();
     }
 }
