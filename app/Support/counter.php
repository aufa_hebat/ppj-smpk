<?php


/*
 * Get Counter based on department code
 */
if (! function_exists('counter')) {
    function counter()
    {
        $department_id = null !== optional(user()->department)->id ? user()->department->id : \App\Models\Department::ALL_ID;

        return \App\Models\Counter::where('department_id', $department_id)->first();
    }
}
