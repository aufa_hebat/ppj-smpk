<?php


/*
 * Send Notification To User
 */
if (! function_exists('notify')) {
    function notify($identifier)
    {
        return \App\Services\NotificationService::make($identifier);
    }
}
