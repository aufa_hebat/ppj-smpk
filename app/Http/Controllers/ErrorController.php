<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function recordNotFound(Request $request)
    {
        return view('errors.404-record');
    }
}
