<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\AllocationResource;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;
use DB;

class ReportingController extends Controller
{
    public function index()
    {
        $repot = Ipc::where('deleted_at',null)->groupBy(DB::raw("MONTH(evaluation_at)"),DB::raw("YEAR(evaluation_at)"))->select(DB::raw("MONTH(evaluation_at) as months"),DB::raw("YEAR(evaluation_at) as years"))->get();
        
        return view('reporting.prestasi.index', compact('repot'));
    }

    public function prestasi($month,$year)
    {
        $getMonth   = Carbon::createFromFormat('m', $month)->format('F');
        $getYear    = $year ?? null;
        $ipc        = Ipc::where('deleted_at',null)->whereMonth('evaluation_at',$month)
                    ->whereYear('evaluation_at',$year)->get();
       
        $allocation = AllocationResource::where('name','!=','Lain-Lain')->get();
        $getit      = null;
        $get_other  = null;
        if(!empty($ipc) && !empty($ipc->pluck('sst')) && !empty($ipc->pluck('sst')->pluck('acquisition')) && !empty($ipc->pluck('sst')->pluck('acquisition')->pluck('approval'))){
            foreach($ipc->pluck('sst')->pluck('acquisition')->pluck('approval') as $approval){
                if($approval->other != null){
                    if($getit == null){
                        $getit = $approval->other;
                    }else{
                        $getit = $getit.','.$approval->other;
                    }
                }
            }
            if($getit != null){
                $get_other = explode(',',$getit);
            }
        }
        $inv_7 = [];$inv_less_14 = [];$inv_more_14 = [];
        if(!empty($ipc)){
            foreach($ipc as $pc){
                if(!empty($pc->ipcInvoice)){
                    foreach($pc->ipcInvoice as $inv){
                        if($inv->invoice_receive_at != null && $inv->sap_date != null){
                            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $inv->invoice_receive_at);
                            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $inv->sap_date);
                            $differ = $to->diffInDays($from);
                            if($differ < 8){
                                array_push($inv_7, [
                                    'inv_id'        => $inv->id,
                                    'amount'        => $inv->demand_amount,
                                    'type'          => $inv->sst->acquisition->approval->allocation_resource_id,
                                ]);
                            }elseif($differ > 7 && $differ < 15){
                                array_push($inv_less_14, [
                                    'inv_id'        => $inv->id,
                                    'amount'        => $inv->demand_amount,
                                    'type'          => $inv->sst->acquisition->approval->allocation_resource_id,
                                ]);
                            }else{
                                array_push($inv_more_14, [
                                    'inv_id'        => $inv->id,
                                    'amount'        => $inv->demand_amount,
                                    'type'          => $inv->sst->acquisition->approval->allocation_resource_id,
                                ]);
                            }
                            
                        }
                    }
                }
            }
        }
        $data = [
            'ipc'           => $ipc,
            'getYear'       => $getYear,
            'getMonth'      => $getMonth,
            'allocation'    => $allocation,
            'getit'         => $getit,
            'get_other'     => $get_other,
            'inv_7'         => $inv_7,
            'inv_less_14'   => $inv_less_14,
            'inv_more_14'   => $inv_more_14,
            ];
        
        return view('reporting.prestasi.prestasi')->with($data);
    }
    
    public function create()
    {
    }

    public function store(Request $request)
    {
    }

    public function show($id)
    {  
    }
    
    public function update($id)
    {  
    }
    
    public function destroy($id)
    {  
    }
    
}
