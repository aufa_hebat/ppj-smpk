<?php

namespace App\Http\Controllers\Contract\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Release;
use App\Models\Acquisition\Bon;
use Carbon\Carbon;

class ReleaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.post.release.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sst       = Sst::with(['acquisition', 'acquisition.appointed', 'acquisition.approval', 'acquisition.approval.type', 'acquisition.category'])->where('hashslug', $id)->first();
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $bon = Bon::where('sst_id',$sst->id)->get();
        
        $new_eot_date = null;
        if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }

        return view('contract.post.release.show', compact(
            'sst',
            'appointed',
            'new_eot_date',
            'bon'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst       = Sst::with(['acquisition', 'acquisition.appointed', 'acquisition.approval', 'acquisition.approval.type', 'acquisition.category'])->where('hashslug', $id)->first();
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $bon = Bon::where('sst_id',$sst->id)->get();
        
        $new_eot_date = null;
        if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }

        return view('contract.post.release.edit', compact(
            'sst',
            'appointed',
            'new_eot_date',
            'bon'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::find($id);
        $release = Release::updateOrCreate([
                'sst_id'    =>  $request->sst_id
            ],
            [
                'sst_id'   =>  $request->sst_id,
                'percentage'   =>  $request->percentage,
                'amount'        =>  $request->amount * 100,
            ]);

        return redirect()->route('contract.post.release.edit', $sst->hashslug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
