<?php

namespace App\Http\Controllers\Contract\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use App\Models\User;
use App\Models\RefVoc;
use App\Models\VO\Approver;
use App\Models\VO\Element;
use App\Models\VO\Item;
use App\Models\VO\SubItem;
use App\Models\VariationOrderType;
use App\Models\VO\VO;

class VariationOrderResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.post.variation-order.manage.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->validate($request, [
            'sst' => 'required',
        ]);
        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($request->sst);
        $vo_count = VO::where('sst_id', $sst->id)->count();
        $vo       = VO::create([
            'sst_id'        => $sst->id,
            'user_id'       => user()->id,
            'department_id' => user()->department->id,
            'no'            => $vo_count + 1,
        ]);

        swal()->success('Perubahanan Kerja', 'Berjaya menjana Perubahan Kerja baru.');

        return redirect()->route('contract.post.variation-order.manage.edit', $vo->hashslug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
 
        return view('contract.post.variation-order.manage.edit', compact('sst', 'appointed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ppk_amend($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;    
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        //$types = VariationOrderType::all();
        $activeWps = false;
        
        foreach($sst->variationOrders as $vos){                    
            if ($vos->active_wps_status) {
                $activeWps = $vos->active_wps_status;    
            }
        }
        
        if ($activeWps) {
            $types = VariationOrderType::all();
        } else {
            $wps_id = '3';
            $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
        }        

        $PPKElemen = Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKItem = Item::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        $defaultVoc = RefVoc::where('deleted_at', null)->get();
        $officer    = Approver::where('vo_id', $vo->id)->get();
        $user       = User::whereHas('grade', function ($query) {
                            $query->where('name', '>=', '44');
                        })->where('deleted_at', null)->orderBy('name')->get();

                
        //return view('contract.post.variation-order.manage.partials.forms.amend.ppk', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub'));
        return view('contract.post.variation-order.manage.partials.forms.amend.update', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub', 'officer', 'user', 'appointed', 'defaultVoc'));       
    }

    public function ppkAmendResult($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);        
        $sst = $vo->sst;   
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first(); 
        $activeWps = false;

        foreach($sst->variationOrders as $vos){
            
            if ($vos->active_wps_status) {
                $activeWps = $vos->active_wps_status;    
            }
        }

        if ($activeWps) {
            $types = VariationOrderType::all();
        } else {
            $wps_id = '3';
            $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
        }

        $PPKElemen = Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKItem = Item::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        return view('contract.post.variation-order.manage.partials.forms.amend.ppk_result', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub', 'appointed'));
    }

    public function ppkAmendApprover($id)
    {
        $vo         = VO::withDetails()->findByHashSlug($id);
        $sst        = $vo->sst; 
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $defaultVoc = RefVoc::where('deleted_at', null)->get();
        $officer    = Approver::where('vo_id', $vo->id)->get();
        $user       = User::whereHas('grade', function ($query) {
                            $query->where('name', '>=', '44');
                        })->where('deleted_at', null)->orderBy('name')->get();

        
        return view('contract.post.variation-order.manage.partials.forms.amend.ppk_approver', compact('vo', 'id', 'sst', 'officer', 'user', 'appointed', 'defaultVoc'));
    }

    public function ppkAmendShow($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;    
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $activeWps = false;
        //$types = VariationOrderType::all();
        
        foreach($sst->variationOrders as $vos){                    
            if ($vos->active_wps_status) {
                $activeWps = $vos->active_wps_status;    
            }
        }
        
        if ($activeWps) {
            $types = VariationOrderType::all();
        } else {
            $wps_id = '3';
            $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
        }

        $PPKElemen = Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKItem = Item::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        $officer    = Approver::where('vo_id', $vo->id)->get();
        $user       = User::where('deleted_at', null)->orderBy('name')->get();

        //return view('contract.post.variation-order.manage.partials.forms.shows.ppk', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub'));
        return view('contract.post.variation-order.manage.partials.forms.shows.show', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub', 'officer', 'user', 'appointed'));
    }

    public function showPpkAmendResult($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;    
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $activeWps = false;

        foreach($sst->variationOrders as $vos){
            
            if ($vos->active_wps_status) {
                $activeWps = $vos->active_wps_status;    
            }
        }

        if ($activeWps) {
            $types = VariationOrderType::all();
        } else {
            $wps_id = '3';
            $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
        }

        $PPKElemen = Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKItem = Item::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        return view('contract.post.variation-order.manage.partials.forms.shows.ppk_result', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub', 'appointed'));
    }

    public function showPpkAmendApprover($id)
    {
        $vo         = VO::withDetails()->findByHashSlug($id);
        $sst        = $vo->sst; 
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $officer    = Approver::where('vo_id', $vo->id)->get();
        $user       = User::where('deleted_at', null)->orderBy('name')->get();

        
        return view('contract.post.variation-order.manage.partials.forms.shows.ppk_approver', compact('vo', 'id', 'sst', 'officer', 'user', 'appointed'));
    }
}
