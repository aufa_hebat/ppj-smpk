<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\EotBon;
use App\Models\Acquisition\EotInsurance;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExtensionEditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $appointed_company = AppointedCompany::withDetails()->findByHashSlug($request->hashslug);

        return view('contract.post.extension.edit_list.index', compact('appointed_company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eot         = Eot::findByHashSlug($id);
        $eot_approve = $eot->eot_approve;
        $bon         = EotBon::where('eot_id', $eot->id)->first();
        $insurance   = EotInsurance::where('eot_id', $eot->id)->first();
        $sst         = Sst::where([
                    ['company_id', '=', $eot->appointed_company->company_id],
                    ['acquisition_id', '=', $eot->appointed_company->acquisition_id],
                ])->first();
        $appointed_company = $eot->appointed_company;

        $expiry_date   = sst($sst)->getExpiredDate();
        $proposed_date = sst($sst)->getProposedDate();

        $eot_one = Eot::where('appointed_company_id', $appointed_company->id)
                ->whereHas('eot_approve', function ($query) {
                    $query->where('approval', '1');
                })->withDetails()->latest()->first();
        
        $new_eot_date = null;
        if(!empty($appointed_company->eot) && !empty($appointed_company->eot->pluck('eot_approve')) && !empty($appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }

        if (! empty($eot_one->eot_approve->id)) {
            $latest_end_date = Carbon::CreateFromFormat('Y-m-d H:i:s', $eot_one->eot_approve->approved_end_date)->format('d/m/Y');
        } else {
            $latest_end_date = Carbon::CreateFromFormat('Y-m-d H:i:s', $sst->end_working_date)->format('d/m/Y');
        }

        $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        $review          = Review::where('eot_id', $eot->id)->where('type', 'EOT')->orderby('id', 'desc')->first();


        $reviewlog      = ReviewLog::where('eot_id', $eot->id)->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

        // 1st semakan
        $sl1 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S2')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s2log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
        if (! empty($s2log)) {
            $semakan2log = $s2log;
        }

        // 2nd semakan
        $sl2 = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNPP')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s3log = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
        if (! empty($s3log)) {
            $semakan3log = $s3log;
        }
        $s3zero = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNPP')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s3zero)) {
            $semakan3zero = $s3zero;
        }

        // 3rd semakan
        $sl3 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S4')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s4log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S4')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s4log)) {
            $semakan4log = $s4log;
        }

        // 4th semakan
        $sl4 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S5')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s5log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S5')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s5log)) {
            $semakan5log = $s5log;
        }

        // 5th semakan
        $sl5 = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNBPUB')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('department','BPUB')->orderby('id', 'desc')->get();
        $cnbpubs = ReviewLog::where('eot_id', $eot->id)->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->where('department','BPUB')->orderby('id', 'desc')->first();
        if (! empty($cnbpubs)) {
            $cetaknotisbpubs = $cnbpubs;
        }

        // nth semakan
        $sln = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNBPUB')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('department','PELAKSANA')->orderby('id', 'desc')->get();
        $cnns = ReviewLog::where('eot_id', $eot->id)->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->where('department','PELAKSANA')->orderby('id', 'desc')->first();
        if (! empty($cnns)) {
            $cetaknotisns = $cnns;
        }


        // 6th semakan
        $sl6 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S7')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s7log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S7')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s7log)) {
            $semakan7log = $s7log;
        }

        // 7th semakan
        $sl7 = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNUU')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnuulog = ReviewLog::where('eot_id', $eot->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($cnuulog)) {
            $cetaknotisuulog = $cnuulog;
        }

        $s1              = Review::where('eot_id', $eot->id)->where('status', 'S1')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s1)) {
            $semakan1 = $s1;
        }
        $s2 = Review::where('eot_id', $eot->id)->where('status', 'S2')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s2)) {
            $semakan2 = $s2;
        }
        $s3 = Review::where('eot_id', $eot->id)->where('status', 'S3')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s3)) {
            $semakan3 = $s3;
        }
        $s4 = Review::where('eot_id', $eot->id)->where('status', 'S4')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s4)) {
            $semakan4 = $s4;
        }
        $s5 = Review::where('eot_id', $eot->id)->where('status', 'S5')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s5)) {
            $semakan5 = $s5;
        }
        $s6 = Review::where('eot_id', $eot->id)->where('status', 'S6')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s6)) {
            $semakan6 = $s6;
        }
        $s7 = Review::where('eot_id', $eot->id)->where('status', 'S7')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s7)) {
            $semakan7 = $s7;
        }
        $cn = Review::where('eot_id', $eot->id)->where('status', 'CN')->where('type', 'EOT')->orderby('id', 'desc')->where('progress', '1')->first();
        if (! empty($cn)) {
            $cetaknotis = $cn;
        }
        $cnuu = Review::where('eot_id', $eot->id)->where('status', 'CN')->where('type', 'EOT')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
        if (! empty($cnuu)) {
            $cetaknotisuu = $cnuu;
        }
        $cnbpub = Review::where('status', 'Selesai')->where('eot_id', $eot->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'EOT')->orWhere('status', null)->where('eot_id', $eot->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'EOT')->first();
        if (! empty($cnbpub)) {
            $cetaknotisbpub = $cnbpub;
        }
        $kuiri = Review::where('eot_id', $eot->id)->where('status', null)->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($kuiri)) {
            $quiry = $kuiri;
        }
        $urus = Review::where('eot_id', $eot->id)->where('status', 'N')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($urus)) {
            $urusetia = $urus;
        }
        $urus1 = Review::where('eot_id', $eot->id)->where('status', 'N')->where('progress', '0')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($urus1)) {
            $urusetia1 = $urus1;
        }

        return view('contract.post.extension.edit_list.edit', compact('eot_approve', 'eot', 'bon', 'insurance', 'sst', 'expiry_date', 'proposed_date', 'appointed_company', 'latest_end_date', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sln' ,'sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','cetaknotisns','semakan7log','cetaknotisuulog','reviewlog','new_eot_date'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
