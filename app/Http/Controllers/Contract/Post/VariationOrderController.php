<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\AppointedCompany;
use App\Models\User;
use App\Models\VariationOrderType;
use App\Models\VO\Cancel;
use App\Models\VO\VO;
use App\Models\VO\Approver;
use App\Models\VO\Element;
use App\Models\VO\Item;
use App\Models\VO\SubItem;
use App\Models\VO\Type;
use App\Models\VO\PPJHK\Ppjhk;
use Illuminate\Http\Request;
use App\Models\VO\Active;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\WorkflowTask;

class VariationOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.post.variation-order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contract.post.variation-order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sst' => 'required',
        ]);
        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($request->sst);
        $vo_count = VO::where('sst_id', $sst->id)->count();
        $vo       = VO::create([
            'sst_id'        => $sst->id,
            'user_id'       => user()->id,
            'department_id' => user()->department->id,
            'no'            => $vo_count + 1,
        ]);

        if($vo->no == 1){
            $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id',$vo->id)->where('flow_location','PPK')->first();
            $workflow->ppk_id = $vo->id;
            $workflow->flow_desc = 'Perubahan Kerja : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->user_id = user()->id;
            $workflow->role_id = 4;
            $workflow->flow_location = 'PPK';
            $workflow->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
            $workflow->update();
        } else {
            $workflow = new WorkflowTask();
            $workflow->acquisition_id = $vo->sst->acquisition_id;
            $workflow->ppk_id = $vo->id;
            $workflow->flow_name = $vo->sst->acquisition->reference . ' : ' . $vo->sst->acquisition->title;
            $workflow->flow_desc = 'Perubahan Kerja : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->flow_location = 'PPK';
            $workflow->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
            $workflow->user_id = user()->id;
            $workflow->role_id = 4;
            $workflow->is_claimed = 1;
            $workflow->workflow_team_id = 1;

            $workflow2 = new WorkflowTask();
            $workflow2->acquisition_id = $vo->sst->acquisition_id;
            $workflow2->ppk_id = $vo->id;
            $workflow2->flow_name = $vo->sst->acquisition->reference . ' : ' . $vo->sst->acquisition->title;
            $workflow2->flow_desc = 'Perubahan Kerja : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow2->flow_location = 'PPK';
            $workflow2->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
            $workflow2->is_claimed = 1;
            $workflow2->workflow_team_id = 2;

            $workflow->save();
            $workflow2->save();
        }

        swal()->success('Perubahanan Kerja', 'Berjaya menjana Perubahan Kerja baru.');

        return redirect()->route('contract.post.variation-order.edit', $vo->hashslug);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;
        $activeWps = false;

        foreach($sst->variationOrders as $vos){
            
            if ($vos->active_wps_status) {
                $activeWps = $vos->active_wps_status;    
            }
        }

        if($sst->acquisition->approval->type->id == '1' || $sst->acquisition->approval->type->id == '3'){
            if ($activeWps) {
                $types = VariationOrderType::whereIn('id', array(1, 3, 4, 6))->get();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::whereIn('id', array(1, 4, 6))->get();
            }
        }else{
            if ($activeWps) {
                $types = VariationOrderType::all();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
            }
        }


        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        return view('contract.post.variation-order.show', compact('vo', 'id', 'types', 'BQElemen', 'BQItem', 'BQSub'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $ipc_akhir = \App\Models\Acquisition\Ipc::where('sst_id', $sst->id)->where('last_ipc', 'y')->count();
        // $company   = Company::where('id', '!=', $sst->company_id)->where('deleted_at', null)->get();

        return view('contract.post.variation-order.edit', compact('sst', 'appointed', 'ipc_akhir'));

        // $vo = VO::withDetails()->findByHashSlug($id);
        // if ($vo->active_wps_status) {
        //     $types = VariationOrderType::all();
        // } else {
        //     $wps_id = '3';
        //     $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
        // }
        // $BQElemen = $vo->sst->acquisition->bqElements;
        // $BQItem   = $vo->sst->acquisition->bqItems;
        // $BQSub    = $vo->sst->acquisition->bqSubItems;

        // $PKElemen = $vo->vo_pk_elements;
        // $PKItem   = $vo->vo_pk_items;
        // $PKSub    = $vo->vo_pk_sub_items;

        // return view('contract.post.variation-order.edit', compact('vo', 'id', 'types', 'BQElemen', 'BQItem', 'BQSub', 'PKElemen', 'PKItem', 'PKSub'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sst' => 'required',
        ]);
        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($request->sst);
        $vo_count = VO::where('sst_id', $sst->id)->count();
        $vo       = VO::create([
            'sst_id'        => $sst->id,
            'user_id'       => user()->id,
            'department_id' => user()->department->id,
            'no'            => $vo_count + 1,
        ]);

        swal()->success('Perubahanan Kerja', 'Berjaya menjana Perubahan Kerja baru.');

        return redirect()->route('contract.post.variation-order.edit', $vo->hashslug);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        VO::findByHashSlug($id)->delete();

        swal()->success('Variation Order', 'Rekod berjaya dihapuskan.');

        return redirect()->route('variation-order.index');
    }

    public function ppk($id)
    {
        $sst       = Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $activeWps = "";
        
        foreach($sst->variationOrders as $vos){                    
            if (($vos->active_wps_status) && ($vos->status == 2)) {
                $activeWps = $vos->active_wps_status;    
            }
        }
        

        if($sst->acquisition->approval->type->id == '1' || $sst->acquisition->approval->type->id == '3'){
            if ($activeWps) {
                $types = VariationOrderType::whereIn('id', array(1, 3, 4, 6))->get();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::whereIn('id', array(1, 4, 6))->get();
            }
        }else{
            if ($activeWps) {
                $types = VariationOrderType::all();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
            }
        }



        // $wps_id = '3';
        // $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
        // $vo_count = VO::where('sst_id', $sst->id)->count();
        // $vo       = VO::create([
        //     'sst_id'        => $sst->id,
        //     'user_id'       => user()->id,
        //     'department_id' => user()->department->id,
        //     'no'            => $vo_count + 1,
        // ]);

        return view('contract.post.variation-order.partials.forms.create.ppk', compact('sst', 'id', 'types', 'appointed'));
    }

    public function ppk_edit($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $activeWps = "";

        foreach($sst->variationOrders as $vos){
            
            if (($vos->active_wps_status) && ($vos->status == 2)) {
                $activeWps = $vos->active_wps_status;    
            }
        }

        if($sst->acquisition->approval->type->id == '1' || $sst->acquisition->approval->type->id == '3'){
            if ($activeWps) {
                $types = VariationOrderType::whereIn('id', array(1, 3, 4, 6))->get();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::whereIn('id', array(1, 4, 6))->get();
            }
        }else{
            if ($activeWps) {
                $types = VariationOrderType::all();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
            }
        }

        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        $PKElemen = null;
        $PKItem = null;
        $PKSub = null;

        foreach($vo->types as $type){
            if($type->variation_order_type_id == 2){
                $PKElemen = Element::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKItem = Item::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKSub = SubItem::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
            }
        }   

        if (! empty($vo)) {
            $review_previous = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNBPUB')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }
            
            $s1              = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S1')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S3')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S6')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'VO')->where('document_contract_type', 'PPK')->where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', null)->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'N')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'N')->where('progress', '0')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }   

        return view('contract.post.variation-order.partials.forms.update.ppk', compact('vo', 'id', 'types', 'BQElemen', 'BQItem', 'BQSub', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'appointed', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    public function pk($id)
    {
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $PKElemen = Element::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PKItem = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PKSub = SubItem::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();


        $statusPk = 0;
        $totalAmount = 0;
        $totalUsed = 0;
        $totalPk = 0;
        $balanced = 0;
        $percentage = 0;
        $maximumPk = 0;
        $defaultMaximumPk = 0;               

        if($sst->acquisition->category->name == 'Tender Terbuka' || $sst->acquisition->category->name == 'Tender Terhad'){
            $percentage = 0.1;     
            $defaultMaximumPk = 100000000;           
        }else if($sst->acquisition->category->name == 'Sebut Harga' || $sst->acquisition->category->name == 'Sebut Harga B'){
            $percentage = 0.2;
            $defaultMaximumPk = 10000000;
        }

        $maximumPk = $appointed->offered_price * $percentage;
        if($maximumPk > $defaultMaximumPk){
            $totalAmount = $defaultMaximumPk;
        }else{
            $totalAmount = $maximumPk;
        }

            
        $ppkList = VO::where('sst_id', $sst->id)->where('deleted_at', null)->get(); 
                   
        foreach($ppkList as $ppkL){                
            if($ppkL->vo_elements->count() > 0){
                foreach($ppkL->vo_elements as $ppkVoE){       
                    
                    if(($ppkVoE->vo_type->variation_order_type->id == $type->variation_order_type->id)&&($ppkVoE->status == null || $ppkVoE->status == '1')){ 
                        $totalPk = 0;

                        if($ppkVoE->status == '1' && $ppkVoE->ppjhk_id != null){
                            $totalPk = \App\Models\VO\PPJHK\Element::where('ppjhk_id', $ppkVoE->ppjhk_id)->where('vo_element_id', $ppkVoE->id)->pluck('net_amount')->sum();
                        }else{
                            foreach($ppkVoE->items as $ppkVoI){
                                $totalPk = $totalPk + $ppkVoI->subItems()->where('deleted_at', null)                            
                                                                         ->pluck('net_amount')->sum();                                                                                                    
                            }
                        }                        
          
                        if($totalPk > 0){
                            $totalUsed = $totalUsed + $totalPk;
                        }
                    }
                }                    
            }                
        }
         
        $balanced = $totalAmount - $totalUsed;   


        return view('contract.post.variation-order.partials.forms.update.pk', compact('vo', 'id', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'appointed', 'totalAmount', 'totalUsed', 'balanced'));
    }

    public function kkk($id)
    {
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $KKKElemen = Element::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $KKKItem = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $KKKSub = SubItem::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();


        return view('contract.post.variation-order.partials.forms.update.kkk', compact('vo', 'id', 'KKKElemen', 'KKKItem', 'KKKSub', 'sst', 'appointed'));
    }

    public function ps($id)
    {
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $PSElemen = Element::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PSItem = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PSSub = SubItem::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();

        $BqEl = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->groupBy('bq_element_id')->get();

        return view('contract.post.variation-order.partials.forms.update.ps', compact('vo', 'id', 'PSElemen', 'PSItem', 'PSSub', 'sst', 'appointed'));
    }

    public function searchBQ($id)
    {
        $id = substr($id,1);
        $PKElemen = Element::find($id);
        $sst = $PKElemen->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $elements = \App\Models\BillOfQuantity\Element::where([
            ['acquisition_id', '=', $PKElemen->sst->acquisition_id]
        ])->get();

        $items = \App\Models\BillOfQuantity\Item::where([
            ['acquisition_id', '=', $PKElemen->sst->acquisition_id]
        ])->get();

        $subItems = \App\Models\BillOfQuantity\SubItem::where([
            ['acquisition_id', '=', $PKElemen->sst->acquisition_id]
        ])->get();

        return view('contract.post.variation-order.partials.forms.update.searchBQ', compact('id', 'elements', 'items', 'subItems', 'sst', 'appointed'));
    }

    public function selectBQ(Request $request, $id)
    {
        $element = Element::find($id);

        if(! empty($request->vo)){
            foreach ($request->vo as $vo) {
                $subItem = \App\Models\BillOfQuantity\SubItem::find($vo['id']);
                $item = Item::where([
                    ['vo_element_id', $id],
                    ['bq_item_id', $subItem->bqItem->id]
                ])->first();

                if(empty($item)){
                    $item = Item::create([
                        'item'          =>  str_replace("'","`",$subItem->bqItem->item),
                        'description'   =>  str_replace("'","`",$subItem->bqItem->description),
                        'sst_id'        =>  $element->sst_id,
                        'vo_id'         =>  $element->vo_id,
                        'vo_type_id'    =>  $element->vo_type_id,
                        'bq_element_id' =>  $subItem->bqElement->id,
                        'bq_item_id'    =>  $subItem->bqItem->id,
                        'vo_element_id' =>  $element->id,
                        'vo_active_id'  =>  $element->vo_active_id,
                    ]);
                }

                if($element->vo_type->variation_order_type->id != 3){
                    SubItem::create([
                        'item'              => str_replace("'","`",$subItem->item),
                        'description'       => str_replace("'","`",$subItem->description),
                        'unit'              => $subItem->unit,
                        'rate_per_unit'     => $subItem->rate_per_unit_adjust,
                        'quantity_omission' => $subItem->quantity,
                        'quantity_addition' => $subItem->quantity,
                        'amount_omission'   => $subItem->amount_adjust,
                        'amount_addition'   => $subItem->amount_adjust,
                        'bq_subitem_id'     => $subItem->id,
                        'bq_element_id'     =>  $subItem->bqElement->id,
                        'bq_item_id'        =>  $subItem->bqItem->id,
                        'sst_id'            =>  $element->sst_id,
                        'vo_id'             =>  $element->vo_id,
                        'vo_type_id'        =>  $element->vo_type_id,
                        'vo_item_id'        =>  $item->id,
                        'vo_active_id'      =>  $element->vo_active_id,
                        'bq_reference'      =>  $subItem->bqElement->no . '.' . $subItem->bqItem->no . '.' . $subItem->no,
                        'status_lock'       => 'BQ Rate'
                    ]);
                }else{
                    SubItem::create([
                        'item'              => str_replace("'","`",$subItem->item),
                        'description'       => str_replace("'","`",$subItem->description),
                        'unit'              => $subItem->unit,
                        'rate_per_unit'     => $subItem->rate_per_unit_adjust,
                        'quantity_omission' => 0,
                        'quantity_addition' => 0,
                        'amount_omission'   => 0,
                        'amount_addition'   => 0,
                        'bq_subitem_id'     => $subItem->id,
                        'bq_element_id'     =>  $subItem->bqElement->id,
                        'bq_item_id'        =>  $subItem->bqItem->id,
                        'sst_id'            =>  $element->sst_id,
                        'vo_id'             =>  $element->vo_id,
                        'vo_type_id'        =>  $element->vo_type_id,
                        'vo_item_id'        =>  $item->id,
                        'vo_active_id'      =>  $element->vo_active_id,
                        'bq_reference'      =>  $subItem->bqElement->no . '.' . $subItem->bqItem->no . '.' . $subItem->no,
                    ]);
                }

            }
        }

        $type = Type::find($element->vo_type_id);      
        
        if($type->variation_order_type_id == 2){
            return redirect()->route('contract.post.variation-order.pk', $type->hashslug);
        }else if($type->variation_order_type_id == 3){
            $active = Active::find($element->vo_active_id);
            //{{ route('contract.post.variation-order-wps.wps-select',['vo' => $active->hashslug, 'id' => $vo->id]) }}
            if($type->vo->type == 'WPS'){
                return redirect()->route('contract.post.variation-order-wps.wps-select', [$active->hashslug, $type->vo->id ]);
            }else{
                return redirect()->route('contract.post.variation-order.wps', [$active->hashslug, $type->vo->id ]);
            }
        }else if($type->variation_order_type_id == 4){
            return redirect()->route('contract.post.variation-order.ps', $type->hashslug);
        }else if($type->variation_order_type_id == 5){
            return redirect()->route('contract.post.variation-order.kkk', $type->hashslug);
        }
        
    }

    public function searchBQPSKKK($id)
    {

        $type = Type::findByHashSlug($id);
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $elements = \App\Models\BillOfQuantity\Element::where([
            ['acquisition_id', '=', $sst->acquisition_id]
        ])->get();

        $items = \App\Models\BillOfQuantity\Item::where([
            ['acquisition_id', '=', $sst->acquisition_id]
        ])->get();

        $subItems = \App\Models\BillOfQuantity\SubItem::where([
            ['acquisition_id', '=', $sst->acquisition_id]
        ])->get();

        return view('contract.post.variation-order.partials.forms.update.searchBQ-PS-KKK', compact('id', 'elements', 'items', 'subItems', 'sst', 'appointed'));
    }


    public function selectBQPSKKK(Request $request, $voTypeId)
    {
        // $element = Element::where('vo_type_id',$voTypeId);
        $type = Type::findByHashSlug($voTypeId);

        if(! empty($request->vo)){
            foreach ($request->vo as $vo) {
                $subItem = \App\Models\BillOfQuantity\SubItem::find($vo['id']);

                $element = Element::where([
                    ['vo_type_id', $type->id],
                    ['bq_element_id', $subItem->bqItem->bqElement->id]
                ])->first();

                if(empty($element)){
                    $element = Element::create([
                        'element'       =>  str_replace("'","`",$subItem->bqElement->element),
                        // 'description'   =>  str_replace("'","`",$subItem->bqElement->description),
                        'sst_id'        =>  $type->vo->sst_id,
                        'vo_id'         =>  $type->vo_id,
                        'vo_type_id'    =>  $type->id,
                        'bq_element_id' =>  $subItem->bqElement->id,
                    ]);
                }

                $item = Item::where([
                    ['vo_element_id', $element->id],
                    ['bq_item_id', $subItem->bqItem->id]
                ])->first();

                if(empty($item)){
                    $item = Item::create([
                        'item'          =>  str_replace("'","`",$subItem->bqItem->item),
                        'description'   =>  str_replace("'","`",$subItem->bqItem->description),
                        'sst_id'        =>  $element->sst_id,
                        'vo_id'         =>  $element->vo_id,
                        'vo_type_id'    =>  $element->vo_type_id,
                        'bq_element_id' =>  $subItem->bqElement->id,
                        'bq_item_id'    =>  $subItem->bqItem->id,
                        'vo_element_id' =>  $element->id,
                        'vo_active_id'  =>  $element->vo_active_id,
                    ]);
                }

                if($element->vo_type->variation_order_type->id != 3){

                    $voSubItem = SubItem::where([
                        ['vo_item_id', $item->id],
                        ['bq_subitem_id', $subItem->id]
                    ])->first();

                    if(empty($voSubItem)){
                        SubItem::create([
                            'item'              => str_replace("'","`",$subItem->item),
                            'description'       => str_replace("'","`",$subItem->description),
                            'unit'              => $subItem->unit,
                            'rate_per_unit'     => $subItem->rate_per_unit_adjust,
                            'quantity_omission' => $subItem->quantity,
                            'quantity_addition' => $subItem->quantity,
                            'amount_omission'   => $subItem->amount_adjust,
                            'amount_addition'   => $subItem->amount_adjust,
                            'bq_subitem_id'     => $subItem->id,
                            'bq_element_id'     =>  $subItem->bqElement->id,
                            'bq_item_id'        =>  $subItem->bqItem->id,
                            'sst_id'            =>  $element->sst_id,
                            'vo_id'             =>  $element->vo_id,
                            'vo_type_id'        =>  $element->vo_type_id,
                            'vo_item_id'        =>  $item->id,
                            'vo_active_id'      =>  $element->vo_active_id,
                            'bq_reference'      =>  $subItem->bqElement->no . '.' . $subItem->bqItem->no . '.' . $subItem->no,
                            'status_lock'       => 'BQ Rate'
                        ]);
                    }
                }else{
                    SubItem::create([
                        'item'              => str_replace("'","`",$subItem->item),
                        'description'       => str_replace("'","`",$subItem->description),
                        'unit'              => $subItem->unit,
                        'rate_per_unit'     => $subItem->rate_per_unit_adjust,
                        'quantity_omission' => 0,
                        'quantity_addition' => 0,
                        'amount_omission'   => 0,
                        'amount_addition'   => 0,
                        'bq_subitem_id'     => $subItem->id,
                        'bq_element_id'     =>  $subItem->bqElement->id,
                        'bq_item_id'        =>  $subItem->bqItem->id,
                        'sst_id'            =>  $element->sst_id,
                        'vo_id'             =>  $element->vo_id,
                        'vo_type_id'        =>  $element->vo_type_id,
                        'vo_item_id'        =>  $item->id,
                        'vo_active_id'      =>  $element->vo_active_id,
                        'bq_reference'      =>  $subItem->bqElement->no . '.' . $subItem->bqItem->no . '.' . $subItem->no,
                    ]);
                }

            }
        }

        // $type = Type::find($voTypeId);      
        
        if($type->variation_order_type_id == 2){
            return redirect()->route('contract.post.variation-order.pk', $type->hashslug);
        }else if($type->variation_order_type_id == 3){
            $active = Active::find($element->vo_active_id);
            if($type->vo->type == 'WPS'){
                return redirect()->route('contract.post.variation-order-wps.wps-select', [$active->hashslug, $type->vo->id ]);
            }else{
                return redirect()->route('contract.post.variation-order.wps', [$active->hashslug, $type->vo->id ]);
            }
        }else if($type->variation_order_type_id == 4){
            return redirect()->route('contract.post.variation-order.ps', $type->hashslug);
        }else if($type->variation_order_type_id == 5){
            return redirect()->route('contract.post.variation-order.kkk', $type->hashslug);
        }
        
    }

    public function activeWps($id){
        
        $type = Type::findByHashSlug($id);        
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $active = Active::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();  

        $activePending = Active::whereHas('vo', function ($query) {
            $query->where('deleted_at', null)->whereIn('status', ['9', '1']);
        })->where([
            ['sst_id', $sst->id],
            ['status', null]
        ])->get(); 
        
        $cancel = Cancel::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();

        $cancelPending = Cancel::whereHas('vo', function ($query) {
            $query->whereIn('status', ['9', '1'])->where('deleted_at', null);
        })->where([
            ['sst_id', $sst->id],
            ['status', null]
        ])->get(); 

        //$BQElemen = $vo->sst->acquisition->bqElements()->where('deleted_at', null)->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%')->get();
//        $BQElemen = $vo->sst->acquisition->bqElements()
//            ->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
//            ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%')
//            ->where('acquisition_id', $vo->sst->acquisition->id)
//            ->where('deleted_at', null)            
//            ->first();
        $BQElemen = $vo->sst->acquisition->bqElements()
            ->where(function($q) {
                $q->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
                ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%');
            })->where('acquisition_id', $vo->sst->acquisition->id)
            ->where('deleted_at', null)            
            ->first();
        $BQItem   = $vo->sst->acquisition->bqItems
                        ->whereNotIn('id', $active->pluck('bq_item_id'))
                        ->whereNotIn('id', $activePending->pluck('bq_item_id'))
                        ->whereNotIn('id', $cancel->pluck('bq_item_id'))
                        ->whereNotIn('id', $cancelPending->pluck('bq_item_id'))
                        ->where('deleted_at', null);
        $BQSub    = $vo->sst->acquisition->bqSubItems->where('deleted_at', null);  
        
        return view('contract.post.variation-order.partials.forms.update.active-wps', compact('vo', 'id', 'sst', 'type', 'BQElemen', 'BQItem', 'BQSub', 'appointed'));        
    }

    public function editSelectWps($id){
        
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        $PKElemen = null;
        $PKItem = null;
        $PKSub = null;

        foreach($vo->types as $type){
            if($type->variation_order_type_id == 2){
                $PKElemen = Element::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKItem = Item::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKSub = SubItem::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
            }
        }  
        
        $cancels = Cancel::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();

        // get list ppk type=wps, status=2, ppk_id=null
        $wpsList = VO::where([
            ['type', 'WPS'],
            ['status', '2'],
            ['ppk_id', null]
        ])->get();

        // $wpsElementList = Element::whereHas('vo', function ($query) {
        //     $query->where([
        //         ['type', 'WPS'],
        //         ['status', '2'],
        //         ['ppk_id', null]
        //     ])->latest();
        // })->where([
        //     ['wps_status', 1]
        // ])->get();
        

        return view('contract.post.variation-order.partials.forms.update.wps_select', 
            compact('vo', 'id', 'BQElemen', 'BQItem', 'BQSub', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'cancels', 'appointed'));
    }

    public function wps($hashslug, $id)
    {
        $totalUsed = 0;
        $active = Active::findByHashSlug($hashslug);
        $vo = VO::find($id);
        // $type = Type::find($id);        
        $sst = $active->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        // $vo = $type->vo;

        $type_id = Type::where([
            ['vo_id', $vo->id],
            ['variation_order_type_id', 3],
            ['deleted_at' ,null]
        ])->first()->hashslug;

        $PKElemen = Element::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $PKItem = Item::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $PKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $totalUsed = SubItem::where([
            ['vo_active_id', '=', $active->id],
            ['wps_status', '=', 1]
        ])->pluck('net_amount')->sum();

        return view('contract.post.variation-order.partials.forms.update.wps', compact('vo', 'id', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'type_id','active', 'totalUsed', 'appointed'));
    }

    public function cancelWps($id){
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $actives = Active::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();       
        
        $cancels = Cancel::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();

        $cancelPending = Cancel::whereHas('vo', function ($query) {
            $query->whereIn('status', ['9', '1'])->where('deleted_at', null);
        })->where([
            ['sst_id', $sst->id],
            ['status', null]
        ])->get(); 

        $activePending = Active::whereHas('vo', function ($query) {
            $query->whereIn('status', ['9', '1'])->where('deleted_at', null);
        })->where([
            ['sst_id', $sst->id],
            ['status', null]
        ])->get(); 

        //$BQElemen = $vo->sst->acquisition->bqElements()->where('deleted_at', null)->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%')->get();
        //$BQElemen = $vo->sst->acquisition->bqElements()->where('deleted_at', null)->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%')->first();
        $BQElemen = $vo->sst->acquisition->bqElements()
            ->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
            ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%')
            ->where('acquisition_id', $vo->sst->acquisition->id)
            ->where('deleted_at', null)            
            ->first();
        $BQItem   = $vo->sst->acquisition->bqItems
                    ->whereNotIn('id', $cancels->pluck('bq_item_id'))
                    ->whereNotIn('id', $cancelPending->pluck('bq_item_id'))
                    ->whereNotIn('id', $activePending->pluck('bq_item_id'))
                    ->where('deleted_at', null) ;
                   
        $BQSub    = $vo->sst->acquisition->bqSubItems->where('deleted_at', null) ;    
        
        $ppjhkUsedList = Ppjhk::where('sst_id', $sst->id)->where('status', '1')->get();
        $ppjhkCurrList = Ppjhk::where('sst_id', $sst->id)->where('status', null)->get();

        return view('contract.post.variation-order.partials.forms.update.cancel-wps', 
            compact('vo', 'id', 'sst', 'type', 'BQElemen', 'BQItem', 'BQSub', 'actives', 'ppjhkUsedList', 'ppjhkCurrList', 'appointed'));        
    }


    public function ppk_amend($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;   
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first(); 
        $types = VariationOrderType::all();

        $PPKElemen = Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKItem = Item::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        return view('contract.post.variation-order.partials.forms.amend.ppk', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub', 'appointed'));
    }

    public function ppkAmendResult($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;    
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        if($sst->acquisition->approval->type->id == '1' || $sst->acquisition->approval->type->id == '3'){
            if ($vo->active_wps_status) {
                $types = VariationOrderType::whereIn('id', array(1, 3, 4, 6))->get();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::whereIn('id', array(1, 4, 6))->get();
            }
        }else{
            if ($activeWps) {
                $types = VariationOrderType::all();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
            }
        }

        $PPKElemen = Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKItem = Item::where([
            ['vo_id', '=', $vo->id],
        ])->get();
        $PPKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        return view('contract.post.variation-order.partials.forms.amend.ppk_result', compact('vo', 'id', 'sst', 'types', 'PPKElemen', 'PPKItem', 'PPKSub', 'appointed'));
    }

    public function ppkAmendApprover($id)
    {
        $vo         = VO::withDetails()->findByHashSlug($id);
        $sst        = $vo->sst; 
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $officer    = Approver::where('vo_id', $vo->id)->get();
        $user       = User::where('deleted_at', null)->orderBy('name')->get();

        
        return view('contract.post.variation-order.partials.forms.amend.ppk_approver', compact('vo', 'id', 'sst', 'officer', 'user', 'appointed'));
    }


    /* BUTIRAN PPK*/
    public function ppk_show($id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);
        $sst = $vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $activeWps = false;

        foreach($sst->variationOrders as $vos){            
            if ($vos->active_wps_status) {
                $activeWps = $vos->active_wps_status;    
            }
        }

        if($sst->acquisition->approval->type->id == '1' || $sst->acquisition->approval->type->id == '3'){
            if ($activeWps) {
                $types = VariationOrderType::whereIn('id', array(1, 3, 4, 6))->get();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::whereIn('id', array(1, 4, 6))->get();
            }
        }else{
            if ($activeWps) {
                $types = VariationOrderType::all();
            } else {
                $wps_id = '3';
                $types  = VariationOrderType::where('id', '!=', $wps_id)->get();
            }
        }

        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        $PKElemen = null;
        $PKItem = null;
        $PKSub = null;

        foreach($vo->types as $type){
            if($type->variation_order_type_id == 2){
                $PKElemen = Element::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKItem = Item::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKSub = SubItem::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
            }
        }  

        if (! empty($vo)) {
            $review_previous = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNBPUB')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }
            
            $s1              = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S1')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S3')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S6')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'VO')->where('document_contract_type', 'PPK')->where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'VO')->where('document_contract_type', 'PPK')->where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', null)->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'N')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id', $vo->id)->where('status', 'N')->where('progress', '0')->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }   

        return view('contract.post.variation-order.partials.forms.show.ppk', compact('vo', 'id', 'types', 'BQElemen', 'BQItem', 'BQSub', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'appointed', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    public function showActiveWps($id){
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $active = Active::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();            

        return view('contract.post.variation-order.partials.forms.show.active-wps', 
            compact('vo', 'id', 'sst', 'type', 'appointed'));        
    }

    public function showCancelWps($id){
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $actives = Active::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();        

        $cancels = Cancel::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();

        return view('contract.post.variation-order.partials.forms.show.cancel-wps', 
            compact('vo', 'id', 'sst', 'type', 'cancels', 'actives', 'appointed'));        
    }

    public function showPk($id)
    {
        $type = Type::findByHashSlug($id);

        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        

        $PKElemen = Element::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();

        $PKItem = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PKSub = SubItem::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();

        return view('contract.post.variation-order.partials.forms.show.pk', compact('vo', 'id', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'appointed'));
    }

    public function showKkk($id)
    {
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $KKKElemen = Element::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $KKKItem = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $KKKSub = SubItem::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();


        return view('contract.post.variation-order.partials.forms.show.kkk', compact('vo', 'id', 'KKKElemen', 'KKKItem', 'KKKSub', 'sst', 'appointed'));
    }

    public function showPs($id)
    {
        $type = Type::findByHashSlug($id);
        $vo = $type->vo;
        $sst = $type->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $PSElemen = Element::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PSItem = Item::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();
        $PSSub = SubItem::where([
                ['vo_id', '=', $vo->id],
                ['vo_type_id', '=', $type->id],
            ])->get();


        return view('contract.post.variation-order.partials.forms.show.ps', compact('vo', 'id', 'PSElemen', 'PSItem', 'PSSub', 'sst', 'appointed'));
    }

    public function showSelectWps($id){
        
        $vo = VO::findByHashSlug($id);
        $sst = $vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        $PKElemen = null;
        $PKItem = null;
        $PKSub = null;

        foreach($vo->types as $type){
            if($type->variation_order_type_id == 2){
                $PKElemen = Element::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKItem = Item::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKSub = SubItem::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
            }
        }   
        
        $cancels = Cancel::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();

        return view('contract.post.variation-order.partials.forms.show.wps_select', 
            compact('vo', 'id', 'BQElemen', 'BQItem', 'BQSub', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'cancels', 'appointed'));
    }

    public function showWps($hashslug, $id)
    {
        $totalUsed = 0;
        $active = Active::findByHashSlug($hashslug);
        $vo = VO::find($id);
        // $type = Type::find($id);        
        $sst = $active->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        // $vo = $type->vo;

        $type_id = Type::where([
            ['vo_id', $vo->id],
            ['variation_order_type_id', 3],
            ['deleted_at' ,null]
        ])->first()->hashslug;

        $PKElemen = Element::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $PKItem = Item::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $PKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        // $totalUsed = Element::where([
        //     ['vo_active_id', '=', $active->id],
        //     ['status', '=', 1]
        // ])->pluck('net_amount')->sum();
        $totalUsed = SubItem::where([
            ['vo_active_id', '=', $active->id],
            ['wps_status', '=', 1]
        ])->pluck('net_amount')->sum();        

        return view('contract.post.variation-order.partials.forms.show.wps', compact('vo', 'id', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'type_id','active', 'totalUsed', 'appointed'));
    }
}
