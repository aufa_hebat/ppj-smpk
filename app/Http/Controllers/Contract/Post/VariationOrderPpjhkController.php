<?php

namespace App\Http\Controllers\Contract\Post;

use Illuminate\Http\Request;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use App\Models\VO\PPJHK\Ppjhk;
use App\Models\VO\PPJHK\Element;
use App\Models\VO\Active;
use App\Models\VO\VO;
use App\Models\VariationOrderType;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\WorkflowTask;

class VariationOrderPpjhkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ppjhk = Ppjhk::withDetails()->findByHashSlug($id);
        $sst = $ppjhk->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $types = VariationOrderType::all();

        $vos = VO::where([
            ['sst_id', '=', $sst->id],
            ['status', '2'],
        ])->get();

        $PpjhkElemen = $ppjhk->elements;
        $PpjhkItem   = $ppjhk->items;
        $PpjhkSub    = $ppjhk->subItems; 

        if (! empty($ppjhk)) {
            $review_previous = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNBPUB')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }
            
            $s1              = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S1')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S3')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S6')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', null)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'N')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'N')->where('progress', '0')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }     

        return view('contract.post.variation-order.ppjhk.partials.forms.show', compact('ppjhk', 'id', 'PpjhkElemen', 'PpjhkItem', 'PpjhkSub', 'sst', 'vos', 'types', 'appointed', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ppjhk = Ppjhk::withDetails()->findByHashSlug($id);
        $sst = $ppjhk->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $types = VariationOrderType::all();

        $vos = VO::where([
            ['sst_id', '=', $sst->id],
            ['status', '2'],
        ])->get();

        $PpjhkElemen = $ppjhk->elements;
        $PpjhkItem   = $ppjhk->items;
        $PpjhkSub    = $ppjhk->subItems;   

        if (! empty($ppjhk)) {
            $review_previous = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNPP')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNBPUB')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }
            
            $s1              = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S1')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S2')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S3')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S4')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S5')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S6')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'S7')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'CN')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', null)->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'N')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id', $ppjhk->id)->where('status', 'N')->where('progress', '0')->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        } 

        return view('contract.post.variation-order.ppjhk.partials.forms.update', compact('ppjhk', 'id', 'PpjhkElemen', 'PpjhkItem', 'PpjhkSub', 'sst', 'vos', 'types', 'appointed', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ppjhk($id)
    {
        $sst       = Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $vos = VO::where([
            ['sst_id', '=', $sst->id],
            ['status', '2'],
        ])->get();
        $ppjhk = Ppjhk::where('sst_id', $sst->id);
        
        $types = VariationOrderType::all();

        return view('contract.post.variation-order.ppjhk.partials.forms.create', compact('sst', 'id', 'vos', 'types', 'ppjhk', 'appointed'));
    }

    public function amend($id)
    {
        $element = Element::where('vo_element_id', $id)->orderBy('id', 'desc')->first();
        $sst = $element->sst;
        $ppjhk = $element->ppjhk;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();        

        $totalAmount = 0;
        $totalUsed = 0;
        $totalPending = 0;
        if($element->voElement->vo_type->variation_order_type_id == 3){
           $totalAmount = Active::find($element->voElement->vo_active_id)->item->amount;

           $ppjhkList = Ppjhk::where('sst_id', $sst->id)->where('status', '1')->get();
           foreach($ppjhkList as $ppjhkL){
               $totalUsed = $totalUsed + $ppjhkL->subItems()->where('wps_status', 1)->where('vo_active_id', $element->voElement->vo_active_id)->pluck('net_amount')->sum();
           }

           $ppjhkCurrList = Ppjhk::where('sst_id', $sst->id)->where('status', null)->get();
           foreach($ppjhkCurrList as $ppjhkL){
               $totalPending = $totalPending + $ppjhkL->subItems()->where('wps_status', 1)->where('vo_active_id', $element->voElement->vo_active_id)->pluck('net_amount')->sum();
           }
        }

        return view('contract.post.variation-order.ppjhk.partials.forms.update.amend', compact('sst', 'ppjhk', 'element', 'totalAmount', 'totalUsed', 'totalPending', 'appointed'));
    }

    public function showAmend($id)
    {
        $element = Element::where('vo_element_id', $id)->orderBy('id', 'desc')->first();
        $sst = $element->sst;
        $ppjhk = $element->ppjhk;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        return view('contract.post.variation-order.ppjhk.partials.forms.show.show-amend', compact('sst', 'ppjhk', 'element', 'appointed'));
    }
}
