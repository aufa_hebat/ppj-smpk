<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Category;
use App\Models\Acquisition\Sst;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Company\CIDB;
use App\Models\Company\MOF;
use App\Models\Sale;
use Illuminate\Http\Request;

class ContractCompletedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.post.contract-completed.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sst       = Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();

        return view('contract.post.contract-completed.show',
            compact('sst', 'appointed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function showPerolehan($id)
    {
        $acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->orWhere('name', 'like', 'Tender' . '%')->pluck('name', 'id');

        $sst       = Sst::findByHashSlug($id);
        $bon       = $sst->bon;
        $insurance = $sst->insurance;
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();

        $sale = Sale::where([
                                        ['company_id', '=', $appointed->company_id],
                                        ['acquisition_id', '=', $appointed->acquisition_id],
                                    ])->first();

        $cidb = CIDB::findByHashSlug($appointed->company->hashslug);
        $mof  = MOF::findByHashSlug($appointed->company->hashslug);

        $BQElemen = Element::where('acquisition_id', $sst->acquisition->id)->get();
        $BQItem   = Item::where('acquisition_id', $sst->acquisition->id)->get();
        $BQSub    = SubItem::where('acquisition_id', $sst->acquisition->id)->get();                                    

        return view('contract.post.contract-completed.show-perolehan',
            compact('acq_cat', 'sst', 'bon', 'insurance','appointed', 'sale', 'cidb', 'mof', 'BQElemen', 'BQItem', 'BQSub'));
    }
}
