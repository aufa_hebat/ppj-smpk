<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Cmgd;
use App\Models\Acquisition\Cnc;
use App\Models\Acquisition\Cpc;
use App\Models\Acquisition\Deposit;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\Sofa;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\SubContract;
use App\Models\VO\PPJHK\Ppjhk;
// use App\Models\VariationOrderElement;
// use App\Models\VariationOrderType;
use Illuminate\Http\Request;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;

class SofaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.post.sofa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sst       = Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();
        $deposit = Deposit::where('sst_id', $sst->id)->first();

        $cpc  = Cpc::where('sst_id', $sst->id)->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();
        $cnc  = Cnc::where('sst_id', $sst->id)->first();

        $vo         = 0; // VariationOrder::where('sst_id', $sst->id)->get();
        $vo_element = 0; //VariationOrderElement::where('sst_id', $sst->id)->get();
        $vo_type    = 0; //VariationOrderType::get();

        $ipc                = Ipc::where('sst_id', $sst->id)->get();
        $prev_total_invoice = IpcInvoice::where('sst_id', $sst->id)->pluck('invoice')->sum();
        $last_ipc           = Ipc::where('sst_id', $sst->id)->orderBy('ipc_no', 'desc')->first();
        $total_lad          = Ipc::where('sst_id', $sst->id)->pluck('compensation_amount')->sum();

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();

        $total_amount   = 0.0;
        $total_invoice  = 0.0;
        $total_balanced = 0.0;

        if ($sub_contract->count() > 0) {
            foreach ($sub_contract as $index => $row) {
                $total_amount   = $total_amount   + $row->amount;
                $total_invoice  = $total_invoice  + $row->invoices->pluck('invoice')->sum();
                $total_balanced = $total_balanced + ($row->amount - $row->invoices->pluck('invoice')->sum());
            }
        }

        return view('contract.post.sofa.show',
            compact('sst', 'appointed', 'deposit', 'cpc', 'cmgd', 'cnc', 'vo', 'vo_element', 'vo_type',
            'ipc', 'prev_total_invoice', 'last_ipc', 'total_lad', 'sub_contract', 'total_amount', 'total_invoice', 'total_balanced'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst       = Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();
        $deposit = Deposit::where('sst_id', $sst->id)->first();

        $eots    =   Eot::where('sst_id', $sst->id)->get();
        $totalYear  = 0;
        $totalMonth = 0;
        $totalWeek  = 0;
        $totalDay   = 0;
        foreach($eots as $eot){
            if(!empty($eot->eot_approve)){
                if($eot->eot_approve->period_type->value == 1){
                    $totalDay = $totalDay + $eot->eot_approve->period;
                }else if($eot->eot_approve->period_type->value == 2){
                    $totalWeek = $totalWeek + $eot->eot_approve->period;
                }else if($eot->eot_approve->period_type->value == 3){
                    $totalMonth = $totalMonth + $eot->eot_approve->period;
                }else if($eot->eot_approve->period_type->value == 4){
                    $totalYear = $totalYear + $eot->eot_approve->period;
                } 
            }
        }

        $cpc  = Cpc::where('sst_id', $sst->id)->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();
        $cnc  = Cnc::where('sst_id', $sst->id)->first();

        $ppjhk         = Ppjhk::where('sst_id', $sst->id)->where('status', 1)->get();
        $vo_element = 0; //VariationOrderElement::where('sst_id', $sst->id)->get();
        $vo_type    = 0; //VariationOrderType::get();

        

        $ipc                = Ipc::where('sst_id', $sst->id)->get();
        $total_adv_amt      = Ipc::where('sst_id', $sst->id)->pluck('advance_amount')->sum();
        $prev_total_invoice = Ipc::where('sst_id', $sst->id)->where('last_ipc', 'n')->pluck('demand_amount')->sum();
        $prev_gv_adv_amt    = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('give_advance_amount')->sum();
        $last_ipc           = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->orderBy('ipc_no', 'desc')->first();
        $total_lad          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('compensation_amount')->sum();
        $wjp_amt            = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('wjp_amount')->sum();
        $wjp_lepas          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('wjp_lepas_amount')->sum();

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();
        $sofa         = Sofa::where('sst_id', $sst->id)->first();

        if (! empty($sst)) {
            $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'SOFA')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNUU')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'SOFA')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }

            $s1              = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S1')->where('type', 'SOFA')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S3')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S6')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'SOFA')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'SOFA')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'SOFA')->orWhere('status', null)->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'SOFA')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $sst->acquisition_id)->where('status', null)->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'SOFA')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }

        $total_amount   = 0.0;
        $total_invoice  = 0.0;
        $total_balanced = 0.0;

        if ($sub_contract->count() > 0) {
            foreach ($sub_contract as $index => $row) {
                $total_amount   = $total_amount   + $row->amount;
                $total_invoice  = $total_invoice  + $row->invoices->pluck('invoice')->sum();
                $total_balanced = $total_balanced + ($row->amount - $row->invoices->pluck('invoice')->sum());
            }
        }

        return view('contract.post.sofa.edit',
            compact('sst', 'appointed', 'deposit', 'cpc', 'cmgd', 'cnc', 'ppjhk', 'vo_element', 'vo_type', 'total_adv_amt', 'prev_gv_adv_amt',
            'wjp_amt', 'wjp_lepas',
            'totalDay','totalWeek', 'totalMonth', 'totalYear',
            'ipc', 'prev_total_invoice', 'last_ipc', 'total_lad', 'sub_contract', 'total_amount', 'total_invoice', 'total_balanced', 'sofa', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1', 'cnbpub', 'cetaknotisbpub','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
