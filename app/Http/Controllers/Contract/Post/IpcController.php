<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcBq;
use App\Models\Acquisition\IpcVo;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\IpcMaterial;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\EmailScheduler;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\SubContract;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\VO\PPJHK\Element as VoElement;
use App\Models\VO\PPJHK\Item as VoItem;
use App\Models\VO\PPJHK\SubItem as VoSub;
use App\Models\VO\VO;
use App\Models\VO\Active;
use App\Models\VO\Element as WpsElement;
use App\Models\VO\Item as WpsItem;
use App\Models\VO\SubItem as WpsSub;
use App\Models\VO\Cancel;
use App\Models\Company\Company;
use App\Models\Document;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\WorkflowTask;

class IpcController extends Controller
{
    
      public function __construct()
    {
        //echo    "<script type='text/javascript'>$( document ).ready(function() { $('.se-pre-con').fadeOut('slow');;});</script>";
        //echo " <div class='se-pre-con'></div>";
        $this->middleware('auth');
        
        
    }
    
    
    public function index()
    {
        return view('contract.post.ipc.index');
    }

    public function create()
    {
    }

    
    public function store(Request $request)
    {
    }

    
    public function show($id)
    {
        $sst = Sst::findByHashSlug($id);
        $ipc = Ipc::where('sst_id', $sst->id)->get();

        $ipc_latest = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->orderby('id', 'desc')->first();
        if (! empty($ipc_latest)) {
            $review_previous = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('type', 'IPC')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S2')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S4')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S4')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S5')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S5')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S9')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s8log = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S9')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s8log)) {
                $semakan8log = $s8log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'CNK')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnklog = ReviewLog::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('progress', '1')->where('status', 'CNK')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnklog)) {
                $cetaknotisklog = $cnklog;
            }

            $s1              = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S1')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S2')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S3')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S4')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S5')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s8 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S8')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s8)) {
                $semakan8 = $s8;
            }
            $s9 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'S9')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s9)) {
                $semakan9 = $s9;
            }
            $cn = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'Selesai')->where('type', 'IPC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'CN')->where('type', 'IPC')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'IPC')->orWhere('status', 'CN')->where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'IPC')->orWhere('status', null)->where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'IPC')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', null)->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'N')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $ipc_latest->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }

        $pekeso_doc  = Document::where('document_id', $sst->id)->where('deleted_at', null)->where('document_types', 'PEKESO')->orderby('id', 'desc')->limit(1)->get();
        $levi_doc    = Document::where('document_id', $sst->id)->where('deleted_at', null)->where('document_types', 'Levi')->orderby('id', 'desc')->limit(1)->get();
        $cert_doc    = Document::where('document_id', $sst->id)->where('deleted_at', null)->where('document_types', 'Sijil_IPC')->orderby('id', 'desc')->limit(1)->get();
        $summary_doc = Document::where('document_id', $sst->id)->where('deleted_at', null)->where('document_types', 'Ringkasan_IPC')->orderby('id', 'desc')->limit(1)->get();
        $status_doc  = Document::where('document_id', $sst->id)->where('deleted_at', null)->where('document_types', 'Status_IPC')->orderby('id', 'desc')->limit(1)->get();
        
        $first_invoice_cert = $sst->ipc->pluck('ipcInvoice');
        foreach($first_invoice_cert as $inv_cert){
            foreach($inv_cert as $inv_certs){
                $first_invoice_doc  = Document::where('document_id', $inv_certs->id)->where('document_types', 'INVOICE/'.$inv_certs->id)->orderby('id', 'desc')->limit(1)->get();
            }
        }

        $user = User::whereIn('position_id', ['67','2052','2032'])->where('scheme_id', '21')->where('grade_id','>=', '11')->where('grade_id','<=','16')->orderby('name', 'asc')->get();
        // return $review;
        return view('contract.post.ipc.show', compact('sst', 'ipc', 'ipc_latest', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's8', 's9', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan8', 'semakan9', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1', 'pekeso_doc', 'levi_doc', 'cert_doc', 'summary_doc', 'status_doc', 'user','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisklog','reviewlog','first_invoice_doc'));
    }

    
    public function summary($id)
    {
        $sst                 = Sst::findByHashSlug($id);
        $appointed           = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $company             = Company::where('id', '!=', $sst->company_id)->where('deleted_at', null)->get();
        $ipc                 = Ipc::where('sst_id',$sst->id)->where('deleted_at',null)->get();
        return view('contract.post.ipc.summary', compact('sst', 'appointed', 'company','ipc'));
    }
    
    public function edit($id)
    {
        $sst                 = Sst::findByHashSlug($id);
        $appointed           = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $company             = Company::where('id', '!=', $sst->company_id)->where('deleted_at', null)->get();
        $date_now            = Carbon::now()->format('Y-m-d');
        $date_4month_sign    = (!empty($sst->al_contractor_date))? $sst->al_contractor_date->addMonths(4)->format('Y-m-d'):null;
        $date_valid          = null;
        if($date_4month_sign > $date_now){
            $date_valid      = "y";
        }
        return view('contract.post.ipc.edit', compact('sst', 'appointed', 'company','date_valid'));
    }

    public function ipc_invoice($id)
    {
        $sst            = Sst::findByHashSlug($id);
        $appointed      = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $kerja          = (!empty($sst) && !empty($sst->acquisition) && !empty($sst->acquisition->approval) && ($sst->acquisition->approval->acquisition_type_id == 2 ))? "kerja": null;
        $bekalan        = (!empty($sst) && !empty($sst->acquisition) && !empty($sst->acquisition->approval) && ($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 4 || $sst->acquisition->approval->acquisition_type_id == 3))? "bekalan": null;
        
        $new_eot_date   = null;
        if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }
        $get_pre_desc   = null;
        if(!empty($sst->ipc) && $sst->ipc->count() > 0){
            $get_pre_desc = $sst->ipc->pluck('long_text')->first();
        }
        
        return view('contract.post.ipc.partials.forms.ipc_invoice', compact('sst', 'appointed', 'company', 'BQElemen', 'BQItem', 'BQSub', 'bq_prev', 'wjp', 'new_eot_date','get_pre_desc','kerja','bekalan'));
    }

    public function ipc_invoice_edit($id)
    {
        $ipc            = Ipc::with('sst','sst.company','sst.acquisition','sst.acquisition.approval','sst.acquisition.approval.financials','ipcInvoice','ipcInvoice.ipcBq','ipcInvoice.ipcBq.ipcBqElement','ipcInvoice.ipcBq.ipcBqItem','ipcInvoice.ipcBq.ipcBqSub','ipcInvoice.ipcBq.ipcBqWpsSub')
                        ->findByHashSlug($id);
        $sst            = $ipc->sst;
        $getipc         = ipc($ipc);
        $total_ipc      = $getipc->getAllIpc();
        $appointed      = $getipc->getAppointed();
        $new_contract   = $ipc->new_contract_amount;
        if(!empty($getipc->getVoCancel()) && $getipc->getVoCancel() < 0){
            $vo_batal       = -($getipc->getVoCancel());
        }else{
            $vo_batal       = $getipc->getVoCancel();
        }
        $pre_vo     = $getipc->getPreVo();
        
        $get_inv_main   = $getipc->getTotalInvMain();
        $new_eot_date   = null;   
        if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }
        $kerja          = (!empty($sst) && !empty($sst->acquisition) && !empty($sst->acquisition->approval) && ($sst->acquisition->approval->acquisition_type_id == 2 ))? "kerja": null;
        $bekalan        = (!empty($sst) && !empty($sst->acquisition) && !empty($sst->acquisition->approval) && ($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 4 || $sst->acquisition->approval->acquisition_type_id == 3))? "bekalan": null;
        $ipc_invoice    = $getipc->getIpc()->ipcInvoice;//IpcInvoice::where('ipc_id',$ipc->id)->where('deleted_at',null)->with('company','financial')->get();
        $invoice_all    = IpcInvoice::where('sst_id',$sst->id)->where('ipc_id','!=',$ipc->id)->where('deleted_at',null)->get();
        $ipc_bq         = IpcBq::where('sst_id', $ipc->sst_id)->where('deleted_at',null)->get();
        $ipc_vo         = IpcVO::where('ipc_id',$ipc->id)->where('sst_id', $ipc->sst_id)->where('deleted_at',null)->get();
        $company        = SubContract::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get() ?? null;
        $BQElemen       = Element::where('acquisition_id', $sst->acquisition_id)->where('deleted_at', null)->with('bqItems','bqSubItems')->get();
        $BQItem         = Item::where('acquisition_id', $sst->acquisition_id)->where('deleted_at', null)->get();
        $BQSub          = SubItem::where('acquisition_id', $sst->acquisition_id)->where('deleted_at', null)->get();
        $prov_id        = Element::where('acquisition_id', $sst->acquisition_id)->where('deleted_at', null)
                ->where(function($q) {
                    $q->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
                    ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%');
                })->pluck('id')->first()   ?? 0;
        
        $voElemen       = VoElement::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        $voItem         = VoItem::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        $voSub          = VoSub::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        
        $pekeso_doc     = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'IPC/PEKESO/'.$ipc->id)->orderby('id', 'desc')->limit(1)->get();
        $levi_doc       = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'IPC/LEVI/'.$ipc->id)->orderby('id', 'desc')->limit(1)->get();
        $cert_doc       = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'IPC/SIJIL_IPC/'.$ipc->id)->orderby('id', 'desc')->limit(1)->get();
        $summary_doc    = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'IPC/SUMMARY_IPC/'.$ipc->id)->orderby('id', 'desc')->limit(1)->get();
        $status_doc     = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'IPC/STATUS_IPC/'.$ipc->id)->orderby('id', 'desc')->limit(1)->get();
        $detail_doc     = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'IPC/DETAIL_IPC/'.$ipc->id)->orderby('id', 'desc')->limit(1)->get();
        
        //get vo for wps
        $all_vo         = VO::where('sst_id',$sst->id)->where('status',2)->where('deleted_at',null)->get();
        $vo_active      = Active::where('sst_id',$sst->id)->where('status',1)->where('deleted_at')->get();
        $vo_id = null;   $all_vo_wps = null; $v_id = null;
        if(!empty($all_vo) && $all_vo->count() > 0){
            foreach($all_vo as $a_v){
                if($vo_id == null){
                    $vo_id = $a_v->id;
                }else{
                    $vo_id = $vo_id.','.$a_v->id;
                }
            }
            $v_id       = explode(',',$vo_id);
            $all_vo_elm = WpsElement::whereIn('vo_id',$v_id)->where('sst_id',$sst->id)->where('status',1)->where('wps_status',1)->where('deleted_at',null)->get();
            
            if(!empty($all_vo_elm)){
                $v_elm = null;$vo_active_id = null;
                foreach($all_vo_elm as $v_e){
                    if($v_elm == null){
                        $v_elm = $v_e->id;
                    }else{
                        $v_elm = $v_elm.','.$v_e->id;
                    }
                    
                    if($vo_active_id == null){
                        $vo_active_id = $v_e->vo_active_id;
                    }else{
                        $vo_active_id = $vo_active_id.','.$v_e->vo_active_id;
                    }
                }
                $v_elm_exp  = explode(',',$v_elm);
                $all_vo_itm = WpsItem::whereIn('vo_element_id',$v_elm_exp)->whereIn('vo_id',$v_id)->where('sst_id',$sst->id)->where('wps_status',1)->where('deleted_at',null)->get();
                $v_act_exp  = explode(',',$vo_active_id);
                $vo_active  = Active::whereIn('id',$v_act_exp)->get();
            }
            if(!empty($all_vo_itm)){
                $v_itm = null;
                foreach($all_vo_itm as $v_i){
                    if($v_itm == null){
                        $v_itm = $v_i->id;
                    }else{
                        $v_itm = $v_itm.','.$v_i->id;
                    }
                }
                $v_itm_exp  = explode(',',$v_itm);
                $all_vo_sub = WpsSub::whereIn('vo_item_id',$v_itm_exp)->whereIn('vo_id',$v_id)->where('sst_id',$sst->id)->where('wps_status',1)->where('deleted_at',null)->get();
            }
        }
        
        $adv             = $getipc->getAdvAmount();
        $adv_balik       = $getipc->getAdvBalikAmount();
        $given_adv       = $adv_balik["tarik_adv"];
        $show_adv        = $adv_balik["formula"];
        $material_tahan  = $getipc->getMaterialTahanAmount();
        $wjp             = $getipc->getWjpAmount();
        $get_wjp         = $wjp["wjp"];
        $show_wjp        = $wjp["formula"];
        $wjp_balik       = $getipc->getWjpLepasAmount();
        $wjp_lepas       = $wjp_balik["wjp"];
        $show_wjp_tolak  = $wjp_balik["formula"];
        $jumlah          = $getipc->getDemandAmount();
        $demand          = $jumlah['demand'];
        $demand_show     = $jumlah['formula'];
        $lad             = $getipc->getLadAmount(); 
        //$lad_value       = $lad["value"];
        $lad_value    = 0;
        if($lad["value"] == 0){
            $lad_value = $ipc->compesation_amount;
        }else{
            $lad_value = $lad["value"];
        }
        $lad_rate        = $lad["rate"];
        $lad_day         = $lad["day"];
        $lad_formula     = $lad["formula"];
        $denda           = (!empty($ipc) && !empty($ipc->sst) && !empty($sst->acquisition) && !empty($sst->acquisition->approval) && !empty($sst->acquisition->approval->acquisition_type_id))? $sst->acquisition->approval->acquisition_type_id:null;
        
        
        $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        $review          = Review::where('ipc_id', $ipc->id)->where('type', 'IPC')->orderby('id', 'desc')->first();
            
            // 1st semakan
            $sl1         = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S2')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log       = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNPP')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log      = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero     = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNPP')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S4')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log      = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S4')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S5')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log      = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S5')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNBPUB')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs    = ReviewLog::where('ipc_id', $ipc->id)->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S9')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s8log      = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S9')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s8log)) {
                $semakan8log = $s8log;
            }

            // 7th semakan
            $sl7        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNK')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnklog     = ReviewLog::where('ipc_id', $ipc->id)->where('progress', '1')->where('status', 'CNK')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnklog)) {
                $cetaknotisklog = $cnklog;
            }

            $s1         = Review::where('ipc_id', $ipc->id)->where('status', 'S1')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2         = Review::where('ipc_id', $ipc->id)->where('status', 'S2')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3         = Review::where('ipc_id', $ipc->id)->where('status', 'S3')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4         = Review::where('ipc_id', $ipc->id)->where('status', 'S4')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5         = Review::where('ipc_id', $ipc->id)->where('status', 'S5')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s8         = Review::where('ipc_id', $ipc->id)->where('status', 'S8')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s8)) {
                $semakan8 = $s8;
            }
            $s9         = Review::where('ipc_id', $ipc->id)->where('status', 'S9')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s9)) {
                $semakan9 = $s9;
            }
            $cn         = Review::where('ipc_id', $ipc->id)->where('status', 'Selesai')->where('type', 'IPC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu       = Review::where('ipc_id', $ipc->id)->where('status', 'CN')->where('type', 'IPC')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub     = Review::where('status', 'Selesai')->where('ipc_id', $ipc->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'IPC')->orWhere('status', 'CN')->where('ipc_id', $ipc->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'IPC')->orWhere('status', null)->where('ipc_id', $ipc->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'IPC')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri      = Review::where('ipc_id', $ipc->id)->where('status', null)->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus       = Review::where('ipc_id', $ipc->id)->where('status', 'N')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1      = Review::where('ipc_id', $ipc->id)->where('status', 'N')->where('progress', '0')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
            $data = [
                'prov_id'           => $prov_id ?? null,
                'review_previous'   => $review_previous ?? null, 
                'review'            => $review ?? null, 
                's1'                => $s1 ?? null, 
                's2'                => $s2 ?? null, 
                's3'                => $s3 ?? null, 
                's4'                => $s4 ?? null, 
                's5'                => $s5 ?? null, 
                's8'                => $s8 ?? null, 
                's9'                => $s9 ?? null, 
                'sl1'               => $sl1 ?? null,
                'sl2'               => $sl2 ?? null,
                'sl3'               => $sl3 ?? null,
                'sl4'               => $sl4 ?? null,
                'sl5'               => $sl5 ?? null,
                'sl6'               => $sl6 ?? null,
                'sl7'               => $sl7 ?? null,
                'cnuu'              => $cnuu ?? null, 
                'kuiri'             => $kuiri ?? null, 
                'urus'              => $urus ?? null, 
                'urus1'             => $urus1 ?? null, 
                'semakan2log'       => $semakan2log ?? null,
                'semakan3log'       => $semakan3log ?? null,
                'semakan4log'       => $semakan4log ?? null,
                'semakan5log'       => $semakan5log ?? null,
                'cetaknotisbpubs'   => $cetaknotisbpubs ?? null,
                'cetaknotisbpub'    => $cetaknotisbpub ?? null,
                'cetaknotisklog'    => $cetaknotisklog ?? null,
                'semakan8log'       => $semakan8log ?? null,
                'semakan1'          => $semakan1 ?? null, 
                'semakan2'          => $semakan2 ?? null, 
                'semakan3'          => $semakan3 ?? null, 
                'semakan4'          => $semakan4 ?? null, 
                'semakan5'          => $semakan5 ?? null, 
                'semakan8'          => $semakan8 ?? null, 
                'semakan9'          => $semakan9 ?? null, 
                'cetaknotis'        => $cetaknotis ?? null, 
                'cetaknotisuu'      => $cetaknotisuu ?? null, 
                'cetaknotis'        => $cetaknotis ?? null, 
                'semakan3zero'      => $semakan3zero ?? null,
                'quiry'             => $quiry ?? null, 
                'urusetia'          => $urusetia ?? null, 
                'urusetia1'         => $urusetia1 ?? null,
                'show_adv'          => $show_adv ?? null,
                'ipc_invoice'       => $ipc_invoice ?? null, 
                'ipc_bq'            => $ipc_bq ?? null, 
                'ipc_vo'            => $ipc_vo ?? null,
                'BQElemen'          => $BQElemen ?? null, 
                'BQItem'            => $BQItem ?? null, 
                'BQSub'             => $BQSub ?? null, 
                'appointed'         => $appointed ?? null, 
                'ipc'               => $ipc ?? null, 
                'company'           => $company ?? null, 
                'given_adv'         => $given_adv ?? null, 
                'get_wjp'           => $get_wjp ?? null, 
                'show_wjp'          => $show_wjp ?? null,
                'demand'            => $demand ?? null, 
                'demand_show'       => $demand_show ?? null,
                'lad_value'         => $lad_value ?? null, 
                'lad_day'           => $lad_day ?? 0,
                'lad_rate'          => $lad_rate ?? 0,
                'lad_formula'       => $lad_formula ?? null,
                'wjp_lepas'         => $wjp_lepas ?? null, 
                'show_wjp_tolak'    => $show_wjp_tolak ?? null,
                'adv'               => $adv ?? null,  
                'material_tahan'    => $material_tahan ?? null, 
                'review'            => $review ?? null, 
                'pekeso_doc'        => $pekeso_doc ?? null, 
                'levi_doc'          => $levi_doc ?? null, 
                'cert_doc'          => $cert_doc ?? null, 
                'summary_doc'       => $summary_doc ?? null, 
                'status_doc'        => $status_doc ?? null,
                'detail_doc'        => $detail_doc ?? null,
                'sst'               => $sst ?? null,
                'voElemen'          => $voElemen ?? null,
                'voItem'            => $voItem ?? null,
                'voSub'             => $voSub ?? null,
                'vo_active'         => $vo_active ??  null,
                'all_vo_wps'        => $all_vo_wps ?? null,
                'all_vo'            => $all_vo ?? null,
                'all_vo_elm'        => $all_vo_elm ?? null,
                'all_vo_itm'        => $all_vo_itm ?? null,
                'all_vo_sub'        => $all_vo_sub ?? null,
                'new_eot_date'      => $new_eot_date ?? null,
                'new_contract'      => $new_contract ?? null,
                'get_inv_main'      => $get_inv_main,
                'invoice_all'       => $invoice_all ?? null,
                'denda'             => $denda,
                'total_ipc'         => $total_ipc ?? null,
                'kerja'             => $kerja ?? null,
                'bekalan'           => $bekalan ?? null,
                'userss'            => $userss ?? null,
                'userSuper'         => $userSuper ?? null,
                'userSSuper'        => $userSSuper ?? null,
                'userID'            => $userID ?? null,
                'vo_batal'          => $vo_batal ?? null,
                'pre_vo'            => $pre_vo ?? null
            ];
        return view('contract.post.ipc.partials.forms.ipc_invoice_edit')->with($data);
    }
    
    public function ipc_show($id)
    {
        $ipc            = Ipc::findByHashSlug($id);
        $sst            = Sst::where('acquisition_id', $ipc->sst->acquisition_id)->first();
        $invoice_all    = IpcInvoice::where('sst_id',$ipc->sst->id)->where('ipc_id','!=',$ipc->id)->where('deleted_at',null)->get();
        $appointed      = AppointedCompany::where('acquisition_id', $ipc->sst->acquisition_id)->where('company_id', $ipc->sst->company_id)->where('deleted_at', null)->first();
        $getipc         = ipc($ipc);
        $total_ipc      = $getipc->getAllIpc();
        //$new_contract   = $getipc->getNewContractValue();
        $new_contract   = $ipc->new_contract_amount;
        if(!empty($getipc->getVoCancel()) && $getipc->getVoCancel() < 0){
            $vo_batal       = -($getipc->getVoCancel());
        }else{
            $vo_batal       = $getipc->getVoCancel();
        }
        $pre_vo     = $getipc->getPreVo();
        $get_inv_main   = $getipc->getTotalInvMain();
        $new_eot_date = null;
        if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }
        $ipc_invoice    = IpcInvoice::where('ipc_id',$ipc->id)->where('deleted_at',null)->get();
        $ipc_bq         = IpcBq::where('sst_id', $ipc->sst_id)->where('deleted_at',null)->get();
        $ipc_vo         = IpcVO::where('ipc_id',$ipc->id)->where('sst_id', $ipc->sst_id)->where('deleted_at',null)->get();
        $company        = SubContract::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get() ?? null;
        $BQElemen       = Element::where('acquisition_id', $ipc->sst->acquisition_id)->where('deleted_at', null)->get();
        $BQItem         = Item::where('acquisition_id', $ipc->sst->acquisition_id)->where('deleted_at', null)->get();
        $BQSub          = SubItem::where('acquisition_id', $ipc->sst->acquisition_id)->where('deleted_at', null)->get();
        
        $voElemen       = VoElement::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        $voItem         = VoItem::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        $voSub          = VoSub::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        $voCancel       = Cancel::where('sst_id',$ipc->sst_id)->where('ppjhk_status',1)->where('status',1)->where('deleted_at',null)->get();
        
        $prov_id        = Element::where('acquisition_id', $ipc->sst->acquisition_id)->where('deleted_at', null)
                            ->where(function($q) {
                                $q->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
                                ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%');
                })          ->pluck('id')->first()   ?? 0;
        $user = User::whereIn('position_id', ['67','2052','2032'])->where('scheme_id', '21')->where('grade_id','>=', '11')->where('grade_id','<=','16')->orderby('name', 'asc')->get();

        $pekeso_doc     = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'PEKESO')->orderby('id', 'desc')->limit(1)->get();
        $levi_doc       = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'LEVI')->orderby('id', 'desc')->limit(1)->get();
        $cert_doc       = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'SIJIL_IPC')->orderby('id', 'desc')->limit(1)->get();
        $summary_doc    = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'SUMMARY_IPC')->orderby('id', 'desc')->limit(1)->get();
        $status_doc     = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'STATUS_IPC')->orderby('id', 'desc')->limit(1)->get();
        $detail_doc     = Document::where('document_id', $ipc->sst_id)->where('deleted_at', null)->where('document_types', 'DETAIL_IPC')->orderby('id', 'desc')->limit(1)->get();

        $adv_balik       = $getipc->getAdvBalikAmount();
        $show_adv        = $adv_balik["formula"];
        $wjp             = $getipc->getWjpAmount();
        $show_wjp        = $wjp["formula"];
        $wjp_balik       = $getipc->getWjpLepasAmount();
        $show_wjp_tolak  = $wjp_balik["formula"];
        $jumlah          = $getipc->getDemandAmount();
        $demand_show     = $jumlah['formula'];
        $lad             = $getipc->getLadAmount(); 
        $lad_formula     = $lad["formula"];
        $denda           = (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->acquisition) && !empty($ipc->sst->acquisition->approval) && !empty($ipc->sst->acquisition->approval->acquisition_type_id))? $ipc->sst->acquisition->approval->acquisition_type_id:null;
        
        if (! empty($ipc)) {
            $review_previous = Review::where('acquisition_id', $ipc->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('ipc_id', $ipc->id)->where('type', 'IPC')->orderby('id', 'desc')->first();

            $reviewlog          = ReviewLog::where('ipc_id', $ipc->id)->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1             = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S2')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log           = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }


            // 2nd semakan
            $sl2            = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNPP')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log          = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'IPC')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero         = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNPP')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3            = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S4')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log          = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S4')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4            = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S5')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log          = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S5')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5            = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNBPUB')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs        = ReviewLog::where('ipc_id', $ipc->id)->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S9')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s8log      = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'S9')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s8log)) {
                $semakan8log = $s8log;
            }

            // 7th semakan
            $sl7        = ReviewLog::where('ipc_id', $ipc->id)->where('status', 'CNK')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnklog     = ReviewLog::where('ipc_id', $ipc->id)->where('progress', '1')->where('status', 'CNK')->where('type', 'IPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnklog)) {
                $cetaknotisklog = $cnklog;
            }

            $s1             = Review::where('ipc_id', $ipc->id)->where('status', 'S1')->where('type', 'IPC')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2             = Review::where('ipc_id', $ipc->id)->where('status', 'S2')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3             = Review::where('ipc_id', $ipc->id)->where('status', 'S3')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4             = Review::where('ipc_id', $ipc->id)->where('status', 'S4')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5         = Review::where('ipc_id', $ipc->id)->where('status', 'S5')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s8 = Review::where('ipc_id', $ipc->id)->where('status', 'S8')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s8)) {
                $semakan8 = $s8;
            }
            $s9 = Review::where('ipc_id', $ipc->id)->where('status', 'S9')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($s9)) {
                $semakan9 = $s9;
            }
            $cn         = Review::where('ipc_id', $ipc->id)->where('status', 'CN')->where('type', 'IPC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnk       = Review::where('status', 'Selesai')->where('type', 'IPC')->where('ipc_id', $ipc->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'IPC')->where('ipc_id', $ipc->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnk)) {
                $cetaknotisk = $cnk;
            }
            $cnbpub     = Review::where('status', 'S8')->where('type', 'IPC')->where('ipc_id', $ipc->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'IPC')->where('ipc_id', $ipc->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri      = Review::where('ipc_id', $ipc->id)->where('status', null)->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus       = Review::where('ipc_id', $ipc->id)->where('status', 'N')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1      = Review::where('ipc_id', $ipc->id)->where('status', 'N')->where('progress', '0')->where('type', 'IPC')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }
        $ssm = null; 
        if(!empty($ipc) && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 1){
            foreach($ipc->ipcInvoice as $invs){
                if($ssm == null){
                    $ssm = $invs->company->ssm_no; 
                }else{
                    $ssm = $ssm.','.$invs->company->ssm_no;  
                }
            }
        }elseif(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() == 1){
            $ssm = $ipc->ipcInvoice->pluck('company')->pluck('ssm_no')->first();
        }
        
        $data = [
            'ssm'               => $ssm ?? null,
            'prov_id'           => $prov_id ?? null, 
            'review_previous'   => $review_previous ?? null, 
            'review'            => $review ?? null, 
            's1'                => $s1 ?? null, 
            's2'                => $s2 ?? null, 
            's3'                => $s3 ?? null, 
            's4'                => $s4 ?? null, 
            's5'                => $s5 ?? null, 
            's8'                => $s8 ?? null, 
            's9'                => $s9 ?? null, 
            'cnk'              => $cnk ?? null, 
            'cnbpub'            => $cnbpub ?? null, 
            'kuiri'             => $kuiri ?? null, 
            'urus'              => $urus ?? null, 
            'urus1'             => $urus1 ?? null, 
            'semakan1'          => $semakan1 ?? null, 
            'semakan2'          => $semakan2 ?? null, 
            'semakan3'          => $semakan3 ?? null, 
            'semakan4'          => $semakan4 ?? null, 
            'semakan5'          => $semakan5 ?? null, 
            'semakan8'          => $semakan8 ?? null, 
            'semakan9'          => $semakan9 ?? null, 
            'cetaknotis'        => $cetaknotis ?? null, 
            'cetaknotisk'      => $cetaknotisk ?? null, 
            'cetaknotisbpub'    => $cetaknotisbpub ?? null, 
            'quiry'             => $quiry ?? null, 
            'urusetia'          => $urusetia ?? null, 
            'urusetia1'         => $urusetia1 ?? null,
            'sl1'               => $sl1 ?? null,
            'sl2'               => $sl2 ?? null,
            'sl3'               => $sl3 ?? null,
            'sl4'               => $sl4 ?? null,
            'sl5'               => $sl5 ?? null,
            'sl6'               => $sl6 ?? null,
            'sl7'               => $sl7 ?? null, 
            'cnuulog'           => $cnuulog ?? null,
            'semakan2log'       => $semakan2log ?? null,
            'semakan3log'       => $semakan3log ?? null,
            'semakan3zero'      => $semakan3zero ?? null,
            'semakan4log'       => $semakan4log ?? null,
            'semakan5log'       => $semakan5log ?? null,
            'cetaknotisbpubs'   => $cetaknotisbpubs ?? null,
            'semakan8log'       => $semakan8log ?? null,
            'cetaknotisklog'   => $cetaknotisklog ?? null,
            'reviewlog'         => $reviewlog ?? null,
            'ipc_invoice'       => $ipc_invoice ?? null, 
            'ipc_bq'            => $ipc_bq ?? null, 
            'BQElemen'          => $BQElemen ?? null, 
            'BQItem'            => $BQItem ?? null, 
            'BQSub'             => $BQSub ?? null, 
            'appointed'         => $appointed ?? null, 
            'ipc'               => $ipc ?? null, 
            'company'           => $company ?? null,
            'user'              => $user ?? null, 
            'pekeso_doc'        => $pekeso_doc ?? null, 
            'levi_doc'          => $levi_doc ?? null, 
            'cert_doc'          => $cert_doc ?? null, 
            'summary_doc'       => $summary_doc ?? null, 
            'status_doc'        => $status_doc ?? null,
            'detail_doc'        => $detail_doc ?? null,
            'sst'               => $sst ?? null,
            'ipc_vo'            => $ipc_vo ?? null,
            'voElemen'          => $voElemen ?? null,
            'voItem'            => $voItem ?? null,
            'voSub'             => $voSub ?? null,
            'voCancel'          => $voCancel ?? null,
            'show_adv'          => $show_adv,        
            'show_wjp'          => $show_wjp,        
            'show_wjp_tolak'    => $show_wjp_tolak,  
            'demand_show'       => $demand_show,     
            'lad_formula'       => $lad_formula,
            'new_eot_date'      => $new_eot_date ?? null,
            'new_contract'      => $new_contract ?? null,
            'get_inv_main'      => $get_inv_main,
            'invoice_all'       => $invoice_all ?? null,
            'lad_value'         => 0, 
            'denda'             => $denda,
            'total_ipc'         => $total_ipc ?? null,
            'vo_batal'          => $vo_batal ?? null,
            'pre_vo'            => $pre_vo ?? null
        ];
        return view('contract.post.ipc.partials.forms.ipc_show')->with($data);
    }

    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function sap(Request $request)
    {
        $ipc       = Ipc::findByHashSlug($request->hashslug);
        if(!empty($ipc) && $ipc->demand_amount < 0){
            return ['message'=>'Tidak Dibenarkan, Amaun Tuntutan Negatif','err'=>'E'];
        }
        
        
        $temp_push = [];
        $temp_text = '';
        
        if (! empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0) {
            foreach ($ipc->ipcInvoice as $inv) {
                $ba = (! empty($inv->financial->businessArea) && ! empty($inv->financial->businessArea->code))  ? $inv->financial->businessArea->code   : '';
                $vendor_sst_id = '';
                if($ba == 'KWM'){
                    $vendor_sst_id = $inv->company->kwm_vendor_id ?? '' ;
                }elseif($ba == 'KWP'){
                    $vendor_sst_id = $inv->company->kwp_vendor_id ?? '' ;
                }
                if ('' == $vendor_sst_id || empty($vendor_sst_id)) {
                    swal()->warning('Sijil Bayaran Interim', 'Tiada Rekod Vendor ID Pada Invois', []);
                    return ['message'=>'Tiada Rekod Vendor ID Pada Syarikat yang Dipilih, Sila Semak Syarikat dahulu','err'=>'E'];
                }
            }
        }

        $response = sap($ipc)->handle();
        if(!empty($response)){//check respond from sapservice got data
            foreach($response as $key1=>$res){
                foreach($res as $re){
                    $data     = json_decode($re);
                    array_push($temp_push, [
                        'data' => $data,
                    ]);
                    
                }
                foreach($ipc->ipcInvoice as $key2=>$inv_updtd){//trying update data for each invoice per ipc
                
                    if($key1 == $key2){// check counter data respond same as inv counter
                        if (! empty($data) && 'S' == $data->ep_MSG_TYPE) {//update sap document id
                            $inv_updtd->ipc->update([
                                'status'       => 3,
                                'sap_doc_data' => $data->ep_XBLNR,
                            ]);
                            $inv_updtd->update([
                                'status'       => 3,
                                'sap_doc_data' => $data->ep_XBLNR,
                            ]);
                            
                        }else{//return error message from sap
                            return ['message'=>$data->ep_MSG_TEXT,'err'=>$data->ep_MSG_TYPE];
                        }
                    }
                }
            }
        }else{//check respond from sapservice got empty data
            return ['message'=>'Tiada Tindakan Dari SAP Servis','err'=>'E'];
        }
        
        foreach ($temp_push as $temp) {//combine all success document id
            if($temp_text == null){
                $temp_text = $temp['data']->ep_XBLNR . ' , ';
            }else{
                $temp_text = $temp_text . $temp['data']->ep_XBLNR . ' , ';
            }
        }
        
        $ipc_new_updtd  = Ipc::findByHashSlug($request->hashslug);//get updated status
        if(!empty($ipc_new_updtd) && $ipc_new_updtd->status == 3){//if success ipc update review and workflow
            
            $review  = new Review();

            $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

            $review->ipc_id    = $request->input('ipc_id');
            $review->acquisition_id = $request->input('acquisition_id');
            $review->requested_by   = $request->input('requested_by');
            $review->requested_at   = Carbon::now();
            $review->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
            $review->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
            $review->task_by        = $request->input('task_by');
            $review->created_by     = $request->input('created_by');
            $review->type           = 'IPC';
            $review->reviews_status           = 'Teratur';

            $reviewlog  = new ReviewLog();
            $reviewlog1 = new ReviewLog();

            $reviewlog->ipc_id = $request->input('ipc_id');
            $reviewlog->acquisition_id = $request->input('acquisition_id');
            $reviewlog->requested_by   = $request->input('requested_by');
            $reviewlog->requested_at   = Carbon::now();
            $reviewlog->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
            $reviewlog->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
            $reviewlog->task_by        = $request->input('task_by');
            $reviewlog->created_by     = $request->input('created_by');
            $reviewlog->type           = 'IPC';
            $reviewlog->reviews_status           = 'Selesai';

            $review->status      = 'Selesai';
            $review->progress    = 1;
            $review->law_task_by    = $uu->law_task_by;
            $review->approved_by = $request->input('approved_by');
            $review->department  = config('enums.BPUB');

            $reviewlog->status      = 'CNK';
            $reviewlog->progress    = 1;
            $reviewlog->approved_by = $request->input('approved_by');
            $reviewlog->department  = config('enums.BPUB');

            $reviewlog1->ipc_id = $request->input('ipc_id');
            $reviewlog1->acquisition_id = $request->input('acquisition_id');
            $reviewlog1->requested_by   = $request->input('requested_by');
            $reviewlog1->requested_at   = Carbon::now();
            $reviewlog1->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
            $reviewlog1->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
            $reviewlog1->task_by        = $request->input('task_by');
            $reviewlog1->created_by     = $request->input('created_by');
            $reviewlog1->type           = 'IPC';
            $reviewlog1->reviews_status           = 'Selesai Semakan';
            $reviewlog1->status      = 'CNK';
            $reviewlog1->progress    = 1;
            $reviewlog1->approved_by = $request->input('approved_by');
            $reviewlog1->department  = config('enums.BPUB');

            $workflow = WorkflowTask::where('acquisition_id', $ipc->sst->acquisition_id)->where('ipc_id', $request->ipc_id)->first();
            $workflow->flow_desc = 'Semakan Selesai.';
            $workflow->flow_location = '';
            $workflow->user_id = null;
            $workflow->is_claimed = 1;
            // $workflow->user_id = $request->input('approved_by');
            $workflow->workflow_team_id = 1;

            $review->document_contract_type    = null;
            $reviewlog->document_contract_type = null;

            // if (! empty($review->approved_by)) {
            //     $email = new EmailScheduler;
            //     $email->status  = 'NEW';
            //     // real email
            //     $email->to      = $review->approve->email;
            //     // dummy data
            //     // $email->to = "coins@ppj.gov.my";

            //     $email->counter = 1;
            //     $email->subject = 'Tindakan : SAP Berjaya Dihantar';
            //     $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
            //     $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : SAP Berjaya Dihantar </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
            //     $email->save();
            // } else {
            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
            // }

            $review->save();
            $reviewlog->save();
            $workflow->update();
            
            return ['message'=>'Document ID: '.$temp_text,'err'=>'S'];
        }
    }

    public function sap_vendor($ssm, $back_route, $back_id)
    {
        $company  = Company::all();
        $client   = new Client();
        $http     = config('sap.vendor');
        $response = $client->request('GET', $http . '/' . $ssm);
        $data     = json_decode($response->getBody()->getContents());
        
        return view('contract.post.ipc.partials.vendor.company', compact('data', 'company', 'back_route', 'back_id'));
    }

    public function update_vendor(Request $request)
    {
        if (! empty($request->vendor_id)) {
            foreach ($request->vendor_id as $ven) {
                $reslt = explode(',', $ven);
                
                if(substr($reslt[1], 0, 1) == "1"){
                    Company::where('ssm_no', $reslt[0])->update([
                        'kwm_vendor_id' => $reslt[1],
                    ]);
                }
                if(substr($reslt[1], 0, 1) == "2"){
                    Company::where('ssm_no', $reslt[0])->update([
                        'kwp_vendor_id' => $reslt[1],
                    ]);
                }
            }
        }
        $back_route = $request->back_route;
        $back_id    = $request->back_id;

        swal()->success('Syarikat dari SAP', 'Vendor ID Berjaya Disimpan');

        return redirect()->route($back_route, $back_id);
    }
}
