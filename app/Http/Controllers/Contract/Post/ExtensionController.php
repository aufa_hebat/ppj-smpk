<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;
use App\Models\Acquisition\Eot;
use Illuminate\Support\Carbon;

class ExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointed_company = AppointedCompany::withDetails()->get();
        $eot = Eot::where('appointed_company_id', $appointed_company->pluck('id'))->withDetails()->get();

        return view('contract.post.extension.index',compact('appointed_company','eot'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $appointed_company = AppointedCompany::withDetails()->findByHashSlug($request->hashslug);
        $sst               = Sst::where([
                    ['company_id', '=', $appointed_company->company_id],
                    ['acquisition_id', '=', $appointed_company->acquisition_id],
                ])->first();

        if(!empty($appointed_company->eot) && !empty($appointed_company->eot->pluck('eot_approve')) && !empty($appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $bil = count($appointed_company->eot) + 1;
        } elseif(!empty($appointed_company->eot) && !empty($appointed_company->eot->pluck('eot_approve')) && !empty($appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '2')->count() > 0)) {
            $bil = count($appointed_company->eot);
        } else {
            $bil = count($appointed_company->eot) + 1;
        }

        $latest_end_date = '';
        if (1 != $bil) {
            foreach ($appointed_company->eot as $eot) {
                if (! empty($eot->eot_approve) && '1' == $eot->eot_approve->approval) {
                    $latest_end_date = $eot->eot_approve->approved_end_date->format('d/m/Y');
                    // break;
                    // break;
                    // break;
                }
            }
        }
        
        $new_eot_date = null;
        if(!empty($appointed_company->eot) && !empty($appointed_company->eot->pluck('eot_approve')) && !empty($appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }

        if ('' == $latest_end_date) {
            $latest_end_date = $sst->end_working_date->format('d/m/Y');
        }

        return view('contract.post.extension.create', compact('appointed_company', 'sst', 'bil', 'latest_end_date','new_eot_date'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('contract.post.extension.edit_list.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
