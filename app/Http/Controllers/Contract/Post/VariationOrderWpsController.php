<?php

namespace App\Http\Controllers\Contract\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\AppointedCompany;
use App\Models\VO\Active;
use App\Models\VO\Cancel;
use App\Models\VO\Element;
use App\Models\VO\Item;
use App\Models\VO\SubItem;
use App\Models\VO\Type;
use App\Models\VO\VO;

class VariationOrderWpsController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
     }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {

     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param \Illuminate\Http\Request $request
      *
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

     }
 
     /**
      * Display the specified resource.
      *
      * @param int $id
      *
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
 
     }
 
     /**
      * Show the form for editing the specified resource.
      *
      * @param int $id
      *
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
 
     }
 
     /**
      * Update the specified resource in storage.
      *
      * @param \Illuminate\Http\Request $request
      * @param int                      $id
      *
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
 
     }
 
     /**
      * Remove the specified resource from storage.
      *
      * @param int $id
      *
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {

     }

     public function wpsActive($id)
     {
         $sst       = Sst::findByHashSlug($id);
         $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
 
         $cancels = Cancel::where([
            ['sst_id', $sst->id],
            ['status', '1']
        ])->get();

        $wpsElement = Element::where([
            ['sst_id', '=', $sst->id],
            ['wps_status', '=', 1],
        ])->get();

        return view('contract.post.variation-order.wps.forms.create.wps', compact('sst', 'id', 'appointed', 'cancels', 'vo', 'wpsElement'));
    }

    
    public function wpsCreate($id)
    {
        $sst       = Sst::findByHashSlug($id);
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $cancels = Cancel::where([
           ['sst_id', $sst->id],
           ['status', '1']
       ])->get();

       $vo       = VO::create([
           'sst_id'        => $sst->id,
           'user_id'       => user()->id,
           'department_id' => user()->department->id,
           'type'          => 'WPS',
       ]);

       $type      = Type::create([
           'vo_id'                   => $vo->id,
           'variation_order_type_id'   => '3',
       ]);

       return redirect()->route('contract.post.variation-order-wps.wps-edit', $vo->hashslug);
    }

    public function wpsSelect($hashslug, $id)
    {
        $totalUsed = 0;
        $active = Active::findByHashSlug($hashslug);
        $vo = VO::find($id);
     
        $sst = $active->vo->sst;
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $type_id = Type::where([
            ['vo_id', $vo->id],
            ['variation_order_type_id', 3],
            ['deleted_at' ,null]
        ])->first()->hashslug;

        $PKElemen = Element::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $PKItem = Item::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $PKSub = SubItem::where([
            ['vo_id', '=', $vo->id],
            ['vo_active_id', '=', $active->id],
        ])->get();
        $totalUsed = SubItem::where([
            ['vo_active_id', '=', $active->id],
            ['wps_status', '=', 1]
        ])->pluck('net_amount')->sum();

        return view('contract.post.variation-order.wps.forms.update.wps-detail', compact('vo', 'id', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'type_id','active', 'totalUsed', 'appointed'));
    }

    public function wpsEdit($id)
    {
        $vo         = VO::findByHashSlug($id);
        $sst        = $vo->sst;
        $appointed  = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        $cancels = Cancel::where([
           ['sst_id', $sst->id],
           ['status', '1']
       ])->get();

        $wpsElement = Element::where([
            ['sst_id', '=', $sst->id],
            ['wps_status', '=', 1],
        ])->get();

        return view('contract.post.variation-order.wps.forms.update.wps', compact('sst', 'id', 'appointed', 'cancels', 'vo', 'wpsElement'));
   }

   public function wpsShow($id)
   {
       $vo         = VO::findByHashSlug($id);
       $sst        = $vo->sst;
       $appointed  = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

       $cancels = Cancel::where([
          ['sst_id', $sst->id],
          ['status', '1']
      ])->get();

       return view('contract.post.variation-order.wps.forms.show.wps', compact('sst', 'id', 'appointed', 'cancels', 'vo'));
  }

  public function wpsShowSelect($hashslug, $id)
  {
      $totalUsed = 0;
      $active = Active::findByHashSlug($hashslug);
      $vo = VO::find($id);
      // $type = Type::find($id);        
      $sst = $active->vo->sst;
      $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
      // $vo = $type->vo;

      $type_id = Type::where([
          ['vo_id', $vo->id],
          ['variation_order_type_id', 3],
          ['deleted_at' ,null]
      ])->first()->hashslug;

      $PKElemen = Element::where([
          ['vo_id', '=', $vo->id],
          ['vo_active_id', '=', $active->id],
      ])->get();
      $PKItem = Item::where([
          ['vo_id', '=', $vo->id],
          ['vo_active_id', '=', $active->id],
      ])->get();
      $PKSub = SubItem::where([
          ['vo_id', '=', $vo->id],
          ['vo_active_id', '=', $active->id],
      ])->get();
      $totalUsed = SubItem::where([
          ['vo_active_id', '=', $active->id],
          ['wps_status', '=', 1]
      ])->pluck('net_amount')->sum();        

      return view('contract.post.variation-order.wps.forms.show.wps-detail', compact('vo', 'id', 'PKElemen', 'PKItem', 'PKSub', 'sst', 'type_id','active', 'totalUsed', 'appointed'));
  }

}
