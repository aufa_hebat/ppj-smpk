<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Warning;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\WorkflowTask;

class WarningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $hashslug = $request->hashslug;
        $sst      = Sst::findByHashSlug($request->hashslug);
        $warning  = Warning::where('sst_id', $sst->id)->get();

        return view('contract.post.termination.warning.index', compact('hashslug', 'warning', 'sst'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $hashslug = $request->hashslug;
        $cnt      = (counter()->counter_warning ?? 0) + 1;

        $sst     = Sst::findByHashSlug($hashslug);
        $warning = Warning::where('sst_id', $sst->id)->where('termination_type_id', $sst->termination_type_id)->get();

        return view('contract.post.termination.warning.create', compact('hashslug', 'cnt', 'sst', 'warning'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sst  = Sst::findByHashSlug($request->hashslug);
        $warn = new Warning();

        $warn->sst_id               = $request->sst_id            ?? 0;
        $warn->warning_letter_no    = $request->warning_letter_no ?? 0;
        $warn->site_meeting_no      = $request->site_meeting_no   ?? 0;
        $warn->warning_letter_at    = (! empty($request->warning_letter_at)) ? Carbon::createFromFormat('d/m/Y', $request->warning_letter_at) : null;
        $warn->site_meeting_at      = (! empty($request->site_meeting_at)) ? Carbon::createFromFormat('d/m/Y', $request->site_meeting_at) : null;
        $warn->clause               = $request->clause ?? '';
        $warn->sub_clause           = $request->sub_clause ?? '';
        $warn->scheduled_completion = $request->scheduled_completion ?? 0;
        $warn->actual_completion    = $request->actual_completion ?? 0;
        $warn->status               = $request->status;
        $warn->termination_type_id  = $sst->termination_type_id;
        $warn->save();

        if ($request->hasFile('document')) {
            $termination_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $warn->sst->acquisition,
                'doc_type'            => 'WARN',
                'doc_id'              => $warn->id,
            ];
            common()->upload_document($termination_requirements);
        }

        $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
        $workflow->warning_id = $warn->id;
        $workflow->flow_desc = 'Surat Amaran '.$warn->warning_letter_no.' : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'Amaran';
        $workflow->role_id = 4;
        $workflow->url = '/termination/warning/'.$warn->hashslug.'/edit';
        $workflow->update();

        swal()->success('Surat Amaran', 'Rekod telah berjaya disimpan.', []);

        // return redirect()->route('termination.warning.index', ['hashslug' => $request->hashslug]);
        //return redirect()->route('termination.termination.edit', ['hashslug' => $warn->sst->hashslug]);
        return redirect()->route('termination.warning.edit', ['hashslug' => $warn->hashslug]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warning = Warning::findByHashSlug($id);

        if (! empty($warning)) {
            $review_previous = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNBPUB')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }

            $s1              = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S1')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S3')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S6')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $selesai = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($selesai)) {
                $spp = $selesai;
            }
            $kuiri = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'N')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'N')->where('progress', '0')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }

        return view('contract.post.termination.warning.show', compact('warning', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'cnbpub', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotisbpub', 'quiry', 'urusetia', 'urusetia1','selesai','spp','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warning = Warning::findByHashSlug($id);

        if (! empty($warning)) {
            $review_previous = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();

            $reviewlog      = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNBPUB')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }
            
            $s1              = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S1')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S3')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S6')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'N')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $warning->sst->acquisition_id)->where('warning_id', $warning->id)->where('status', 'N')->where('progress', '0')->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }

        return view('contract.post.termination.warning.edit', compact('warning', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = 1;
        
        if($request->btnSubmit == 'Simpan'){
            $status = 1;
        }elseif($request->btnSubmit == 'Selesai'){
            $status = 3;
        }

        $warn                       = Warning::findByHashSlug($id);
        $warn->site_meeting_no      = $request->site_meeting_no ?? 0;
        $warn->warning_letter_at    = (! empty($request->warning_letter_at)) ? Carbon::createFromFormat('d/m/Y', $request->warning_letter_at) : null;
        $warn->site_meeting_at      = (! empty($request->site_meeting_at)) ? Carbon::createFromFormat('d/m/Y', $request->site_meeting_at) : null;
        $warn->clause               = $request->clause ?? '';
        $warn->sub_clause           = $request->sub_clause ?? '';
        $warn->scheduled_completion = $request->scheduled_completion ?? 0;
        $warn->actual_completion    = $request->actual_completion ?? 0;
        $warn->termination_type_id  = $warn->sst->termination_type_id;
        $warn->status               = $status;

        $warn->update();

        if ($request->hasFile('document')) {
            $termination_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $warn->sst->acquisition,
                'doc_type'            => 'WARN',
                'doc_id'              => $warn->id,
            ];
            common()->upload_document($termination_requirements);
        }

        swal()->success('Surat Amaran', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $warn->sst->hashslug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        swal()->success('Surat Amaran', 'Rekod telah berjaya dipadamkan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $warn->sst->hashslug]);
    }
}
