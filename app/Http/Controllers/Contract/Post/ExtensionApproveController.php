<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\EotApprove;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\EmailScheduler;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use Carbon\Carbon;

class ExtensionApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $appointed_company = AppointedCompany::withDetails()->findByHashSlug($request->hashslug);

        return view('contract.post.extension.approve_list.index', compact('appointed_company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            // semakan
            $acq_rev = Review::where('eot_id',$request->eot_id)->orderby('id','desc')->first();
            $review = new Review();
            $reviewlog = new ReviewLog();

            // if($acq_rev->department == 'BPUB'){
            //     ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('eot_id', $request->input('eot_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
            // } else {
            //     ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('eot_id', $request->input('eot_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
            // }

            $reviewlog1 = new ReviewLog();
            $workflow = WorkflowTask::where('acquisition_id', $acq_rev->acquisition_id)->where('eot_id', $request->eot_id)->first();
            
            if (1 == $request->sentStatus) {
                $review->acquisition_id = $acq_rev->acquisition_id;
                $review->eot_id = $request->eot_id;
                $review->created_by = $acq_rev->created_by;
                $review->requested_by = user()->id;
                $review->requested_at = Carbon::now();
                $review->approved_by = $acq_rev->created_by;
                $review->approved_at    = Carbon::now();
                $review->reviews_status = 'Teratur';
                $review->progress = 1;
                $review->status = 'Selesai';
                $review->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $review->department   = config('enums.PP');
                $review->task_by        = $acq_rev->task_by;
                $review->law_task_by        = $acq_rev->law_task_by;
                $review->type           = 'EOT';

                $reviewlog->acquisition_id = $acq_rev->acquisition_id;
                $reviewlog->eot_id = $request->eot_id;
                $reviewlog->created_by = $acq_rev->created_by;
                $reviewlog->requested_by = user()->id;
                $reviewlog->requested_at = Carbon::now();
                $reviewlog->approved_by = $acq_rev->created_by;
                $reviewlog->approved_at    = Carbon::now();
                $reviewlog->reviews_status = 'Selesai';
                $reviewlog->progress = 1;
                $reviewlog->status = 'CNBPUB';
                $reviewlog->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $reviewlog->department   = config('enums.PP');
                $reviewlog->task_by        = $acq_rev->task_by;
                $reviewlog->law_task_by        = $acq_rev->law_task_by;
                $reviewlog->type           = 'EOT';

                $reviewlog1->acquisition_id = $acq_rev->acquisition_id;
                $reviewlog1->eot_id = $request->eot_id;
                $reviewlog1->created_by = $acq_rev->created_by;
                $reviewlog1->requested_by = user()->id;
                $reviewlog1->requested_at = Carbon::now();
                $reviewlog1->approved_by = $acq_rev->created_by;
                $reviewlog1->approved_at    = Carbon::now();
                $reviewlog1->reviews_status = 'Selesai Semakan';
                $reviewlog1->progress = 1;
                $reviewlog1->status = 'CNBPUB';
                $reviewlog1->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $reviewlog1->department   = config('enums.PP');
                $reviewlog1->task_by        = $acq_rev->task_by;
                $reviewlog1->law_task_by        = $acq_rev->law_task_by;
                $reviewlog1->type           = 'EOT';

                $eot = EOT::find($request->eot_id);
                $eot->status = 3;
                $eot->update();
                if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){
                    $workflow->flow_desc = 'Bon Pelanjutan Masa '.$eot->bil.' : Sila teruskan dengan Bon Pelanjutan Masa.';
                    $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit#bon';
                    $workflow->flow_location = 'EOT';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $review->approved_by;
                    $workflow->role_id = 4;
                    $workflow->workflow_team_id = 1;

                    audit($review, __('Semakan EOT '.$eot->bil.' - ' .$acq_rev->acquisition->reference. ' : ' .$acq_rev->acquisition->title. ' Dihantar Kepada ' . $review->approve->name));
                } else {
                    $workflow->flow_desc = 'Bon Pelanjutan Masa : Sila teruskan dengan Bon Pelanjutan Masa.';
                    $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit#bon';
                    $workflow->flow_location = 'EOT';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $review->approved_by;
                    $workflow->role_id = 4;
                    $workflow->workflow_team_id = 1;

                    audit($review, __('Semakan EOT - ' .$acq_rev->acquisition->reference. ' : ' .$acq_rev->acquisition->title. ' Dihantar Kepada ' . $review->approve->name));
                }

                $email = new EmailScheduler();
                $email->status  = 'NEW';
                // real email
                $email->to      = $review->approve->email;
                // dummy data
                // $email->to = "coins@ppj.gov.my";

                $email->counter = 1;
                $email->subject = 'Tindakan : Penyediaan Bon Pelanjutan Masa';
                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Bon Pelanjutan Masa </b></br>No Kontrak : '.$acq_rev->acquisition->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                $email->save();
                $review->save();
                $reviewlog->save();
                $workflow->update();

                if($request->ajax()){
                    swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);
                    return response()->json(['Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.']);
                }
            } elseif (1 == $request->saveStatus){
                $review->acquisition_id = $acq_rev->acquisition_id;
                $review->eot_id = $request->eot_id;
                $review->created_by = $acq_rev->created_by;
                $review->requested_by = user()->id;
                $review->requested_at = Carbon::now();
                $review->approved_by = user()->id;
                $review->approved_at    = Carbon::now();
                $review->reviews_status = 'Deraf';
                $review->progress = 1;
                $review->status = 'N';
                $review->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $review->department   = config('enums.BPUB');
                $review->task_by        = $acq_rev->task_by;
                $review->law_task_by        = $acq_rev->law_task_by;
                $review->type           = 'EOT';

                $reviewlog->acquisition_id = $acq_rev->acquisition_id;
                $reviewlog->eot_id = $request->eot_id;
                $reviewlog->created_by = $acq_rev->created_by;
                $reviewlog->requested_by = user()->id;
                $reviewlog->requested_at = Carbon::now();
                $reviewlog->approved_by = user()->id;
                $reviewlog->approved_at    = Carbon::now();
                $reviewlog->reviews_status = 'Deraf';
                $reviewlog->progress = 1;
                $reviewlog->status = 'N';
                $reviewlog->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $reviewlog->department   = config('enums.BPUB');
                $reviewlog->task_by        = $acq_rev->task_by;
                $reviewlog->law_task_by        = $acq_rev->law_task_by;
                $reviewlog->type           = 'EOT';
                
                $eot = EOT::find($request->eot_id);
                if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){
                    $workflow->flow_desc = 'Semakan EOT '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#eot-review';
                    $workflow->flow_location = 'EOT';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $review->approved_by;
                    $workflow->workflow_team_id = 1;

                    audit($review, __('Semakan EOT '.$eot->bil.' - ' .$acq_rev->acquisition->reference. ' : ' .$acq_rev->acquisition->title. ' Dihantar Kepada ' . $review->approve->name));
                } else {
                    $workflow->flow_desc = 'Semakan EOT : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#eot-review';
                    $workflow->flow_location = 'EOT';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $review->approved_by;
                    $workflow->workflow_team_id = 1;

                    audit($review, __('Semakan EOT - ' .$acq_rev->acquisition->reference. ' : ' .$acq_rev->acquisition->title. ' Dihantar Kepada ' . $review->approve->name));
                }

                // $email = new EmailScheduler();
                // $email->status  = 'NEW';
                // // real email
                // $email->to      = $review->approve->email;
                // // dummy data
                // $email->to = "coins@ppj.gov.my";

                // $email->counter = 1;
                // $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : DeRAF';
                // $email->text    = 'Ulasan Semakan : Serah Tugasan Kepada ' . $review->lawtask->name . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;

                // $email->save();

                $review->save();
                $reviewlog->save();
                $workflow->update();

                if($request->ajax()){
                    swal()->success('Deraf', 'Rekod telah berjaya disimpan', []);
                    return response()->json(['Deraf', 'Rekod telah berjaya disimpan']);
                }
            } elseif (1 == $request->rejectStatus) {
                $review->acquisition_id = $acq_rev->acquisition_id;
                $review->eot_id = $request->eot_id;
                $review->created_by = $acq_rev->created_by;
                $review->requested_by = user()->id;
                $review->requested_at = Carbon::now();
                $review->approved_by = $acq_rev->created_by;
                $review->approved_at    = Carbon::now();
                $review->reviews_status = 'Kuiri';
                $review->progress = 0;
                $review->status = null;
                $review->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $review->department   = config('enums.PP');
                $review->task_by        = $acq_rev->task_by;
                $review->law_task_by        = $acq_rev->law_task_by;
                $review->type           = 'EOT';

                $reviewlog->acquisition_id = $acq_rev->acquisition_id;
                $reviewlog->eot_id = $request->eot_id;
                $reviewlog->created_by = $acq_rev->created_by;
                $reviewlog->requested_by = user()->id;
                $reviewlog->requested_at = Carbon::now();
                $reviewlog->approved_by = $acq_rev->created_by;
                $reviewlog->approved_at    = Carbon::now();
                $reviewlog->reviews_status = 'Kuiri';
                $reviewlog->progress = 0;
                $reviewlog->status = 'CNBPUB';
                $reviewlog->remarks = str_replace("'","`",$request->input('remarks')) ?? '';
                $reviewlog->department   = config('enums.PP');
                $reviewlog->task_by        = $acq_rev->task_by;
                $reviewlog->law_task_by        = $acq_rev->law_task_by;
                $reviewlog->type           = 'EOT';

                $reviewlog1->acquisition_id = $acq_rev->acquisition_id;
                $reviewlog1->eot_id = $request->eot_id;
                $reviewlog1->created_by = $acq_rev->created_by;
                $reviewlog1->requested_by = $acq_rev->created_by;
                $reviewlog1->requested_at = Carbon::now();
                $reviewlog1->approved_by = $acq_rev->created_by;
                $reviewlog1->approved_at    = Carbon::now();
                $reviewlog1->reviews_status = 'Terima';
                $reviewlog1->progress = 0;
                $reviewlog1->status = 'CNBPUB';
                $reviewlog1->department   = config('enums.PP');
                $reviewlog1->task_by        = $acq_rev->task_by;
                $reviewlog1->law_task_by        = $acq_rev->law_task_by;
                $reviewlog1->type           = 'EOT';
                
                $eot = EOT::find($request->eot_id);
                $eot->status = null;
                $eot->update();
                if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){
                    $workflow->flow_desc = 'Semakan EOT '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->url = '/extension/extension_s/'.$eot->hashslug.'/edit#eot-review';
                    $workflow->flow_location = 'EOT';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $review->approved_by;
                    $workflow->role_id = 4;
                    $workflow->workflow_team_id = 1;

                    $email = new EmailScheduler();
                    $email->status  = 'NEW';
                    // real email
                    $email->to      = $review->approve->email;
                    // dummy data
                    // $email->to = "coins@ppj.gov.my";

                    $email->counter = 1;
                    $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT '.$eot->bil.' (Kuiri) </b></br>No Kontrak : '.$acq_rev->acquisition->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                    $email->save();

                    audit($review, __('Semakan EOT '.$eot->bil.' - ' .$acq_rev->acquisition->reference. ' : ' .$acq_rev->acquisition->title. ' Dihantar Kepada ' . $review->approve->name));
                } else {
                    $workflow->flow_desc = 'Semakan EOT : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit#extension';
                    $workflow->flow_location = 'EOT';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $review->approved_by;
                    $workflow->role_id = 4;
                    $workflow->workflow_team_id = 1;

                    $email = new EmailScheduler();
                    $email->status  = 'NEW';
                    // real email
                    $email->to      = $review->approve->email;
                    // dummy data
                    // $email->to = "coins@ppj.gov.my";

                    $email->counter = 1;
                    $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT (Kuiri) </b></br>No Kontrak : '.$acq_rev->acquisition->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                    $email->save();

                    audit($review, __('Semakan EOT - ' .$acq_rev->acquisition->reference. ' : ' .$acq_rev->acquisition->title. ' Dihantar Kepada ' . $review->approve->name));
                }

                $review->save();
                $reviewlog->save();
                $reviewlog1->save();
                $workflow->update();

                if($request->ajax()){
                    swal()->success('Kuiri', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);
                    return response()->json(['Kuiri', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.']);
                }
            }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eot               = Eot::findByHashSlug($id);
        $appointed_company = $eot->appointed_company;
        
        $sst               = Sst::where([
                    ['company_id', '=', $appointed_company->company_id],
                    ['acquisition_id', '=', $appointed_company->acquisition_id],
                ])->first();

        $review = Review::where('eot_id', $eot->id)->where('type', 'EOT')->orderby('id', 'desc')->first();

        $eot_approve = EotApprove::where('approval', '=', '1')
            ->whereHas('eot', function ($query) use ($appointed_company) {
                $query->where('appointed_company_id', $appointed_company->id);
            })->latest();
        
        $new_eot_date = null;
        if(!empty($appointed_company->eot) && !empty($appointed_company->eot->pluck('eot_approve')) && !empty($appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }

        if (! empty($appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->last())) {
            $latest_end_date = \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed_company->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        } else {
            $latest_end_date = \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $sst->end_working_date)->format('d/m/Y');
        }

        $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        $review          = Review::where('eot_id', $eot->id)->where('type', 'EOT')->orderby('id', 'desc')->first();

        $reviewlog      = ReviewLog::where('eot_id', $eot->id)->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

        // 1st semakan
        $sl1 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S2')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s2log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
        if (! empty($s2log)) {
            $semakan2log = $s2log;
        }

        // 2nd semakan
        $sl2 = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNPP')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s3log = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
        if (! empty($s3log)) {
            $semakan3log = $s3log;
        }
        $s3zero = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNPP')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s3zero)) {
            $semakan3zero = $s3zero;
        }

        // 3rd semakan
        $sl3 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S4')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s4log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S4')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s4log)) {
            $semakan4log = $s4log;
        }

        // 4th semakan
        $sl4 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S5')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s5log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S5')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s5log)) {
            $semakan5log = $s5log;
        }

        // 5th semakan
        $sl5 = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNBPUB')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('department','BPUB')->orderby('id', 'desc')->get();
        $cnbpubs = ReviewLog::where('eot_id', $eot->id)->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->where('department','BPUB')->orderby('id', 'desc')->first();
        if (! empty($cnbpubs)) {
            $cetaknotisbpubs = $cnbpubs;
        }

        // nth semakan
        $sln = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNBPUB')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('department','PELAKSANA')->orderby('id', 'desc')->get();
        $cnns = ReviewLog::where('eot_id', $eot->id)->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->where('department','PELAKSANA')->orderby('id', 'desc')->first();
        if (! empty($cnns)) {
            $cetaknotisns = $cnns;
        }

        // 6th semakan
        $sl6 = ReviewLog::where('eot_id', $eot->id)->where('status', 'S7')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s7log = ReviewLog::where('eot_id', $eot->id)->where('status', 'S7')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s7log)) {
            $semakan7log = $s7log;
        }

        // 7th semakan
        $sl7 = ReviewLog::where('eot_id', $eot->id)->where('status', 'CNUU')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnuulog = ReviewLog::where('eot_id', $eot->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'EOT')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($cnuulog)) {
            $cetaknotisuulog = $cnuulog;
        }
        
        $s1              = Review::where('eot_id', $eot->id)->where('status', 'S1')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s1)) {
            $semakan1 = $s1;
        }
        $s2 = Review::where('eot_id', $eot->id)->where('status', 'S2')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s2)) {
            $semakan2 = $s2;
        }
        $s3 = Review::where('eot_id', $eot->id)->where('status', 'S3')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s3)) {
            $semakan3 = $s3;
        }
        $s4 = Review::where('eot_id', $eot->id)->where('status', 'S4')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s4)) {
            $semakan4 = $s4;
        }
        $s5 = Review::where('eot_id', $eot->id)->where('status', 'S5')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s5)) {
            $semakan5 = $s5;
        }
        $s6 = Review::where('eot_id', $eot->id)->where('status', 'S6')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s6)) {
            $semakan6 = $s6;
        }
        $s7 = Review::where('eot_id', $eot->id)->where('status', 'S7')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($s7)) {
            $semakan7 = $s7;
        }
        $cn = Review::where('eot_id', $eot->id)->where('status', 'CN')->where('type', 'EOT')->orderby('id', 'desc')->where('progress', '1')->first();
        if (! empty($cn)) {
            $cetaknotis = $cn;
        }
        $cnuu = Review::where('eot_id', $eot->id)->where('status', 'CN')->where('type', 'EOT')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
        if (! empty($cnuu)) {
            $cetaknotisuu = $cnuu;
        }
        $cnbpub = Review::where('status', 'Selesai')->where('eot_id', $eot->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'EOT')->orWhere('status', null)->where('eot_id', $eot->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'EOT')->orWhere('status', 'N')->where('eot_id', $eot->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'EOT')->first();
        if (! empty($cnbpub)) {
            $cetaknotisbpub = $cnbpub;
        }
        $kuiri = Review::where('eot_id', $eot->id)->where('status', null)->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($kuiri)) {
            $quiry = $kuiri;
        }
        $urus = Review::where('eot_id', $eot->id)->where('status', 'N')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($urus)) {
            $urusetia = $urus;
        }
        $urus1 = Review::where('eot_id', $eot->id)->where('status', 'N')->where('progress', '0')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($urus1)) {
            $urusetia1 = $urus1;
        }

        $noti              = Review::where('eot_id', $eot->id)->where('status', 'N')->where('type', 'EOT')->orderby('id', 'desc')->first();
        if (! empty($noti)) {
            $semakan1 = $noti;
        }

        return view('contract.post.extension.approve_list.edit', compact('eot', 'appointed_company', 'sst', 'latest_end_date', 'review','new_eot_date', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu','cnbpub', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','cetaknotisbpub','sl1','sl2','sl3','sl4','sl5', 'sln','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','cetaknotisns','semakan7log','cetaknotisuulog','reviewlog','noti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
