<?php

namespace App\Http\Controllers\Contract\Post;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Termination;
use App\Models\Acquisition\Warning;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\WorkflowTask;

class TerminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.post.termination.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst     = Sst::findByHashSlug($id);
        $warning = Warning::where('sst_id', $sst->id)->where('termination_type_id', $sst->termination_type_id)->get();

        $appointed       = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $reasonNotice    = Termination::where('sst_id', $sst->id)->where('notice_type', 1)->first();
        $terminateNotice = Termination::where('sst_id', $sst->id)->where('notice_type', 2)->first();
        $othersTerminateNotice = Termination::where('sst_id', $sst->id)->where('notice_type', 3)->first();

        // $company   = Company::where('id', '!=', $sst->company_id)->get();

        if (! empty($reasonNotice)) {
            $review_previous_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review_reason          = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();

            $reviewlog_reason      = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log_reason)) {
                $semakan2log_reason = $s2log_reason;
            }

            // 2nd semakan
            $sl2_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log_reason)) {
                $semakan3log_reason = $s3log_reason;
            }
            $s3zero_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero_reason)) {
                $semakan3zero_reason = $s3zero_reason;
            }

            // 3rd semakan
            $sl3_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log_reason)) {
                $semakan4log_reason = $s4log_reason;
            }

            // 4th semakan
            $sl4_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log_reason)) {
                $semakan5log_reason = $s5log_reason;
            }

            // 5th semakan
            $sl5_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs_reason)) {
                $cetaknotisbpubs_reason = $cnbpubs_reason;
            }

            // 6th semakan
            $sl6_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log_reason)) {
                $semakan7log_reason = $s7log_reason;
            }

            // 7th semakan
            $sl7_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog_reason = ReviewLog::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog_reason)) {
                $cetaknotisuulog_reason = $cnuulog_reason;
            }

            $s1_reason              = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S1')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();

            if (! empty($s1_reason)) {
                $semakan1_reason = $s1_reason;
            }
            $s2_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($s2_reason)) {
                $semakan2_reason = $s2_reason;
            }
            $s3_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S3')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($s3_reason)) {
                $semakan3_reason = $s3_reason;
            }
            $s4_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($s4_reason)) {
                $semakan4_reason = $s4_reason;
            }
            $s5_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($s5_reason)) {
                $semakan5_reason = $s5_reason;
            }
            $s6_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S6')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($s6_reason)) {
                $semakan6_reason = $s6_reason;
            }
            $s7_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($s7_reason)) {
                $semakan7_reason = $s7_reason;
            }
            $cn_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn_reason)) {
                $cetaknotis_reason = $cn_reason;
            }
            $cnuu_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu_reason)) {
                $cetaknotisuu_reason = $cnuu_reason;
            }
            $cnbpub_reason = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub_reason)) {
                $cetaknotisbpub_reason = $cnbpub_reason;
            }
            $selesai_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($selesai_reason)) {
                $spp_reason = $selesai_reason;
            }
            $kuiri_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($kuiri_reason)) {
                $quiry_reason = $kuiri_reason;
            }
            $urus_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'N')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($urus_reason)) {
                $urusetia_reason = $urus_reason;
            }
            $urus1_reason = Review::where('acquisition_id', $reasonNotice->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Penamatan')->where('document_contract_type', 'Tujuan')->orderby('id', 'desc')->first();
            if (! empty($urus1_reason)) {
                $urusetia1_reason = $urus1_reason;
            }
        }

        if (! empty($terminateNotice)) {
            $review_previous_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review_notice          = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();

            $reviewlog_notice      = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s2log_notice)) {
                $semakan2log_notice = $s2log_notice;
            }

            // 2nd semakan
            $sl2_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->orderby('id', 'desc')->first();
            if (! empty($s3log_notice)) {
                $semakan3log_notice = $s3log_notice;
            }
            $s3zero_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero_notice)) {
                $semakan3zero_notice = $s3zero_notice;
            }

            // 3rd semakan
            $sl3_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log_notice)) {
                $semakan4log_notice = $s4log_notice;
            }

            // 4th semakan
            $sl4_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log_notice)) {
                $semakan5log_notice = $s5log_notice;
            }

            // 5th semakan
            $sl5_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs_notice)) {
                $cetaknotisbpubs_notice = $cnbpubs_notice;
            }

            // 6th semakan
            $sl6_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log_notice)) {
                $semakan7log_notice = $s7log_notice;
            }

            // 7th semakan
            $sl7_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog_notice = ReviewLog::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog_notice)) {
                $cetaknotisuulog_notice = $cnuulog_notice;
            }

            $s1_notice              = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S1')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();

            if (! empty($s1_notice)) {
                $semakan1_notice = $s1_notice;
            }
            $s2_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S2')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($s2_notice)) {
                $semakan2_notice = $s2_notice;
            }
            $s3_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S3')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($s3_notice)) {
                $semakan3_notice = $s3_notice;
            }
            $s4_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S4')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($s4_notice)) {
                $semakan4_notice = $s4_notice;
            }
            $s5_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S5')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($s5_notice)) {
                $semakan5_notice = $s5_notice;
            }
            $s6_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S6')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($s6_notice)) {
                $semakan6_notice = $s6_notice;
            }
            $s7_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'S7')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($s7_notice)) {
                $semakan7_notice = $s7_notice;
            }
            $cn_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn_notice)) {
                $cetaknotis_notice = $cn_notice;
            }
            $cnuu_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'CN')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu_notice)) {
                $cetaknotisuu_notice = $cnuu_notice;
            }
            $cnbpub_notice = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub_notice)) {
                $cetaknotisbpub_notice = $cnbpub_notice;
            }
            $selesai_notice = Review::where('status', 'Selesai')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($selesai_notice)) {
                $spp_notice = $selesai_notice;
            }
            $kuiri_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', null)->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($kuiri_notice)) {
                $quiry_notice = $kuiri_notice;
            }
            $urus_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'N')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($urus_notice)) {
                $urusetia_notice = $urus_notice;
            }
            $urus1_notice = Review::where('acquisition_id', $terminateNotice->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Penamatan')->where('document_contract_type', 'Notis')->orderby('id', 'desc')->first();
            if (! empty($urus1_notice)) {
                $urusetia1_notice = $urus1_notice;
            }
        }

        return view('contract.post.termination.edit', compact('sst', 'warning', 'appointed', 'reasonNotice', 'terminateNotice', 'othersTerminateNotice', 'review_previous_reason', 'review_reason', 's1_reason', 's2_reason', 's3_reason', 's4_reason', 's5_reason', 's6_reason', 's7_reason', 'cnuu_reason', 'kuiri_reason', 'urus_reason', 'urus1_reason', 'semakan1_reason', 'semakan2_reason', 'semakan3_reason', 'semakan4_reason', 'semakan5_reason', 'semakan6_reason', 'semakan7_reason', 'cetaknotis_reason', 'cetaknotisuu_reason', 'cetaknotis_reason', 'quiry_reason', 'urusetia_reason', 'urusetia1_reason', 'review_previous_notice', 'review_notice', 's1_notice', 's2_notice', 's3_notice', 's4_notice', 's5_notice', 's6_notice', 's7_notice', 'cnuu_notice', 'kuiri_notice', 'urus_notice', 'urus1_notice', 'semakan1_notice', 'semakan2_notice', 'semakan3_notice', 'semakan4_notice', 'semakan5_notice', 'semakan6_notice', 'semakan7_notice', 'cetaknotis_notice', 'cetaknotisuu_notice', 'cetaknotis_notice', 'quiry_notice', 'urusetia_notice', 'urusetia1_notice','selesai_notice', 'selesai_reason','spp_notice','spp_reason','sl1_reason','sl2_reason','sl3_reason','sl4_reason','sl5_reason','sl6_reason','sl7_reason', 'cnuulog_reason','semakan2log_reason','semakan3log_reason','semakan3zero_reason','semakan4log_reason','semakan5log_reason','cetaknotisbpubs_reason','semakan7log_reason','cetaknotisuulog_reason','reviewlog_reason','sl1_notice','sl2_notice','sl3_notice','sl4_notice','sl5_notice','sl6_notice','sl7_notice', 'cnuulog_notice','semakan2log_notice','semakan3log_notice','semakan3zero_notice','semakan4log_notice','semakan5log_notice','cetaknotisbpubs_notice','semakan7log_notice','cetaknotisuulog_notice','reviewlog_notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst                      = Sst::findByHashSlug($id);
        $sst->termination_type_id = $request->termination_type_id;
        $sst->type_suspension     = $request->type_suspension;
        $sst->update();

        $appointed                      = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $appointed->termination_type_id = $request->termination_type_id;
        $appointed->update();

        if($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 2){
            if(1 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Surat Amaran : Sila teruskan dengan surat amaran.';
                $workflow->flow_location = 'Amaran';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#warning-letter';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();

            } elseif(2 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();
            } elseif(3 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();
            } elseif(4 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();
            }
        } else {
            if(1 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();

            } elseif(2 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();
            } elseif(3 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();
            } elseif(4 == $request->termination_type_id){
                $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
                $workflow->flow_desc = 'Tujuan Penamatan : Sila teruskan dengan tujuan penamatan.';
                $workflow->flow_location = 'Tujuan';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
                $workflow->update();

                $sst->acquisition->status_task = config('enums.flow15');
                $sst->acquisition->update();
            }

        }
            

        swal()->success('Penamatan Kontrak', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $sst->hashslug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function reasonTerminate(Request $request)
    {
        $sst = Sst::findByHashSlug($request->sst_hashslug);
        $status = 1;

        if($request->btnSubmit == 'Simpan'){
            $status = 1;
        }elseif($request->btnSubmit == 'Selesai'){
            $status = 3;
        }

        $termination = Termination::updateOrCreate([
            'sst_id'      => $sst->id,
            'notice_type' => 1,
        ], [
            'sst_id'               => $sst->id,
            'notice_type'          => 1,
            'notice_at'            => (! empty($request->notice_at)) ? Carbon::createFromFormat('d/m/Y', $request->notice_at) : null,
            'clause'               => $request->clause,
            'sub_clause'           => $request->sub_clause,
            'scheduled_completion' => (! empty($request->scheduled_completion)) ? $request->scheduled_completion : 0,
            'actual_completion'    => (! empty($request->actual_completion)) ? $request->actual_completion : 0,
            'status'               => $status,
        ]);

        if ($request->hasFile('document')) {
            $termination_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $termination->sst->acquisition,
                'doc_type'            => 'TERMINATE',
                'doc_id'              => $termination->id,
            ];
            common()->upload_document($termination_requirements);
        }

        $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
        $workflow->flow_desc = 'Tujuan Penamatan : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'Tujuan';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;
        $workflow->is_claimed = 1;
        $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#reason';
        $workflow->update();

        $sst->acquisition->status_task = config('enums.flow15');
        $sst->acquisition->update();

        swal()->success('Notis Tujuan Penamatan', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $sst->hashslug]);
    }

    public function noticeTerminate(Request $request)
    {
        $sst = Sst::findByHashSlug($request->sst_hashslug);
        $status = 1;

        if($request->btnSubmit == 'Simpan'){
            $status = 1;
        }elseif($request->btnSubmit == 'Selesai'){
            $status = 3;
        }

        $termination = Termination::updateOrCreate([
            'sst_id'      => $sst->id,
            'notice_type' => 2,
        ], [
            'sst_id'               => $sst->id,
            'notice_type'          => 2,
            'notice_at'            => (! empty($request->terminate_notice_at)) ? Carbon::createFromFormat('d/m/Y', $request->terminate_notice_at) : null,
            'clause'               => $request->terminate_clause,
            'sub_clause'           => $request->terminate_sub_clause,
            'scheduled_completion' => (! empty($request->terminate_scheduled_completion)) ? $request->terminate_scheduled_completion : 0,
            'actual_completion'    => (! empty($request->terminate_actual_completion)) ? $request->terminate_actual_completion : 0,
            'termination_at'       => (! empty($request->termination_at)) ? Carbon::createFromFormat('d/m/Y', $request->termination_at) : null,
            'status'               => $status,
        ]);

        if(($sst->termination_type_id == 2 || $sst->termination_type_id == 3 || $sst->termination_type_id == 4) && ($status == 3)){
            $sst->termination_at = (! empty($request->termination_at)) ? Carbon::createFromFormat('d/m/Y', $request->termination_at) : null;
            $sst->update();
    
            $appointed                 = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
            $appointed->termination_at = (! empty($request->termination_at)) ? Carbon::createFromFormat('d/m/Y', $request->termination_at) : null;
            $appointed->update();
        }

        if ($request->hasFile('document')) {
            $termination_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $termination->sst->acquisition,
                'doc_type'            => 'TERMINATE',
                'doc_id'              => $termination->id,
            ];
            common()->upload_document($termination_requirements);
        }

        $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
        $workflow->flow_desc = 'Notis Penamatan : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'Notis';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;
        $workflow->is_claimed = 1;
        $workflow->url = '/termination/termination/'.$sst->hashslug.'/edit#notice';
        $workflow->update();

        $sst->acquisition->status_task = config('enums.flow15');
        $sst->acquisition->update();

        swal()->success('Notis Penamatan', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $sst->hashslug]);
    }

    public function othersTerminate(Request $request)
    {        
        $sst = Sst::findByHashSlug($request->sst_hashslug);
        $status = 1;
        
        if($request->btnSubmit == 'Simpan'){
            $status = 1;
        }elseif($request->btnSubmit == 'Selesai'){
            $status = 3;
        }

        $termination = Termination::updateOrCreate([
            'sst_id'      => $sst->id,
            'notice_type' => 3,
        ], [
            'sst_id'               => $sst->id,
            'notice_type'          => 3,
            'description'          => $request->description,
            'termination_at'       => (! empty($request->others_termination_at)) ? Carbon::createFromFormat('d/m/Y', $request->others_termination_at) : null,
            'status'               => $status,
        ]);
        

        if($status == 3){
            $sst->termination_at = (! empty($request->others_termination_at)) ? Carbon::createFromFormat('d/m/Y', $request->others_termination_at) : null;
            $sst->update();
    
            $appointed                 = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
            $appointed->termination_at = (! empty($request->termination_at)) ? Carbon::createFromFormat('d/m/Y', $request->termination_at) : null;
            $appointed->update();
        }


        if ($request->hasFile('document')) {
            $termination_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $termination->sst->acquisition,
                'doc_type'            => 'TERMINATE',
                'doc_id'              => $termination->id,
            ];
            common()->upload_document($termination_requirements);
        }

        swal()->success('Notis Penamatan', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $sst->hashslug]);
    }

    public function uploadReason(Request $request, $id)
    {
        $reason = Termination::findByHashSlug($id);

        if ($request->hasFile('document')) {
            $reason_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $reason->sst->acquisition,
                'doc_type'            => 'TERMINATE',
                'doc_id'              => $reason->id,
            ];
            common()->upload_document($reason_requirements);
        }

        swal()->success('Notis Tujuan Penamatan', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $reason->sst->hashslug]);
    }

    public function uploadTermination(Request $request, $id)
    {
        $termination = Termination::findByHashSlug($id);

        if ($request->hasFile('document')) {
            $termination_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $termination->sst->acquisition,
                'doc_type'            => 'TERMINATE',
                'doc_id'              => $termination->id,
            ];
            common()->upload_document($termination_requirements);
        }

        swal()->success('Notis Penamatan', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('termination.termination.edit', ['hashslug' => $termination->sst->hashslug]);
    }
}
