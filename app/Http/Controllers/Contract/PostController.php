<?php

namespace App\Http\Controllers\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Bon;
use App\Models\Acquisition\Cmgd;
use App\Models\Acquisition\Cnc;
use App\Models\Acquisition\Cpc;
use App\Models\Acquisition\Cpo;
use App\Models\Acquisition\Deposit;
use App\Models\Acquisition\Document as AcqDocument;
use App\Models\Acquisition\Insurance;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\SubContract;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Department;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sst = [];
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){   
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                $sst = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                    ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->whereHas('acquisition', function ($query) {
                        $query->where('deleted_at',null);
                    })
                    ->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                $sst = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                    ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->whereHas('acquisition', function ($query) {
                        $query->where('department_id', user()->department_id)
                        ->where('deleted_at',null);
                    })
                    ->get();
            }else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                $sst = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                    ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->whereHas('acquisition', function ($query) {
                        $query->where('department_id', user()->department_id)
                        ->where('executor_department_id','=',user()->executor_department_id)
                        ->where('deleted_at',null);
                    })
                    ->get();
            }elseif(user()->current_role_login == 'penyedia') {   
                //user biasa , section
                $sst = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                    ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->whereHas('acquisition', function ($query) {
                        $query->where([
                            ['user_id', user()->id],
                            ['cancelled_at',null],
                            ['deleted_at',null],
                        ])->whereHas('history_semakan_sst', function ($que) {
                            $que->where('status','Selesai')->latest();
                        });
                    })
                    ->get();
            }else {   
                //user biasa , section
                $sst = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                    ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->whereHas('acquisition', function ($query) {
                        $query->where([
                            ['cancelled_at',null],['deleted_at',null]
                        ])->whereHas('history_semakan_sst', function ($que) {
                            $que->where('status','Selesai')->latest();
                        })
                        ->where(function($q) {
                            $q->where([
                                ['department_id',user()->department_id],
                                ['executor_department_id',user()->executor_department_id],
                                ['section_id',user()->section_id],
                            ])->orWhereHas('history_semakan_kontrak', function ($que) {
                                $que->where('requested_by',user()->id)->latest();
                            });
                        });
                    })
                    ->get();
            }
        }
        return view('contract.post.index',compact('sst'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companies = companies();

        $sst       = Sst::where('hashslug', $id)->with(['acquisition.bqElements','acquisition.bqElements.bqItems','acquisition.bqElements.bqItems.bqSubItems'])->first();
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();

        $bon = Bon::where('sst_id', $sst->id)->first();

        $insurance = Insurance::where('sst_id', $sst->id)->first();

        $BQElemen = Element::where('acquisition_id', $sst->acquisition->id)->where('deleted_at',null)->orderBy('no','asc')->get();
        $BQItem   = Item::where('acquisition_id', $sst->acquisition->id)->where('deleted_at',null)->orderBy('bq_no_element','asc')->orderBy('no','asc')->get();
        $BQSub    = SubItem::where('acquisition_id', $sst->acquisition->id)->where('deleted_at',null)->orderBy('bq_no_element','asc')->orderBy('bq_no_item','asc')->orderBy('no','asc')->get();

        $deposit  = Deposit::where('sst_id', $sst->id)->first();
        $document = AcqDocument::where('sst_id', $sst->id)->first();

        $cpc  = Cpc::where('sst_id', $sst->id)->first();
        $cnc  = Cnc::where('sst_id', $sst->id)->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();
        $cpo  = Cpo::where('sst_id', $sst->id)->first();

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();

        $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        $review          = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();

        $reviewlog      = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

        // 1st semakan
        $sl1 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s2log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s2log)) {
            $semakan2log = $s2log;
        }

        // 2nd semakan
        $sl2 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s3log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s3log)) {
            $semakan3log = $s3log;
        }
        $s3zero = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s3zero)) {
            $semakan3zero = $s3zero;
        }

        // 3rd semakan
        $sl3 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s4log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s4log)) {
            $semakan4log = $s4log;
        }

        // 4th semakan
        $sl4 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s5log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s5log)) {
            $semakan5log = $s5log;
        }

        // 5th semakan
        $sl5 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnbpubs = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
        if (! empty($cnbpubs)) {
            $cetaknotisbpubs = $cnbpubs;
        }

        // 6th semakan
        $sl6 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s7log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s7log)) {
            $semakan7log = $s7log;
        }

        // 7th semakan
        $sl7 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnuulog = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($cnuulog)) {
            $cetaknotisuulog = $cnuulog;
        }

        $s1              = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s1)) {
            $semakan1 = $s1;
        }
        $s2 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s2)) {
            $semakan2 = $s2;
        }
        $s3 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s3)) {
            $semakan3 = $s3;
        }
        $s4 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s4)) {
            $semakan4 = $s4;
        }
        $s5 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s5)) {
            $semakan5 = $s5;
        }
        $s6 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s6)) {
            $semakan6 = $s6;
        }
        $s7 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s7)) {
            $semakan7 = $s7;
        }
        $cn = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->where('progress', '1')->first();
        if (! empty($cn)) {
            $cetaknotis = $cn;
        }
        $cnuu = Review::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->where('status', 'Selesai')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->first();
        if (! empty($cnuu)) {
            $cetaknotisuu = $cnuu;
        }
        $cnbpub = Review::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->where('status', 'Selesai')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->first();
        if (! empty($cnbpub)) {
            $cetaknotisbpub = $cnbpub;
        }
        $kuiri = Review::where('acquisition_id', $sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($kuiri)) {
            $quiry = $kuiri;
        }
        $urus = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($urus)) {
            $urusetia = $urus;
        }
        $urus1 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($urus1)) {
            $urusetia1 = $urus1;
        }

        // bon
        if (! empty($bon)) {
            $review_previous_bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewbon           = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();

            $reviewlogbon      = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            // 1st semakan
            $sl1bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logbon)) {
                $semakan2logbon = $s2logbon;
            }

            // 2nd semakan
            $sl2bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logbon)) {
                $semakan3logbon = $s3logbon;
            }
            $s3zerobon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerobon)) {
                $semakan3zerobon = $s3zerobon;
            }

            // 3rd semakan
            $sl3bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logbon)) {
                $semakan4logbon = $s4logbon;
            }

            // 4th semakan
            $sl4bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logbon)) {
                $semakan5logbon = $s5logbon;
            }

            // 5th semakan
            $sl5bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubsbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubsbon)) {
                $cetaknotisbpubsbon = $cnbpubsbon;
            }

            // 6th semakan
            $sl6bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logbon)) {
                $semakan7logbon = $s7logbon;
            }

            // 7th semakan
            $sl7bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogbon)) {
                $cetaknotisuulogbon = $cnuulogbon;
            }

            $s1bon               = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s1bon)) {
                $semakan1bon = $s1bon;
            }
            $s2bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s2bon)) {
                $semakan2bon = $s2bon;
            }
            $s3bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s3bon)) {
                $semakan3bon = $s3bon;
            }
            $s4bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s4bon)) {
                $semakan4bon = $s4bon;
            }
            $s5bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s5bon)) {
                $semakan5bon = $s5bon;
            }
            $s6bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s6bon)) {
                $semakan6bon = $s6bon;
            }
            $s7bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s7bon)) {
                $semakan7bon = $s7bon;
            }
            $cnbon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cnbon)) {
                $cetaknotisbon = $cnbon;
            }
            $cnuubon = Review::where('status', 'Selesai')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orWhere('status', 'CN')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->first();
            if (! empty($cnuubon)) {
                $cetaknotisuubon = $cnuubon;
            }
            $cnbpubbon = Review::where('status', 'Selesai')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orWhere('status', 'CN')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orWhere('status', null)->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->first();
            if (! empty($cnbpubbon)) {
                $cetaknotisbpubbon = $cnbpubbon;
            }
            $kuiribon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($kuiribon)) {
                $quirybon = $kuiribon;
            }
            $urusbon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($urusbon)) {
                $urusetiabon = $urusbon;
            }
            $urus1bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($urus1bon)) {
                $urusetia1bon = $urus1bon;
            }
        }

        // insurans
        if (! empty($insurance)) {
            $review_previous_insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewinsurans           = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();

            $reviewloginsurans      = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2loginsurans)) {
                $semakan2loginsurans = $s2loginsurans;
            }

            // 2nd semakan
            $sl2insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3loginsurans)) {
                $semakan3loginsurans = $s3loginsurans;
            }
            $s3zeroinsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zeroinsurans)) {
                $semakan3zeroinsurans = $s3zeroinsurans;
            }

            // 3rd semakan
            $sl3insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4loginsurans)) {
                $semakan4loginsurans = $s4loginsurans;
            }

            // 4th semakan
            $sl4insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5loginsurans)) {
                $semakan5loginsurans = $s5loginsurans;
            }

            // 5th semakan
            $sl5insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubsinsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubsinsurans)) {
                $cetaknotisbpubsinsurans = $cnbpubsinsurans;
            }

            // 6th semakan
            $sl6insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7loginsurans)) {
                $semakan7loginsurans = $s7loginsurans;
            }

            // 7th semakan
            $sl7insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuuloginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuuloginsurans)) {
                $cetaknotisuuloginsurans = $cnuuloginsurans;
            }

            $s1insurans               = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s1insurans)) {
                $semakan1insurans = $s1insurans;
            }
            $s2insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s2insurans)) {
                $semakan2insurans = $s2insurans;
            }
            $s3insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s3insurans)) {
                $semakan3insurans = $s3insurans;
            }
            $s4insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s4insurans)) {
                $semakan4insurans = $s4insurans;
            }
            $s5insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s5insurans)) {
                $semakan5insurans = $s5insurans;
            }
            $s6insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s6insurans)) {
                $semakan6insurans = $s6insurans;
            }
            $s7insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s7insurans)) {
                $semakan7insurans = $s7insurans;
            }
            $cninsurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cninsurans)) {
                $cetaknotisinsurans = $cninsurans;
            }
            $cnuuinsurans = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuuinsurans)) {
                $cetaknotisuuinsurans = $cnuuinsurans;
            }
            $cnbpubinsurans = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubinsurans)) {
                $cetaknotisbpubinsurans = $cnbpubinsurans;
            }
            $kuiriinsurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($kuiriinsurans)) {
                $quiryinsurans = $kuiriinsurans;
            }
            $urusinsurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($urusinsurans)) {
                $urusetiainsurans = $urusinsurans;
            }
            $urus1insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($urus1insurans)) {
                $urusetia1insurans = $urus1insurans;
            }
        }

        // wang pendahuluan
        if (! empty($deposit)) {
            $review_previous_wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewwang           = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();

            $reviewlogwang      = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logwang)) {
                $semakan2logwang = $s2logwang;
            }

            // 2nd semakan
            $sl2wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logwang)) {
                $semakan3logwang = $s3logwang;
            }
            $s3zerowang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerowang)) {
                $semakan3zerowang = $s3zerowang;
            }

            // 3rd semakan
            $sl3wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logwang)) {
                $semakan4logwang = $s4logwang;
            }

            // 4th semakan
            $sl4wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logwang)) {
                $semakan5logwang = $s5logwang;
            }

            // 5th semakan
            $sl5wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubswang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubswang)) {
                $cetaknotisbpubswang = $cnbpubswang;
            }

            // 6th semakan
            $sl6wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logwang)) {
                $semakan7logwang = $s7logwang;
            }

            // 7th semakan
            $sl7wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogwang)) {
                $cetaknotisuulogwang = $cnuulogwang;
            }

            $s1wang               = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s1wang)) {
                $semakan1wang = $s1wang;
            }
            $s2wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s2wang)) {
                $semakan2wang = $s2wang;
            }
            $s3wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s3wang)) {
                $semakan3wang = $s3wang;
            }
            $s4wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s4wang)) {
                $semakan4wang = $s4wang;
            }
            $s5wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s5wang)) {
                $semakan5wang = $s5wang;
            }
            $s6wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s6wang)) {
                $semakan6wang = $s6wang;
            }
            $s7wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s7wang)) {
                $semakan7wang = $s7wang;
            }
            $cnwang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cnwang)) {
                $cetaknotiswang = $cnwang;
            }
            $cnuuwang = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuuwang)) {
                $cetaknotisuuwang = $cnuuwang;
            }
            $cnbpubwang = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubwang)) {
                $cetaknotisbpubwang = $cnbpubwang;
            }
            $kuiriwang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($kuiriwang)) {
                $quirywang = $kuiriwang;
            }
            $uruswang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($uruswang)) {
                $urusetiawang = $uruswang;
            }
            $urus1wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($urus1wang)) {
                $urusetia1wang = $urus1wang;
            }
        }

        // dokumen kontrak
        if (! empty($document)) {
            $review_previous_doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewdoc           = Review::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();

            $reviewlogdoc      = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logdoc)) {
                $semakan2logdoc = $s2logdoc;
            }

            // 2nd semakan
            $sl2doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logdoc)) {
                $semakan3logdoc = $s3logdoc;
            }
            $s3zerodoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerodoc)) {
                $semakan3zerodoc = $s3zerodoc;
            }

            // 3rd semakan
            $sl3doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logdoc)) {
                $semakan4logdoc = $s4logdoc;
            }

            // 4th semakan
            $sl4doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logdoc)) {
                $semakan5logdoc = $s5logdoc;
            }

            // 5th semakan
            $sl5doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubsdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubsdoc)) {
                $cetaknotisbpubsdoc = $cnbpubsdoc;
            }

            // 6th semakan
            $sl6doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logdoc)) {
                $semakan7logdoc = $s7logdoc;
            }

            // 7th semakan
            $sl7doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogdoc)) {
                $cetaknotisuulogdoc = $cnuulogdoc;
            }

            $s1doc               = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s1doc)) {
                $semakan1doc = $s1doc;
            }
            $s2doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s2doc)) {
                $semakan2doc = $s2doc;
            }
            $s3doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s3doc)) {
                $semakan3doc = $s3doc;
            }
            $s4doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s4doc)) {
                $semakan4doc = $s4doc;
            }
            $s5doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s5doc)) {
                $semakan5doc = $s5doc;
            }
            $s6doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s6doc)) {
                $semakan6doc = $s6doc;
            }
            $s7doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s7doc)) {
                $semakan7doc = $s7doc;
            }
            $cndoc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cndoc)) {
                $cetaknotisdoc = $cndoc;
            }
            $cnuudoc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuudoc)) {
                $cetaknotisuudoc = $cnuudoc;
            }
            $cnbpubdoc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubdoc)) {
                $cetaknotisbpubdoc = $cnbpubdoc;
            }
            $kuiridoc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($kuiridoc)) {
                $quirydoc = $kuiridoc;
            }
            $urusdoc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($urusdoc)) {
                $urusetiadoc = $urusdoc;
            }
            $urus1doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($urus1doc)) {
                $urusetia1doc = $urus1doc;
            }
        }

        // cpc
        if (! empty($cpc)) {
            $review_previous_cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcpc           = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();

            $reviewlogcpc      = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcpc)) {
                $semakan2logcpc = $s2logcpc;
            }

            // 2nd semakan
            $sl2cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcpc)) {
                $semakan3logcpc = $s3logcpc;
            }
            $s3zerocpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocpc)) {
                $semakan3zerocpc = $s3zerocpc;
            }

            // 3rd semakan
            $sl3cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcpc)) {
                $semakan4logcpc = $s4logcpc;
            }

            // 4th semakan
            $sl4cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcpc)) {
                $semakan5logcpc = $s5logcpc;
            }

            // 5th semakan
            $sl5cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscpc)) {
                $cetaknotisbpubscpc = $cnbpubscpc;
            }

            // 6th semakan
            $sl6cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcpc)) {
                $semakan7logcpc = $s7logcpc;
            }

            // 7th semakan
            $sl7cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcpc)) {
                $cetaknotisuulogcpc = $cnuulogcpc;
            }

            $s1cpc               = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s1cpc)) {
                $semakan1cpc = $s1cpc;
            }
            $s2cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s2cpc)) {
                $semakan2cpc = $s2cpc;
            }
            $s3cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s3cpc)) {
                $semakan3cpc = $s3cpc;
            }
            $s4cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s4cpc)) {
                $semakan4cpc = $s4cpc;
            }
            $s5cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s5cpc)) {
                $semakan5cpc = $s5cpc;
            }
            $s6cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s6cpc)) {
                $semakan6cpc = $s6cpc;
            }
            $s7cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s7cpc)) {
                $semakan7cpc = $s7cpc;
            }
            $cncpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncpc)) {
                $cetaknotiscpc = $cncpc;
            }
            $cnuucpc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucpc)) {
                $cetaknotisuucpc = $cnuucpc;
            }
            $cnbpubcpc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcpc)) {
                $cetaknotisbpubcpc = $cnbpubcpc;
            }
            $kuiricpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($kuiricpc)) {
                $quirycpc = $kuiricpc;
            }
            $uruscpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($uruscpc)) {
                $urusetiacpc = $uruscpc;
            }
            $urus1cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($urus1cpc)) {
                $urusetia1cpc = $urus1cpc;
            }
        }

        // cnc
        if (! empty($cnc)) {
            $review_previous_cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcnc           = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();

            $reviewlogcnc      = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcnc)) {
                $semakan2logcnc = $s2logcnc;
            }

            // 2nd semakan
            $sl2cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcnc)) {
                $semakan3logcnc = $s3logcnc;
            }
            $s3zerocnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocnc)) {
                $semakan3zerocnc = $s3zerocnc;
            }

            // 3rd semakan
            $sl3cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcnc)) {
                $semakan4logcnc = $s4logcnc;
            }

            // 4th semakan
            $sl4cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcnc)) {
                $semakan5logcnc = $s5logcnc;
            }

            // 5th semakan
            $sl5cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscnc)) {
                $cetaknotisbpubscnc = $cnbpubscnc;
            }

            // 6th semakan
            $sl6cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcnc)) {
                $semakan7logcnc = $s7logcnc;
            }

            // 7th semakan
            $sl7cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcnc)) {
                $cetaknotisuulogcnc = $cnuulogcnc;
            }

            $s1cnc               = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s1cnc)) {
                $semakan1cnc = $s1cnc;
            }
            $s2cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s2cnc)) {
                $semakan2cnc = $s2cnc;
            }
            $s3cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s3cnc)) {
                $semakan3cnc = $s3cnc;
            }
            $s4cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s4cnc)) {
                $semakan4cnc = $s4cnc;
            }
            $s5cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s5cnc)) {
                $semakan5cnc = $s5cnc;
            }
            $s6cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s6cnc)) {
                $semakan6cnc = $s6cnc;
            }
            $s7cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s7cnc)) {
                $semakan7cnc = $s7cnc;
            }
            $cncnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncnc)) {
                $cetaknotiscnc = $cncnc;
            }
            $cnuucnc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucnc)) {
                $cetaknotisuucnc = $cnuucnc;
            }
            $cnbpubcnc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcnc)) {
                $cetaknotisbpubcnc = $cnbpubcnc;
            }
            $kuiricnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($kuiricnc)) {
                $quirycnc = $kuiricnc;
            }
            $uruscnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($uruscnc)) {
                $urusetiacnc = $uruscnc;
            }
            $urus1cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($urus1cnc)) {
                $urusetia1cnc = $urus1cnc;
            }
        }

        // cmgd
        if (! empty($cmgd)) {
            $review_previous_cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcmgd           = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();

            $reviewlogcmgd      = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcmgd)) {
                $semakan2logcmgd = $s2logcmgd;
            }

            // 2nd semakan
            $sl2cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcmgd)) {
                $semakan3logcmgd = $s3logcmgd;
            }
            $s3zerocmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocmgd)) {
                $semakan3zerocmgd = $s3zerocmgd;
            }

            // 3rd semakan
            $sl3cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcmgd)) {
                $semakan4logcmgd = $s4logcmgd;
            }

            // 4th semakan
            $sl4cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcmgd)) {
                $semakan5logcmgd = $s5logcmgd;
            }

            // 5th semakan
            $sl5cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscmgd)) {
                $cetaknotisbpubscmgd = $cnbpubscmgd;
            }

            // 6th semakan
            $sl6cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcmgd)) {
                $semakan7logcmgd = $s7logcmgd;
            }

            // 7th semakan
            $sl7cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcmgd)) {
                $cetaknotisuulogcmgd = $cnuulogcmgd;
            }

            $s1cmgd               = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s1cmgd)) {
                $semakan1cmgd = $s1cmgd;
            }
            $s2cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s2cmgd)) {
                $semakan2cmgd = $s2cmgd;
            }
            $s3cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s3cmgd)) {
                $semakan3cmgd = $s3cmgd;
            }
            $s4cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s4cmgd)) {
                $semakan4cmgd = $s4cmgd;
            }
            $s5cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s5cmgd)) {
                $semakan5cmgd = $s5cmgd;
            }
            $s6cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s6cmgd)) {
                $semakan6cmgd = $s6cmgd;
            }
            $s7cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s7cmgd)) {
                $semakan7cmgd = $s7cmgd;
            }
            $cncmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncmgd)) {
                $cetaknotiscnc = $cncmgd;
            }
            $cnuucmgd = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucmgd)) {
                $cetaknotisuucmgd = $cnuucmgd;
            }
            $cnbpubcmgd = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcmgd)) {
                $cetaknotisbpubcmgd = $cnbpubcmgd;
            }
            $kuiricmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($kuiricmgd)) {
                $quirycmgd = $kuiricmgd;
            }
            $uruscmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($uruscmgd)) {
                $urusetiacmgd = $uruscmgd;
            }
            $urus1cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($urus1cmgd)) {
                $urusetia1cmgd = $urus1cmgd;
            }
        }

        // cpo
        if (! empty($cpo)) {
            $review_previous_cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcpo           = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();

            $reviewlogcpo      = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcpo)) {
                $semakan2logcpo = $s2logcpo;
            }

            // 2nd semakan
            $sl2cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcpo)) {
                $semakan3logcpo = $s3logcpo;
            }
            $s3zerocpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocpo)) {
                $semakan3zerocpo = $s3zerocpo;
            }

            // 3rd semakan
            $sl3cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcpo)) {
                $semakan4logcpo = $s4logcpo;
            }

            // 4th semakan
            $sl4cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcpo)) {
                $semakan5logcpo = $s5logcpo;
            }

            // 5th semakan
            $sl5cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscpo)) {
                $cetaknotisbpubscpo = $cnbpubscpo;
            }

            // 6th semakan
            $sl6cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcpo)) {
                $semakan7logcpo = $s7logcpo;
            }

            // 7th semakan
            $sl7cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcpo)) {
                $cetaknotisuulogcpo = $cnuulogcpo;
            }

            $s1cpo               = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s1cpo)) {
                $semakan1cpo = $s1cpo;
            }
            $s2cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s2cpo)) {
                $semakan2cpo = $s2cpo;
            }
            $s3cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s3cpo)) {
                $semakan3cpo = $s3cpo;
            }
            $s4cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s4cpo)) {
                $semakan4cpo = $s4cpo;
            }
            $s5cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s5cpo)) {
                $semakan5cpo = $s5cpo;
            }
            $s6cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s6cpo)) {
                $semakan6cpo = $s6cpo;
            }
            $s7cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s7cpo)) {
                $semakan7cpo = $s7cpo;
            }
            $cncpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncpo)) {
                $cetaknotiscnc = $cncpo;
            }
            $cnuucpo = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucpo)) {
                $cetaknotisuucpo = $cnuucpo;
            }
            $cnbpubcpo = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcpo)) {
                $cetaknotisbpubcpo = $cnbpubcpo;
            }
            $kuiricpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($kuiricpo)) {
                $quirycpo = $kuiricpo;
            }
            $uruscpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($uruscpo)) {
                $urusetiacpo = $uruscpo;
            }
            $urus1cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($urus1cpo)) {
                $urusetia1cpo = $urus1cpo;
            }
        }

        if (Department::ALL_ID == $sst->acquisition->approval->department_id) {
            $department = Department::exceptAll()->get();
        }
        
        //Add filter 
        $isViewDocumentContract = true;
        
       // $sst = $this->sst;
       // $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $bonApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Bon Pelaksanaan')
                                        ->where('type','Dokumen Kontrak')->first();
        $insuranceApproved = Review::where('acquisition_id', $sst->acquisition_id)
                                        ->where('status','Selesai')
                                        ->where('document_contract_type','Insurans')
                                        ->where('type','Dokumen Kontrak')->first();

//        $sstApproved = Review::where('acquisition_id', $sst->acquisition_id)
//                                        ->where('status','Selesai')
//                                        ->where('type','SST')->first();
        
        if(empty($bonApproved) || empty($insuranceApproved)){
            $isViewDocumentContract = false;
        }
        

        return view('contract.post.show', compact(
            'companies',
            'sst',
            'appointed',
            'bon',
            'insurance',
            'BQElemen',
            'BQItem',
            'BQSub',
            'deposit',
            'document',
            'cpc',
            'cnc',
            'cmgd',
            'cpo',
            'sub_contract',
            'review', 'review_previous', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog',
            'reviewbon', 'review_previous_bon', 's1bon', 's2bon', 's3bon', 's4bon', 's5bon', 's6bon', 's7bon', 'cnbon', 'cnuubon', 'cnbpubbon', 'cetaknotisbpubbon', 'kuiribon', 'urusbon', 'urus1bon', 'semakan1bon', 'semakan2bon', 'semakan3bon', 'semakan4bon', 'semakan5bon', 'semakan6bon', 'semakan7bon', 'cetaknotisbon', 'cetaknotisuubon', 'cetaknotisbon', 'quirybon', 'urusetiabon', 'urusetia1bon','sl1bon','sl2bon','sl3bon','sl4bon','sl5bon','sl6bon','sl7bon', 'cnuulogbon','semakan2logbon','semakan3logbon','semakan3zerobon','semakan4logbon','semakan5logbon','cetaknotisbpubsbon','semakan7logbon','cetaknotisuulogbon','reviewlogbon',
            'reviewinsurans', 'review_previous_insurans', 's1insurans', 's2insurans', 's3insurans', 's4insurans', 's5insurans', 's6insurans', 's7insurans', 'cnuuinsurans', 'cninsurans', 'cnbpubinsurans', 'cetaknotisbpubinsurans', 'kuiriinsurans', 'urusinsurans', 'urus1insurans', 'semakan1insurans', 'semakan2insurans', 'semakan3insurans', 'semakan4insurans', 'semakan5insurans', 'semakan6insurans', 'semakan7insurans', 'cetaknotisinsurans', 'cetaknotisuuinsurans', 'cetaknotisinsurans', 'quiryinsurans', 'urusetiainsurans', 'urusetia1insurans','sl1insurans','sl2insurans','sl3insurans','sl4insurans','sl5insurans','sl6insurans','sl7insurans', 'cnuuloginsurans','semakan2loginsurans','semakan3loginsurans','semakan3zeroinsurans','semakan4loginsurans','semakan5loginsurans','cetaknotisbpubsinsurans','semakan7loginsurans','cetaknotisuuloginsurans','reviewloginsurans',
            'reviewwang', 's1wang', 'review_previous_wang', 's2wang', 's3wang', 's4wang', 's5wang', 's6wang', 's7wang', 'cnwang', 'cnuuwang', 'cnbpubwang', 'cetaknotisbpubwang', 'kuiriwang', 'uruswang', 'urus1wang', 'semakan1wang', 'semakan2wang', 'semakan3wang', 'semakan4wang', 'semakan5wang', 'semakan6wang', 'semakan7wang', 'cetaknotiswang', 'cetaknotisuuwang', 'cetaknotiswang', 'quirywang', 'urusetiawang', 'urusetia1wang','sl1wang','sl2wang','sl3wang','sl4wang','sl5wang','sl6wang','sl7wang', 'cnuulogwang','semakan2logwang','semakan3logwang','semakan3zerowang','semakan4logwang','semakan5logwang','cetaknotisbpubswang','semakan7logwang','cetaknotisuulogwang','reviewlogwang',
            'reviewdoc', 'review_previous_doc', 's1doc', 's2doc', 's3doc', 's4doc', 's5doc', 's6doc', 's7doc', 'cndoc', 'cnuudoc', 'cnbpubdoc', 'cetaknotisbpubdoc', 'kuiridoc', 'urusdoc', 'urus1doc', 'semakan1doc', 'semakan2doc', 'semakan3doc', 'semakan4doc', 'semakan5doc', 'semakan6doc', 'semakan7doc', 'cetaknotisdoc', 'cetaknotisuudoc', 'cetaknotisdoc', 'quirydoc', 'urusetiadoc', 'urusetia1doc','sl1doc','sl2doc','sl3doc','sl4doc','sl5doc','sl6doc','sl7doc', 'cnuulogdoc','semakan2logdoc','semakan3logdoc','semakan3zerodoc','semakan4logdoc','semakan5logdoc','cetaknotisbpubsdoc','semakan7logdoc','cetaknotisuulogdoc','reviewlogdoc',
            'reviewcpc', 'review_previous_cpc', 's1cpc', 's2cpc', 's3cpc', 's4cpc', 's5cpc', 's6cpc', 's7cpc', 'cncpc', 'cnuucpc', 'cnbpubcpc', 'cetaknotisbpubcpc', 'kuiricpc', 'uruscpc', 'urus1cpc', 'semakan1cpc', 'semakan2cpc', 'semakan3cpc', 'semakan4cpc', 'semakan5cpc', 'semakan6cpc', 'semakan7cpc', 'cetaknotiscpc', 'cetaknotisuucpc', 'cetaknotiscpc', 'quirycpc', 'urusetiacpc', 'urusetia1cpc','sl1cpc','sl2cpc','sl3cpc','sl4cpc','sl5cpc','sl6cpc','sl7cpc', 'cnuulogcpc','semakan2logcpc','semakan3logcpc','semakan3zerocpc','semakan4logcpc','semakan5logcpc','cetaknotisbpubscpc','semakan7logcpc','cetaknotisuulogcpc','reviewlogcpc',
            'reviewcnc', 'review_previous_cnc', 's1cnc', 's2cnc', 's3cnc', 's4cnc', 's5cnc', 's6cnc', 's7cnc', 'cncnc', 'cnuucnc', 'cnbpubcnc', 'cetaknotisbpubcnc', 'kuiricnc', 'uruscnc', 'urus1cnc', 'semakan1cnc', 'semakan2cnc', 'semakan3cnc', 'semakan4cnc', 'semakan5cnc', 'semakan6cnc', 'semakan7cnc', 'cetaknotiscnc', 'cetaknotisuucnc', 'cetaknotiscnc', 'quirycnc', 'urusetiacnc', 'urusetia1cnc','sl1cnc','sl2cnc','sl3cnc','sl4cnc','sl5cnc','sl6cnc','sl7cnc', 'cnuulogcnc','semakan2logcnc','semakan3logcnc','semakan3zerocnc','semakan4logcnc','semakan5logcnc','cetaknotisbpubscnc','semakan7logcnc','cetaknotisuulogcnc','reviewlogcnc',
            'reviewcmgd', 'review_previous_cmgd', 's1cmgd', 's2cmgd', 's3cmgd', 's4cmgd', 's5cmgd', 's6cmgd', 's7cmgd', 'cncmgd', 'cnuucmgd', 'cnbpubcmgd', 'cetaknotisbpubcmgd', 'kuiricmgd', 'uruscmgd', 'urus1cmgd', 'semakan1cmgd', 'semakan2cmgd', 'semakan3cmgd', 'semakan4cmgd', 'semakan5cmgd', 'semakan6cmgd', 'semakan7cmgd', 'cetaknotiscmgd', 'cetaknotisuucmgd', 'cetaknotiscmgd', 'quirycmgd', 'urusetiacmgd', 'urusetia1cmgd','sl1cmgd','sl2cmgd','sl3cmgd','sl4cmgd','sl5cmgd','sl6cmgd','sl7cmgd', 'cnuulogcmgd','semakan2logcmgd','semakan3logcmgd','semakan3zerocmgd','semakan4logcmgd','semakan5logcmgd','cetaknotisbpubscmgd','semakan7logcmgd','cetaknotisuulogcmgd','reviewlogcmgd',
            'reviewcpo', 'review_previous_cpo', 's1cpo', 's2cpo', 's3cpo', 's4cpo', 's5cpo', 's6cpo', 's7cpo', 'cncpo', 'cnuucpo', 'cnbpubcpo', 'cetaknotisbpubcpo', 'kuiricpo', 'uruscpo', 'urus1cpo', 'semakan1cpo', 'semakan2cpo', 'semakan3cpo', 'semakan4cpo', 'semakan5cpo', 'semakan6cpo', 'semakan7cpo', 'cetaknotiscpo', 'cetaknotisuucpo', 'cetaknotiscpo', 'quirycpo', 'urusetiacpo', 'urusetia1cpo','sl1cpo','sl2cpo','sl3cpo','sl4cpo','sl5cpo','sl6cpo','sl7cpo', 'cnuulogcpo','semakan2logcpo','semakan3logcpo','semakan3zerocpo','semakan4logcpo','semakan5logcpo','cetaknotisbpubscpo','semakan7logcpo','cetaknotisuulogcpo','reviewlogcpo',
            'isViewDocumentContract'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst       = Sst::with(['acquisition', 'acquisition.appointed', 'acquisition.approval', 'acquisition.approval.type', 'acquisition.category','acquisition.bqElements','acquisition.bqElements.bqItems','acquisition.bqElements.bqItems.bqSubItems'])->where('hashslug', $id)->select('ssts.*')->first();
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->first();

        $banks      = banks()->pluck('name', 'id');
        $insurances = insurances()->pluck('name', 'id');

        /*
        * 04122019 - wjp cuma keluar untuk kerja sahaja
        */
        if($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 3 || $sst->acquisition->approval->acquisition_type_id == 4){
            $bon_types  = bon_types_bekalan_perkhidmatan();
        } else {
            $bon_types  = bon_types();
        }

        $companies  = companies();

        $getExpiredDate = sst($sst)->getExpiredDate();//dd($getExpiredDate);
        $expiry_date   = sst($sst)->getExpiredDateBon($getExpiredDate);//dd($expiry_date);
        $expiry_date_insuran   = sst($sst)->getExpiredDateInsuran($getExpiredDate);//dd($expiry_date_insuran);
        $proposed_date = sst($sst)->getProposedDate();

        $bon = Bon::with(['documents'])->where('sst_id', $sst->id)->first();
        $bon_has_reviewed = common()->isReviewApproved($sst->acquisition_id, 'Dokumen Kontrak', 'Bon Pelaksanaan');

        $insurance = Insurance::with(['documents_publicLiability', 'documents_work', 'documents_compensation'])->where('sst_id', $sst->id)->first();

        $BQElemen = Element::where('acquisition_id', $sst->acquisition->id)->where('deleted_at',null)->orderBy('no','asc')->get();
        $BQItem   = Item::where('acquisition_id', $sst->acquisition->id)->where('deleted_at',null)->orderBy('bq_no_element','asc')->orderBy('no','asc')->get();
        $BQSub    = SubItem::where('acquisition_id', $sst->acquisition->id)->where('deleted_at',null)->orderBy('bq_no_element','asc')->orderBy('bq_no_item','asc')->orderBy('no','asc')->get();

        $getDeposit               = sst($sst)->getDepositRules();
        $isDepositAllowed         = $getDeposit["isAllow"];
        $depositReason            = $getDeposit["reason"];
        $deposit                  = Deposit::with(['documents'])->where('sst_id', $sst->id)->first();
        $deposit_prime_cost       = Element::where('acquisition_id', $sst->acquisition->id)->where('element', 'like', '%' . 'PRO' . '%' . ' SUM' . '%')->pluck('amount_adjust')->sum();
        $deposit_amount           = $sst->acquisition->appointed->first()->offered_price - $deposit_prime_cost;
        
        $data                     = sst($sst)->getTotalWjp($deposit_amount);
        $deposit_total_amount     = $data['deposit_total_amount'];
        $deposit_qualified_amount = $data['deposit_qualified_amount'];
        $deposit_has_reviewed     = common()->isReviewApproved($sst->acquisition_id, 'Dokumen Kontrak', 'Wang Pendahuluan');

        $document               = AcqDocument::with(['sst'])->where('sst_id', $sst->id)->first();
        $document_proposed_date = sst($sst)->getDocProposedDate() ?? null;
        $getDocument               = sst($sst)->getDocumentContractRules();
        $isDocumentAllowed         = $getDocument["isAllow"];

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();

        $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        $review          = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        $reviewlog      = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

        // 1st semakan
        $sl1 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s2log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s2log)) {
            $semakan2log = $s2log;
        }

        // 2nd semakan
        $sl2 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s3log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s3log)) {
            $semakan3log = $s3log;
        }
        $s3zero = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s3zero)) {
            $semakan3zero = $s3zero;
        }

        // 3rd semakan
        $sl3 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s4log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s4log)) {
            $semakan4log = $s4log;
        }

        // 4th semakan
        $sl4 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s5log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s5log)) {
            $semakan5log = $s5log;
        }

        // 5th semakan
        $sl5 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnbpubs = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
        if (! empty($cnbpubs)) {
            $cetaknotisbpubs = $cnbpubs;
        }

        // 6th semakan
        $sl6 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s7log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s7log)) {
            $semakan7log = $s7log;
        }

        // 7th semakan
        $sl7 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnuulog = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($cnuulog)) {
            $cetaknotisuulog = $cnuulog;
        }

        $s1              = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s1)) {
            $semakan1 = $s1;
        }
        $s2 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s2)) {
            $semakan2 = $s2;
        }
        $s3 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s3)) {
            $semakan3 = $s3;
        }
        $s4 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s4)) {
            $semakan4 = $s4;
        }
        $s5 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s5)) {
            $semakan5 = $s5;
        }
        $s6 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s6)) {
            $semakan6 = $s6;
        }
        $s7 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($s7)) {
            $semakan7 = $s7;
        }
        $cn = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->where('progress', '1')->first();
        if (! empty($cn)) {
            $cetaknotis = $cn;
        }
        $cnuu = Review::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->where('status', 'Selesai')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->first();
        if (! empty($cnuu)) {
            $cetaknotisuu = $cnuu;
        }
        $cnbpub = Review::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->where('status', 'Selesai')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->first();
        if (! empty($cnbpub)) {
            $cetaknotisbpub = $cnbpub;
        }
        $kuiri = Review::where('acquisition_id', $sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($kuiri)) {
            $quiry = $kuiri;
        }
        $urus = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($urus)) {
            $urusetia = $urus;
        }
        $urus1 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
        if (! empty($urus1)) {
            $urusetia1 = $urus1;
        }

        // bon
        if (! empty($bon)) {
            $review_previous_bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewbon           = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();

            $reviewlogbon      = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logbon)) {
                $semakan2logbon = $s2logbon;
            }

            // 2nd semakan
            $sl2bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logbon)) {
                $semakan3logbon = $s3logbon;
            }
            $s3zerobon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerobon)) {
                $semakan3zerobon = $s3zerobon;
            }

            // 3rd semakan
            $sl3bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logbon)) {
                $semakan4logbon = $s4logbon;
            }

            // 4th semakan
            $sl4bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logbon)) {
                $semakan5logbon = $s5logbon;
            }

            // 5th semakan
            $sl5bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubsbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubsbon)) {
                $cetaknotisbpubsbon = $cnbpubsbon;
            }

            // 6th semakan
            $sl6bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logbon)) {
                $semakan7logbon = $s7logbon;
            }

            // 7th semakan
            $sl7bon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogbon = ReviewLog::where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogbon)) {
                $cetaknotisuulogbon = $cnuulogbon;
            }

            $s1bon               = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s1bon)) {
                $semakan1bon = $s1bon;
            }
            $s2bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s2bon)) {
                $semakan2bon = $s2bon;
            }
            $s3bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s3bon)) {
                $semakan3bon = $s3bon;
            }
            $s4bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s4bon)) {
                $semakan4bon = $s4bon;
            }
            $s5bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s5bon)) {
                $semakan5bon = $s5bon;
            }
            $s6bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s6bon)) {
                $semakan6bon = $s6bon;
            }
            $s7bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($s7bon)) {
                $semakan7bon = $s7bon;
            }
            $cnbon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cnbon)) {
                $cetaknotisbon = $cnbon;
            }
            $cnuubon = Review::where('status', 'Selesai')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orWhere('status', 'CN')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->first();
            if (! empty($cnuubon)) {
                $cetaknotisuubon = $cnuubon;
            }
            $cnbpubbon = Review::where('status', 'Selesai')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orWhere('status', 'CN')->where('acquisition_id', $bon->sst->acquisition_id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->first();
            if (! empty($cnbpubbon)) {
                $cetaknotisbpubbon = $cnbpubbon;
            }
            $kuiribon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($kuiribon)) {
                $quirybon = $kuiribon;
            }
            $urusbon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($urusbon)) {
                $urusetiabon = $urusbon;
            }
            $urus1bon = Review::where('acquisition_id', $bon->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
            if (! empty($urus1bon)) {
                $urusetia1bon = $urus1bon;
            }
        }

        // insurans
        if (! empty($insurance)) {
            $review_previous_insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewinsurans           = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();

            $reviewloginsurans      = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2loginsurans)) {
                $semakan2loginsurans = $s2loginsurans;
            }

            // 2nd semakan
            $sl2insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3loginsurans)) {
                $semakan3loginsurans = $s3loginsurans;
            }
            $s3zeroinsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zeroinsurans)) {
                $semakan3zeroinsurans = $s3zeroinsurans;
            }

            // 3rd semakan
            $sl3insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4loginsurans)) {
                $semakan4loginsurans = $s4loginsurans;
            }

            // 4th semakan
            $sl4insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5loginsurans)) {
                $semakan5loginsurans = $s5loginsurans;
            }

            // 5th semakan
            $sl5insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubsinsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubsinsurans)) {
                $cetaknotisbpubsinsurans = $cnbpubsinsurans;
            }

            // 6th semakan
            $sl6insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7loginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7loginsurans)) {
                $semakan7loginsurans = $s7loginsurans;
            }

            // 7th semakan
            $sl7insurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuuloginsurans = ReviewLog::where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuuloginsurans)) {
                $cetaknotisuuloginsurans = $cnuuloginsurans;
            }

            $s1insurans               = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s1insurans)) {
                $semakan1insurans = $s1insurans;
            }
            $s2insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s2insurans)) {
                $semakan2insurans = $s2insurans;
            }
            $s3insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s3insurans)) {
                $semakan3insurans = $s3insurans;
            }
            $s4insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s4insurans)) {
                $semakan4insurans = $s4insurans;
            }
            $s5insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s5insurans)) {
                $semakan5insurans = $s5insurans;
            }
            $s6insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s6insurans)) {
                $semakan6insurans = $s6insurans;
            }
            $s7insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($s7insurans)) {
                $semakan7insurans = $s7insurans;
            }
            $cninsurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cninsurans)) {
                $cetaknotisinsurans = $cninsurans;
            }
            $cnuuinsurans = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuuinsurans)) {
                $cetaknotisuuinsurans = $cnuuinsurans;
            }
            $cnbpubinsurans = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->where('acquisition_id', $insurance->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubinsurans)) {
                $cetaknotisbpubinsurans = $cnbpubinsurans;
            }
            $kuiriinsurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($kuiriinsurans)) {
                $quiryinsurans = $kuiriinsurans;
            }
            $urusinsurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($urusinsurans)) {
                $urusetiainsurans = $urusinsurans;
            }
            $urus1insurans = Review::where('acquisition_id', $insurance->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
            if (! empty($urus1insurans)) {
                $urusetia1insurans = $urus1insurans;
            }
        }

        // wang pendahuluan
        if (! empty($deposit)) {
            $review_previous_wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewwang           = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();

            $reviewlogwang      = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logwang)) {
                $semakan2logwang = $s2logwang;
            }

            // 2nd semakan
            $sl2wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logwang)) {
                $semakan3logwang = $s3logwang;
            }
            $s3zerowang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerowang)) {
                $semakan3zerowang = $s3zerowang;
            }

            // 3rd semakan
            $sl3wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logwang)) {
                $semakan4logwang = $s4logwang;
            }

            // 4th semakan
            $sl4wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logwang)) {
                $semakan5logwang = $s5logwang;
            }

            // 5th semakan
            $sl5wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubswang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubswang)) {
                $cetaknotisbpubswang = $cnbpubswang;
            }

            // 6th semakan
            $sl6wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logwang)) {
                $semakan7logwang = $s7logwang;
            }

            // 7th semakan
            $sl7wang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogwang = ReviewLog::where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogwang)) {
                $cetaknotisuulogwang = $cnuulogwang;
            }

            $s1wang               = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s1wang)) {
                $semakan1wang = $s1wang;
            }
            $s2wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s2wang)) {
                $semakan2wang = $s2wang;
            }
            $s3wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s3wang)) {
                $semakan3wang = $s3wang;
            }
            $s4wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s4wang)) {
                $semakan4wang = $s4wang;
            }
            $s5wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s5wang)) {
                $semakan5wang = $s5wang;
            }
            $s6wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s6wang)) {
                $semakan6wang = $s6wang;
            }
            $s7wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($s7wang)) {
                $semakan7wang = $s7wang;
            }
            $cnwang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cnwang)) {
                $cetaknotiswang = $cnwang;
            }
            $cnuuwang = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuuwang)) {
                $cetaknotisuuwang = $cnuuwang;
            }
            $cnbpubwang = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->where('acquisition_id', $deposit->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubwang)) {
                $cetaknotisbpubwang = $cnbpubwang;
            }
            $kuiriwang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($kuiriwang)) {
                $quirywang = $kuiriwang;
            }
            $uruswang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($uruswang)) {
                $urusetiawang = $uruswang;
            }
            $urus1wang = Review::where('acquisition_id', $deposit->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
            if (! empty($urus1wang)) {
                $urusetia1wang = $urus1wang;
            }
        }

        // dokumen kontrak
        if (! empty($document)) {
            $review_previous_doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewdoc           = Review::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();

            $reviewlogdoc      = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logdoc)) {
                $semakan2logdoc = $s2logdoc;
            }

            // 2nd semakan
            $sl2doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logdoc)) {
                $semakan3logdoc = $s3logdoc;
            }
            $s3zerodoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerodoc)) {
                $semakan3zerodoc = $s3zerodoc;
            }

            // 3rd semakan
            $sl3doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logdoc)) {
                $semakan4logdoc = $s4logdoc;
            }

            // 4th semakan
            $sl4doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logdoc)) {
                $semakan5logdoc = $s5logdoc;
            }

            // 5th semakan
            $sl5doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubsdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubsdoc)) {
                $cetaknotisbpubsdoc = $cnbpubsdoc;
            }

            // 6th semakan
            $sl6doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logdoc)) {
                $semakan7logdoc = $s7logdoc;
            }

            // 7th semakan
            $sl7doc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogdoc = ReviewLog::where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogdoc)) {
                $cetaknotisuulogdoc = $cnuulogdoc;
            }

            $s1doc               = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s1doc)) {
                $semakan1doc = $s1doc;
            }
            $s2doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s2doc)) {
                $semakan2doc = $s2doc;
            }
            $s3doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s3doc)) {
                $semakan3doc = $s3doc;
            }
            $s4doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s4doc)) {
                $semakan4doc = $s4doc;
            }
            $s5doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s5doc)) {
                $semakan5doc = $s5doc;
            }
            $s6doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s6doc)) {
                $semakan6doc = $s6doc;
            }
            $s7doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($s7doc)) {
                $semakan7doc = $s7doc;
            }
            $cndoc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cndoc)) {
                $cetaknotisdoc = $cndoc;
            }
            $cnuudoc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuudoc)) {
                $cetaknotisuudoc = $cnuudoc;
            }
            $cnbpubdoc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->where('acquisition_id', $document->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubdoc)) {
                $cetaknotisbpubdoc = $cnbpubdoc;
            }
            $kuiridoc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($kuiridoc)) {
                $quirydoc = $kuiridoc;
            }
            $urusdoc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($urusdoc)) {
                $urusetiadoc = $urusdoc;
            }
            $urus1doc = Review::where('acquisition_id', $document->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();
            if (! empty($urus1doc)) {
                $urusetia1doc = $urus1doc;
            }
        }

        // cpc
        if (! empty($cpc)) {
            $review_previous_cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcpc           = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();

            $reviewlogcpc      = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcpc)) {
                $semakan2logcpc = $s2logcpc;
            }

            // 2nd semakan
            $sl2cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcpc)) {
                $semakan3logcpc = $s3logcpc;
            }
            $s3zerocpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocpc)) {
                $semakan3zerocpc = $s3zerocpc;
            }

            // 3rd semakan
            $sl3cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcpc)) {
                $semakan4logcpc = $s4logcpc;
            }

            // 4th semakan
            $sl4cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcpc)) {
                $semakan5logcpc = $s5logcpc;
            }

            // 5th semakan
            $sl5cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscpc)) {
                $cetaknotisbpubscpc = $cnbpubscpc;
            }

            // 6th semakan
            $sl6cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcpc)) {
                $semakan7logcpc = $s7logcpc;
            }

            // 7th semakan
            $sl7cpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcpc = ReviewLog::where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcpc)) {
                $cetaknotisuulogcpc = $cnuulogcpc;
            }

            $s1cpc               = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s1cpc)) {
                $semakan1cpc = $s1cpc;
            }
            $s2cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s2cpc)) {
                $semakan2cpc = $s2cpc;
            }
            $s3cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s3cpc)) {
                $semakan3cpc = $s3cpc;
            }
            $s4cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s4cpc)) {
                $semakan4cpc = $s4cpc;
            }
            $s5cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s5cpc)) {
                $semakan5cpc = $s5cpc;
            }
            $s6cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s6cpc)) {
                $semakan6cpc = $s6cpc;
            }
            $s7cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($s7cpc)) {
                $semakan7cpc = $s7cpc;
            }
            $cncpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncpc)) {
                $cetaknotiscpc = $cncpc;
            }
            $cnuucpc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucpc)) {
                $cetaknotisuucpc = $cnuucpc;
            }
            $cnbpubcpc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->where('acquisition_id', $cpc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcpc)) {
                $cetaknotisbpubcpc = $cnbpubcpc;
            }
            $kuiricpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($kuiricpc)) {
                $quirycpc = $kuiricpc;
            }
            $uruscpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($uruscpc)) {
                $urusetiacpc = $uruscpc;
            }
            $urus1cpc = Review::where('acquisition_id', $cpc->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPC')->orderby('id', 'desc')->first();
            if (! empty($urus1cpc)) {
                $urusetia1cpc = $urus1cpc;
            }
        }

        // cnc
        if (! empty($cnc)) {
            $review_previous_cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcnc           = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();

            $reviewlogcnc      = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcnc)) {
                $semakan2logcnc = $s2logcnc;
            }

            // 2nd semakan
            $sl2cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcnc)) {
                $semakan3logcnc = $s3logcnc;
            }
            $s3zerocnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocnc)) {
                $semakan3zerocnc = $s3zerocnc;
            }

            // 3rd semakan
            $sl3cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcnc)) {
                $semakan4logcnc = $s4logcnc;
            }

            // 4th semakan
            $sl4cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcnc)) {
                $semakan5logcnc = $s5logcnc;
            }

            // 5th semakan
            $sl5cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscnc)) {
                $cetaknotisbpubscnc = $cnbpubscnc;
            }

            // 6th semakan
            $sl6cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcnc)) {
                $semakan7logcnc = $s7logcnc;
            }

            // 7th semakan
            $sl7cnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcnc = ReviewLog::where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcnc)) {
                $cetaknotisuulogcnc = $cnuulogcnc;
            }

            $s1cnc               = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s1cnc)) {
                $semakan1cnc = $s1cnc;
            }
            $s2cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s2cnc)) {
                $semakan2cnc = $s2cnc;
            }
            $s3cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s3cnc)) {
                $semakan3cnc = $s3cnc;
            }
            $s4cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s4cnc)) {
                $semakan4cnc = $s4cnc;
            }
            $s5cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s5cnc)) {
                $semakan5cnc = $s5cnc;
            }
            $s6cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s6cnc)) {
                $semakan6cnc = $s6cnc;
            }
            $s7cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($s7cnc)) {
                $semakan7cnc = $s7cnc;
            }
            $cncnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncnc)) {
                $cetaknotiscnc = $cncnc;
            }
            $cnuucnc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucnc)) {
                $cetaknotisuucnc = $cnuucnc;
            }
            $cnbpubcnc = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->where('acquisition_id', $cnc->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcnc)) {
                $cetaknotisbpubcnc = $cnbpubcnc;
            }
            $kuiricnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($kuiricnc)) {
                $quirycnc = $kuiricnc;
            }
            $uruscnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($uruscnc)) {
                $urusetiacnc = $uruscnc;
            }
            $urus1cnc = Review::where('acquisition_id', $cnc->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CNC')->orderby('id', 'desc')->first();
            if (! empty($urus1cnc)) {
                $urusetia1cnc = $urus1cnc;
            }
        }

        // cmgd
        if (! empty($cmgd)) {
            $review_previous_cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcmgd           = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();

            $reviewlogcmgd      = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcmgd)) {
                $semakan2logcmgd = $s2logcmgd;
            }

            // 2nd semakan
            $sl2cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcmgd)) {
                $semakan3logcmgd = $s3logcmgd;
            }
            $s3zerocmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocmgd)) {
                $semakan3zerocmgd = $s3zerocmgd;
            }

            // 3rd semakan
            $sl3cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcmgd)) {
                $semakan4logcmgd = $s4logcmgd;
            }

            // 4th semakan
            $sl4cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcmgd)) {
                $semakan5logcmgd = $s5logcmgd;
            }

            // 5th semakan
            $sl5cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscmgd)) {
                $cetaknotisbpubscmgd = $cnbpubscmgd;
            }

            // 6th semakan
            $sl6cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcmgd)) {
                $semakan7logcmgd = $s7logcmgd;
            }

            // 7th semakan
            $sl7cmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcmgd = ReviewLog::where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcmgd)) {
                $cetaknotisuulogcmgd = $cnuulogcmgd;
            }

            $s1cmgd               = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s1cmgd)) {
                $semakan1cmgd = $s1cmgd;
            }
            $s2cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s2cmgd)) {
                $semakan2cmgd = $s2cmgd;
            }
            $s3cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s3cmgd)) {
                $semakan3cmgd = $s3cmgd;
            }
            $s4cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s4cmgd)) {
                $semakan4cmgd = $s4cmgd;
            }
            $s5cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s5cmgd)) {
                $semakan5cmgd = $s5cmgd;
            }
            $s6cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s6cmgd)) {
                $semakan6cmgd = $s6cmgd;
            }
            $s7cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($s7cmgd)) {
                $semakan7cmgd = $s7cmgd;
            }
            $cncmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncmgd)) {
                $cetaknotiscnc = $cncmgd;
            }
            $cnuucmgd = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucmgd)) {
                $cetaknotisuucmgd = $cnuucmgd;
            }
            $cnbpubcmgd = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->where('acquisition_id', $cmgd->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubcmgd)) {
                $cetaknotisbpubcmgd = $cnbpubcmgd;
            }
            $kuiricmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($kuiricmgd)) {
                $quirycmgd = $kuiricmgd;
            }
            $uruscmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($uruscmgd)) {
                $urusetiacmgd = $uruscmgd;
            }
            $urus1cmgd = Review::where('acquisition_id', $cmgd->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CMGD')->orderby('id', 'desc')->first();
            if (! empty($urus1cmgd)) {
                $urusetia1cmgd = $urus1cmgd;
            }
        }

        // cpo
        if (! empty($cpo)) {
            $review_previous_cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $reviewcpo           = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();

            $reviewlogcpo      = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2logcpo)) {
                $semakan2logcpo = $s2logcpo;
            }

            // 2nd semakan
            $sl2cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3logcpo)) {
                $semakan3logcpo = $s3logcpo;
            }
            $s3zerocpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNPP')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zerocpo)) {
                $semakan3zerocpo = $s3zerocpo;
            }

            // 3rd semakan
            $sl3cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4logcpo)) {
                $semakan4logcpo = $s4logcpo;
            }

            // 4th semakan
            $sl4cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5logcpo)) {
                $semakan5logcpo = $s5logcpo;
            }

            // 5th semakan
            $sl5cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubscpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubscpo)) {
                $cetaknotisbpubscpo = $cnbpubscpo;
            }

            // 6th semakan
            $sl6cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7logcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7logcpo)) {
                $semakan7logcpo = $s7logcpo;
            }

            // 7th semakan
            $sl7cpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulogcpo = ReviewLog::where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulogcpo)) {
                $cetaknotisuulogcpo = $cnuulogcpo;
            }

            $s1cpo               = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S1')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s1cpo)) {
                $semakan1cpo = $s1cpo;
            }
            $s2cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S2')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s2cpo)) {
                $semakan2cpo = $s2cpo;
            }
            $s3cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S3')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s3cpo)) {
                $semakan3cpo = $s3cpo;
            }
            $s4cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S4')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s4cpo)) {
                $semakan4cpo = $s4cpo;
            }
            $s5cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S5')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s5cpo)) {
                $semakan5cpo = $s5cpo;
            }
            $s6cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S6')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s6cpo)) {
                $semakan6cpo = $s6cpo;
            }
            $s7cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'S7')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($s7cpo)) {
                $semakan7cpo = $s7cpo;
            }
            $cncpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cncpo)) {
                $cetaknotiscnc = $cncpo;
            }
            $cnuucpo = Review::where('status', 'Selesai')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuucpo)) {
                $cetaknotisuucpo = $cnuucpo;
            }
            $cnbpubcpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->where('status', 'Selesai')->orWhere('status', 'CN')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->first();
            if (! empty($cnbpubcpo)) {
                $cetaknotisbpubcpo = $cnbpubcpo;
            }
            $kuiricpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', null)->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($kuiricpo)) {
                $quirycpo = $kuiricpo;
            }
            $uruscpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'N')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($uruscpo)) {
                $urusetiacpo = $uruscpo;
            }
            $urus1cpo = Review::where('acquisition_id', $cpo->sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'Dokumen Kontrak')->where('document_contract_type', 'CPO')->orderby('id', 'desc')->first();
            if (! empty($urus1cpo)) {
                $urusetia1cpo = $urus1cpo;
            }
        }

        return view('contract.post.edit', compact(
            'bon',
            'bon_has_reviewed',
            'insurance',
            'sst',
            'appointed',
            'expiry_date',
            'expiry_date_insuran',
            'proposed_date',
            'BQElemen',
            'BQItem',
            'BQSub',
            'isDepositAllowed',
            'depositReason',
            'deposit',
            'deposit_prime_cost',
            'deposit_amount',
            'deposit_total_amount',
            'deposit_qualified_amount',
            'deposit_has_reviewed',
            'document',
            'document_proposed_date',
            'isDocumentAllowed',
            'sub_contract',
            'banks',
            'insurances',
            'bon_types',
            'companies',
            'review', 'review_previous', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog',
            'reviewbon', 'review_previous_bon', 's1bon', 's2bon', 's3bon', 's4bon', 's5bon', 's6bon', 's7bon', 'cnbon', 'cnuubon', 'cnbpubbon', 'cetaknotisbpubbon', 'kuiribon', 'urusbon', 'urus1bon', 'semakan1bon', 'semakan2bon', 'semakan3bon', 'semakan4bon', 'semakan5bon', 'semakan6bon', 'semakan7bon', 'cetaknotisbon', 'cetaknotisuubon', 'cetaknotisbon', 'quirybon', 'urusetiabon', 'urusetia1bon','sl1bon','sl2bon','sl3bon','sl4bon','sl5bon','sl6bon','sl7bon', 'cnuulogbon','semakan2logbon','semakan3logbon','semakan3zerobon','semakan4logbon','semakan5logbon','cetaknotisbpubsbon','semakan7logbon','cetaknotisuulogbon','reviewlogbon',
            'reviewinsurans', 'review_previous_insurans', 's1insurans', 's2insurans', 's3insurans', 's4insurans', 's5insurans', 's6insurans', 's7insurans', 'cnuuinsurans', 'cninsurans', 'cnbpubinsurans', 'cetaknotisbpubinsurans', 'kuiriinsurans', 'urusinsurans', 'urus1insurans', 'semakan1insurans', 'semakan2insurans', 'semakan3insurans', 'semakan4insurans', 'semakan5insurans', 'semakan6insurans', 'semakan7insurans', 'cetaknotisinsurans', 'cetaknotisuuinsurans', 'cetaknotisinsurans', 'quiryinsurans', 'urusetiainsurans', 'urusetia1insurans','sl1insurans','sl2insurans','sl3insurans','sl4insurans','sl5insurans','sl6insurans','sl7insurans', 'cnuuloginsurans','semakan2loginsurans','semakan3loginsurans','semakan3zeroinsurans','semakan4loginsurans','semakan5loginsurans','cetaknotisbpubsinsurans','semakan7loginsurans','cetaknotisuuloginsurans','reviewloginsurans',
            'reviewwang', 's1wang', 'review_previous_wang', 's2wang', 's3wang', 's4wang', 's5wang', 's6wang', 's7wang', 'cnwang', 'cnuuwang', 'cnbpubwang', 'cetaknotisbpubwang', 'kuiriwang', 'uruswang', 'urus1wang', 'semakan1wang', 'semakan2wang', 'semakan3wang', 'semakan4wang', 'semakan5wang', 'semakan6wang', 'semakan7wang', 'cetaknotiswang', 'cetaknotisuuwang', 'cetaknotiswang', 'quirywang', 'urusetiawang', 'urusetia1wang','sl1wang','sl2wang','sl3wang','sl4wang','sl5wang','sl6wang','sl7wang', 'cnuulogwang','semakan2logwang','semakan3logwang','semakan3zerowang','semakan4logwang','semakan5logwang','cetaknotisbpubswang','semakan7logwang','cetaknotisuulogwang','reviewlogwang',
            'reviewdoc', 'review_previous_doc', 's1doc', 's2doc', 's3doc', 's4doc', 's5doc', 's6doc', 's7doc', 'cndoc', 'cnuudoc', 'cnbpubdoc', 'cetaknotisbpubdoc', 'kuiridoc', 'urusdoc', 'urus1doc', 'semakan1doc', 'semakan2doc', 'semakan3doc', 'semakan4doc', 'semakan5doc', 'semakan6doc', 'semakan7doc', 'cetaknotisdoc', 'cetaknotisuudoc', 'cetaknotisdoc', 'quirydoc', 'urusetiadoc', 'urusetia1doc','sl1doc','sl2doc','sl3doc','sl4doc','sl5doc','sl6doc','sl7doc', 'cnuulogdoc','semakan2logdoc','semakan3logdoc','semakan3zerodoc','semakan4logdoc','semakan5logdoc','cetaknotisbpubsdoc','semakan7logdoc','cetaknotisuulogdoc','reviewlogdoc',
            'reviewcpc', 'review_previous_cpc', 's1cpc', 's2cpc', 's3cpc', 's4cpc', 's5cpc', 's6cpc', 's7cpc', 'cncpc', 'cnuucpc', 'cnbpubcpc', 'cetaknotisbpubcpc', 'kuiricpc', 'uruscpc', 'urus1cpc', 'semakan1cpc', 'semakan2cpc', 'semakan3cpc', 'semakan4cpc', 'semakan5cpc', 'semakan6cpc', 'semakan7cpc', 'cetaknotiscpc', 'cetaknotisuucpc', 'cetaknotiscpc', 'quirycpc', 'urusetiacpc', 'urusetia1cpc','sl1cpc','sl2cpc','sl3cpc','sl4cpc','sl5cpc','sl6cpc','sl7cpc', 'cnuulogcpc','semakan2logcpc','semakan3logcpc','semakan3zerocpc','semakan4logcpc','semakan5logcpc','cetaknotisbpubscpc','semakan7logcpc','cetaknotisuulogcpc','reviewlogcpc',
            'reviewcnc', 'review_previous_cnc', 's1cnc', 's2cnc', 's3cnc', 's4cnc', 's5cnc', 's6cnc', 's7cnc', 'cncnc', 'cnuucnc', 'cnbpubcnc', 'cetaknotisbpubcnc', 'kuiricnc', 'uruscnc', 'urus1cnc', 'semakan1cnc', 'semakan2cnc', 'semakan3cnc', 'semakan4cnc', 'semakan5cnc', 'semakan6cnc', 'semakan7cnc', 'cetaknotiscnc', 'cetaknotisuucnc', 'cetaknotiscnc', 'quirycnc', 'urusetiacnc', 'urusetia1cnc','sl1cnc','sl2cnc','sl3cnc','sl4cnc','sl5cnc','sl6cnc','sl7cnc', 'cnuulogcnc','semakan2logcnc','semakan3logcnc','semakan3zerocnc','semakan4logcnc','semakan5logcnc','cetaknotisbpubscnc','semakan7logcnc','cetaknotisuulogcnc','reviewlogcnc',
            'reviewcmgd', 'review_previous_cmgd', 's1cmgd', 's2cmgd', 's3cmgd', 's4cmgd', 's5cmgd', 's6cmgd', 's7cmgd', 'cncmgd', 'cnuucmgd', 'cnbpubcmgd', 'cetaknotisbpubcmgd', 'kuiricmgd', 'uruscmgd', 'urus1cmgd', 'semakan1cmgd', 'semakan2cmgd', 'semakan3cmgd', 'semakan4cmgd', 'semakan5cmgd', 'semakan6cmgd', 'semakan7cmgd', 'cetaknotiscmgd', 'cetaknotisuucmgd', 'cetaknotiscmgd', 'quirycmgd', 'urusetiacmgd', 'urusetia1cmgd','sl1cmgd','sl2cmgd','sl3cmgd','sl4cmgd','sl5cmgd','sl6cmgd','sl7cmgd', 'cnuulogcmgd','semakan2logcmgd','semakan3logcmgd','semakan3zerocmgd','semakan4logcmgd','semakan5logcmgd','cetaknotisbpubscmgd','semakan7logcmgd','cetaknotisuulogcmgd','reviewlogcmgd',
            'reviewcpo', 'review_previous_cpo', 's1cpo', 's2cpo', 's3cpo', 's4cpo', 's5cpo', 's6cpo', 's7cpo', 'cncpo', 'cnuucpo', 'cnbpubcpo', 'cetaknotisbpubcpo', 'kuiricpo', 'uruscpo', 'urus1cpo', 'semakan1cpo', 'semakan2cpo', 'semakan3cpo', 'semakan4cpo', 'semakan5cpo', 'semakan6cpo', 'semakan7cpo', 'cetaknotiscpo', 'cetaknotisuucpo', 'cetaknotiscpo', 'quirycpo', 'urusetiacpo', 'urusetia1cpo','sl1cpo','sl2cpo','sl3cpo','sl4cpo','sl5cpo','sl6cpo','sl7cpo', 'cnuulogcpo','semakan2logcpo','semakan3logcpo','semakan3zerocpo','semakan4logcpo','semakan5logcpo','cetaknotisbpubscpo','semakan7logcpo','cetaknotisuulogcpo','reviewlogcpo'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
