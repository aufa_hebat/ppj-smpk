<?php

namespace App\Http\Controllers\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Company\CIDB;
use App\Models\Company\MOF;
use App\Models\Sale;
use App\Services\ProjectMonitoringChartService as MonitoringChartService;
use App\Services\ProjectSiteVisitChartService as SiteVisitChartService;
use App\Services\ContractProgressChartService;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use Carbon\Carbon;

use App\Models\ReportType;

use App\Models\Department;

class MonitoringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ProjectProgressProcessor = new \App\Processors\Charts\ProjectProgressProcessorPersonal();
        
        $contractsSixMonthToEnd = $ProjectProgressProcessor->contractsSixMonthToEnd()->count();    
        $contractsOnGoing = $ProjectProgressProcessor->contractsOnGoing()->count();
        $contractsNotSignedYet = $ProjectProgressProcessor->contractsNotSignedYet()->count();
        $contractsDoneButNotSofaYet = $ProjectProgressProcessor->contractsDoneButNotSofaYet()->count();
        $contractsNotAwardYet = $ProjectProgressProcessor->contractsNotAwardYet()->count();
        
        
        //$sst2 = $ProjectProgressProcessor->contractsOnGoing(); 
            //return null;
        //$sstIdArray2 = $sst2->pluck('id');
        
        //dd($sstIdArray2);
         
        return view('contract.post.monitoring.index',
           compact('contractsSixMonthToEnd','contractsOnGoing',
                   'contractsNotSignedYet','contractsDoneButNotSofaYet',
                   'contractsNotAwardYet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $JabatanPelaksana = Department::get();
        
      
        
        
        $sst = \App\Models\Acquisition\Sst::query()
            ->withDetails()
            ->withIpcdetails()
            ->withCertificates()
            // ->withVariationOrders()
            ->withEots()
            ->findByHashSlug($id);

        /**
         * Charts.
         */
        $chart            = MonitoringChartService::make($sst)->handle();
        $site_visit_chart = SiteVisitChartService::make($sst)->handle();
        $progress_chart = ContractProgressChartService::make($sst)->handle();

        /**
         * SST.
         */
        $approval  = $sst->acquisition->approval;
        
        //TODO -  
        //$appointed = AppointedCompany::where('company_id', $sst->company_id)->first();
        //Modified Query need to add acquisition Id
        $appointed = AppointedCompany::where([['acquisition_id', '=', $sst->acquisition_id],['company_id', $sst->company_id]])->first();
        
        /**
         * eot.
         */
        $new_eot_date = null;
        if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
            $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
        }
        
       // dd($sst->company_id);

        $sale = Sale::where([
                    //['company_id', '=', $appointed->company_id], // add $sst->company_id
                    ['company_id', '=', $sst->company_id],
                    ['acquisition_id', '=', $sst->acquisition_id],
                ])->first();
        
        
        
        //CIDB
        if($approval->acquisition_type_id == 1 || $approval->acquisition_type_id == 3) {
            $cidb = common()->getKelayakanMOFView($approval->id);
        }else{
            $cidb = common()->getKelayakanCIDBView($approval->id);
        }
        
        
        //comment yang lama $cidb = $approval->cidbQualifications;
       // $mof  = MOF::findByHashSlug($appointed->company->hashslug);
        $mof  = MOF::findByHashSlug($sst->company->hashslug);

        /**
         * Post Contract - Document.
         */
        $bon       = $sst->bon;
        $insurance = $sst->insurance;
        $deposit   = $sst->document;
        $document  = $sst->document;
        $BQElemen  = Element::where('acquisition_id', $sst->acquisition_id)->get();
        $BQItem    = Item::where('acquisition_id', $sst->acquisition_id)->get();
        $BQSub     = SubItem::where('acquisition_id', $sst->acquisition_id)->get();

        /**
         * Post Contract - IPC.
         */
        $ipc = $sst->ipc;
        
        //add Contract Progress
        $contractProgresses =  \App\Models\Acquisition\Monitoring\ContractProgress::where('acquisition_id', $sst->acquisition->id)
                ->orderBy('on_plan_date','ASC')
                ->get();
        
        
        //add value for perinci
         $totalPaid = null;
        foreach ($ipc as $ipcx) {          
            if ($ipcx->status == 4) {
//                foreach ($ipc->ipcInvoice as $ipcInvoice) {
//                    foreach ($ipcInvoice->ipcBq as $ipcBq) {
//                        if(!is_null($ipcBq->adjust_amount)){
//                           $this->finance[] = money()->toCommon($ipcBq->adjust_amount);
//                        }
//                    }
//                }
                
                //column demand amount $given_adv = $give_adv - $return_adv;
              $totalPaid = $totalPaid + $ipcx->demand_amount;
                      
            }        
        }
        
        //add contract value - get from Appointed_company , but now just support 0ne to one
        $contractValue = 50000;
        
        
        //Add PPJHK
        $ppjhk_count = \App\Models\VO\PPJHK\Ppjhk::where('sst_id', $sst->id)->count();
        $ppjhk = \App\Models\VO\PPJHK\Ppjhk::where('sst_id', $sst->id)->get();
        
        //dd($ppjhk);appointed_company - JUST FIX BUGS, why this variable in the view file ? need to investigate
        $appointed_company = $appointed;
        
        
       //this code for get view of extension 
       //$appointed_company = AppointedCompany::withDetails()->findByHashSlug($request->hashslug);
       //return view('contract.post.extension.show_list.index', compact('appointed_company'));
        
        $reportType = ReportType::where([
            ['acquisition_type_id', '=', $appointed->acquisition->approval->acquisition_type_id],
            ])->pluck('name', 'id');
        
        return view('contract.post.monitoring.show', compact(
            'sst', 'chart',
            'approval', 'sale', 'appointed', 'cidb', 'mof', // sst
            'bon', 'insurance', 'BQElemen', 'BQItem', 'BQSub', 'deposit', 'document', // document
            'ipc', 'site_visit_chart','contractProgresses','progress_chart','totalPaid','contractValue'
                ,'ppjhk','ppjhk_count','appointed_company','new_eot_date','JabatanPelaksana','reportType'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sst = \App\Models\Acquisition\Sst::withDetails()->with('activities')->findByHashSlug($id);

        return view('contract.post.monitoring.edit', compact('sst'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @todo Handle Validation
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        if($request->upload_type == 'progress_chart'){ 
           // try{
            $this->updateprogresschart($request, $id);  

            $sst = \App\Models\Acquisition\Sst::withDetails()->with('activities')->findByHashSlug($id);

            $workflow = WorkflowTask::where('acquisition_id',$sst->acquisition_id)->where('workflow_team_id',1)->first();
            $workflow->flow_desc = 'Pemantauan Projek : Sila teruskan dengan Lawatan Penilaian Tapak.';
            $workflow->flow_location = 'Pemantauan Projek';
            $workflow->role_id = 4;
            $workflow->url = '/acquisition/monitoring/'.$sst->hashslug.'#site-visit';
            $workflow->update();

            swal()->success('Pemantauan Projek', 'Maklumat pemantauan telah berjaya dikemaskini.', []);
            return redirect()->route('acquisition.monitoring.show', $id);
            //}
            //catch (\Exception $e) {
                //swal()->success($e->getMessage(), []);
            //}            
        }else{
        
        
        
        
        // $this->validate($request, [
        //     'activity' => 'required|mimes:csv'
        // ], [
        //     'activity.required' => 'Fail activiti projek diperlukan.',
        //     'activity.mime' => 'Format fail activiti projek hendaklah dalam bentuk Microsoft Excel (XLSX).',
        // ]);

        $sst = \App\Models\Acquisition\Sst::withDetails()->with('activities')->findByHashSlug($id);

        if ($request->hasFile('activity') && $request->file('activity')->isValid()) {
            \App\Models\Acquisition\Activity::query()
                ->where('acquisition_id', $sst->acquisition_id)
                ->delete();
            $acquisition = $sst->acquisition;

            $path = $acquisition->addDocument($request, 'activity')->getPath();

            // process the excel file
            \Excel::filter('chunk')
                ->load($path)
                ->chunk(20, function ($activities) use ($sst) {
                    $json = str_replace(
                        ['\u00a0'],
                        [''],
                        json_encode($activities->toArray()));
                    $activities = json_decode($json);

                    foreach ($activities as $activity) {
                        
                        
                        if($activity->scheduled_started_at == null){
                            $scheduled_started_at = null;
                            $scheduled_ended_at = null;
                        }else{
                            $scheduled_started_at = \Carbon\Carbon::parse(str_replace('/', '-', $activity->scheduled_started_at));
                            $scheduled_ended_at = \Carbon\Carbon::parse(str_replace('/', '-', $activity->scheduled_ended_at));
                        }
                        
                        
                        \App\Models\Acquisition\Activity::create([
                            'acquisition_approval_id' => $sst->acquisition->acquisition_approval_id,
                            'acquisition_id'          => $sst->acquisition_id,
                            'company_id'              => $sst->company_id,
                            'sst_id'                  => $sst->id,
                            'parent_id'               => null,
                            'name'                    => $activity->name,
                            // 'no'                      => $activity->no,
                            //'scheduled_started_at' => \Carbon\Carbon::parse(str_replace('/', '-', $activity->scheduled_started_at)),
                            //'scheduled_ended_at'   => \Carbon\Carbon::parse(str_replace('/', '-', $activity->scheduled_ended_at)),
                            
                            'scheduled_started_at' => $scheduled_started_at,
                            'scheduled_ended_at'   => $scheduled_ended_at,
                            
                            /* delete , just need upload start_date and end_date only
                            'scheduled_duration'   => $activity->scheduled_duration,
                            'scheduled_completion' => $activity->scheduled_completion,
                            'current_started_at'   => \Carbon\Carbon::parse(str_replace('/', '-', $activity->current_started_at)),
                            'current_ended_at'     => \Carbon\Carbon::parse(str_replace('/', '-', $activity->current_started_at)),
                            'current_duration'     => $activity->current_duration,
                            'current_completion'   => $activity->current_completion,
                            'actual_started_at'    => \Carbon\Carbon::parse(str_replace('/', '-', $activity->actual_started_at)),
                            'actual_ended_at'      => \Carbon\Carbon::parse(str_replace('/', '-', $activity->actual_started_at)),
                            'actual_duration'      => $activity->actual_duration,
                            'actual_completion'    => $activity->actual_completion,*/
                        ]);
                    }
                }, 'UTF-8');

            audit($sst, __('Dokumen aktiviti projek bagi perolehan ' .$sst->acquisition->reference. ' : ' .$sst->acquisition->title. ' telah berjaya dimuatnaik.'));
        }

        swal()->success('Pemantauan Projek', 'Maklumat pemantauan telah beryaya dikemaskini.', []);

        return redirect()->route('acquisition.monitoring.show', $id);
        }
    }
    
    
    
    
    //Add function update for progress_chart , function name: updateprogresschart
    public function updateprogresschart(Request $request,$id)
    {
        // $this->validate($request, [
        //     'activity' => 'required|mimes:csv'
        // ], [
        //     'activity.required' => 'Fail activiti projek diperlukan.',
        //     'activity.mime' => 'Format fail activiti projek hendaklah dalam bentuk Microsoft Excel (XLSX).',
        // ]);
        
        
        $sst = \App\Models\Acquisition\Sst::findByHashSlug($id);     
        $acquisitionId = $sst->acquisition;    
        //$contractProgresses = \App\Models\Acquisition\Monitoring\ContractProgress::where('acquisition_id', $acquisitionId->id);

        if ($request->hasFile('progress_chart') && $request->file('progress_chart')->isValid()) {
            
            //DIN : comment delete , add EOT just add
            /*
            \App\Models\Acquisition\Monitoring\ContractProgress::query()
                ->where('acquisition_id', $acquisitionId->id)
                ->delete();
            
            \App\Models\Acquisition\Monitoring\PaymentMonitor::query()
                ->where('acquisition_id', $acquisitionId->id)
                ->delete();
            
            \App\Models\Acquisition\Monitoring\SiteVisit::query()
                ->where('acquisition_id', $acquisitionId->id)
                ->delete();
             * 
             * */
             
            //$acquisition = \App\Models\Acquisition\Acquisition::where('id', $acquisitionId);

            $path = $acquisitionId->addDocument($request, 'progress_chart')->getPath();

            // process the excel file
            \Excel::filter('chunk')
                ->load($path)
                ->chunk(20, function ($contractProgresses) use ($sst) {
                    $json = str_replace(
                        ['\u00a0'],
                        [''],
                        json_encode($contractProgresses->toArray()));
                    $contractProgresses = json_decode($json);

                    foreach ($contractProgresses as $obj) {
                             
                        //add if date has same with database , ignore
                        ////query->where('slug', $slug);
                       // \App\Models\Acquisition\Monitoring\ContractProgress::findBy
                        $isHave = \App\Models\Acquisition\Monitoring\ContractProgress::where('acquisition_id', $sst->acquisition->id)
                                ->where('on_plan_date', \Carbon\Carbon::parse(str_replace('/', '-', $obj->on_plan_date)))
                                ->get();
                        //dd(\Carbon\Carbon::parse(str_replace('/', '-', $obj->on_plan_date)));
                        
                       
                        if(count($isHave) == 0){
                        //dd('here');
                        \App\Models\Acquisition\Monitoring\ContractProgress::create([
                            'acquisition_id'         => $sst->acquisition->id,
                            'on_plan_date'           => \Carbon\Carbon::parse(str_replace('/', '-', $obj->on_plan_date)),
                            'on_plan_date_percent'   => $obj->on_plan_percent,
                            //'on_site_payment_percent'   => null,
                            'site_visit_id'   => null,
                            //'payment_monitor_id' => null,
                            //'on_plan_payment_percent'     => $obj->on_plan_payment_percent,
                        ]);
                        }else{
                            //dd('dah ada');
                        }
                    }
                }, 'UTF-8');

            audit($sst, __('Dokumen Jadual Carta Kemajuan Fizikal projek bagi perolehan ' .$sst->acquisition->reference. ' : ' .$sst->acquisition->title. ' telah berjaya dimuatnaik.'));
        }

        
    }
    
    
    
    

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
