<?php

namespace App\Http\Controllers\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Bon;
use App\Models\Acquisition\Insurance;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\Review;
use App\Models\EmailScheduler;
use App\Models\ReviewLog;
use App\Models\User;
use App\Models\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;
use App\Models\WorkflowTask;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Warning;
use App\Models\Acquisition\Termination;
use App\Models\VO\PPJHK\Ppjhk;
use App\Models\VO\VO;

class ReviewController extends Controller
{
        // Penanda Semakan
        // S1 = Semakan Penyemak JP
        // S2 = Semakan Pengesah JP
        // N = Penyerahan Tugasan
        // S3 = Semakan Penyemak 1 BPUB
        // S4 = Semakan Penyemak 2 BPUB
        // S5 = Semakan Pengesah BPUB
        // S6 = Semakan Penyemak Undang-undang
        // S7 = Semakan Pengesah Undang-undang
        // S8 = Semakan Penyemak Kewangan
        // S9 = Semakan Pengesah Kewangan

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review = Review::where('status', 'N')->latest()->get();
        $lawreview = Review::where('status', 'S6')->latest()->get();
        $user = DB::table('users')->select('users.id as user_id','users.name as user_name','grades.name as grade')->join('grades','grades.id','=','users.grade_id')->where('users.executor_department_id', '25')->where('grades.name','>=', '29')->where('grades.name','<=','36')->get();
        $lawuser   = User::where('department_id', '3')->where('id','!=',user()->id)->get();
        // return $review;

        return view('contract.pre.review.index', compact('review', 'user','lawreview','lawuser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $review  = new Review();
        $review2 = new Review();

        $reviewlog = new ReviewLog();

        $email   = new EmailScheduler();
        $review3 = new Review();
        // $review4 = new Review();
        // $review5 = new Review();

        $acq = Acquisition::find($request->input('acquisition_id'));

        $sst = $acq->sst;

        $company = AppointedCompany::where('acquisition_id', $acq->id)->first();

        if ('EOT' == $request->type) {
            $cnt_review = Review::where('eot_id', $request->input('eot_id'))->where('type', 'EOT')->orderby('id', 'desc')->first();

            $pp_null = Review::where('eot_id', $request->input('eot_id'))->where('type', 'EOT')->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

            $bpub_null = Review::where('eot_id', $request->input('eot_id'))->where('type', 'EOT')->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

            $uu_null = Review::where('eot_id', $request->input('eot_id'))->where('type', 'EOT')->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

            $bpub = Review::where('eot_id', $request->input('eot_id'))->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

            $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

            $pp = Review::where('eot_id', $request->input('eot_id'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

            $ppn = Review::where('eot_id', $request->input('eot_id'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();

            $pps5 = Review::where('eot_id', $request->input('eot_id'))->where('department', config('enums.BPUB'))->where('status','S5')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            $pps6 = Review::where('eot_id', $request->input('eot_id'))->where('department', config('enums.UU'))->where('status','S6')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            $pps7 = Review::where('eot_id', $request->input('eot_id'))->where('department', config('enums.UU'))->where('status','S7')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            // if(!empty($cnt_review)){
            //     if($cnt_review->department == 'BPUB'){
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('eot_id', $request->input('eot_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
            //     } else {
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('eot_id', $request->input('eot_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
            //     }
            // }

            $reviewlog1 = new ReviewLog();

            $review->eot_id    = $request->input('eot_id');
            $reviewlog->eot_id = $request->input('eot_id');
            $reviewlog1->eot_id = $request->input('eot_id');
        } elseif ('IPC' == $request->type) {
            $cnt_review = Review::where('ipc_id', $request->input('ipc_id'))->where('type', 'IPC')->orderby('id', 'desc')->first();

            $pp_null = Review::where('ipc_id', $request->input('ipc_id'))->where('type', 'IPC')->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

            $bpub_null = Review::where('ipc_id', $request->input('ipc_id'))->where('type', 'IPC')->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

            $uu_null = Review::where('ipc_id', $request->input('ipc_id'))->where('type', 'IPC')->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

            $bpub = Review::where('ipc_id', $request->input('ipc_id'))->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

            $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

            $pp = Review::where('ipc_id', $request->input('ipc_id'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

            $ppn = Review::where('ipc_id', $request->input('ipc_id'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();

            $pps5 = Review::where('ipc_id', $request->input('ipc_id'))->where('department', config('enums.BPUB'))->where('status','S5')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            $pps6 = Review::where('ipc_id', $request->input('ipc_id'))->where('department', config('enums.UU'))->where('status','S6')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            $pps7 = Review::where('ipc_id', $request->input('ipc_id'))->where('department', config('enums.UU'))->where('status','S7')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            // if(!empty($cnt_review)){
            //     if($cnt_review->department == 'BPUB'){
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('ipc_id', $request->input('ipc_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
            //     } else {
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('ipc_id', $request->input('ipc_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
            //     }
            // }

            $reviewlog1 = new ReviewLog();

            $review->ipc_id    = $request->input('ipc_id');
            $reviewlog->ipc_id = $request->input('ipc_id');
            $reviewlog1->ipc_id = $request->input('ipc_id');
        } elseif ('VO' == $request->type) {
            if('PPK' == $request->document_contract_type){

                $cnt_review = Review::where('ppk_id', $request->input('ppk_id'))->where('type', 'VO')->where('document_contract_type', 'PPK')->orderby('id', 'desc')->first();

                $pp_null = Review::where('ppk_id', $request->input('ppk_id'))->where('type', 'VO')->where('document_contract_type', null)->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $bpub_null = Review::where('ppk_id', $request->input('ppk_id'))->where('type', 'VO')->where('document_contract_type', null)->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

                $uu_null = Review::where('ppk_id', $request->input('ppk_id'))->where('type', 'VO')->where('document_contract_type', null)->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

                $bpub = Review::where('ppk_id', $request->input('ppk_id'))->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

                $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

                $pp = Review::where('ppk_id', $request->input('ppk_id'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $ppn = Review::where('ppk_id', $request->input('ppk_id'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();

                $pps5 = Review::where('ppk_id', $request->input('ppk_id'))->where('department', config('enums.BPUB'))->where('status','S5')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

                $pps6 = Review::where('ppk_id', $request->input('ppk_id'))->where('department', config('enums.UU'))->where('status','S6')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

                $pps7 = Review::where('ppk_id', $request->input('ppk_id'))->where('department', config('enums.UU'))->where('status','S7')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

                // if(!empty($cnt_review)){
                //     if($cnt_review->department == 'BPUB'){
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('ppk_id', $request->input('ppk_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
                //     } else {
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('ppk_id', $request->input('ppk_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
                //     }
                // }

                $reviewlog1 = new ReviewLog();

                $review->ppk_id    = $request->input('ppk_id');
                $reviewlog->ppk_id = $request->input('ppk_id');
                $reviewlog1->ppk_id = $request->input('ppk_id');

            } elseif('PPJHK' == $request->document_contract_type){

                $cnt_review = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('type', 'VO')->where('document_contract_type', 'PPJHK')->orderby('id', 'desc')->first();

                $pp_null = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('type', 'VO')->where('document_contract_type', null)->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $bpub_null = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('type', 'VO')->where('document_contract_type', null)->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

                $uu_null = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('type', 'VO')->where('document_contract_type', null)->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

                $bpub = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

                $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

                $pp = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $ppn = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();

                $pps5 = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('department', config('enums.BPUB'))->where('status','S5')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

                $pps6 = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('department', config('enums.UU'))->where('status','S6')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

                $pps7 = Review::where('ppjhk_id', $request->input('ppjhk_id'))->where('department', config('enums.UU'))->where('status','S7')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

                // if(!empty($cnt_review)){
                //     if($cnt_review->department == 'BPUB'){
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('ppjhk_id', $request->input('ppjhk_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
                //     } else {
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('ppjhk_id', $request->input('ppjhk_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
                //     }
                // }

                $reviewlog1 = new ReviewLog();

                $review->ppjhk_id    = $request->input('ppjhk_id');
                $reviewlog->ppjhk_id = $request->input('ppjhk_id');
                $reviewlog1->ppjhk_id = $request->input('ppjhk_id');

            }
            
        } elseif ('Penamatan' == $request->type) {
            if('Amaran' == $request->document_contract_type){
                $cnt_review = Review::where('warning_id', $request->input('warning_id'))->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->orderby('id', 'desc')->first();

                $pp_null = Review::where('warning_id', $request->input('warning_id'))->where('type', 'Penamatan')->where('document_contract_type', 'Amaran')->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $pp = Review::where('warning_id', $request->input('warning_id'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $ppn = Review::where('warning_id', $request->input('warning_id'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();
                
                // if(!empty($cnt_review)){
                //     if($cnt_review->department == 'BPUB'){
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('warning_id', $request->input('warning_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
                //     } else {
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('warning_id', $request->input('warning_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
                //     }
                // }

                $reviewlog1 = new ReviewLog();

                $review->warning_id    = $request->input('warning_id');
                $reviewlog->warning_id = $request->input('warning_id');
                $reviewlog1->warning_id = $request->input('warning_id');
            } else {
                $cnt_review = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->orderby('id', 'desc')->first();

                $pp_null = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('department', config('enums.PP'))->where('type', $request->input('type'))->where('document_contract_type', null)->orderby('id', 'desc')->first();

                $pp = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

                $ppn = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();

                // if(!empty($cnt_review)){
                //     if($cnt_review->department == 'BPUB'){
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
                //     } else {
                //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
                //     }
                // }

                $reviewlog1 = new ReviewLog();

                $review->warning_id    = null;
                $reviewlog->warning_id = null;
                $reviewlog1->warning_id = null;
            }
            
        } else {
            $cnt_review = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->orderby('id', 'desc')->first();

            $pp_null = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('department', config('enums.PP'))->where('type', $request->input('type'))->where('document_contract_type', null)->orderby('id', 'desc')->first();

            $bpub_null = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('department', config('enums.BPUB'))->where('type', $request->input('type'))->where('document_contract_type', null)->orderby('id', 'desc')->first();

            $uu_null = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('department', config('enums.UU'))->where('type', $request->input('type'))->where('document_contract_type', null)->orderby('id', 'desc')->first();

            $bpub = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

            $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

            $pp = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

            $ppn = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.PP'))->where('status',null)->orderby('id', 'desc')->first();

            $pps5 = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.BPUB'))->where('status','S5')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            $pps6 = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.UU'))->where('status','S6')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            $pps7 = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', $request->input('type'))->where('document_contract_type', $request->input('document_contract_type'))->where('department', config('enums.UU'))->where('status','S7')->where('reviews_status','Teratur')->orderby('id', 'desc')->first();

            // if(!empty($cnt_review)){
            //     if($cnt_review->department == 'BPUB'){
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('reviews_status','Terima')->where('department', config('enums.BPUB'))->delete();
            //     } elseif($cnt_review->department == 'UNDANG2'){
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('reviews_status','Terima')->where('department', config('enums.UU'))->delete();
            //     } else {
            //         ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('reviews_status','Terima')->where('department', config('enums.PP'))->delete();
            //     }
            // }
            

            $reviewlog1 = new ReviewLog();
            $reviewlog2 = new ReviewLog();
        
            $reviewlog2->acquisition_id = $request->input('acquisition_id');
            $reviewlog2->requested_by   = user()->supervisor->id;
            $reviewlog2->requested_at   = Carbon::now();
            $reviewlog2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
            $reviewlog2->remarks        = null;
            $reviewlog2->task_by        = $request->input('task_by');
            $reviewlog2->created_by     = $request->input('created_by');
            $reviewlog2->type           = $request->input('type');

            $review->eot_id    = null;
            $reviewlog->eot_id = null;
            $reviewlog1->eot_id = null;
            $reviewlog2->eot_id = null;
        }

        $acq_review = Review::where('acquisition_id', $request->input('acquisition_id'))->where('type', 'Acquisitions')->orderby('id', 'desc')->first();

        $bon  = Review::where('acquisition_id', $request->input('acquisition_id'))->where('document_contract_type', 'Bon Pelaksanaan')->orderby('id', 'desc')->first();
        $ins  = Review::where('acquisition_id', $request->input('acquisition_id'))->where('document_contract_type', 'Insurans')->orderby('id', 'desc')->first();
        $wang = Review::where('acquisition_id', $request->input('acquisition_id'))->where('document_contract_type', 'Wang Pendahuluan')->orderby('id', 'desc')->first();
        $doc  = Review::where('acquisition_id', $request->input('acquisition_id'))->where('document_contract_type', 'Dokumen Kontrak')->orderby('id', 'desc')->first();

        $review->acquisition_id = $request->input('acquisition_id');
        $review->requested_by   = $request->input('requested_by');
        $review->requested_at   = Carbon::now();
        $review->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
        $review->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
        $review->task_by        = $request->input('task_by');
        $review->law_task_by        = $request->input('law_task_by');
        $review->created_by     = $request->input('created_by');
        $review->type           = $request->input('type');

        $reviewlog->acquisition_id = $request->input('acquisition_id');
        $reviewlog->requested_by   = $request->input('requested_by');
        $reviewlog->requested_at   = Carbon::now();
        $reviewlog->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
        $reviewlog->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
        $reviewlog->task_by        = $request->input('task_by');
        $reviewlog->created_by     = $request->input('created_by');
        $reviewlog->type           = $request->input('type');

        $reviewlog1->acquisition_id = $request->input('acquisition_id');
        $reviewlog1->requested_by   = user()->supervisor->id;
        $reviewlog1->requested_at   = Carbon::now();
        $reviewlog1->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
        $reviewlog1->remarks        = null;
        $reviewlog1->task_by        = $request->input('task_by');
        $reviewlog1->created_by     = $request->input('created_by');
        $reviewlog1->type           = $request->input('type');

        // if ('Draft Penyedia' == $acq->status_task || 'Semakan Dokumen Perolehan' == $acq->status_task) {
        //     $review->type = 'Acquisitions';
        // } elseif ('SST' == $acq->status_task || 'Cetak Notis' == $acq->status_task) {
        //     $review->type = 'SST';
        // } elseif ('Dokumen Kontrak' == $acq->status_task) {
        //     $review->type                   = 'Dokumen Kontrak';
        //     $review->document_contract_type = $request->input('document_contract_type');
        // } elseif ('Pemantauan' == $acq->status_task) {
        //     $review->type = 'Pemantauan';
        // } elseif ('IPC' == $acq->status_task) {
        //     $review->type = 'IPC';
        // } elseif ('VO' == $acq->status_task) {
        //     $review->type = 'VO';
        // } elseif ('EOT' == $acq->status_task) {
        //     $review->type = 'EOT';
        // } elseif ('SOFA' == $acq->status_task) {
        //     $review->type = 'SOFA';
        // }
        
        // if (! empty($workflow2)) {
        //     $workflow2->acquisition_id = $acq->id;
        //     $workflow2->flow_name = $acq->reference . ' : ' . $acq->title;
        // }


        if ('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type) {
            $review->document_contract_type    = $request->input('document_contract_type');
            $reviewlog->document_contract_type = $request->input('document_contract_type');
            $reviewlog1->document_contract_type = $request->input('document_contract_type');

            if('Bon Pelaksanaan' == $request->document_contract_type){

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Bon Pelaksanaan')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';

                $bons = Bon::where('sst_id',$acq->sst->id)->first()->update([
                    'bank_approval_received_date' => $request->input('bank_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_approval_received_date')) : null,
                    'bank_approval_proposed_date'          => $request->input('bank_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_approval_proposed_date')) : null,
                    'insurance_approval_received_date' => $request->input('insurance_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_approval_received_date')) : null,
                    'insurance_approval_proposed_date'          => $request->input('insurance_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_approval_proposed_date')) : null,
                ]);
            } elseif('Insurans' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';

            } elseif('Dokumen Kontrak' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';

            } elseif('Wang Pendahuluan' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Wang Pendahuluan')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';

            } elseif('CPC' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','CPC')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';

            } elseif('CNC' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','CNC')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';

            } elseif('CPO' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','CPO')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';

            } elseif('CMGD' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','CMGD')->first();

                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';

            } elseif('Amaran' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Amaran')->first();
                $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();
                $workflow->acquisition_id = $acq->id;
                $workflow->warning_id = $warn->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/termination/warning/'.$warn->hashslug;

            } elseif('Tujuan' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Tujuan')->first();
                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/termination/termination/'.$acq->sst->hashslug.'/edit#reason';
                $warn = Termination::where('sst_id',$acq->sst->id)->where('notice_type',1)->first();

            } elseif('Notis' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Notis')->first();
                $workflow->acquisition_id = $acq->id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/termination/termination/'.$acq->sst->hashslug.'/edit#terminate';
                $warn = Termination::where('sst_id',$acq->sst->id)->where('notice_type',2)->first();

            }

            // $insurance = Insurance::where('sst_id',$acq->sst->id)->first()->update([
            //     'public_approval_received_date' => $request->input('public_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_approval_received_date')) : null,
            //     'public_approval_proposed_date'          => $request->input('public_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_approval_proposed_date')) : null,

            //     'work_approval_received_date' => $request->input('work_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_approval_received_date')) : null,
            //     'work_approval_proposed_date'          => $request->input('work_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_approval_proposed_date')) : null,

            //     'compensation_approval_received_date' => $request->input('compensation_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_approval_received_date')) : null,
            //     'compensation_approval_proposed_date'          => $request->input('compensation_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_approval_proposed_date')) : null,
            // ]);
        } elseif('EOT' == $request->type){

            $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('eot_id', $request->eot_id)->first();
            $eot = EOT::find($request->eot_id);
            $workflow->url = '/extension/extension_show/'.$eot->hashslug.'#eot-review';
            $workflow->acquisition_id = $acq->id;
            $workflow->eot_id = $request->eot_id;
            $workflow->flow_name = $acq->reference . ' : ' . $acq->title;

            $review->document_contract_type    = null;
            $reviewlog->document_contract_type = null;
            $reviewlog1->document_contract_type = null;

        } elseif('IPC' == $request->type){

            $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('ipc_id', $request->ipc_id)->first();
            $ipc = IPC::find($request->ipc_id);
            $workflow->url = '/post/ipc/'.$ipc->hashslug.'/ipc_show#ipc-review';
            $workflow->acquisition_id = $acq->id;
            $workflow->ipc_id = $request->ipc_id;
            $workflow->flow_name = $acq->reference . ' : ' . $acq->title;

            $review->document_contract_type    = null;
            $reviewlog->document_contract_type = null;
            $reviewlog1->document_contract_type = null;

        } elseif('VO' == $request->type){

            $review->document_contract_type    = $request->input('document_contract_type');
            $reviewlog->document_contract_type = $request->input('document_contract_type');
            $reviewlog1->document_contract_type = $request->input('document_contract_type');

            if('PPK' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('ppk_id', $request->ppk_id)->where('flow_location','PPK')->first();

                $ppk = VO::find($request->ppk_id);
                $workflow->acquisition_id = $acq->id;
                $workflow->ppk_id = $request->ppk_id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/post/variation-order/'.$ppk->hashslug.'/ppk_show#document-review';

            } elseif('PPJHK' == $request->document_contract_type) {

                $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('ppjhk_id', $request->ppjhk_id)->where('flow_location','PPJHK')->first();

                $ppjhk = Ppjhk::find($request->ppjhk_id);
                $workflow->acquisition_id = $acq->id;
                $workflow->ppjhk_id = $request->ppjhk_id;
                $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
                $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'#document-review';

            }

        } else {

            $workflow = WorkflowTask::where('acquisition_id', $acq->id)->first();

            $workflow->acquisition_id = $acq->id;
            $workflow->flow_name = $acq->reference . ' : ' . $acq->title;

            $review->document_contract_type    = null;
            $reviewlog->document_contract_type = null;
            $reviewlog1->document_contract_type = null;
            $reviewlog2->document_contract_type = null;

            if('Acquisitions' == $request->type){
                $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
            } elseif('SST' == $request->type){
                $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
            } elseif('SOFA' == $request->type){
                $workflow->url = '/post/sofa/'.$acq->sst->hashslug.'/edit#sst';
            }
        }

        if (1 == $request->sentStatus) {
            // function hantar mula

            $review->reviews_status    = 'Teratur';
            $review2->reviews_status   = 'Teratur';
            $reviewlog->reviews_status = 'Hantar';
            $reviewlog1->reviews_status = 'Terima';
            if(!empty($reviewlog2->acquisition_id)){

                $reviewlog2->reviews_status = 'Terima';

            }

            if('Acquisitions' == $request->type){
                $acq->status_task = config('enums.flow2');
                $acq->update();
            } elseif('SST' == $request->type){
                $acq->status_task = config('enums.flow22');
                $acq->update();

                $sst->status = 1;
                $sst->update();
            } elseif('Dokumen Kontrak' == $request->type){
                if('CPC' == $request->document_contract_type){
                    $acq->status_task = config('enums.flow29');
                    $acq->update();
                } elseif('CNC' == $request->document_contract_type){
                    $acq->status_task = config('enums.flow30');
                    $acq->update();
                } elseif('CPO' == $request->document_contract_type){
                    $acq->status_task = config('enums.flow31');
                    $acq->update();
                } elseif('CMGD' == $request->document_contract_type){
                    $acq->status_task = config('enums.flow32');
                    $acq->update();
                } else {
                    $acq->status_task = config('enums.flow23');
                    $acq->update();
                }
            } elseif('IPC' == $request->type){
                $acq->status_task = config('enums.flow24');
                $acq->update();
            } elseif('EOT' == $request->type){
                $acq->status_task = config('enums.flow25');
                $acq->update();
            } elseif('VO' == $request->type){
                $acq->status_task = config('enums.flow26');
                $acq->update();
            } elseif('Penamatan' == $request->type){
                $acq->status_task = config('enums.flow27');
                $acq->update();
            } elseif('SOFA' == $request->type){
                $acq->status_task = config('enums.flow28');
                $acq->update();
            }

            if (empty($cnt_review)) {
                // if ('EOT' == $request->input('type')) {
                //     $review->progress    = $request->input('progress');
                //     $review->status      = $request->input('status');
                //     $review->approved_by = $request->input('approved_by');
                //     $review->department  = $request->input('department');
                //     $review->reviews_status = '';
                    
                //     $reviewlog->progress    = $request->input('progress');
                //     $reviewlog->status      = $request->input('status');
                //     $reviewlog->approved_by = $request->input('approved_by');
                //     $reviewlog->department  = $request->input('department');
                //     $reviewlog->remarks  = 'Kontrak Dihantar Kepada ' . $review->approve->name;
                //     $reviewlog->reviews_status = '';

                //     $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                //     $workflow->flow_location = $request->input('status');
                //     $workflow->is_claimed = 1;
                //     $workflow->user_id = $request->input('approved_by');
                //     $workflow->workflow_team_id = 1;

                // } else

                if ('IPC' == $request->input('type')) {
                    // if (1 == $request->semak1) {
                        
                        $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                        // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                        $ipc = Ipc::find($request->input('ipc_id'));
                        if(empty($rev_ipc) || (!empty($rev_ipc) && $ipc->status == 1)){
                            $ipc->status = 2;
                            $ipc->update();
                            IpcInvoice::where('ipc_id',$ipc->id)->update([
                                'status'       => 2,
                            ]);
                        }
                        
                        $review2->status         = 'S1';
                        $review2->progress       = 0;
                        $review2->department     = config('enums.BPUB');
                        $review2->acquisition_id = $request->input('acquisition_id');
                        $review2->ipc_id = $request->input('ipc_id');
                        $review2->requested_by   = $request->input('requested_by');
                        $review2->requested_at   = Carbon::now();
                        $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        $review2->remarks        = $acq_review->remarks;
                        $review2->approved_by    = $acq_review->task_by;
                        $review2->task_by        = $acq_review->task_by;
                        $review2->law_task_by    = $acq_review->law_task_by;
                        $review2->type           = $request->input('type');
                        $review2->created_by     = $acq_review->created_by;

                        $review->status = 'S1';
                        $review->progress    = 1;
                        $review->task_by        = $acq_review->task_by;
                        $review->law_task_by    = $acq_review->law_task_by;
                        $review->approved_by = user()->supervisor->id;
                        $review->department     = config('enums.PP');

                        $reviewlog->status = 'S1';
                        $reviewlog->approved_by = user()->supervisor->id;
                        $reviewlog->remarks = 'Kontrak Dihantar Kepada ' . $review->approve->name;

                        $reviewlog1->status = 'S1';
                        $reviewlog1->department     = config('enums.PP');
                        $reviewlog1->approved_by = user()->supervisor->id;

                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.';
                        $workflow->flow_location = 'S1';
                        $workflow->is_claimed = 1;
                        $workflow->user_id = user()->supervisor->id;
                        $workflow->role_id = 5;
                        $workflow->workflow_team_id = 1;

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    // } elseif (1 == $request->semak2) {
                    //     $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                    //     // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                    //     $ipc = Ipc::find($request->input('ipc_id'));
                    //     if(empty($rev_ipc) || (!empty($rev_ipc) && $ipc->status == 1)){
                            
                    //         $ipc->status = 2;
                    //         $ipc->update();
                    //         IpcInvoice::where('ipc_id',$ipc->id)->update([
                    //             'status'       => 2,
                    //         ]);
                    //     }
                        
                    //     $review2->status         = 'S2';
                    //     $review2->progress       = 0;
                    //     $review2->department     = config('enums.BPUB');
                    //     $review2->acquisition_id = $request->input('acquisition_id');
                    //     $review2->ipc_id = $request->input('ipc_id');
                    //     $review2->requested_by   = $request->input('requested_by');
                    //     $review2->requested_at   = Carbon::now();
                    //     $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                    //     $review2->remarks        = $acq_review->remarks;
                    //     $review2->approved_by    = $acq_review->task_by;
                    //     $review2->task_by        = $acq_review->task_by;
                    //     $review2->law_task_by    = $acq_review->law_task_by;
                    //     $review2->type           = $request->input('type');
                    //     $review2->created_by     = $acq_review->created_by;

                    //     $review->status      = 'S2';
                    //     $review->progress    = 1;
                    //     $review->task_by        = $acq_review->task_by;
                    //     $review->law_task_by    = $acq_review->law_task_by;
                    //     $review->approved_by = user()->supervisor->supervisor->id;
                    //     $review->department     = config('enums.PP');

                    //     $reviewlog->status      = 'S2';
                    //     $reviewlog->progress       = 1;
                    //     $reviewlog->approved_by = user()->supervisor->supervisor->id;
                    //     $reviewlog->remarks = 'Kontrak Dihantar Kepada ' . $review->approve->name;

                    //     $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
                    //     $workflow->flow_location = 'S2';
                    //     $workflow->is_claimed = 1;
                    //     $workflow->user_id = user()->supervisor->supervisor->id;
                    //     $workflow->workflow_team_id = 1;

                    //     if (! empty($review->approved_by)) {
                    //         $email->status  = 'NEW';
                    //         // real email
                    //         $email->to      = $review->approve->email;
                    //         // dummy data
                    //         // $email->to = "coins@ppj.gov.my";

                    //         $email->counter = 1;
                    //         $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                    //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Bahagian : '.$review->request->department->name.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah';
                    //         $email->save();
                    //     } else {
                    //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    //     }

                    //     audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    // } elseif (1 == $request->semak3) {
                    //     $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                    //     // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                    //     $ipc = Ipc::find($request->input('ipc_id'));
                    //     if(empty($rev_ipc) || (!empty($rev_ipc) && $ipc->status == 1)){
                            
                    //         $ipc->status = 2;
                    //         $ipc->update();
                    //         IpcInvoice::where('ipc_id',$ipc->id)->update([
                    //             'status'       => 2,
                    //         ]);
                    //     }
                        
                    //     $review2->status         = 'S3';
                    //     $review2->progress       = 0;
                    //     $review2->department     = config('enums.PP');
                    //     $review2->acquisition_id = $request->input('acquisition_id');
                    //     $review2->ipc_id = $request->input('ipc_id');
                    //     $review2->requested_by   = $request->input('requested_by');
                    //     $review2->requested_at   = Carbon::now();
                    //     $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                    //     $review2->remarks        = $acq_review->remarks;
                    //     $review2->approved_by    = $acq_review->task_by;
                    //     $review2->task_by        = $acq_review->task_by;
                    //     $review2->law_task_by    = $acq_review->law_task_by;
                    //     $review2->type           = $request->input('type');
                    //     $review2->created_by     = $acq_review->created_by;

                    //     $review3->status         = 'S3';
                    //     $review3->progress       = 0;
                    //     $review3->department     = config('enums.BPUB');
                    //     $review3->acquisition_id = $request->input('acquisition_id');
                    //     $review3->ipc_id = $request->input('ipc_id');
                    //     $review3->requested_by   = $request->input('requested_by');
                    //     $review3->requested_at   = Carbon::now();
                    //     $review3->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                    //     $review3->remarks        = $acq_review->remarks;
                    //     $review3->approved_by    = $acq_review->task_by;
                    //     $review3->task_by        = $acq_review->task_by;
                    //     $review3->law_task_by    = $acq_review->law_task_by;
                    //     $review3->type           = $request->input('type');
                    //     $review3->created_by     = $acq_review->created_by;

                    //     $review->status      = 'S3';
                    //     $review->progress    = 1;
                    //     $review->task_by        = $acq_review->task_by;
                    //     $review->law_task_by    = $acq_review->law_task_by;
                    //     $review->approved_by = $acq_review->task_by;
                    //     $review->department  = config('enums.BPUB');

                    //     $reviewlog->status      = 'S3';
                    //     $reviewlog->progress       = 1;
                    //     $reviewlog->approved_by = $acq_review->task_by;
                    //     $reviewlog->department  = config('enums.BPUB');
                    //     $reviewlog->remarks = 'Kontrak Dihantar Kepada ' . $review->approve->name;

                    //     $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.';
                    //     $workflow->flow_location = 'S3';
                    //     $workflow->is_claimed = 1;
                    //     $workflow->user_id = $acq_review->task_by;
                    //     $workflow->workflow_team_id = 1;

                    //     if (! empty($review->approved_by)) {
                    //         $email->status  = 'NEW';
                    //         // real email
                    //         $email->to      = $review->approve->email;
                    //         // dummy data
                    //         // $email->to = "coins@ppj.gov.my";

                    //         $email->counter = 1;
                    //         $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                    //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Bahagian : '.$review->request->department->name.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah';

                    //         $email->save();
                    //     } else {
                    //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    //     }

                    //     audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    // } else {}
                } else {
                    $review->progress    = 0;
                    $review->status      = 'S1';
                    $review->approved_by = $request->input('approved_by');
                    $review->department  = config('enums.PP');
                    $review->reviews_status    = '';

                    $reviewlog->progress       = 0;
                    $reviewlog->status         = 'S1';
                    $reviewlog->approved_by    = $request->input('approved_by');
                    $reviewlog->department     = config('enums.PP');
                    $reviewlog->remarks        = 'Kontrak Dihantar Kepada ' . $review->approve->name;
                    // $reviewlog->reviews_status = '';

                    $reviewlog1->progress       = 0;
                    $reviewlog1->status         = 'S1';
                    $reviewlog1->approved_by    = $request->input('approved_by');
                    $reviewlog1->department     = config('enums.PP');

                    if('Dokumen Kontrak' == $request->type){
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                        $workflow->role_id = 5;

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                    } elseif('Penamatan' == $request->type){
                        if('Amaran' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$warn->warning_letter_no.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->role_id = 5;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : Amaran </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $warn->status = 2;
                            $warn->update();

                            audit($review, __('Semakan '.$request->document_contract_type. ' '.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        } else {
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->role_id = 5;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : '.$request->document_contract_type.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $warn->status = 2;
                            $warn->update();

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        }
                        
                    } elseif('VO' == $request->type){
                        if('PPK' == $request->document_contract_type){
                            $vo_ppk = VO::where('id',$request->ppk_id)->where('no',$ppk->no)->first();
                            $vo_ppk->status = 9;
                            $vo_ppk->update();
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->role_id = 5;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan PPK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPK '.$ppk->no.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        } elseif('PPJHK' == $request->document_contract_type){
                            $vo_ppjhk = Ppjhk::where('id',$request->ppjhk_id)->where('no',$ppjhk->no)->first();
                            $vo_ppjhk->status = 9;
                            $vo_ppjhk->update();
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->role_id = 5;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan PPJHK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPJHK '.$ppjhk->no.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        }
                        
                    } elseif('Acquisitions' == $request->type){
                        $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                        $workflow->flow_location = 'S1';
                        $workflow->role_id = 5;

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan  Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                    } elseif('EOT' == $request->type){
                        $eot->status = 1;
                        $eot->update();
                        if($acq->approval->acquisition_type_id == 2){
                            $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->role_id = 5;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        } else {
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->role_id = 5;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        }
                        $workflow->flow_location = 'S1';
                    } else {
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                        $workflow->flow_location = 'S1';
                        $workflow->role_id = 5;

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan '.$request->type.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                    }
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->workflow_team_id = 1;
                }
            } else {
                if (null == $pp->status && $request->type == $pp->type && (($request->document_contract_type == $pp->document_contract_type) || ($request->document_contract_type == $pp_null->document_contract_type)) && $pp->approved_by == $pp->created_by && $pp->created_by == user()->id) {
                    $review->progress    = $pp->progress;
                    $review->status      = 'S1';
                    $review->approved_by = $request->input('approved_by');
                    $review->remarks     = $pp->remarks;
                    $review->department  = config('enums.PP');
                    $review->task_by     = $pp->task_by;
                    $review->law_task_by     = $pp->law_task_by;
                    $review->reviews_status    = '';

                    $reviewlog->progress       = $pp->progress;
                    $reviewlog->status         = 'S1';
                    $reviewlog->approved_by    = $request->input('approved_by');
                    $reviewlog->remarks        = $pp->remarks;
                    $reviewlog->department     = config('enums.PP');
                    $reviewlog->task_by        = $pp->task_by;
                    $reviewlog->remarks        = 'Kontrak Dihantar Kepada ' . $review->approve->name;
                    // $reviewlog->reviews_status = '';

                    $reviewlog1->progress       = $pp->progress;
                    $reviewlog1->status         = 'S1';
                    $reviewlog1->approved_by    = $request->input('approved_by');
                    $reviewlog1->remarks        = null;
                    $reviewlog1->department     = config('enums.PP');
                    $reviewlog1->task_by        = $pp->task_by;

                    if('Dokumen Kontrak' == $request->type){
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                    } elseif('Penamatan' == $request->type){
                        if('Amaran' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$warn->warning_letter_no.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : Amaran </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $warn->status = 2;
                            $warn->update();

                            audit($review, __('Semakan '.$request->document_contract_type. ' '.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        } else {
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $warn->status = 2;
                            $warn->update();

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        }
                    } elseif('VO' == $request->type){
                        if('PPK' == $request->document_contract_type){
                            $vo_ppk = VO::where('id',$request->ppk_id)->where('no',$ppk->no)->first();
                            $vo_ppk->status = 9;
                            $vo_ppk->update();
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan PPK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPK '.$ppk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        } elseif('PPJHK' == $request->document_contract_type){
                            $vo_ppjhk = Ppjhk::where('id',$request->ppjhk_id)->where('no',$ppjhk->no)->first();
                            $vo_ppjhk->status = 9;
                            $vo_ppjhk->update();
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan PPJHK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPJHK '.$ppjhk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        }
                        
                    } elseif('Acquisitions' == $request->type){
                        $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                        $workflow->flow_location = 'S1';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan  Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                    } elseif ('IPC' == $request->input('type')) {
                        // if (1 == $request->semak1) {
                            
                            $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                            // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                            $ipc = Ipc::find($request->input('ipc_id'));
                            if(empty($rev_ipc) || (!empty($rev_ipc) && $ipc->status == 1)){
                                $ipc->status = 2;
                                $ipc->update();
                                IpcInvoice::where('ipc_id',$ipc->id)->update([
                                    'status'       => 2,
                                ]);
                            }
                            
                            $review2->status         = 'S1';
                            $review2->progress       = 0;
                            $review2->department     = config('enums.BPUB');
                            $review2->acquisition_id = $request->input('acquisition_id');
                            $review2->ipc_id = $request->input('ipc_id');
                            $review2->requested_by   = $request->input('requested_by');
                            $review2->requested_at   = Carbon::now();
                            $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            $review2->remarks        = $acq_review->remarks;
                            $review2->approved_by    = $acq_review->task_by;
                            $review2->task_by        = $acq_review->task_by;
                            $review2->law_task_by    = $acq_review->law_task_by;
                            $review2->type           = $request->input('type');
                            $review2->created_by     = $acq_review->created_by;

                            $review->status = 'S1';
                            $review->progress    = 1;
                            $review->task_by        = $acq_review->task_by;
                            $review->law_task_by    = $acq_review->law_task_by;
                            $review->approved_by = user()->supervisor->id;
                            $review->department     = config('enums.PP');

                            $reviewlog->status = 'S1';
                            $reviewlog->approved_by = user()->supervisor->id;
                            $reviewlog->remarks = 'Kontrak Dihantar Kepada ' . $review->approve->name;

                            $reviewlog1->status = 'S1';
                            $reviewlog1->department     = config('enums.PP');
                            $reviewlog1->approved_by = user()->supervisor->id;

                            $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->flow_location = 'S1';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = user()->supervisor->id;
                            $workflow->workflow_team_id = 1;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        // } elseif (1 == $request->semak2) {
                        //     $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                        //     // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                        //     $ipc = Ipc::find($request->input('ipc_id'));
                        //     if(empty($rev_ipc) || (!empty($rev_ipc) && $ipc->status == 1)){
                                
                        //         $ipc->status = 2;
                        //         $ipc->update();
                        //         IpcInvoice::where('ipc_id',$ipc->id)->update([
                        //             'status'       => 2,
                        //         ]);
                        //     }
                            
                        //     $review2->status         = 'S2';
                        //     $review2->progress       = 0;
                        //     $review2->department     = config('enums.BPUB');
                        //     $review2->acquisition_id = $request->input('acquisition_id');
                        //     $review2->ipc_id = $request->input('ipc_id');
                        //     $review2->requested_by   = $request->input('requested_by');
                        //     $review2->requested_at   = Carbon::now();
                        //     $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        //     $review2->remarks        = $acq_review->remarks;
                        //     $review2->approved_by    = $acq_review->task_by;
                        //     $review2->task_by        = $acq_review->task_by;
                        //     $review2->law_task_by    = $acq_review->law_task_by;
                        //     $review2->type           = $request->input('type');
                        //     $review2->created_by     = $acq_review->created_by;

                        //     $review->status      = 'S2';
                        //     $review->progress    = 1;
                        //     $review->task_by        = $acq_review->task_by;
                        //     $review->law_task_by    = $acq_review->law_task_by;
                        //     $review->approved_by = user()->supervisor->supervisor->id;
                        //     $review->department     = config('enums.PP');

                        //     $reviewlog->status      = 'S2';
                        //     $reviewlog->progress       = 1;
                        //     $reviewlog->approved_by = user()->supervisor->supervisor->id;
                        //     $reviewlog->remarks = 'Kontrak Dihantar Kepada ' . $review->approve->name;

                        //     $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
                        //     $workflow->flow_location = 'S2';
                        //     $workflow->is_claimed = 1;
                        //     $workflow->user_id = user()->supervisor->supervisor->id;
                        //     $workflow->workflow_team_id = 1;

                        //     if (! empty($review->approved_by)) {
                        //         $email->status  = 'NEW';
                        //         // real email
                        //         $email->to      = $review->approve->email;
                        //         // dummy data
                        //         // $email->to = "coins@ppj.gov.my";

                        //         $email->counter = 1;
                        //         $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                        //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Bahagian : '.$review->request->department->name.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah';

                        //         $email->save();
                        //     } else {
                        //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        //     }

                        //     audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        // } elseif (1 == $request->semak3) {
                        //     $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                        //     // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                        //     $ipc = Ipc::find($request->input('ipc_id'));
                        //     if(empty($rev_ipc) || (!empty($rev_ipc) && $ipc->status == 1)){
                                
                        //         $ipc->status = 2;
                        //         $ipc->update();
                        //         IpcInvoice::where('ipc_id',$ipc->id)->update([
                        //             'status'       => 2,
                        //         ]);
                        //     }
                            
                        //     $review2->status         = 'S3';
                        //     $review2->progress       = 0;
                        //     $review2->department     = config('enums.PP');
                        //     $review2->acquisition_id = $request->input('acquisition_id');
                        //     $review2->ipc_id = $request->input('ipc_id');
                        //     $review2->requested_by   = $request->input('requested_by');
                        //     $review2->requested_at   = Carbon::now();
                        //     $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        //     $review2->remarks        = $acq_review->remarks;
                        //     $review2->approved_by    = $acq_review->task_by;
                        //     $review2->task_by        = $acq_review->task_by;
                        //     $review2->law_task_by    = $acq_review->law_task_by;
                        //     $review2->type           = $request->input('type');
                        //     $review2->created_by     = $acq_review->created_by;

                        //     $review3->status         = 'S3';
                        //     $review3->progress       = 0;
                        //     $review3->department     = config('enums.BPUB');
                        //     $review3->acquisition_id = $request->input('acquisition_id');
                        //     $review3->ipc_id = $request->input('ipc_id');
                        //     $review3->requested_by   = $request->input('requested_by');
                        //     $review3->requested_at   = Carbon::now();
                        //     $review3->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        //     $review3->remarks        = $acq_review->remarks;
                        //     $review3->approved_by    = $acq_review->task_by;
                        //     $review3->task_by        = $acq_review->task_by;
                        //     $review3->law_task_by    = $acq_review->law_task_by;
                        //     $review3->type           = $request->input('type');
                        //     $review3->created_by     = $acq_review->created_by;

                        //     $review->status      = 'S3';
                        //     $review->progress    = 1;
                        //     $review->task_by        = $acq_review->task_by;
                        //     $review->law_task_by    = $acq_review->law_task_by;
                        //     $review->approved_by = $acq_review->task_by;
                        //     $review->department  = config('enums.BPUB');

                        //     $reviewlog->status      = 'S3';
                        //     $reviewlog->progress       = 1;
                        //     $reviewlog->approved_by = $acq_review->task_by;
                        //     $reviewlog->department  = config('enums.BPUB');
                        //     $reviewlog->remarks = 'Kontrak Dihantar Kepada ' . $review->approve->name;

                        //     $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.';
                        //     $workflow->flow_location = 'S3';
                        //     $workflow->is_claimed = 1;
                        //     $workflow->user_id = $acq_review->task_by;
                        //     $workflow->workflow_team_id = 1;

                        //     if (! empty($review->approved_by)) {
                        //         $email->status  = 'NEW';
                        //         // real email
                        //         $email->to      = $review->approve->email;
                        //         // dummy data
                        //         // $email->to = "coins@ppj.gov.my";

                        //         $email->counter = 1;
                        //         $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                        //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Bahagian : '.$review->request->department->name.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah';

                        //         $email->save();
                        //     } else {
                        //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        //     }

                        //     audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        // } else {}
                    } elseif('EOT' == $request->type){
                        $eot = EOT::find($request->eot_id);
                        $eot->status = 1;
                        $eot->update();
                        if($acq->approval->acquisition_type_id == 2){
                            $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil.'</b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        } else {
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                        }
                    } else {
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                        $workflow->flow_location = 'S1';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan ' .$request->type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                    }
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 5;
                    $workflow->workflow_team_id = 1;

                } elseif ('S1' == $pp->status && $request->type == $pp->type && (($request->document_contract_type == $pp->document_contract_type) || ($request->document_contract_type == $pp_null->document_contract_type)) && '3' != user()->department_id) {
                    $review->status    = 'S2';
                    $reviewlog->status = 'S2';
                    $reviewlog1->status = 'S2';
                    $review->approved_by = $request->input('approved_by');
                    $reviewlog->approved_by = $request->input('approved_by');
                    $reviewlog1->approved_by = $request->input('approved_by');

                    if ('Acquisitions' == $pp->type) {
                        $review->progress = $pp->progress;
                        $review->department  = config('enums.PP');
                        $reviewlog->progress = $pp->progress;
                        $reviewlog->department  = config('enums.PP');
                        $reviewlog1->progress = $pp->progress;
                        $reviewlog1->department  = config('enums.PP');
                        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                    } elseif('Dokumen Kontrak' == $pp->type || 'Penamatan' == $pp->type || 'VO' == $pp->type){
                        $review->progress = 1;
                        $review->department  = config('enums.PP');
                        $reviewlog->progress = 1;
                        $reviewlog->department  = config('enums.PP');
                        $reviewlog1->progress = 1;
                        $reviewlog1->department  = config('enums.PP');
                        if('Dokumen Kontrak' == $pp->type){

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        } elseif('Penamatan' == $pp->type) {
                            if('Amaran' == $request->document_contract_type){

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : Amaran </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                $warn->status = 2;
                                $warn->update();

                                audit($review, __('Semakan '.$request->document_contract_type. ' '.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                            } else {

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                $warn->status = 2;
                                $warn->update();

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' .$review->approve->name));
                            }

                        } elseif('VO' == $pp->type) {
                            if('PPK' == $pp->document_contract_type){   

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan PPK';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPK : ' .$ppk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 

                                audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } elseif('PPJHK' == $pp->document_contract_type) {   

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan PPJHK';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPJHK : ' .$ppjhk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            }

                        }
                    } else {
                        $workflow->flow_location = 'S2';
                        $review->progress = 1;
                        $review->department  = config('enums.PP');
                        $reviewlog->progress = 1;
                        $reviewlog->department  = config('enums.PP');
                        $reviewlog1->progress = 1;
                        $reviewlog1->department  = config('enums.PP');
                        if('IPC' == $pp->type) {
                            
                            $ipc = Ipc::find($request->input('ipc_id'));

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC ' .$ipc->ipc_no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }
                            audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        } elseif('EOT' == $pp->type) {

                            if($acq->approval->acquisition_type_id == 2){   

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT ' .$eot->bil. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }                         

                                audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } else {  

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                                                      
                                audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                                                        
                            }
                        } else {

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan ' .$request->type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        }
                    }

                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;

                    if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                        if('Amaran' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$warn->warning_letter_no.' : Sila beri ulasan untuk proses pengesahan.';
                        } else {
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses pengesahan.';
                        }
                    } elseif('VO' == $request->type){
                        if('PPK' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses pengesahan.';
                        } elseif('PPJHK' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses pengesahan.';
                        }
                    } elseif('IPC' == $request->type){
                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
                    } elseif('EOT' == $request->type){
                        $eot = EOT::find($request->eot_id);
                        if($acq->approval->acquisition_type_id == 2){
                            $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses pengesahan.';
                        } else {
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';
                        }
                    } elseif('Acquisitions' == $request->type){
                        $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses pengesahan.';
                    } else {
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';
                    }
                    
                } elseif ('S2' == $pp->status && $request->type == $pp->type && (($request->document_contract_type == $pp->document_contract_type) || ($request->document_contract_type == $pp_null->document_contract_type)) && 0 == $pp->progress && 'PELAKSANA' == $cnt_review->department && '3' != user()->department_id) {
                    $review->status      = 'N';
                    $review->progress    = $pp->progress;
                    $review->department  = config('enums.BPUB');
                    $review->approved_by = $request->input('task_by');

                    $reviewlog->status      = 'CNPP';
                    $reviewlog->progress    = $pp->progress;
                    $reviewlog->department  = config('enums.BPUB');
                    $reviewlog->approved_by = $request->input('task_by');

                    $reviewlog1->status      = 'CNPP';
                    $reviewlog1->progress    = $pp->progress;
                    $reviewlog1->department  = config('enums.BPUB');
                    $reviewlog1->approved_by = $request->input('task_by');
                    $reviewlog1->requested_by = $request->input('task_by');

                    $reviewlog2->status      = 'CNPP';
                    $reviewlog2->progress    = $pp->progress;
                    $reviewlog2->department  = config('enums.UU');
                    $reviewlog2->approved_by = $request->input('task_by');
                    $reviewlog2->requested_by = $request->input('task_by');

                    $workflow->flow_desc = 'Penyerahan Tugasan : Sila pilih penyemak untuk proses penyemakan (Khas untuk BPUB).';
                    $workflow->flow_location = 'N';
                    $workflow->is_claimed = 0;
                    $workflow->url = '/acquisition/review';
                    $workflow->user_id = null;
                    $workflow->role_id = 6;
                    $workflow->workflow_team_id = 1;


                    $workflow2 = new WorkflowTask(); 

                    $workflow2->acquisition_id = $acq->id;
                    $workflow2->flow_name = $acq->reference . ' : ' . $acq->title;

                    $workflow2->flow_desc = 'Penyerahan Tugasan : Sila pilih penyemak untuk proses penyemakan (Khas untuk undang-undang).';
                    $workflow2->flow_location = 'N';
                    $workflow2->is_claimed = 0;
                    $workflow2->url = '/acquisition/review';
                    $workflow2->user_id = null;
                    $workflow2->role_id = 6;
                    $workflow2->workflow_team_id = 2;

                    $review2->status         = 'S6';
                    $review2->department     = config('enums.UU');
                    $review2->acquisition_id = $request->input('acquisition_id');
                    $review2->requested_by   = $request->input('requested_by');
                    $review2->requested_at   = Carbon::now();
                    // $review2->approved_by    = $request->input('approved_by');
                    $review2->approved_at = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                    $review2->remarks     = str_replace("'","`",$request->input('remarks')) ?? '';
                    $review2->approved_by = $request->input('task_by');
                    $review2->type        = $request->input('type');

                    // if ('Draft Penyedia' == $acq->status_task || 'Semakan Dokumen Perolehan' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('SST' == $acq->status_task || 'Cetak Notis' == $acq->status_task) {
                    //     $review2->type = 'SST';
                    // } elseif ('Dokumen Kontrak' == $acq->status_task) {
                    //     $review2->type = 'Dokumen Kontrak';
                    // } elseif ('IPC' == $acq->status_task) {
                    //     $review2->type = 'IPC';
                    // } elseif ('VO' == $acq->status_task) {
                    //     $review2->type = 'VO';
                    // } elseif ('EOT' == $acq->status_task) {
                    //     $review2->type = 'EOT';
                    // } elseif ('SOFA' == $acq->status_task) {
                    //     $review2->type = 'SOFA';
                    // }

                    $review2->created_by = $pp->created_by;
                    
                    $claim_task_bpub = user()->where('department_id',9)->role(['urusetia'])->get();
                    foreach($claim_task_bpub as $claimed_bpub){
                        $email_claimed_bpub = new EmailScheduler;
                        $email_claimed_bpub->status  = 'NEW';
                        // real email
                        $email_claimed_bpub->to      = $claimed_bpub->email;
                        // dummy data
                        // $email_claimed_bpub->to = "coins@ppj.gov.my";

                        $email_claimed_bpub->counter = 1;
                        $email_claimed_bpub->subject = 'Tindakan : Penyerahan Tugasan kepada BPUB ';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Tindakan : Penyerahan Tugasan kepada BPUB </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email_claimed_bpub->save();
                    }
                    
                    $claim_task_uu = user()->where('department_id',3)->role(['urusetia'])->get();
                    foreach($claim_task_uu as $claimed_uu){
                        $email_claimed_uu = new EmailScheduler;
                        $email_claimed_uu->status  = 'NEW';
                        // real email
                        $email_claimed_uu->to      = $claimed_uu->email;
                        // dummy data
                        // $email_claimed_uu->to = "coins@ppj.gov.my";

                        $email_claimed_uu->counter = 1;
                        $email_claimed_uu->subject = 'Tindakan : Penyerahan Tugasan kepada Bahagian Undang-Undang ';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email_claimed_uu->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Tindakan : Penyerahan Tugasan kepada Bahagian Undang-Undang </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email_claimed_uu->save();
                    }

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada Urusetia BPUB dan Undang-Undang'));
                // $review2->task_by   = $request->input('task_by');
                } elseif ('S2' == $pp->status && $request->type == $pp->type && (($request->document_contract_type == $pp->document_contract_type) || ($request->document_contract_type == $pp_null->document_contract_type)) && 1 == $pp->progress && 'PELAKSANA' == $cnt_review->department && '3' != user()->department_id) {
                    if('Penamatan' == $request->type){
                        $review->status      = 'Selesai';
                        $review->progress    = $pp->progress;
                        $review->approved_by = $pp->created_by;
                        $review->department  = config('enums.PP');

                        $reviewlog->status      = 'CNPP';
                        $reviewlog->progress    = $pp->progress;
                        $reviewlog->approved_by = $pp->created_by;
                        $reviewlog->department  = config('enums.PP');

                        $reviewlog1->status      = 'CNPP';
                        $reviewlog1->progress    = $pp->progress;
                        $reviewlog1->approved_by = $pp->created_by;
                        $reviewlog1->department  = config('enums.PP');
                        $reviewlog1->reviews_status  = 'Selesai Semakan';

                        $acq->status_task = config('enums.flow15');
                        $acq->update();

                        if('Amaran' == $request->document_contract_type || 'Notis' == $request->document_contract_type){
                            $workflow->flow_desc = 'Penamatan Kontrak : Perolehan ini sudah ditamatkan.';
                            $workflow->flow_location = $request->document_contract_type;
                            $workflow->is_claimed = 1;
                            $workflow->user_id = null;
                            $workflow->workflow_team_id = 1;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : Amaran </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $warn->status = 3;
                            $warn->update();

                            if('Notis' == $request->document_contract_type){
                                $sst->termination_at = $warn->termination_at;
                                $sst->update();
                                $company->termination_at = $warn->termination_at;
                                $company->update();
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' sudah ditamatkan.'));

                        } else {
                            $workflow->url = '/termination/termination/'.$acq->sst->hashslug.'/edit#terminate';
                            $workflow->flow_desc = 'Notis Penamatan : Sila teruskan dengan Notis Penamatan.';
                            $workflow->flow_location = 'Notis';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $pp->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $warn->status = 3;
                            $warn->update();

                            audit($review, __('Semakan '.$request->document_contract_type. ' Selesai - ' .$acq->reference. ' : ' .$acq->title. ' dan Dihantar Kepada ' . $review->create->name));

                        }
                        
                    } elseif('EOT' == $pp->type && ( (2 == $acq->approval->acquisition_type_id && (1 == $acq->acquisition_category_id || 2 == $acq->acquisition_category_id)) || ( (1 == $acq->approval->acquisition_type_id || 3 == $acq->approval->acquisition_type_id || 4 == $acq->approval->acquisition_type_id) && (1 == $acq->acquisition_category_id || 2 == $acq->acquisition_category_id || 3 == $acq->acquisition_category_id || 4 == $acq->acquisition_category_id)) ) ){

                        $review->progress    = 1;
                        $review->status      = 'N';
                        $review->approved_by      = null;
                        $review->department  = config('enums.BPUB');

                        $reviewlog->progress    = 1;
                        $reviewlog->status      = 'CNPP';
                        $reviewlog->approved_by      = null;
                        $reviewlog->department  = config('enums.BPUB');

                        $reviewlog1->progress    = 1;
                        $reviewlog1->status      = 'CNPP';
                        $reviewlog1->approved_by      = null;
                        $reviewlog1->requested_by      = null;
                        $reviewlog1->department  = config('enums.BPUB');

                        $eot = EOT::find($request->eot_id);
                        $eot->status = 2;
                        $eot->update();
                            
                        $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak ' .$eot->bil. ' : Sila isi maklumat kelulusan dan muat naik keputusan mesyuarat';     
                    
                        $claim_task_bpub = user()->where('department_id',9)->role(['urusetia'])->get();
                        foreach($claim_task_bpub as $claimed_bpub){
                            $email_claimed_bpub = new EmailScheduler;
                            $email_claimed_bpub->status  = 'NEW';
                            // real email
                            $email_claimed_bpub->to      = $claimed_bpub->email;
                            // dummy data
                            // $email_claimed_bpub->to = "coins@ppj.gov.my";

                            $email_claimed_bpub->counter = 1;
                            $email_claimed_bpub->subject = 'Tindakan : Semakan dan Sahkan EOT';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email_claimed_bpub->save();
                        }                     

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada Urusetia'));

                        $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#extension';
                        $workflow->flow_location = 'N';
                        $workflow->is_claimed = 0;
                        $workflow->user_id = null;
                        $workflow->role_id = 6;
                        $workflow->workflow_team_id = 1;

                        $acq->status_task = config('enums.flowALT1');
                        $acq->update();
                    } else {
                        $review->status      = 'S3';
                        $review->progress    = $pp->progress;
                        $review->approved_by = $pp->task_by;
                        $review->department  = config('enums.BPUB');

                        $reviewlog->status      = 'CNPP';
                        $reviewlog->progress    = $pp->progress;
                        $reviewlog->approved_by = $pp->task_by;
                        $reviewlog->department  = config('enums.BPUB');

                        $reviewlog1->status      = 'CNPP';
                        $reviewlog1->progress    = $pp->progress;
                        $reviewlog1->approved_by = $pp->task_by;
                        $reviewlog1->requested_by = $pp->task_by;
                        $reviewlog1->department  = config('enums.BPUB');

                        if(!empty($pp) && $pp->reviews_status == 'Kuiri' && $pp->request->department_id == '3'){
                            if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                            } else {
                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                            }

                            if(empty($workflow2)){
                                $workflow2 = new WorkflowTask();
                            }

                        } else {
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $pp->task_by;
                            $workflow->role_id = 5;
                            $workflow->workflow_team_id = 1;
                            if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                                $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first(); 
                                
                                $workflow2->role_id = 5;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } elseif('VO' == $request->type){
                                if('PPK' == $request->document_contract_type){
                                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses penyemakan.';
                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('ppk_id',$ppk->id)->where('flow_location','PPK')->where('workflow_team_id',2)->first(); 

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan PPK';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPK '.$ppk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                                } elseif('PPJHK' == $request->document_contract_type){
                                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses penyemakan.';
                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('ppjhk_id',$ppjhk->id)->where('flow_location','PPJHK')->where('workflow_team_id',2)->first();

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan PPJHK';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPJHK '.$ppjhk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                                }
                                
                            } elseif('Acquisitions' == $request->type){
                                $workflow->flow_location = 'S3';
                                $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';

                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                $workflow2->role_id = 5;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } elseif('IPC' == $request->type) {
                                $workflow->flow_location = 'S3';
                                $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                $workflow2->role_id = 5;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 

                                $ipc = Ipc::find($request->input('ipc_id'));               
                                audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } elseif('EOT' == $request->type) {
                                $workflow->flow_location = 'S3';
                                $eot = EOT::find($request->eot_id);
                                if($acq->approval->acquisition_type_id == 2 && ($acq->acquisition_category_id == 3 || $acq->acquisition_category_id == 4)){
                                    $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }
                                    audit($review, __('Semakan '.$request->type.' '.$eot->bil.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                                } else {
                                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }
                                    audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
                                }

                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();               

                            } else {
                                $workflow->flow_location = 'S3';
                                $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                $workflow2->role_id = 5;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan '.$request->type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }                        

                                audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            }

                            if(empty($workflow2)){
                                $workflow2 = new WorkflowTask();

                                $workflow2->acquisition_id = $acq->id;
                                $workflow2->flow_name = $acq->reference . ' : ' . $acq->title;
                            }
                        }

                        if ('Acquisitions' == $request->type) {
                            $review2->status         = 'S6';
                            $review2->department     = config('enums.UU');
                            $review2->acquisition_id = $request->input('acquisition_id');
                            $review2->requested_by   = $request->input('requested_by');
                            $review2->requested_at   = Carbon::now();
                            $review2->progress    = $uu->progress;
                            $review2->approved_by    = $uu->law_task_by;
                            $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                            // $review2->approved_by    = $request->input('task_by');
                            $review2->task_by    = $request->input('task_by');
                            $review2->law_task_by    = $uu->law_task_by;
                            $review2->created_by = $pp->created_by;
                            $review2->type       = 'Acquisitions';

                            $reviewlog2->status      = 'CNPP';
                            $reviewlog2->progress    = $uu->progress;
                            $reviewlog2->approved_by = $uu->law_task_by;
                            $reviewlog2->requested_by = $uu->law_task_by;
                            $reviewlog2->law_task_by    = $uu->law_task_by;
                            $reviewlog2->department  = config('enums.UU');
                            
                            if(!empty($pp) && $pp->reviews_status == 'Kuiri' && $pp->request->department_id == '9'){

                            } else {
                                $workflow2->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                                $workflow2->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                                $workflow2->flow_location = 'S6';
                                $workflow2->is_claimed = 1;
                                $workflow2->user_id = $uu->law_task_by;
                                $workflow2->role_id = 5;
                                $workflow2->workflow_team_id = 2;

                                if (! empty($review2->approved_by)) {
                                    $email2 = new EmailScheduler;
                                    $email2->status  = 'NEW';
                                    // real email
                                    $email2->to      = $review2->approve->email;
                                    // dummy data
                                    // $email2->to = "coins@ppj.gov.my";

                                    $email2->counter = 1;
                                    $email2->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email2->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email2->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review2, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review2->approve->name));

                            }

                        } elseif ('SST' == $request->type) {
                            $review2->status         = 'S6';
                            $review2->department     = config('enums.UU');
                            $review2->acquisition_id = $request->input('acquisition_id');
                            $review2->requested_by   = $cnt_review->law_task_by;
                            $review2->requested_at   = Carbon::now();
                            $review2->progress    = $cnt_review->progress;
                            $review2->approved_by    = $cnt_review->law_task_by;
                            $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                            // $review2->approved_by    = $request->input('task_by');
                            $review2->task_by    = $request->input('task_by');
                            $review2->law_task_by    = $cnt_review->law_task_by;
                            $review2->created_by = $pp->created_by;
                            $review2->type       = 'SST';

                            $reviewlog2->status      = 'CNPP';
                            $reviewlog2->progress    = $cnt_review->progress;
                            $reviewlog2->approved_by = $cnt_review->law_task_by;
                            $reviewlog2->requested_by = $cnt_review->law_task_by;
                            $reviewlog2->law_task_by    = $cnt_review->law_task_by;
                            $reviewlog2->department  = config('enums.UU');
                            
                            if(!empty($pp) && $pp->reviews_status == 'Kuiri' && $pp->request->department_id == '9'){

                            } else {

                                $workflow2->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow2->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                                $workflow2->flow_location = 'S6';
                                $workflow2->is_claimed = 1;
                                $workflow2->user_id = $cnt_review->law_task_by;
                                $workflow2->role_id = 5;
                                $workflow2->workflow_team_id = 2; 

                                if (! empty($review2->approved_by)) {
                                    $email2 = new EmailScheduler;
                                    $email2->status  = 'NEW';
                                    // real email
                                    $email2->to      = $review2->approve->email;
                                    // dummy data
                                    // $email2->to = "coins@ppj.gov.my";

                                    $email2->counter = 1;
                                    $email2->subject = 'Tindakan : Semakan dan Sahkan SST';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email2->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan SST </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email2->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review2, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review2->approve->name));

                            }

                        } elseif ('Dokumen Kontrak' == $request->type) {
                            // if('Bon Pelaksanaan' == $request->document_contract_type || 'Insurans' == $request->document_contract_type || 'Dokumen Kontrak' == $request->document_contract_type ||'Wang Pendahuluan' == $request->document_contract_type){
                            if('Dokumen Kontrak' == $request->document_contract_type){
                                $review2->status         = 'S6';
                                $review2->department     = config('enums.UU');
                                $review2->acquisition_id = $request->input('acquisition_id');
                                $review2->requested_by   = $request->input('requested_by');
                                $review2->requested_at   = Carbon::now();
                                $review2->progress    = $cnt_review->progress;
                                $review2->approved_by    = $cnt_review->law_task_by;
                                $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                                $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                                // $review2->approved_by    = $request->input('task_by');
                                $review2->task_by                = $request->input('task_by');
                                $review2->law_task_by    = $cnt_review->law_task_by;
                                $review2->created_by             = $pp->created_by;
                                $review2->type                   = 'Dokumen Kontrak';
                                $review2->document_contract_type = $request->input('document_contract_type');

                                $reviewlog2->status      = 'CNPP';
                                $reviewlog2->progress    = $cnt_review->progress;
                                $reviewlog2->approved_by = $cnt_review->law_task_by;
                                $reviewlog2->requested_by = $cnt_review->law_task_by;
                                $reviewlog2->law_task_by    = $cnt_review->law_task_by;
                                $reviewlog2->department  = config('enums.UU');
                                
                                if(!empty($pp) && $pp->reviews_status == 'Kuiri' && $pp->request->department_id == '9'){

                                } else {

                                    $workflow2->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                    if('Bon Pelaksanaan' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                                        $workflow2->flow_location = 'Bon Pelaksanaan';
                                    } elseif('Insurans' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                                        $workflow2->flow_location = 'Insurans';
                                    } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                                        $workflow2->flow_location = 'Dokumen Kontrak';
                                    } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                                        $workflow2->flow_location = 'Wang Pendahuluan';
                                    } elseif('CPC' == $request->document_contract_type) {
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';
                                        $workflow2->flow_location = 'CPC';
                                    } elseif('CNC' == $request->document_contract_type) {
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';
                                        $workflow2->flow_location = 'CNC';
                                    } elseif('CPO' == $request->document_contract_type) {
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';
                                        $workflow2->flow_location = 'CPO';
                                    } elseif('CMGD' == $request->document_contract_type) {
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';
                                        $workflow2->flow_location = 'CMGD';
                                    } elseif('Amaran' == $request->document_contract_type) {
                                        $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();
                                        $workflow2->url = '/termination/warning/'.$warn->hashslug;
                                        $workflow2->flow_location = 'Amaran';
                                    }
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = $cnt_review->law_task_by;
                                    $workflow2->role_id = 5;
                                    $workflow2->workflow_team_id = 2;

                                    if (! empty($review2->approved_by)) {
                                        $email2 = new EmailScheduler;
                                        $email2->status  = 'NEW';
                                        // real email
                                        $email2->to      = $review2->approve->email;
                                        // dummy data
                                        // $email2->to = "coins@ppj.gov.my";

                                        $email2->counter = 1;
                                        $email2->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email2->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email2->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review2, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review2->approve->name));

                                }
                            } else {}

                        } elseif ('IPC' == $request->type) {
                            // $review2->status         = 'S6';
                            // $review2->department     = config('enums.UU');
                            // $review2->acquisition_id = $request->input('acquisition_id');
                            // $review2->requested_by   = $request->input('requested_by');
                            // $review2->requested_at   = Carbon::now();
                            // $review2->approved_by    = $request->input('approved_by');
                            // $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            // $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                            // // $review2->approved_by    = $request->input('task_by');
                            // $review2->task_by    = $request->input('task_by');
                            // $review2->created_by = $pp->created_by;
                            // $review2->type       = 'IPC';
                        } elseif ('VO' == $request->type) {
                            // $review2->status         = 'S6';
                            // $review2->department     = config('enums.UU');
                            // $review2->acquisition_id = $request->input('acquisition_id');
                            // $review2->requested_by   = $request->input('requested_by');
                            // $review2->requested_at   = Carbon::now();
                            // $review2->approved_by    = $request->input('approved_by');
                            // $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            // $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                            // // $review2->approved_by    = $request->input('task_by');
                            // $review2->task_by    = $request->input('task_by');
                            // $review2->created_by = $pp->created_by;
                            // $review2->type       = 'VO';
                        } elseif ('EOT' == $request->type) {
                            // $review2->status         = 'S6';
                            // $review2->department     = config('enums.UU');
                            // $review2->acquisition_id = $request->input('acquisition_id');
                            // $review2->requested_by   = $request->input('requested_by');
                            // $review2->requested_at   = Carbon::now();
                            // $review2->approved_by    = $request->input('approved_by');
                            // $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            // $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                            // // $review2->approved_by    = $request->input('task_by');
                            // $review2->task_by    = $request->input('task_by');
                            // $review2->created_by = $pp->created_by;
                            // $review2->type       = 'EOT';
                        } elseif ('SOFA' == $request->type) {
                            // $review2->status         = 'S6';
                            // $review2->department     = config('enums.UU');
                            // $review2->acquisition_id = $request->input('acquisition_id');
                            // $review2->requested_by   = $request->input('requested_by');
                            // $review2->requested_at   = Carbon::now();
                            // $review2->approved_by    = $request->input('approved_by');
                            // $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            // $review2->remarks        = str_replace("'","`",$request->input('remarks')) ?? '';
                            // // $review2->approved_by    = $request->input('task_by');
                            // $review2->task_by    = $request->input('task_by');
                            // $review2->law_task_by    = $request->input('law_task_by');
                            // $review2->created_by = $pp->created_by;
                            // $review2->type       = 'SOFA';
                        }
                    }
                } elseif ('N' == $bpub->status && 0 == $bpub->progress && '3' != user()->department_id) {
                    $review2->status         = 'N';
                    $review2->progress       = 1;
                    $review2->department     = config('enums.BPUB');
                    $review2->acquisition_id = $request->input('acquisition_id');
                    $review2->requested_by   = $request->input('requested_by');
                    $review2->requested_at   = Carbon::now();
                    $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                    $review2->remarks        = $bpub->remarks;
                    $review2->approved_by    = $request->input('task_by');
                    $review2->task_by        = $request->input('task_by');
                    $review2->law_task_by    = $uu->law_task_by;
                    $review2->type           = $request->input('type');

                    // if ('Draft Penyedia' == $acq->status_task || 'Semakan Dokumen Perolehan' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('SST' == $acq->status_task || 'Cetak Notis' == $acq->status_task) {
                    //     $review2->type = 'SST';
                    // } elseif ('Dokumen Kontrak' == $acq->status_task) {
                    //     $review2->type = 'Dokumen Kontrak';
                    // } elseif ('IPC' == $acq->status_task) {
                    //     $review2->type = 'IPC';
                    // } elseif ('VO' == $acq->status_task) {
                    //     $review2->type = 'VO';
                    // } elseif ('EOT' == $acq->status_task) {
                    //     $review2->type = 'EOT';
                    // } elseif ('SOFA' == $acq->status_task) {
                    //     $review2->type = 'SOFA';
                    // }

                    $review2->created_by = $bpub->created_by;

                    $review->status       = 'S3';
                    $review->progress     = 1;
                    $review->remarks      = $bpub->remarks;
                    $review->approved_by  = $request->input('task_by');
                    $review->department   = config('enums.BPUB');
                    $review->created_by   = $bpub->created_by;
                    $review->requested_by = $bpub->requested_by;

                    $reviewlog->status       = 'CNPP';
                    $reviewlog->progress     = 1;
                    $reviewlog->remarks      = 'Serah Tugasan Kepada ' . $review->task->name;
                    $reviewlog->approved_by  = $request->input('task_by');
                    $reviewlog->department   = config('enums.BPUB');
                    $reviewlog->created_by   = $bpub->created_by;
                    $reviewlog->requested_by = user()->id;

                    $reviewlog1->status       = 'CNPP';
                    $reviewlog1->progress     = 1;
                    $reviewlog1->approved_by  = $request->input('task_by');
                    $reviewlog1->department   = config('enums.BPUB');
                    $reviewlog1->created_by   = $bpub->created_by;
                    $reviewlog1->requested_by = $request->input('task_by');
                    
                    $review->reviews_status = 'Selesai Serah Tugas';
                    $review2->reviews_status = 'Selesai Serah Tugas';
                    $reviewlog->reviews_status = 'Selesai Serah Tugas';

                    $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                    $workflow->flow_location = 'S3';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('task_by');
                    $workflow->role_id = 5;
                    $workflow->workflow_team_id = 1;

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    }

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));



                } elseif ('S3' == $bpub->status && $request->type == $bpub->type && (($request->document_contract_type == $bpub->document_contract_type) || ($request->document_contract_type == $bpub_null->document_contract_type)) && '3' != user()->department_id) {
                    $review->status        = 'S4';
                    $review->progress      = $bpub->progress;
                    $review->quiry_remarks = $bpub->remarks;
                    $review->approved_by   = $request->input('approved_by');
                    $review->department    = config('enums.BPUB');

                    if('SOFA' == $request->type || 'EOT' == $request->type){
                        $review->law_task_by    = $bpub->law_task_by;
                        if('EOT' == $request->type){
                            $eot = EOT::find($request->eot_id);
                            if($acq->approval->acquisition_type_id == 2){
                                $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } else {
                                $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.'; 

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }      

                                audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
    
                            }
                        } elseif('SOFA' == $request->type){
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan SOFA';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }                        
                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        }
                    } elseif('Dokumen Kontrak' == $request->type){
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('Penamatan' == $request->type){
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Penamatan';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Penamatan : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('Acquisitions' == $request->type){
                        $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('IPC' == $request->type) {
                        $review->law_task_by    = $bpub->law_task_by;
                        $workflow->flow_location = 'S4';
                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.'; 

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('VO' == $request->type) {
                        $review->law_task_by    = $bpub->law_task_by;
                        if('PPK' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses penyemakan.';   

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan PPK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPK '.$ppk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        } elseif('PPJHK' == $request->document_contract_type){
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses penyemakan.';  

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan PPJHK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPJHK '.$ppjhk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        }
                    } else {
                        $review->law_task_by    = $uu->law_task_by;
                        $workflow->flow_location = 'S4';
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan '.$request->type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }                        
                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    }

                    $reviewlog->status        = 'S4';
                    $reviewlog->progress      = $bpub->progress;
                    $reviewlog->quiry_remarks = $bpub->remarks;
                    $reviewlog->approved_by   = $request->input('approved_by');
                    $reviewlog->department    = config('enums.BPUB');

                    $reviewlog1->status        = 'S4';
                    $reviewlog1->progress      = $bpub->progress;
                    $reviewlog1->approved_by   = $request->input('approved_by');
                    $reviewlog1->department    = config('enums.BPUB');

                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 5;
                    $workflow->workflow_team_id = 1;

                } elseif ('S4' == $bpub->status && $request->type == $bpub->type && (($request->document_contract_type == $bpub->document_contract_type) || ($request->document_contract_type == $bpub_null->document_contract_type)) && '3' != user()->department_id) {
                    $review->status        = 'S5';
                    $review->quiry_remarks = $bpub->remarks;
                    $review->department    = config('enums.BPUB');

                    $reviewlog->status        = 'S5';
                    $reviewlog->progress      = $bpub->progress;
                    $reviewlog->quiry_remarks = $bpub->remarks;
                    $reviewlog->department    = config('enums.BPUB');

                    $reviewlog1->status        = 'S5';
                    $reviewlog1->progress      = $bpub->progress;
                    $reviewlog1->department    = config('enums.BPUB');

                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;
                    $review->progress      = $bpub->progress;
                    if('SOFA' == $request->type || 'EOT' == $request->type){
                        $review->law_task_by    = $bpub->law_task_by;
                        $review->approved_by   = $request->input('approved_by');
                        $reviewlog->approved_by   = $request->input('approved_by');
                        $reviewlog1->approved_by   = $request->input('approved_by');
                        $workflow->is_claimed = 1;
                        if('EOT' == $request->type){
                            $eot = EOT::find($request->eot_id);
                            $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#extension';
                            if($acq->approval->acquisition_type_id == 2){
                                $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila isi maklumat kelulusan dan muat naik keputusan mesyuarat.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                            } else {
                                $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila isi maklumat kelulusan dan muat naik keputusan mesyuarat.';  

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));
     
                            }
                        } elseif('SOFA' == $request->type){
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan dan Sahkan SOFA';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }                        
                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                        }
                    } elseif('Dokumen Kontrak' == $request->type){
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses pengesahan.';
                        $review->approved_by   = $request->input('approved_by');
                        $reviewlog->approved_by   = $request->input('approved_by');
                        $reviewlog1->approved_by   = $request->input('approved_by');
                        $workflow->is_claimed = 1; 

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan EOT';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('Acquisitions' == $request->type){
                        $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses pengesahan.';
                        $review->approved_by   = $request->input('approved_by');
                        $reviewlog->approved_by   = $request->input('approved_by');
                        $reviewlog1->approved_by   = $request->input('approved_by');
                        $workflow->is_claimed = 1;

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('IPC' == $request->type) {
                        $review->law_task_by    = $bpub->law_task_by;
                        $workflow->flow_location = 'S4';
                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
                        $workflow->is_claimed = 0; 
                    
                        $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->role(['pengesah'])->get();
                        foreach($claim_task_bpub as $claimed_bpub){
                            $email_claimed_bpub = new EmailScheduler;
                            $email_claimed_bpub->status  = 'NEW';
                            // real email
                            $email_claimed_bpub->to      = $claimed_bpub->email;
                            // dummy data
                            // $email_claimed_bpub->to = "coins@ppj.gov.my";

                            $email_claimed_bpub->counter = 1;
                            $email_claimed_bpub->subject = 'Tindakan : Semakan dan Sahkan IPC';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email_claimed_bpub->save();
                        }

                        audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada Pengesah.'));

                    } elseif('VO' == $request->type) {
                        $review->law_task_by    = $bpub->law_task_by;
                        $review->approved_by = null;
                        $reviewlog->approved_by = null;
                        $reviewlog1->approved_by = null;
                        $reviewlog1->requested_by = null;
                        if('PPK' == $request->document_contract_type){
                           $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses pengesahan.'; 
                    
                            $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->role(['pengesah'])->get();
                            foreach($claim_task_bpub as $claimed_bpub){
                                $email_claimed_bpub = new EmailScheduler;
                                $email_claimed_bpub->status  = 'NEW';
                                // real email
                                $email_claimed_bpub->to      = $claimed_bpub->email;
                                // dummy data
                                // $email_claimed_bpub->to = "coins@ppj.gov.my";

                                $email_claimed_bpub->counter = 1;
                                $email_claimed_bpub->subject = 'Tindakan : Semakan dan Sahkan PPK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPK '.$ppk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email_claimed_bpub->save();
                            }                            
                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada Pengesah.'));

                        } elseif('PPJHK' == $request->document_contract_type){
                           $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses pengesahan.'; 
                    
                            $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->role(['pengesah'])->get();
                            foreach($claim_task_bpub as $claimed_bpub){
                                $email_claimed_bpub = new EmailScheduler;
                                $email_claimed_bpub->status  = 'NEW';
                                // real email
                                $email_claimed_bpub->to      = $claimed_bpub->email;
                                // dummy data
                                // $email_claimed_bpub->to = "coins@ppj.gov.my";

                                $email_claimed_bpub->counter = 1;
                                $email_claimed_bpub->subject = 'Tindakan : Semakan dan Sahkan PPJHK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan PPJHK '.$ppjhk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email_claimed_bpub->save();
                            }
                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada Pengesah.'));
                        }
                        $workflow->is_claimed = 0;
                        $workflow->user_id = null;
                    } else {
                        $review->law_task_by    = $uu->law_task_by;
                        $workflow->flow_location = 'S5';
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';
                        $review->approved_by   = $request->input('approved_by');
                        $reviewlog->approved_by   = $request->input('approved_by');
                        $reviewlog1->approved_by   = $request->input('approved_by');
                        $workflow->is_claimed = 1;  

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan '.$request->type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }                      
                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    }

                } elseif ('S5' == $bpub->status && $request->type == $bpub->type && (($request->document_contract_type == $bpub->document_contract_type) || ($request->document_contract_type == $bpub_null->document_contract_type)) && '3' != user()->department_id) {
                    if ('IPC' == $bpub->type) {
                        $review->status        = 'S8';
                        $review->progress      = $bpub->progress;
                        $review->law_task_by    = $bpub->law_task_by;
                        $review->quiry_remarks = $bpub->remarks;
                        $review->approved_by   = null;
                        $review->department    = config('enums.BPUB');

                        $reviewlog->status        = 'CNBPUB';
                        $reviewlog->progress      = $bpub->progress;
                        $reviewlog->quiry_remarks = $bpub->remarks;
                        $reviewlog->approved_by   = null;
                        $reviewlog->department    = config('enums.BPUB');

                        $reviewlog1->status        = 'CNBPUB';
                        $reviewlog1->progress      = $bpub->progress;
                        $reviewlog1->approved_by   = null;
                        $reviewlog1->requested_by   = null;
                        $reviewlog1->department    = config('enums.BPUB');

                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan (Khas untuk Pegawai Kewangan).';
                        $workflow->flow_location = 'S8';
                        $workflow->is_claimed = 0;
                        $workflow->user_id = null;
                        $workflow->role_id = 5;
                        $workflow->workflow_team_id = 1;
                    
                        $claim_task_bpk = user()->whereIn('position_id',[67,2052,2032])->where('scheme_id',21)->whereIn('grade_id',[11,12,13,14,15,16])->role(['penyemak'])->get();
                        foreach($claim_task_bpk as $claimed_bpk){
                            $email_claimed_bpk = new EmailScheduler;
                            $email_claimed_bpk->status  = 'NEW';
                            // real email
                            $email_claimed_bpk->to      = $claimed_bpk->email;
                            // dummy data
                            // $email_claimed_bpk->to = "coins@ppj.gov.my";

                            $email_claimed_bpk->counter = 1;
                            $email_claimed_bpk->subject = 'Tindakan : Semakan dan Sahkan IPC';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email_claimed_bpk->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email_claimed_bpk->save();
                        }

                        audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada Pegawai Kewangan'));

                        $acq->status_task = config('enums.flow11');
                        $acq->update();
                        
                    } elseif ('VO' == $bpub->type) {
                        $review->status        = 'Selesai';
                        $review->progress      = $bpub->progress;
                        $review->approved_by = $review->created_by;
                        $review->quiry_remarks = $bpub->remarks;
                        $review->law_task_by    = $bpub->law_task_by;
                        $review->department    = config('enums.BPUB');

                        $reviewlog->status        = 'CNBPUB';
                        $reviewlog->progress      = $bpub->progress;
                        $reviewlog->approved_by = $review->created_by;
                        $reviewlog->quiry_remarks = $bpub->remarks;
                        $reviewlog->department    = config('enums.BPUB');

                        $reviewlog1->status        = 'CNBPUB';
                        $reviewlog1->progress      = $bpub->progress;
                        $reviewlog1->approved_by = $review->created_by;
                        $reviewlog1->quiry_remarks = $bpub->remarks;
                        $reviewlog1->department    = config('enums.BPUB');
                        $reviewlog1->reviews_status  = 'Selesai Semakan';

                        $acq->status_task = config('enums.flowALT2');
                        $acq->update();

                        if('PPK' == $request->document_contract_type){
                            $workflow->flow_desc = 'Perubahan Kerja (PPK) ' .$ppk->no. ' : Sila Muat Naik Borang Pengesahan PPK.';
                            $workflow->flow_location = 'PPK';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $review->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;   

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Muat Naik Borang Pengesahan PPK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Muat Naik Borang Pengesahan PPK '.$ppk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            // $vo_ppk = VO::where('id',$request->ppk_id)->where('no',$ppk->no)->first();
                            // $vo_ppk->status = 1;
                            // $vo_ppk->update();
                        } elseif('PPJHK' == $request->document_contract_type){
                            $workflow->flow_desc = 'Perubahan Kerja (PPJHK) ' .$ppjhk->no. ' : Sila Muat Naik Dokumen PPJHK.';
                            $workflow->flow_location = 'PPJHK';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $review->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            $vo_ppjhk = Ppjhk::where('id',$request->ppjhk_id)->where('no',$ppjhk->no)->first();
                            $vo_ppjhk->status = 9;
                            $vo_ppjhk->update(); 

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Muat Naik Dokumen PPJHK';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Muat Naik Dokumen PPJHK '.$ppjhk->no. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                        }

                    } elseif ('SOFA' == $bpub->type) {
                        $review->status        = 'Selesai';
                        $review->progress      = $bpub->progress;
                        $review->approved_by = $review->created_by;
                        $review->quiry_remarks = $bpub->remarks;
                        $review->law_task_by    = $bpub->law_task_by;
                        // $review->approved_by   = $request->input('approved_by');
                        $review->department    = config('enums.BPUB');

                        $reviewlog->status        = 'CNBPUB';
                        $reviewlog->progress      = $bpub->progress;
                        $reviewlog->approved_by = $review->created_by;
                        $reviewlog->quiry_remarks = $bpub->remarks;
                        // $reviewlog->approved_by   = $request->input('approved_by');
                        $reviewlog->department    = config('enums.BPUB');

                        $reviewlog1->status        = 'CNBPUB';
                        $reviewlog1->progress      = $bpub->progress;
                        $reviewlog1->approved_by = $review->created_by;
                        $reviewlog1->quiry_remarks = $bpub->remarks;
                        $reviewlog1->department    = config('enums.BPUB');
                        $reviewlog1->reviews_status  = 'Selesai Semakan';

                        $workflow->flow_desc = 'Bayaran Muktamad : Sila Muat naik dokumen SOFA.';
                        $workflow->url = '/post/sofa/'.$acq->sst->hashslug.'/edit#upload-sofa';
                        $workflow->flow_location = 'SOFA';
                        $workflow->is_claimed = 1;
                        $workflow->user_id = $request->created_by;
                        $workflow->role_id = 4;
                        $workflow->workflow_team_id = 1;  

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Muat naik dokumen SOFA';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Muat naik dokumen SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }  

                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Sudah Selesai.'));

                        $acq->status_task = config('enums.flow16');
                        $acq->update();

                    } elseif('EOT' == $bpub->type){
                        $review->status        = 'Selesai';
                        $review->progress      = $bpub->progress;
                        $review->approved_by = $review->created_by;
                        $review->law_task_by    = $bpub->law_task_by;
                        $review->department    = config('enums.BPUB');

                        $reviewlog->status        = 'CNBPUB';
                        $reviewlog->progress      = $bpub->progress;
                        $reviewlog->approved_by = $review->created_by;
                        $reviewlog->department    = config('enums.BPUB');

                        $reviewlog1->status        = 'CNBPUB';
                        $reviewlog1->progress      = $bpub->progress;
                        $reviewlog1->approved_by = $review->created_by;
                        $reviewlog1->department    = config('enums.BPUB');

                        $eot = EOT::find($request->eot_id);
                        $eot->status = 2;
                        $eot->update();

                        if($acq->approval->acquisition_type_id == 2){   
                                     
                            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila isi maklumat kelulusan dan muat naik keputusan mesyuarat';    
                    
                            // $claim_task_bpub = user()->where('department_id',9)->role(['urusetia'])->get();
                            // foreach($claim_task_bpub as $claimed_bpub){
                            //     $email_claimed_bpub = new EmailScheduler;
                            //     $email_claimed_bpub->status  = 'NEW';
                            //     // real email
                            //     $email_claimed_bpub->to      = $claimed_bpub->email;
                            //     // dummy data
                            //     // $email_claimed_bpub->to = "coins@ppj.gov.my";

                            //     $email_claimed_bpub->counter = 1;
                            //     $email_claimed_bpub->subject = 'Tindakan : Semakan dan Sahkan EOT';
                            //     $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            //     $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT '.$eot->bil. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            //     $email_claimed_bpub->save();
                            // }                      

                            audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Sudah Selesai.'));

                        } else {  
                                     
                            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila isi maklumat kelulusan dan muat naik keputusan mesyuarat'; 
                    
                            // $claim_task_bpub = user()->where('department_id',9)->role(['urusetia'])->get();
                            // foreach($claim_task_bpub as $claimed_bpub){
                            //     $email_claimed_bpub = new EmailScheduler;
                            //     $email_claimed_bpub->status  = 'NEW';
                            //     // real email
                            //     $email_claimed_bpub->to      = $claimed_bpub->email;
                            //     // dummy data
                            //     // $email_claimed_bpub->to = "coins@ppj.gov.my";

                            //     $email_claimed_bpub->counter = 1;
                            //     $email_claimed_bpub->subject = 'Tindakan : Semakan dan Sahkan EOT';
                            //     $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            //     $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan EOT </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            //     $email_claimed_bpub->save();
                            // }

                            audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Sudah Selesai'));
                                                    
                        }
                        $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#extension';
                        $workflow->flow_location = 'EOT';
                        $workflow->is_claimed = 1;
                        $workflow->user_id = null;
                        $workflow->role_id = 6;
                        $workflow->workflow_team_id = 1;

                        $acq->status_task = config('enums.flowALT1');
                        $acq->update();
                    } elseif('Dokumen Kontrak' == $bpub->type && ('Bon Pelaksanaan' == $request->document_contract_type || 'Insurans' == $request->document_contract_type || 'Wang Pendahuluan' == $request->document_contract_type || 'CNC' == $request->document_contract_type || 'CPC' == $request->document_contract_type || 'CPO' == $request->document_contract_type || 'CMGD' == $request->document_contract_type)) {
                        if('Bon Pelaksanaan' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';

                            $workflow->flow_desc = 'Semakan : Selesai';
                            $workflow->flow_location = 'Bon Pelaksanaan';
                            $workflow->url = '';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = null;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                        } elseif('Insurans' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';

                            $workflow->flow_desc = 'Semakan : Selesai';
                            $workflow->flow_location = 'Insurans';
                            $workflow->url = '';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = null;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));
                            
                        } elseif('Wang Pendahuluan' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';

                            $workflow->flow_desc = 'Semakan : Selesai';
                            $workflow->flow_location = 'Wang Pendahuluan';
                            $workflow->url = '';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = null;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));
                            
                        } elseif('CPC' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';

                            if($acq->approval->acquisition_type_id == '1' || $acq->approval->acquisition_type_id == '3' || $acq->approval->acquisition_type_id == '4'){
                            
                                $workflow->flow_desc = 'Pelepasan Bon : Sila teruskan dengan Pelepasan Bon.';
                                $workflow->flow_location = 'Pelepasan Bon';
                                $workflow->url = '/post/release/'.$acq->sst->hashslug.'/edit';
                                $workflow->is_claimed = 1;
                                $workflow->user_id = $request->created_by;
                                $workflow->role_id = 4;
                                $workflow->workflow_team_id = 1;

                                // $workflow2->flow_desc = 'Semakan : Selesai.'; 
                                // $workflow2->flow_location = 'Pelepasan Bon'; 
                                // $workflow2->is_claimed = 1;
                                // $workflow2->user_id = null;
                                // $workflow2->workflow_team_id = 2;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan Pelepasan Bon';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pelepasan Bon </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } else {
                                $workflow->flow_desc = 'Sijil Siap Membaiki Kecacatan : Sila teruskan dengan CMGD.';
                                $workflow->flow_location = 'CMGD';
                                $workflow->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                $workflow->is_claimed = 1;
                                $workflow->user_id = $request->created_by;
                                $workflow->role_id = 4;
                                $workflow->workflow_team_id = 1;

                                // $workflow2->flow_desc = 'Semakan : Selesai.'; 
                                // $workflow2->flow_location = 'CMGD'; 
                                // $workflow2->is_claimed = 1;
                                // $workflow2->user_id = null;
                                // $workflow2->workflow_team_id = 2; 

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan CMGD';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CMGD </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            }

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            $acq->status_task = config('enums.flow20');
                            $acq->update();
                        } elseif('CNC' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';

                            $workflow->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                            $workflow->flow_location = 'CPC';
                            $workflow->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $request->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            // $workflow2->flow_desc = 'Semakan : Selesai.'; 
                            // $workflow2->flow_location = 'CPC'; 
                            // $workflow2->is_claimed = 1;
                            // $workflow2->user_id = null;
                            // $workflow2->workflow_team_id = 2;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Penyediaan CPC';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            $acq->status_task = config('enums.flow17');
                            $acq->update();
                        } elseif('CPO' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';
                            
                            $workflow->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                            $workflow->flow_location = 'CPC';
                            $workflow->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $request->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            // $workflow2->flow_desc = 'Semakan : Selesai.';  
                            // $workflow2->flow_location = 'CPC';
                            // $workflow2->is_claimed = 1;
                            // $workflow2->user_id = null;
                            // $workflow2->workflow_team_id = 2;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Penyediaan CPC';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            $acq->status_task = config('enums.flow17');
                            $acq->update();
                        } elseif('CMGD' == $request->document_contract_type) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';
                            $review->progress      = $bpub->progress;
                            $review->quiry_remarks = $bpub->remarks;
                            $review->department    = config('enums.BPUB');

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';
                            $reviewlog->progress      = $bpub->progress;
                            $reviewlog->quiry_remarks = $bpub->remarks;
                            $reviewlog->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->quiry_remarks = $bpub->remarks;
                            $reviewlog1->department    = config('enums.BPUB');
                            $reviewlog1->reviews_status  = 'Selesai Semakan';
                            
                            $workflow->flow_desc = 'Pelepasan Bon : Sila teruskan dengan Pelepasan Bon.';
                            $workflow->flow_location = 'Pelepasan Bon';
                            $workflow->url = '/post/release/'.$acq->sst->hashslug.'/edit';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $request->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            // $workflow2->flow_desc = 'Semakan : Selesai.'; 
                            // $workflow2->flow_location = 'Pelepasan Bon'; 
                            // $workflow2->is_claimed = 1;
                            // $workflow2->user_id = null;
                            // $workflow2->workflow_team_id = 2;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Penyediaan Pelepasan Bon';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pelepasan Bon </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            $acq->status_task = config('enums.flow21');
                            $acq->update();
                        }

                    } else {
                        $review->status        = 'CN';
                        $review->approved_by = $review->created_by;
                        $review->progress      = $bpub->progress;
                        if('SOFA' == $request->type){
                            $review->law_task_by    = $bpub->law_task_by;
                        } else {
                            $review->law_task_by    = $uu->law_task_by;
                        }
                        $review->quiry_remarks = $bpub->remarks;
                        // $review->approved_by   = $request->input('approved_by');
                        $review->department    = config('enums.BPUB');

                        $reviewlog->status        = 'CNBPUB';
                        $reviewlog->approved_by = $review->created_by;
                        $reviewlog->progress      = $bpub->progress;
                        $reviewlog->quiry_remarks = $bpub->remarks;
                        // $reviewlog->approved_by   = $request->input('approved_by');
                        $reviewlog->department    = config('enums.BPUB');
                        
                        if ('CN' == $uu->status) {
                            $review->approved_by = $review->created_by;
                            $review->status      = 'Selesai';

                            $reviewlog->approved_by = $review->created_by;
                            $reviewlog->status      = 'CNBPUB';

                            $reviewlog1->status        = 'CNBPUB';
                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->requested_by = $review->created_by;
                            $reviewlog1->progress      = $bpub->progress;
                            $reviewlog1->department    = config('enums.BPUB');

                            $reviewlog1->approved_by = $review->created_by;
                            $reviewlog1->requested_by = $review->created_by;
                            $reviewlog1->status      = 'CNBPUB';
                            $reviewlog1->reviews_status  = 'Selesai Semakan';

                            if('Acquisitions' == $request->type){
                                if($acq->briefing_status == 1){
                                    $workflow->flow_desc = 'Taklimat : Sila teruskan dengan Taklimat.';
                                    $workflow->url = '/briefing/'.$acq->hashslug.'/listing';   
                                    $workflow->flow_location = 'Taklimat'; 
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;

                                    // if (! empty($review->approved_by)) {
                                    //     $email->status  = 'NEW';
                                    //     // real email
                                        // $email->to      = $review->approve->email;
                                    //     // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                    //     $email->counter = 1;
                                    //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                    //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                    //     $email->save();
                                    // } else {
                                    //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    // } 

                                    audit($review, __('Semakan Dokumen Perolehan ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada '.$review->create->name));

                                    $acq->status_task = config('enums.flow4');
                                    $acq->verify_status = null;
                                    $acq->update();
                                } else {
                                    if($acq->visit_status == 1){
                                        $workflow->flow_desc = 'Lawatan Tapak : Sila kemaskini kehadiran lawatan tapak.';
                                        $workflow->url = '/site-visit/'.$acq->hashslug.'/edit';   
                                        $workflow->flow_location = 'Lawatan Tapak'; 
                                        $workflow->is_claimed = 1;
                                        $workflow->user_id = null;
                                        $workflow->workflow_team_id = 1;

                                        // $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->get();
                                        // foreach($claim_task_bpub as $claimed_bpub){
                                        //     $email_claimed_bpub = new EmailScheduler;
                                        //     $email_claimed_bpub->status  = 'NEW';
                                        //     // real email
                                        //     // $email_claimed_bpub->to      = $claimed_bpub->email;
                                        //     // dummy data
                                            // $email_claimed_bpub->to = "coins@ppj.gov.my";

                                        //     $email_claimed_bpub->counter = 1;
                                        //     $email_claimed_bpub->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                        //     $email_claimed_bpub->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $claimed_bpub->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                        //     $email_claimed_bpub->save();
                                        // } 

                                        audit($review, __('Semakan Dokumen Perolehan ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada Kaunter BPUB bagi modul Lawatan Tapak'));

                                        $acq->status_task = config('enums.flow5');
                                        $acq->verify_status = null;
                                        $acq->update();
                                    } else {
                                        $workflow->flow_desc = 'Jualan : Sila pastikan jualan perolehan wujud dan isi nombor resit.';
                                        $workflow->url = '/sale';   
                                        $workflow->flow_location = 'Jualan'; 
                                        $workflow->user_id = null;
                                        $workflow->is_claimed = 1;
                                        // $workflow->user_id = $request->created_by;
                                        $workflow->workflow_team_id = 1;

                                        // $claim_task_bpub = user()->role(['kaunter'])->get();
                                        // foreach($claim_task_bpub as $claimed_bpub){
                                        //     $email_claimed_bpub = new EmailScheduler;
                                        //     $email_claimed_bpub->status  = 'NEW';
                                        //     // real email
                                        //     // $email_claimed_bpub->to      = $claimed_bpub->email;
                                        //     // dummy data
                                            // $email_claimed_bpub->to = "coins@ppj.gov.my";

                                        //     $email_claimed_bpub->counter = 1;
                                        //     $email_claimed_bpub->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                        //     $email_claimed_bpub->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $claimed_bpub->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                        //     $email_claimed_bpub->save();
                                        // } 

                                        audit($review, __('Semakan Dokumen Perolehan ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada Kaunter BPUB bagi modul Jualan'));

                                        $acq->status_task = config('enums.flow6');
                                        $acq->verify_status = null;
                                        $acq->update();
                                    }
                                }
                            } elseif('SST' == $request->type){
                                // $workflow->flow_desc = 'Dokumen Kontrak : Sila teruskan dengan Dokumen Kontrak.';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit';
                                // $workflow->flow_location = 'Dokumen Kontrak'; 
                                // $workflow->is_claimed = 1;
                                // $workflow->user_id = $request->created_by;
                                // $workflow->workflow_team_id = 1;

                                // $acq->status_task = config('enums.flow10');
                                // $acq->update();

                                $workflow->flow_desc = 'Surat Setuju Terima : Sila muat naik kelulusan SST.';
                                $workflow->url = '/acceptance-letter/'.$company->hashslug.'#upload';
                                $workflow->flow_location = 'SST'; 
                                $workflow->is_claimed = 1;
                                $workflow->user_id = $request->created_by;
                                $workflow->role_id = 4;
                                $workflow->workflow_team_id = 1; 

                                $sst->status = 2;
                                $sst->update();

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Muat naik kelulusan SST';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Muat naik kelulusan SST </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }   

                                $acq->verify_status = null;
                                $acq->update();

                                audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));


                            } elseif('Dokumen Kontrak' == $request->type){
                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',1)->first();
                                $workflow3 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                                $workflow4 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Bon Pelaksanaan')->where('workflow_team_id',1)->first();
                                $workflow5 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Bon Pelaksanaan')->where('workflow_team_id',2)->first();
                                if ('CPC' == $request->document_contract_type){
                                    $workflow2->flow_desc = 'Sijil Siap Membaiki Kecacatan : Sila teruskan dengan CMGD.';
                                    $workflow2->flow_location = 'CMGD';
                                    $workflow2->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = $request->created_by;
                                    $workflow2->role_id = 4;
                                    $workflow2->workflow_team_id = 1;

                                    $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                    $workflow3->flow_location = 'CMGD'; 
                                    $workflow3->is_claimed = 1;
                                    $workflow3->user_id = null;
                                    $workflow3->workflow_team_id = 2; 

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan CMGD';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CMGD </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                    $acq->status_task = config('enums.flow20');
                                    $acq->update();

                                } elseif ('CNC' == $request->document_contract_type){
                                    $workflow2->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                                    $workflow2->flow_location = 'CPC';
                                    $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = $request->created_by;
                                    $workflow2->role_id = 4;
                                    $workflow2->workflow_team_id = 1;

                                    $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                    $workflow3->flow_location = 'CPC'; 
                                    $workflow3->is_claimed = 1;
                                    $workflow3->user_id = null;
                                    $workflow3->workflow_team_id = 2;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan CPC';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                    $acq->status_task = config('enums.flow17');
                                    $acq->update();

                                } elseif ('CPO' == $request->document_contract_type){
                                    $workflow2->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                                    $workflow2->flow_location = 'CPC';
                                    $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = $request->created_by;
                                    $workflow2->role_id = 4;
                                    $workflow2->workflow_team_id = 1;

                                    $workflow3->flow_desc = 'Semakan : Selesai.';  
                                    $workflow3->flow_location = 'CPC';
                                    $workflow3->is_claimed = 1;
                                    $workflow3->user_id = null;
                                    $workflow3->workflow_team_id = 2;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan CPC';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                    $acq->status_task = config('enums.flow17');
                                    $acq->update();

                                } elseif ('CMGD' == $request->document_contract_type){
                                    $workflow2->flow_desc = 'Pelepasan Bon : Sila teruskan dengan Pelepasan Bon.';
                                    $workflow2->flow_location = 'Pelepasan Bon';
                                    $workflow2->url = '/post/release/'.$acq->sst->hashslug.'/edit';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = $request->created_by;
                                    $workflow2->role_id = 4;
                                    $workflow2->workflow_team_id = 1;

                                    $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                    $workflow3->flow_location = 'Pelepasan Bon'; 
                                    $workflow3->is_claimed = 1;
                                    $workflow3->user_id = null;
                                    $workflow3->workflow_team_id = 2;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan Pelepasan Bon';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pelepasan Bon </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                    $acq->status_task = config('enums.flow21');
                                    $acq->update();

                                } elseif('Amaran' == $request->document_contract_type) {
                                    $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();
                                    $workflow2->url = '/termination/warning/'.$warn->hashslug;
                                    $workflow2->flow_location = 'Amaran';

                                    $workflow2->flow_desc = '';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = null;
                                    $workflow2->workflow_team_id = 1;

                                    $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                    $workflow3->flow_location = 'Pelepasan Bon'; 
                                    $workflow3->is_claimed = 1;
                                    $workflow3->user_id = null;
                                    $workflow3->workflow_team_id = 2;

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Sudah Selesai'));

                                    $acq->status_task = config('enums.flow21');
                                    $acq->update();

                                }  elseif('Wang Pendahuluan' == $request->document_contract_type) {

                                    $workflow2->flow_desc = 'Semakan : Selesai.'; 
                                    $workflow2->flow_location = 'Wang Pendahuluan'; 
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = null;
                                    $workflow2->workflow_team_id = 1;

                                    // if (! empty($review->approved_by)) {
                                    //     $email->status  = 'NEW';
                                    //     // real email
                                        // $email->to      = $review->approve->email;
                                    //     // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                    //     $email->counter = 1;
                                    //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                    //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                    //     $email->save();
                                    // } else {
                                    //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    // }
                                    $acq->verify_status = null;
                                    $acq->update();

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                } 
                                // elseif(!empty($ins) && !empty($doc) && 'Selesai' == $ins->status && 'Selesai' == $doc->status){
                                //     $workflow4->flow_desc = 'Pemantauan Projek : Sila muat naik jadual item dan carta kemajuan fizikal.';  
                                //     $workflow4->flow_location = 'Pemantauan Projek';
                                //     $workflow4->url = '/acquisition/monitoring/'.$acq->sst->hashslug.'#physical';
                                //     $workflow4->is_claimed = 1;
                                //     $workflow4->user_id = $request->created_by;
                                //     $workflow4->role_id = 4;
                                //     $workflow4->workflow_team_id = 1;

                                //     $workflow5->flow_desc = 'Semakan : Selesai.';  
                                //     $workflow5->is_claimed = 1;
                                //     $workflow5->user_id = null;
                                //     $workflow5->workflow_team_id = 2;

                                //     if (! empty($review->approved_by)) {
                                //         $email->status  = 'NEW';
                                //         // real email
                                //         $email->to      = $review->approve->email;
                                //         // dummy data
                                //         // $email->to = "coins@ppj.gov.my";

                                //         $email->counter = 1;
                                //         $email->subject = 'Tindakan : Penyediaan Pemantauan Projek';
                                //         $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pemantauan Projek </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                //         $email->save();
                                //     } else {
                                //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                //     }

                                //     audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                //     $acq->status_task = config('enums.flow12');
                                //     $acq->verify_status = null;
                                //     $acq->update();

                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first()->delete();
                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first()->delete();
                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first()->delete();
                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first()->delete();
                                // } elseif(!empty($bon) && !empty($doc) && 'Selesai' == $bon->status && 'Selesai' == $doc->status){
                                //     $workflow4->flow_desc = 'Pemantauan Projek : Sila muat naik jadual item dan carta kemajuan fizikal.';  
                                //     $workflow4->flow_location = 'Pemantauan Projek';
                                //     $workflow4->url = '/acquisition/monitoring/'.$acq->sst->hashslug.'#physical';
                                //     $workflow4->is_claimed = 1;
                                //     $workflow4->user_id = $request->created_by;
                                //     $workflow4->role_id = 4;
                                //     $workflow4->workflow_team_id = 1;

                                //     $workflow5->flow_desc = 'Semakan : Selesai.';  
                                //     $workflow5->is_claimed = 1;
                                //     $workflow5->user_id = null;
                                //     $workflow5->workflow_team_id = 2;

                                //     if (! empty($review->approved_by)) {
                                //         $email->status  = 'NEW';
                                //         // real email
                                //         $email->to      = $review->approve->email;
                                //         // dummy data
                                //         // $email->to = "coins@ppj.gov.my";

                                //         $email->counter = 1;
                                //         $email->subject = 'Tindakan : Penyediaan Pemantauan Projek';
                                //         $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pemantauan Projek </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                //         $email->save();
                                //     } else {
                                //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                //     }

                                //     audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                //     $acq->status_task = config('enums.flow12');
                                //     $acq->verify_status = null;
                                //     $acq->update();

                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first()->delete();
                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first()->delete();
                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first()->delete();
                                //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first()->delete();
                                // } 
                                elseif(!empty($bon) && !empty($ins) && 'Selesai' == $bon->status && 'Selesai' == $ins->status){
                                    $workflow4->flow_desc = 'Pemantauan Projek : Sila muat naik jadual item dan carta kemajuan fizikal.';  
                                    $workflow4->flow_location = 'Pemantauan Projek';
                                    $workflow4->url = '/acquisition/monitoring/'.$acq->sst->hashslug.'#physical';
                                    $workflow4->is_claimed = 1;
                                    $workflow4->user_id = $request->created_by;
                                    $workflow4->role_id = 4;
                                    $workflow4->workflow_team_id = 1;

                                    $workflow5->flow_desc = 'Semakan : Selesai.';  
                                    $workflow5->is_claimed = 1;
                                    $workflow5->user_id = null;
                                    $workflow5->workflow_team_id = 2;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan Pemantauan Projek';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pemantauan Projek </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                    $acq->status_task = config('enums.flow12');
                                    $acq->verify_status = null;
                                    $acq->update();

                                    WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first()->delete();
                                    WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first()->delete();
                                    WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first()->delete();
                                    WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first()->delete();
                                } 
                                    // elseif (empty($bon->status == 'Selesai') || empty($ins->status == 'Selesai') || empty($doc->status == 'Selesai') || empty($wang->status == 'Selesai')) {
                                    //     $workflow->flow_desc = 'Semakan : Selesai.';  
                                    //     $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit';
                                    //     $workflow->is_claimed = 1;
                                    //     $workflow->user_id = null;
                                    //     $workflow->workflow_team_id = 1;

                                    //     $acq->status_task = config('enums.flow10');
                                    //     $acq->verify_status = null;
                                    //     $acq->update();

                                    // }
                                 else {
                                    $workflow->flow_desc = 'Semakan : Selesai.';  
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = null;
                                    $workflow->workflow_team_id = 1;

                                    $acq->status_task = config('enums.flow10');
                                    $acq->verify_status = null;
                                    $acq->update();
                                } 
                            } elseif('EOT' == $request->type){
                                $workflow->flow_desc = 'Bayaran Muktamad : Sila teruskan dengan SOFA.';  
                                $workflow->flow_location = 'SOFA';  
                                $workflow->is_claimed = 1;
                                $workflow->user_id = $request->created_by;
                                $workflow->role_id = 4;
                                $workflow->workflow_team_id = 1;

                                if($acq->approval->acquisition_type_id == 2){    
                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan SOFA';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }                        

                                    audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                } else {  
                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Penyediaan SOFA';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }
                                                          
                                    audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));
                                                            
                                }

                                $acq->status_task = config('enums.flow16');
                                $acq->update();
                            }


                            // if ('Draft Penyedia' == $acq->status_task || 'Semakan Dokumen Perolehan' == $acq->status_task) {
                            //     $review->type     = 'SST';
                            //     $review2->type    = 'Acquisitions';
                            //     $acq->status_task = config('enums.flow9');
                            // } elseif ('SST' == $acq->status_task || 'Cetak Notis' == $acq->status_task) {
                            //     $review->type                   = 'Dokumen Kontrak';
                            //     $review->document_contract_type = 'Bon Pelaksanaan';
                            //     $review2->type                  = 'SST';

                            //     $review3->status                 = 'CN';
                            //     $review3->progress               = 1;
                            //     $review3->department             = config('enums.UU');
                            //     $review3->acquisition_id         = $request->input('acquisition_id');
                            //     $review3->requested_by           = $request->input('requested_by');
                            //     $review3->requested_at           = Carbon::now();
                            //     $review3->approved_at            = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            //     $review3->remarks                = $request->input('remarks');
                            //     $review3->approved_by            = $review->created_by;
                            //     $review3->created_by             = $review->created_by;
                            //     $review3->task_by                = $request->input('task_by');
                            //     $review3->type                   = 'Dokumen Kontrak';
                            //     $review3->document_contract_type = 'Insurans';

                            //     $review4->status                 = 'CN';
                            //     $review4->progress               = 1;
                            //     $review4->department             = config('enums.UU');
                            //     $review4->acquisition_id         = $request->input('acquisition_id');
                            //     $review4->requested_by           = $request->input('requested_by');
                            //     $review4->requested_at           = Carbon::now();
                            //     $review4->approved_at            = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            //     $review4->remarks                = $request->input('remarks');
                            //     $review4->approved_by            = $review->created_by;
                            //     $review4->created_by             = $review->created_by;
                            //     $review4->task_by                = $request->input('task_by');
                            //     $review4->type                   = 'Dokumen Kontrak';
                            //     $review4->document_contract_type = 'Wang Pendahuluan';

                            //     $review5->status                 = 'CN';
                            //     $review5->progress               = 1;
                            //     $review5->department             = config('enums.UU');
                            //     $review5->acquisition_id         = $request->input('acquisition_id');
                            //     $review5->requested_by           = $request->input('requested_by');
                            //     $review5->requested_at           = Carbon::now();
                            //     $review5->approved_at            = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            //     $review5->remarks                = $request->input('remarks');
                            //     $review5->approved_by            = $review->created_by;
                            //     $review5->created_by             = $review->created_by;
                            //     $review5->task_by                = $request->input('task_by');
                            //     $review5->type                   = 'Dokumen Kontrak';
                            //     $review5->document_contract_type = 'Dokumen Kontrak';
                            //     $acq->status_task                = config('enums.flow10');
                            // } elseif ('Dokumen Kontrak' == $acq->status_task) {
                            //     if (('CN' == $bon->status && 'CN' == $ins->status && 'S5' == $doc->status) || ('S5' == $bon->status && 'CN' == $ins->status && 'CN' == $doc->status) || ('CN' == $bon->status && 'S5' == $ins->status && 'CN' == $doc->status)) {
                            //         $review->type     = 'IPC';
                            //         $review2->type    = 'Dokumen Kontrak';
                            //         $acq->status_task = config('enums.flow11');
                            //     } else {
                            //         return $doc;
                            //         $review2->status         = null;
                            //         $review2->progress       = null;
                            //         $review2->department     = null;
                            //         $review2->acquisition_id = null;
                            //         $review2->requested_by   = null;
                            //         $review2->requested_at   = null;
                            //         $review2->approved_at    = null;
                            //         $review2->remarks        = null;
                            //         $review2->approved_by    = null;
                            //         $review2->task_by        = null;
                            //         $review->type            = 'Dokumen Kontrak';
                            //         $review2->type           = '';
                            //     }
                            // } elseif ('IPC' == $acq->status_task) {
                            //     $review->type     = 'VO';
                            //     $review2->type    = 'IPC';
                            //     $acq->status_task = config('enums.flowALT1');
                            // } elseif ('VO' == $acq->status_task) {
                            //     $review->type     = 'EOT';
                            //     $review2->type    = 'VO';
                            //     $acq->status_task = config('enums.flow14');
                            // } elseif ('EOT' == $acq->status_task) {
                            //     $review->type     = 'SOFA';
                            //     $review2->type    = 'EOT';
                            //     $acq->status_task = config('enums.flowALT2');
                            // } elseif ('SOFA' == $acq->status_task) {
                            //     $review->type     = 'Selesai';
                            //     $review2->type    = 'SOFA';
                            //     $acq->status_task = 'Selesai';
                            // }

                            // $review2->status         = 'CN';
                            // $review2->progress       = 1;
                            // $review2->department     = config('enums.BPUB');
                            // $review2->acquisition_id = $request->input('acquisition_id');
                            // $review2->requested_by   = $request->input('requested_by');
                            // $review2->requested_at   = Carbon::now();
                            // $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                            // $review2->remarks        = $request->input('remarks');
                            // $review2->approved_by    = $review->created_by;
                            // $review2->task_by        = $request->input('task_by');
                            // // $review2->type           = 'Acquisitions';
                            // $review2->created_by = $bpub->created_by;

                            // $acq->update();
                        } else {
                            if(!empty($ppn) && !empty($ppn->request->department_id == '3') && ($uu->status == 'S7')){
            
                                $acq->status_task = config('enums.flow1');
                                $acq->verify_status = null;
                                $acq->update();

                                $reviewlog1->status        = 'CNBPUB';
                                $reviewlog1->approved_by = $review->created_by;
                                $reviewlog1->requested_by = $review->created_by;
                                $reviewlog1->progress      = $bpub->progress;
                                $reviewlog1->department    = config('enums.BPUB');
                                $reviewlog1->reviews_status  = 'Terima';
                                if('Acquisitions' == $request->type){
                                    $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
                                    $workflow->flow_location = 'S5';
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first()->delete();
                                } elseif('SST' == $request->type){
                                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
                                    $workflow->flow_location = 'S5';
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;

                                    $sst->status = null;
                                    $sst->update();

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan SST';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan SST </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }
                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first()->delete();
                                } elseif('Dokumen Kontrak' == $request->type){
                                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila tekan butang hantar untuk proses penyemakan.';

                                    if('Bon Pelaksanaan' == $request->document_contract_type){
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#bon-implementation';
                                    } elseif('Insurans' == $request->document_contract_type){
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#insurance';
                                    } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#document-contract';
                                    } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#cash-deposit';
                                    } elseif('CPC' == $request->document_contract_type) {
                                        $review->approved_by = $review->created_by;
                                        $review->status      = 'Selesai';

                                        $reviewlog->approved_by = $review->created_by;
                                        $reviewlog->status      = 'CNBPUB';

                                        $reviewlog1->approved_by = $review->created_by;
                                        $reviewlog1->status      = 'CNBPUB';
                                        $reviewlog1->reviews_status  = 'Selesai Semakan';

                                        $workflow2->flow_desc = 'Sijil Siap Membaiki Kecacatan : Sila teruskan dengan CMGD.';
                                        $workflow2->flow_location = 'CMGD';
                                        $workflow2->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                        $workflow2->is_claimed = 1;
                                        $workflow2->user_id = $request->created_by;
                                        $workflow2->role_id = 4;
                                        $workflow2->workflow_team_id = 1;

                                        $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                        $workflow3->flow_location = 'CMGD'; 
                                        $workflow3->is_claimed = 1;
                                        $workflow3->user_id = null;
                                        $workflow3->workflow_team_id = 2; 

                                        if (! empty($review->approved_by)) {
                                            $email->status  = 'NEW';
                                            // real email
                                            $email->to      = $review->approve->email;
                                            // dummy data
                                            // $email->to = "coins@ppj.gov.my";

                                            $email->counter = 1;
                                            $email->subject = 'Tindakan : Penyediaan CMGD';
                                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CMGD : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                            $email->save();
                                        } else {
                                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                        }

                                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                        $acq->status_task = config('enums.flow20');
                                        $acq->update();
                                    } elseif('CNC' == $request->document_contract_type) {
                                        $review->approved_by = $review->created_by;
                                        $review->status      = 'Selesai';

                                        $reviewlog->approved_by = $review->created_by;
                                        $reviewlog->status      = 'CNBPUB';

                                        $reviewlog1->approved_by = $review->created_by;
                                        $reviewlog1->status      = 'CNBPUB';
                                        $reviewlog1->reviews_status  = 'Selesai Semakan';

                                        $workflow2->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                                        $workflow2->flow_location = 'CPC';
                                        $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                        $workflow2->is_claimed = 1;
                                        $workflow2->user_id = $request->created_by;
                                        $workflow2->role_id = 4;
                                        $workflow2->workflow_team_id = 1;

                                        $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                        $workflow3->flow_location = 'CPC'; 
                                        $workflow3->is_claimed = 1;
                                        $workflow3->user_id = null;
                                        $workflow3->workflow_team_id = 2;

                                        if (! empty($review->approved_by)) {
                                            $email->status  = 'NEW';
                                            // real email
                                            $email->to      = $review->approve->email;
                                            // dummy data
                                            // $email->to = "coins@ppj.gov.my";

                                            $email->counter = 1;
                                            $email->subject = 'Tindakan : Penyediaan CPC';
                                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                            $email->save();
                                        } else {
                                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                        }

                                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                        $acq->status_task = config('enums.flow17');
                                        $acq->update();
                                    } elseif('CPO' == $request->document_contract_type) {
                                        $review->approved_by = $review->created_by;
                                        $review->status      = 'Selesai';

                                        $reviewlog->approved_by = $review->created_by;
                                        $reviewlog->status      = 'CNBPUB';

                                        $reviewlog1->approved_by = $review->created_by;
                                        $reviewlog1->status      = 'CNBPUB';
                                        $reviewlog1->reviews_status  = 'Selesai Semakan';
                                        
                                        $workflow2->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                                        $workflow2->flow_location = 'CPC';
                                        $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                        $workflow2->is_claimed = 1;
                                        $workflow2->user_id = $request->created_by;
                                        $workflow2->role_id = 4;
                                        $workflow2->workflow_team_id = 1;

                                        $workflow3->flow_desc = 'Semakan : Selesai.';  
                                        $workflow3->flow_location = 'CPC';
                                        $workflow3->is_claimed = 1;
                                        $workflow3->user_id = null;
                                        $workflow3->workflow_team_id = 2;

                                        if (! empty($review->approved_by)) {
                                            $email->status  = 'NEW';
                                            // real email
                                            $email->to      = $review->approve->email;
                                            // dummy data
                                            // $email->to = "coins@ppj.gov.my";

                                            $email->counter = 1;
                                            $email->subject = 'Tindakan : Penyediaan CPC';
                                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                            $email->save();
                                        } else {
                                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                        }

                                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                        $acq->status_task = config('enums.flow17');
                                        $acq->update();
                                    } elseif('CMGD' == $request->document_contract_type) {
                                        $review->approved_by = $review->created_by;
                                        $review->status      = 'Selesai';

                                        $reviewlog->approved_by = $review->created_by;
                                        $reviewlog->status      = 'CNBPUB';

                                        $reviewlog1->approved_by = $review->created_by;
                                        $reviewlog1->status      = 'CNBPUB';
                                        $reviewlog1->reviews_status  = 'Selesai Semakan';
                                        
                                        $workflow2->flow_desc = 'Pelepasan Bon : Sila teruskan dengan Pelepasan Bon.';
                                        $workflow2->flow_location = 'Pelepasan Bon';
                                        $workflow2->url = '/post/release/'.$acq->sst->hashslug.'/edit';
                                        $workflow2->is_claimed = 1;
                                        $workflow2->user_id = $request->created_by;
                                        $workflow2->role_id = 4;
                                        $workflow2->workflow_team_id = 1;

                                        $workflow3->flow_desc = 'Semakan : Selesai.'; 
                                        $workflow3->flow_location = 'Pelepasan Bon'; 
                                        $workflow3->is_claimed = 1;
                                        $workflow3->user_id = null;
                                        $workflow3->workflow_team_id = 2;

                                        if (! empty($review->approved_by)) {
                                            $email->status  = 'NEW';
                                            // real email
                                            $email->to      = $review->approve->email;
                                            // dummy data
                                            // $email->to = "coins@ppj.gov.my";

                                            $email->counter = 1;
                                            $email->subject = 'Tindakan : Penyediaan Pelepasan Bon';
                                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pelepasan Bon : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                            $email->save();
                                        } else {
                                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                        }

                                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                        $acq->status_task = config('enums.flow21');
                                        $acq->update();
                                    } elseif('Amaran' == $request->document_contract_type) {
                                        $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();

                                        $workflow->url = '/termination/warning/'.$warn->hashslug.'/edit';
                                    }
                                    // $workflow->is_claimed = 1;
                                    // $workflow->user_id = null;
                                    // $workflow->workflow_team_id = 1;

                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;
                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first()->delete();
                                } else {
                                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow->flow_location = 'S5';
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;
                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first()->delete();
                                }

                            } else {
                                $workflow->flow_desc = 'Semakan Selesai';
                                if('Dokumen Kontrak' == $request->type){

                                } else {
                                    $workflow->flow_location = '';
                                }
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 1;
                            }
                        }
                    }
                } elseif ('S6' == $uu->status && $request->type == $uu->type && (($request->document_contract_type == $uu->document_contract_type) || ($request->document_contract_type == $uu_null->document_contract_type)) && '3' == user()->department_id) {
                    // $review2->status         = 'S6';
                    // $review2->approved_by    = $request->input('approved_by');
                    // $review2->department     = config('enums.UU');
                    // $review2->acquisition_id = $request->input('acquisition_id');
                    // $review2->type           = $request->input('type');

                    // if ('Draft Penyedia' == $acq->status_task || 'Semakan Dokumen Perolehan' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('SST' == $acq->status_task || 'Cetak Notis' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('Dokumen Kontrak' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('IPC' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('VO' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('EOT' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // } elseif ('SOFA' == $acq->status_task) {
                    //     $review2->type = 'Acquisitions';
                    // }

                    // $review2->requested_by = $request->input('requested_by');
                    // $review2->requested_at = Carbon::now();
                    // $review2->approved_at  = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                    // $review2->remarks      = str_replace("'","`",$request->input('remarks')) ?? '';
                    // $review2->task_by      = $request->input('task_by');
                    $review->status      = 'S7';
                    $review->progress      = $uu->progress;
                    $review->law_task_by    = $uu->law_task_by;
                    $review->approved_by = $request->input('approved_by');
                    $review->department  = config('enums.UU');

                    $reviewlog->status      = 'S7';
                    $reviewlog->progress    = $uu->progress;
                    $reviewlog->approved_by = $request->input('approved_by');
                    $reviewlog->department  = config('enums.UU');

                    $reviewlog2->status      = 'S7';
                    $reviewlog2->progress    = $uu->progress;
                    $reviewlog2->approved_by = $request->input('approved_by');
                    $reviewlog2->department  = config('enums.UU');

                    if('Acquisitions' == $request->type){
                        $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                        $workflow2->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                        $workflow2->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                        $workflow2->flow_location = 'S7';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('SST' == $request->type){
                        $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                        $workflow2->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                        $workflow2->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                        $workflow2->flow_location = 'S7';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan SST';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan SST </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } elseif('Dokumen Kontrak' == $request->type){
                        $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                        $workflow2->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if('Bon Pelaksanaan' == $request->document_contract_type){
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                        } elseif('Insurans' == $request->document_contract_type){
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                        } elseif('Dokumen Kontrak' == $request->document_contract_type){
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                        } elseif('Wang Pendahuluan' == $request->document_contract_type){
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                        } elseif('CPC' == $request->document_contract_type) {
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';
                        } elseif('CNC' == $request->document_contract_type) {
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';
                        } elseif('CPO' == $request->document_contract_type) {
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';
                        } elseif('CMGD' == $request->document_contract_type) {
                            $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';
                        } elseif('Amaran' == $request->document_contract_type) {
                            $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();

                            $workflow->url = '/termination/warning/'.$warn->hashslug;
                        }

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    } else {
                        $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                        $workflow2->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                        $workflow2->flow_location = 'S7';  

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan dan Sahkan '.$request->type;
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan '.$request->type. ' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }  

                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

                    }

                    $workflow2->is_claimed = 1;
                    $workflow2->user_id = $request->input('approved_by');
                    $workflow2->role_id = 3;
                    $workflow2->workflow_team_id = 2;
                    // return 'aaa';

                } elseif ('S7' == $uu->status && $request->type == $uu->type && (($request->document_contract_type == $uu->document_contract_type) || ($request->document_contract_type == $uu_null->document_contract_type)) && '3' == user()->department_id) {
                    $review->status        = 'CN';
                    $review->progress      = $uu->progress;
                    $review->law_task_by    = $uu->law_task_by;
                    $review->approved_by = $uu->created_by;
                    // $review->approved_by   = $request->input('approved_by');
                    $review->quiry_remarks = $uu->remarks;
                    $review->department    = config('enums.UU');

                    $reviewlog->status        = 'CNUU';
                    $reviewlog->progress      = $uu->progress;
                    $reviewlog->approved_by = $uu->created_by;
                    // $reviewlog->approved_by   = $request->input('approved_by');
                    $reviewlog->quiry_remarks = $uu->remarks;
                    $reviewlog->department    = config('enums.UU');

                    if ('CN' == $bpub->status) {
                        // $acq = Acquisition::find($request->acquisition_id);

                        $review->approved_by = $review->created_by;
                        $review->status      = 'Selesai';

                        $reviewlog->approved_by = $review->created_by;
                        $reviewlog->status      = 'CNUU';

                        $reviewlog2->status        = 'CNUU';
                        $reviewlog2->progress      = $uu->progress;
                        $reviewlog2->approved_by = $uu->created_by;
                        $reviewlog2->requested_by = $uu->created_by;
                        $reviewlog2->department    = config('enums.UU');
                        $reviewlog2->reviews_status  = 'Selesai Semakan';

                        if('Acquisitions' == $request->type){
                           if($acq->briefing_status == 1){
                                $workflow->flow_desc = 'Taklimat : Sila teruskan dengan Taklimat.';
                                $workflow->url = '/briefing/'.$acq->hashslug.'/listing';   
                                $workflow->flow_location = 'Taklimat'; 
                                $workflow->is_claimed = 1;
                                $workflow->user_id = $review->created_by;
                                $workflow->role_id = 4;
                                $workflow->workflow_team_id = 1;

                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                $workflow2->user_id = null;

                                // if (! empty($review->approved_by)) {
                                //     $email->status  = 'NEW';
                                //     // real email
                                    // $email->to      = $review->approve->email;
                                //     // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                //     $email->counter = 1;
                                //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                //     $email->save();
                                // } else {
                                //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                // }

                                audit($review, __('Semakan Dokumen Perolehan ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada '.$review->create->name));

                                $acq->status_task = config('enums.flow4');
                                $acq->verify_status = null;
                                $acq->update();
                            } else {
                                if($acq->visit_status == 1){
                                    $workflow->flow_desc = 'Lawatan Tapak : Sila kemaskini kehadiran lawatan tapak.';
                                    $workflow->url = '/site-visit/'.$acq->hashslug.'/edit';   
                                    $workflow->flow_location = 'Lawatan Tapak'; 
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = null;
                                    $workflow->workflow_team_id = 1;

                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                    $workflow2->user_id = null;

                                    // $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->get();
                                    // foreach($claim_task_bpub as $claimed_bpub){
                                    //     $email_claimed_bpub = new EmailScheduler;
                                    //     $email_claimed_bpub->status  = 'NEW';
                                    //     // real email
                                    //     // $email_claimed_bpub->to      = $claimed_bpub->email;
                                    //     // dummy data
                                        // $email_claimed_bpub->to = "coins@ppj.gov.my";

                                    //     $email_claimed_bpub->counter = 1;
                                    //     $email_claimed_bpub->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                    //     $email_claimed_bpub->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $claimed_bpub->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                    //     $email_claimed_bpub->save();
                                    // } 

                                    audit($review, __('Semakan Dokumen Perolehan ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada Kaunter BPUB bagi modul Lawatan Tapak'));

                                    $acq->status_task = config('enums.flow5');
                                    $acq->verify_status = null;
                                    $acq->update();
                                } else {
                                    $workflow->flow_desc = 'Jualan : Sila pastikan jualan perolehan wujud dan isi nombor resit.';
                                    $workflow->url = '/sale';   
                                    $workflow->flow_location = 'Jualan'; 
                                    $workflow->user_id = null;
                                    $workflow->is_claimed = 1;
                                    // $workflow->user_id = $request->created_by;
                                    $workflow->workflow_team_id = 1;

                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                    $workflow2->user_id = null;

                                    // $claim_task_bpub = user()->role(['kaunter'])->get();
                                    // foreach($claim_task_bpub as $claimed_bpub){
                                    //     $email_claimed_bpub = new EmailScheduler;
                                    //     $email_claimed_bpub->status  = 'NEW';
                                    //     // real email
                                    //     // $email_claimed_bpub->to      = $claimed_bpub->email;
                                    //     // dummy data
                                        // $email_claimed_bpub->to = "coins@ppj.gov.my";

                                    //     $email_claimed_bpub->counter = 1;
                                    //     $email_claimed_bpub->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                    //     $email_claimed_bpub->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $claimed_bpub->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                    //     $email_claimed_bpub->save();
                                    // } 

                                    audit($review, __('Semakan Dokumen Perolehan ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada Kaunter BPUB bagi modul Jualan'));

                                    $acq->status_task = config('enums.flow6');
                                    $acq->verify_status = null;
                                    $acq->update();
                                }
                            }
                        } elseif('SST' == $request->type){

                            $workflow->flow_desc = 'Surat Setuju Terima : Sila muat naik kelulusan SST.';
                            $workflow->url = '/acceptance-letter/'.$company->hashslug.'#upload';
                            $workflow->flow_location = 'SST'; 
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $request->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                            $workflow2->user_id = null;   

                            $sst->status = 2;
                            $sst->update();

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Muat naik kelulusan SST';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Muat naik kelulusan SST </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 
                            $acq->verify_status = null;
                            $acq->update();

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                        } elseif('Dokumen Kontrak' == $request->type){
                            $workflow = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Bon Pelaksanaan')->where('workflow_team_id',1)->first();
                            $workflow3 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',1)->first();
                            if ('CPC' == $request->document_contract_type){
                                $workflow3->flow_desc = 'Sijil Siap Membaiki Kecacatan : Sila teruskan dengan CMGD.';
                                $workflow3->flow_location = 'CMGD';
                                $workflow3->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                $workflow3->is_claimed = 1;
                                $workflow3->user_id = $request->created_by;
                                $workflow3->role_id = 4;
                                $workflow3->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.';  
                                $workflow->flow_location = 'CMGD';
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2; 

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan CMGD';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CMGD </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                $acq->status_task = config('enums.flow20');
                                $acq->update();
                            } elseif ('CNC' == $request->document_contract_type){
                                $workflow3->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                                $workflow3->flow_location = 'CPC';
                                $workflow3->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                $workflow3->is_claimed = 1;
                                $workflow3->user_id = $request->created_by;
                                $workflow3->role_id = 4;
                                $workflow3->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.';  
                                $workflow->flow_location = 'CPC';
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan CPC';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                $acq->status_task = config('enums.flow17');
                                $acq->update();
                            } elseif ('CPO' == $request->document_contract_type){
                                $workflow3->flow_desc = 'Sijil Siap Kerja : Sila teruskan dengan CPC.';
                                $workflow3->flow_location = 'CPC';
                                $workflow3->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                $workflow3->is_claimed = 1;
                                $workflow3->user_id = $request->created_by;
                                $workflow3->role_id = 4;
                                $workflow3->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.';  
                                $workflow->flow_location = 'CPC';
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan CPC';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan CPC </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                $acq->status_task = config('enums.flow17');
                                $acq->update();
                            } elseif ('CMGD' == $request->document_contract_type){
                                $workflow3->flow_desc = 'Pelepasan Bon : Sila teruskan dengan Pelepasan Bon.';
                                $workflow3->flow_location = 'Pelepasan Bon';
                                $workflow3->url = '/post/release/'.$acq->sst->hashslug.'/edit';
                                $workflow3->is_claimed = 1;
                                $workflow3->user_id = $request->created_by;
                                $workflow3->role_id = 4;
                                $workflow3->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.'; 
                                $workflow->flow_location = 'Pelepasan Bon'; 
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan Pelepasan Bon';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pelepasan Bon </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                $acq->status_task = config('enums.flow21');
                                $acq->update();
                            } elseif('Amaran' == $request->document_contract_type) {
                                $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();
                                $workflow3->url = '/termination/warning/'.$warn->hashslug;
                                $workflow3->flow_location = 'Amaran';

                                $workflow3->flow_desc = '';
                                $workflow3->is_claimed = 1;
                                $workflow3->user_id = null;
                                $workflow3->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.'; 
                                $workflow->flow_location = 'Pelepasan Bon'; 
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai.'));

                                $acq->status_task = config('enums.flow21');
                                $acq->update();
                            }  elseif('Wang Pendahuluan' == $request->document_contract_type) { 
                                $workflow3->flow_location = 'Wang Pendahuluan';
                                $workflow3->flow_desc = '';
                                $workflow3->is_claimed = 1;
                                $workflow3->user_id = null;
                                $workflow3->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.'; 
                                $workflow->flow_location = 'Wang Pendahuluan'; 
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                // if (! empty($review->approved_by)) {
                                //     $email->status  = 'NEW';
                                //     // real email
                                    // $email->to      = $review->approve->email;
                                //     // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                //     $email->counter = 1;
                                //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
                                //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                //     $email->save();
                                // } else {
                                //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                // }
                                $acq->verify_status = null;
                                $acq->update();

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            } 
                            // elseif(!empty($ins) && !empty($doc) && 'Selesai' == $ins->status && 'Selesai' == $doc->status){
                            //     $workflow2->flow_desc = 'Pemantauan Projek : Sila muat naik jadual item dan carta kemajuan fizikal.';
                            //     $workflow2->flow_location = 'Pemantauan Projek';
                            //     $workflow2->url = '/acquisition/monitoring/'.$acq->sst->hashslug.'#physical';
                            //     $workflow2->is_claimed = 1;
                            //     $workflow2->user_id = $request->created_by;
                            //     $workflow2->role_id = 4;
                            //     $workflow2->workflow_team_id = 1;

                            //     $workflow->flow_desc = 'Semakan : Selesai.';  
                            //     $workflow->is_claimed = 1;
                            //     $workflow->user_id = null;
                            //     $workflow->workflow_team_id = 2;

                            //     if (! empty($review->approved_by)) {
                            //         $email->status  = 'NEW';
                            //         // real email
                            //         $email->to      = $review->approve->email;
                            //         // dummy data
                            //         // $email->to = "coins@ppj.gov.my";

                            //         $email->counter = 1;
                            //         $email->subject = 'Tindakan : Penyediaan Pemantauan Projek';
                            //         $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pemantauan Projek </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            //         $email->save();
                            //     } else {
                            //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            //     }

                            //     audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            //     $acq->status_task = config('enums.flow12');
                            //     $acq->verify_status = null;
                            //     $acq->update();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first()->delete();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first()->delete();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first()->delete();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first()->delete();
                            // } elseif(!empty($bon) && !empty($doc) && 'Selesai' == $bon->status && 'Selesai' == $doc->status){
                            //     $workflow2->flow_desc = 'Pemantauan Projek : Sila muat naik jadual item dan carta kemajuan fizikal.';
                            //     $workflow2->flow_location = 'Pemantauan Projek';
                            //     $workflow2->url = '/acquisition/monitoring/'.$acq->sst->hashslug.'#physical';
                            //     $workflow2->is_claimed = 1;
                            //     $workflow2->user_id = $request->created_by;
                            //     $workflow2->role_id = 4;
                            //     $workflow2->workflow_team_id = 1;

                            //     $workflow->flow_desc = 'Semakan : Selesai.';  
                            //     $workflow->is_claimed = 1;
                            //     $workflow->user_id = null;
                            //     $workflow->workflow_team_id = 2;

                            //     if (! empty($review->approved_by)) {
                            //         $email->status  = 'NEW';
                            //         // real email
                            //         $email->to      = $review->approve->email;
                            //         // dummy data
                            //         // $email->to = "coins@ppj.gov.my";

                            //         $email->counter = 1;
                            //         $email->subject = 'Tindakan : Penyediaan Pemantauan Projek';
                            //         $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            //         $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pemantauan Projek </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            //         $email->save();
                            //     } else {
                            //         swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            //     }

                            //     audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            //     $acq->status_task = config('enums.flow12');
                            //     $acq->verify_status = null;
                            //     $acq->update();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first()->delete();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first()->delete();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first()->delete();
                            //     WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first()->delete();
                            // }
                            elseif(!empty($bon) && !empty($ins) && 'Selesai' == $bon->status && 'Selesai' == $ins->status){
                                $workflow2->flow_desc = 'Pemantauan Projek : Sila muat naik jadual item dan carta kemajuan fizikal.';
                                $workflow2->flow_location = 'Pemantauan Projek';
                                $workflow2->url = '/acquisition/monitoring/'.$acq->sst->hashslug.'#physical';
                                $workflow2->is_claimed = 1;
                                $workflow2->user_id = $request->created_by;
                                $workflow2->role_id = 4;
                                $workflow2->workflow_team_id = 1;

                                $workflow->flow_desc = 'Semakan : Selesai.';  
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan Pemantauan Projek';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan Pemantauan Projek </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }

                                audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                                $acq->status_task = config('enums.flow12');
                                $acq->verify_status = null;
                                $acq->update();
                                WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first()->delete();
                                WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first()->delete();
                                WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first()->delete();
                                WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first()->delete();
                            } 
                                // elseif (empty($bon->status == 'Selesai') || empty($ins->status == 'Selesai') || empty($doc->status == 'Selesai') || empty($wang->status == 'Selesai')) {
                                //     $workflow->flow_desc = 'Semakan : Selesai.';  
                                //     $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit';
                                //     $workflow->is_claimed = 1;
                                //     $workflow->user_id = null;
                                //     $workflow->workflow_team_id = 2;

                                //     $acq->status_task = config('enums.flow10');
                                //     $acq->verify_status = null;
                                //     $acq->update();
                                // }
                             else {
                                $workflow->flow_desc = 'Semakan : Selesai.';  
                                $workflow->is_claimed = 1;
                                $workflow->user_id = null;
                                $workflow->workflow_team_id = 2;

                                $acq->status_task = config('enums.flow10');
                                $acq->verify_status = null;
                                $acq->update();
                            }
                        } elseif('EOT' == $request->type){
                            $workflow->flow_desc = 'Bayaran Muktamad : Sila teruskan dengan SOFA.';  
                            $workflow->flow_location = 'SOFA';  
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $request->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;

                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                            $workflow2->user_id = null;

                            if($acq->approval->acquisition_type_id == 2){   

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan SOFA';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }                         

                                audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));

                            } else {  

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Penyediaan SOFA';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Penyediaan SOFA </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                                                      
                                audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));
                                                        
                            }

                            $acq->status_task = config('enums.flow16');
                            $acq->update();
                        }

                        // $review2->status         = 'CN';
                        // $review2->progress       = 0;
                        // $review2->department     = config('enums.UU');
                        // $review2->acquisition_id = $request->input('acquisition_id');
                        // $review2->requested_by   = $request->input('requested_by');
                        // $review2->requested_at   = Carbon::now();
                        // $review2->approved_at    = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        // $review2->remarks        = $request->input('remarks');
                        // $review2->approved_by    = $review->created_by;
                        // // $review2->type = 'Acquisitions';
                        // $review2->task_by = $request->input('task_by');

                        $review->progress    = 1;
                        $reviewlog->progress = 1;
                        $reviewlog1->progress = 1;

                        // if ('Draft Penyedia' == $acq->status_task || 'Semakan Dokumen Perolehan' == $acq->status_task) {
                        //     $review->type     = 'SST';
                        //     $review2->type    = 'Acquisitions';
                        //     $acq->status_task = config('enums.flow9');
                        // } elseif ('SST' == $acq->status_task || 'Cetak Notis' == $acq->status_task) {
                        //     $review->type                   = 'Dokumen Kontrak';
                        //     $review->document_contract_type = 'Bon Pelaksanaan';
                        //     $review2->type                  = 'SST';

                        //     $review3->status                 = 'CN';
                        //     $review3->progress               = 1;
                        //     $review3->department             = config('enums.UU');
                        //     $review3->acquisition_id         = $request->input('acquisition_id');
                        //     $review3->requested_by           = $request->input('requested_by');
                        //     $review3->requested_at           = Carbon::now();
                        //     $review3->approved_at            = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        //     $review3->remarks                = $request->input('remarks');
                        //     $review3->approved_by            = $review->created_by;
                        //     $review3->created_by             = $review->created_by;
                        //     $review3->task_by                = $request->input('task_by');
                        //     $review3->type                   = 'Dokumen Kontrak';
                        //     $review3->document_contract_type = 'Insurans';

                        //     $review4->status                 = 'CN';
                        //     $review4->progress               = 1;
                        //     $review4->department             = config('enums.UU');
                        //     $review4->acquisition_id         = $request->input('acquisition_id');
                        //     $review4->requested_by           = $request->input('requested_by');
                        //     $review4->requested_at           = Carbon::now();
                        //     $review4->approved_at            = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        //     $review4->remarks                = $request->input('remarks');
                        //     $review4->approved_by            = $review->created_by;
                        //     $review4->created_by             = $review->created_by;
                        //     $review4->task_by                = $request->input('task_by');
                        //     $review4->type                   = 'Dokumen Kontrak';
                        //     $review4->document_contract_type = 'Wang Pendahuluan';

                        //     $review5->status                 = 'CN';
                        //     $review5->progress               = 1;
                        //     $review5->department             = config('enums.UU');
                        //     $review5->acquisition_id         = $request->input('acquisition_id');
                        //     $review5->requested_by           = $request->input('requested_by');
                        //     $review5->requested_at           = Carbon::now();
                        //     $review5->approved_at            = (! empty($request->approved_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->approved_at) : null;
                        //     $review5->remarks                = $request->input('remarks');
                        //     $review5->approved_by            = $review->created_by;
                        //     $review5->created_by             = $review->created_by;
                        //     $review5->task_by                = $request->input('task_by');
                        //     $review5->type                   = 'Dokumen Kontrak';
                        //     $review5->document_contract_type = 'Dokumen Kontrak';
                        //     $acq->status_task                = config('enums.flow10');
                        // } elseif ('Dokumen Kontrak' == $acq->status_task) {
                        //     if (('CN' == $bon->status && 'CN' == $ins->status && 'S7' == $doc->status) || ('S7' == $bon->status && 'CN' == $ins->status && 'CN' == $doc->status) || ('CN' == $bon->status && 'S7' == $ins->status && 'CN' == $doc->status)) {
                        //         $review->type     = 'IPC';
                        //         $review2->type    = 'Dokumen Kontrak';
                        //         $acq->status_task = config('enums.flow11');
                        //     } else {
                        //         return $doc;
                        //         $review2->status         = null;
                        //         $review2->progress       = null;
                        //         $review2->department     = null;
                        //         $review2->acquisition_id = null;
                        //         $review2->requested_by   = null;
                        //         $review2->requested_at   = null;
                        //         $review2->approved_at    = null;
                        //         $review2->remarks        = null;
                        //         $review2->approved_by    = null;
                        //         $review2->task_by        = null;
                        //         $review->type            = 'Dokumen Kontrak';
                        //         $review2->type           = '';
                        //     }
                        // } elseif ('IPC' == $acq->status_task) {
                        //     $review->type     = 'VO';
                        //     $review2->type    = 'IPC';
                        //     $acq->status_task = config('enums.flowALT1');
                        // } elseif ('VO' == $acq->status_task) {
                        //     $review->type     = 'EOT';
                        //     $review2->type    = 'VO';
                        //     $acq->status_task = config('enums.flow14');
                        // } elseif ('EOT' == $acq->status_task) {
                        //     $review->type     = 'SOFA';
                        //     $review2->type    = 'EOT';
                        //     $acq->status_task = config('enums.flowALT2');
                        // } elseif ('SOFA' == $acq->status_task) {
                        //     $review->type     = 'Selesai';
                        //     $review2->type    = 'SOFA';
                        //     $acq->status_task = 'Selesai';
                        // }

                        // $review2->created_by = $uu->created_by;

                        // $acq->update();
                    } else {
                        if('Acquisitions' == $request->type || 'SST' == $request->type || 'Dokumen Kontrak' == $request->type){
                            if(!empty($ppn) && !empty($ppn->request->department_id == '9') && $bpub->status == 'S5'){

                                $acq->status_task = config('enums.flow1');
                                $acq->verify_status = null;
                                $acq->update();

                                $reviewlog2->status        = 'CNUU';
                                $reviewlog2->progress      = $uu->progress;
                                $reviewlog2->approved_by = $uu->created_by;
                                $reviewlog2->requested_by = $uu->created_by;
                                $reviewlog2->department    = config('enums.UU');
                                $reviewlog2->reviews_status  = 'Terima';
                                if('Acquisitions' == $request->type){
                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                    $workflow2->flow_desc = 'Semakan Pra Kontrak : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow2->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
                                    $workflow2->flow_location = 'S7';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = null;
                                    $workflow2->workflow_team_id = 2;

                                    $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
                                    $workflow->flow_location = 'S5';
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                                } elseif('SST' == $request->type){
                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                                    $workflow2->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow2->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
                                    $workflow2->flow_location = 'S7';
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = null;
                                    $workflow2->workflow_team_id = 2;

                                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
                                    $workflow->flow_location = 'S5';
                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;

                                    $sst->status = null;
                                    $sst->update();

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan SST';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan SST </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                                } elseif('Dokumen Kontrak' == $request->type){
                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                                    $workflow2->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila tekan butang hantar untuk proses penyemakan.';
                                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila tekan butang hantar untuk proses penyemakan.';

                                    if('Bon Pelaksanaan' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#bon-implementation';
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#bon-implementation';
                                    } elseif('Insurans' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#insurance';
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#insurance';
                                    } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#document-contract';
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#document-contract';
                                    } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                        $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#cash-deposit';
                                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#cash-deposit';
                                    } elseif('CPC' == $request->document_contract_type) {
                                        $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                    } elseif('CNC' == $request->document_contract_type) {
                                        $workflow2->url = '/post/cnc/'.$acq->sst->hashslug.'/edit';
                                    } elseif('CPO' == $request->document_contract_type) {
                                        $workflow2->url = '/post/cpo/'.$acq->sst->hashslug.'/edit';
                                    } elseif('CMGD' == $request->document_contract_type) {
                                        $workflow2->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                    }
                                    $workflow2->is_claimed = 1;
                                    $workflow2->user_id = null;
                                    $workflow2->workflow_team_id = 2;

                                    $workflow->is_claimed = 1;
                                    $workflow->user_id = $review->created_by;
                                    $workflow->role_id = 4;
                                    $workflow->workflow_team_id = 1;

                                    if (! empty($review->approved_by)) {
                                        $email->status  = 'NEW';
                                        // real email
                                        $email->to      = $review->approve->email;
                                        // dummy data
                                        // $email->to = "coins@ppj.gov.my";

                                        $email->counter = 1;
                                        $email->subject = 'Tindakan : Semakan dan Sahkan Dokumen Kontrak';
                                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Dokumen Kontrak : '.$request->document_contract_type.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                        $email->save();
                                    } else {
                                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                    }

                                    // WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',1)->first()->delete();
                                }
                            } else {

                                $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                                $workflow2->flow_desc = 'Semakan Selesai';

                                if('Dokumen Kontrak' == $request->type){
                                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                                    $workflow2->flow_desc = 'Semakan Selesai';
                                } else {
                                    $workflow2->flow_location = '';
                                }
                                $workflow2->is_claimed = 1;
                                $workflow2->user_id = null;
                                $workflow2->workflow_team_id = 2;
                            }
                        }
                    }
                } elseif ('S8' == $bpub->status && $request->type == $bpub->type && '3' != user()->department_id) {
                    $review->status      = 'S9';
                    $review->progress    = $bpub->progress;
                    $review->law_task_by    = $uu->law_task_by;
                    $review->approved_by = $request->input('approved_by');
                    $review->department  = config('enums.BPUB');

                    $reviewlog->status      = 'S9';
                    $reviewlog->progress    = $bpub->progress;
                    $reviewlog->approved_by = $request->input('approved_by');
                    $reviewlog->department  = config('enums.BPUB');

                    $reviewlog1->status      = 'S9';
                    $reviewlog1->progress    = $bpub->progress;
                    $reviewlog1->approved_by = $request->input('approved_by');
                    $reviewlog1->department  = config('enums.BPUB');

                    $ipc = Ipc::find($request->input('ipc_id'));
                    $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
                    $workflow->flow_location = 'S9';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;  

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Kewangan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    }
               
                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));


                } elseif ('S9' == $bpub->status && $request->type == $bpub->type && '3' != user()->department_id) {
                    $review->status      = 'Selesai';
                    $review->progress    = $bpub->progress;
                    $review->law_task_by    = $uu->law_task_by;
                    $review->approved_by = $request->input('approved_by');
                    $review->department  = config('enums.BPUB');

                    $reviewlog->status      = 'CNK';
                    $reviewlog->progress    = $bpub->progress;
                    $reviewlog->approved_by = $request->input('approved_by');
                    $reviewlog->department  = config('enums.BPUB');

                    $reviewlog1->status      = 'CNK';
                    $reviewlog1->progress    = $bpub->progress;
                    $reviewlog1->approved_by = $request->input('approved_by');
                    $reviewlog1->department  = config('enums.BPUB');
                    $reviewlog1->reviews_status  = 'Selesai Semakan';

                    $workflow->flow_desc = 'Semakan Selesai.';
                    $workflow->flow_location = '';
                    $workflow->user_id = null;
                    $workflow->is_claimed = 1;
                    // $workflow->user_id = $request->input('approved_by');
                    $workflow->workflow_team_id = 1; 

                    // if (! empty($review->approved_by)) {
                    //     $email->status  = 'NEW';
                    //     // real email
                    //     $email->to      = $review->approve->email;
                    //     // dummy data
                    //     // $email->to = "coins@ppj.gov.my";

                    //     $email->counter = 1;
                    //     $email->subject = 'Tindakan : Semakan dan Sahkan IPC';
                    //     $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                    //     $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan IPC '.$ipc->ipc_no.' </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                    //     $email->save();
                    // } else {
                    //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    // }

                    $ipc = Ipc::find($request->input('ipc_id'));               
                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Selesai dan dihantar Kepada ' . $review->approve->name));


                    // if ('CN' == $bpub->status) {
                    //     $acq = Acquisition::find($request->acquisition_id);

                    //     $acq->status_task    = config('enums.flowALT1');
                    //     $review->approved_by = $review->created_by;

                    //     $acq->update();
                    // }
                }
            }

            if (! empty($review3) && '' != $review3->status) {
                $review3->save();
            }
            if (! empty($review2) && '' != $review2->status) {
                $review2->save();
            }
            // if (! empty($review4) && '' != $review4->status) {
            //     $review4->save();
            // }
            // if (! empty($review5) && '' != $review5->status) {
            //     $review5->save();
            // }

            $review->save();
            $reviewlog->save();
            if (! empty($reviewlog1) && '' != $reviewlog1->status) {
                $reviewlog1->save();
            }
            if (! empty($reviewlog2) && '' != $reviewlog2->status) {
                $reviewlog2->save();
            }

            $workflow->update();
            if (! empty($workflow2->flow_desc)) {
                $workflow2->save();
            }
            if (! empty($workflow3->flow_desc)) {
                $workflow3->save();
            }
            if (! empty($workflow4->flow_desc)) {
                $workflow4->save();
            }
            if (! empty($workflow5->flow_desc)) {
                $workflow5->save();
            }

            // if($review->status == 'Selesai'){
            //     if($request->type == 'Acquisitions' || $request->type == 'SST' || $request->type == 'Dokumen Kontrak'){
            //         WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first()->delete();
            //     }
            // }

            // if (! empty($review->approved_by)) {
            //     $email->status  = 'NEW';
            //     // real email
                // $email->to      = $review->approve->email;
            //     // dummy data
                // $email->to = "coins@ppj.gov.my";

            //     $email->counter = 1;
            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : TERATUR';
            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
            //     $email->save();
            // }

            if(!empty($review->approve)){
                if($request->type == 'EOT') {
                    if( (2 == $acq->approval->acquisition_type_id && (1 == $acq->acquisition_category_id || 2 == $acq->acquisition_category_id)) || ((1 == $acq->approval->acquisition_type_id || 3 == $acq->approval->acquisition_type_id || 4 == $acq->approval->acquisition_type_id) && (1 == $acq->acquisition_category_id || 2 == $acq->acquisition_category_id || 3 == $acq->acquisition_category_id || 4 == $acq->acquisition_category_id)) ){
                        if(empty($pp->status)){
                            swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);
                        }elseif('S2' == $pp->status){
                            swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Urusetia', []);
                        } else {
                            swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []); 
                        }
                    } else {
                        if(!empty($cnt_review->status) && 'S5' == $cnt_review->status){
                            if(2 == $acq->approval->acquisition_type_id && (3 == $acq->acquisition_category_id || 4 == $acq->acquisition_category_id)){
                                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);
                            } else { 
                                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Urusetia', []);
                            }
                        } else {
                            swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []); 
                        }
                    }
                } elseif(!empty($review2->approve) && ($request->type == 'SST' || ($request->type == 'Dokumen Kontrak' && ($request->document_contract_type == 'Bon Pelaksanaan' || $request->document_contract_type == 'Insurans' || $request->document_contract_type == 'Dokumen Kontrak' || $request->document_contract_type == 'Wang Pendahuluan')) || $request->type == 'Acquisitions')) {
                    if('S2' == $pp->status && 1 == $pp->progress){
                        swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . ' dan ' . $review2->approve->name . '.', []);
                    } else {
                        swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);
                    }
                } else {
                    swal()->success('Teratur', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);
                }
            } elseif($review->status == 'N' && $review->progress == 0) {
                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Urusetia', []);
            } elseif($request->type == 'IPC' && $review->status == 'S5') {
                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Pengesah', []);
            } elseif($request->type == 'VO' && $review->status == 'S5') {
                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Pengesah', []);
            } elseif($request->type == 'IPC' && $review->status == 'S8') {
                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Pegawai Kewangan', []);
            } else {
                swal()->success('Teratur', 'Rekod telah berjaya dihantar ke Urusetia', []);
            }
            
                // return redirect()->route('home');

            // function hantar tamat
        } elseif (1 == $request->saveStatus) {
            // function simpan mula
            $review->reviews_status = 'Deraf';

            $reviewlog->reviews_status = 'Deraf';

            if ('S1' == $pp->status && 'PELAKSANA' == $cnt_review->department && '3' != user()->department_id) {
                $review->status       = 'S1';
                $review->progress     = $pp->progress;
                $review->approved_by  = $pp->approved_by;
                $review->requested_by = $pp->requested_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'S1';
                $reviewlog->progress     = $pp->progress;
                $reviewlog->approved_by  = $pp->approved_by;
                $reviewlog->requested_by = user()->id;
                $reviewlog->department   = config('enums.PP');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                    if('Amaran' == $request->document_contract_type){
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$warn->warning_letter_no.' : Sila beri ulasan untuk proses penyemakan.'; 

                        audit($review, __('Semakan '.$request->document_contract_type. ''.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                    } else {
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.'; 

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                    }

                } elseif('IPC' == $request->type){
                    $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.'; 

                    $ipc = Ipc::find($request->input('ipc_id'));               
                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('VO' == $request->type){
                    if('PPK' == $request->document_contract_type){
                       $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses penyemakan.';   

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    } elseif('PPJHK' == $request->document_contract_type){
                       $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses penyemakan.'; 

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                    }
                    
                } elseif('Acquisitions' == $request->type){
                    $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S1';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('EOT' == $request->type) {

                    if($acq->approval->acquisition_type_id == 2){  

                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    } else {  

                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                                                
                    }

                    $workflow->flow_location = 'S1';
                } else {
                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S1';  

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }
                $workflow->is_claimed = 1;
                $workflow->user_id = $pp->approved_by;
                $workflow->workflow_team_id = 1;

            } elseif ('S2' == $pp->status && 'PELAKSANA' == $cnt_review->department && 0 == $pp->progress && 'PELAKSANA' == $cnt_review->department && '3' != user()->department_id) {
                $review->status       = 'S2';
                $review->progress     = $pp->progress;
                $review->approved_by  = $pp->approved_by;
                $review->requested_by = $pp->requested_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'S2';
                $reviewlog->progress     = $pp->progress;
                $reviewlog->approved_by  = $pp->approved_by;
                $reviewlog->requested_by = user()->id;
                $reviewlog->department   = config('enums.PP');

                $workflow->flow_location = 'S2';
                $workflow->is_claimed = 1;
                $workflow->user_id = $pp->approved_by;
                $workflow->workflow_team_id = 1;

                audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

            // $review->approved_by   = $request->input('task_by');
            } elseif ('S2' == $pp->status && 'PELAKSANA' == $cnt_review->department && 1 == $pp->progress && '3' != user()->department_id) {
                $review->status       = 'S2';
                $review->progress     = $pp->progress;
                $review->approved_by  = $pp->approved_by;
                $review->requested_by = $pp->requested_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'S2';
                $reviewlog->progress     = $pp->progress;
                $reviewlog->approved_by  = $pp->approved_by;
                $reviewlog->requested_by = user()->id;
                $reviewlog->department   = config('enums.PP');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                    if('Amaran' == $request->document_contract_type){
                        audit($review, __('Semakan '.$request->document_contract_type. ' '.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                    } else {
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                    }

                } elseif('VO' == $request->type) {
                    if('PPK' == $request->document_contract_type) {

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    } elseif('PPJHK' == $request->document_contract_type){

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    }
                } elseif('Acquisitions' == $request->type) {

                    $workflow->flow_location = 'S2';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('IPC' == $request->type) {     

                    $workflow->flow_location = 'S2';

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('EOT' == $request->type) {

                    $workflow->flow_location = 'S2';

                    if($acq->approval->acquisition_type_id == 2){                            

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan'));

                    } else {  
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                                                
                    }

                } else {

                    $workflow->flow_location = 'S2';

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $pp->approved_by;
                $workflow->workflow_team_id = 1;

            } elseif ('S3' == $bpub->status && ('BPUB' == $cnt_review->department || ('PELAKSANA' == $cnt_review->department && 'Deraf' == $cnt_review->reviews_status) ) && '3' != user()->department_id) {
                $review->status        = 'S3';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->approved_by;
                $review->requested_by  = $bpub->requested_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.BPUB');

                $reviewlog->status        = 'S3';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.BPUB');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
 
                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('VO' == $request->type) {
                    if('PPK' == $request->document_contract_type) {

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    } elseif('PPJHK' == $request->document_contract_type){

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    }
                } elseif('Acquisitions' == $request->type) {

                    $workflow->flow_location = 'S3';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('IPC' == $request->type) {     

                    $workflow->flow_location = 'S3';

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('EOT' == $request->type) {

                    $workflow->flow_location = 'S3';

                    if($acq->approval->acquisition_type_id == 2){                            

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan'));

                    } else {  
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                                                
                    }

                } else {
                    
                    $workflow->flow_location = 'S3';

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->approved_by;
                $workflow->workflow_team_id = 1;

            } elseif ('S4' == $bpub->status && ('BPUB' == $cnt_review->department || ('PELAKSANA' == $cnt_review->department && 'Deraf' == $cnt_review->reviews_status) ) && '3' != user()->department_id) {
                $review->status        = 'S4';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->approved_by;
                $review->requested_by  = $bpub->requested_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.BPUB');

                $reviewlog->status        = 'S4';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.BPUB');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
 
                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('VO' == $request->type) {
                    if('PPK' == $request->document_contract_type) {

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    } elseif('PPJHK' == $request->document_contract_type){

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    }
                } elseif('Acquisitions' == $request->type) {

                    $workflow->flow_location = 'S4';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('IPC' == $request->type) {     

                    $workflow->flow_location = 'S4';

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('EOT' == $request->type) {

                    $workflow->flow_location = 'S4';

                    if($acq->approval->acquisition_type_id == 2){                            

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan'));

                    } else {  
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                                                
                    }

                } else {
                    
                    $workflow->flow_location = 'S4';

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->approved_by;
                $workflow->workflow_team_id = 1;

            } elseif ('S5' == $bpub->status && ('BPUB' == $cnt_review->department || ('PELAKSANA' == $cnt_review->department && 'Deraf' == $cnt_review->reviews_status) ) && '3' != user()->department_id) {
                $review->status        = 'S5';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->approved_by;
                $review->requested_by  = $bpub->requested_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'S5';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.PP');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
 
                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('VO' == $request->type) {
                    if('PPK' == $request->document_contract_type) {

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    } elseif('PPJHK' == $request->document_contract_type){

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                    }
                } elseif('Acquisitions' == $request->type) {

                    $workflow->flow_location = 'S5';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('IPC' == $request->type) {     

                    $workflow->flow_location = 'S5';

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('EOT' == $request->type) {

                    $workflow->flow_location = 'S5';

                    if($acq->approval->acquisition_type_id == 2){                            

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan'));

                    } else {  
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
                                                
                    }

                } else {
                    
                    $workflow->flow_location = 'S5';

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->approved_by;
                $workflow->workflow_team_id = 1;

            } elseif ('S6' == $uu->status && '3' == user()->department_id) {
                $review->status        = 'S6';
                $review->progress      = $uu->progress;
                $review->approved_by   = $uu->approved_by;
                $review->requested_by  = $uu->requested_by;
                $review->quiry_remarks = $uu->remarks;
                $review->law_task_by = $uu->law_task_by;
                $review->department    = config('enums.UU');

                $reviewlog->status        = 'S6';
                $reviewlog->progress      = $uu->progress;
                $reviewlog->approved_by   = $uu->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $uu->remarks;
                $reviewlog->law_task_by = $uu->law_task_by;
                $reviewlog->department    = config('enums.UU');

                if('Dokumen Kontrak' == $request->type){
                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();

                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('Acquisitions' == $request->type) {

                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                    $workflow2->flow_location = 'S6';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } else {

                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                    $workflow2->flow_location = 'S6';

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }

                $workflow2->is_claimed = 1;
                $workflow2->user_id = $uu->approved_by;
                $workflow2->workflow_team_id = 2;

            } elseif ('S7' == $uu->status && '3' == user()->department_id) {
                $review->status        = 'S7';
                $review->progress      = $uu->progress;
                $review->approved_by   = $uu->approved_by;
                $review->requested_by  = $uu->requested_by;
                $review->quiry_remarks = $uu->remarks;
                $review->law_task_by = $uu->law_task_by;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'S7';
                $reviewlog->progress      = $uu->progress;
                $reviewlog->approved_by   = $uu->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $uu->remarks;
                $reviewlog->law_task_by = $uu->law_task_by;
                $reviewlog->department    = config('enums.PP');

                if('Dokumen Kontrak' == $request->type){
                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();

                    audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } elseif('Acquisitions' == $request->type) {

                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                    $workflow2->flow_location = 'S7';

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                } else {

                    $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                    $workflow2->flow_location = 'S7';

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));

                }

                $workflow2->is_claimed = 1;
                $workflow2->user_id = $uu->approved_by;
                $workflow2->workflow_team_id = 2;

            } elseif ('S8' == $bpub->status && ('BPUB' == $cnt_review->department || ('PELAKSANA' == $cnt_review->department && 'Deraf' == $cnt_review->reviews_status) ) && '3' != user()->department_id) {
                $review->status        = 'S8';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->approved_by;
                $review->requested_by  = $bpub->requested_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'S8';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.PP');

                $workflow->flow_location = 'S8';
                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->approved_by;
                $workflow->workflow_team_id = 1;    

                audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));


            } elseif ('S9' == $bpub->status && ('BPUB' == $cnt_review->department || ('PELAKSANA' == $cnt_review->department && 'Deraf' == $cnt_review->reviews_status) ) && '3' != user()->department_id) {
                $review->status        = 'S9';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->approved_by;
                $review->requested_by  = $bpub->requested_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'S9';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->approved_by;
                $reviewlog->requested_by  = user()->id;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.PP');

                $workflow->flow_location = 'S9';
                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->approved_by;
                $workflow->workflow_team_id = 1;

                audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Disimpan.'));
            }

            $review->save();
            $reviewlog->save();

            $workflow->update();
            if (! empty($workflow2)) {
                $workflow2->save();
            }

            // if (! empty($review->approved_by)) {
            //     $email->status  = 'NEW';
            //     // real email
                // $email->to      = $review->approve->email;
            //     // dummy data
                // $email->to = "coins@ppj.gov.my";

            //     $email->counter = 1;
            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : DRAF';
            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
            //     $email->save();
            // }

            if($request->ajax()){
                swal()->success('Deraf', 'Rekod telah berjaya disimpan.', []);
                return response()->json(['message'=>'Rekod telah berjaya disimpan.']);
            }
            else{
                // return redirect()->back();
                return redirect()->route('home');
            }

        // function simpan tamat
        } elseif (1 == $request->rejectStatus) {
            // function kuiri mula
            $review->reviews_status = 'Kuiri';

            $reviewlog->reviews_status = 'Kuiri';

            $reviewlog1->reviews_status = 'Terima';

            if(!empty($reviewlog2->acquisition_id)){

                $reviewlog2->reviews_status = 'Terima';

            }
            
            $acq->status_task = config('enums.flow1');
            $acq->update();
            if('Acquisitions' == $request->type){
                $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
                $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila tekan butang hantar untuk proses penyemakan.';
            } elseif('SST' == $request->type){
                $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
                $workflow->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';

                $sst->status = null;
                $sst->update();
            } elseif('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila tekan butang hantar untuk proses penyemakan.';
                if('Bon Pelaksanaan' == $request->document_contract_type){
                    $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#bon-implementation';
                } elseif('Insurans' == $request->document_contract_type){
                    $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#insurance';
                } elseif('Dokumen Kontrak' == $request->document_contract_type){
                    $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#document-contract';
                } elseif('Wang Pendahuluan' == $request->document_contract_type){
                    $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#cash-deposit';
                } elseif('CPC' == $request->document_contract_type) {
                    $workflow->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                } elseif('CNC' == $request->document_contract_type) {
                    $workflow->url = '/post/cnc/'.$acq->sst->hashslug.'/edit';
                } elseif('CPO' == $request->document_contract_type) {
                    $workflow->url = '/post/cpo/'.$acq->sst->hashslug.'/edit';
                } elseif('CMGD' == $request->document_contract_type) {
                    $workflow->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                } elseif('Amaran' == $request->document_contract_type) {
                    $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();
                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$warn->warning_letter_no.' : Sila tekan butang hantar untuk proses penyemakan.';
                    $workflow->url = '/termination/warning/'.$warn->hashslug.'/edit';
                } elseif('Tujuan' == $request->document_contract_type) {
                    $workflow->url = '/termination/termination/'.$acq->sst->hashslug.'/edit#reason';
                } elseif('Notis' == $request->document_contract_type) {
                    $workflow->url = '/termination/termination/'.$acq->sst->hashslug.'/edit#terminate';
                }
            } elseif('EOT' == $request->type){
                $eot = EOT::find($request->eot_id);
                $eot->status = null;
                $eot->update();
                $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit';
                if($acq->approval->acquisition_type_id == 2){                            

                    $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil. ' : Sila tekan butang hantar untuk proses penyemakan.';

                } else {                              

                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';
                                            
                }
            } elseif('IPC' == $request->type){

                $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                $ipc = Ipc::find($request->input('ipc_id'));
                $ipc->status = 1;
                $ipc->update();
                IpcInvoice::where('ipc_id',$ipc->id)->update([
                    'status'       => 1,
                ]);
                $workflow->url = '/post/ipc/'.$ipc->hashslug.'/ipc_invoice_edit#ipc-checklist';
                $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no. ' : Sila tekan butang hantar untuk proses penyemakan.';
            } elseif('VO' == $request->type){
                if('PPK' == $request->document_contract_type){
                    $vo_ppk = VO::where('id',$request->ppk_id)->where('no',$ppk->no)->first();
                    $vo_ppk->status = '';
                    $vo_ppk->update();
                    $workflow->url = '/post/variation-order/'.$ppk->hashslug.'/ppk_edit#ppk-doc';
                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' : Sila tekan butang hantar untuk proses penyemakan.';
                } elseif('PPJHK' == $request->document_contract_type){
                    $vo_ppjhk = Ppjhk::where('id',$request->ppjhk_id)->where('no',$ppjhk->no)->first();
                    $vo_ppjhk->status = '';
                    $vo_ppjhk->update();
                    $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit#detail';
                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' : Sila tekan butang hantar untuk proses penyemakan.';
                }
                
            } elseif('SOFA' == $request->type){
                $workflow->url = '/post/sofa/'.$acq->sst->hashslug.'/edit#statement-details';
                $workflow->flow_desc = 'Semakan '.$request->type.' : Sila tekan butang hantar untuk proses penyemakan.';
            }

            if ('S1' == $pp->status && '3' != user()->department_id) {
                $review->status       = null;
                $review->progress     = $pp->progress;
                $review->approved_by  = $pp->created_by;
                $review->requested_by = $pp->approved_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'S2';
                $reviewlog->progress     = $pp->progress;
                $reviewlog->approved_by  = $pp->created_by;
                $reviewlog->requested_by = $pp->approved_by;
                $reviewlog->department   = config('enums.PP');

                $reviewlog1->status       = 'S2';
                $reviewlog1->progress     = $pp->progress;
                $reviewlog1->approved_by  = $pp->created_by;
                $reviewlog1->requested_by = $pp->created_by;
                $reviewlog1->department   = config('enums.PP');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                    if('Amaran' == $request->document_contract_type){

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Penamatan (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Penamatan (Kuiri) : Amaran </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $warn->status = 1;
                        $warn->update();

                        audit($review, __('Semakan '.$request->document_contract_type. ' '.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } else {

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan '.$request->type.' (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan '.$request->type.' : '.$request->document_contract_type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        if('Penamatan' == $request->type){
                            $warn->status = 1;
                            $warn->update();   
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    }

                } elseif('VO' == $request->type) {
                    if('PPK' == $request->document_contract_type) {

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPK ' .$ppk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    } elseif('PPJHK' == $request->document_contract_type){

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPJHK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPJHK ' .$ppjhk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    }
                } elseif('Acquisitions' == $request->type) {

                    $workflow->flow_location = 'S1';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                } elseif('IPC' == $request->type) {   

                    $workflow->flow_location = 'S1';  

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                } elseif('EOT' == $request->type) {

                    $workflow->flow_location = 'S1';

                    if($acq->approval->acquisition_type_id == 2){

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT '.$eot->bil. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }                             

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    } else {  

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                                                
                    }

                } else {

                    $workflow->flow_location = 'S1';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $pp->created_by;
                $workflow->role_id = 4;
                $workflow->workflow_team_id = 1;

            } elseif ('S2' == $pp->status && 0 == $pp->progress && 'PELAKSANA' == $cnt_review->department && '3' != user()->department_id) {
                $review->status       = null;
                $review->progress     = $pp->progress;
                $review->approved_by  = $pp->created_by;
                $review->requested_by = $pp->approved_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'CNPP';
                $reviewlog->progress     = $pp->progress;
                $reviewlog->approved_by  = $pp->created_by;
                $reviewlog->requested_by = $pp->approved_by;
                $reviewlog->department   = config('enums.PP');

                $reviewlog1->status       = 'CNPP';
                $reviewlog1->progress     = $pp->progress;
                $reviewlog1->approved_by  = $pp->created_by;
                $reviewlog1->requested_by = $pp->created_by;
                $reviewlog1->department   = config('enums.PP');

                $workflow->flow_location = 'S2';
                $workflow->is_claimed = 1;
                $workflow->user_id = $pp->created_by;
                $workflow->role_id = 4;
                $workflow->workflow_team_id = 1;

                if (! empty($review->approved_by)) {
                    $email->status  = 'NEW';
                    // real email
                    $email->to      = $review->approve->email;
                    // dummy data
                    // $email->to = "coins@ppj.gov.my";

                    $email->counter = 1;
                    $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                    $email->save();
                } else {
                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                } 

                audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

            } elseif ('S2' == $pp->status && 'PELAKSANA' == $cnt_review->department && 1 == $pp->progress && '3' != user()->department_id) {
                $review->status       = null;
                $review->progress     = $pp->progress;
                $review->approved_by  = $pp->created_by;
                $review->requested_by = $pp->approved_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'CNPP';
                $reviewlog->progress     = $pp->progress;
                $reviewlog->approved_by  = $pp->created_by;
                $reviewlog->requested_by = $pp->approved_by;
                $reviewlog->department   = config('enums.PP');

                $reviewlog1->status       = 'CNPP';
                $reviewlog1->progress     = $pp->progress;
                $reviewlog1->approved_by  = $pp->created_by;
                $reviewlog1->requested_by = $pp->created_by;
                $reviewlog1->department   = config('enums.PP');

                if('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type){
                    if('Amaran' == $request->document_contract_type){

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Penamatan (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Penamatan : Amaran (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        if('Penamatan' == $request->type){
                            $warn->status = 1;
                            $warn->update();   
                        }
                        
                        audit($review, __('Semakan '.$request->document_contract_type. ' '.$warn->warning_letter_no.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } else {

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan '.$request->type.' (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan '.$request->type.' : '.$request->document_contract_type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        if('Penamatan' == $request->type){
                            $warn->status = 1;
                            $warn->update();   
                        }

                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    }

                } elseif('VO' == $request->type) {
                    if('PPK' == $request->document_contract_type) {

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPK '.$ppk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    } elseif('PPJHK' == $request->document_contract_type){

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPJHK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPJHK '.$ppjhk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    }
                } elseif('Acquisitions' == $request->type) {

                    $workflow->flow_location = 'S2';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                } elseif('IPC' == $request->type) {   

                    $workflow->flow_location = 'S2';  

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                } elseif('EOT' == $request->type) {

                    $workflow->flow_location = 'S2';

                    if($acq->approval->acquisition_type_id == 2){  

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT '.$eot->bil. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }                           

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    } else {  

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                                                
                    }

                } else {

                    $workflow->flow_location = 'S2';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $pp->created_by;
                $workflow->role_id = 4;
                $workflow->workflow_team_id = 1;
            } elseif ('S3' == $bpub->status && '3' != user()->department_id) {
                $review->status        = 'S4';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $request->input('approved_by');
                $review->department    = config('enums.BPUB');
                $review->quiry_remarks = $bpub->remarks;

                $reviewlog->status        = 'S4';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $request->input('approved_by');
                $reviewlog->department    = config('enums.BPUB');
                $reviewlog->quiry_remarks = $bpub->remarks;

                $reviewlog1->status        = 'S4';
                $reviewlog1->progress      = $bpub->progress;
                $reviewlog1->approved_by   = $request->input('approved_by');
                $reviewlog1->department    = config('enums.BPUB');

                if('Acquisitions' == $request->type){
                    $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                    $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S3';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    $acq->status_task = config('enums.flow2');
                    $acq->update();

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('SST' == $request->type){
                    $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                    $workflow->flow_desc = 'Semakan SST : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S3';

                    $sst->status = 1;
                    $sst->update();

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan SST (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan SST (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    $acq->status_task = config('enums.flow22');
                    $acq->update();

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('Dokumen Kontrak' == $request->type || 'Penamatan' == $request->type || 'VO' == $request->type){
                    if('Bon Pelaksanaan' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Bon Pelaksanaan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Insurans' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Insurans (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Dokumen Kontrak' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Dokumen Kontrak (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Wang Pendahuluan' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Wang Pendahuluan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CPC' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CPC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow29');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CNC' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CNC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CNC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow30');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CPO' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CPO (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPO (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow31');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CMGD' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CMGD (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CMGD (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow32');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Amaran' == $request->document_contract_type) {
                        $warn = Warning::where('sst_id',$acq->sst->id)->where('warning_letter_no',$request->warning_letter_no)->first();
                        $workflow->url = '/termination/warning/'.$warn->hashslug.'/edit';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Penamatan (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Penamatan : Amaran (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow27');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('PPK' == $request->document_contract_type) {
                        $workflow->url = '/post/variation-order/'.$ppk->hashslug.'/ppk_show#document-review';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        $vo_ppk = VO::where('id',$request->ppk_id)->where('no',$ppk->no)->first();
                        $vo_ppk->status = 9;
                        $vo_ppk->update();

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPK ' .$ppk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow26');
                        $acq->update();

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('PPJHK' == $request->document_contract_type) {
                        $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'#document-review';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                        $vo_ppjhk = Ppjhk::where('id',$request->ppjhk_id)->where('no',$ppjhk->no)->first();
                        $vo_ppjhk->status = 9;
                        $vo_ppjhk->update();

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPJHK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPJHK ' .$ppjhk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow26');
                        $acq->update();

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    }
                } elseif('EOT' == $request->type){
                    $workflow->url = '/extension/extension_show/'.$eot->hashslug.'#eot-review';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->workflow_team_id = 1;
                    $eot = EOT::find($request->eot_id);
                    $eot->status = 1;
                    $eot->update();

                    if($acq->approval->acquisition_type_id == 2){
                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses pengesahan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT '.$eot->bil. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow25');
                        $acq->update();

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } else {
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow25');
                        $acq->update();
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    }
                } elseif('IPC' == $request->type){
                    $ipc = IPC::find($request->ipc_id);
                    $workflow->url = '/post/ipc/'.$ipc->hashslug.'/ipc_show#ipc-review';
                    $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S3';

                    $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                    // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                    $ipc = Ipc::find($request->input('ipc_id'));
                    $ipc->status = 2;
                    $ipc->update();
                    IpcInvoice::where('ipc_id',$ipc->id)->update([
                        'status'       => 2,
                    ]);

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    $acq->status_task = config('enums.flow24');
                    $acq->update();

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('SOFA' == $request->type){
                    $workflow->url = '/post/sofa/'.$acq->sst->hashslug.'/edit#sst';
                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S3';
                } else {
                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                    $workflow->flow_location = 'S3';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $request->input('approved_by');
                $workflow->role_id = 5;
                $workflow->workflow_team_id = 1;

            } elseif ('S4' == $bpub->status && '3' != user()->department_id) {
                $review->status        = 'S5';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $request->input('approved_by');
                $review->department    = config('enums.BPUB');
                $review->quiry_remarks = $bpub->remarks;

                $reviewlog->status        = 'S5';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $request->input('approved_by');
                $reviewlog->department    = config('enums.BPUB');
                $reviewlog->quiry_remarks = $bpub->remarks;

                $reviewlog1->status        = 'S5';
                $reviewlog1->progress      = $bpub->progress;
                $reviewlog1->approved_by   = $request->input('approved_by');
                $reviewlog1->department    = config('enums.BPUB');

                $acq->status_task = config('enums.flow2');
                $acq->update();

                if('Acquisitions' == $request->type){
                    $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;
                    $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses pengesahan.';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    $acq->status_task = config('enums.flow2');
                    $acq->update();

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('SST' == $request->type){
                    $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;
                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';

                    $sst->status = 1;
                    $sst->update();

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan SST (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan SST (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Bahagian : '.$review->request->department->name.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    $acq->status_task = config('enums.flow22');
                    $acq->update();

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('Dokumen Kontrak' == $request->type || 'VO' == $request->type){
                    $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses pengesahan.';
                    if('Bon Pelaksanaan' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Bon Pelaksanaan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Insurans' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Insurans (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Dokumen Kontrak' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Dokumen Kontrak (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Wang Pendahuluan' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Wang Pendahuluan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow23');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CPC' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CPC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow29');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CNC' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CNC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CNC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow30');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CPO' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CPO (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPO (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow31');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CMGD' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CMGD (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CMGD (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        $acq->status_task = config('enums.flow32');
                        $acq->update();
 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('PPK' == $request->document_contract_type) {
                        $workflow->url = '/post/variation-order/'.$ppk->hashslug.'/ppk_show#document-review';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppk->no.' : Sila beri ulasan untuk proses pengesahan.';

                        $vo_ppk = VO::where('id',$request->ppk_id)->where('no',$ppk->no)->first();
                        $vo_ppk->status = 9;
                        $vo_ppk->update();

                        $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->role(['pengesah'])->get();
                        foreach($claim_task_bpub as $claimed_bpub){
                            $email_claimed_bpub = new EmailScheduler;
                            $email_claimed_bpub->status  = 'NEW';
                            // real email
                            $email_claimed_bpub->to      = $claimed_bpub->email;
                            // dummy data
                            // $email_claimed_bpub->to = "coins@ppj.gov.my";

                            $email_claimed_bpub->counter = 1;
                            $email_claimed_bpub->subject = 'Tindakan : Semakan PPK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPK ' .$ppk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email_claimed_bpub->save();
                        }

                        $acq->status_task = config('enums.flow26');
                        $acq->update();

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada Pengesah.'));
                    } elseif('PPJHK' == $request->document_contract_type) {
                        $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'#document-review';
                        $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' '.$ppjhk->no.' : Sila beri ulasan untuk proses pengesahan.';

                        $vo_ppjhk = Ppjhk::where('id',$request->ppjhk_id)->where('no',$ppjhk->no)->first();
                        $vo_ppjhk->status = 9;
                        $vo_ppjhk->update();

                        $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->role(['pengesah'])->get();
                        foreach($claim_task_bpub as $claimed_bpub){
                            $email_claimed_bpub = new EmailScheduler;
                            $email_claimed_bpub->status  = 'NEW';
                            // real email
                            $email_claimed_bpub->to      = $claimed_bpub->email;
                            // dummy data
                            // $email_claimed_bpub->to = "coins@ppj.gov.my";

                            $email_claimed_bpub->counter = 1;
                            $email_claimed_bpub->subject = 'Tindakan : Semakan PPJHK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPJHK ' .$ppjhk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email_claimed_bpub->save();
                        }

                        $acq->status_task = config('enums.flow26');
                        $acq->update();

                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada Pengesah.'));
                    }
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;
                } elseif('EOT' == $request->type){
                    $workflow->url = '/extension/extension_show/'.$eot->hashslug.'#eot-review';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;
                    $eot = EOT::find($request->eot_id);
                    $eot->status = 1;
                    $eot->update();

                    $acq->status_task = config('enums.flow25');
                    $acq->update();

                    if($acq->approval->acquisition_type_id == 2){
                        $workflow->flow_desc = 'Semakan '.$request->type.' '.$eot->bil.' : Sila beri ulasan untuk proses pengesahan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT '.$eot->bil. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } else {
                        $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                                              
                        audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    }
                } elseif('IPC' == $request->type){
                    $review->approved_by   = null;
                    $reviewlog->approved_by   = null;
                    $reviewlog1->approved_by   = null;
                    $reviewlog1->requested_by   = null;

                    $ipc = IPC::find($request->ipc_id);
                    $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
                    $workflow->url = '/post/ipc/'.$ipc->hashslug.'/ipc_show#ipc-review';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 0;
                    $workflow->user_id = null;
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;

                    $rev_ipc = Review::where('ipc_id',$request->input('ipc_id'))->where('type','IPC')->first();
                    // $rev_ipc = Review::where('acquisition_id',$acq->id)->where('type','IPC')->first();
                    $ipc = Ipc::find($request->input('ipc_id'));
                    $ipc->status = 2;
                    $ipc->update();
                    IpcInvoice::where('ipc_id',$ipc->id)->update([
                        'status'       => 2,
                    ]);

                    $claim_task_bpub = user()->where('department_id',9)->where('executor_department_id',25)->role(['pengesah'])->get();
                    foreach($claim_task_bpub as $claimed_bpub){
                        $email_claimed_bpub = new EmailScheduler;
                        $email_claimed_bpub->status  = 'NEW';
                        // real email
                        $email_claimed_bpub->to      = $claimed_bpub->email;
                        // dummy data
                        // $email_claimed_bpub->to = "coins@ppj.gov.my";

                        $email_claimed_bpub->counter = 1;
                        $email_claimed_bpub->subject = 'Tindakan : Semakan IPC (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email_claimed_bpub->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email_claimed_bpub->save();
                    }

                    $acq->status_task = config('enums.flow24');
                    $acq->update();

                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada Pengesah.'));
                } elseif('SOFA' == $request->type){
                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';
                    $workflow->url = '/post/sofa/'.$acq->sst->hashslug.'/edit#sst';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;
                } else {
                    $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses pengesahan.';
                    $workflow->flow_location = 'S4';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $request->input('approved_by');
                    $workflow->role_id = 3;
                    $workflow->workflow_team_id = 1;

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                }

            } elseif ('S5' == $bpub->status && '3' != user()->department_id) {
                $review->status        = null;
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->created_by;
                $review->requested_by  = $bpub->approved_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'CNBPUB';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->created_by;
                $reviewlog->requested_by  = $bpub->approved_by;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.PP');

                if('Acquisitions' == $request->type || 'SST' == $request->type || ('Dokumen Kontrak' == $request->type && ('Dokumen Kontrak' == $request->document_contract_type)) || 'VO' == $request->type){
                    if( ((!empty($ppn) || empty($ppn)) && ($uu->status == 'CN' || $pp->status == null)) ){

                        $reviewlog1->status        = 'CNBPUB';
                        $reviewlog1->progress      = $bpub->progress;
                        $reviewlog1->approved_by   = $bpub->created_by;
                        $reviewlog1->requested_by  = $bpub->created_by;
                        $reviewlog1->department    = config('enums.PP');

                        if('Dokumen Kontrak' == $request->type){

                            $workflow->flow_location = $request->document_contract_type;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : '.$request->document_contract_type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } elseif('VO' == $request->type) {

                            $workflow->flow_location = $request->document_contract_type;
                            
                            if('PPK' == $request->document_contract_type) {

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan PPK (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPK ' .$ppk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 

                                audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            } elseif('PPJHK' == $request->document_contract_type){

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan PPJHK (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPJHK ' .$ppjhk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 

                                audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            }
                        } elseif('Acquisitions' == $request->type) {

                            $workflow->flow_location = 'S5';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } elseif('SST' == $request->type) {

                            $workflow->flow_location = 'S5';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } else {

                            $workflow->flow_location = 'S5';

                            $sst->status = 1;
                            $sst->update();

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        }

                        $workflow->is_claimed = 1;
                        $workflow->user_id = $bpub->created_by;
                        $workflow->role_id = 4;
                        $workflow->workflow_team_id = 1;

                        // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first()->delete();
                    } else {

                        if('Dokumen Kontrak' == $request->type){

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow23');
                            $acq->verify_status = '1';
                            $acq->update();
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } elseif('VO' == $request->type) {
                            if('PPK' == $request->document_contract_type) {

                                // if (! empty($review->approved_by)) {
                                //     $email->status  = 'NEW';
                                //     // real email
                                    // $email->to      = $review->approve->email;
                                //     // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                //     $email->counter = 1;
                                //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                                //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                //     $email->save();
                                // } else {
                                //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                // } 

                                audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            } elseif('PPJHK' == $request->document_contract_type){

                                // if (! empty($review->approved_by)) {
                                //     $email->status  = 'NEW';
                                //     // real email
                                    // $email->to      = $review->approve->email;
                                //     // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                //     $email->counter = 1;
                                //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                                //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                                //     $email->save();
                                // } else {
                                //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                // } 

                                audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            }
                        } elseif('Acquisitions' == $request->type) {

                            $workflow->flow_location = 'S5';

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow2');
                            $acq->verify_status = '1';
                            $acq->update();

                            audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } elseif('SST' == $request->type) {

                            $workflow->flow_location = 'S5';

                            $sst->status = 1;
                            $sst->update();

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow22');
                            $acq->verify_status = '1';
                            $acq->update();

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } else {

                            $workflow->flow_location = 'S5';

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow22');
                            $acq->verify_status = '1';
                            $acq->update();

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        }

                        $workflow->is_claimed = 1;
                        $workflow->user_id = null;
                        $workflow->workflow_team_id = 1;
                        // $workflow->is_claimed = 1;
                        // $workflow->user_id = $bpub->created_by;
                        // $workflow->role_id = 4;
                        // $workflow->workflow_team_id = 1;
                    }
                } elseif(('Dokumen Kontrak' == $request->type && ('Bon Pelaksanaan' == $request->document_contract_type || 'Insurans' == $request->document_contract_type || 'Wang Pendahuluan' == $request->document_contract_type || 'CPC' == $request->document_contract_type || 'CPO' == $request->document_contract_type || 'CNC' == $request->document_contract_type || 'CMGD' == $request->document_contract_type)) || 'IPC' == $request->type || 'EOT' == $request->type || 'SOFA' == $request->type) {
                    $workflow->flow_location = 'S5';
                    $workflow->is_claimed = 1;
                    $workflow->user_id = $bpub->created_by;
                    $workflow->role_id = 4;
                    $workflow->workflow_team_id = 1;

                    $reviewlog1->status        = 'CNBPUB';
                    $reviewlog1->progress      = $bpub->progress;
                    $reviewlog1->approved_by   = $bpub->created_by;
                    $reviewlog1->requested_by  = $bpub->created_by;
                    $reviewlog1->department    = config('enums.PP');

                    if('Dokumen Kontrak' == $request->type){

                        $workflow->flow_location = $request->document_contract_type;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : '.$request->document_contract_type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } elseif('IPC' == $request->type) {   

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        }  

                        audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    } elseif('EOT' == $request->type) {

                        if($acq->approval->acquisition_type_id == 2){  

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT '.$eot->bil. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }                           

                            audit($review, __('Semakan '.$request->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                        } else { 

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan EOT (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan EOT (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }  
                                                  
                            audit($review, __('Semakan '.$request->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                                                    
                        }

                    } else {

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 

                        audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                    }
                }
                
            } elseif ('S6' == $uu->status && 1 == $uu->progress && '3' == user()->department_id) {
                $review->status       = 'S7';
                $review->progress     = $uu->progress;
                $review->approved_by  = $request->approved_by;
                $review->requested_by = $uu->approved_by;
                $review->law_task_by = $uu->law_task_by;
                $review->department   = config('enums.UU');

                $reviewlog->status       = 'S7';
                $reviewlog->progress     = $uu->progress;
                $reviewlog->approved_by  = $request->approved_by;
                $reviewlog->requested_by = $uu->approved_by;
                $reviewlog->law_task_by = $uu->law_task_by;
                $reviewlog->department   = config('enums.UU');

                $reviewlog2->status       = 'S7';
                $reviewlog2->progress     = $uu->progress;
                $reviewlog2->approved_by  = $request->approved_by;
                $reviewlog2->requested_by = $request->approved_by;
                $reviewlog2->law_task_by = $uu->law_task_by;
                $reviewlog2->department   = config('enums.UU');

                $acq->status_task = config('enums.flow2');
                $acq->update();

                if('Acquisitions' == $request->type || 'SST' == $request->type || 'Dokumen Kontrak' == $request->type){
                    if(((!empty($ppn) && $ppn == $cnt_review && !empty($ppn->request->department_id == '9') && $bpub->status == 'S5') || $bpub->status == 'CN')){

                        if('Acquisitions' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                            $workflow2->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                            $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                            $workflow2->flow_location = 'S6';
                            $workflow2->is_claimed = 1;
                            $workflow2->user_id = $request->approved_by;
                            $workflow2->role_id = 3;
                            $workflow2->workflow_team_id = 2;
                            $workflow2->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                        } elseif('SST' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                            $workflow2->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                            $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                            $workflow2->flow_location = 'S6';
                            $workflow2->is_claimed = 1;
                            $workflow2->user_id = $request->approved_by;
                            $workflow2->role_id = 3;
                            $workflow2->workflow_team_id = 2;
                            $workflow2->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan.';

                            $sst->status = 1;
                            $sst->update();

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan SST (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan SST (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                        } elseif('Dokumen Kontrak' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();

                            if('Bon Pelaksanaan' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Bon Pelaksanaan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('Insurans' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Insurans (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Dokumen Kontrak (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Wang Pendahuluan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CPC' == $request->document_contract_type) {
                                $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CPC (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CNC' == $request->document_contract_type) {
                                $workflow2->url = '/post/cnc/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CNC (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CNC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CPO' == $request->document_contract_type) {
                                $workflow2->url = '/post/cpo/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CPO (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPO (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CMGD' == $request->document_contract_type) {
                                $workflow2->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';
                                $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                                $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CMGD (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CMGD (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            }
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            $workflow2->is_claimed = 1;
                            $workflow2->user_id = $request->approved_by;
                            $workflow2->role_id = 3;
                            $workflow2->workflow_team_id = 2;

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',1)->first()->delete();
                        } else {
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();
                            $workflow2->flow_location = 'S6';
                            $workflow2->is_claimed = 1;
                            $workflow2->user_id = $request->approved_by;
                            $workflow2->role_id = 3;
                            $workflow2->workflow_team_id = 2;
                            $workflow2->flow_desc = 'Semakan '. $request->type.' : Sila beri ulasan untuk proses penyemakan.';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                        }
                    } else {
                        if('Acquisitions' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow2->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                            $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                            $workflow2->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
                            $workflow2->flow_location = 'S6';

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            }

                            $acq->status_task = config('enums.flow2');
                            $acq->verify_status = '1';
                            $acq->update();

                            audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } elseif('SST' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow2->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                            $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                            $workflow2->flow_desc = 'Semakan '. $request->type.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->flow_desc = 'Semakan '. $request->type.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow2->flow_location = 'S6';  

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan SST (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan SST (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            $acq->status_task = config('enums.flow22');
                            $acq->verify_status = '1';
                            $acq->update();

                            $sst->status = 1;
                            $sst->update();

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.')); 
                        } elseif('Dokumen Kontrak' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                            $workflow2->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';
                            $workflow->flow_desc = 'Semakan '. $request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan.';

                            if('Bon Pelaksanaan' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Bon Pelaksanaan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('Insurans' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Insurans (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Dokumen Kontrak (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Wang Pendahuluan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CPC' == $request->document_contract_type) {
                                $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CPC (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CNC' == $request->document_contract_type) {
                                $workflow2->url = '/post/cnc/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CNC (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CNC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CPO' == $request->document_contract_type) {
                                $workflow2->url = '/post/cpo/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CPO (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPO (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            } elseif('CMGD' == $request->document_contract_type) {
                                $workflow2->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CMGD (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CMGD (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                }
                            }

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow23');
                            $acq->verify_status = '1';
                            $acq->update();
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } else {
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow2->flow_location = 'S6';


                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        }
                        $workflow2->is_claimed = 1;
                        $workflow2->user_id = $request->approved_by;
                        $workflow2->role_id = 3;
                        $workflow2->workflow_team_id = 2;
                    }
                }

            } elseif ('S7' == $uu->status && '3' == user()->department_id) {
                $review->status       = null;
                $review->progress     = $uu->progress;
                $review->approved_by  = $uu->created_by;
                $review->requested_by = $uu->approved_by;
                $review->law_task_by = $uu->law_task_by;
                $review->department   = config('enums.PP');

                $reviewlog->status       = 'CNUU';
                $reviewlog->progress     = $uu->progress;
                $reviewlog->approved_by  = $uu->created_by;
                $reviewlog->requested_by = $uu->approved_by;
                $reviewlog->law_task_by = $uu->law_task_by;
                $reviewlog->department   = config('enums.PP');

                if('Acquisitions' == $request->type || 'SST' == $request->type || 'Dokumen Kontrak' == $request->type){
                    if( (!empty($ppn) || empty($ppn)) && ($bpub->status == 'CN' || $pp->status == null) ){

                        $reviewlog2->status       = 'CNUU';
                        $reviewlog2->progress     = $uu->progress;
                        $reviewlog2->approved_by  = $uu->created_by;
                        $reviewlog2->requested_by = $uu->created_by;
                        $reviewlog2->law_task_by = $uu->law_task_by;
                        $reviewlog2->department   = config('enums.PP');

                        if('Acquisitions' == $request->type){

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
                            // $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                            // $workflow2->flow_location = 'S7';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $uu->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;
                            $workflow2->user_id = null;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } elseif('SST' == $request->type){

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
                            // $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                            // $workflow->flow_location = 'S7';
                            $workflow->is_claimed = 1;
                            $workflow->user_id = $uu->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;
                            $workflow2->user_id = null;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan SST (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan SST (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } elseif('Dokumen Kontrak' == $request->type){

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',1)->first()->delete();
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();

                            if('Bon Pelaksanaan' == $request->document_contract_type){
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#bon-implementation';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Bon Pelaksanaan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('Insurans' == $request->document_contract_type){
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#insurance';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Insurans (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#document-contract';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Dokumen Kontrak (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'/edit#cash-deposit';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Wang Pendahuluan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('CPC' == $request->document_contract_type) {
                                $workflow->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CPC (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('CNC' == $request->document_contract_type) {
                                $workflow->url = '/post/cnc/'.$acq->sst->hashslug.'/edit';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CNC (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CNC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('CPO' == $request->document_contract_type) {
                                $workflow->url = '/post/cpo/'.$acq->sst->hashslug.'/edit';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CPO (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPO (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            } elseif('CMGD' == $request->document_contract_type) {
                                $workflow->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                // $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';

                                if (! empty($review->approved_by)) {
                                    $email->status  = 'NEW';
                                    // real email
                                    $email->to      = $review->approve->email;
                                    // dummy data
                                    // $email->to = "coins@ppj.gov.my";

                                    $email->counter = 1;
                                    $email->subject = 'Tindakan : Semakan CMGD (Kuiri)';
                                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CMGD (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                    $email->save();
                                } else {
                                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                                } 
                            }
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                            $workflow->is_claimed = 1;
                            $workflow->user_id = $uu->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;
                            $workflow2->user_id = null;
                        } else {

                            // WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',1)->first()->delete();
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            // $workflow2->flow_location = 'S7';

                            $workflow->is_claimed = 1;
                            $workflow->user_id = $uu->created_by;
                            $workflow->role_id = 4;
                            $workflow->workflow_team_id = 1;
                            $workflow2->user_id = null;

                            if (! empty($review->approved_by)) {
                                $email->status  = 'NEW';
                                // real email
                                $email->to      = $review->approve->email;
                                // dummy data
                                // $email->to = "coins@ppj.gov.my";

                                $email->counter = 1;
                                $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                                $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                                $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                                $email->save();
                            } else {
                                swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        }
                    } else {
                        if('Acquisitions' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow2->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
                            $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                            $workflow2->flow_location = 'S7';
                            $workflow->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan';

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow2');
                            $acq->verify_status = '1';
                            $acq->update();

                            audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } elseif('SST' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow2->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
                            $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                            $workflow2->flow_location = 'S7';
                            $workflow->flow_desc = 'Semakan SST : Sila beri ulasan untuk proses penyemakan';


                            $sst->status = 1;
                            $sst->update();

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow22');
                            $acq->verify_status = '1';
                            $acq->update();

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } elseif('Dokumen Kontrak' == $request->type){
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('flow_location',$request->document_contract_type)->where('workflow_team_id',2)->first();
                            $workflow->flow_desc = 'Semakan '.$request->document_contract_type.' : Sila beri ulasan untuk proses penyemakan';

                            if('Bon Pelaksanaan' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#bon-implementation';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';
                            } elseif('Insurans' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#insurance';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';
                            } elseif('Dokumen Kontrak' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#document-contract';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';
                            } elseif('Wang Pendahuluan' == $request->document_contract_type){
                                $workflow2->url = '/contract/post/'.$acq->sst->hashslug.'/edit#cash-deposit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';
                            } elseif('CPC' == $request->document_contract_type) {
                                $workflow2->url = '/post/cpc/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';
                            } elseif('CNC' == $request->document_contract_type) {
                                $workflow2->url = '/post/cnc/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';
                            } elseif('CPO' == $request->document_contract_type) {
                                $workflow2->url = '/post/cpo/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';
                            } elseif('CMGD' == $request->document_contract_type) {
                                $workflow2->url = '/post/cmgd/'.$acq->sst->hashslug.'/edit';
                                $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';
                            }

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            $acq->status_task = config('enums.flow23');
                            $acq->verify_status = '1';
                            $acq->update();
         
                            audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        } else {
                            $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

                            $workflow2->flow_location = 'S7';
                            $workflow->flow_desc = 'Semakan '.$request->type.' : Sila beri ulasan untuk proses penyemakan';

                            // if (! empty($review->approved_by)) {
                            //     $email->status  = 'NEW';
                            //     // real email
                                // $email->to      = $review->approve->email;
                            //     // dummy data
                                // $email->to = "coins@ppj.gov.my";

                            //     $email->counter = 1;
                            //     $email->subject = 'PEMBERITAHUAN UNTUK MEMBUAT SEMAKAN : KUIRI';
                            //     $email->text    = 'Ulasan Semakan : ' . $review->remarks . ". Sepatutnya kepada : " . $review->approve->email . ". No Perolehan : " . $review->acquisition->approval->reference;
                            //     $email->save();
                            // } else {
                            //     swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                            // } 

                            audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                        }

                        $workflow2->is_claimed = 1;
                        $workflow2->user_id = null;
                        $workflow2->workflow_team_id = 2;
                    }
                }

            }elseif ('S3' == $bpub->status && '3' != user()->department_id && 'BPUB' == $cnt_review->department) {
                $review->status        = 'S4';
                $review->progress      = $bpub->progress;
                $review->approved_by   = $request->input('approved_by');
                $review->department    = config('enums.BPUB');
                $review->quiry_remarks = $bpub->remarks;

                $reviewlog->status        = 'S4';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $request->input('approved_by');
                $reviewlog->department    = config('enums.BPUB');
                $reviewlog->quiry_remarks = $bpub->remarks;

                $reviewlog1->status        = 'S4';
                $reviewlog1->progress      = $bpub->progress;
                $reviewlog1->approved_by   = $request->input('approved_by');
                $reviewlog1->department    = config('enums.BPUB');

                if('Acquisitions' == $request->type){
                    $workflow->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
                    $workflow->flow_location = 'S3';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan Notis & Dokumen (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Notis & Dokumen (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('SST' == $request->type){
                    $workflow->url = '/acceptance-letter/'.$company->hashslug.'#sst-review';
                    $workflow->flow_location = 'S3';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan SST (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan SST (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                } elseif('Dokumen Kontrak' == $request->type || 'VO' == $request->type){
                    if('Bon Pelaksanaan' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-bon';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Bon Pelaksanaan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Insurans' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-insurance';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Insurans (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Dokumen Kontrak' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-document';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Dokumen Kontrak (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('Wang Pendahuluan' == $request->document_contract_type){
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-deposit';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan Dokumen Kontrak (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan Dokumen Kontrak : Wang Pendahuluan (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CPC' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpc';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CPC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CNC' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cnc';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CNC (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CNC (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CPO' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cpo';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CPO (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CPO (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('CMGD' == $request->document_contract_type) {
                        $workflow->url = '/contract/post/'.$acq->sst->hashslug.'#document-contract-review-cmgd';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan CMGD (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan CMGD (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('PPK' == $request->document_contract_type) {
                        $workflow->url = '/post/variation-order/'.$ppk->hashslug.'/ppk_show#document-review';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPK ' .$ppk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    } elseif('PPJHK' == $request->document_contract_type) {
                        $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'#document-review';

                        if (! empty($review->approved_by)) {
                            $email->status  = 'NEW';
                            // real email
                            $email->to      = $review->approve->email;
                            // dummy data
                            // $email->to = "coins@ppj.gov.my";

                            $email->counter = 1;
                            $email->subject = 'Tindakan : Semakan PPJHK (Kuiri)';
                            $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                            $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan PPJHK ' .$ppjhk->no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                            $email->save();
                        } else {
                            swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                        } 
                        audit($review, __('Semakan '.$request->document_contract_type. ' ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
                    }
         
                } elseif('IPC' == $request->type){
                    $workflow->flow_location = 'S3';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 
                    
                    audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

                } else {
                    $workflow->flow_location = 'S3';

                    if (! empty($review->approved_by)) {
                        $email->status  = 'NEW';
                        // real email
                        $email->to      = $review->approve->email;
                        // dummy data
                        // $email->to = "coins@ppj.gov.my";

                        $email->counter = 1;
                        $email->subject = 'Tindakan : Semakan ' .$request->type. ' (Kuiri)';
                        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan ' .$request->type. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                        $email->save();
                    } else {
                        swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                    } 

                    audit($review, __('Semakan ' .$request->type. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.')); 
                }

                $workflow->is_claimed = 1;
                $workflow->user_id = $request->input('approved_by');
                $workflow->role_id = 5;
                $workflow->workflow_team_id = 1;

            } elseif ('S8' == $bpub->status && '3' != user()->department_id) {
                $review->status        = null;
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->created_by;
                $review->requested_by  = $bpub->approved_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'S9';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->created_by;
                $reviewlog->requested_by  = $bpub->approved_by;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.PP');

                $reviewlog1->status        = 'S9';
                $reviewlog1->progress      = $bpub->progress;
                $reviewlog1->approved_by   = $bpub->created_by;
                $reviewlog1->requested_by  = $bpub->created_by;
                $reviewlog1->department    = config('enums.PP');

                $workflow->flow_location = 'S8';
                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->created_by;
                $workflow->role_id = 4;
                $workflow->workflow_team_id = 1;
                $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila tekan butang hantar untuk proses penyemakan.';

                if (! empty($review->approved_by)) {
                    $email->status  = 'NEW';
                    // real email
                    $email->to      = $review->approve->email;
                    // dummy data
                    // $email->to = "coins@ppj.gov.my";

                    $email->counter = 1;
                    $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                    $email->save();
                } else {
                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                } 
                    
                audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));

            } elseif ('S9' == $bpub->status && '3' != user()->department_id) {
                $review->status        = null;
                $review->progress      = $bpub->progress;
                $review->approved_by   = $bpub->created_by;
                $review->requested_by  = $bpub->approved_by;
                $review->quiry_remarks = $bpub->remarks;
                $review->department    = config('enums.PP');

                $reviewlog->status        = 'CNK';
                $reviewlog->progress      = $bpub->progress;
                $reviewlog->approved_by   = $bpub->created_by;
                $reviewlog->requested_by  = $bpub->approved_by;
                $reviewlog->quiry_remarks = $bpub->remarks;
                $reviewlog->department    = config('enums.PP');

                $reviewlog1->status        = 'CNK';
                $reviewlog1->progress      = $bpub->progress;
                $reviewlog1->approved_by   = $bpub->created_by;
                $reviewlog1->requested_by  = $bpub->created_by;
                $reviewlog1->department    = config('enums.PP');

                $workflow->flow_location = 'S9';
                $workflow->is_claimed = 1;
                $workflow->user_id = $bpub->created_by;
                $workflow->role_id = 4;
                $workflow->workflow_team_id = 1;
                $workflow->flow_desc = 'Semakan '.$request->type.' '.$ipc->ipc_no.' : Sila tekan butang hantar untuk proses penyemakan.';

                if (! empty($review->approved_by)) {
                    $email->status  = 'NEW';
                    // real email
                    $email->to      = $review->approve->email;
                    // dummy data
                    // $email->to = "coins@ppj.gov.my";

                    $email->counter = 1;
                    $email->subject = 'Tindakan : Semakan IPC (Kuiri)';
                    $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
                    $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan IPC '.$ipc->ipc_no. ' (Kuiri) </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Juruukur Bahan & Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';
                    $email->save();
                } else {
                    swal()->success('Teratur', 'Rekod tidak berjaya dihantar. Sila pastikan Pegawai mempunyai Pegawai Atasan', []); 
                } 
                
                audit($review, __('Semakan '.$request->type.' '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Dikuiri kepada '  . $review->approve->name . '.'));
            }

            $review->save();
            $reviewlog->save();
            if (! empty($reviewlog1) && '' != $reviewlog1->status) {
                $reviewlog1->save();
            }
            if (! empty($reviewlog2) && '' != $reviewlog2->status) {
                $reviewlog2->save();
            }

            $workflow->update();
            if (! empty($workflow2)) {
                $workflow2->save();
            }

            if($request->ajax()){
                if(!empty($review->approve)){
                    swal()->success('Kuiri', 'Rekod telah dihantar ke ' . $review->approve->name . '.', []);
                    return response()->json(['message'=>'Rekod telah dihantar ke ' . $review->approve->name . '.']);
                } else {
                    swal()->success('Kuiri', 'Rekod telah dihantar ke Pengesah.', []);
                    return response()->json(['message'=>'Rekod telah dihantar ke Pengesah.']);
                }
            }
            else{
                // return redirect()->back();
                return redirect()->route('home');
            }

            // function kuiri tamat
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acquisition = Acquisition::findByHashSlug($id);
        $review      = Review::where('acquisition_id', $id)->orderby('id', 'desc')->first();
        $bpub        = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.BPUB'))->orderby('id', 'desc')->first();

        $uu = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.UU'))->orderby('id', 'desc')->first();

        $pp = Review::where('acquisition_id', $request->input('acquisition_id'))->where('department', config('enums.PP'))->orderby('id', 'desc')->first();

        return view('contract.pre.review.show', compact('acquisition', 'review', 'bpub', 'uu', 'pp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
