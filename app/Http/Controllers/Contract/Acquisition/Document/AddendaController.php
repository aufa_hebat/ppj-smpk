<?php

namespace App\Http\Controllers\Contract\Acquisition\Document;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Approval;
use App\Models\Addendum\Addendum;
use App\Models\Addendum\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AddendaController extends Controller
{
    public function index(Request $request)
    {
        $hashslug = $request->hashslug;
        $acq      = Acquisition::findByHashSlug($hashslug);
        $approval = Approval::find($acq->acquisition_approval_id);
        $addendas = Addendum::where('acquisition_id', $acq->id ?? 0)->get();

        return view('contract.pre.document.addenda.show', compact('hashslug', 'acq', 'addendas', 'approval'));
    }

    public function create(Request $request)
    {
    }

    public function store(Request $request)
    {
        $tempID                  = null;
        $tempIDP                 = null;
        $ida                     = [];
        $idaS                    = [];
        $idp                     = [];
        $idpS                    = [];
        $hashslug                = $request->hashslug;
        $extended_closed_sale_at = (! empty($request->extended_closed_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->extended_closed_sale_at) : null;

        if (! empty($request->baru)) {
            //check delete dolu
            if (! empty($request->exist)) {
                foreach ($request->exist as $ex) {
                    $checkAddendum = Addendum::where('acquisition_id', $request->acqID)->get();
                    if (! empty($checkAddendum)) {
                        foreach ($checkAddendum as $a) {
                            $checkPerkara = Item::where('addendum_id', $a->id)->get();
                            foreach ($checkPerkara as $p) {
                                $idp[] = $p['id'];
                            }
                            $ida[] = $a['id'];
                        }
                        $idaS[] = $ex['addendaID'];
                        $idpS[] = $ex['perkaraID'];
                        $idpS[] = $ex['attachmentID'];
                    }
                }
                $diffP = array_diff($idp, $idpS);
                $diffA = array_diff($ida, $idaS);

                foreach ($diffP as $pr) {
                    Item::destroy($pr);
                }
                foreach ($diffA as $ad) {
                    Addendum::destroy($ad);
                }
            } else {
                $addendum = Addendum::where('acquisition_id', $request->acqID)->get();
                Item::whereIn('addendum_id', $addendum->pluck('id'))->delete();
                Addendum::where('acquisition_id', $request->acqID)->delete();
            }

            $count                 = Addendum::where('acquisition_id', $request->acqID)->count();
            $adden                 = new Addendum();
            $adden->acquisition_id = $request->acqID;
            $adden->no             = $count + 1;
            $adden->save();
            $tempID = $adden->id;

            foreach ($request->baru as $baru) {
                if (! empty($baru['perkara'])) {
                    $perkara              = new Item();
                    $perkara->addendum_id = $adden->id;
                    $perkara->description = $baru['perkara'];
                    $perkara->attachment  = $baru['attachment'];
                    $perkara->user_id     = user()->id;
                    $perkara->save();
                    $tempIDP = $perkara->id;
                }
            }

            $acq                          = Acquisition::findByHashSlug($hashslug);
            $acq->extended_closed_sale_at = $extended_closed_sale_at;
            $acq->Update();
        } else {
            $addendum = Addendum::where('acquisition_id', $request->acqID)->get();
            Item::whereIn('addendum_id', $addendum->pluck('id'))->delete();
            Addendum::where('acquisition_id', $request->acqID)->delete();
        }
        swal()->success('Addenda', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('doc.addenda.index', ['hashslug' => $hashslug]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
