<?php

namespace App\Http\Controllers\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Singleton\SingletonAcquisition;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Approval;
use App\Models\Acquisition\Category;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\Addendum\Addendum;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\WorkflowTask;

class DocumentController extends Controller
{
    public function index(Request $request)
    {
        $hashslug = session('approval_hashslug');

        if (! empty($request->hashslug)) {
            $hashslug = $request->hashslug;
            $request->session()->put('approval_hashslug', $hashslug);
        }

        $approval = Approval::findByHashSlug($hashslug);

        return view('contract.pre.document.index', compact('hashslug', 'approval'));
    }

    public function create(Request $request)
    {
        $hashslug = $request->hashslug;
        $cnt      = (counter()->counter_acquisition ?? 0) + 1;
        $approval = Approval::findByHashSlug($hashslug);
        //$acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->orWhere('name', 'like', 'Tender' . '%')->pluck('name', 'id');
        if($approval->est_cost_project > 50000000){
            $acq_cat  = Category::where('name', 'like', 'Tender' . '%')->pluck('name', 'id');
        }elseif($approval->est_cost_project <= 50000000){
            $acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->pluck('name', 'id');
        }

        return view('contract.pre.document.create', compact('hashslug', 'cnt', 'approval', 'acq_cat'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'acquisition_approval_id' => 'required',
            'acquisition_category_id' => 'required',
        ], [
            'acquisition_category_id.required' => __('Sila Pilih Kategori Perolehan'),
        ]);

        $approval = Approval::findByHashSlug($request->hashslug);
        //DIN test singleton
        $acq = SingletonAcquisition::save($request);
        
        // event(new \App\Events\Workflow\CreateTasks($acq, \App\Models\Workflow::SECTION, \App\Models\Workflow::DPN));
        // return $acq->id;
        $workflow = new WorkflowTask();
        $workflow->acquisition_id = $acq->id;
        $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
        $workflow->flow_desc = 'Dokumen Perolehan : Sila kemaskini maklumat dokumen perolehan.';
        $workflow->flow_location = 'Dokumen Perolehan';
        $workflow->is_claimed = 1;
        $workflow->user_id = $acq->user_id;
        $workflow->role_id = 4;
        $workflow->workflow_team_id = 1;
        $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-datetime';
        $workflow->save();

        audit($acq, __('Dokumen Perolehan ' .$acq->reference. ' : ' .$approval->title. ' telah berjaya disimpan.'));

        swal()->success('Dokumen Perolehan', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('acquisition.document.edit', ['hashslug' => $acq->hashslug]);
    }

    public function show($id)
    {
        $acquisition = Acquisition::findByHashSlug($id);
        $review      = Review::where('acquisition_id', $acquisition->id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        $reviewlog      = ReviewLog::where('acquisition_id', $acquisition->id)->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        $s1          = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S1')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s1)) {
            $semakan1 = $s1;
        }

        // 1st semakan
        $sl1 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S2')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->get();
        $s2 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s2)) {
            $semakan2 = $s2;
        }

        // 2nd semakan
        $sl2 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNPP')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->get();
        $s3 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s3)) {
            $semakan3 = $s3;
        }
        $s3zero = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNPP')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s3zero)) {
            $semakan3zero = $s3zero;
        }

        // 3rd semakan
        $sl3 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S4')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->get();
        $s4 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S4')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s4)) {
            $semakan4 = $s4;
        }

        // 4th semakan
        $sl4 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S5')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->get();
        $s5 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S5')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s5)) {
            $semakan5 = $s5;
        }

        // 5th semakan
        $sl5 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNBPUB')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->get();
        $cnbpubs = ReviewLog::where('acquisition_id', $acquisition->id)->orderby('id', 'desc')->where('status', 'CNBPUB')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('type', 'Acquisitions')->first();
        if (! empty($cnbpubs)) {
            $cetaknotisbpubs = $cnbpubs;
        }

        $cnbpub = Review::where('status', '!=','Selesai')->where('acquisition_id', $acquisition->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->where('type', 'Acquisitions')->orWhere('status', 'CN')->where('acquisition_id', $acquisition->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->where('type', 'Acquisitions')->orWhere('status', '!=','')->where('acquisition_id', $acquisition->id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->where('type', 'Acquisitions')->first();
        if (! empty($cnbpub)) {
            $cetaknotisbpub = $cnbpub;
        }
        $cnbpub1 = ReviewLog::where('acquisition_id', $acquisition->id)->where('progress', '1')->where('status', 'CNBPUB')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($cnbpub1)) {
            $cetaknotisbpub1 = $cnbpub1;
        }
        $s6 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S6')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s6)) {
            $semakan6 = $s6;
        }

        // 6th semakan
        $sl6 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S7')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->get();
        $s7 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S7')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s7)) {
            $semakan7 = $s7;
        }


        $cn = Review::where('acquisition_id', $acquisition->id)->where('status', 'CN')->where('type', 'Acquisitions')->orderby('id', 'desc')->where('progress', '1')->first();
        if (! empty($cn)) {
            $cetaknotis = $cn;
        }

        // 7th semakan
        $sl7 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNUU')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','Selesai Serah Tugas')->orderby('id', 'desc')->get();
        $cnuu = ReviewLog::where('acquisition_id', $acquisition->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Acquisitions')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($cnuu)) {
            $cetaknotisuu = $cnuu;
        }

        $kuiri = Review::where('acquisition_id', $acquisition->id)->where('status', null)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($kuiri)) {
            $quiry = $kuiri;
        }
        $urus = Review::where('acquisition_id', $acquisition->id)->where('status', 'N')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($urus)) {
            $urusetia = $urus;
        }
        $urus1 = Review::where('acquisition_id', $acquisition->id)->where('status', 'N')->where('progress', '0')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($urus1)) {
            $urusetia1 = $urus1;
        }
        $approval = Approval::find($acquisition->acquisition_approval_id);
        $BQElemen = Element::where('acquisition_id', $acquisition->id)->get();
        $BQItem   = Item::where('acquisition_id', $acquisition->id)->get();
        $BQSub    = SubItem::where('acquisition_id', $acquisition->id)->get();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $acquisition->approval->department_id)->where('position_id', $position->id)->first();
        $addendas = Addendum::where('acquisition_id', $acquisition->id ?? 0)->get();
        //$acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->orWhere('name', 'like', 'Tender' . '%')->pluck('name', 'id');
        if($approval->est_cost_project > 50000000){
            $acq_cat  = Category::where('name', 'like', 'Tender' . '%')->pluck('name', 'id');
        }elseif($approval->est_cost_project <= 50000000){
            $acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->pluck('name', 'id');
        }

        return view('contract.pre.document.show', compact('acquisition', 'approval', 'cat', 'BQElemen', 'BQItem', 'BQSub', 'review', 'np_dept', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'cnbpub', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'cetaknotisbpub', 'quiry', 'urusetia', 'urusetia1', 'addendas', 'acq_cat','sl1','sl2','sl3','sl4','sl5','s3zero','semakan3zero','cnbpubs','cetaknotisbpubs','sl6','semakan7kuiri','s7kuiri','reviewlog','sl7','cetaknotisbpub1'));
    }

    public function edit($id)
    {
        $acquisition = Acquisition::findByHashSlug($id);
        $review      = Review::where('acquisition_id', $acquisition->id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();

        $reviewlog      = ReviewLog::where('acquisition_id', $acquisition->id)->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

        // 1st semakan
        $sl1 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S2')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s2log = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s2log)) {
            $semakan2log = $s2log;
        }

        // 2nd semakan
        $sl2 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNPP')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s3log = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
        if (! empty($s3log)) {
            $semakan3log = $s3log;
        }
        $s3zero = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNPP')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s3zero)) {
            $semakan3zero = $s3zero;
        }

        // 3rd semakan
        $sl3 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S4')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s4log = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S4')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s4log)) {
            $semakan4log = $s4log;
        }

        // 4th semakan
        $sl4 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S5')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s5log = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S5')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s5log)) {
            $semakan5log = $s5log;
        }

        // 5th semakan
        $sl5 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNBPUB')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $cnbpubs = ReviewLog::where('acquisition_id', $acquisition->id)->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
        if (! empty($cnbpubs)) {
            $cetaknotisbpubs = $cnbpubs;
        }

        // 6th semakan
        $sl6 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S7')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
        $s7log = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'S7')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($s7log)) {
            $semakan7log = $s7log;
        }

        // 7th semakan
        $sl7 = ReviewLog::where('acquisition_id', $acquisition->id)->where('status', 'CNUU')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('reviews_status','!=','Selesai Serah Tugas')->orderby('id', 'desc')->get();
        $cnuulog = ReviewLog::where('acquisition_id', $acquisition->id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'Acquisitions')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
        if (! empty($cnuulog)) {
            $cetaknotisuulog = $cnuulog;
        }

        $s1          = Review::where('acquisition_id', $acquisition->id)->where('status', 'S1')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s1)) {
            $semakan1 = $s1;
        }
        $s2 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S2')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s2)) {
            $semakan2 = $s2;
        }
        $s3 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S3')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s3)) {
            $semakan3 = $s3;
        }
        $s4 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S4')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s4)) {
            $semakan4 = $s4;
        }
        $s5 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S5')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s5)) {
            $semakan5 = $s5;
        }
        $s6 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S6')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s6)) {
            $semakan6 = $s6;
        }
        $s7 = Review::where('acquisition_id', $acquisition->id)->where('status', 'S7')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($s7)) {
            $semakan7 = $s7;
        }
        $cn = Review::where('acquisition_id', $acquisition->id)->where('status', 'CN')->where('type', 'Acquisitions')->orderby('id', 'desc')->where('progress', '1')->where('department', 'BPUB')->first();
        if (! empty($cn)) {
            $cetaknotis = $cn;
        }
        $cnuu = Review::where('acquisition_id', $acquisition->id)->where('status', 'CN')->where('type', 'Acquisitions')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
        if (! empty($cnuu)) {
            $cetaknotisuu = $cnuu;
        }
        $cnbpub = Review::where('status', 'Selesai')->where('acquisition_id', $acquisition->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'Acquisitions')->orWhere('status', 'CN')->where('acquisition_id', $acquisition->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'BPUB')->where('type', 'Acquisitions')->orWhere('status', null)->where('acquisition_id', $acquisition->id)->where('progress', '1')->orderby('id', 'desc')->where('department', 'PELAKSANA')->where('type', 'Acquisitions')->first();
        if (! empty($cnbpub)) {
            $cetaknotisbpub = $cnbpub;
        }
        $kuiri = Review::where('acquisition_id', $acquisition->id)->where('status', null)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($kuiri)) {
            $quiry = $kuiri;
        }
        $urus = Review::where('acquisition_id', $acquisition->id)->where('status', 'N')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($urus)) {
            $urusetia = $urus;
        }
        $urus1 = Review::where('acquisition_id', $acquisition->id)->where('status', 'N')->where('progress', '0')->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
        if (! empty($urus1)) {
            $urusetia1 = $urus1;
        }
        $cnt      = (counter()->counter_acquisition ?? 0) + 1;
        $approval = Approval::find($acquisition->acquisition_approval_id);
        $BQElemen = Element::where('acquisition_id', $acquisition->id)->orderBy('no','asc')->get();
        $BQItem   = Item::where('acquisition_id', $acquisition->id)->orderBy('bq_no_element','asc')->orderBy('no','asc')->get();
        $BQSub    = SubItem::where('acquisition_id', $acquisition->id)->orderBy('bq_no_element','asc')->orderBy('bq_no_item','asc')->orderBy('no','asc')->get();
        //$acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->orWhere('name', 'like', 'Tender' . '%')->pluck('name', 'id');
        if($approval->est_cost_project > 50000000){
            $acq_cat  = Category::where('name', 'like', 'Tender' . '%')->pluck('name', 'id');
        }elseif($approval->est_cost_project <= 50000000){
            $acq_cat  = Category::where('name', 'like', 'Sebut Harga' . '%')->pluck('name', 'id');
        }

        return view('contract.pre.document.edit', compact('acquisition', 'cnt', 'approval', 'BQElemen', 'BQItem', 'BQSub', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'quiry', 'urusetia', 'urusetia1', 'acq_cat','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog','cetaknotisbpub'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'acquisition_approval_id' => 'required',
            'acquisition_category_id' => 'required',
        ]);

        $acq                          = Acquisition::findByHashSlug($id);
        $acq->acquisition_approval_id = $request->acquisition_approval_id;
        $acq->acquisition_category_id = $request->acquisition_category_id;
        $acq->document_price          = money()->toMachine($request->document_price ?? 0);
//        $acq->gst                     = $request->gst;
//        $acq->total_document_price    = money()->toMachine($request->total_document_price ?? 0);

        $acq->advertised_at       = (! empty($request->advertised_at)) ? Carbon::createFromFormat('d/m/Y', $request->advertised_at) : null;
        $acq->start_sale_at       = (! empty($request->start_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->start_sale_at) : null;
        $acq->closed_at           = (! empty($request->closed_at)) ? Carbon::createFromFormat('d/m/Y', $request->closed_at) : null;
        $acq->closed_sale_at      = (! empty($request->closed_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->closed_sale_at) : null;
        $acq->experience_status   = (isset($request->experience_status)) ? true : false;
        $acq->experience_value    = money()->toMachine($request->experience_value ?? 0);
        $acq->experience_duration = $request->experience_duration;
        $acq->briefing_status     = (isset($request->briefing_status)) ? true : false;
        $acq->briefing_at         = (! empty($request->briefing_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->briefing_at) : null;
        $acq->briefing_location   = $request->briefing_location;
        $acq->visit_status        = (isset($request->visit_status)) ? true : false;
        $acq->visit_date_time     = (! empty($request->visit_date_time)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->visit_date_time) : null;
        $acq->visit_location      = $request->visit_location;

        $acq->status_task   = config('enums.flow1');
        $acq->document_sale_location = $request->document_sale_location;
        $acq->user_id                = user()->id;
        $acq->save();

        $workflow = new WorkflowTask();
        $workflow->acquisition_id = $acq->id;
        $workflow->flow_name = 'Maklumat Dokumen Perolehan';
        $workflow->flow_desc = 'Dokumen Perolehan : Sila kemaskini dokumen perolehan.';
        $workflow->flow_location = 'Dokumen Perolehan';
        $workflow->is_claimed = 1;
        $workflow->user_id = $acq->user_id;
        $workflow->workflow_team_id = 1;
        $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
        $workflow->save();

        swal()->success('Dokumen Perolehan', 'Rekod telah berjaya dikemaskini.', []);

        $apprv = Approval::find($request->acquisition_approval_id);

        audit($apprv, __('Dokumen Perolehan ' .$acq->reference. ' : ' .$apprv->title. ' telah berjaya dikemaskini.'));

        return redirect()->route('acquisition.document.index', ['hashslug' => $apprv->hashslug]);
    }

    public function destroy($id)
    {
    }
}
