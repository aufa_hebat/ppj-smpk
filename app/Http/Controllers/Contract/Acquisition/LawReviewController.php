<?php

namespace App\Http\Controllers\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use Illuminate\Http\Request;
use App\Models\EmailScheduler;
use App\Models\WorkflowTask;
use Illuminate\Support\Carbon;

class LawReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'law_task_by'       => 'required',
        ],$messages = [
            'law_task_by.required' => 'Anda perlu pilih pegawai utk ditugaskan.',
        ]);

        $acq    = Acquisition::find($id);
        $review = Review::where('Acquisition_id', $acq->id)->where('status', 'S6')->where('progress', 0)->orderby('id', 'desc')->first();
        $review1 = Review::where('Acquisition_id', $acq->id)->orderby('id', 'desc')->first();

        $review->approved_by = $request->input('law_task_by');
        $review->progress    = 1;
        $review->law_task_by = $request->input('law_task_by');

        $reviewlog = new ReviewLog();
        $reviewlog->acquisition_id = $acq->id;
        $reviewlog->status       = 'CNUU';
        $reviewlog->progress     = 1;
        $reviewlog->remarks      = 'Serah Tugasan Kepada ' . $review->lawtask->name;
        $reviewlog->approved_by  = $request->input('law_task_by');
        $reviewlog->approved_at  = Carbon::now();
        $reviewlog->department   = config('enums.UU');
        $reviewlog->created_by   = $review->created_by;
        $reviewlog->requested_by = user()->id;
        $reviewlog->requested_at  = $review1->approved_at;
        $reviewlog->type  = 'Acquisitions';
        $reviewlog->reviews_status = 'Selesai Serah Tugas';
        $reviewlog->law_task_by = $request->input('law_task_by');

        // ReviewLog::where('acquisition_id',$request->input('acquisition_id'))->where('reviews_status','Terima')->where('department', config('enums.UU'))->delete();

        $reviewlog2 = new ReviewLog();
        $reviewlog2->acquisition_id = $acq->id;
        $reviewlog2->status       = 'CNUU';
        $reviewlog2->progress     = 1;
        $reviewlog2->approved_by  = $request->input('law_task_by');
        $reviewlog2->approved_at  = Carbon::now();
        $reviewlog2->department   = config('enums.UU');
        $reviewlog2->created_by   = $review->created_by;
        $reviewlog2->requested_by = $request->input('law_task_by');
        $reviewlog2->requested_at  = $review1->approved_at;
        $reviewlog2->type  = 'Acquisitions';
        $reviewlog2->reviews_status = 'Terima';
        $reviewlog2->law_task_by = $request->input('law_task_by');

        $workflow2 = WorkflowTask::where('acquisition_id', $acq->id)->where('workflow_team_id',2)->first();

        $workflow2->flow_desc = 'Semakan Dokumen Perolehan : Sila beri ulasan untuk proses penyemakan.';
        $workflow2->url = '/acquisition/document/'.$acq->hashslug.'#document-review';
        $workflow2->flow_location = 'S6';
        $workflow2->is_claimed = 1;
        $workflow2->user_id = $review->law_task_by;
        $workflow2->role_id = 5;
        $workflow2->workflow_team_id = 2;
                    
        audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Dihantar Kepada ' . $review->approve->name));

        $email = new EmailScheduler();
        $email->status  = 'NEW';
        // real email
        $email->to      = $review->approve->email;
        // dummy data
        // $email->to = "coins@ppj.gov.my";

        $email->counter = 1;
        $email->subject = 'Tindakan : Semakan dan Sahkan Notis & Dokumen';
        $dept           = $review->request->bahagian ? $review->request->bahagian->name : '-';
        $email->text    = '<p>Berikut memerlukan tindakan pihak Tuan/Puan :-</p><p><br/><b>Tindakan : Semakan dan Sahkan Notis & Dokumen </b></br>No Kontrak : '.$acq->reference.'<br/>Pemohon : '.$review->request->name.'<br/>Jabatan : '.$review->request->department->name.'<br/>Bahagian : '.$dept.'<br/>Sila ke sistem COINS untuk tindakan selanjutnya</br></br>Tindakan : Penolong Pengarah</br></br></br><a href="https://coins.ppj.gov.my">https://coins.ppj.gov.my</a>';

        $review->update();
        $reviewlog->save();
        $reviewlog2->save();
        $email->save();
        $workflow2->update();

        swal()->success('Pemilihan Dokumen Kontrak', 'Rekod telah berjaya dihantar ke ' . $review->approve->name . '.', []);

        // return redirect()->back();
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
