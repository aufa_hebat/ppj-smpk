<?php

namespace App\Http\Controllers\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use App\Models\Department;
use App\Models\Acquisition\Type;
use App\Models\Singleton\SingletonApproval;
use Illuminate\Http\Request;

class PreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total = 12;

        return view('contract.pre.index', compact('total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $acq_types      = Type::where('id', '!=', 4)->pluck('name', 'id');
        $acq_types      = Type::where('id', '!=', null)->pluck('name', 'id');
        
        return view('contract.pre.create',compact('acq_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'               => 'required|min:5',
            'acquisition_type_id' => 'required',
            'period_length'       => 'required|min:1',
        ]);

        /*
        $approval = Approval::create([
            'user_id'             => user()->id,
            'department_id'       => null !== optional(user()->department)->id ? optional(user()->department)->id : Department::ALL_ID, // All
            'acquisition_type_id' => $request->acquisition_type_id,
            'period_type_id'      => $request->period_type_id,
            'title'               => strtoupper($request->title),
            'description'         => $request->description,
            'year'                => $request->year,
            'file_reference'      => $request->file_reference,
            'period_length'       => $request->period_length,
            'est_cost_project'    => money()->toMachine('0'),
        ]);
         
         */
        //DIN test singleton
        $approval = SingletonApproval::save($request);

        audit($approval, __('Kelulusan Perolehan ' .strtoupper($request->title). ' telah berjaya disimpan.'));

        swal()->success('Kelulusan Perolehan', 'Rekod telah berjaya dijana.', []);

        return redirect()->route('contract.pre.edit', $approval->hashslug);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approval = Approval::withDetails()->findByHashSlug($id);

        $approval->title       = $approval->title;
        $approval->description = $approval->description;

        if (Department::ALL_ID == $approval->department_id) {
            $department = Department::exceptAll()->get();
        }

        if($approval->acquisition_type_id == 1 || $approval->acquisition_type_id == 3) {
            $cidb = common()->getKelayakanMOFView($approval->id);
        }else{
            $cidb = common()->getKelayakanCIDBView($approval->id);
        }

        
        
        $acq_types      = Type::where('id', '!=', 4)->pluck('name', 'id');

        return view('contract.pre.show', compact('approval', 'department', 'cidb', 'acq_types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori       = cidb_codes('category');
        $kategori_mof   = mof_codes('category');
        $approval       = Approval::withDetails()->findByHashSlug($id);
        // $acq_types      = Type::where('id', '!=', 4)->pluck('name', 'id');
        $acq_types      = Type::where('id', '!=', null)->pluck('name', 'id');
        
        return view('contract.pre.edit', compact('approval', 'kategori', 'kategori_mof','acq_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'               => 'required|min:5',
            'acquisition_type_id' => 'required',
            'period_length'       => 'required|min:1',
            'est_cost_project'    => 'required',
        ]);

        Approval::hashslug($id)->update($request->only('acquisition_type_id'));

        swal()->success('Kelulusan Perolehan', 'Rekod telah berjaya dikemaskini', []);

        return redirect()->route('contract.pre.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Approval::hashslug($id)->delete();

        swal()->success('Kelulusan Perolehan', 'Rekod telah berjaya dihapuskan.', []);

        return redirect()->route('contract.pre.index');
    }

    // private function getKelayakanCIDB(int $approval_id)
    // {
    //     $approval = Approval::find($approval_id);

    //     $cidb             = '';
    //     $atatusGrade      = '';
    //     $atatusCategoryB  = '';
    //     $atatusCategoryCE = '';
    //     $atatusCategoryM  = '';
    //     $atatusCategoryE  = '';
    //     $atatusKhususB    = '';
    //     $atatusKhususCE   = '';
    //     $atatusKhususM    = '';
    //     $atatusKhususE    = '';
    //     $catB             = '';
    //     $catM             = '';
    //     $catE             = '';
    //     $catCE            = '';
    //     $br               = '';

    //     if ($approval->cidbQualifications->count() > 0) {
    //         foreach ($approval->cidbQualifications as $index => $row) {
    //             if ('grade' == $row->code->type) {
    //                 if (0 == $index) {
    //                     $cidb = '';
    //                 }

    //                 $cidb = $cidb . ' ' . $atatusGrade . ' ' . $row->code->code;

    //                 if (1 == $row->status) {
    //                     $atatusGrade = 'Dan';
    //                 } else {
    //                     $atatusGrade = 'Atau';
    //                 }
    //             } else {
    //                 if ('category' == $row->code->type) {
    //                     if ('B' == $row->code->code) {
    //                         $catB = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryB = 'Dan';
    //                         } else {
    //                             $atatusCategoryB = 'Atau';
    //                         }
    //                     } elseif ('CE' == $row->code->code) {
    //                         $catCE = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryCE = 'Dan';
    //                         } else {
    //                             $atatusCategoryCE = 'Atau';
    //                         }
    //                     } elseif ('M' == $row->code->code) {
    //                         $catM = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryM = 'Dan';
    //                         } else {
    //                             $atatusCategoryM = 'Atau';
    //                         }
    //                     } elseif ('E' == $row->code->code) {
    //                         $catE = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryE = 'Dan';
    //                         } else {
    //                             $atatusCategoryE = 'Atau';
    //                         }
    //                     }
    //                 }

    //                 if ('khusus' == $row->code->type) {
    //                     if ('B' == $row->code->category) {
    //                         $catB = $catB . ' ' . $atatusKhususB . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususB = 'Dan';
    //                         } else {
    //                             $atatusKhususB = 'Atau';
    //                         }
    //                     } elseif ('CE' == $row->code->category) {
    //                         $catCE = $catCE . ' ' . $atatusKhususCE . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususCE = 'Dan';
    //                         } else {
    //                             $atatusKhususCE = 'Atau';
    //                         }
    //                     } elseif ('M' == $row->code->category) {
    //                         $catM = $catM . ' ' . $atatusKhususM . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususM = 'Dan';
    //                         } else {
    //                             $atatusKhususM = 'Atau';
    //                         }
    //                     } elseif ('E' == $row->code->category) {
    //                         $catE = $catE . ' ' . $atatusKhususE . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususE = 'Dan';
    //                         } else {
    //                             $atatusKhususE = 'Atau';
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //         if (! empty($catB)) {
    //             $cidb = $cidb . '<br/>' . $catB;
    //         }
    //         if (! empty($catCE)) {
    //             if (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catCE;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catCE;
    //             }
    //         }
    //         if (! empty($catM)) {
    //             if (! empty($catCE)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryCE . '<br/> ' . $catM;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catM;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catM;
    //             }
    //         }
    //         if (! empty($catE)) {
    //             if (! empty($catM)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryM . '<br/> ' . $catE;
    //             } elseif (! empty($catCE)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryCE . '<br/> ' . $catE;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catE;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catE;
    //             }
    //         }
    //     }

    //     return $cidb;
    // }

    // private function getKelayakanMOF(int $approval_id)
    // {
    //     $approval = Approval::find($approval_id);
    //     $collectionCategoryCode = collect();
    //     $collectionCategoryName = collect();
    //     $collectionCategoryStatus = collect();
    //     $collectionKhususCat = collect([]);
    //     $collectionKhususCode = collect([]);
    //     $collectionKhususName = collect([]);
    //     $collectionKhususStatus = collect([]);


    //     if ($approval->mofQualifications->count() > 0) {
    //         foreach ($approval->mofQualifications as $index => $row) {

    //             if ('category' == $row->code->type) {
    //                 $collectionCategoryCode = $collectionCategoryCode->concat([$row->code->code]);
    //                 $collectionCategoryName = $collectionCategoryName->concat([$row->code->name]);
    //                 $collectionCategoryStatus = $collectionCategoryStatus->concat([$row->status]);
    //             }elseif ('khusus' == $row->code->type) {
    //                 $collectionKhususCat = $collectionKhususCat->concat([$row->code->category]);
    //                 $collectionKhususCode = $collectionKhususCode->concat([$row->code->code]);
    //                 $collectionKhususName = $collectionKhususName->concat([$row->code->name]);
    //                 $collectionKhususStatus = $collectionKhususStatus->concat([$row->status]);
    //             }
    //         }            
    //     }

    //     $mofBidang = '';
    //     $bidangStatus = '';
    //     $mofKhusus = '';
    //     $status = '';
    //     $mofCombine = '';
    //     $mofLoop = '';
        
    //     for($c = 0; $c< $collectionCategoryCode->count(); $c++){
    //         $mofBidang = 'Bidang ' . $collectionCategoryCode[$c] . ' ';

    //         if($collectionCategoryStatus[$c] == '0')
    //             $bidangStatus = 'ATAU';
    //         elseif($collectionCategoryStatus[$c] == '1')
    //             $bidangStatus = 'DAN';
    //         else
    //             $bidangStatus = '';

    //         $mofKhusus = 'Pengkhususan ';
    //         for($k = 0; $k < $collectionKhususCat->count(); $k++){
    //             if($collectionKhususCat[$k] == $collectionCategoryCode[$c]){
                    
    //                 if($collectionKhususStatus[$k] == '0')
    //                     $status = 'ATAU';
    //                 elseif($collectionKhususStatus[$k] == '1')
    //                     $status = 'DAN';
    //                 else
    //                     $status = '';

    //                 $mofKhusus = $mofKhusus . ' ' . $collectionKhususCode[$k] . ' ' . $status;
    //             }
    //         }
    //         $mofCombine = $mofBidang . $mofKhusus . $bidangStatus . '<br/>';
    //         $mofLoop = $mofLoop . $mofCombine;

    //     }

    //     return $mofLoop;
    // }
}
