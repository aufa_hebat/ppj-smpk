<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Review;
use App\Models\ReviewLog;
use App\Models\Acquisition\Sst;
use App\Models\Company\CIDB;
use App\Models\Company\MOF;
use App\Models\Sale;
use App\Models\ReportType;
use Illuminate\Http\Request;

class AcceptanceLetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request);
        //dd($request->tag);
        
        $tag = $request->tag;
        //if($tag == "6MonthToEnd"){
           // dd(true);
        //}else{
           // dd(false);
       // }
        return view('contract.post.acceptance-letter.index',compact('tag'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointed = AppointedCompany::findByHashSlug($id);
        $sst       = Sst::where([
                    ['company_id', '=', $appointed->company_id],
                    ['acquisition_id', '=', $appointed->acquisition_id],
                ])->first();
        $sale = Sale::where([
                    ['company_id', '=', $appointed->company_id],
                    ['acquisition_id', '=', $appointed->acquisition_id],
                ])->first();
        $cidb = CIDB::findByHashSlug($appointed->company->hashslug);
        $mof  = MOF::findByHashSlug($appointed->company->hashslug);

        $reportType = ReportType::where([
            ['acquisition_type_id', '=', $appointed->acquisition->approval->acquisition_type_id],
            ])->pluck('name', 'id');

        if (! empty($sst)) {
            $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'SST')->orderby('id', 'desc')->first();
            $reviewlog      = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNUU')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }

            $s1              = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S1')->where('type', 'SST')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S3')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S6')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'SST')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('status', 'Selesai')->where('type', 'SST')->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'SST')->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'UNDANG2')->orderby('id', 'desc')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $cnbpub = Review::where('status', 'Selesai')->where('type', 'SST')->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', 'CN')->where('type', 'SST')->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'BPUB')->orderby('id', 'desc')->orWhere('status', null)->where('type', 'SST')->where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('department', 'PELAKSANA')->orderby('id', 'desc')->first();
            if (! empty($cnbpub)) {
                $cetaknotisbpub = $cnbpub;
            }
            $kuiri = Review::where('acquisition_id', $sst->acquisition_id)->where('status', null)->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }

        if (null != $sst) {
            $sst->all();
        }

        return view('contract.post.acceptance-letter.show', compact('appointed', 'sst', 'sale', 'cidb', 'mof', 'review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'cnbpub', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotisbpub', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog','reviewlog', 'reportType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointed = AppointedCompany::findByHashSlug($id);
        $sst       = Sst::where([
                    ['company_id', '=', $appointed->company_id],
                    ['acquisition_id', '=', $appointed->acquisition_id],
                ])->first();
        $sale = Sale::where([
                    ['company_id', '=', $appointed->company_id],
                    ['acquisition_id', '=', $appointed->acquisition_id],
                ])->first();
        $cidb = CIDB::findByHashSlug($appointed->company->hashslug);
        $mof  = MOF::findByHashSlug($appointed->company->hashslug);

        //dd($appointed->acquisition->approval->acquisition_type_id);
        $reportType = ReportType::where([
            ['acquisition_type_id', '=', $appointed->acquisition->approval->acquisition_type_id],
            ])->pluck('name', 'id');

        //Harga Kontrak tanpa WPS
        //$priceWithoutWps = sst($sst)->getPriceWithoutWps($appointed);
        //$priceWithoutWps = common()->getPriceWithoutWps($appointed);
        $priceWithoutWps = appointed($appointed)->getPriceWithoutWps($appointed);

        //insuran
        //$insPliAmount = sst($sst)->getInsuranTanggunganAwam($appointed->acquisition->category->name, $appointed->offered_price);
        //$insPliAmount = common()->getInsuranTanggunganAwam($appointed->acquisition->category->name, $appointed->offered_price);
        $insPliAmount = appointed($appointed)->getInsuranTanggunganAwam($appointed->acquisition->category->name, $appointed->offered_price);
        $insWorkAmount = $appointed->offered_price;
        $insCliAmount = $priceWithoutWps * 0.1;

        if (! empty($sst)) {
            $review_previous = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
            $review          = Review::where('acquisition_id', $sst->acquisition_id)->where('type', 'SST')->orderby('id', 'desc')->first();

            // 1st semakan
            $sl1 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s2log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id', 'desc')->first();
            if (! empty($s2log)) {
                $semakan2log = $s2log;
            }

            // 2nd semakan
            $sl2 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Selesai Serah Tugas')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s3log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3log)) {
                $semakan3log = $s3log;
            }
            $s3zero = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNPP')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('progress','0')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s3zero)) {
                $semakan3zero = $s3zero;
            }

            // 3rd semakan
            $sl3 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s4log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s4log)) {
                $semakan4log = $s4log;
            }

            // 4th semakan
            $sl4 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s5log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s5log)) {
                $semakan5log = $s5log;
            }

            // 5th semakan
            $sl5 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNBPUB')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnbpubs = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('status', 'CNBPUB')->orderby('id', 'desc')->first();
            if (! empty($cnbpubs)) {
                $cetaknotisbpubs = $cnbpubs;
            }

            // 6th semakan
            $sl6 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $s7log = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($s7log)) {
                $semakan7log = $s7log;
            }

            // 7th semakan
            $sl7 = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('status', 'CNUU')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->get();
            $cnuulog = ReviewLog::where('acquisition_id', $sst->acquisition_id)->where('progress', '1')->where('status', 'CNUU')->where('type', 'SST')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->where('reviews_status','!=','')->where('reviews_status','!=','Deraf')->orderby('id', 'desc')->first();
            if (! empty($cnuulog)) {
                $cetaknotisuulog = $cnuulog;
            }
            $s1              = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S1')->where('type', 'SST')->orderby('id', 'desc')->first();

            if (! empty($s1)) {
                $semakan1 = $s1;
            }
            $s2 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S2')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s2)) {
                $semakan2 = $s2;
            }
            $s3 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S3')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s3)) {
                $semakan3 = $s3;
            }
            $s4 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S4')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s4)) {
                $semakan4 = $s4;
            }
            $s5 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S5')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s5)) {
                $semakan5 = $s5;
            }
            $s6 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S6')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s6)) {
                $semakan6 = $s6;
            }
            $s7 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'S7')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($s7)) {
                $semakan7 = $s7;
            }
            $cn = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'SST')->orderby('id', 'desc')->where('progress', '1')->first();
            if (! empty($cn)) {
                $cetaknotis = $cn;
            }
            $cnuu = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'CN')->where('type', 'SST')->orderby('id', 'desc')->where('progress', '0')->where('department', 'UNDANG2')->first();
            if (! empty($cnuu)) {
                $cetaknotisuu = $cnuu;
            }
            $kuiri = Review::where('acquisition_id', $sst->acquisition_id)->where('status', null)->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($kuiri)) {
                $quiry = $kuiri;
            }
            $urus = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($urus)) {
                $urusetia = $urus;
            }
            $urus1 = Review::where('acquisition_id', $sst->acquisition_id)->where('status', 'N')->where('progress', '0')->where('type', 'SST')->orderby('id', 'desc')->first();
            if (! empty($urus1)) {
                $urusetia1 = $urus1;
            }
        }

        return view('contract.post.acceptance-letter.edit', compact('appointed', 'sst', 'sale', 'cidb', 'mof', 'insPliAmount', 'insWorkAmount', 'insCliAmount', 'priceWithoutWps','review_previous', 'review', 's1', 's2', 's3', 's4', 's5', 's6', 's7', 'cnuu', 'kuiri', 'urus', 'urus1', 'semakan1', 'semakan2', 'semakan3', 'semakan4', 'semakan5', 'semakan6', 'semakan7', 'cetaknotis', 'cetaknotisuu', 'cetaknotis', 'quiry', 'urusetia', 'urusetia1','sl1','sl2','sl3','sl4','sl5','sl6','sl7', 'cnuulog','semakan2log','semakan3log','semakan3zero','semakan4log','semakan5log','cetaknotisbpubs','semakan7log','cetaknotisuulog', 'reportType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
