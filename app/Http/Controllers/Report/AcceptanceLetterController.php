<?php

namespace App\Http\Controllers\Report;

use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sst;
use App\Models\Company\CIDB;
use App\Models\Company\MOF;
use Illuminate\Http\Request;

class AcceptanceLetterController extends BaseController
{
    public function __invoke(Request $request)
    {
        $appointed = AppointedCompany::withDetails()->findByHashSlug($request->hashslug);
        $sst       = Sst::where([
                    ['company_id', '=', $appointed->company_id],
                    ['acquisition_id', '=', $appointed->acquisition_id],
                ])->with('company')->first();
        $cidb = CIDB::findByHashSlug($appointed->company->hashslug);
        $mof  = MOF::findByHashSlug($appointed->company->hashslug);
        $bon      = Bon::where('sst_id', $sst->id)->first();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $sst->acquisition->approval->department_id)->where('position_id', $position->id)->first() ?? '';

        $sale = Sale::where([
            ['company_id', '=', $appointed->company_id],
            ['acquisition_id', '=', $appointed->acquisition_id],
        ])->first();

        $data = [
            'appointed' => $appointed,
            'sst'       => $sst,
            'cidb'      => $cidb,
            'mof'       => $mof,
            'bon'       => $bon,
            'np_dept'   => $np_dept,
            'sale'      => $sale,
        ];

        $period_length = false; // [ True : less then 4 month  | False : more than 4 month ]
        if (1 == $sst->acquisition->approval->period_type_id && $sst->acquisition->approval->period_length < 120) {
            $period_length = true;
        } elseif (2 == $sst->acquisition->approval->period_type_id && $sst->acquisition->approval->period_length < 16) {
            $period_length = true;
        } elseif (4 == $sst->acquisition->approval->period_type_id && $sst->acquisition->approval->period_length < 4) {
            $period_length = true;
        }

        if ('1' == $sst->acquisition->acquisition_category_id || '2' == $sst->acquisition->acquisition_category_id) {
            if ($period_length) {
                if (! empty($sst->company->gst_registered_no)) {
                    $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_b';
                } else {
                    $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_a';
                }
            } else {
                $report_name = 'contract.post.acceptance-letter.report.acceptance_letter';
            }
        } elseif ('3' == $sst->acquisition->acquisition_category_id || '4' == $sst->acquisition->acquisition_category_id) {
            if (! empty($sst->company->gst_registered_no)) {
                $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_c';
            } else {
                $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_d';
            }
        }


        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Surat_Setuju_Terima_' . snake_case($appointed->acquisition->title),
            'reportViewPath' => $report_name,
        ];

        return $this->generate($request, $config, $data);
    }
}
