<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use PDF;

class BaseController extends Controller
{
    public function generate(Request $request, array $config, array $data)
    {
        $noPerolehan = '';

        if (! empty($config['acquisition'])) {
            $noPerolehan = str_replace('/', '', $config['acquisition']->reference);
        }

        $data = array_add($data, 'hashslug', $request->hashslug);
        $data = array_add($data, 'routeName', Route::currentRouteName());
        $data = array_add($data, 'reportViewPath', $config['reportViewPath']);

        view()->share($data);

        // if ($request->has('download')) {
            $pdf = PDF::loadView($config['reportViewPath'], ['print' => true]);
            $pdf->setPaper($config['size'], $config['orientation']);

            return $pdf->stream(
                now(config('app.timezone'))->format(config('datetime.file')) . '-' .
                $noPerolehan . '-' . $config['outputFileName'] . '.pdf'
            );
        // }

        // return view('layouts.report', ['print' => false]);
    }


    private function getKelayakanCIDB(int $approval_id)
    {
        $approval = Approval::find($approval_id);

        $cidb             = '';
        $atatusGrade      = '';
        $atatusCategoryB  = '';
        $atatusCategoryCE = '';
        $atatusCategoryM  = '';
        $atatusCategoryE  = '';
        $atatusKhususB    = '';
        $atatusKhususCE   = '';
        $atatusKhususM    = '';
        $atatusKhususE    = '';
        $catB             = '';
        $catM             = '';
        $catE             = '';
        $catCE            = '';

        if ($approval->cidbQualifications->count() > 0) {
            foreach ($approval->cidbQualifications as $index => $row) {
                if ('grade' == $row->code->type) {
                    if (0 == $index) {
                        $cidb = 'Gred';
                    }

                    $cidb = $cidb . ' ' . $atatusGrade . ' ' . $row->code->code;

                    if (1 == $row->status) {
                        $atatusGrade = 'Dan';
                    } else {
                        $atatusGrade = 'Atau';
                    }
                } else {
                    if ('category' == $row->code->type) {
                        if ('B' == $row->code->code) {
                            $catB = 'Kategori B';

                            if (1 == $row->status) {
                                $atatusCategoryB = 'Dan';
                            } else {
                                $atatusCategoryB = 'Atau';
                            }
                        } elseif ('CE' == $row->code->code) {
                            $catCE = 'Kategori CE';

                            if (1 == $row->status) {
                                $atatusCategoryCE = 'Dan';
                            } else {
                                $atatusCategoryCE = 'Atau';
                            }
                        } elseif ('M' == $row->code->code) {
                            $catM = 'Kategori M';

                            if (1 == $row->status) {
                                $atatusCategoryM = 'Dan';
                            } else {
                                $atatusCategoryM = 'Atau';
                            }
                        } elseif ('E' == $row->code->code) {
                            $catE = 'Kategori E';

                            if (1 == $row->status) {
                                $atatusCategoryE = 'Dan';
                            } else {
                                $atatusCategoryE = 'Atau';
                            }
                        }
                    }

                    if ('khusus' == $row->code->type) {
                        if ('B' == $row->code->category) {
                            $catB = $catB . ' ' . $atatusKhususB . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususB = 'Dan';
                            } else {
                                $atatusKhususB = 'Atau';
                            }
                        } elseif ('CE' == $row->code->category) {
                            $catCE = $catCE . ' ' . $atatusKhususCE . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususCE = 'Dan';
                            } else {
                                $atatusKhususCE = 'Atau';
                            }
                        } elseif ('M' == $row->code->category) {
                            $catM = $catM . ' ' . $atatusKhususM . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususM = 'Dan';
                            } else {
                                $atatusKhususM = 'Atau';
                            }
                        } elseif ('E' == $row->code->category) {
                            $catE = $catE . ' ' . $atatusKhususE . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususE = 'Dan';
                            } else {
                                $atatusKhususE = 'Atau';
                            }
                        }
                    }
                }
            }
            if (! empty($catB)) {
                $cidb = $cidb . ', ' . $catB;
            }
            if (! empty($catCE)) {
                if (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catCE;
                } else {
                    $cidb = $cidb . ', ' . $catCE;
                }
            }
            if (! empty($catM)) {
                if (! empty($catCE)) {
                    $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catM;
                } elseif (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catM;
                } else {
                    $cidb = $cidb . ', ' . $catM;
                }
            }
            if (! empty($catE)) {
                if (! empty($catM)) {
                    $cidb = $cidb . ', ' . $atatusCategoryM . ' ' . $catE;
                } elseif (! empty($catCE)) {
                    $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catE;
                } elseif (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catE;
                } else {
                    $cidb = $cidb . ', ' . $catE;
                }
            }
        }

        return $cidb;
    }
}
