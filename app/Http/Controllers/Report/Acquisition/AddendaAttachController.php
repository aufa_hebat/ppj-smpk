<?php

namespace App\Http\Controllers\Report\Acquisition;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class AddendaAttachController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acq     = Acquisition::withAddendums()->withDetails()->findByHashSlug($request->hashslug);
        $addenda = $acq->addenda->first();
        $perkara = $acq->addenda->first()->items;

        $data = [
            'acq'     => $acq,
            'addenda' => $addenda,
            'perkara' => $perkara,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Adenda-Lampiran',
            'reportViewPath' => 'contract.pre.document.addenda.report.addenda_attach',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $config, $data);
    }
}
