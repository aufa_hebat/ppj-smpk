<?php

namespace App\Http\Controllers\Report\Acquisition;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class BillOfQuantityController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acq       = Acquisition::withBillOfQuantity()->withDetails()->findByHashSlug($request->hashslug);
        $bq_elemen = $acq->bqElements;
        $bq_item   = $acq->bqItems;
        $bq_sub    = $acq->bqSubItems;

        $data = [
            'acq'       => $acq,
            'bq_elemen' => $bq_elemen,
            'bq_item'   => $bq_item,
            'bq_sub'    => $bq_sub,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Senarai-Kuantiti',
            'reportViewPath' => 'contract.pre.document.report.acq_bq',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $config, $data);
    }
}
