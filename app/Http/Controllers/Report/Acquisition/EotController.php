<?php

namespace App\Http\Controllers\Report\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\EotApprove;
use Illuminate\Http\Request;

class EotController extends Controller
{
    public function __invoke(Request $request)
    {
        $eot_approve = EotApprove::findByHashSlug($request->hashslug);

        $data = [
            'eot_approve' => $eot_approve,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Senarai-Kehadiran-Taklimat',
            'reportViewPath' => 'contract.post.extension.report.extension',
            'acquisition'    => $eot_approve->eot->appointed_company->acquisition,
        ];

        return $this->generate($request, $config, $data);
    }
}
