<?php

namespace App\Http\Controllers\Report\Termination;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Cmgd;
use App\Models\Acquisition\Cnc;
use App\Models\Acquisition\Cpc;
use App\Models\Acquisition\Deposit;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\SubContract;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;

class SofaController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst       = Sst::findByHashSlug($request->get('hashslug'));
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();
        $deposit = Deposit::where('sst_id', $sst->id)->first();

        // $eots    =   Eot::where('sst_id', $sst->id)->get();
        $totalYear  = 0;
        $totalMonth = 0;
        $totalWeek  = 0;
        $totalDay   = 0;

        $totalDay = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
            return $query->where('value', '==', 1);
        })->count();

        $totalWeek = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
            return $query->where('value', '==', 2);
        })->count();

        $totalMonth = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
            return $query->where('value', '==', 3);
        })->count();

        $totalYear = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
            return $query->where('value', '==', 4);
        })->count();

        $cpc  = Cpc::where('sst_id', $sst->id)->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();
        $cnc  = Cnc::where('sst_id', $sst->id)->first();

        $vo         = 0;//VariationOrder::where('sst_id', $sst->id)->get();
        $vo_element = 0;//VariationOrderElement::where('sst_id', $sst->id)->get();
        $vo_type    = 0;//VariationOrderType::get();

        $ipc                = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->get();
        $prev_total_invoice = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('demand_amount')->sum();
        $last_ipc           = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->orderBy('ipc_no', 'desc')->first();
        $total_lad          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('compensation_amount')->sum();
        $wjp_lepas          = Ipc::where('sst_id', $sst->id)->where('last_ipc', 'y')->where('deleted_at', null)->first() ?? null;

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();
        

        $total_amount   = 0.0;
        $total_invoice  = 0.0;
        $total_balanced = 0.0;

        if ($sub_contract->count() > 0) {
            foreach ($sub_contract as $index => $row) {
                $total_amount   = $total_amount   + $row->amount;
                $total_invoice  = $total_invoice  + $row->invoices->pluck('invoice')->sum();
                $total_balanced = $total_balanced + ($row->amount - $row->invoices->pluck('invoice')->sum());
            }
        }

        $data = [
            'sst'                => $sst,
            'appointed'          => $appointed,
            'deposit'            => $deposit,
            'cpc'                => $cpc,
            'cmgd'               => $cmgd,
            'cnc'                => $cnc,
            'vo'                 => $vo,
            'vo_element'         => $vo_element,
            'vo_type'            => $vo_type,
            'ipc'                => $ipc,
            'prev_total_invoice' => $prev_total_invoice,
            'last_ipc'           => $last_ipc,
            'wjp_lepas'          => $wjp_lepas,
            'sub_contract'       => $sub_contract,
            'total_balanced'     => $total_balanced,
            'total_lad'          => $total_lad,
            // 'eot_aprv'           => $eotAprv,
            'totalDay'   => $totalDay,
            'totalWeek'  => $totalWeek,
            'totalMonth' => $totalMonth,
            'totalYear'  => $totalYear,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SofaReport',
            'reportViewPath' => 'contract.post.sofa.reports.sofaRpt',
            'sst'            => $sst,
        ];

        return $this->generate($request, $config, $data);
    }
}