<?php

namespace App\Http\Controllers\Report\Acquisition;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class NoticeController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acq = Acquisition::findByHashSlug($request->hashslug);

        $data = [
            'acq'     => $acq,
            'np_dept' => $request->np_dept,
        ];

        if ('1' == $acq->acquisition_category_id || '2' == $acq->acquisition_category_id) {
            $report_name = 'contract.pre.document.report.acq_notice';
        } elseif ('3' == $acq->acquisition_category_id || '4' == $acq->acquisition_category_id) {
            $report_name = 'contract.pre.document.report.acq_notice_tender';
        }

        // $report_name = 'contract.pre.document.report.acq_notice_tender';

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Notis',
            'reportViewPath' => $report_name,
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $config, $data);
    }
}
