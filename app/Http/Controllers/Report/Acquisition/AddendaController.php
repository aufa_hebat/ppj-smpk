<?php

namespace App\Http\Controllers\Report\Acquisition;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Acquisition;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;

class AddendaController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acq      = Acquisition::withAddendums()->withDetails()->findByHashSlug($request->hashslug);
        $addenda  = $acq->addenda->first();
        $perkara  = $acq->addenda->first()->items;
        $position = Position::where('name', 'Naib Presiden')->first()                                                         ?? 0;
        $np_dept  = User::where('department_id', $acq->approval->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'acq'     => $acq,
            'addenda' => $addenda,
            'perkara' => $perkara,
            'np_dept' => $np_dept,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Adenda',
            'reportViewPath' => 'contract.pre.document.addenda.report.addenda',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $config, $data);
    }
}
