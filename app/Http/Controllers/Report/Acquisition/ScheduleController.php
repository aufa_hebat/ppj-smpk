<?php

namespace App\Http\Controllers\Report\Acquisition;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class ScheduleController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acquisition = Acquisition::findByHashSlug($request->hashslug);

        $data = [
            'acquisition' => $acquisition,
            'cidb'        => $this->getKelayakanCIDB($acquisition->approval->id),
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Jadual-Harga',
            'reportViewPath' => 'contract.pre.box.report.jadual',
            'acquisition'    => $acquisition,
        ];

        return $this->generate($request, $config, $data);
    }
}
