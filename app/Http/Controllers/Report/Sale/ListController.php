<?php

namespace App\Http\Controllers\Report\Sale;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class ListController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acq = Acquisition::with('sales')->findByHashSlug($request->hashslug);

        $data = [
            'acq'        => $acq,
            'sales_list' => $acq->sales,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Senarai-Jualan',
            'reportViewPath' => 'sales.report.sale_list',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
