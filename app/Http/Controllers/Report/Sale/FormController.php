<?php

namespace App\Http\Controllers\Report\Sale;

use App\Http\Controllers\Report\BaseController;
use App\Models\Sale;
use Illuminate\Http\Request;

class FormController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sales = Sale::withDetails()->findByHashSlug($request->hashslug);

        $data = [
            'sales' => $sales,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Borang-Jualan',
            'reportViewPath' => 'sales.report.sale_form',
            'acquisition'    => $sales->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
