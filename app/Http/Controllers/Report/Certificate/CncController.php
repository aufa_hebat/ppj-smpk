<?php

namespace App\Http\Controllers\Report\Certificate;

use App\Http\Controllers\Report\BaseController;
use Illuminate\Http\Request;

class CncController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst = Sst::withCertificates()->findByHashSlug($request->hashslug);
        $cnc = $sst->cnc;

        $data = [
            'sst' => $sst,
            'cnc' => $cnc,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CNC',
            'reportViewPath' => 'contract.post.cnc.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
