<?php

namespace App\Http\Controllers\Report\Certificate;

use App\Http\Controllers\Report\BaseController;
use Illuminate\Http\Request;

class CpcController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst = Sst::withCertificates()->findByHashSlug($request->hashslug);
        $cpc = $sst->cpc;

        $data = [
            'sst' => $sst,
            'cpc' => $cpc,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CPC',
            'reportViewPath' => 'contract.post.cpc.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
