<?php

namespace App\Http\Controllers\Report\Certificate;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;

class CpoController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst = Sst::withCertificates()->findByHashSlug($request->hashslug);
        $cpo = $sst->cpo;

        $data = [
            'sst'  => $sst,
            'cpo'  => $cpo,
            'cidb' => common()->getKelayakanCIDB($sst->acquisition->approval->id),
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CPO',
            'reportViewPath' => 'contract.post.cpo.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
