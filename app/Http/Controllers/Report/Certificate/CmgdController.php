<?php

namespace App\Http\Controllers\Report\Certificate;

use App\Http\Controllers\Report\BaseController;
use Illuminate\Http\Request;

class CmgdController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst  = Sst::withCertificates()->findByHashSlug($request->hashslug);
        $cmgd = $sst->cmgd;

        $data = [
            'sst'  => $sst,
            'cmgd' => $cmgd,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CMGD',
            'reportViewPath' => 'contract.post.cmgd.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $config, $data);
    }
}
