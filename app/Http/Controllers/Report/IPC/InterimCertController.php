<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcInvoice;
use Illuminate\Http\Request;

class InterimCertController extends BaseController
{
    public function __invoke(Request $request)
    {
        $ipc         = Ipc::findByHashSlug($request->get('hashslug'));
        $appointed   = AppointedCompany::where('acquisition_id', $ipc->sst->acquisition_id)->where('company_id', $ipc->sst->company_id)->where('deleted_at', null)->first();
        $ipc_all     = Ipc::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get();
        $ipc_pre     = (1 != $ipc->ipc_no || 0 != $ipc->ipc_no) ? Ipc::where('deleted_at', null)->where('ipc_no', ($ipc->ipc_no - 1))->first() : null;
        $invoice_all = IpcInvoice::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get() ?? null;
        $invoice     = IpcInvoice::where('ipc_id', $ipc->id)->where('deleted_at', null)->get()     ?? null;
        $invoice_pre = (null != $ipc_pre) ? IpcInvoice::where('ipc_id', $ipc_pre->id)->where('deleted_at', null)->get() : null;

        $data = [
            'appointed'   => $appointed,
            'ipc'         => $ipc,
            'ipc_all'     => $ipc_all,
            'ipc_pre'     => $ipc_pre,
            'invoice_all' => $invoice_all,
            'invoice'     => $invoice,
            'invoice_pre' => $invoice_pre,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.interim_cert',
            'sst'            => '',
        ];

        return $this->generate($request, $config, $data);
    }
}