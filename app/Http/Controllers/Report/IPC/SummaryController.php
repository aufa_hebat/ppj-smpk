<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;

class SummaryController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst     = Sst::withIpcDetails()->withBqDetails()->findByHashSlug($request->hashslug);
        $ipc     = $sst->ipc;
        $ipc_bq  = $sst->ipcBq;
        $element = $sst->bqElement;

        $data = [
            'sst'     => $sst,
            'ipc'     => $ipc,
            'ipc_bq'  => $ipc_bq,
            'element' => $element,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.ipc_summary',
            'sst'            => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
