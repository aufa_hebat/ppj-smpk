<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;

class FirstChecklistController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst = Sst::findByHashSlug($request->get('hashslug'));
        $ipc = Ipc::where('sst_id', $sst->id)->first();

        $data = [
            'sst' => $sst,
            'ipc' => $ipc,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Semakan IPC 1',
            'reportViewPath' => 'contract.post.ipc.report.ipc_first_checklist',
            'sst'            => '',
        ];

        return $this->generate($request, $config, $data);
    }
}