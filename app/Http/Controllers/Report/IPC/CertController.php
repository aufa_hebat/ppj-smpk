<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;

class CertController extends BaseController
{
    public function __invoke(Request $request)
    {
        $sst = Sst::withIpcDetails()->findByHashSlug($request->hashslug);

        $data = [
            'sst' => $sst,
            'ipc' => $sst->ipc,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.ipc_cert',
            'sst'            => '',
        ];

        return $this->generate($request, $config, $data);
    }
}
