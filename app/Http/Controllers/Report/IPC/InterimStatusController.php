<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Ipc;
use Illuminate\Http\Request;

class InterimStatusController extends BaseController
{
    public function __invoke(Request $request)
    {
        $ipc       = Ipc::findByHashSlug($request->get('hashslug'));
        $ipc_all   = Ipc::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get();
        $ipc_pre   = (1 != $ipc->ipc_no || 0 != $ipc->ipc_no) ? Ipc::where('ipc_no', ($ipc->ipc_no - 1))->where('deleted_at', null)->first() : null;
        $appointed = AppointedCompany::where('acquisition_id', $ipc->sst->acquisition_id)->where('company_id', $ipc->sst->company_id)->where('deleted_at', null)->first();

        $data = [
            'ipc'       => $ipc,
            'ipc_all'   => $ipc_all,
            'ipc_pre'   => $ipc_pre,
            'appointed' => $appointed,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.interim_status',
            'sst'            => '',
        ];

        return $this->generate($request, $config, $data);
    }
}