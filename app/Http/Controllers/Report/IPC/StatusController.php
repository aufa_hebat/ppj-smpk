<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use Illuminate\Http\Request;

class StatusController extends BaseController
{
    public function __invoke(Request $request)
    {
        $data = [
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.ipc_status',
            'acquisition'    => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
