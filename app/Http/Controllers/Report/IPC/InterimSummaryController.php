<?php

namespace App\Http\Controllers\Report\IPC;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcInvoice;
use Illuminate\Http\Request;

class InterimSummaryController extends BaseController
{
    public function __invoke(Request $request)
    {
        $ipc         = Ipc::findByHashSlug($request->get('hashslug'));
        $ipc_pre     = (1 != $ipc->ipc_no || 0 != $ipc->ipc_no) ? Ipc::where('deleted_at', null)->where('ipc_no', ($ipc->ipc_no - 1))->first() : null;
        $invoice_all = IpcInvoice::where('sst_id', $ipc->sst_id)->where('deleted_at', null)->get() ?? null;
        $invoice     = IpcInvoice::where('ipc_id', $ipc->id)->where('deleted_at', null)->get()     ?? null;
        $invoice_pre = (null != $ipc_pre) ? IpcInvoice::where('ipc_id', $ipc_pre->id)->where('deleted_at', null)->get() : null;
        $bq_all      = (null != $invoice_all) ? IpcBq::whereIn('ipc_invoice_id', $invoice_all->pluck('id'))->where('deleted_at', null)->get() : null;
        $bq          = (null != $invoice) ? IpcBq::whereIn('ipc_invoice_id', $invoice->pluck('id'))->where('deleted_at', null)->get() : null;
        $bq_pre      = (null != $invoice_pre) ? IpcBq::whereIn('ipc_invoice_id', $invoice_pre->pluck('id'))->where('deleted_at', null)->get() : null;
        $sub_all     = (null != $bq_all) ? SubAdjust::whereIn('id', $bq_all->pluck('bq_subitem_id'))->where('deleted_at', null)->get() : null;
        $elements    = (null != $sub_all) ? ElemenAdjust::where('sst_id', $ipc->sst_id)->whereIn('no', $sub_all->pluck('adjust_no_element'))->where('deleted_at', null)->get() : null;

        $data = [
            'ipc'         => $ipc,
            'invoice_all' => $invoice_all,
            'invoice'     => $invoice,
            'invoice_pre' => $invoice_pre,
            'bq_all'      => $bq_all,
            'bq'          => $bq,
            'bq_pre'      => $bq_pre,
            'elements'    => $elements,
            'sub_all'     => $sub_all,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.interim_summary',
            'sst'            => '',
        ];

        return $this->generate($request, $config, $data);
    }
}