<?php

namespace App\Http\Controllers\Report\Monitoring;

use App\Http\Controllers\Report\BaseController;
use Illuminate\Http\Request;

class SiteVisitPerHashslugController extends BaseController
{
    public function __invoke(Request $request)
    {
        
        //dd($request->get('hashslug'));
        $siteVisit = \App\Models\Acquisition\Monitoring\SiteVisit::findByHashSlug($request->get('hashslug'));
        
        $data = [
            'siteVisit'        => $siteVisit,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Lawatan Penilaian Tapak',
            'reportViewPath' => 'contract.post.monitoring.report.site_visit_per_hashslug',
            'siteVisit'    => $siteVisit,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
    
    
    
    /*
     * ORI
     * 
     * $acq = Acquisition::with('sales')->findByHashSlug($request->hashslug);

        $data = [
            'acq'        => $acq,
            'sales_list' => $acq->sales,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Folder-Jualan',
            'reportViewPath' => 'sales.report.sale_folder',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
     * 
     * 
     */
}
