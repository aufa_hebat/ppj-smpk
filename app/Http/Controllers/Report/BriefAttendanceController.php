<?php

namespace App\Http\Controllers\Report;

use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class BriefAttendanceController extends BaseController
{
    public function __invoke(Request $request)
    {
        $acquisition = Acquisition::findByHashSlug($request->hashslug);

        $data = [
            'acquisition' => $acquisition,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Senarai-Kehadiran-Taklimat',
            'reportViewPath' => 'contract.pre.briefing.report.attendance',
            'acquisition'    => $acquisition,
        ];

        return $this->generate($request, $config, $data);
    }
}
