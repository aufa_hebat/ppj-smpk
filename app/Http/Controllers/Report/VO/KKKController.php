<?php

namespace App\Http\Controllers\Report\VO;

use App\Http\Controllers\Report\BaseController;
use App\Models\VO\VO;
use Illuminate\Http\Request;

class KKKController extends BaseController
{
    public function __invoke(Request $request)
    {
        $vo = VO::withDetails()->findByHashSlug($request->hashslug);

        $data = [
            'vo' => $vo,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Kesilapan Keterangan Kerja & Kuantiti',
            'reportViewPath' => 'contract.post.variation-order.report.kkk',
            'acquisition'    => $vo->sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }
}
