<?php

namespace App\Http\Controllers\Report\Termination;

use App\Http\Controllers\Report\BaseController;
use App\Models\Acquisition\tERMINATION;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;

class TerminateNoticeController extends BaseController
{
    public function __invoke(Request $request)
    {
        $termination = Termination::findByHashSlug($request->get('hashslug'));
        $appointed   = AppointedCompany::where('acquisition_id', $termination->sst->acquisition->id)->first();
        $position    = Position::where('name', 'Naib Presiden')->first();
        $department  = Department::where('code', 'JU')->first();
        $np_dept     = User::where('department_id', $department->id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'termination' => $termination,
            'appointed'   => $appointed,
            'np_dept'     => $np_dept,
        ];

        $config = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'TerminationLetter',
            'reportViewPath' => 'contract.post.termination.report.terminate_notice',
            'termination'    => $termination,
        ];

        return $this->generate($request, $config, $data);
    }
}