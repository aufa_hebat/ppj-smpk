<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\AppointedCompany;
use App\Models\DocumentAcceptance;

class DocumentAcceptanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.document-acceptance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function shows($id)
    {
        $document           = DocumentAcceptance::findByHashSlug($id);
        $sst                = Sst::where('id',$document->sst_id)->first();
        $ipc                = Ipc::where('sst_id', $sst->id)->get();
        $appointed          = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        return view('manage.document-acceptance.shows', compact('document', 'sst', 'ipc', 'appointed'));
    }

    public function edits($id)
    {
        $document           = DocumentAcceptance::findByHashSlug($id);
        $sst                = Sst::where('id',$document->sst_id)->first();
        $ipc                = Ipc::where('sst_id', $sst->id)->get();
        $appointed          = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();

        return view('manage.document-acceptance.edits', compact('document', 'sst', 'ipc', 'appointed'));
    }

    public function list($id)
    {
        $sst                 = Sst::findByHashSlug($id);
        $ipc = Ipc::where('sst_id', $sst->id)->get();
        $appointed           = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->where('company_id', $sst->company_id)->where('deleted_at', null)->first();
        $hashslug = $sst->hashslug;

        return view('manage.document-acceptance.list', compact('sst', 'ipc', 'appointed','hashslug'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
