<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\Company\Owner;
use App\Models\StandardCode;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $hashslug     = $request->hashslug;
        $owner        = Company::findByHashSlug($hashslug);
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();

        return view('manage.owner.index', compact('hashslug', 'owner', 'std_cd_state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'ic_number' => 'required',
            // 'email'   => 'required|email',
            'primary' => 'required',
            // 'secondary' => 'required',
            'postcode' => 'required',
            'state' => 'required',
        ]);

        $owner = Owner::create($request->only('company_id', 'name', 'ic_number', 'email', 'cidb_cert', 'mof_cert', 'kdn_cert'));

        $owner->addresses()->create([
            'primary'   => str_replace("'", '`', $request->primary),
            'secondary' => str_replace("'", '`', $request->secondary),
            'postcode'  => $request->input('postcode'),
            'state'     => $request->input('state'),
        ]);

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                $owner->phones()->create(['phone_type_id' => $key, 'phone_number' => $pn]);
            }
        }

        audit($owner, __('Pemilik ' . $owner->company->company_name . ' telah berjaya disimpan.'));

        swal()->success('Pemilik Syarikat', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('manage.companies.edit', $request->hashslug)->withInput(['tab'=>'owner']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $owner        = Owner::findByHashSlug($id);
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();

        return view('manage.owner.show', compact('owner', 'std_cd_state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $owner        = Owner::findByHashSlug($id);
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();

        return view('manage.owner.edit', compact('owner', 'std_cd_state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'name'         => 'required',
            'ic_number' => 'required',
            // 'email'   => 'required',
            'primary' => 'required',
            // 'secondary' => 'required',
            'postcode' => 'required',
            'state' => 'required',
        ]);
        $owner = Owner::find($id);
        
        if($owner){
            
            Owner::find($id)->update($request->only('name', 'ic_number', 'email', 'cidb_cert', 'mof_cert', 'kdn_cert'));
            
            $address = Owner::find($id)->addresses();
            if($address->count() > 0){
                Owner::find($id)->addresses()->update([
                    'primary'   => str_replace("'", '`', $request->primary),
                    'secondary' => str_replace("'", '`', $request->secondary),
                    'postcode'  => $request->input('postcode'),
                    'state'     => $request->input('state'),
                ]);
        
            }else{
                Owner::find($id)->addresses()->create([
                    'primary'   => str_replace("'", '`', $request->primary),
                    'secondary' => str_replace("'", '`', $request->secondary),
                    'postcode'  => $request->input('postcode'),
                    'state'     => $request->input('state'),
                ]);               
            }
            
            foreach ($request->only('phone_number') as  $phone_numbers) {
                foreach ($phone_numbers as $key => $pn) {
                    Owner::find($id)->phones()->updateOrCreate(['phone_type_id' => $key, 'phone_number' => $pn]);
                }
            }
    
            audit($owner, __('Pemilik ' . $owner->company->company_name . ' telah berjaya dikemaskini.'));            

        }


        swal()->success('Pemilik Syarikat', 'Rekod telah berjaya dikemaskini.', []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $owner = Owner::hashslug($id);

        // audit($owner, __('Pemilik ' . $owner->company->company_name . ' telah berjaya dihapuskan.'));

        $owner->delete();
       
        if($request->ajax()){

            return response()->json(['message'=>'Rekod telah berjaya dihapuskan.']);
        }

    }
}
