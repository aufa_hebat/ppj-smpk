<?php

namespace App\Http\Controllers\Manage\Committee;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Approval;
use App\Models\Committee\Assessment;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.committee.assessment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $JabatanPelaksana = Department::get();
        $listJawatan      = Position::get();
        $kelulusan        = standard_code('Kod_KuasaMelulus');

        return view('manage.committee.assessment.create', compact('JabatanPelaksana', 'listJawatan', 'kelulusan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assessment = new Assessment();

        $assessment->acquisition_id   = $request->input('acquisition_id');
        $assessment->appointment_date = $request->input('appointment_date') ? Carbon::createFromFormat('d/m/Y', $request->input('appointment_date')) : null;
        $assessment->approval         = $request->input('approval');
        $assessment->category         = $request->input('category');

        if (1 == ($request->category)) {
            $assessment->name          = $request->input('name1');
            $assessment->department_id = $request->input('department_id1');
            $assessment->position_id   = $request->input('position_id1');
            $assessment->role          = $request->input('role1');
        } elseif (2 == ($request->category)) {
            $assessment->name          = $request->input('name');
            $assessment->department_id = $request->input('department_id');
            $assessment->position_id   = $request->input('position_id');
            $assessment->role          = $request->input('role');
        }

        $assessment->save();

        audit($assessment, __('Ahli Jawatan Kuasa Penilai telah berjaya disimpan bagi ' . $assessment->acquisition->reference . ':' . $assessment->acquisition->title . '.'));

        swal()->success('Ahli Jawatan Kuasa Penilai', 'Rekod telah berjaya disimpan.', []);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approval         = Acquisition::withDetails()->findByHashSlug($id);
        $user             = User::where('name','!=','Administrator')->where('name','!=','Developer')->where('name','!=','Pengesah')->where('name','!=','Penyedia')->where('name','!=','Penyemak')->where('name','!=','Urusetia')->where('name','!=','User')->orderBy('name','asc')->get();
        $assessment       = Assessment::where('acquisition_id', $approval->id)->get();
        $JabatanPelaksana = Department::get();
        $listJawatan      = Position::get();
        $kelulusan        = standard_code('Kod_KuasaMelulus')->where('remark', 'kelulusan');

        return view('manage.committee.assessment.show', compact('approval', 'JabatanPelaksana', 'listJawatan', 'kelulusan', 'assessment', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assessment       = Assessment::find($id);
        $user             = User::get();
        $JabatanPelaksana = Department::get();
        $listJawatan      = Position::get();
        $kelulusan        = standard_code('Kod_KuasaMelulus')->where('remark', 'kelulusan');

        return view('manage.committee.assessment.edit', compact('JabatanPelaksana', 'listJawatan', 'kelulusan', 'assessment', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assessment = Assessment::find($id);

        $assessment->acquisition_id   = $request->input('acquisition_id');
        $assessment->appointment_date = $request->input('appointment_date') ? Carbon::createFromFormat('d/m/Y', $request->input('appointment_date')) : null;
        $assessment->approval         = $request->input('approval');
        $assessment->category         = $request->input('category');

        if (1 == ($request->category)) {
            $assessment->name          = $request->input('name1');
            $assessment->department_id = $request->input('department_id1');
            $assessment->position_id   = $request->input('position_id1');
            $assessment->role          = $request->input('role1');
        } elseif (2 == ($request->category)) {
            $assessment->name          = $request->input('name');
            $assessment->department_id = $request->input('department_id');
            $assessment->position_id   = $request->input('position_id');
            $assessment->role          = $request->input('role');
        }

        $assessment->update();

        audit($assessment, __('Ahli Jawatan Kuasa Penilai telah berjaya dikemaskini bagi ' . $assessment->acquisition->reference . ':' . $assessment->acquisition->title . '.'));

        swal()->success('Ahli Jawatan Kuasa Penilai', 'Rekod telah berjaya dikemaskini.', []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assessment = Assessment::find($id);
        $assessment->delete();

        audit($assessment, __('Ahli Jawatan Kuasa Penilai telah berjaya dihapuskan bagi ' . $assessment->acquisition->reference . ':' . $assessment->acquisition->title . '.'));

        swal()->success('Ahli Jawatan Kuasa Penilai', 'Rekod telah berjaya dihapuskan.', []);

        return back();
    }
}
