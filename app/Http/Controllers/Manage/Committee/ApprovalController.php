<?php

namespace App\Http\Controllers\Manage\Committee;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Committee\Approval;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.committee.approval.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'appointment_date' => 'required',
        ]);

        $tempID   = null;
        $tempIDP  = null;
        $ida      = [];
        $idaS     = [];
        $hashslug = $request->hashslug;

        if (! empty($request->baru)) {
            //check delete dolu
            if (! empty($request->exist)) {
                foreach ($request->exist as $ex) {
                    $checkJKPembuka = Approval::where('acquisition_id', $request->acqID)->get();
                    if (! empty($checkJKPembuka)) {
                        foreach ($checkJKPembuka as $a) {
                            $ida[] = $a['id'];
                        }
                        $idaS[] = $ex['id'];
                    }
                }
                $diffA = array_diff($ida, $idaS);

                foreach ($diffA as $ad) {
                    Approval::destroy($ad);
                }
            } else {
                $checkJKPembuka = Approval::where('acquisition_id', $request->acqID)->get();
                Approval::whereIn('acquisition_id', $checkJKPembuka->pluck('acquisition_id'))->delete();
            }

            foreach ($request->baru as $baru) {
                if (! empty($baru['name'])) {
                    $JKPembuka                   = new Approval();
                    $JKPembuka->acquisition_id   = $request->acqID;
                    $JKPembuka->appointment_date = $request->input('appointment_date') ? Carbon::createFromFormat('d/m/Y', $request->input('appointment_date')) : null;
                    $JKPembuka->name             = $baru['name'];

                    $user = User::where('name', $baru['name'])->first();
                    if (! empty($user)) {
                        $JKPembuka->position_id   = $user->position_id;
                        $JKPembuka->department_id = $user->department_id;
                    }
                    $JKPembuka->save();

                    audit($JKPembuka, __('Ahli Jawatan Kuasa Pembuka telah berjaya disimpan bagi ' . $JKPembuka->acquisition->reference . ':' . $JKPembuka->acquisition->title . '.'));
                }
            }
        } else {
            $checkJKPembuka = Approval::where('acquisition_id', $request->acqID)->get();
            Approval::whereIn('acquisition_id', $checkJKPembuka->pluck('acquisition_id'))->delete();
        }

        swal()->success('Ahli Jawatan Kuasa Pembuka', 'Rekod telah berjaya disimpan.', []);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approval         = Acquisition::withDetails()->findByHashSlug($id);
        //$user             = User::where('department_id', '9')->orderBy('name')->get();
        $user             = User::where('department_id', '9')
                            ->whereHas('grade', function ($query) {
                                $query->where('gred_int', '>', 28);
                            })
                            ->orderBy('name')->get();
        $officer          = Approval::where('acquisition_id', $approval->id)->get();
        $officer1         = Approval::where('acquisition_id', $approval->id)->orderby('id', 'desc')->first();
        $JabatanPelaksana = Department::get();
        $listJawatan      = Position::get();
        $kelulusan        = standard_code('Kod_KuasaMelulus')->where('remark', 'kelulusan');

        return view('manage.committee.approval.show', compact('approval', 'JabatanPelaksana', 'listJawatan', 'kelulusan', 'officer', 'officer1', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $officer          = Approval::find($id);
        $user             = User::orderBy('name')->get();
        $JabatanPelaksana = Department::get();
        $listJawatan      = Position::get();
        $kelulusan        = standard_code('Kod_KuasaMelulus')->where('remark', 'kelulusan');

        return view('manage.committee.approval.edit', compact('JabatanPelaksana', 'listJawatan', 'kelulusan', 'officer', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $officer = Approval::find($id);

        $officer->acquisition_id   = $request->input('acquisition_id');
        $officer->appointment_date = $request->input('appointment_date') ? Carbon::createFromFormat('d/m/Y', $request->input('appointment_date')) : null;
        $officer->approval         = $request->input('approval');
        $officer->name             = $request->input('name');
        $officer->department_id    = $request->input('department_id');
        $officer->position_id      = $request->input('position_id');
        $officer->role             = $request->input('role');

        $officer->update();

        audit($officer, __('Ahli Jawatan Kuasa Pembuka telah berjaya dikemaskini bagi ' . $officer->acquisition->reference . ':' . $officer->acquisition->title . '.'));

        swal()->success('Ahli Jawatan Kuasa Pembuka', 'Rekod telah berjaya dikemaskini.', []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $officer = Approval::find($id);
        $officer->delete();

        audit($officer, __('Ahli Jawatan Kuasa Pembuka telah berjaya dihapuskan bagi ' . $officer->acquisition->reference . ':' . $officer->acquisition->title . '.'));

        swal()->success('Ahli Jawatan Kuasa Pembuka', 'Rekod telah berjaya dihapuskan.', []);

        return back();
    }
}
