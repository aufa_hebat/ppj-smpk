<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Company\BPKU;
use App\Models\Company\Bumiputra;
use App\Models\Company\CIDB;
use App\Models\Company\CIDBCategory;
use App\Models\Company\CIDBGrade;
use App\Models\Company\CIDBKhusus;
use App\Models\Company\Company;
use App\Models\Company\KDN;
use App\Models\Company\MOF;
use App\Models\Company\MOFBidang;
use App\Models\Company\MOFKhusus;
use App\Models\Company\SPKK;
use App\Models\Company\SSM;
use App\Models\Document;
use App\Models\StandardCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Redirect;
use storage;
use Validator;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $hashslug = $request->hashslug;
        $company  = Company::findByHashSlug($hashslug);

        return view('manage.certificate.index', compact('hashslug', 'company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company       = Company::findByHashSlug($id);
        $ssm1          = SSM::findByHashSlug($id);
        $cidb1         = CIDB::findByHashSlug($id);
        $bpku1         = BPKU::findByHashSlug($id);
        $spkk1         = SPKK::findByHashSlug($id);
        $mof1          = MOF::findByHashSlug($id);
        $bumiputra1    = Bumiputra::findByHashSlug($id);
        $kdn1          = KDN::findByHashSlug($id);
        $std_cd_states = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $ssm_doc       = Document::where('document_id', $company->id)->where('document_types', 'SSM')->orderby('id', 'desc')->limit(1)->get();
        $cidb_doc      = Document::where('document_id', $company->id)->where('document_types', 'CIDB')->orderby('id', 'desc')->limit(1)->get();
        $bpku_doc      = Document::where('document_id', $company->id)->where('document_types', 'BPKU')->orderby('id', 'desc')->limit(1)->get();
        $spkk_doc      = Document::where('document_id', $company->id)->where('document_types', 'SPKK')->orderby('id', 'desc')->limit(1)->get();
        $mof_doc       = Document::where('document_id', $company->id)->where('document_types', 'MOF')->orderby('id', 'desc')->limit(1)->get();
        $bumi_doc      = Document::where('document_id', $company->id)->where('document_types', 'Bumiputra')->orderby('id', 'desc')->limit(1)->get();
        $kdn_doc       = Document::where('document_id', $company->id)->where('document_types', 'KDN')->orderby('id', 'desc')->limit(1)->get();
        $gred          = cidb_codes('grade');
        $kategori      = cidb_codes('category');
        $pengkhususan  = cidb_codes('khusus');
        $db1           = standard_code('MOF_Bidang');
        $db2           = standard_code('MOF_Khusus_10000');
        $db3           = standard_code('MOF_Khusus_20000');
        $db4           = standard_code('MOF_Khusus_30000');
        $db5           = standard_code('MOF_Khusus_40000');
        $db6           = standard_code('MOF_Khusus_50000');
        $db7           = standard_code('MOF_Khusus_60000');
        $db8           = standard_code('MOF_Khusus_70000');
        $db9           = standard_code('MOF_Khusus_80000');
        $db10          = standard_code('MOF_Khusus_90000');
        $db11          = standard_code('MOF_Khusus_100000');
        $db12          = standard_code('MOF_Khusus_210000');
        $db13          = standard_code('MOF_Khusus_220000');
        $kategori_mof       = mof_codes('category');
        $mof_khusus    = mof_codes('khusus');

        return view('manage.certificate.edit', compact('company', 'std_cd_state', 'ssm1', 'cidb1', 'bpku1', 'spkk1', 'mof1', 'bumiputra1', 'kdn1', 'ssm_doc', 'cidb_doc', 'bpku_doc', 'spkk_doc', 'mof_doc', 'bumi_doc', 'kdn_doc', 'gred', 'kategori', 'pengkhususan', 'db1', 'db2', 'db3', 'db4', 'db5', 'db6', 'db7', 'db8', 'db9', 'db10', 'db11', 'db12', 'db13', 'kategori_mof', 'mof_khusus', 'std_cd_states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ssm_no'         => 'required|max:20',
            'ssm_start_date' => 'required',
            // 'ssm_end_date'   => 'required',
            'ssm_primary' => 'required',
            'ssm_secondary' => 'required',
            'ssm_postcode' => 'required|max:5',
            'ssm_state' => 'required',
            // 'ssm_cert' => 'required',
            'ssm' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
        ]);
        if(!empty($request->input('cidb_no'))){
            $this->validate($request, [
                'cidb_no'         => 'required|max:20',
                'cidb_start_date' => 'required',
                'cidb_end_date'   => 'required',
                'cidb_primary' => 'required',
                'cidb_secondary' => 'required',
                'cidb_postcode' => 'required|max:5',
                'cidb_state' => 'required',
                // 'cidb_cert' => 'required',
                'cidb' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
            ]);
        }
        if(!empty($request->input('bpku_no'))){
            $this->validate($request, [
                'bpku_no'         => 'required|max:20',
                'bpku_start_date' => 'required',
                'bpku_end_date'   => 'required',
                // 'bpku_cert' => 'required',
                'bpku' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
            ]);
        }
        if(!empty($request->input('spkk_no'))){
            $this->validate($request, [
                'spkk_no'         => 'required|max:20',
                'spkk_start_date' => 'required',
                'spkk_end_date'   => 'required',
                // 'spkk_cert' => 'required',
                'spkk' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
            ]);
        }
        if(!empty($request->input('mof_no'))){
            $this->validate($request, [
                'mof_no'         => 'required|max:20',
                'mof_start_date' => 'required',
                'mof_end_date'   => 'required',
                'mof_primary' => 'required',
                'mof_secondary' => 'required',
                'mof_postcode' => 'required|max:5',
                'mof_state' => 'required',
                // 'mof_cert' => 'required',
                'mof' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
            ]);
        }
        if(!empty($request->input('bumiputra_no'))){
            $this->validate($request, [
                'bumiputra_no'         => 'required|max:20',
                'bumiputra_start_date' => 'required',
                'bumiputra_end_date'   => 'required',
                // 'bumiputra_cert' => 'required',
                'bumiputra' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
            ]);
        }
        if(!empty($request->input('kdn_no'))){
            $this->validate($request, [
                'kdn_no'         => 'required|max:20',
                'kdn_start_date' => 'required',
                'kdn_end_date'   => 'required',
                'kdn_primary' => 'required',
                'kdn_secondary' => 'required',
                'kdn_postcode' => 'required|max:5',
                'kdn_state' => 'required',
                // 'kdn_cert' => 'required',
                'license_no'         => 'required',
                'kdn' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
            ]);
        }

        $company   = Company::findByHashSlug($id);
        $ssm       = SSM::findByHashSlug($id);
        $cidb      = CIDB::findByHashSlug($id);
        $bpku      = BPKU::findByHashSlug($id);
        $spkk      = SPKK::findByHashSlug($id);
        $mof       = MOF::findByHashSlug($id);
        $bumiputra = Bumiputra::findByHashSlug($id);
        $kdn       = KDN::findByHashSlug($id);

        // ------------------- UPLOAD SSM -------------------

        if(!empty($request->ssm_start_date)){
            if(empty($ssm->ssm_start_date)){
                audit($ssm, __('Maklumat sijil SSM telah berjaya disimpan.'));
            } else {
                if($ssm->ssm_start_date == $request->ssm_start_date){
                    audit($ssm, __('Maklumat sijil SSM telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->ssm)) {
            $ssm_files = Input::file('ssm');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $ssm_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'company'; // upload folder in public directory
                $NamaDokumen     = strtoupper($request->ssm_no) . '_ssm_' . str_replace("&", '', $ssm_files->getClientOriginalName());
                $upload_success  = $ssm_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $ssm_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $ssm->id;
                $entry->document_types = 'SSM';
                $entry->mime           = $ssm_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $ssm_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }

        $ssm_start_date = $request->input('ssm_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('ssm_start_date')) : null;
        $ssm_end_date   = $request->input('ssm_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('ssm_end_date')) : null;
        Company::find($company->id)->update(
            [
                'ssm_no'         => strtoupper($request->input('ssm_no')),
                'ssm_start_date' => $ssm_start_date,
                'ssm_end_date'   => $ssm_end_date,
            ]
        );

        $ssm->update();

        $ssm->addresses()->updateOrCreate(['addressable_type' => 'App\Models\Company\SSM', 'addressable_id' => $company->id],
        [
            'primary'   => str_replace("'", '`', $request->ssm_primary),
            'secondary' => str_replace("'", '`', $request->ssm_secondary),
            'postcode'  => $request->input('ssm_postcode'),
            'state'     => $request->input('ssm_state'),
        ]);

        // ------------------- UPLOAD CIDB -------------------

        if(!empty($request->cidb_no)){
            if(empty($cidb->cidb_no)){
                audit($cidb, __('Maklumat sijil CIDB telah berjaya disimpan.'));
            } else {
                if($cidb->cidb_no == $request->cidb_no){
                    audit($cidb, __('Maklumat sijil CIDB telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->cidb)) {
            $cidb_files    = Input::file('cidb');
            $rulescidb     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validatorcidb = Validator::make(['file' => $cidb_files], $rulescidb);
            if ($validatorcidb->passes()) {
                $destinationPathCIDB = 'company'; // upload folder in public directory
                $NamaDokumenCIDB     = strtoupper($request->ssm_no) . '_cidb_' . str_replace("&", '', $cidb_files->getClientOriginalName());
                $upload_success_cidb = $cidb_files->move($destinationPathCIDB, $NamaDokumenCIDB);

                // save into database
                $extension_cidb             = $cidb_files->getClientOriginalExtension();
                $entry_cidb                 = new Document();
                $entry_cidb->document_id    = $cidb->id;
                $entry_cidb->document_types = 'CIDB';
                $entry_cidb->mime           = $cidb_files->getClientMimeType();
                $entry_cidb->document_name  = $NamaDokumenCIDB;
                $entry_cidb->document_path  = $cidb_files->getFilename() . '.' . $extension_cidb;
                $entry_cidb->save();
            }
        }

        $cidb_start_date           = $request->input('cidb_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cidb_start_date')) : null;
        $cidb_end_date             = $request->input('cidb_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cidb_end_date')) : null;
        $cidb_blacklist            = isset($request->cidb_blacklist) ? true : false;
        $cidb_blacklist_start_date = isset($request->cidb_blacklist) ? new Carbon() : null;
        $cidb_blacklist_end_date   = $request->input('cidb_blacklist_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cidb_blacklist_end_date')) : null;
        Company::find($company->id)->update(
            [
                'cidb_no'                   => strtoupper($request->input('cidb_no')),
                'cidb_start_date'           => $cidb_start_date,
                'cidb_end_date'             => $cidb_end_date,
                'cidb_blacklist'            => $cidb_blacklist,
                'cidb_blacklist_start_date' => $cidb_blacklist_start_date,
                'cidb_blacklist_end_date'   => $cidb_blacklist_end_date,
            ]
        );

        $cidb->update();

        $this->StoreCIDB($request, $cidb->id, $company);

        $cidb->addresses()->updateOrCreate(['addressable_type' => 'App\Models\Company\CIDB', 'addressable_id' => $company->id],
        [
            'primary'   => str_replace("'", '`', $request->cidb_primary),
            'secondary' => str_replace("'", '`', $request->cidb_secondary),
            'postcode'  => $request->input('cidb_postcode'),
            'state'     => $request->input('cidb_state'),
        ]);

        if(!empty($request->bpku_no)){
            if(empty($bpku->bpku_no)){
                audit($bpku, __('Maklumat sijil BPKU telah berjaya disimpan.'));
            } else {
                if($bpku->bpku_no == $request->bpku_no){
                    audit($bpku, __('Maklumat sijil BPKU telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->bpku)) {
            $bpku_files    = Input::file('bpku');
            $rulesbpku     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validatorbpku = Validator::make(['file' => $bpku_files], $rulesbpku);
            if ($validatorbpku->passes()) {
                $destinationPathBPKU = 'company'; // upload folder in public directory
                $NamaDokumenBPKU     = strtoupper($request->ssm_no) . '_bpku_' . str_replace("&", '', $bpku_files->getClientOriginalName());
                $upload_success_bpku = $bpku_files->move($destinationPathBPKU, $NamaDokumenBPKU);

                // save into database
                $extension_bpku             = $bpku_files->getClientOriginalExtension();
                $entry_bpku                 = new Document();
                $entry_bpku->document_id    = $bpku->id;
                $entry_bpku->document_types = 'BPKU';
                $entry_bpku->mime           = $bpku_files->getClientMimeType();
                $entry_bpku->document_name  = $NamaDokumenBPKU;
                $entry_bpku->document_path  = $bpku_files->getFilename() . '.' . $extension_bpku;
                $entry_bpku->save();
            }
        }

        $bpku_start_date           = $request->input('bpku_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bpku_start_date')) : null;
        $bpku_end_date             = $request->input('bpku_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bpku_end_date')) : null;
        $bpku_blacklist            = isset($request->bpku_blacklist) ? true : false;
        $bpku_blacklist_start_date = isset($request->bpku_blacklist) ? new Carbon() : null;
        $bpku_blacklist_end_date   = $request->input('bpku_blacklist_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bpku_blacklist_end_date')) : null;
        Company::find($company->id)->update(
            [
                'bpku_no'                   => strtoupper($request->input('bpku_no')),
                'bpku_start_date'           => $bpku_start_date,
                'bpku_end_date'             => $bpku_end_date,
                'bpku_blacklist'            => $bpku_blacklist,
                'bpku_blacklist_start_date' => $bpku_blacklist_start_date,
                'bpku_blacklist_end_date'   => $bpku_blacklist_end_date,
            ]
        );

        $bpku->update();

        if(!empty($request->spkk_no)){
            if(empty($spkk->spkk_no)){
                audit($spkk, __('Maklumat sijil SPKK telah berjaya disimpan.'));
            } else {
                if($spkk->spkk_no == $request->spkk_no){
                    audit($spkk, __('Maklumat sijil SPKK telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->spkk)) {
            $spkk_files    = Input::file('spkk');
            $rulesspkk     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validatorspkk = Validator::make(['file' => $spkk_files], $rulesspkk);
            if ($validatorspkk->passes()) {
                $destinationPathSPKK = 'company'; // upload folder in public directory
                $NamaDokumenSPKK     = strtoupper($request->ssm_no) . '_spkk_' . str_replace("&", '', $spkk_files->getClientOriginalName());
                $upload_success_spkk = $spkk_files->move($destinationPathSPKK, $NamaDokumenSPKK);

                // save into database
                $extension_spkk             = $spkk_files->getClientOriginalExtension();
                $entry_spkk                 = new Document();
                $entry_spkk->document_id    = $spkk->id;
                $entry_spkk->document_types = 'SPKK';
                $entry_spkk->mime           = $spkk_files->getClientMimeType();
                $entry_spkk->document_name  = $NamaDokumenSPKK;
                $entry_spkk->document_path  = $spkk_files->getFilename() . '.' . $extension_spkk;
                $entry_spkk->save();
            }
        }

        $spkk_start_date           = $request->input('spkk_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('spkk_start_date')) : null;
        $spkk_end_date             = $request->input('spkk_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('spkk_end_date')) : null;
        $spkk_blacklist            = isset($request->spkk_blacklist) ? true : false;
        $spkk_blacklist_start_date = isset($request->spkk_blacklist) ? new Carbon() : null;
        $spkk_blacklist_end_date   = $request->input('spkk_blacklist_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('spkk_blacklist_end_date')) : null;
        Company::find($company->id)->update(
            [
                'spkk_no'                   => strtoupper($request->input('spkk_no')),
                'spkk_start_date'           => $spkk_start_date,
                'spkk_end_date'             => $spkk_end_date,
                'spkk_blacklist'            => $spkk_blacklist,
                'spkk_blacklist_start_date' => $spkk_blacklist_start_date,
                'spkk_blacklist_end_date'   => $spkk_blacklist_end_date,
            ]
        );

        $spkk->update();

        if(!empty($request->mof_no)){
            if(empty($mof->mof_no)){
                audit($mof, __('Maklumat sijil MOF telah berjaya disimpan.'));
            } else {
                if($mof->mof_no == $request->mof_no){
                    audit($mof, __('Maklumat sijil MOF telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->mof)) {
            $mof_files    = Input::file('mof');
            $rulesmof     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validatormof = Validator::make(['file' => $mof_files], $rulesmof);
            if ($validatormof->passes()) {
                $destinationPathMOF = 'company'; // upload folder in public directory
                $NamaDokumenMOF     = strtoupper($request->ssm_no) . '_mof_' . str_replace("&", '', $mof_files->getClientOriginalName());
                $upload_success_mof = $mof_files->move($destinationPathMOF, $NamaDokumenMOF);

                // save into database
                $extension_mof             = $mof_files->getClientOriginalExtension();
                $entry_mof                 = new Document();
                $entry_mof->document_id    = $mof->id;
                $entry_mof->document_types = 'MOF';
                $entry_mof->mime           = $mof_files->getClientMimeType();
                $entry_mof->document_name  = $NamaDokumenMOF;
                $entry_mof->document_path  = $mof_files->getFilename() . '.' . $extension_mof;
                $entry_mof->save();
            }
        }

        $mof_start_date           = $request->input('mof_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('mof_start_date')) : null;
        $mof_end_date             = $request->input('mof_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('mof_end_date')) : null;
        $mof_blacklist            = isset($request->mof_blacklist) ? true : false;
        $mof_blacklist_start_date = isset($request->mof_blacklist) ? new Carbon() : null;
        $mof_blacklist_end_date   = $request->input('mof_blacklist_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('mof_blacklist_end_date')) : null;
        Company::find($company->id)->update(
            [
                'mof_no'                   => strtoupper($request->input('mof_no')),
                'mof_start_date'           => $mof_start_date,
                'mof_end_date'             => $mof_end_date,
                'mof_blacklist'            => $mof_blacklist,
                'mof_blacklist_start_date' => $mof_blacklist_start_date,
                'mof_blacklist_end_date'   => $mof_blacklist_end_date,
            ]
        );

        $mof->update();

        $this->StoreMOF($request, $mof->id, $company);

        $mof->addresses()->updateOrCreate(['addressable_type' => 'App\Models\Company\MOF', 'addressable_id' => $company->id],
            [
                'primary'   => str_replace("'", '`', $request->mof_primary),
                'secondary' => str_replace("'", '`', $request->mof_secondary),
                'postcode'  => $request->input('mof_postcode'),
                'state'     => $request->input('mof_state'),
            ]
        );

        if(!empty($request->bumiputra_no)){
            if(empty($bumiputra->bumiputra_no)){
                audit($bumiputra, __('Maklumat sijil Bumiputra telah berjaya disimpan.'));
            } else {
                if($bumiputra->bumiputra_no == $request->bumiputra_no){
                    audit($bumiputra, __('Maklumat sijil Bumiputra telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->bumiputra)) {
            $bumiputra_files    = Input::file('bumiputra');
            $rulesbumiputra     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validatorbumiputra = Validator::make(['file' => $bumiputra_files], $rulesbumiputra);
            if ($validatorbumiputra->passes()) {
                $destinationPathBumiputra = 'company'; // upload folder in public directory
                $NamaDokumenBumiputra     = strtoupper($request->ssm_no) . '_bumiputra_' . str_replace("&", '', $bumiputra_files->getClientOriginalName());
                $upload_success_bumiputra = $bumiputra_files->move($destinationPathBumiputra, $NamaDokumenBumiputra);

                // save into database
                $extension_bumiputra             = $bumiputra_files->getClientOriginalExtension();
                $entry_bumiputra                 = new Document();
                $entry_bumiputra->document_id    = $bumiputra->id;
                $entry_bumiputra->document_types = 'Bumiputra';
                $entry_bumiputra->mime           = $bumiputra_files->getClientMimeType();
                $entry_bumiputra->document_name  = $NamaDokumenBumiputra;
                $entry_bumiputra->document_path  = $bumiputra_files->getFilename() . '.' . $extension_bumiputra;
                $entry_bumiputra->save();
            }
        }

        $bumiputra_start_date = $request->input('bumiputra_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bumiputra_start_date')) : null;
        $bumiputra_end_date   = $request->input('bumiputra_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bumiputra_end_date')) : null;
        Company::find($company->id)->update(
            [
                'bumiputra_no'         => strtoupper($request->input('bumiputra_no')),
                'bumiputra_start_date' => $bumiputra_start_date,
                'bumiputra_end_date'   => $bumiputra_end_date,
            ]
        );

        $bumiputra->update();

        if(!empty($request->kdn_no)){
            if(empty($kdn->kdn_no)){
                audit($kdn, __('Maklumat sijil KDN telah berjaya disimpan.'));
            } else {
                if($kdn->kdn_no == $request->kdn_no){
                    audit($kdn, __('Maklumat sijil KDN telah berjaya dikemaskini.'));
                } else {

                }
            }
        }

        if (! empty($request->kdn)) {
            $kdn_files    = Input::file('kdn');
            $ruleskdn     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validatorkdn = Validator::make(['file' => $kdn_files], $ruleskdn);
            if ($validatorkdn->passes()) {
                $destinationPathKDN = 'company'; // upload folder in public directory
                $NamaDokumenKDN     = strtoupper($request->ssm_no) . '_kdn_' . str_replace("&", '', $kdn_files->getClientOriginalName());
                $upload_success_kdn = $kdn_files->move($destinationPathKDN, $NamaDokumenKDN);

                // save into database
                $extension_kdn             = $kdn_files->getClientOriginalExtension();
                $entry_kdn                 = new Document();
                $entry_kdn->document_id    = $kdn->id;
                $entry_kdn->document_types = 'KDN';
                $entry_kdn->mime           = $kdn_files->getClientMimeType();
                $entry_kdn->document_name  = $NamaDokumenKDN;
                $entry_kdn->document_path  = $kdn_files->getFilename() . '.' . $extension_kdn;
                $entry_kdn->save();
            }
        }

        $kdn_start_date           = $request->input('kdn_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('kdn_start_date')) : null;
        $kdn_end_date             = $request->input('kdn_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('kdn_end_date')) : null;
        $kdn_blacklist            = isset($request->kdn_blacklist) ? true : false;
        $kdn_blacklist_start_date = isset($request->kdn_blacklist) ? new Carbon() : null;
        $kdn_blacklist_end_date   = $request->input('kdn_blacklist_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('kdn_blacklist_end_date')) : null;
        Company::find($company->id)->update(
            [
                'kdn_no'                   => strtoupper($request->input('kdn_no')),
                'kdn_start_date'           => $kdn_start_date,
                'kdn_end_date'             => $kdn_end_date,
                'kdn_blacklist'            => $kdn_blacklist,
                'kdn_blacklist_start_date' => $kdn_blacklist_start_date,
                'kdn_blacklist_end_date'   => $kdn_blacklist_end_date,
                'license_no'               => $request->input('license_no'),
                'kdn_state'                => $request->input('kdn_states'),
                'ppkkm'                    => $request->input('ppkkm'),
            ]
        );

        $kdn->update();

        $kdn->addresses()->updateOrCreate(['addressable_type' => 'App\Models\Company\KDN', 'addressable_id' => $company->id],
            [
                'primary'   => str_replace("'", '`', $request->kdn_primary),
                'secondary' => str_replace("'", '`', $request->kdn_secondary),
                'postcode'  => $request->input('kdn_postcode'),
                'state'     => $request->input('kdn_state'),
            ]
        );

        swal()->success('Kemaskini Sijil Kelayakan', 'Rekod telah berjaya dikemaskini.', []);

        return redirect()->route('manage.companies.edit', ['hashslug' => $company->hashslug])->withInput(['tab'=>'cert']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
    
    public function delete_ssm($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
    
    public function delete_cidb($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
    
    public function delete_bpku($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
    
    public function delete_spkk($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
    
    public function delete_mof($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
    
    public function delete_bumi($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
    
    public function delete_kdn($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }


    public function StoreCIDB(Request $request, $id, $company)
    {
        // delete all CIDB first
        if (! empty($company->syarikatCIDBGreds)) {
            foreach ($company->syarikatCIDBGreds as $gred) {
                $gred->delete();
            }
        }

        if (! empty($company->syarikatCIDBKategori)) {
            foreach ($company->syarikatCIDBKategori as $kat) {
                if (! empty($kat->SyarikatCIDBKhusus)) {
                    foreach ($kat->SyarikatCIDBKhusus as $kus) {
                        $kus->delete();
                    }
                }
                $kat->delete();
            }
        }

        $greds = $request->input('Gred');
        if (! empty($greds)) {
            foreach ($greds as $gred) {
                $grd             = new CIDBGrade();
                $grd->company_id = $company->id;
                $grd->grades     = $gred;
                $grd->user_id    = user()->id;
                $grd->save();
            }
        }

        $kategoris = $request->input('Kategori');
        if (! empty($kategoris)) {
            foreach ($kategoris as $kategori) {
                $kat             = new CIDBCategory();
                $kat->company_id = $company->id;
                $kat->categories = $kategori;

                $cidbRequired  = [];

                foreach(cidb_codes('category') as $cat){
                    $category = $request->input('Pengkhususan'.$cat->code);
                    if (! empty($category) && $cat->code == $kat->categories) {
                        $kat->save();
                        foreach ($category as $khusus) {
                            $khu           = new CIDBKhusus();
                            $khu->categories_id = $kat->id;
                            $khu->khusus   = $khusus;
                            $khu->user_id  = user()->id;
                            $khu->save();
                        }
                    }
                }
                // $PengkhususanB = $request->input('PengkhususanB');
                // if (! empty($PengkhususanB && 'B' == $kat->categories)) {
                //     $kat->save();
                //     foreach ($PengkhususanB as $khusus) {
                //         $khu                = new CIDBKhusus();
                //         $khu->categories_id = $kat->id;
                //         $khu->khusus        = $khusus;
                //         $khu->user_id       = user()->id;
                //         $khu->save();
                //     }
                // }

                // $PengkhususanCE = $request->input('PengkhususanCE');
                // if (! empty($PengkhususanCE && 'CE' == $kat->categories)) {
                //     $kat->save();
                //     foreach ($PengkhususanCE as $khusus) {
                //         $khu                = new CIDBKhusus();
                //         $khu->categories_id = $kat->id;
                //         $khu->khusus        = $khusus;
                //         $khu->user_id       = user()->id;
                //         $khu->save();
                //     }
                // }

                // $PengkhususanM = $request->input('PengkhususanM');
                // if (! empty($PengkhususanM && 'M' == $kat->categories)) {
                //     $kat->save();
                //     foreach ($PengkhususanM as $khusus) {
                //         $khu                = new CIDBKhusus();
                //         $khu->categories_id = $kat->id;
                //         $khu->khusus        = $khusus;
                //         $khu->user_id       = user()->id;
                //         $khu->save();
                //     }
                // }

                // $PengkhususanE = $request->input('PengkhususanE');
                // if (! empty($PengkhususanE && 'E' == $kat->categories)) {
                //     $kat->save();
                //     foreach ($PengkhususanE as $khusus) {
                //         $khu                = new CIDBKhusus();
                //         $khu->categories_id = $kat->id;
                //         $khu->khusus        = $khusus;
                //         $khu->user_id       = user()->id;
                //         $khu->save();
                //     }
                // }

                // $PengkhususanF = $request->input('PengkhususanF');
                // if (! empty($PengkhususanF && 'F' == $kat->categories)) {
                //     $kat->save();
                //     foreach ($PengkhususanF as $khusus) {
                //         $khu                = new CIDBKhusus();
                //         $khu->categories_id = $kat->id;
                //         $khu->khusus        = $khusus;
                //         $khu->user_id       = user()->id;
                //         $khu->save();
                //     }
                // }
            }
        }
    }

    public function StoreMOF(Request $request, $id, $company)
    {
        // delete all CIDB first
        if (! empty($company->syarikatMOFBidang)) {
            foreach ($company->syarikatMOFBidang as $bidang) {
                $bidang->delete();
            }
        }

        if (! empty($company->syarikatMOFBidang)) {
            foreach ($company->syarikatMOFBidang as $bidang) {
                if (! empty($bidang->syarikatMOFKhusus)) {
                    foreach ($bidang->syarikatMOFKhusus as $khus) {
                        $khus->delete();
                    }
                }
                $bidang->delete();
            }
        }

        // $bidangs = $request->input('Bidang');
        // if (! empty($bidangs)) {
        //     foreach ($bidangs as $bidang) {
        //         $bdg             = new MOFBidang();
        //         $bdg->company_id = $company->id;
        //         $bdg->areas      = $bidang;
        //         $bdg->user_id    = user()->id;
        //         $bdg->save();
        //     }
        // }

        $bidangs = $request->input('Bidang');
        if (! empty($bidangs)) {
            foreach ($bidangs as $bidang) {
                $bdg             = new MOFBidang();
                $bdg->company_id = $company->id;
                $bdg->areas      = $bidang;
                $bdg->user_id    = user()->id;

                $mofRequired       = [];
                foreach(mof_codes('category') as $cat){
                    $category = $request->input('Pengkhususan'.$cat->code);
                    if (! empty($category) && $cat->code == $bdg->areas) {
                        $bdg->save();
                        foreach ($category as $khusus) {
                            $khu           = new MOFKhusus();
                            $khu->company_id = $company->id;
                            $khu->areas_id = $bdg->id;
                            $khu->khusus   = $khusus;
                            $khu->user_id  = user()->id;
                            $khu->save();
                        }
                    }
                }
                // $Pengkhususan10000 = $request->input('Pengkhususan10000');
                // if (! empty($Pengkhususan10000 && '10000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan10000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan20000 = $request->input('Pengkhususan20000');
                // if (! empty($Pengkhususan20000 && '20000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan20000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan30000 = $request->input('Pengkhususan30000');
                // if (! empty($Pengkhususan30000 && '30000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan30000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan40000 = $request->input('Pengkhususan40000');
                // if (! empty($Pengkhususan40000 && '40000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan40000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan50000 = $request->input('Pengkhususan50000');
                // if (! empty($Pengkhususan50000 && '50000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan50000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan60000 = $request->input('Pengkhususan60000');
                // if (! empty($Pengkhususan60000 && '60000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan60000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan70000 = $request->input('Pengkhususan70000');
                // if (! empty($Pengkhususan70000 && '70000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan70000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan80000 = $request->input('Pengkhususan80000');
                // if (! empty($Pengkhususan80000 && '80000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan80000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan90000 = $request->input('Pengkhususan90000');
                // if (! empty($Pengkhususan90000 && '90000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan90000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan100000 = $request->input('Pengkhususan100000');
                // if (! empty($Pengkhususan100000 && '100000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan100000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan210000 = $request->input('Pengkhususan210000');
                // if (! empty($Pengkhususan210000 && '210000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan210000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }

                // $Pengkhususan220000 = $request->input('Pengkhususan220000');
                // if (! empty($Pengkhususan220000 && '220000' == $bdg->areas)) {
                //     $bdg->save();
                //     foreach ($Pengkhususan220000 as $khusus) {
                //         $khu           = new MOFKhusus();
                //         $khu->company_id = $company->id;
                //         $khu->areas_id = $bdg->id;
                //         $khu->khusus   = $khusus;
                //         $khu->user_id  = user()->id;
                //         $khu->save();
                //     }
                // }
            }
        }
        // end
    }
}
