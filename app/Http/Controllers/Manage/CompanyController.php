<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Company\BPKU;
use App\Models\Company\Bumiputra;
use App\Models\Company\CIDB;
use App\Models\Company\Company;
use App\Models\Company\KDN;
use App\Models\Company\MOF;
use App\Models\Company\SPKK;
use App\Models\Company\SSM;
use App\Models\Company\Owner;
use Illuminate\Support\Carbon;
use App\Models\Bank;
use App\Models\Document;
use App\Models\StandardCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless(user()->can('urus-syarikat_index'), 403);

        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        // $std_cd_bank  = standard_code('BankAccount');
        $std_cd_bank  = Bank::get();

        return view('manage.company.index', compact('std_cd_state', 'std_cd_bank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company     = Company::get();
        // $std_cd_state = standard_code('KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia');
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        // $std_cd_bank  = standard_code('BankAccount');
        $std_cd_bank  = Bank::get();

        return view('manage.company.create', compact('company', 'std_cd_state', 'std_cd_bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(user()->can('urus-syarikat_store'), 403);

        $this->validate($request, [
            'ssm_no'       => 'required||unique:companies,ssm_no|max:20',
            'company_name' => 'required|unique:companies,company_name',
            'primary' => 'required',
            'secondary' => 'required',
            'postcode' => 'required|max:5',
            'state' => 'required',
            'email' => 'required|email',
            'gst' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
        ]);

        $company = Company::create([
            'ssm_no' => strtoupper($request->ssm_no), 
            'company_name' => str_replace("'", '`', $request->company_name), 
            'email' =>  str_replace("'", '`', $request->email), 
            'gst_registered_no' => strtoupper($request->gst_registered_no),
            'gst_start_date' => $request->input('gst_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('gst_start_date')) : null, 
            'account_bank_no' => $request->account_bank_no, 
            'bank_name' => $request->bank_name, 
            'status_id' => $request->status_id ,
        ]);

        $company->addresses()->create($request->only('primary', 'secondary', 'postcode', 'state'));

        foreach ($request->only('phone_number') as $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                if (! empty($phone_numbers)) {
                    $company->phones()->create(['phone_type_id' => $key, 'phone_number' => $pn]);
                } else {
                    $company->phones()->create(['phone_type_id' => $key, 'phone_number' => '-']);
                }
            }
        }

        if (! empty($request->gst)) {
            $gst_files = Input::file('gst');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $gst_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'company'; // upload folder in public directory
                $NamaDokumen     = strtoupper($request->ssm_no) . '_gst_' . str_replace("&", '', $gst_files->getClientOriginalName());
                $upload_success  = $gst_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $gst_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $company->id;
                $entry->document_types = 'GST';
                $entry->mime           = $gst_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $gst_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }

        return response()->json(['hashslug' => $company->hashslug ?? null, 'message' => __('Rekod telah berjaya disimpan.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort_unless(user()->can('urus-syarikat_show'), 403);

        $company = Company::findByHashSlug($id);
        $owner   = Owner::where('company_id', $company->id)->get();

        $hashslug     = $company->hashslug;

        if (! is_array($company->addresses)) {
            $address['primary']   = '-';
            $address['secondary'] = '-';
            $address['postcode']  = '-';
            $address['state']     = '-';
        } else {
            $address['primary']   = $company->addresses[0]->primary;
            $address['secondary'] = $company->addresses[0]->secondary;
            $address['postcode']  = $company->addresses[0]->postcode;
            $address['state']     = $company->addresses[0]->state;
        }

        if (! is_array($company->phones)) {
            $phone['office'] = '-';
            $phone['mobile'] = '-';
            $phone['fax']    = '-';
        } else {
            $phone['office'] = $company->phones[0]->phone_number;
            $phone['mobile'] = $company->phones[1]->phone_number;
            $phone['fax']    = $company->phones[2]->phone_number;
        }

        $data = [
            'company' => $company,
            'address' => $address,
            'phone'   => $phone,
        ];

        $todayDate = Carbon::now()->format('Y-m-d');

        // ssm
        $ssm_active_date = Company::where('hashslug',$id)->whereDate('ssm_start_date', '<=', $todayDate)->whereDate('ssm_end_date', '>=', $todayDate)->first();

        $ssm_inactive_date = Company::where('hashslug',$id)->whereDate('ssm_end_date', '<=', $todayDate)->orWhereDate('ssm_start_date', '>=', $todayDate)->first();

        // cidb
        $cidb_active_date = Company::where('hashslug',$id)->whereDate('cidb_start_date', '<=', $todayDate)->whereDate('cidb_end_date', '>=', $todayDate)->first();

        $cidb_inactive_date = Company::where('hashslug',$id)->whereDate('cidb_end_date', '<=', $todayDate)->orWhereDate('cidb_start_date', '>=', $todayDate)->first();

        $cidb_blacklist_date = Company::where('hashslug',$id)->whereDate('cidb_blacklist_start_date', '<=', $todayDate)->whereDate('cidb_blacklist_end_date', '>=', $todayDate)->where('cidb_blacklist',1)->first();

        // mof
        $mof_active_date = Company::where('hashslug',$id)->whereDate('mof_start_date', '<=', $todayDate)->whereDate('mof_end_date', '>=', $todayDate)->first();

        $mof_inactive_date = Company::where('hashslug',$id)->whereDate('mof_end_date', '<=', $todayDate)->orWhereDate('mof_start_date', '>=', $todayDate)->first();

        $mof_blacklist_date = Company::where('hashslug',$id)->whereDate('mof_blacklist_start_date', '<=', $todayDate)->whereDate('mof_blacklist_end_date', '>=', $todayDate)->where('mof_blacklist',1)->first();

        // kdn
        $kdn_active_date = Company::where('hashslug',$id)->whereDate('kdn_start_date', '<=', $todayDate)->whereDate('kdn_end_date', '>=', $todayDate)->first();

        $kdn_inactive_date = Company::where('hashslug',$id)->whereDate('kdn_end_date', '<=', $todayDate)->orWhereDate('kdn_start_date', '>=', $todayDate)->first();

        $kdn_blacklist_date = Company::where('hashslug',$id)->whereDate('kdn_blacklist_start_date', '<=', $todayDate)->whereDate('kdn_blacklist_end_date', '>=', $todayDate)->where('kdn_blacklist',1)->first();
        
        $ssm1         = SSM::findByHashSlug($id);
        $cidb1        = CIDB::findByHashSlug($id);
        $bpku1        = BPKU::findByHashSlug($id);
        $spkk1        = SPKK::findByHashSlug($id);
        $mof1         = MOF::findByHashSlug($id);
        $bumiputra1   = Bumiputra::findByHashSlug($id);
        $kdn1         = KDN::findByHashSlug($id);

        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $std_cd_bank  = standard_code('BankAccount');
        $gst_doc      = Document::where('document_id', $company->id)->where('document_types', 'GST')->orderby('id', 'desc')->limit(1)->get();
        $ssm_doc      = Document::where('document_id', $company->id)->where('document_types', 'SSM')->orderby('id', 'desc')->limit(1)->get();
        $cidb_doc     = Document::where('document_id', $company->id)->where('document_types', 'CIDB')->orderby('id', 'desc')->limit(1)->get();
        $bpku_doc     = Document::where('document_id', $company->id)->where('document_types', 'BPKU')->orderby('id', 'desc')->limit(1)->get();
        $spkk_doc     = Document::where('document_id', $company->id)->where('document_types', 'SPKK')->orderby('id', 'desc')->limit(1)->get();
        $mof_doc      = Document::where('document_id', $company->id)->where('document_types', 'MOF')->orderby('id', 'desc')->limit(1)->get();
        $bumi_doc     = Document::where('document_id', $company->id)->where('document_types', 'Bumiputra')->orderby('id', 'desc')->limit(1)->get();
        $kdn_doc      = Document::where('document_id', $company->id)->where('document_types', 'KDN')->orderby('id', 'desc')->limit(1)->get();

        return view('manage.company.show', compact('company', 'data', 'std_cd_state', 'std_cd_bank', 'gst_doc', 'owner', 'ssm1', 'cidb1', 'bpku1', 'spkk1', 'mof1', 'bumiputra1', 'kdn1', 'ssm_doc', 'cidb_doc', 'bpku_doc', 'spkk_doc', 'mof_doc', 'bumi_doc', 'kdn_doc','ssm_active_date','ssm_inactive_date','cidb_active_date','cidb_inactive_date','cidb_blacklist_date','mof_active_date','mof_inactive_date','mof_blacklist_date','kdn_active_date','kdn_inactive_date','kdn_blacklist_date','hashslug'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_unless(user()->can('urus-syarikat_edit'), 403);

        $company = Company::findByHashSlug($id);

        $todayDate = Carbon::now()->format('Y-m-d');

        // ssm
        $ssm_active_date = Company::where('hashslug',$id)->whereDate('ssm_start_date', '<=', $todayDate)->whereDate('ssm_end_date', '>=', $todayDate)->first();

        $ssm_unlimited_active_date = Company::where('hashslug',$id)->where('ssm_end_date', null)->first();

        $ssm_inactive_date = Company::where('hashslug',$id)->whereDate('ssm_end_date', '<=', $todayDate)->orWhereDate('ssm_start_date', '>=', $todayDate)->first();

        // cidb
        $cidb_active_date = Company::where('hashslug',$id)->whereDate('cidb_start_date', '<=', $todayDate)->whereDate('cidb_end_date', '>=', $todayDate)->first();

        $cidb_inactive_date = Company::where('hashslug',$id)->whereDate('cidb_end_date', '<=', $todayDate)->orWhereDate('cidb_start_date', '>=', $todayDate)->first();

        $cidb_blacklist_date = Company::where('hashslug',$id)->whereDate('cidb_blacklist_start_date', '<=', $todayDate)->whereDate('cidb_blacklist_end_date', '>=', $todayDate)->where('cidb_blacklist',1)->first();

        // mof
        $mof_active_date = Company::where('hashslug',$id)->whereDate('mof_start_date', '<=', $todayDate)->whereDate('mof_end_date', '>=', $todayDate)->first();

        $mof_inactive_date = Company::where('hashslug',$id)->whereDate('mof_end_date', '<=', $todayDate)->orWhereDate('mof_start_date', '>=', $todayDate)->first();

        $mof_blacklist_date = Company::where('hashslug',$id)->whereDate('mof_blacklist_start_date', '<=', $todayDate)->whereDate('mof_blacklist_end_date', '>=', $todayDate)->where('mof_blacklist',1)->first();

        // kdn
        $kdn_active_date = Company::where('hashslug',$id)->whereDate('kdn_start_date', '<=', $todayDate)->whereDate('kdn_end_date', '>=', $todayDate)->first();

        $kdn_inactive_date = Company::where('hashslug',$id)->whereDate('kdn_end_date', '<=', $todayDate)->orWhereDate('kdn_start_date', '>=', $todayDate)->first();

        $kdn_blacklist_date = Company::where('hashslug',$id)->whereDate('kdn_blacklist_start_date', '<=', $todayDate)->whereDate('kdn_blacklist_end_date', '>=', $todayDate)->where('kdn_blacklist',1)->first();

        $hashslug = $company->hashslug;
        $owner    = Company::findByHashSlug($hashslug);

        $ssm1         = SSM::findByHashSlug($id);
        $cidb1        = CIDB::findByHashSlug($id);
        $bpku1        = BPKU::findByHashSlug($id);
        $spkk1        = SPKK::findByHashSlug($id);
        $mof1         = MOF::findByHashSlug($id);
        $bumiputra1   = Bumiputra::findByHashSlug($id);
        $kdn1         = KDN::findByHashSlug($id);
        $std_cd_states = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $ssm_doc      = Document::where('document_id', $company->id)->where('document_types', 'SSM')->orderby('id', 'desc')->limit(1)->get();
        $cidb_doc     = Document::where('document_id', $company->id)->where('document_types', 'CIDB')->orderby('id', 'desc')->limit(1)->get();
        $bpku_doc     = Document::where('document_id', $company->id)->where('document_types', 'BPKU')->orderby('id', 'desc')->limit(1)->get();
        $spkk_doc     = Document::where('document_id', $company->id)->where('document_types', 'SPKK')->orderby('id', 'desc')->limit(1)->get();
        $mof_doc      = Document::where('document_id', $company->id)->where('document_types', 'MOF')->orderby('id', 'desc')->limit(1)->get();
        $bumi_doc     = Document::where('document_id', $company->id)->where('document_types', 'Bumiputra')->orderby('id', 'desc')->limit(1)->get();
        $kdn_doc      = Document::where('document_id', $company->id)->where('document_types', 'KDN')->orderby('id', 'desc')->limit(1)->get();
        $gred         = cidb_codes('grade');
        $kategori     = cidb_codes('category');
        $pengkhususan = cidb_codes('khusus');
        $db1          = standard_code('MOF_Bidang');
        $db2          = standard_code('MOF_Khusus_10000');
        $db3          = standard_code('MOF_Khusus_20000');
        $db4          = standard_code('MOF_Khusus_30000');
        $db5          = standard_code('MOF_Khusus_40000');
        $db6          = standard_code('MOF_Khusus_50000');
        $db7          = standard_code('MOF_Khusus_60000');
        $db8          = standard_code('MOF_Khusus_70000');
        $db9          = standard_code('MOF_Khusus_80000');
        $db10         = standard_code('MOF_Khusus_90000');
        $db11         = standard_code('MOF_Khusus_100000');
        $db12         = standard_code('MOF_Khusus_210000');
        $db13         = standard_code('MOF_Khusus_220000');
        $kategori_mof       = mof_codes('category');
        $mof_khusus    = mof_codes('khusus');

        if (! is_array($company->addresses)) {
            $address['primary']   = '';
            $address['secondary'] = '';
            $address['postcode']  = '';
            $address['state']     = '';
        } else {
            $address['primary']   = $company->addresses[0]->primary;
            $address['secondary'] = $company->addresses[0]->secondary;
            $address['postcode']  = $company->addresses[0]->postcode;
            $address['state']     = $company->addresses[0]->state;
        }

        if (! is_array($company->phones)) {
            $phone['office'] = '';
            $phone['mobile'] = '';
            $phone['fax']    = '';
        } else {
            $phone['office'] = $company->phones[0]->phone_number;
            $phone['mobile'] = $company->phones[1]->phone_number;
            $phone['fax']    = $company->phones[2]->phone_number;
        }

        $data = [
            'company' => $company,
            'address' => $address,
            'phone'   => $phone,
        ];

        // $std_cd_bank  = standard_code('BankAccount');
        $std_cd_bank  = Bank::get();
        $gst_doc      = Document::where('document_id', $company->id)->where('document_types', 'GST')->orderby('id', 'desc')->limit(1)->get();

        return view('manage.company.edit', compact('company', 'std_cd_state', 'std_cd_bank', 'gst_doc', 'hashslug', 'owner', 'ssm1', 'cidb1', 'bpku1', 'spkk1', 'mof1', 'bumiputra1', 'kdn1', 'ssm_doc', 'cidb_doc', 'bpku_doc', 'spkk_doc', 'mof_doc', 'bumi_doc', 'kdn_doc', 'gred', 'kategori', 'pengkhususan', 'db1', 'db2', 'db3', 'db4', 'db5', 'db6', 'db7', 'db8', 'db9', 'db10', 'db11', 'db12', 'db13', 'kategori_mof', 'mof_khusus','std_cd_states','std_cd_state','ssm_active_date', 'ssm_unlimited_active_date','ssm_inactive_date','cidb_active_date','cidb_inactive_date','cidb_blacklist_date','mof_active_date','mof_inactive_date','mof_blacklist_date','kdn_active_date','kdn_inactive_date','kdn_blacklist_date'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless(user()->can('urus-syarikat_update'), 403);

        $this->validate($request, [
            'ssm_no'       => 'required|max:20',
            'company_name' => 'required',
            'primary' => 'required',
            'secondary' => 'required',
            'postcode' => 'required|max:5',
            'state' => 'required',
            'phone_number3' => 'required',
            'phone_number2' => 'required',
            'email' => 'required|email',
            'gst' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
        ]);

        $company = Company::find($id);

        if(empty($company->ssm_no)){
            audit($company, __($request->company_name . ' telah berjaya disimpan.'));
        } else {
            audit($company, __($request->company_name . ' telah berjaya dikemaskini.'));
        }

        Company::find($id)->update([
            'ssm_no' => strtoupper($request->ssm_no), 
            'company_name' => str_replace("'", '`', $request->company_name), 
            'email' =>  str_replace("'", '`', $request->email), 
            'gst_registered_no' => $request->gst_registered_no, 
            'gst_start_date' =>  $request->input('gst_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('gst_start_date')) : null,
            'account_bank_no' => $request->account_bank_no, 
            'bank_name' => $request->bank_name, 
            'status_id' => $request->status_id ,
        ]);

        $address = Company::find($id)->addresses();
        if($address->count() > 0){
            Company::find($id)->addresses()->update([
                'primary' => str_replace("'", '`', $request->primary), 
                'secondary' => str_replace("'", '`', $request->secondary), 
                'postcode' => $request->postcode, 
                'state' => $request->state,
            ]);
        }else{
            Company::find($id)->addresses()->create([
                'primary' => str_replace("'", '`', $request->primary), 
                'secondary' => str_replace("'", '`', $request->secondary), 
                'postcode' => $request->postcode, 
                'state' => $request->state,
            ]);
        }
        
        
        if (! empty($company->phones[0])) {
            $company->phones[0]->phone_number = $request->input('phone_number3');
            $company->phones[0]->update();
        }
        if (! empty($company->phones[1])) {
            $company->phones[1]->phone_number = $request->input('phone_number2');
            $company->phones[1]->update();
        }
        if (! empty($company->phones[2])) {
            $company->phones[2]->phone_number = $request->input('phone_number4');
            $company->phones[2]->update();
        }

        if (! empty($request->gst)) {
            $gst_files = Input::file('gst');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $gst_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'company'; // upload folder in public directory
                $NamaDokumen     = strtoupper($request->ssm_no) . '_gst_' . str_replace("&", '', $gst_files->getClientOriginalName());
                $upload_success  = $gst_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $gst_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $id;
                $entry->document_types = 'GST';
                $entry->mime           = $gst_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $gst_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }
        

        swal()->success('Syarikat', 'Rekod telah berjaya dikemaskini.', []);

        return redirect()->route('manage.companies.edit', $company->hashslug)->withInput(['tab'=>'company']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function owners()
    {
        return $this->has_many('Owner');
    }

    public function cidbgrade()
    {
        return $this->has_many('CIDBGrade');
    }

    public function cidbcategory()
    {
        return $this->has_many('CIDBCategory');
    }

    public function mofcourse()
    {
        return $this->has_many('MOFBidang');
    }

    public function destroy($id)
    {
        abort_unless(user()->can('urus-syarikat_destroy'), 403);

        // $company = Company::hashslug($id);
        $company = Company::findByHashSlug($id);
        $company->owners()->delete();
        $company->cidbgrade()->delete();
        $company->cidbcategory()->delete();
        $company->mofcourse()->delete();
        $company_id = $company->id;
        Document::where('document_id',$company_id)->delete();
        $company->delete();

        // audit($company, __($request->company_name . ' telah berjaya dihapuskan.'));

        swal()->success('Syarikat', 'Rekod telah berjaya dihapuskan.', []);

        return redirect()->route('manage.companies.index');
    }
    
    public function delete($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
}
