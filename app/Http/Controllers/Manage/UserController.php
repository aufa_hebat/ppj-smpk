<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Grade;
use App\Models\Position;
use App\Models\Scheme;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\WorkflowTeamTask;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $JabatanPelaksana = Department::get();
        $Seksyen          = Section::get();
        $PegawaiAtasan    = User::OrderBy('name', 'ASC')->get();
        $listJawatan      = Position::get();
        $listSkim         = Scheme::get();
        $listGred         = grade::get();

        return view('manage.users.index', compact('JabatanPelaksana', 'Seksyen', 'PegawaiAtasan', 'listJawatan', 'listSkim', 'listGred'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.users.create');
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user    = User::findByHashSlug($id);
        $Seksyen = Section::get();

        return view('manage.users.show', compact('user', 'Seksyen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user             = User::findByHashSlug($id);
        $JabatanPelaksana = Department::get();
        $Seksyen          = Section::get();
        $PegawaiAtasan    = User::OrderBy('name', 'ASC')->where('department_id', '!=', null)->get();
        $listJawatan      = Position::get();
        $listSkim         = Scheme::get();
        $listGred         = grade::get();

        return view('manage.users.edit', compact('user', 'JabatanPelaksana', 'Seksyen', 'PegawaiAtasan', 'listJawatan', 'listSkim', 'listGred'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $user = User::findByHashSlug($id);
        $data = $request->only('name', 'email', 'phone_no', 'executor_department_id', 'department_id', 'section_id', 'supervisor_id', 'position_id', 'scheme_id', 'grade_id','honourary');

        $user->update([
            'name'                   => $data['name'],
            'email'                  => $data['email'],
            'phone_no'               => $data['phone_no'],
            'executor_department_id' => $data['executor_department_id'] ?? null,
            'department_id'          => $data['department_id'] ?? null,
            'section_id'             => $data['section_id'] ?? null,
            'supervisor_id'          => $data['supervisor_id'] ?? null,
            'position_id'            => $data['position_id'] ?? null,
            'scheme_id'              => $data['scheme_id'] ?? null,
            'grade_id'               => $data['grade_id'] ?? null,
            'honourary'              => $data['honourary'] ?? null,
        ]);

        if($request->department_id == 3) {
            $workflow_team = 2;
        } else {
            $workflow_team = 1;
        }

        $user_flow_task = WorkflowTeamTask::where('user_id',$user->id)->update([
            'workflow_team_id' => $workflow_team,
            'user_id' => $user->id,
        ]);

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                $user->phones()->update(['phone_type_id' => $key, 'phone_number' => $pn]);
            }
        }

        $user->syncRoles($request->roles);

        swal()->success('Kemaskini Pengguna', 'Rekod telah berjaya dikemaskini.', []);

        return redirect()->route('manage.users.show', ['hashslug' => $user->hashslug]);
    }
}
