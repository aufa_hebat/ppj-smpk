<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Approval;
use App\Models\Acquisition\Bon;
use App\Models\Acquisition\Cmgd;
use App\Models\Acquisition\Cnc;
use App\Models\Acquisition\Cpc;
use App\Models\Acquisition\Deposit;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcBq;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\SubContract;
use App\Models\Acquisition\Termination;
use App\Models\Acquisition\Warning;
use App\Models\Addendum\Addendum;
use App\Models\Addendum\Item as addendums;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Company\CIDB;
use App\Models\Company\MOF;
use App\Models\Company\Owner;
use App\Models\Company\Agent;
use App\Models\Department;
use App\Models\Position;
use App\Models\Sale;
use App\Models\User;
use App\Models\VariationOrderType;
use App\Models\VO\PPJHK\Element as VoElement;
use App\Models\Acquisition\Review;
// use App\Models\VariationOrder;
// use App\Models\VariationOrderElement;
// use App\Models\VariationOrderType;
use App\Models\VO\VO;
use App\Models\VO\PPJHK\Ppjhk;
use App\Models\VO\Active;
use App\Models\VO\SubItem as WpsSub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use PDF;

class ReportViewerController extends Controller
{
    public function generate(Request $request, array $PageSetup, array $data)
    {


        $noPerolehan = '';
        if (! empty($PageSetup['acquisition'])) {
            $noPerolehan = str_replace('/', '', $PageSetup['acquisition']->reference);
        }


        $data = array_add($data, 'hashslug', $request->get('hashslug'));
        $data = array_add($data, 'routeName', Route::currentRouteName());
        $data = array_add($data, 'reportViewPath', $PageSetup['reportViewPath']);

        view()->share($data);



        // if ($request->has('download')) {
        $pdf = PDF::loadView($PageSetup['reportViewPath'], ['print' => true]);
        $pdf->setPaper($PageSetup['size'], $PageSetup['orientation']);

        return $pdf->stream(Carbon::now('Asia/Kuala_Lumpur')->format('Ymd') . '-' . $noPerolehan . '-' . $PageSetup['outputFileName'] . '.pdf');
        // }

        // return view('reportViewer', ['print' => false]);
    }

    public function jadualPerolehan(Request $request)
    {
        $acquisition = Acquisition::where('hashslug', $request->get('hashslug'))->first();

        $data = [
            'acquisition' => $acquisition,
            'cidb'        => $this->getKelayakanCIDB($acquisition->approval->id),
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Jadual-Harga',
            'reportViewPath' => 'contract.pre.box.report.jadual',
            'acquisition'    => $acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function vo_kkk(Request $request)
    {
        $vo = VO::where('hashslug', $request->get('hashslug'))->first();

        $data = [
            'vo' => $vo,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Senarai-Kehadiran-Taklimat',
            'reportViewPath' => 'contract.post.variation-order.reports.kkk',
            'acquisition'    => $vo->sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function vo_ppk(Request $request)
    {
        $vo = VO::where('hashslug', $request->get('hashslug'))->first();
        $appointed = AppointedCompany::where([
            ['acquisition_id', $vo->sst->acquisition_id],
            ['company_id', $vo->sst->company_id],
        ])->first();
        // $types = VariationOrderType::all();
        $types = VariationOrderType::whereNotIn('id', ['1', '6'])->get();

        $Elemen = \App\Models\VO\Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        $PKElemen = "";
        $PKItem = "";
        $PKSub = "";

        foreach($vo->types as $type){
            if($type->variation_order_type_id == 2){
                $PKElemen = \App\Models\VO\Element::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKItem = \App\Models\VO\Item::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKSub = \App\Models\VO\SubItem::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
            }
        } 

        $wpsElements = \App\Models\VO\Element::where([
            ['sst_id', '=', $vo->sst->id],
            ['wps_status', '=', 1],
        ])->get();

        $eots    =   Eot::where('sst_id', $vo->sst->id)->get();

        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $vo->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'vo'            => $vo,
            'appointed'     => $appointed,
            'types'         => $types,
            'PKElemen'      => $PKElemen,
            'PKItem'        => $PKItem,
            'PKSub'         => $PKSub,
            'Elemen'        => $Elemen,
            'eots'          => $eots,
            'np_dept'       => $np_dept,
            'wpsElements'    => $wpsElements,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Permohonan-Perubahan-Kerja',
            'reportViewPath' => 'contract.post.variation-order.reports.ppk.ppk',
            'acquisition'    => $vo->sst->acquisition,
        ];

        //return $this->generate($request, $pageSetup, $data);


        $response = jasper('vo', $vo->sst->acquisition->acquisition_category_id, $vo->hashslug)->handle();
        if(!empty($response)){
            foreach($response as $key1=>$res){

                foreach($res as $re){
                    header('Content-type: application/pdf');
                    header('Content-Disposition: inline; filename="Permohonan-Perubahan-Kerja.pdf"');
                    header('Content-Length: '. strlen($re));
                    echo $re;                   
                    
                }
            }
        }

        

    }

    public function vo_ppk_result(Request $request)
    {
        $vo = VO::where('hashslug', $request->get('hashslug'))->first();
        $appointed = AppointedCompany::where([
            ['acquisition_id', $vo->sst->acquisition_id],
            ['company_id', $vo->sst->company_id],
        ])->first();
        // $types = VariationOrderType::all();
        $types = VariationOrderType::whereNotIn('id', ['1', '6'])->get();

        $Elemen = \App\Models\VO\Element::where([
            ['vo_id', '=', $vo->id],
        ])->get();

        $PKElemen = "";
        $PKItem = "";
        $PKSub = "";

        foreach($vo->types as $type){
            if($type->variation_order_type_id == 2){
                $PKElemen = \App\Models\VO\Element::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKItem = \App\Models\VO\Item::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
                $PKSub = \App\Models\VO\SubItem::where([
                    ['vo_id', '=', $vo->id],
                    ['vo_type_id', '=', $type->id],
                ])->get();
            }
        } 

        $eots    =   Eot::where('sst_id', $vo->sst->id)->get();

        $data = [
            'vo'        => $vo,
            'appointed' => $appointed,
            'types'     => $types,
            'PKElemen'  => $PKElemen,
            'PKItem'    => $PKItem,
            'PKSub'     => $PKSub,
            'Elemen'    => $Elemen,
            'eots'      => $eots,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Keputusan-Permohonan-Perubahan-Kerja',
            'reportViewPath' => 'contract.post.variation-order.reports.ppk.result',
            'acquisition'    => $vo->sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function vo_ppjhk(Request $request)
    {
        $ppjhk = Ppjhk::findByHashSlug($request->get('hashslug'));
        $ppjhks = Ppjhk::where([
            ['sst_id', $ppjhk->sst_id],
            ['status', "1"]
        ])->get();
        $appointed = AppointedCompany::where([
            ['acquisition_id', $ppjhk->sst->acquisition_id],
            ['company_id', $ppjhk->sst->company_id],
        ])->first();

        $data = [
            'sst'           =>  $ppjhk->sst,
            'ppjhk'         =>  $ppjhk,
            'ppjhks'        =>  $ppjhks,
            'appointed'     =>  $appointed
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Perakuan-Pelarasan-Jumlah-Harga-Kontrak',
            'reportViewPath' => 'contract.post.variation-order.reports.ppjhk.ppjhk',
            'acquisition'    => $ppjhk->sst->acquisition,
        ];

        //return $this->generate($request, $pageSetup, $data);

        $response = jasper('ppjhk', $ppjhk->sst->acquisition->acquisition_category_id, $ppjhk->hashslug)->handle();
        if(!empty($response)){
            foreach($response as $key1=>$res){

                foreach($res as $re){
                    header('Content-type: application/pdf');
                    header('Content-Disposition: inline; filename="'.Carbon::now('Asia/Kuala_Lumpur')->format('Ymd') . '-' . $ppjhk->sst->acquisition->reference . '-' . 'Perakuan-Pelarasan-Jumlah-Harga-Kontrak.pdf"');
                    header('Content-Length: '. strlen($re));
                    echo $re;                   
                    
                }
            }
        }
    }

    public function extension(Request $request)
    {
        $appointed = AppointedCompany::findByHashSlug($request->get('hashslug'));
        $eot = Eot::where('hashslug', $appointed->eot->pluck('hashslug')->first())->first();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $eot->department_id)->where('position_id', $position->id)->first() ?? '';
        // $eot = Eot::find($request->get('hashslug'));
        // $eot = AppointedCompany::findByHashSlug($request->get('hashslug'));
        // $appointed = AppointedCompany::where([
        //     ['acquisition_id', $eot->acquisition_id],
        //     ['company_id', $eot->sst->company_id],
        // ])->first();

        $data = [
            // 'eot' => $eot,
            'eot'     =>  $eot,
            'np_dept' =>  $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SENARAI-SEMAK-EOT',
            'reportViewPath' => 'contract.post.extension.report.extension',
            'acquisition'    => $eot->acquisition,
        ];



        return $this->generate($request, $pageSetup, $data);
    }

    public function extension_work(Request $request)
    {

        $eot = Eot::where('hashslug', $request->get('hashslug'))->first();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $eot->department_id)->where('position_id', $position->id)->first() ?? '';
        // $eot = Eot::find($request->get('hashslug'));
        // $eot = AppointedCompany::findByHashSlug($request->get('hashslug'));
        // $appointed = AppointedCompany::where([
        //     ['acquisition_id', $eot->acquisition_id],
        //     ['company_id', $eot->sst->company_id],
        // ])->first();

        $data = [
            // 'eot' => $eot,
            'eot'     =>  $eot,
            'np_dept' =>  $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SENARAI-SEMAK-EOT',
            'reportViewPath' => 'contract.post.extension.report.extension',
            'acquisition'    => $eot->acquisition,
        ];



        return $this->generate($request, $pageSetup, $data);
    }

    public function extension_letter_work(Request $request)
    {

        $eot = Eot::where('hashslug', $request->get('hashslug'))->first();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $eot->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'eot'     =>  $eot,
            'np_dept' =>  $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SURAT-TAWARAN-PELANJUTAN-KONTRAK',
            'reportViewPath' => 'contract.post.extension.report.extension_letter_work',
            'acquisition'    => $eot->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function extension_letter_bekalan_perkhidmatan(Request $request)
    {
        $appointed = AppointedCompany::findByHashSlug($request->get('hashslug'));
        $eot = Eot::where('hashslug', $appointed->eot->pluck('hashslug')->first())->first();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $eot->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'eot'     =>  $eot,
            'np_dept' =>  $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SURAT-TAWARAN-PELANJUTAN-KONTRAK',
            'reportViewPath' => 'contract.post.extension.report.extension_letter_bekalan_perkhidmatan',
            'acquisition'    => $eot->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function brief_attendance(Request $request)
    {
        $acquisition = Acquisition::where('hashslug', $request->get('hashslug'))->first();

        $data = [
            'acquisition' => $acquisition,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Senarai-Kehadiran-Taklimat',
            'reportViewPath' => 'contract.pre.briefing.report.attendance',
            'acquisition'    => $acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function acceptance_letter(Request $request)
    {
        $appointed = AppointedCompany::findByHashSlug($request->get('hashslug'));
        $priceWithoutWps = appointed($appointed)->getPriceWithoutWps($appointed);
        $sst       = Sst::where([
                    ['company_id', '=', $appointed->company_id],
                    ['acquisition_id', '=', $appointed->acquisition_id],
                ])->first();
        $cidb     = CIDB::findByHashSlug($appointed->company->hashslug);
        $mof      = MOF::findByHashSlug($appointed->company->hashslug);
        $bon      = Bon::where('sst_id', $sst->id)->first();
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $sst->acquisition->approval->department_id)->where('position_id', $position->id)->first() ?? '';

        //get president detail
        $positionPresident = Position::where('name', 'Presiden')->first() ?? null;
        $president  = User::where('position_id', $positionPresident->id)->first() ?? '';

        $sale = Sale::where([
            ['company_id', '=', $appointed->company_id],
            ['acquisition_id', '=', $appointed->acquisition_id],
        ])->first();

        $period_length = false; // [ True : less then 4 month  | False : more than 4 month ]

        if (1 == $sale->box->period_type_id && $sale->box->period_length < 120) {
            $period_length = true;
        } elseif (2 == $sale->box->period_type_id && $sale->box->period_length < 16) {
            $period_length = true;
        } elseif (4 == $sale->box->period_type_id && $sale->box->period_length < 4) {
            $period_length = true;
        }

        if($sst->acquisition->approval->type->id == 1){
            //$report_name = 'contract.post.acceptance-letter.report.acceptance_letter_bekalan';
            if(!empty($sst->report_type_id)){
                $report_name = 'contract.post.acceptance-letter.report.bekalan.lampiran' . $sst->report_type_id . '.main';
            }else{
                $report_name = 'contract.post.acceptance-letter.report.bekalan.lampiran1.main';
            }                
        }else if($sst->acquisition->approval->type->id == 2){
            // $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_kerja';
            if(!empty($sst->report_type_id)){
                $report_name = 'contract.post.acceptance-letter.report.kerja.lampiran' . $sst->report_type_id . '.main';
            }else{
                $report_name = 'contract.post.acceptance-letter.report.perkhidmatan.lampiran5.main';
            }     
        }else if($sst->acquisition->approval->type->id == 3){
            // $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_perkhidmatan';
            if(!empty($sst->report_type_id)){
                $report_name = 'contract.post.acceptance-letter.report.perkhidmatan.lampiran' . $sst->report_type_id . '.main';
            }else{
                $report_name = 'contract.post.acceptance-letter.report.perkhidmatan.lampiran8.main';
            }        
        }else if($sst->acquisition->approval->type->id == 4){
            // $report_name = 'contract.post.acceptance-letter.report.acceptance_letter_kerja';
            if(!empty($sst->report_type_id)){
                $report_name = 'contract.post.acceptance-letter.report.penyelenggaraan.lampiran' . $sst->report_type_id . '.main';
            }else{
                $report_name = 'contract.post.acceptance-letter.report.penyelenggaraan.lampiran9.main';
            }
        }

        $data = [
            'appointed' => $appointed,
            'sst'       => $sst,
            'cidb'      => $cidb,
            'mof'       => $mof,
            'bon'       => $bon,
            'np_dept'   => $np_dept,
            'sale'      => $sale,
            'president' => $president,
            'period_length' => $period_length,
            'priceWithoutWps' => $priceWithoutWps
        ];
        


        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Surat_Setuju_Terima_' . $appointed->acquisition->reference,
            'reportViewPath' => $report_name,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function sale_form(Request $request)
    {
        $sales = Sale::findByHashSlug($request->get('hashslug'));
        $officer = User::where('id',$sales->user_id)->where('deleted_at',null)->first();
        $pembeli = null;
        if($sales->buyer_status == 1){
            $pembeli = Owner::whereRaw("replace(ic_number,'-','') = $sales->ic_number")->where('company_id', $sales->company_id)->where('deleted_at',null)->first();
        }else{
            $pembeli = Agent::whereRaw("replace(ic_number,'-','') = $sales->ic_number")->where('company_id', $sales->company_id)->where('deleted_at',null)->first();
        }
        $check_lepas = false;
        if(!empty($sales)){
            $jual = $sales->sales_date;
            $tutup = Carbon::createFromFormat('Y-m-d H:i:s', $sales->acquisition->closed_sale_at)->format('Y-m-d');
            if($jual > $tutup){
                $check_lepas = true;
            }
        }
            
        $data = [
            'sales' => $sales,
            'pembeli' => $pembeli,
            'officer' => $officer,
            'check_lepas'   => $check_lepas,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Borang-Jualan',
            'reportViewPath' => 'sales.report.sale_form',
            'acquisition'    => $sales->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function sale_list(Request $request)
    {
        $acq        = Acquisition::findByHashSlug($request->get('hashslug'));
        $sales_list = Sale::where('acquisition_id', $acq->id)->get();

        $data = [
            'acq'        => $acq,
            'sales_list' => $sales_list,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Senarai-Jualan',
            'reportViewPath' => 'sales.report.sale_list',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function sale_folder(Request $request)
    {
        $acq        = Acquisition::findByHashSlug($request->get('hashslug'));
        $sales_list = Sale::where('acquisition_id', $acq->id)->get();
        $p1         = Review::where('deleted_at',null)->where('type','Acquisitions')->where('status','S1')->where('acquisition_id',$acq->id)->orderBy('id', 'DESC')->pluck('approved_by')->first();
        $p2         = Review::where('deleted_at',null)->where('type','Acquisitions')->where('status','S2')->where('acquisition_id',$acq->id)->orderBy('id', 'DESC')->pluck('approved_by')->first();
        $b1         = Review::where('deleted_at',null)->where('type','Acquisitions')->where('status','S3')->where('acquisition_id',$acq->id)->orderBy('id', 'DESC')->pluck('approved_by')->first();
        $b2         = Review::where('deleted_at',null)->where('type','Acquisitions')->where('status','S4')->where('acquisition_id',$acq->id)->orderBy('id', 'DESC')->pluck('approved_by')->first();
        $pelaksana1 = (!empty($p1))? User::where('id',$p1)->first():null;
        $pelaksana2 = (!empty($p2))? User::where('id',$p2)->first():null;
        $bpub1      = (!empty($b1))? User::where('id',$b1)->first():null;
        $bpub2      = (!empty($b2))? User::where('id',$b2)->first():null;
        
        $data = [
            'acq'        => $acq,
            'sales_list' => $sales_list,
            'pelaksana1' => $pelaksana1,
            'pelaksana2' => $pelaksana2,
            'bpub1'      => $bpub1,
            'bpub2'      => $bpub2,   
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Folder-Jualan',
            'reportViewPath' => 'sales.report.sale_folder',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function sale_folder_verify(Request $request)
    {
        $acq        = Acquisition::findByHashSlug($request->get('hashslug'));
        $sales_list = Sale::where('acquisition_id', $acq->id)->get();

        $data = [
            'acq'        => $acq,
            'sales_list' => $sales_list,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Semakan-Folder-Jualan',
            'reportViewPath' => 'sales.report.sale_folder_verify',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function acq_notice(Request $request)
    {
        $acq     = Acquisition::findByHashSlug($request->get('hashslug'));
        $np_dept = $request->get('np_dept');
        $np_honor = $request->get('np_honor');

        if($acq->approval->acquisition_type_id == 1 || $acq->approval->acquisition_type_id == 3){
            $cidb = $this->getKelayakanMOF($acq->approval->id);
        }else{
            $cidb = $this->getKelayakanCIDB($acq->approval->id);
        }
        

        $data = [
            'acq'     => $acq,
            'np_dept' => $np_dept,
            'np_honor'=>$np_honor,
            'cidb'    => $cidb,
        ];

        // $pageSetup = [
        //     'size'           => 'A4',
        //     'orientation'    => 'potrait',
        //     'outputFileName' => 'Notis',
        //     'reportViewPath' => 'contract.pre.document.report.acq_notice',
        //     'acquisition'    => $acq,
        // ];

        if ('1' == $acq->acquisition_category_id || '2' == $acq->acquisition_category_id) {
            $report_name = 'contract.pre.document.report.acq_notice';
        } elseif ('3' == $acq->acquisition_category_id || '4' == $acq->acquisition_category_id) {
            $report_name = 'contract.pre.document.report.acq_notice_tender';
        }

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Notis',
            'reportViewPath' => $report_name,
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function acq_bq(Request $request)
    {
        $acq       = Acquisition::findByHashSlug($request->get('hashslug'));
        $bq_elemen = Element::where('acquisition_id', $acq->id)->orderBy('no', 'asc')->get();
        $bq_item   = Item::where('acquisition_id', $acq->id)->orderBy('bq_no_element', 'asc')->orderBy('no', 'asc')->get();
        $bq_sub    = SubItem::where('acquisition_id', $acq->id)->orderBy('bq_no_element', 'asc')->orderBy('bq_no_item', 'asc')->orderBy('no', 'asc')->get();

        $data = [
            'acq'       => $acq,
            'bq_elemen' => $bq_elemen,
            'bq_item'   => $bq_item,
            'bq_sub'    => $bq_sub,
        ];

        if ('1' == $acq->acquisition_category_id || '2' == $acq->acquisition_category_id) {
            $report_name = 'contract.pre.document.report.acq_bq';
        } elseif ('3' == $acq->acquisition_category_id || '4' == $acq->acquisition_category_id) {
            $report_name = 'contract.pre.document.report.acq_bq_tender';
        }

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Senarai-Kuantiti',
            'reportViewPath' => $report_name,
            'acquisition'    => $acq,
        ];

        //return $this->generate($request, $pageSetup, $data);

        if(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25){
            $response = jasper('bq_full', $acq->acquisition_category_id, $acq->hashslug)->handle();
        }else{
            $response = jasper('bq', $acq->acquisition_category_id, $acq->hashslug)->handle();
        }
        

        if(!empty($response)){
            foreach($response as $key1=>$res){

                foreach($res as $re){
                    header('Content-type: application/pdf');
                    header('Content-Disposition: inline; filename="Senarai-Kuantiti.pdf"');
                    header('Content-Length: '. strlen($re));
                    echo $re;
                    
                    
                }
            }
        }
    }

    public function addenda(Request $request)
    {
        $acq      = Acquisition::findByHashSlug($request->get('hashslug'));
        $addenda  = (!empty($acq))? Addendum::where('acquisition_id', $acq->id)->orderBy('id', 'desc')->first(): null;
        $perkara  = (!empty($addenda))? addendums::where('addendum_id', $addenda->id)->get(): null;
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = (!empty($acq))? User::where('department_id', 9)->where('executor_department_id',25)->where('position_id', $position->id)->first() :null ;

        $data = [
            'acq'     => $acq,
            'addenda' => $addenda,
            'perkara' => $perkara,
            'np_dept' => $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Adenda',
            'reportViewPath' => 'contract.pre.document.addenda.report.addenda',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function addenda_attach(Request $request)
    {
        $acq     = Acquisition::findByHashSlug($request->get('hashslug'));
        $addenda = (!empty($acq))? Addendum::where('acquisition_id', $acq->id)->orderBy('id', 'desc')->first():null;
        $perkara = (!empty($addenda))? addendums::where('addendum_id', $addenda->id)->get():null;

        $data = [
            'acq'     => $acq,
            'addenda' => $addenda,
            'perkara' => $perkara,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Adenda-Lampiran',
            'reportViewPath' => 'contract.pre.document.addenda.report.addenda_attach',
            'acquisition'    => $acq,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function ipc_cert(Request $request)
    {
        
        $ipc           = Ipc::findByHashSlug($request->get('hashslug'));
        $ipc_no        = $ipc->ipc_no - 1;
        $appointed     = (!empty($ipc))? AppointedCompany::where('acquisition_id', $ipc->sst->acquisition_id)->where('company_id', $ipc->sst->company_id)->where('deleted_at', null)->first():null;
        $ipc_kumpul    = (!empty($ipc))? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<=',$ipc->ipc_no)->where('deleted_at',null)->get(): null;
        // $ipc_pre       = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no',$ipc_no)->where('deleted_at',null)->first():null;
        $ipc_pre       = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        $kumpul_total  = null;
        $kumpul_inv    = null;
        $kumpul_sst    = null;
        $pre_total     = null;
        $pre_inv       = null;
        $pre_sst       = null;
        $semak_bpub    = Review::where('deleted_at',null)->where('ipc_id','!=',null)->where('status','S5')->pluck('approved_by')->first();
        $pengesah      = (!empty($semak_bpub))? User::where('id',$semak_bpub)->first():null;
        if($ipc_pre != null){
            foreach($ipc_pre as $prev){
                if($prev == null){
                    $pre_total = $prev->ipcInvoice->pluck('invoice')->sum() ;
                    $pre_inv   = $prev->ipcInvoice->pluck('demand_amount')->sum() ;
                    $pre_sst   = $prev->ipcInvoice->pluck('sst_amount')->sum() ;
                }else{
                    $pre_total = $pre_total + $prev->ipcInvoice->pluck('invoice')->sum() ;
                    $pre_inv   = $pre_inv + $prev->ipcInvoice->pluck('demand_amount')->sum() ;
                    $pre_sst   = $pre_sst + $prev->ipcInvoice->pluck('sst_amount')->sum() ;
                }
            }
        }
        if($ipc_kumpul != null){
            foreach($ipc_kumpul as $kump){
                if($kumpul_total == null){
                    $kumpul_total = $kump->ipcInvoice->pluck('invoice')->sum() ;
                    $kumpul_inv   = $kump->ipcInvoice->pluck('demand_amount')->sum() ;
                    $kumpul_sst   = $kump->ipcInvoice->pluck('sst_amount')->sum() ;
                }else{
                    $kumpul_total = $kumpul_total + $kump->ipcInvoice->pluck('invoice')->sum() ;
                    $kumpul_inv   = $kumpul_inv + $kump->ipcInvoice->pluck('demand_amount')->sum() ;
                    $kumpul_sst   = $kumpul_sst + $kump->ipcInvoice->pluck('sst_amount')->sum() ;
                }
            }
        }
        $position = Position::where('name', 'Naib Presiden')->first() ?? 0;
        $np_dept  = User::where('department_id', $ipc->sst->acquisition->approval->department_id)->where('position_id', $position->id)->first();
                                
        $data = [
            'ipc'           => $ipc,
            'appointed'     => $appointed,
            'ipc_kumpul'    => $ipc_kumpul,
            'ipc_pre'       => $ipc_pre,
            'pre_total'     => $pre_total,
            'pre_inv'       => $pre_inv,
            'pre_sst'       => $pre_sst,
            'kumpul_total'  => $kumpul_total,
            'kumpul_inv'    => $kumpul_inv,
            'kumpul_sst'    => $kumpul_sst,
            'pengesah'      => $pengesah,
            'np_dept'       => $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.ipc_cert',
            'sst'            => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function ipc_summary(Request $request)
    {
        $ipc          = Ipc::findByHashSlug($request->get('hashslug'));
        $ipc_no       = $ipc->ipc_no - 1;
        //$ipc_pre      = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('ipc_no',$ipc_no)->where('sst_id',$ipc->sst_id)->where('deleted_at',null)->first():null;
        $ipc_pre       = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        $ipc_kumpul   = (!empty($ipc))? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<=',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        $elemen       = Element::where('acquisition_id',$ipc->sst->acquisition_id)->where('deleted_at',null)->get();
        $prov_sum     = Element::where('acquisition_id', $ipc->sst->acquisition_id)->where('deleted_at', null)
                ->where(function($q) {
                    $q->where('element','like','%'. 'PRO' . '%' . ' SUM' . '%')
                    ->orWhere('element','like', '%' . 'PERUNTUKAN' . '%' . 'SEMENTARA'.'%');
                })->pluck('id')->first()   ?? null;
        $elemen_vo    = VoElement::where('sst_id',$ipc->sst_id)->where('deleted_at',null)->get();
        $ipc_bq       = null;
        $kumpul       = null;
        $pre          = null;
        $bq_pre       = null;
        
        if(!empty($ipc_kumpul)){
            foreach($ipc_kumpul as $kump){
                if(!empty($kump->ipcInvoice) && $kump->ipcInvoice != null){
                    foreach($kump->ipcInvoice as $kump_inv){
                        if($kumpul == null){
                            $kumpul = $kump_inv->id;
                        }else{
                            $kumpul = $kumpul.','.$kump_inv->id;
                        }
                    }
                }
            }
        }
        
        if($kumpul != null){
            $kum = explode(',',$kumpul);
            $ipc_bq       = IpcBq::whereIn('ipc_invoice_id',$kum)->where('deleted_at',null)->get();
        }

        if(!empty($ipc_pre)){
            foreach($ipc_pre as $prev){
                if(!empty($prev->ipcInvoice) && $prev->ipcInvoice != null){
                    foreach($prev->ipcInvoice as $pre_inv){
                        if($pre == null){
                            $pre = $pre_inv->id;
                        }else{
                            $pre = $pre.','.$pre_inv->id;
                        }
                    }
                }
            }
        }
        
        if($pre != null){
            $kum_pre = explode(',',$pre);
            $bq_pre       = IpcBq::whereIn('ipc_invoice_id',$kum_pre)->where('deleted_at',null)->get();
        }
        
        $data = [
            'ipc'          => $ipc,
            'ipc_pre'      => $ipc_pre,
            'ipc_kumpul'   => $ipc_kumpul,
            'elemen'       => $elemen,
            'elemen_vo'    => $elemen_vo,
            'ipc_bq'       => $ipc_bq,
            'prov_sum'     => $prov_sum,
            'pre'          => $pre,
            'bq_pre'       => $bq_pre,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Ringkasan Bayaran',
            'reportViewPath' => 'contract.post.ipc.report.ipc_summary',
            'sst'            => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function ipc_status(Request $request)
    {
        $ipc                = Ipc::findByHashSlug($request->get('hashslug'));
        $appointed          = (!empty($ipc))? AppointedCompany::where('acquisition_id', $ipc->sst->acquisition_id)->where('company_id', $ipc->sst->company_id)->where('deleted_at', null)->first():null;
        $ipc_no             = $ipc->ipc_no - 1;
        //$ipc_pre            = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('ipc_no',$ipc_no)->where('sst_id',$ipc->sst_id)->where('deleted_at',null)->first():null;
        //$ipc_pre            = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        $ipc_kumpul         = (!empty($ipc))? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<=',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        
        $data = [
            'ipc'                => $ipc,
            'appointed'          => $appointed,
            'ipc_kumpul'         => $ipc_kumpul,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Status Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.ipc_status',
            'acquisition'    => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }
    
    public function ipc_butiran(Request $request)
    {
        $ipc          = Ipc::findByHashSlug($request->get('hashslug'));
        $ipc_no       = $ipc->ipc_no - 1;
        //$ipc_pre      = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('ipc_no',$ipc_no)->where('deleted_at',null)->first():null;
        $ipc_pre       = (!empty($ipc) && !empty($ipc_no) && $ipc_no != 0)? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        $ipc_kumpul   = (!empty($ipc))? Ipc::where('sst_id',$ipc->sst_id)->where('ipc_no','<=',$ipc->ipc_no)->where('deleted_at',null)->get():null;
        $kumpul       = null;
        $pre          = null;
        $bq_pre       = null;
        $ipc_bq       = null;
        //kiraan terdahulu
        if(!empty($ipc_pre)){
            foreach($ipc_pre as $prev){
                if(!empty($prev->ipcInvoice) && $prev->ipcInvoice != null){
                    foreach($prev->ipcInvoice as $pre_inv){
                        if($pre == null){
                            $pre = $pre_inv->id;
                        }else{
                            $pre = $pre.','.$pre_inv->id;
                        }
                    }
                }
            }
        }
        if($pre != null){
            $kum_pre = explode(',',$pre);
            $bq_pre       = IpcBq::whereIn('ipc_invoice_id',$kum_pre)->where('deleted_at',null)->get();
        }
        //kiraan kumpul
        if(!empty($ipc_kumpul)){
            foreach($ipc_kumpul as $kump){
                if(!empty($kump->ipcInvoice) && $kump->ipcInvoice != null){
                    foreach($kump->ipcInvoice as $kump_inv){
                        if($kumpul == null){
                            $kumpul = $kump_inv->id;
                        }else{
                            $kumpul = $kumpul.','.$kump_inv->id;
                        }
                    }
                }
            }
        }
        
        if($kumpul != null){
            $kum = explode(',',$kumpul);
            $ipc_bq       = IpcBq::whereIn('ipc_invoice_id',$kum)->where('deleted_at',null)->get();
            $el = null;$itm = null;$sub = null;
            $sub_v = null;
            foreach($ipc_bq as $bqs){
                if($bqs->status == 'N'){//normal bq
                    // kumpul element id
                    if($el == null){
                        $el = $bqs->bq_element_id;
                    }else{
                        $el = $el.','.$bqs->bq_element_id;
                    }
                    //kumpul item id
                    if($itm == null){
                        $itm = $bqs->bq_item_id;
                    }else{
                        $itm = $itm.','.$bqs->bq_item_id;
                    }
                    //kumpul sub id
                    if($sub == null){
                        $sub = $bqs->bq_subitem_id;
                    }else{
                        $sub = $sub.','.$bqs->bq_subitem_id;
                    }
                }else{//vo or wang peruntukan bq
                    //kumpul sub id
                    if($sub_v == null){
                        $sub_v = $bqs->bq_subitem_id;
                    }else{
                        $sub_v = $sub_v.','.$bqs->bq_subitem_id;
                    }
                }
                
            }
            $kum_el       = explode(',',$el);
            $kum_itm      = explode(',',$itm);
            $kum_sub      = explode(',',$sub);
            
            $kum_sub_v      = explode(',',$sub_v);
            
            $elemen_all   = Element::whereIn('id',$kum_el)->where('deleted_at',null)->orderBy('no')->get();
            $item_all     = Item::whereIn('id',$kum_itm)->where('deleted_at',null)->orderBy('no')->get();
            $subs_all     = SubItem::whereIn('id',$kum_sub)->where('deleted_at',null)->orderBy('no')->get();
            
            if(!empty($kum_sub_v)){
                $subs_v_all  = WpsSub::whereIn('id',$kum_sub_v)->where('deleted_at',null)->get();
                $sve = null;
                foreach($subs_v_all as $subv){
                    if($sve == null){
                        $sve = $subv->vo_item->vo_element->actives->id;
                    }else{
                        $sve = $sve.','.$subv->vo_item->vo_element->actives->id;
                    }
                }
                $kum_el_v    = explode(',',$sve);
                $elemen_v_all   = Active::whereIn('id',$kum_el_v)->where('deleted_at',null)->get();
            }
            
        }
//        dd($subs_all);
        $data = [
            'ipc'          => $ipc,
            'ipc_pre'      => $ipc_pre,
            'pre'          => $pre,
            'bq_pre'       => $bq_pre,
            'ipc_kumpul'   => $ipc_kumpul,
            'subs_all'     => $subs_all ?? null,
            'ipc_bq'       => $ipc_bq,
            'elemen_all'   => $elemen_all ?? null,
            'item_all'     => $item_all ?? null,
            'elemen_v_all'   => $elemen_v_all ?? null,
            'subs_v_all'     => $subs_v_all ?? null,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'landscape',
            'outputFileName' => 'Butiran Bayaran Interim',
            'reportViewPath' => 'contract.post.ipc.report.ipc_butiran',
            'acquisition'    => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function ipc_first_checklist(Request $request)
    {
        $sst = Sst::findByHashSlug($request->get('hashslug'));
        $ipc = Ipc::where('sst_id', $sst->id)->first();

        $data = [
            'sst' => $sst,
            'ipc' => $ipc,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Semakan IPC 1',
            'reportViewPath' => 'contract.post.ipc.report.ipc_first_checklist',
            'sst'            => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function ipc_current_checklist(Request $request)
    {
        $sst = Sst::findByHashSlug($request->get('hashslug'));
        $ipc = Ipc::where('sst_id', $sst->id)->get();

        $data = [
            'sst' => $sst,
            'ipc' => $ipc,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Semakan IPC Semasa',
            'reportViewPath' => 'contract.post.ipc.report.ipc_current_checklist',
            'sst'            => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function ipc_final_checklist(Request $request)
    {
        $sst = Sst::findByHashSlug($request->get('hashslug'));
        $ipc = Ipc::where('sst_id', $sst->id)->orderBy('id', 'desc')->first();

        $data = [
            'sst' => $sst,
            'ipc' => $ipc,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'Sijil Semakan IPC Akhir',
            'reportViewPath' => 'contract.post.ipc.report.ipc_final_checklist',
            'sst'            => '',
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function warning(Request $request)
    {
        $warning    = Warning::findByHashSlug($request->get('hashslug'));
        $appointed  = AppointedCompany::where('acquisition_id', $warning->sst->acquisition->id)->first();
        $position   = Position::where('name', 'Naib Presiden')->first();
        // $department = Department::where('code', 'JU')->first();
        $np_dept    = User::where('department_id', $warning->sst->acquisition->approval->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'warning'   => $warning,
            'appointed' => $appointed,
            'np_dept'   => $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'WarningLetter',
            'reportViewPath' => 'contract.post.termination.warning.report.warning_letter',
            'warning'        => $warning,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function cpcCertificate(Request $request)
    {
        $sst = Sst::where('hashslug', $request->get('hashslug'))->first();
        $cpc = Cpc::where('sst_id', $sst->id)->first();
        $appointed = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();

        
        $data = [
            'sst'  => $sst,
            'cpc'  => $cpc,
            'cidb' => $this->getKelayakanCIDB($sst->acquisition->approval->id),
            'mof' => $this->getKelayakanMOF($sst->acquisition->approval->id),
            'appointed' => $appointed,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CPC',
            'reportViewPath' => 'contract.post.cpc.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function cncCertificate(Request $request)
    {
        $sst = Sst::where('hashslug', $request->get('hashslug'))->first();
        $cnc = Cnc::where('sst_id', $sst->id)->first();

        $data = [
            'sst' => $sst,
            'cnc' => $cnc,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CNC',
            'reportViewPath' => 'contract.post.cnc.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function terminateReason(Request $request)
    {
        $termination = Termination::findByHashSlug($request->get('hashslug'));
        $appointed   = AppointedCompany::where('acquisition_id', $termination->sst->acquisition->id)->first();
        $position    = Position::where('name', 'Naib Presiden')->first();
        // $department  = Department::where('code', 'JU')->first();
        $np_dept     = User::where('department_id', $termination->sst->acquisition->approval->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'termination' => $termination,
            'appointed'   => $appointed,
            'np_dept'     => $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'WarningLetter',
            'reportViewPath' => 'contract.post.termination.report.reason_terminate',
            'termination'    => $termination,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function terminateNotice(Request $request)
    {
        $termination = Termination::findByHashSlug($request->get('hashslug'));
        $appointed   = AppointedCompany::where('acquisition_id', $termination->sst->acquisition->id)->first();
        $position    = Position::where('name', 'Naib Presiden')->first();
        // $department  = Department::where('code', 'JU')->first();
        $np_dept     = User::where('department_id', $termination->sst->acquisition->approval->department_id)->where('position_id', $position->id)->first() ?? '';

        $data = [
            'termination' => $termination,
            'appointed'   => $appointed,
            'np_dept'     => $np_dept,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'WarningLetter',
            'reportViewPath' => 'contract.post.termination.report.terminate_notice',
            'termination'    => $termination,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function cmgdCertificate(Request $request)
    {
        $sst  = Sst::where('hashslug', $request->get('hashslug'))->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();

        $data = [
            'sst'  => $sst,
            'cmgd' => $cmgd,
            'cidb' => $this->getKelayakanCIDB($sst->acquisition->approval->id),
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'CMGD',
            'reportViewPath' => 'contract.post.cmgd.reports.certificate',
            'acquisition'    => $sst->acquisition,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function sofaRpt(Request $request)
    {
        $sst       = Sst::findByHashSlug($request->get('hashslug'));
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();
        $deposit = Deposit::where('sst_id', $sst->id)->first();

        $eots    =   Eot::where('sst_id', $sst->id)->get();
        $totalYear  = 0;
        $totalMonth = 0;
        $totalWeek  = 0;
        $totalDay   = 0;
        foreach($eots as $eot){
            if(!empty($eot->eot_approve)){
                if($eot->eot_approve->period_type->value == 1){
                    $totalDay = $totalDay + $eot->eot_approve->period;
                }else if($eot->eot_approve->period_type->value == 2){
                    $totalWeek = $totalWeek + $eot->eot_approve->period;
                }else if($eot->eot_approve->period_type->value == 3){
                    $totalMonth = $totalMonth + $eot->eot_approve->period;
                }else if($eot->eot_approve->period_type->value == 4){
                    $totalYear = $totalYear + $eot->eot_approve->period;
                }
            }
        }

        // $totalYear  = 0;
        // $totalMonth = 0;
        // $totalWeek  = 0;
        // $totalDay   = 0;

        // $totalDay = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
        //     return $query->where('value', '==', 1);
        // })->count();

        // $totalWeek = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
        //     return $query->where('value', '==', 2);
        // })->count();

        // $totalMonth = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
        //     return $query->where('value', '==', 3);
        // })->count();

        // $totalYear = \App\Models\Acquisition\Eot::where('sst_id', $sst->id)->whereHas('eot_approve.period_type', function ($query) {
        //     return $query->where('value', '==', 4);
        // })->count();

        $cpc  = Cpc::where('sst_id', $sst->id)->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();
        $cnc  = Cnc::where('sst_id', $sst->id)->first();

        $ppjhk         = Ppjhk::where('sst_id', $sst->id)->where('status', 1)->get();
        $vo_element = 0; //VariationOrderElement::where('sst_id', $sst->id)->get();
        $vo_type    = 0; //VariationOrderType::get();

        $ipc                = Ipc::where('sst_id', $sst->id)->get();
        // $prev_total_invoice = IpcInvoice::where('sst_id', $sst->id)->pluck('invoice')->sum();
        //$prev_total_invoice = Ipc::where('sst_id', $sst->id)->where('last_ipc', 'n')->pluck('demand_amount')->sum();
        $prev_total_invoice = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'n')->pluck('demand_amount')->sum();
        $total_adv_amt      = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'n')->pluck('advance_amount')->sum();
        $prev_gv_adv_amt    = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'n')->pluck('give_advance_amount')->sum();
        $last_ipc           = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'n')->orderBy('ipc_no', 'desc')->first();
        $total_lad          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'y')->pluck('compensation_amount')->sum();
        $wjp_amt            = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'n')->pluck('wjp_amount')->sum();
        $wjp_lepas          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->where('last_ipc', 'n')->pluck('wjp_lepas_amount')->sum();

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();

        
        $total_amount   = 0.0;
        $total_invoice  = 0.0;
        $total_balanced = 0.0;

        if ($sub_contract->count() > 0) {
            foreach ($sub_contract as $index => $row) {
                $total_amount   = $total_amount   + $row->amount;
                $total_invoice  = $total_invoice  + $row->invoices->pluck('invoice')->sum();
                $total_balanced = $total_balanced + ($row->amount - $row->invoices->pluck('invoice')->sum());
            }
        }

        $data = [
            'sst'                => $sst,
            'appointed'          => $appointed,
            'deposit'            => $deposit,
            'cpc'                => $cpc,
            'cmgd'               => $cmgd,
            'cnc'                => $cnc,
            'ppjhk'              => $ppjhk,
            'vo_element'         => $vo_element,
            'vo_type'            => $vo_type,
            'ipc'                => $ipc,
            'prev_total_invoice' => $prev_total_invoice,
            'prev_gv_adv_amt'    => $prev_gv_adv_amt,
            'total_adv_amt'      => $total_adv_amt,
            'last_ipc'           => $last_ipc,
            'wjp_lepas'          => $wjp_lepas,
            'wjp_amt'          => $wjp_amt,
            'sub_contract'       => $sub_contract,
            'total_balanced'     => $total_balanced,
            'total_lad'          => $total_lad,
            // 'eot_aprv'           => $eotAprv,
            'totalDay'   => $totalDay,
            'totalWeek'  => $totalWeek,
            'totalMonth' => $totalMonth,
            'totalYear'  => $totalYear,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SofaReport',
            'reportViewPath' => 'contract.post.sofa.reports.sofaRpt',
            'sst'            => $sst,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    public function akuanRpt(Request $request)
    {
        $sst       = Sst::findByHashSlug($request->get('hashslug'));
        $appointed = AppointedCompany::where([
                                        ['acquisition_id', $sst->acquisition_id],
                                        ['company_id', $sst->company_id],
                                    ])->first();
        $deposit = Deposit::where('sst_id', $sst->id)->first();

        $eots    =   Eot::where('sst_id', $sst->id)->get();

        $cpc  = Cpc::where('sst_id', $sst->id)->first();
        $cmgd = Cmgd::where('sst_id', $sst->id)->first();
        $cnc  = Cnc::where('sst_id', $sst->id)->first();

        $vo         = 0; //VariationOrder::where('sst_id', $sst->id)->get();
        $vo_element = 0; //VariationOrderElement::where('sst_id', $sst->id)->get();
        $vo_type    = 0; //VariationOrderType::get();

        $ipc                = Ipc::where('sst_id', $sst->id)->get();
        // $prev_total_invoice = IpcInvoice::where('sst_id', $sst->id)->pluck('invoice')->sum();
        $prev_total_invoice = Ipc::where('sst_id', $sst->id)->where('last_ipc', 'n')->pluck('demand_amount')->sum();
        $total_adv_amt      = Ipc::where('sst_id', $sst->id)->pluck('advance_amount')->sum();
        $prev_gv_adv_amt    = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('give_advance_amount')->sum();
        $last_ipc           = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->orderBy('ipc_no', 'desc')->first();
        $total_lad          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('compensation_amount')->sum();
        $wjp_amt            = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('wjp_amount')->sum();
        $wjp_lepas          = Ipc::where('sst_id', $sst->id)->where('deleted_at', null)->pluck('wjp_lepas_amount')->sum();

        $sub_contract = SubContract::where('sst_id', $sst->id)->get();

        
        $total_amount   = 0.0;
        $total_invoice  = 0.0;
        $total_balanced = 0.0;

        if ($sub_contract->count() > 0) {
            foreach ($sub_contract as $index => $row) {
                $total_amount   = $total_amount   + $row->amount;
                $total_invoice  = $total_invoice  + $row->invoices->pluck('invoice')->sum();
                $total_balanced = $total_balanced + ($row->amount - $row->invoices->pluck('invoice')->sum());
            }
        }

        $data = [
            'sst'                => $sst,
            'appointed'          => $appointed,
            'deposit'            => $deposit,
            'cpc'                => $cpc,
            'cmgd'               => $cmgd,
            'cnc'                => $cnc,
            'vo'                 => $vo,
            'vo_element'         => $vo_element,
            'vo_type'            => $vo_type,
            'ipc'                => $ipc,
            'prev_total_invoice' => $prev_total_invoice,
            'prev_gv_adv_amt'    => $prev_gv_adv_amt,
            'total_adv_amt'      => $total_adv_amt,
            'last_ipc'           => $last_ipc,
            'wjp_lepas'          => $wjp_lepas,
            'wjp_amt'          => $wjp_amt,
            'sub_contract'       => $sub_contract,
            'total_balanced'     => $total_balanced,
            'total_lad'          => $total_lad,
        ];

        $pageSetup = [
            'size'           => 'A4',
            'orientation'    => 'potrait',
            'outputFileName' => 'SofaReport',
            'reportViewPath' => 'contract.post.sofa.reports.akuanRpt',
            'sst'            => $sst,
        ];

        return $this->generate($request, $pageSetup, $data);
    }

    // private function getKelayakanCIDB(int $approval_id)
    // {
    //     $approval = Approval::find($approval_id);

    //     $cidb             = '';
    //     $atatusGrade      = '';
    //     $atatusCategoryB  = '';
    //     $atatusCategoryCE = '';
    //     $atatusCategoryM  = '';
    //     $atatusCategoryE  = '';
    //     $atatusKhususB    = '';
    //     $atatusKhususCE   = '';
    //     $atatusKhususM    = '';
    //     $atatusKhususE    = '';
    //     $catB             = '';
    //     $catM             = '';
    //     $catE             = '';
    //     $catCE            = '';

    //     if ($approval->cidbQualifications->count() > 0) {
    //         foreach ($approval->cidbQualifications as $index => $row) {
    //             if ('grade' == $row->code->type) {
    //                 if (0 == $index) {
    //                     $cidb = '<b>Gred : </b>';
    //                 }

    //                 $cidb = $cidb . ' ' . $atatusGrade . ' ' . $row->code->code;

    //                 if (1 == $row->status) {
    //                     $atatusGrade = 'Dan';
    //                 } else {
    //                     $atatusGrade = 'Atau';
    //                 }
    //             } else {
    //                 if ('category' == $row->code->type) {
    //                     if ('B' == $row->code->code) {
    //                         $catB = 'Kategori B Pengkhususan';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryB = 'Dan';
    //                         } else {
    //                             $atatusCategoryB = 'Atau';
    //                         }
    //                     } elseif ('CE' == $row->code->code) {
    //                         $catCE = 'Kategori CE Pengkhususan';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryCE = 'Dan';
    //                         } else {
    //                             $atatusCategoryCE = 'Atau';
    //                         }
    //                     } elseif ('M' == $row->code->code) {
    //                         $catM = 'Kategori M Pengkhususan';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryM = 'Dan';
    //                         } else {
    //                             $atatusCategoryM = 'Atau';
    //                         }
    //                     } elseif ('E' == $row->code->code) {
    //                         $catE = 'Kategori E Pengkhususan';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryE = 'Dan';
    //                         } else {
    //                             $atatusCategoryE = 'Atau';
    //                         }
    //                     }
    //                 }

    //                 if ('khusus' == $row->code->type) {
    //                     if ('B' == $row->code->category) {
    //                         $catB = $catB . ' ' . $atatusKhususB . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususB = 'Dan';
    //                         } else {
    //                             $atatusKhususB = 'Atau';
    //                         }
    //                     } elseif ('CE' == $row->code->category) {
    //                         $catCE = $catCE . ' ' . $atatusKhususCE . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususCE = 'Dan';
    //                         } else {
    //                             $atatusKhususCE = 'Atau';
    //                         }
    //                     } elseif ('M' == $row->code->category) {
    //                         $catM = $catM . ' ' . $atatusKhususM . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususM = 'Dan';
    //                         } else {
    //                             $atatusKhususM = 'Atau';
    //                         }
    //                     } elseif ('E' == $row->code->category) {
    //                         $catE = $catE . ' ' . $atatusKhususE . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususE = 'Dan';
    //                         } else {
    //                             $atatusKhususE = 'Atau';
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //         if (! empty($catB)) {
    //             $cidb = $cidb . ', ' . $catB;
    //         }
    //         if (! empty($catCE)) {
    //             if (! empty($catB)) {
    //                 $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catCE;
    //             } else {
    //                 $cidb = $cidb . ', ' . $catCE;
    //             }
    //         }
    //         if (! empty($catM)) {
    //             if (! empty($catCE)) {
    //                 $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catM;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catM;
    //             } else {
    //                 $cidb = $cidb . ', ' . $catM;
    //             }
    //         }
    //         if (! empty($catE)) {
    //             if (! empty($catM)) {
    //                 $cidb = $cidb . ', ' . $atatusCategoryM . ' ' . $catE;
    //             } elseif (! empty($catCE)) {
    //                 $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catE;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catE;
    //             } else {
    //                 $cidb = $cidb . ', ' . $catE;
    //             }
    //         }
    //     }

    //     return $cidb;
    // }

    private function getKelayakanCIDB(int $approval_id)
    {
        $approval = Approval::find($approval_id);

        $collectionGradeCode = collect();
        $collectionGradeName = collect();
        $collectionGradeStatus = collect();

        $collectionCategoryCode = collect();
        $collectionCategoryName = collect();
        $collectionCategoryStatus = collect();

        $collectionKhususCat = collect([]);
        $collectionKhususCode = collect([]);
        $collectionKhususName = collect([]);
        $collectionKhususStatus = collect([]);

        if ($approval->cidbQualifications->count() > 0) {
            foreach ($approval->cidbQualifications as $index => $row) {
                if ('grade' == $row->code->type) {
                    $collectionGradeCode = $collectionGradeCode->concat([$row->code->code]);
                    $collectionGradeName = $collectionGradeName->concat([$row->code->name]);
                    $collectionGradeStatus = $collectionGradeStatus->concat([$row->status]);
                }elseif('category' == $row->code->type){
                    $collectionCategoryCode = $collectionCategoryCode->concat([$row->code->code]);
                    $collectionCategoryName = $collectionCategoryName->concat([$row->code->name]);
                    $collectionCategoryStatus = $collectionCategoryStatus->concat([$row->status]);
                }elseif ('khusus' == $row->code->type) {
                    $collectionKhususCat = $collectionKhususCat->concat([$row->code->category]);
                    $collectionKhususCode = $collectionKhususCode->concat([$row->code->code]);
                    $collectionKhususName = $collectionKhususName->concat([$row->code->name]);
                    $collectionKhususStatus = $collectionKhususStatus->concat([$row->status]);
                }
            }
        }

        $cidbGrade = '';
        $gradeStatus = '';
        $gradeCombine = '';
        $cidbBidang = '';
        $bidangStatus = '';
        $cidbKhusus = '';
        $status = '';
        $cidbCombine = '';
        $cidbLoop = '';        

        for($g = 0; $g< $collectionGradeCode->count(); $g++){
            if($g == 0) {
                $cidbGrade = 'Gred :  ' . $collectionGradeCode[$g] . ' ';
            }else {
                $cidbGrade = $cidbGrade . $collectionGradeCode[$g] . ' ';
            }

            if($collectionGradeStatus[$g] == '0')
                $gradeStatus = 'ATAU ';
            elseif($collectionGradeStatus[$g] == '1')
                $gradeStatus = 'DAN ';
            else
                $gradeStatus = '';

            $gradeCombine = $cidbGrade . $gradeStatus;
        }

        if($collectionGradeCode->count() > 0){
            $gradeCombine = $gradeCombine . ', ';
        }
        
        for($c = 0; $c< $collectionCategoryCode->count(); $c++){
            $cidbBidang = 'Kategori :  ' . $collectionCategoryCode[$c] . ' ';

            if($collectionCategoryStatus[$c] == '0')
                $bidangStatus = 'ATAU ';
            elseif($collectionCategoryStatus[$c] == '1')
                $bidangStatus = 'DAN ';
            else
                $bidangStatus = '';

            $cidbKhusus = '';
            for($k = 0; $k < $collectionKhususCat->count(); $k++){
                if($collectionKhususCat[$k] == $collectionCategoryCode[$c]){
                    
                    if($collectionKhususStatus[$k] == '0')
                        $status = 'ATAU ';
                    elseif($collectionKhususStatus[$k] == '1')
                        $status = 'DAN ';
                    else
                        $status = '';

                    $cidbKhusus = $cidbKhusus . ' ' . $collectionKhususName[$k] . ' ' . $status;
                }
            }
            $cidbCombine = $cidbBidang . $cidbKhusus . $bidangStatus . '';
            $cidbLoop = $cidbLoop . $cidbCombine;

            if($c < $collectionCategoryCode->count() - 1){
                $cidbLoop = $cidbLoop . ', ';
            }
        }

        $cidbLoop = $gradeCombine . $cidbLoop;
        
        return $cidbLoop;
    }

    private function getKelayakanMOF(int $approval_id)
    {
        $approval = Approval::find($approval_id);
        $collectionCategoryCode = collect();
        $collectionCategoryName = collect();
        $collectionCategoryStatus = collect();
        $collectionKhususCat = collect([]);
        $collectionKhususCode = collect([]);
        $collectionKhususName = collect([]);
        $collectionKhususStatus = collect([]);


        if ($approval->mofQualifications->count() > 0) {
            foreach ($approval->mofQualifications as $index => $row) {

                if ('category' == $row->code->type) {
                    $collectionCategoryCode = $collectionCategoryCode->concat([$row->code->code]);
                    $collectionCategoryName = $collectionCategoryName->concat([$row->code->name]);
                    $collectionCategoryStatus = $collectionCategoryStatus->concat([$row->status]);
                }elseif ('khusus' == $row->code->type) {
                    $collectionKhususCat = $collectionKhususCat->concat([$row->code->category]);
                    $collectionKhususCode = $collectionKhususCode->concat([$row->code->code]);
                    $collectionKhususName = $collectionKhususName->concat([$row->code->name]);
                    $collectionKhususStatus = $collectionKhususStatus->concat([$row->status]);
                }
            }            
        }

        $mofBidang = '';
        $bidangStatus = '';
        $mofKhusus = '';
        $status = '';
        $mofCombine = '';
        $mofLoop = '';
        
        for($c = 0; $c< $collectionCategoryCode->count(); $c++){
            //$mofBidang = 'Bidang ' . $collectionCategoryCode[$c] . ' ';

            if($collectionCategoryStatus[$c] == '0')
                $bidangStatus = 'ATAU ';
            elseif($collectionCategoryStatus[$c] == '1')
                $bidangStatus = 'DAN ';
            else
                $bidangStatus = '';

            $mofKhusus = '';
            for($k = 0; $k < $collectionKhususCat->count(); $k++){
                if($collectionKhususCat[$k] == $collectionCategoryCode[$c]){
                    
                    if($collectionKhususStatus[$k] == '0')
                        $status = 'ATAU ';
                    elseif($collectionKhususStatus[$k] == '1')
                        $status = 'DAN ';
                    else
                        $status = '';

                    $mofKhusus = $mofKhusus . ' ' . $collectionKhususName[$k] . ' ' . $status;
                }
            }
            $mofCombine = $mofBidang . $mofKhusus . $bidangStatus . '';
            $mofLoop = $mofLoop . $mofCombine;

        }

        return $mofLoop;
    }
}
