<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Approval;
use App\Models\Company\Company;
use App\Models\Company\Owner;
use App\Models\Company\Representative;
use App\Models\Sale;
use App\Models\Document;
use App\Models\Singleton\SingletonReceiptSale;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\WorkflowTask;

class SaleController extends Controller
{
    public function index()
    {
        $acquisition = Acquisition::where([
            ['deleted_at', null],
            ['cancelled_at', null],
        ])->get();
        $sale     = Sale::where('deleted_at',null)->get();
        $company  = Company::where('deleted_at',null)->get();
        $approval = Approval::where('deleted_at', null)->get();

        return view('sales.index', compact('acquisition', 'sale', 'company', 'approval'));
    }

    public function search(Request $request)
    {
        if (isset($request->ic_number)) {
            $ic_number = str_replace("-","",$request->ic_number);
            $owner     = Owner::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
            $agent     = Representative::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
            $company   = Company::whereNotIn('id', $owner->pluck('company_id'))->orWhereNotIn('id', $agent->pluck('company_id'))->where('deleted_at',null)->get();
            if (2 == $request->sale_status) {
                return redirect()->route('sale.receipt', $ic_number);
            } else {
                return view('sales.search', compact('owner', 'agent', 'company', 'ic_number'));
            }
        } else {
            swal()->success('Carian', 'Tiada Rekod ditemui.', []);

            return view('sales.index');
        }
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $ic_number = str_replace("-","",$request->ic_number);
        if (! empty($request->sale)) {
            $new_sal = SingletonReceiptSale::create($request);
        }
        $message = '';
        if($new_sal != null){
            $message = 'Rekod telah berjaya disimpan.';
        }
        
        return ['message'=>$message];
    }

    public function show($id)
    {
        $todayDate = Carbon::now()->format('Y-m-d');
        
        $ic_number    = str_replace("-","",$id);
        $owner        = Owner::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        $agent        = Representative::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        $check_sale   = Sale::whereIn('company_id', $owner->pluck('company_id'))->orWhereIn('company_id', $agent->pluck('company_id'))->where('deleted_at',null)->get();
        if(user()->current_role_login == 'administrator'|| user()->current_role_login == 'developer'){
            $acquisition  = Acquisition::whereDate('start_sale_at', '<=', $todayDate)->where('deleted_at', null)->where('cancelled_at', null)
                    //->whereDate('closed_at', '>=', $todayDate)->whereDate('closed_sale_at', '>=', $todayDate)
                    ->whereHas('reviews', function ($que) {
                        $que->where('status','Selesai')->latest();
                    })
                    ->where(function($q) {
                        $q->where('extended_closed_sale_at',null)
                        ->whereDate('closed_at', '>=', Carbon::now()->format('Y-m-d'))
                        ->orWhere('extended_closed_sale_at','!=',null)
                        ->whereDate('extended_closed_sale_at','>=', Carbon::now()->format('Y-m-d'));
                    })
                        ->get(); // add filter by start_sale_at , and closed_at
        }else{
            $acquisition  = Acquisition::whereDate('start_sale_at', '<=', $todayDate)->whereDate('closed_sale_at', '>=', $todayDate)
                ->where([['deleted_at', null],['cancelled_at', null]])
                ->whereHas('reviews', function ($que) {
                    $que->where('status','Selesai')->latest();
                })->get(); 
        }
        
        $sales        = [];
        $acq_all      = null;
        $acq_arry     = [];
        $mof          = [];
        $cidb         = [];
        $check_cidb   = [];
        $check_mof    = [];
        
        foreach($acquisition as $acq){
            $g_and  = [];$g_or  = [];
            $cC_and = [];$cC_or = [];
            $kC_and = [];$kC_or = [];
            $cM_and = [];$cM_or = [];
            $kM_and = [];$kM_or = [];
            
            //if approval has cidb grades
            if($acq->approval->cidb == '1' && !empty($acq->approval->cidbQualifications) && $acq->approval->cidbQualifications->count() > 0){

                $check_cidb = $this->getKelayakanCIDB($acq->approval->id);
                if($check_cidb['statement'] != null || empty($check_cidb['statement']))
                {
                    array_push($cidb, [
                        'acq_id'        => $acq->id,
                        'cidb'          => $check_cidb['statement'],
                    ]);
                }
                
            }
            //if approval has mof grades
            if($acq->approval->mof == '1' && !empty($acq->approval->mofQualifications) && $acq->approval->mofQualifications->count() > 0){
                $check_mof = $this->getKelayakanMOF($acq->approval->id);
                
                if($check_mof['statement'] != null || empty($check_mof['statement']))
                {
                    array_push($mof, [
                        'acq_id'        => $acq->id,
                        'mof'          => $check_mof['statement'],
                    ]);
                }
            }
            if($acq->briefing_status == 1){
                if(!empty($acq->briefings->pluck('company')) && !empty($acq->briefings->where('attendant_stastus','!=',3)->pluck('company'))){
                    foreach($acq->briefings->where('attendant_status','!=',3)->pluck('company') as $brief_comp){
                        
                        if(($acq->visit_status == 1 && $acq->briefings->where('company_id',$brief_comp->id)->pluck('site_visit')->first() == 1) || (($acq->visit_status == 0 || $acq->visit_status == '0' || $acq->visit_status == null || empty($acq->visit_status)) && $acq->briefings->where('company_id',$brief_comp->id)->pluck('site_visit')->first() == 2)){
                            
//                            if(!empty($brief_comp->agents) && !empty($brief_comp->agents->whereRaw("replace(ic_number,'-','') = $ic_number"))){
                            if(!empty($brief_comp->agents)){
                                //get agents for briefing
//                                foreach($brief_comp->agents->whereRaw("replace(ic_number,'-','') = $ic_number")->all() as $agn){
                                foreach($brief_comp->agents as $agn){
                                    $ic_agent    = str_replace("-","",$agn->ic_number);
                                    if($ic_agent == $ic_number){
                                        array_push($sales, [
                                            'acq_id'        => $acq->id,
                                            'comp_name'     => $agn->company->company_name ?? null,
                                            'comp_id'       => $agn->company_id ?? null,
                                            'status_type'   => 'WAKIL',
                                            'buyer'         => 2,
                                            'status_brief'  => 'y',
                                            'cidb_grade'    => ($acq->approval->cidb == 1 && !empty($agn->company->cidbgrade) && !empty($agn->company->cidbgrade->pluck('grades')))? $agn->company->cidbgrade->pluck('grades')->all() : null,
                                            'cidb_category' => ($acq->approval->cidb == 1 && !empty($agn->company->cidbcategory) && !empty($agn->company->cidbcategory->pluck('categories')) )? $agn->company->cidbcategory->pluck('categories')->all() : null,
                                            'cidb_khusus'   => ($acq->approval->cidb == 1 && !empty($agn->company->cidbcategory) && !empty($agn->company->cidbcategory->pluck('cidbspecial')) && !empty($agn->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')))? $agn->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all() : null,
                                            'mof_area'      => ($acq->approval->mof == 1 && !empty($agn->company->mofcourse) && !empty($agn->company->mofcourse->pluck('areas')))? $agn->company->mofcourse->pluck('areas')->all() : null,
                                            'mof_khusus'    => ($acq->approval->mof == 1 && !empty($agn->company->mofcourse) && !empty($agn->company->mofcourse->pluck('syarikatMOFKhusus')) && !empty($agn->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')))? $agn->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')->all() : null,
                                            'kebenaranC'    => ((!empty($acq->briefings)) ? $acq->briefings->where('company_id',$agn->company_id)->pluck('permission_remark'):null),
                                            'kebenaranC1'   => null,
                                            'kebenaranC2'   => null,
                                            'kebenaranM1'   => null,
                                            'kebenaranM2'   => null,
                                        ]);
                                        array_push($acq_arry, ['acq_id'   => $acq->id]);
                                    }
                                }
                            }
//                            if(!empty($brief_comp->owners) && !empty($brief_comp->owners->whereRaw("replace(ic_number,'-','') = $ic_number"))){
                            if(!empty($brief_comp->owners)){
                                //get owners for briefing
//                                foreach($brief_comp->owners->whereRaw("replace(ic_number,'-','') = $ic_number")->all() as $own){
                                foreach($brief_comp->owners as $own){
                                    $ic_owner    = str_replace("-","",$own->ic_number);
                                    if($ic_owner == $ic_number){
                                        array_push($sales, [
                                            'acq_id'         => $acq->id,
                                            'comp_name'      => $own->company->company_name ?? null,
                                            'comp_id'        => $own->company_id ?? null,
                                            'status_type'    => 'PEMILIK',
                                            'buyer'          => 1,
                                            'status_brief'   => 'y',
                                            'cidb_grade'     => ($acq->approval->cidb == 1 && !empty($own->company->cidbgrade) && !empty($own->company->cidbgrade->pluck('grades')))? $own->company->cidbgrade->pluck('grades')->all() : null,
                                            'cidb_category'  => ($acq->approval->cidb == 1 && !empty($own->company->cidbcategory) && !empty($own->company->cidbcategory->pluck('categories')) )? $own->company->cidbcategory->pluck('categories')->all() : null,
                                            'cidb_khusus'    => ($acq->approval->cidb == 1 && !empty($own->company->cidbcategory) && !empty($own->company->cidbcategory->pluck('cidbspecial')) && !empty($own->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')))? $own->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all() : null,
                                            'mof_area'       => ($acq->approval->mof == 1 && !empty($own->company->mofcourse) && !empty($own->company->mofcourse->pluck('areas')))? $own->company->mofcourse->pluck('areas')->all() : null,
                                            'mof_khusus'     => ($acq->approval->mof == 1 && !empty($own->company->mofcourse) && !empty($own->company->mofcourse->pluck('syarikatMOFKhusus')) && !empty($own->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')))? $own->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')->all() : null,
                                            'kebenaranC'     => ((!empty($acq->briefings)) ? $acq->briefings->where('company_id',$own->company_id)->pluck('permission_remark'):null),
                                            'kebenaranC1'    => null,
                                            'kebenaranC2'    => null,
                                            'kebenaranM1'    => null,
                                            'kebenaranM2'    => null,
                                        ]);
                                        array_push($acq_arry, ['acq_id'   => $acq->id]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if(($acq->briefing_status == 0 || $acq->briefing_status == null) && empty($acq->briefing_status)){
                
                $g_and  = ($check_cidb != null)? $check_cidb['gred_and']:null;
                $g_or   = ($check_cidb != null)? $check_cidb['gred_or']:null;
                $cC_and = ($check_cidb != null)? $check_cidb['cat_and']:null;
                $cC_or  = ($check_cidb != null)? $check_cidb['cat_or']:null;
                $kC_and = ($check_cidb != null)? $check_cidb['khusus_and']:null;
                $kC_or  = ($check_cidb != null)? $check_cidb['khusus_or']:null;
                
                $cM_and = ($check_mof != null)? $check_mof['cat_and']:null;
                $cM_or  = ($check_mof != null)? $check_mof['cat_or']:null;
                $kM_and = ($check_mof != null)? $check_mof['khusus_and']:null;
                $kM_or  = ($check_mof != null)? $check_mof['khusus_or']:null;
                
                
                if(!empty($agent) && $agent->count() > 0){
                    foreach($agent as $agn){
                        $kebenaranC1    = null;$kebenaranC2    = null;$kebenaranC3    = null;$kebenaranM1    = null;$kebenaranM2    = null;
                        //cidb
                        if($acq->approval->cidb == 1 && !empty($agn->company->cidbgrade) && !$agn->company->cidbgrade->isEmpty() && ($g_and != null || $g_or != null || $cC_and != null || $cC_or != null|| $kC_and != null || $kC_or != null)){
                            if(!empty($g_and)){
                                $kebenaranC1 = implode(' Dan ',array_diff($g_and,$agn->company->cidbgrade->pluck('grades')->all()));
                            }
                            if(!empty($g_or)){
                                if(empty(array_intersect($g_or,$agn->company->cidbgrade->pluck('grades')->all()))){
                                    $kebenaranC1 = ($kebenaranC1 != null)? $kebenaranC1 . ' Dan ' . implode(' Atau ',$g_or):implode(' Atau ',$g_or);
                                }
                            }
                            if(!empty($cC_and)){
                                $kebenaranC3 = implode(' Dan ',array_diff($cC_and,$agn->company->cidbcategory->pluck('categories')->all()));
                            }
                            if(!empty($cC_or)){
                                if(empty(array_intersect($cC_or,$agn->company->cidbcategory->pluck('categories')->all()))){
                                    $kebenaranC3 = ($kebenaranC3 != null)? $kebenaranC3 . ' Dan ' . implode(' Atau ',$cC_or):implode(' Atau ',$cC_or);
                                }
                            }
                            if(!empty($kC_and)){
                                $kebenaranC2 = implode(' Dan ',array_diff($kC_and,$agn->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all()));
                            }
                            if(!empty($kC_or)){
                                if(empty(array_intersect($kC_or,$agn->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all()))){
                                    $kebenaranC2 = ($kebenaranC2 != null)? $kebenaranC2 . ' Dan ' . implode(' Atau ',$kC_or):implode(' Atau ',$kC_or);
                                }
                            }
                            array_push($sales, [
                                'acq_id'        => $acq->id,
                                'comp_name'     => $agn->company->company_name ?? null,
                                'comp_id'       => $agn->company_id ?? null,
                                'status_type'   => 'WAKIL',
                                'buyer'         => 2,
                                'status_brief'  => 'n',
                                'cidb_grade'    => (!empty($agn->company->cidbgrade) && !empty($agn->company->cidbgrade->pluck('grades')))? $agn->company->cidbgrade->pluck('grades')->all() : null,
                                'cidb_category' => (!empty($agn->company->cidbcategory) && !empty($agn->company->cidbcategory->pluck('categories')) )? $agn->company->cidbcategory->pluck('categories')->all() : null,
                                'cidb_khusus'   => (!empty($agn->company->cidbcategory) && !empty($agn->company->cidbcategory->pluck('cidbspecial')) && !empty($agn->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')))? $agn->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all() : null,
                                'mof_area'      =>  null,
                                'mof_khusus'    =>  null,
                                'kebenaranC'     => null,
                                'kebenaranC1'    => $kebenaranC1,
                                'kebenaranC2'    => $kebenaranC2,
                                'kebenaranC3'    => $kebenaranC3,
                                'kebenaranM1'    => $kebenaranM1,
                                'kebenaranM2'    => $kebenaranM2,
                            ]);
                            array_push($acq_arry, ['acq_id'   => $acq->id]);
                        }
                        //cidb
                        //mof
                        
                        if($acq->approval->mof == 1 && !empty($agn->company->mofcourse) && !$agn->company->mofcourse->isEmpty() && ($cM_and != null || $cM_or != null || $kM_and != null || $kM_or != null)){
                            if(!empty($cM_and)){
                                $kebenaranM1 = implode(' Dan ',array_diff($cM_and,$agn->company->mofcourse->pluck('areas')->all()));
                            }
                            if(!empty($cM_or)){
                                if(empty(array_intersect($cM_or,$agn->company->mofcourse->pluck('areas')->all()))){
                                    $kebenaranM1 = ($kebenaranM1 != null)? $kebenaranM1 . ' Dan ' . implode(' Atau ',$cM_or):implode(' Atau ',$cM_or);
                                }
                            }
                            
                            if(!empty($kM_and)){
                                $kebenaranM2 = implode(' Dan ',array_diff($kM_and,$agn->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11,12)->pluck('khusus')->all()));
                            }
                            if(!empty($kM_or)){
                                if(empty(array_intersect($kM_or,$agn->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11,12)->pluck('khusus')->all()))){
                                    $kebenaranM2 = ($kebenaranM2 != null)? $kebenaranM2 . ' Dan ' . implode(' Atau ',$kM_or):implode(' Atau ',$kM_or);
                                }
                            }
                            
                            array_push($sales, [
                                'acq_id'        => $acq->id,
                                'comp_name'     => $agn->company->company_name ?? null,
                                'comp_id'       => $agn->company_id ?? null,
                                'status_type'   => 'WAKIL',
                                'buyer'         => 2,
                                'status_brief'  => 'n',
                                'cidb_grade'    =>  null,
                                'cidb_category' =>  null,
                                'cidb_khusus'   =>  null,
                                'mof_area'      => (!empty($agn->company->mofcourse) && !empty($agn->company->mofcourse->pluck('areas')))? $agn->company->mofcourse->pluck('areas')->all() : null,
                                'mof_khusus'    => (!empty($agn->company->mofcourse) && !empty($agn->company->mofcourse->pluck('syarikatMOFKhusus')) && !empty($agn->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')))? $agn->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')->all() : null,
                                'kebenaranC'     => null,
                                'kebenaranC1'    => $kebenaranC1,
                                'kebenaranC2'    => $kebenaranC2,
                                'kebenaranC3'    => $kebenaranC3,
                                'kebenaranM1'    => $kebenaranM1,
                                'kebenaranM2'    => $kebenaranM2,
                            ]);
                            array_push($acq_arry, ['acq_id'   => $acq->id]);
                        }
                        //mof
                    }
                }
                
                if(!empty($owner) && $owner->count() > 0){
                    foreach($owner as $own){
                        $kebenaranC1    = null;$kebenaranC2    = null;$kebenaranC3    = null;$kebenaranM1    = null;$kebenaranM2    = null;
                        
                        //cidb
                        if($acq->approval->cidb == 1 && !empty($own->company->cidbgrade) && !$own->company->cidbgrade->isEmpty() && ($g_and != null || $g_or != null || $cC_and != null || $cC_or != null || $kC_and != null || $kC_or != null)){
                            if(!empty($g_and)){
                                $kebenaranC1 = implode(' Dan ',array_diff($g_and,$own->company->cidbgrade->pluck('grades')->all()));
                            }
                            if(!empty($g_or)){
                                if(empty(array_intersect($g_or,$own->company->cidbgrade->pluck('grades')->all())))
                                {
                                    $kebenaranC1 = ($kebenaranC1 != null)? $kebenaranC1 . ' Dan ' . implode(' Atau ',$g_or):implode(' Atau ',$g_or);
                                }
                            }
                            if(!empty($cC_and)){
                                $kebenaranC3 = implode(' Dan ',array_diff($cC_and,$own->company->cidbcategory->pluck('categories')->all()));
                            }
                            if(!empty($cC_or)){
                                if(empty(array_intersect($cC_or,$own->company->cidbcategory->pluck('categories')->all()))){
                                    $kebenaranC3 = ($kebenaranC3 != null)? $kebenaranC3 . ' Dan ' . implode(' Atau ',$cC_or):implode(' Atau ',$cC_or);
                                }
                            }
                            if(!empty($kC_and)){
                                $kebenaranC2 = implode(' Dan ',array_diff($kC_and,$own->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all()));
                            }
                            if(!empty($kC_or)){
                                if(empty(array_intersect($kC_or,$own->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all())))
                                {
                                    $kebenaranC2 = ($kebenaranC2 != null)? $kebenaranC2 . ' Dan ' . implode(' Atau ',$kC_or):implode(' Atau ',$kC_or);
                                }
                            }
                            array_push($sales, [
                                'acq_id'        => $acq->id,
                                'comp_name'     => $own->company->company_name ?? null,
                                'comp_id'       => $own->company_id ?? null,
                                'status_type'   => 'PEMILIK',
                                'buyer'         => 1,
                                'status_brief'  => 'n',
                                'cidb_grade'    => (!empty($own->company->cidbgrade) && !empty($own->company->cidbgrade->pluck('grades')))? $own->company->cidbgrade->pluck('grades')->all() : null,
                                'cidb_category' => (!empty($own->company->cidbcategory) && !empty($own->company->cidbcategory->pluck('categories')) )? $own->company->cidbcategory->pluck('categories')->all() : null,
                                'cidb_khusus'   => (!empty($own->company->cidbcategory) && !empty($own->company->cidbcategory->pluck('cidbspecial')) && !empty($own->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')))? $own->company->cidbcategory->pluck('cidbspecial')->collapse(0,1,2,3,4)->pluck('khusus')->all() : null,
                                'mof_area'      =>  null,
                                'mof_khusus'    =>  null,
                                'kebenaranC'     => null,
                                'kebenaranC1'    => $kebenaranC1,
                                'kebenaranC2'    => $kebenaranC2,
                                'kebenaranC3'    => $kebenaranC3,
                                'kebenaranM1'    => $kebenaranM1,
                                'kebenaranM2'    => $kebenaranM2,
                            ]);
                            array_push($acq_arry, ['acq_id'   => $acq->id]);
                        }
                        //cidb
                        //mof
                        if($acq->approval->mof == 1 && !empty($own->company->mofcourse) && !$own->company->mofcourse->isEmpty() && ($cM_and != null || $cM_or != null)){
                            if(!empty($cM_and)){
                                $kebenaranM1 = implode(' Dan ',array_diff($cM_and,$own->company->mofcourse->pluck('areas')->all()));
                            }
                            if(!empty($cM_or)){
                                if(empty(array_intersect($cM_or,$own->company->mofcourse->pluck('areas')->all()))){
                                    $kebenaranM1 = ($kebenaranM1 != null)? $kebenaranM1 . ' Dan ' . implode(' Atau ',$cM_or):implode(' Atau ',$cM_or);
                                }
                            }
                            if(!empty($kM_and)){
                                $kebenaranM2 = implode(' Dan ',array_diff($kM_and,$own->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11,12)->pluck('khusus')->all()));
                            }
                            if(!empty($kM_or)){
                                if(empty(array_intersect($kM_or,$own->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11,12)->pluck('khusus')->all()))){
                                    $kebenaranM2 = ($kebenaranM2 != null)? $kebenaranM2 . ' Dan ' . implode(' Atau ',$kM_or):implode(' Atau ',$kM_or);
                                }
                            }
                            array_push($sales, [
                                'acq_id'        => $acq->id,
                                'comp_name'     => $own->company->company_name ?? null,
                                'comp_id'       => $own->company_id ?? null,
                                'status_type'   => 'PEMILIK',
                                'buyer'         => 1,
                                'status_brief'  => 'n',
                                'cidb_grade'    =>  null,
                                'cidb_category' =>  null,
                                'cidb_khusus'   =>  null,
                                'mof_area'      => (!empty($own->company->mofcourse) && !empty($own->company->mofcourse->pluck('areas')))? $own->company->mofcourse->pluck('areas')->all() : null,
                                'mof_khusus'    => (!empty($own->company->mofcourse) && !empty($own->company->mofcourse->pluck('syarikatMOFKhusus')) && !empty($own->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')))? $own->company->mofcourse->pluck('syarikatMOFKhusus')->collapse(0,1,2,3,4,5,6,7,8,9,10,11)->pluck('khusus')->all() : null,
                                'kebenaranC'     => null,
                                'kebenaranC1'    => $kebenaranC1,
                                'kebenaranC2'    => $kebenaranC2,
                                'kebenaranC3'    => $kebenaranC3,
                                'kebenaranM1'    => $kebenaranM1,
                                'kebenaranM2'    => $kebenaranM2,
                            ]);
                            array_push($acq_arry, ['acq_id'   => $acq->id]);
                        }
                        //mof
                    }
                }
            }
        }
        $list_acq = Acquisition::whereIn('id',$acq_arry)->get();
        $data = [
                'list_acq'     => $list_acq ?? null,
                'acq_all'      => $acq_all ?? null,
                'acq_arry'     => $acq_arry ?? null,
                'acquisition'  => $acquisition ?? null,
                'owner'        => $owner ?? null,
                'agent'        => $agent ?? null,
                'ic_number'    => $ic_number ?? null,
                'check_sale'   => $check_sale ?? null,
                'sales'        => $sales ?? null,
                'cidb'         => $cidb ?? null,
                'mof'          => $mof ?? null,
            ];

        return view('sales.show')->with($data);
    }

    public function receipt($id)
    {
        $ic_number = str_replace('-','',$id);
        $owner     = Owner::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        $agent     = Representative::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        $sale      = Sale::where('deleted_at',null)->whereIn('company_id', $owner->pluck('company_id'))->orWhereIn('company_id', $agent->pluck('company_id'))
                    ->whereHas('acquisition', function ($query) {
                        $query->where('deleted_at',null)->where('cancelled_at',null);
                    })
                    ->get();
        $check_lepas = [];
        if(!empty($sale) && $sale->count() > 0){
            foreach($sale as $jualan){
                $jual = $jualan->sales_date;
                $tutup = Carbon::createFromFormat('Y-m-d H:i:s', $jualan->acquisition->closed_sale_at)->format('Y-m-d');
                if($jual > $tutup){
                    array_push($check_lepas,$jualan->id);
                }
            }
        }
//        $sale      = Sale::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        
        return view('sales.receipt', compact('owner', 'agent', 'company', 'ic_number', 'sale', 'check_lepas'));
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $ic_number = str_replace('-','',$request->ic_number);
        $sale = SingletonReceiptSale::update($request, $id);
        $message = '';
        if($sale != null){
            $message = 'Resit berjaya dikemakini';
        }
        return ['message'=>$message];
    }

    public function wakil(Request $request)
    {
        $ic_number = str_replace('-','',$request->ic_number);
        if(!empty($request->ic_number)){
            $wakil = Representative::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->first();
            if(!empty($wakil) && $wakil->count() > 0){
                $wakil_edit = Representative::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
                if(!empty($wakil_edit) && $wakil_edit->count() > 0){
                    foreach($wakil_edit as $we){
                        if($we->name != $request->name || $we->telephone_num != $request->telephone_num)
                        {
                            $we->name          = $request->name;
                            $we->telephone_num = $request->telephone_num;
                            $we->update();
                            audit($we, __('Wakil ID: ' .$we->id. ' telah berjaya dikemaskini.'));
                        }
                    }
                }
            }
        }
        if (! empty($request->company_id)) {
            foreach ($request->company_id as $comp) {
                $check_duplicate = Representative::where('deleted_at',null)->where('company_id', $comp)->whereRaw("replace(ic_number,'-','') = $ic_number")->count();
                
                if ($check_duplicate < 1) {
                    $rep                = new Representative();
                    $rep->company_id    = $comp;
                    $rep->name          = $request->name;
                    $rep->ic_number     = $ic_number;
                    $rep->telephone_num = $request->telephone_num;
                    $rep->save();

                    $companies = Company::find($comp);

                    audit($rep, __('Wakil syarikat ' .$companies->company_name. ' telah berjaya ditambah.'));

                    swal()->success('Tambah Wakil', 'Rekod telah berjaya disimpan.', []);
                }
            }
        }

        $owner     = Owner::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        $agent     = Representative::where('deleted_at',null)->whereRaw("replace(ic_number,'-','') = $ic_number")->get();
        $company   = Company::whereNotIn('id', $owner->pluck('company_id'))->orWhereNotIn('id', $agent->pluck('company_id'))->where('deleted_at',null)->get();

        return view('sales.search', compact('owner', 'agent', 'company', 'ic_number'));
    }

    private function getKelayakanCIDB(int $approval_id)
    {
        $approval = Approval::find($approval_id);

        $cidb             = '';
        $statusGrade      = '';
        $statusCat        = '';
        $statusKhusus     = '';
        $cat              = '';
        $khusus           = '';
        $grade_and        = [];
        $grade_or         = [];
        $cat_and          = [];
        $cat_or           = [];
        $khusus_and       = [];
        $khusus_or        = [];
        
        if ($approval->cidbQualifications->count() > 0) {
            foreach ($approval->cidbQualifications as $index => $row) {
                if ('grade' == $row->code->type) {
                    
                    if ($cidb == null) {
                        $cidb = '<b>CIDB : </b><br> GRED <b>' . $row->code->code . '</b>';
                    }else{
                        $cidb = $cidb . ' ' . $statusGrade . ' <b>' . $row->code->code . '</b>';
                    }
                    
                    if (1 == $row->status) {
                        $statusGrade = '<b> DAN </b>';
                        array_push($grade_and, $row->code->code);
                    } elseif(0 == $row->status){
                        $statusGrade = '<b> ATAU </b>';
                        array_push($grade_or, $row->code->code);
                    }else{
                        if($grade_and == null && $grade_or == null){
                            array_push($grade_and, $row->code->code);
                        }else{
                            if($approval->cidbQualifications[$index-1]->code->code == 1){
                                array_push($grade_and, $row->code->code);
                            }elseif($approval->cidbQualifications[$index-1]->code->code == 0){
                                array_push($grade_or, $row->code->code);
                            } 
                        }
                    }
                }
                if ('category' == $row->code->type) {
                    
                    if($cat == ''){
                        $cat = '<br>KATEGORI <b>' .$row->code->code . '</b>';
                    }else{
                        $cat = $cat . ' ' .$statusCat . ' <b>' .$row->code->code . '</b>';
                    }
                    if (1 == $row->status) {
                        $statusCat = '<b> DAN </b>';
                        array_push($cat_and, $row->code->code);
                    } elseif(0 == $row->status){
                        $statusCat = '<b> ATAU </b>';
                        array_push($cat_or, $row->code->code);
                    }else{
                        if($cat_and == null && $cat_or == null){
                            array_push($cat_and, $row->code->code);
                        }else{
                            if($approval->cidbQualifications[$index-1]->code->code == 1){
                                array_push($cat_and, $row->code->code);
                            }elseif($approval->cidbQualifications[$index-1]->code->code == 0){
                                array_push($cat_or, $row->code->code);
                            } 
                        }
                    }
                } 
                if ('khusus' == $row->code->type) {
                    
                    if($khusus == ''){
                        $khusus = '<br>PENGKHUSUSAN  <b>' . $row->code->code . '</b>';
                    }else{
                        $khusus = $khusus. ' ' . $statusKhusus . ' <b>' . $row->code->code . '</b>';
                    }
                    if (1 == $row->status) {
                        $statusKhusus = '<b> DAN </b>';
                        array_push($khusus_and, $row->code->code);
                    } elseif(0 == $row->status){
                        $statusKhusus = '<b> ATAU </b>';
                        array_push($khusus_or, $row->code->code);
                    }else{
                        $statusKhusus = ',';
                        if($khusus_and == null && $khusus_or == null){
                            array_push($khusus_and, $row->code->code);
                        }else{
                            if($approval->cidbQualifications[$index-1]->code->code == 1){
                                array_push($khusus_and, $row->code->code);
                            }elseif($approval->cidbQualifications[$index-1]->code->code == 0){
                                array_push($khusus_or, $row->code->code);
                            } 
                        }
                    }
                }
            }
            if($cidb != null && $cat != null && $khusus != ''){
                $cidb = $cidb . ' ' . $cat . ' ' .$khusus;
            }elseif($cidb == null && $cat != null && $khusus != ''){
                $cidb = $cat . ' ' .$khusus;
            }elseif($cidb != null && $cat == null && $khusus != ''){
                $cidb = $cidb . ' ' .$khusus;
            }elseif($cidb != null && $cat != null && $khusus == ''){
                $cidb = $cidb . ' ' . $cat ;
            }elseif($cidb == null && $cat == null && $khusus != ''){
                $cidb = $khusus;
            }elseif($cidb == null && $cat != null && $khusus == ''){
                $cidb = $cat;
            }elseif($cidb != null && $cat == null && $khusus == ''){
                $cidb = $cidb;
            }
            
        }
        return [
            'statement'=>$cidb, 
            'gred_and'=>$grade_and, 
            'gred_or'=>$grade_or, 
            'cat_and'=>$cat_and, 
            'cat_or'=>$cat_or, 
            'khusus_and'=>$khusus_and, 
            'khusus_or'=>$khusus_or
                ];
    }

    private function getKelayakanMOF(int $approval_id)
    {
        $approval = Approval::find($approval_id);
        $collectionCategoryCode = collect();
        $collectionCategoryName = collect();
        $collectionCategoryStatus = collect();
        $collectionKhususCat = collect([]);
        $collectionKhususCode = collect([]);
        $collectionKhususName = collect([]);
        $collectionKhususStatus = collect([]);
        $cat_and          = [];
        $cat_or           = [];
        $khusus_and       = [];
        $khusus_or        = [];
        $bidangStatus = '';
        $mofKhusus = '';
        $status = '';
        $mofLoop = '';
        $mofLoop1 = '';

        if ($approval->mofQualifications->count() > 0) {
            foreach ($approval->mofQualifications as $index => $row) {
                
                if ('category' == $row->code->type) {
                    if($row->status == '0'){
                        $bidangStatus = '<b> ATAU </b>';
                        array_push($cat_or, $row->code->code);
                    }elseif($row->status == '1'){
                        $bidangStatus = '<b> DAN </b>';
                        array_push($cat_and, $row->code->code);
                    }else{
                        if($cat_and == null && $cat_or == null){
                            array_push($cat_and, $row->code->code);
                        }else{
                            if($approval->mofQualifications[$index-1]->status == '0'){
                                array_push($cat_or, $row->code->code);
                            }elseif($approval->mofQualifications[$index-1]->status == '1'){
                                array_push($cat_and, $row->code->code);
                            }
                        }
                    }
                    
                    if($mofLoop1 == ''){
                        $mofLoop1 = '<b>' . $row->code->code . '</b> ';
                    }else{
                        $mofLoop1 = $mofLoop1 . ' '. $bidangStatus . ' <b>' . $row->code->code . '</b> ';
                    }

                    
                }elseif ('khusus' == $row->code->type) {
                    
                    if($row->status == '0'){
                        $status = '<b> ATAU </b>';
                        array_push($khusus_or, $row->code->code);
                    }elseif($row->status == '1'){
                        $status = '<b> DAN </b>';
                        array_push($khusus_and, $row->code->code);
                    }else{
                        $status = '<b> , </b>';
                        if($khusus_and == null && $khusus_or == null){
                            array_push($khusus_and, $row->code->code);
                        }else{
                            if($approval->mofQualifications[$index-1]->status == '0'){
                                array_push($khusus_or, $row->code->code);
                            }elseif($approval->mofQualifications[$index-1]->status == '1'){
                                array_push($khusus_and, $row->code->code);
                            }
                        }
                    }
                    
                    if($mofLoop == ''){
                        $mofLoop = '<b>' . $row->code->code . '</b> ';
                    }else{
                        $mofLoop = $mofLoop . ' '. $status . ' <b>' . $row->code->code . '</b> ';
                    }
                }
            }            
        }
        
        $statement = "<b>MOF : </b><br>BIDANG " . $mofLoop1.' <br>KATEGORI '. $mofLoop;
        return ['statement'=>$statement,'cat_and'=>$cat_and,'cat_or'=>$cat_or,'khusus_and'=>$khusus_and,'khusus_or'=>$khusus_or];
    }
    
    public function destroy(Request $request, $id)
    {
        Document::hashslug($id)->delete();
        if($request->ajax()){
            return response()->json(['message'=>'Lampiran telah berjaya dibuang.']);
        }
    }
}
