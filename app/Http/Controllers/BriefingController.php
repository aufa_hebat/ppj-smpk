<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Briefing;
use App\Models\Company\Representative;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use App\Models\Acquisition\Review;
use Illuminate\Support\Carbon;

class BriefingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.pre.briefing.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $acquisition = Acquisition::withDetails()->findByHashSlug($request->id);
        
        $briefing = (!empty($acquisition->briefings) && $acquisition->briefings->count() > 0)? $acquisition->briefings->where('attendant_status', '!=', '3'):null;
        //var_dump(count($briefing)); die();
        return view('contract.pre.briefing.create', compact('acquisition', 'briefing'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if ('on' == $request->special_permission_status) {
            $special_permission_status = 1;
        } else {
            $special_permission_status = 2;
        }
        $company          = Company::withDetails()->findByHashSlug($request->company_id);
        $company_owner_id = $request->company_owner_id;

        $results = Briefing::where('acquisition_id', '=', $request->acquisition_id)
                    ->where('company_id', '=', $company->id)
                    ->where('attendant_status', '!=', 3)
                    ->first();
        $check_visit = Acquisition::find($request->acquisition_id);
        if (! isset($results->id)) {
            if (2 == $request->attendant_status && '' != $request->company_agent_name && '' != $request->company_agent_ic && '' != $request->company_agent_phone) {
                $approval = Representative::create([
                    'company_id'    => $company->id,
                    'name'          => $request->company_agent_name,
                    'ic_number'     => $request->company_agent_ic,
                    'telephone_num' => $request->company_agent_phone,
                ]);
                $company_owner_id = $approval->id;
            } elseif (2 == $request->attendant_status && '' == $request->company_agent_name && '' == $request->company_agent_ic && '' == $request->company_agent_phone) {
                $company_owner_id = $request->company_agent_id;
            }
            $approval = Briefing::create([
                'acquisition_id'            => $request->acquisition_id,
                'user_id'                   => user()->id,
                'company_id'                => $company->id,
                'company_owner_id'          => $company_owner_id,
                'briefing_staff_id'         => user()->id,
                'attendant_status'          => $request->attendant_status,
                'special_permission_status' => $special_permission_status,
                'permission_remark'         => $request->permission_remark,
                'site_visit'                => 2,
            ]);

            if($check_visit->visit_status == 1){
                $acq_review = Review::where('acquisition_id', $request->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
                
                $workflow = WorkflowTask::where('acquisition_id',$request->acquisition_id)->first();
                $workflow->flow_desc = 'Lawatan Tapak : Sila kemaskini kehadiran lawatan tapak.';
                $workflow->flow_location = 'Lawatan Tapak';
                $workflow->user_id = null;
                $workflow->url = '/site-visit/'.$check_visit->hashslug.'/edit';
                $workflow->is_claimed = 1;
                $workflow->update();

                $check_visit->status_task = config('enums.flow5');
                $check_visit->update();
            } else {
                $acq_review = Review::where('acquisition_id', $request->acquisition_id)->where('type', 'Acquisitions')->orderby('id', 'desc')->first();
                
                $workflow = WorkflowTask::where('acquisition_id',$request->acquisition_id)->first();
                $workflow->flow_desc = 'Jualan : Sila pastikan jualan perolehan wujud dan isi nombor resit.';
                $workflow->flow_location = 'Jualan';
                $workflow->user_id = null;
                $workflow->url = '/sale';
                $workflow->is_claimed = 1;
                $workflow->update();

                $check_visit->status_task = config('enums.flow6');
                $check_visit->update();
            }

            audit($check_visit, __('Kehadiran Taklimat oleh ' .$company->company_name. ' bagi perolehan ' .$check_visit->reference. ' : ' .$check_visit->title. ' telah berjaya ditambah.'));

            swal()->success('Kehadiran Taklimat', 'Rekod telah berjaya disimpan.', []);

        } else {
            swal()->error('Kehadiran Taklimat', 'Syarikat ini sudah menghadiri', []);
        }

//        return redirect()->back();
        return redirect()->route('briefing.listing', $request->acq_hash);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acquisition      = Acquisition::withDetails()->findByHashSlug($id);
        $unsorted_company = [];
        foreach ($acquisition->briefings as $briefing) {
            if (3 != $briefing->attendant_status) {
                $unsorted_company[] = $briefing->company->company_name;
            }
        }

        $sorted_company = array_sort($unsorted_company);

        if($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3) {
            $cidb = common()->getKelayakanMOFView($acquisition->approval->id);
        }else{
            $cidb = common()->getKelayakanCIDBView($acquisition->approval->id);
        }

        return view('contract.pre.briefing.show', compact('acquisition', 'sorted_company', 'cidb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gred = null;$cat = null;$khusus = null;$catm = null;$khususm = null;
        $brief = Briefing::findByHashSlug($id);
        $acquisition = Acquisition::find($brief->acquisition_id);
        $owner = (!empty($brief->company) && !empty($brief->company->owners)) ? $brief->company->owners: null;
        $agent = (!empty($brief->company) && !empty($brief->company->agents)) ? $brief->company->agents: null;
        
        if(!empty($brief) && !empty($brief->company) && !empty($brief->company->cidbgrade) && $brief->company->cidbgrade->count() > 0){
            foreach($brief->company->cidbgrade as $g){
                if($gred == null){
                    $gred = $g->grades;
                }else{
                    $gred = $gred.'<br>'.$g->grades;
                }
            }
        }
        if(!empty($brief) && !empty($brief->company) && !empty($brief->company->cidbcategory) && $brief->company->cidbcategory->count() > 0){
            foreach($brief->company->cidbcategory as $c){
                if($cat == null){
                    $cat = $c->categories;
                }else{
                    $cat = $cat.'<br>'.$c->categories;
                }
                if(!empty($c->cidbspecial) && $c->cidbspecial->count() > 0){
                    foreach($c->cidbspecial as $k){
                        if($khusus == null){
                            $khusus = $k->khusus;
                        }else{
                            $khusus = $khusus.'<br>'.$k->khusus;
                        }
                    }
                }
            }
        }
        
        if(!empty($brief) && !empty($brief->company) && !empty($brief->company->mofcourse) && $brief->company->mofcourse->count() > 0){
            foreach($brief->company->mofcourse as $cm){
                if($catm == null){
                    $catm = $c->areas;
                }else{
                    $catm = $catm.'<br>'.$cm->areas;
                }
                if(!empty($cm->syarikatMOFKhusus) && $cm->syarikatMOFKhusus->count() > 0){
                    foreach($cm->syarikatMOFKhusus as $km){
                        if($khususm == null){
                            $khususm = $km->khusus;
                        }else{
                            $khususm = $khususm.'<br>'.$km->khusus;
                        }
                    }
                }
            }
        }
        if($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3) {
            $cidb = common()->getKelayakanMOFView($acquisition->approval->id);
        }else{
            $cidb = common()->getKelayakanCIDBView($acquisition->approval->id);
        }
        return view('contract.pre.briefing.partials.forms.edit', compact('brief','acquisition', 'cidb','gred','cat','khusus','owner','agent','catm','khususm'));
    }
    
    public function listing($id)
    {
        $acquisition    = Acquisition::withDetails()->findByHashSlug($id);
        $date_sale_end  = (!empty($acquisition) && !empty($acquisition->closed_sale_at))? Carbon::createFromFormat('Y-m-d H:i:s', $acquisition->closed_sale_at)->format('Y-m-d'):null;
        $curr_date      = Carbon::now()->format('Y-m-d');
        $show_add       = false;
        if($curr_date <= $date_sale_end){
            $show_add = true;
        }
        
        if($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3) {
            $cidb = common()->getKelayakanMOFView($acquisition->approval->id);
        }else{
            $cidb = common()->getKelayakanCIDBView($acquisition->approval->id);
        }
        
        return view('contract.pre.briefing.edit', compact('acquisition','cidb','show_add'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ('on' == $request->special_permission_status) {
            $special_permission_status = 1;
        } else {
            $special_permission_status = 2;
        }
        
        if($request->attendant_status == 1){
            $company_owner_id = $request->company_owner_id;
        }elseif($request->attendant_status == 2){
            $company_owner_id = $request->company_agent_id;
        }

        $results = Briefing::findByHashSlug($id);
        if (isset($results->id)) {
            if (2 == $request->attendant_status && isset($request->new_agent) && '' != $request->company_agent_name && '' != $request->company_agent_ic) {
                $approval = Representative::create([
                    'company_id'    => $request->company_id,
                    'name'          => $request->company_agent_name,
                    'ic_number'     => $request->company_agent_ic,
                    'telephone_num' => $request->company_agent_phone,
                ]);
                $company_owner_id = $approval->id;
            }
            
            if($request->status == 3){
                $status = $request->status;
            }else{
                $status = $request->attendant_status;
            }
            Briefing::findByHashSlug($id)->update([
                'acquisition_id'            => $request->acquisition_id,
                'user_id'                   => user()->id,
                'company_id'                => $request->company_id,
                'company_owner_id'          => $company_owner_id,
                'attendant_status'          => $status,
                'special_permission_status' => $special_permission_status,
                'permission_remark'         => $request->permission_remark,
                'site_visit'                => 2,
                'status_remark'             => $request->status_remark,
            ]);
            $check_visit = Acquisition::find($request->acquisition_id);
            $company          = Company::withDetails()->find($request->company_id);

            audit($results, __('Kehadiran Taklimat oleh ' .$company->company_name. ' bagi perolehan ' .$check_visit->reference. ' : ' .$check_visit->title. ' telah berjaya dikemaskini.'));
            swal()->success('Kemaskini Taklimat', 'Rekod telah berjaya disimpan.', []);
        } else {
            swal()->error('Kemaskini Taklimat', 'Tiada data bagi id ini', []);
        }
        
        return redirect()->route('briefing.listing', [$request->acq_hash]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
