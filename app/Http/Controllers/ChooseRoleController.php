<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;


class ChooseRoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {    
        $roles = auth()->user()->roles;      
         
        $countRoles = count($roles);
        if($countRoles > 0){
            $user = auth()->user();   
            $user->current_role_login = $roles[0]->name;  
            $user->update(); 
        }     
        return redirect()->route('home');
    }
    
     public function chooseRole()
    {
       $roles = auth()->user()->roles;
       return view('chooseRole', compact('roles'));
    }
    
     public function update(Request $request)
    {
         //dd($request->input('selected_role'));      
         //session put ROLE - need to set into table ,      
         //update into users, 
         $user = auth()->user();     
         $user->current_role_login = $request->input('selected_role');  
         $user->update();       
         return redirect()->route('home');
        
    }
}
