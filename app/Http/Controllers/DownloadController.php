<?php

namespace App\Http\Controllers;

class DownloadController extends Controller
{
    public function downloadFile($approval = '', $acquisition = '', $type = '', $file = '')
    {
        return response()->file(storage_path('app/public/' . $approval . '/' . $acquisition . '/' . $type . '/' . $file));
    }

    public function downloadVoFile($approval = '', $acquisition = '', $module = '', $no = '', $type = '', $file = '')
    {
        return response()->file(storage_path('app/public/' . $approval . '/' . $acquisition . '/' . $module . '/' . $no . '/' . $type . '/' . $file));
    }
    
    public function downloadIpcFile($approval = '', $acquisition = '', $module = '', $no = '', $file = '')
    {
        return response()->file(storage_path('app/public/' . $approval . '/' . $acquisition . '/' . $module . '/' . $no . '/' . $file));
    }
    
    //Download file for pemantauan , acquisitionActivitySample , acquisitionProgressSample
    public function acquisitionActivitySample()
    {
       return response()->download('template_file/acquisition-activitiy-template_sample.csv');
    }
    
    public function acquisitionProgressSample()
    {
         return response()->download('template_file/acquisition-progress-template_sample.csv');
    }
    
     public function downloadStorage( $folder = '', $file = '')
    {
        return response()->file(storage_path('app/public/' . $folder . '/'. $file));
    }
    
}
