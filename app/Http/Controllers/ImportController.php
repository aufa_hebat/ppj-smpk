<?php

namespace App\Http\Controllers;

use App\Http\Requests\CsvImportRequest;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\CSV\Preview;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Acquisition\Acquisition;
use App\Models\WorkflowTask;
use Illuminate\Support\Collection;


class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function parse(CsvImportRequest $request)
    {
        DB::transaction(function () use($request) {
            $path = $request->file('csv_file')->getRealPath();
        
            if ($request->has('header')) {
            
            
                //delete previous bq
                SubItem::where('acquisition_id', '=', $request->acquisition_id)->delete();
                Item::where('acquisition_id', '=', $request->acquisition_id)->delete();
                Element::where('acquisition_id', '=', $request->acquisition_id)->delete();
                //delete previous bq
                $elemen = [];
                $item   = [];
                $sub    = [];
            
                $row = 1;
                if (($handle = fopen($path, "r+")) !== FALSE) {
                    while (($data = fgetcsv($handle, ",")) !== FALSE) {
                        $data = array_map("utf8_encode", $data); //added
                        if($row == 1){ $row++; continue; }
                        $num = count($data);//for column yg ada value
                        //for ($c=0; $c < $num; $c++) {
                        //0 => no elemen, 1 => no item, 2 => no subitem, 3 => keterangan, 4 => huraian, 5=>unit, 6 => kuantiti, 7 => harga/unit, 8 => jumlah 
                        for ($c=0; $c < 1; $c++) {
                            if ('0' != $data[0] && '0' == $data[1] && '0' == $data[2]) {
                                array_push($elemen, [
                                    'no'               => $data[0],
                                    'element'          => strtoupper($data[3]),
                                    'description'      => $data[4],
                                    'amount'           => (!empty($data[8]) && $data[8] != 0 && $data[8] != '')? money()->toMachine(preg_replace('/[,]/', '', $data[8])):0,
                                ]);
                            }elseif ('0' != $data[0] && '0' != $data[1] && '0' == $data[2]) {
                                array_push($item, [
                                    'bq_no_element'      => $data[0],
                                    'no'                 => $data[1],
                                    'item'               => strtoupper($data[3]),
                                    'description'        => $data[4],
                                    'amount'             => (!empty($data[8]) && $data[8] != 0 && $data[8] != '')? money()->toMachine(preg_replace('/[,]/', '', $data[8])):0,
                                ]);
                            }elseif ('0' != $data[0] && '0' != $data[1] && '0' != $data[2]) {
                                array_push($sub, [
                                    'bq_no_element'       => $data[0],
                                    'bq_no_item'          => $data[1],
                                    'no'                  => $data[2],
                                    'item'                => $data[3],
                                    'description'         => $data[4],
                                    'unit'                => $data[5],
                                    'quantity'            => (!empty($data[6]) && $data[6] != 0 && $data[6] != '')? money()->toMachine(floor(preg_replace('/[,]/', '', $data[6]))):0,
                                    'baki_quantity'       => (!empty($data[6]) && $data[6] != 0 && $data[6] != '')? money()->toMachine(floor(preg_replace('/[,]/', '', $data[6]))):0,
                                    'paid_quantity'       => 0,
                                    'rate_per_unit'       => (!empty($data[7]) && $data[7] != 0 && $data[7] != '')? money()->toMachine(preg_replace('/[,]/', '', $data[7])):0,
                                    'amount'              => (!empty($data[8]) && $data[8] != 0 && $data[8] != '')? money()->toMachine(preg_replace('/[,]/', '', $data[8])):0,
                                ]);
                            }

                        }
                    }
                    fclose($handle);
                }
            
                if(!empty($elemen)){
                    foreach($elemen as $el){
                        Element::create([
                            'acquisition_id'   => $request->acquisition_id,
                            'status'           => "A",
                            'created_by'       => user()->id,
                            'no'               => $el["no"],
                            'element'          => $el["element"],
                            'description'      => $el["description"],
                            'amount'           => $el["amount"],
                        ]);
                    }
                }
            
                if(!empty($item)){
                    foreach($item as $itm){
                        $temp_el = Element::where('acquisition_id',$request->acquisition_id)->where('no',$itm["bq_no_element"])->where('deleted_at',null)->first();
                        Item::create([
                            'bq_element_id'      => $temp_el->id ?? null,
                            'acquisition_id'     => $request->acquisition_id,
                            'status'             => "A",
                            'created_by'         => user()->id,
                            'bq_no_element'      => $itm["bq_no_element"],
                            'no'                 => $itm["no"],
                            'item'               => $itm["item"],
                            'description'        => $itm["description"],
                            'amount'             => $itm["amount"],
                        ]);
                    
                    }
                }
            
                if(!empty($sub)){
                    foreach($sub as $sb){
                        $temp_el = Element::where('acquisition_id',$request->acquisition_id)->where('no',$sb["bq_no_element"])->where('deleted_at',null)->first();
                        $temp_it = Item::where('acquisition_id',$request->acquisition_id)->where('bq_no_element',$sb["bq_no_element"])->where('no',$sb["bq_no_item"])->where('deleted_at',null)->first();
                        SubItem::create([
                            'bq_element_id'       => $temp_el->id ?? null,
                            'bq_item_id'          => $temp_it->id ?? null,
                            'acquisition_id'      => $request->acquisition_id,
                            'status'              => "A",
                            'created_by'          => user()->id,
                            'bq_no_element'       => $sb["bq_no_element"],
                            'bq_no_item'          => $sb["bq_no_item"],
                            'no'                  => $sb["no"],
                            'item'                => $sb["item"],
                            'description'         => $sb["description"],
                            'unit'                => $sb["unit"],
                            'quantity'            => $sb["quantity"],
                            'baki_quantity'       => $sb["baki_quantity"],
                            'paid_quantity'       => $sb["paid_quantity"],
                            'rate_per_unit'       => $sb["rate_per_unit"],
                            'amount'              => $sb["amount"],
                        ]);
                    
                    }
                }
            } else {
                return redirect()->back()->withInput(['tab'=>'#document-quantity']);
            }
            Preview::create([
                'file'   => $request->file('csv_file')->getClientOriginalName(),
                'header' => $request->has('header'),
                'data'   => json_encode($data),
            ]);
        

            $acq = Acquisition::find($request->acquisition_id);
        
            $workflow = WorkflowTask::where('acquisition_id', $request->acquisition_id)->first();
            $workflow->acquisition_id = $acq->id;
            $workflow->flow_name = $acq->reference . ' : ' . $acq->title;
            $workflow->flow_desc = 'Dokumen Perolehan : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->flow_location = 'Dokumen Perolehan';
            $workflow->is_claimed = 1;
            $workflow->user_id = $acq->user_id;
            $workflow->role_id = 4;
            $workflow->workflow_team_id = 1;
            $workflow->url = '/acquisition/document/'.$acq->hashslug.'/edit#document-quantity';
            $workflow->update();

            audit($acq, __('Senarai Kuantiti dokumen perolehan ' .$acq->reference. ' : ' .$acq->title. ' telah berjaya dikemaskini.'));

        }, 10);
        
        return back()->withInput(['tab'=>'#document-quantity']);
    }
}
