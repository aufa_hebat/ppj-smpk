<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Review;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //echo    "<script type='text/javascript'>$( document ).ready(function() { $('.se-pre-con').fadeOut('slow');;});</script>";
        //echo " <div class='se-pre-con'></div>";
        $this->middleware('auth');
        
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
      

        $s6 = Review::where('status', 'S6')->where('progress', 0)->orderby('id', 'desc')->get();

        return view('home', compact('s6', 'review'));
    }
}
