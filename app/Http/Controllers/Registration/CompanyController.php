<?php

namespace App\Http\Controllers\Registration;

use App\Http\Controllers\Controller;
use App\Models\Company\BPKU;
use App\Models\Company\Bumiputra;
use App\Models\Company\CIDB;
use App\Models\Company\Company;
use App\Models\Company\KDN;
use App\Models\Company\MOF;
use App\Models\Company\SPKK;
use App\Models\Company\SSM;
use App\Models\Document;
use Illuminate\Support\Carbon;
use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\EmailScheduler;
use App\Models\StandardCode;
use Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $syarikat     = Company::get();
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        // $std_cd_bank  = standard_code('BankAccount');
        $std_cd_bank  = Bank::get();

        return view('manage.registration.company.show', compact('syarikat', 'std_cd_state', 'std_cd_bank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company     = Company::get();
        // $std_cd_state = standard_code('KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia');
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        // $std_cd_bank  = standard_code('BankAccount');
        $std_cd_bank  = Bank::get();

        return view('manage.registration.company.create', compact('company', 'std_cd_state', 'std_cd_bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ssm_no'       => 'required||unique:companies,ssm_no|max:20',
            'company_name' => 'required|unique:companies,company_name',
            'primary' => 'required',
            'secondary' => 'required',
            'postcode' => 'required|max:5',
            'state' => 'required',
            'email' => 'required|email',
            'gst' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
        ]);

        $company = Company::create([
            'ssm_no' => strtoupper($request->ssm_no), 
            'company_name' => str_replace("'", '`', $request->company_name), 
            'email' =>  str_replace("'", '`', $request->email), 
            'gst_registered_no' => strtoupper($request->gst_registered_no),
            'gst_start_date' => $request->input('gst_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('gst_start_date')) : null, 
            'account_bank_no' => $request->account_bank_no, 
            'bank_name' => $request->bank_name, 
            'status_id' => $request->status_id ,
        ]);

        $company->addresses()->create($request->only('primary', 'secondary', 'postcode', 'state'));

        foreach ($request->only('phone_number') as $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                if (! empty($phone_numbers)) {
                    $company->phones()->create(['phone_type_id' => $key, 'phone_number' => $pn]);
                } else {
                    $company->phones()->create(['phone_type_id' => $key, 'phone_number' => '-']);
                }
            }
        }

        if (! empty($request->gst)) {
            $gst_files = Input::file('gst');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $gst_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'company'; // upload folder in public directory
                $NamaDokumen     = strtoupper($request->ssm_no) . '_gst_' . str_replace("&", '', $gst_files->getClientOriginalName());
                $upload_success  = $gst_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $gst_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $company->id;
                $entry->document_types = 'GST';
                $entry->mime           = $gst_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $gst_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }

        return response()->json(['hashslug' => $company->hashslug ?? null, 'message' => __('Rekod telah berjaya disimpan.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('manage.registration.company.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findByHashSlug($id);

        $hashslug = $company->hashslug;
        $owner    = Company::findByHashSlug($hashslug);

        $ssm1         = SSM::findByHashSlug($id);
        $cidb1        = CIDB::findByHashSlug($id);
        $bpku1        = BPKU::findByHashSlug($id);
        $spkk1        = SPKK::findByHashSlug($id);
        $mof1         = MOF::findByHashSlug($id);
        $bumiputra1   = Bumiputra::findByHashSlug($id);
        $kdn1         = KDN::findByHashSlug($id);
        $std_cd_states = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $ssm_doc      = Document::where('document_id', $company->id)->where('document_types', 'SSM')->orderby('id', 'desc')->limit(1)->get();
        $cidb_doc     = Document::where('document_id', $company->id)->where('document_types', 'CIDB')->orderby('id', 'desc')->limit(1)->get();
        $bpku_doc     = Document::where('document_id', $company->id)->where('document_types', 'BPKU')->orderby('id', 'desc')->limit(1)->get();
        $spkk_doc     = Document::where('document_id', $company->id)->where('document_types', 'SPKK')->orderby('id', 'desc')->limit(1)->get();
        $mof_doc      = Document::where('document_id', $company->id)->where('document_types', 'MOF')->orderby('id', 'desc')->limit(1)->get();
        $bumi_doc     = Document::where('document_id', $company->id)->where('document_types', 'Bumiputra')->orderby('id', 'desc')->limit(1)->get();
        $kdn_doc      = Document::where('document_id', $company->id)->where('document_types', 'KDN')->orderby('id', 'desc')->limit(1)->get();
        $gred         = cidb_codes('grade');
        $kategori     = cidb_codes('category');
        $pengkhususan = cidb_codes('khusus');
        $db1          = standard_code('MOF_Bidang');
        $db2          = standard_code('MOF_Khusus_10000');
        $db3          = standard_code('MOF_Khusus_20000');
        $db4          = standard_code('MOF_Khusus_30000');
        $db5          = standard_code('MOF_Khusus_40000');
        $db6          = standard_code('MOF_Khusus_50000');
        $db7          = standard_code('MOF_Khusus_60000');
        $db8          = standard_code('MOF_Khusus_70000');
        $db9          = standard_code('MOF_Khusus_80000');
        $db10         = standard_code('MOF_Khusus_90000');
        $db11         = standard_code('MOF_Khusus_100000');
        $db12         = standard_code('MOF_Khusus_210000');
        $db13         = standard_code('MOF_Khusus_220000');
        $kategori_mof       = mof_codes('category');
        $mof_khusus    = mof_codes('khusus');

        if (! is_array($company->addresses)) {
            $address['primary']   = '';
            $address['secondary'] = '';
            $address['postcode']  = '';
            $address['state']     = '';
        } else {
            $address['primary']   = $company->addresses[0]->primary;
            $address['secondary'] = $company->addresses[0]->secondary;
            $address['postcode']  = $company->addresses[0]->postcode;
            $address['state']     = $company->addresses[0]->state;
        }

        if (! is_array($company->phones)) {
            $phone['office'] = '';
            $phone['mobile'] = '';
            $phone['fax']    = '';
        } else {
            $phone['office'] = $company->phones[0]->phone_number;
            $phone['mobile'] = $company->phones[1]->phone_number;
            $phone['fax']    = $company->phones[2]->phone_number;
        }

        $data = [
            'company' => $company,
            'address' => $address,
            'phone'   => $phone,
        ];

        // $std_cd_bank  = standard_code('BankAccount');
        $std_cd_bank  = Bank::get();
        $gst_doc      = Document::where('document_id', $company->id)->where('document_types', 'GST')->orderby('id', 'desc')->limit(1)->get();

        return view('manage.registration.company.edit', compact('company', 'std_cd_state', 'std_cd_states', 'std_cd_bank', 'gst_doc', 'hashslug', 'owner', 'ssm1', 'cidb1', 'bpku1', 'spkk1', 'mof1', 'bumiputra1', 'kdn1', 'ssm_doc', 'cidb_doc', 'bpku_doc', 'spkk_doc', 'mof_doc', 'bumi_doc', 'kdn_doc', 'gred', 'kategori', 'pengkhususan', 'db1', 'db2', 'db3', 'db4', 'db5', 'db6', 'db7', 'db8', 'db9', 'db10', 'db11', 'db12', 'db13', 'kategori_mof', 'mof_khusus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ssm_no'       => 'required|max:20',
            'company_name' => 'required',
            'primary' => 'required',
            'secondary' => 'required',
            'postcode' => 'required|max:5',
            'state' => 'required',
            'phone_number3' => 'required',
            'phone_number2' => 'required',
            'email' => 'required|email',
            'gst' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:2048',
        ]);
        
        $company = Company::find($id);

        if(empty($company->ssm_no)){
            audit($company, __($request->company_name . ' telah berjaya disimpan.'));
        } else {
            audit($company, __($request->company_name . ' telah berjaya dikemaskini.'));
        }

        Company::find($id)->update([
            'ssm_no' => strtoupper($request->ssm_no), 
            'company_name' => str_replace("'", '`', $request->company_name), 
            'email' =>  str_replace("'", '`', $request->email), 
            'gst_registered_no' => strtoupper($request->gst_registered_no), 
            'gst_start_date' =>  $request->input('gst_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('gst_start_date')) : null,
            'account_bank_no' => $request->account_bank_no, 
            'bank_name' => $request->bank_name, 
            'status_id' => $request->status_id ,
        ]);

        $address = Company::find($id)->addresses();
        if($address->count() > 0){
            Company::find($id)->addresses()->update([
                'primary' => str_replace("'", '`', $request->primary), 
                'secondary' => str_replace("'", '`', $request->secondary), 
                'postcode' => $request->postcode, 
                'state' => $request->state,
            ]);    
        }else{
            Company::find($id)->addresses()->create([
                'primary' => str_replace("'", '`', $request->primary), 
                'secondary' => str_replace("'", '`', $request->secondary), 
                'postcode' => $request->postcode, 
                'state' => $request->state,
            ]);
        }
        
        if (! empty($company->phones[0])) {
            $company->phones[0]->phone_number = $request->input('phone_number3');
            $company->phones[0]->update();
        }
        if (! empty($company->phones[1])) {
            $company->phones[1]->phone_number = $request->input('phone_number2');
            $company->phones[1]->update();
        }
        if (! empty($company->phones[2])) {
            $company->phones[2]->phone_number = $request->input('phone_number4');
            $company->phones[2]->update();
        }

        if (! empty($request->gst)) {
            $gst_files = Input::file('gst');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt,jpeg,jpg,png|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $gst_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'company'; // upload folder in public directory
                $NamaDokumen     = strtoupper($request->ssm_no) . '_gst_' . str_replace("&", '', $gst_files->getClientOriginalName());
                $upload_success  = $gst_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $gst_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $id;
                $entry->document_types = 'GST';
                $entry->mime           = $gst_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $gst_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }

        if(1 == $request->confirmation){
            $company->upload_file_status = 'Submitted';
            $company->update();

            $email          = new EmailScheduler();
            $email->status  = 'NEW';
            $email->to      = $company->email;
            $email->counter = 1;
            $email->subject = 'Sistem COINS Perbadanan Putrajaya (PPj) - Pendaftaran Syarikat';
            $dept           = '-';
            $email->text    = 
            'Tahniah,<br><br>Syarikat anda telah berjaya didaftarkan ke dalam sistem COINS Perbadanan Putrajaya (PPj).  
            <br>Berikut adalah ringkasan maklumat yang telah didaftarkan : 
            <br>Nama Syarikat : '.$company->company_name.'
            <br>Nombor Pendaftaran Syarikat : '.$company->ssm_no.'
            <br>Alamat Syarikat : '.$company->addresses[0]->primary. ', ' .$company->addresses[0]->secondary. ', ' .$company->addresses[0]->postcode. ' ' .$company->addresses[0]->state.', Malaysia. 
            <br>Email : '.$company->email.'
            <br>No Telefon : '.$company->phones[0]->phone_number.'. <br><br><br> Sekian, Terima Kasih. <br><br><br>
            Sebarang perubahan atau kemaskini butiran syarikat boleh dibuat di Kaunter Jualan, Bahagian Perolehan & Ukur Bahan, Blok C, Aras 1, Kompleks Perbadanan Putrajaya, 24, Persiaran Perdana, Presint 3, 62675, Wilayah Persekutuan Putrajaya.';
            $email->save();

            swal()->success('Kemaskini Syarikat', 'Rekod telah berjaya dikemaskini.', []);
            return redirect(route('welcome'));
        }

        swal()->success('Kemaskini Syarikat', 'Rekod telah berjaya dikemaskini.', []);

        return back()->withInput(['tab'=>'company']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
    
    public function delete($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
}
