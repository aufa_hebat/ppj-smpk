<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Briefing;
use Illuminate\Http\Request;

class SiteVisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.pre.site-visit.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $acquisition = Acquisition::withDetails()->findByHashSlug($request->id);

        return view('contract.pre.site-visit.create', compact('acquisition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->company_id as $company_id) {
            $approval = Briefing::where('acquisition_id', $request->acquisition_id)
                ->where('company_id', $company_id)
                ->update([
                    'visit_staff_id' => user()->id,
                    'site_visit'     => 1,
                ]);
        }

        swal()->success('Kehadiran Lawatan Tapak', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('site-visit.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acquisition      = Acquisition::withDetails()->findByHashSlug($id);
        $unsorted_company = [];
        foreach ($acquisition->briefings as $briefing) {
            if (3 != $briefing->attendant_status && 1 == $briefing->site_visit) {
                $unsorted_company[] = $briefing->company->company_name;
            }
        }

        $sorted_company = array_sort($unsorted_company);

        return view('contract.pre.site-visit.show', compact('acquisition', 'sorted_company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $acquisition = Acquisition::withDetails()->findByHashSlug($id);

        return view('contract.pre.site-visit.edit', compact('acquisition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
