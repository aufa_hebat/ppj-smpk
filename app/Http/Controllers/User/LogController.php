<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Log;

class LogController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function __invoke()
    {
        // $logs = auth()->user()->activity->sortByDesc('created_at');
        $logs = Log::where('causer_id',user()->id)->orderBy('created_at','desc')->get();

        return view('users.log', compact('logs'));
    }
}
