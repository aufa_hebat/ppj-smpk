<?php

namespace App\Http\Controllers;

use App\Models\Company\Owner;
use App\Models\StandardCode;
use Illuminate\Http\Request;

class PendaftaranPemilikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $owner = Owner::create($request->only('company_id', 'name', 'ic_number', 'email', 'cidb_cert', 'mof_cert', 'kdn_cert'));

        $owner->addresses()->create($request->only('primary', 'secondary', 'postcode', 'state'));

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                $owner->phones()->create(['phone_type_id' => $key, 'phone_number' => $pn]);
            }
        }

        // swal()->success('Pemilik Syarikat', 'Rekod telah berjaya disimpan.', []);

        return redirect()->route('PendaftaranSyarikat.edit', $request->hashslug);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $owner        = Owner::findByHashSlug($id);
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();

        return view('owner.show', compact('owner', 'std_cd_state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $owner        = Owner::findByHashSlug($id);
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();

        return view('owner.edit', compact('owner', 'std_cd_state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Owner::find($id)->update($request->only('name', 'ic_number', 'email', 'cidb_cert', 'mof_cert', 'kdn_cert'));

        $address = Owner::find($id)->addresses();
        if($address->count() > 0){
            Owner::find($id)->addresses()->update($request->only('primary', 'secondary', 'postcode', 'state'));
        }else{
            Owner::find($id)->addresses()->create($request->only('primary', 'secondary', 'postcode', 'state'));
        }
        

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                Owner::find($id)->phones()->update(['phone_type_id' => $key, 'phone_number' => $pn]);
            }
        }

        swal()->success('Pemilik Syarikat', 'Rekod telah berjaya disimpan.', []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
