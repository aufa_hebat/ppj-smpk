<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Models\Acquisition\Review;
use App\Models\Acquisition\Acquisition;
use App\Models\ReviewLog;
use App\Models\VO\PPJHK\Ppjhk;
use App\Models\VO\VO;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\Ipc;
use Illuminate\Support\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    //claimed
    public function claim(Request $request)
    {
        try {
            $id = $request->input('id');
            //find task by id,then set isClaimed = true and set userId = loginId
            $currentUserId = user()->id;
            $isClaimed     = true;

            $workflowTask = \App\Models\WorkflowTask::find($id);

            if (user()->currentRole() == 'pengesah') {
                // return 'ddd';
                if($workflowTask->flow_location == 'PPK'){
                    $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('ppk_id',$workflowTask->ppk_id)->where('document_contract_type','PPK')->where('status','S5')->orderby('id','desc')->first();
                    
                    $review->approved_by = $currentUserId;

                    $workflowTask->role_id = 3;

                    $reviewlog = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ppk_id',$workflowTask->ppk_id)->where('document_contract_type','PPK')->where('status','S5')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id','desc')->first();
                    $reviewlog->approved_by = $currentUserId;

                    $reviewlog1 = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ppk_id',$workflowTask->ppk_id)->where('document_contract_type','PPK')->where('status','S5')->where('reviews_status','Terima')->orderby('id','desc')->first();
                    $reviewlog1->approved_by = $currentUserId;
                    $reviewlog1->requested_by = $currentUserId;

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $ppk = VO::find($workflowTask->ppk_id);

                    audit($review, __('Semakan PPK ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . $review->approve->name));


                } elseif($workflowTask->flow_location == 'PPJHK'){
                    $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('ppjhk_id',$workflowTask->ppjhk_id)->where('document_contract_type','PPJHK')->where('status','S5')->orderby('id','desc')->first();
                    
                    $review->approved_by = $currentUserId;

                    $workflowTask->role_id = 3;

                    $reviewlog = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ppjhk_id',$workflowTask->ppjhk_id)->where('document_contract_type','PPJHK')->where('status','S5')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id','desc')->first();
                    $reviewlog->approved_by = $currentUserId;

                    $reviewlog1 = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ppjhk_id',$workflowTask->ppjhk_id)->where('document_contract_type','PPJHK')->where('status','S5')->where('reviews_status','Terima')->orderby('id','desc')->first();
                    $reviewlog1->approved_by = $currentUserId;
                    $reviewlog1->requested_by = $currentUserId;

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $ppjhk = PPJHK::find($workflowTask->ppjhk_id);

                    audit($review, __('Semakan PPJHK ' .$ppjhk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . $review->approve->name));

                } else {
                    $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('ipc_id',$workflowTask->ipc_id)->where('type','IPC')->where('status','S5')->orderby('id','desc')->first();
                    
                    $review->approved_by = $currentUserId;

                    $workflowTask->role_id = 3;

                    $reviewlog = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ipc_id',$workflowTask->ipc_id)->where('type','IPC')->where('status','S5')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id','desc')->first();
                    $reviewlog->approved_by = $currentUserId;

                    $reviewlog1 = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ipc_id',$workflowTask->ipc_id)->where('type','IPC')->where('status','S5')->where('reviews_status','Terima')->orderby('id','desc')->first();
                    $reviewlog1->approved_by = $currentUserId; 
                    $reviewlog1->requested_by = $currentUserId;

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $ipc = Ipc::find($workflowTask->ipc_id);

                    audit($review, __('Semakan IPC '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . $review->approve->name));

                }

                $review->update();
                $reviewlog->update();
                $reviewlog1->update();
            } elseif(user()->currentRole() == 'penyemak' && (user()->position_id == '67' || user()->position_id == '2052' || user()->position_id == '2032') && user()->scheme_id == '21' && (user()->grade_id == '11' || user()->grade_id == '12' || user()->grade_id == '13' || user()->grade_id == '14' || user()->grade_id == '15' || user()->grade_id == '16')) {
                $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('ipc_id',$workflowTask->ipc_id)->where('type','IPC')->where('status','S8')->orderby('id','desc')->first();
                
                $review->approved_by = $currentUserId;

                $workflowTask->role_id = 5;

                $reviewlog = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ipc_id',$workflowTask->ipc_id)->where('type','IPC')->where('status','CNBPUB')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id','desc')->first();
                $reviewlog->approved_by = $currentUserId;

                $reviewlog1 = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('ipc_id',$workflowTask->ipc_id)->where('type','IPC')->where('status','CNBPUB')->where('reviews_status','Terima')->orderby('id','desc')->first();
                $reviewlog1->approved_by = $currentUserId;
                $reviewlog1->requested_by = $currentUserId;

                $acq = Acquisition::find($workflowTask->acquisition_id);

                $ipc = Ipc::find($workflowTask->ipc_id); 

                audit($review, __('Semakan IPC '.$ipc->ipc_no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . $review->approve->name));

                $review->update();
                $reviewlog->update();
                $reviewlog1->update();
            } elseif(user()->currentRole() == 'urusetia' && $workflowTask->flow_location == 'EOT') {
                $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('eot_id',$workflowTask->eot_id)->where('type','EOT')->where('status','N')->orderby('id','desc')->first();
                
                $review->approved_by = $currentUserId;

                $workflowTask->role_id = 6;

                $acq = Acquisition::find($workflowTask->acquisition_id);

                $eot = Eot::find($workflowTask->eot_id);

                if($acq->approval->acquisition_type_id == 2){

                    $reviewlog = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('eot_id',$workflowTask->eot_id)->where('type','EOT')->where('status','CNPP')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id','desc')->first();

                    $reviewlog1 = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('eot_id',$workflowTask->eot_id)->where('type','EOT')->where('status','CNPP')->where('reviews_status','Terima')->orderby('id','desc')->first();                            

                    audit($review, __('Semakan '.$review->type.' '.$eot->bil. ' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . $review->approve->name));

                } else {  

                    $reviewlog = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('eot_id',$workflowTask->eot_id)->where('type','EOT')->where('status','CNBPUB')->where('reviews_status','!=','Terima')->where('reviews_status','!=','Baru')->where('reviews_status','!=','Selesai Semakan')->orderby('id','desc')->first();

                    $reviewlog1 = ReviewLog::where('acquisition_id',$workflowTask->acquisition_id)->where('eot_id',$workflowTask->eot_id)->where('type','EOT')->where('status','CNBPUB')->where('reviews_status','Terima')->orderby('id','desc')->first();
                                          
                    audit($review, __('Semakan '.$review->type.' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . $review->approve->name));
                                            
                }
                
                $reviewlog->approved_by = $currentUserId;

                $reviewlog1->approved_by = $currentUserId;
                $reviewlog1->requested_by = $currentUserId;

                $review->update();
                $reviewlog->update();
                $reviewlog1->update();
            } elseif(user()->currentRole() == 'urusetiavo' ) {

                $acq = Acquisition::find($workflowTask->acquisition_id);

                $workflowTask->role_id = 12;

                $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('ppk_id',$workflowTask->ppk_id)->where('document_contract_type','PPK')->orderby('id','desc')->first();

                $ppk = VO::find($workflowTask->ppk_id);

                audit($review, __('PPK ' .$ppk->no. ' - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

            } else {
                if($workflowTask->flow_location == 'Taklimat'){

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $workflowTask->role_id = 4;
                                          
                    audit($acq, __('Taklimat ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

                } elseif($workflowTask->flow_location == 'Lawatan Tapak'){

                    $acq = Acquisition::find($workflowTask->acquisition_id);
                                          
                    audit($acq, __('Lawatan Tapak ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

                } elseif($workflowTask->flow_location == 'Jualan'){

                    $acq = Acquisition::find($workflowTask->acquisition_id);
                                          
                    audit($acq, __('Jualan ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

                } elseif($workflowTask->flow_location == 'Buka Peti'){

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $workflowTask->role_id = 6;
                                          
                    audit($acq, __('Buka Peti ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

                } elseif($workflowTask->flow_location == 'N'){

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $workflowTask->role_id = 6;

                    $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('status','N')->orderby('id','desc')->first();

                    $reviewlog = new ReviewLog();
                    $reviewlog->acquisition_id = $acq->id;
                    $reviewlog->status       = 'N';
                    $reviewlog->progress     = 1;
                    $reviewlog->approved_at  = Carbon::now();
                    $reviewlog->department   = config('enums.BPUB');
                    $reviewlog->created_by   = $review->created_by;
                    $reviewlog->requested_at  = $review->approved_at;
                    $reviewlog->requested_by  = user()->id;
                    $reviewlog->type  = 'Acquisitions';
                    $reviewlog->reviews_status = 'Terima';
                    $reviewlog->save();

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

                } elseif($workflowTask->flow_location == 'S6'){

                    $acq = Acquisition::find($workflowTask->acquisition_id);

                    $workflowTask->role_id = 5;

                    $review = Review::where('acquisition_id',$workflowTask->acquisition_id)->where('status','S6')->orderby('id','desc')->first();

                    $reviewlog = new ReviewLog();
                    $reviewlog->acquisition_id = $acq->id;
                    $reviewlog->status       = 'S6';
                    $reviewlog->progress     = 1;
                    $reviewlog->approved_at  = Carbon::now();
                    $reviewlog->department   = config('enums.UU');
                    $reviewlog->created_by   = $review->created_by;
                    $reviewlog->requested_at  = $review->approved_at;
                    $reviewlog->requested_by  = user()->id;
                    $reviewlog->type  = 'Acquisitions';
                    $reviewlog->reviews_status = 'Terima';
                    $reviewlog->save();

                    audit($review, __('Semakan Dokumen Perolehan - ' .$acq->reference. ' : ' .$acq->title. ' Diambil oleh ' . user()->name));

                } else {

                }

            }

            $workflowTask->update([
                'is_claimed' => $isClaimed,
                'user_id'    => $currentUserId,
                'role_id'    => $workflowTask->role_id,
            ]);
            //mock exception, to try exception
            //$s = 1+'a';
            return 'success';
        } catch (Exception $e) {
            return 'failed';
        }
    }
}
