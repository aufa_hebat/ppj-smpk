<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
   

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //DIN - add one more page that can choose ROLE than put into session
    //protected $redirectTo = '/home';
    protected $redirectTo = '/lasdjlaksdjasdj';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
     
    // override class AuthenticatesUsers
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            //'password' => 'required|string'
        ]);
    }
 
    protected function credentials(Request $request)
    {
        //return array_merge($request->only($this->username()));
        //return array_merge($request->only('email'));
        
        if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $modifiedEmail = $request->get('email');
        }else{
            $modifiedEmail = $request->get('email').'@ppj.gov.my';  
        }
               
        //TODO - add default current login based on priority role
        //dd('Test');
        
               
        return ['email'=>$modifiedEmail,'password'=>$request->get('password')];      
       //return $request->only('email', 'password');
       //return $request->only('email');     
    }
    
    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        //dd('hi');
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        //TODO set current role
        //dd($this->guard()->user()->roles);
        $roles = auth()->user()->roles;   
        //dd($roles);
        $rolesArray = $roles->pluck('name')->toArray();
         
        $countRoles = count($roles);
        if($countRoles > 0){
            $user = auth()->user();   
            
           // in_array("100", $marks)
            if(in_array("pengesah", $rolesArray)){
                 $user->current_role_login = 'pengesah';  
            }elseif(in_array("penyemak", $rolesArray)){
                 $user->current_role_login = 'penyemak'; 
            }elseif(in_array("penyedia", $rolesArray)){
                 $user->current_role_login = 'penyedia'; 
            }
            elseif(in_array("urusetia", $rolesArray)){
                 $user->current_role_login = 'urusetia'; 
            }
            elseif(in_array("urusetiavo", $rolesArray)){
                 $user->current_role_login = 'urusetiavo'; 
            }elseif(in_array("Kaunter", $rolesArray)){
                 $user->current_role_login = 'Kaunter'; 
            }elseif(in_array("administrator", $rolesArray)){
                 $user->current_role_login = 'administrator'; 
            }
            else{
                //nothing
            }
            
           //dd($user->current_role_login);
            
            $user->update(); 
            
            //dd($user->current_role_login);
            
        }     
        
        return redirect()->route('home');
        /*
        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
          */
         
    }
   
}
