<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Acquisition;
use App\Models\Document;
use App\Models\StandardCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\WorkflowTask;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.pre.box.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acquisition = Acquisition::withDetails()->findByHashSlug($id);
        $bjh_doc     = Document::where('document_id', $acquisition->id)->where('document_types', 'BJH')->orderby('id', 'desc')->limit(1)->get();

        return view('contract.pre.box.show', compact('acquisition', 'bjh_doc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $acquisition     = Acquisition::where('hashslug', $id)->first();
        $listHargaBersih = StandardCode::where('code', 'HARGA_BERSIH')->get();
        $bjh_doc         = Document::where('document_id', $acquisition->id)->where('document_types', 'BJH')->orderby('id', 'desc')->limit(1)->get();

        $data = [
            'acquisition'     => $acquisition,
            'boxes'           => $acquisition->boxes,
            'sales'           => $acquisition->sales->paginate(config('pagination.box.per_page')),
            'listHargaBersih' => $listHargaBersih,
            'bjh_doc'         => $bjh_doc,
            ];

        return view('contract.pre.box.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $acquisition = Acquisition::find($id);
        
        
        $acquisition->box_review = $request->box_review;
        
        $acquisition->new_department_estimation = ('' != $request->new_department_estimation ? money()->toMachine($request->new_department_estimation) : money()->toMachine(0));
        
        $acquisition->update();

        if ($request->hasFile('document')) {
            $box_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $acquisition,
                'doc_type'            => 'BJH',
                'doc_id'              => $acquisition->id,
            ];
            common()->upload_document($box_requirements);

            audit($acquisition, __('Keputusan Borang Jadual Harga bagi perolehan ' .$acquisition->reference. ' : ' .$acquisition->title. ' telah berjaya dimuatnaik.'));
        }

        // delete all boxes then create new
        if ($acquisition->boxes->count() > 0) {
            foreach ($acquisition->boxes as $box) {
                $box->delete();
            }
        }

        $bulkInsert = [];
        if ($request->input('cbBox')) {
            foreach ($request->input('cbBox') as $index => $id_jualan) {
                array_push($bulkInsert, [
                    'user_id'        => user()->id,
                    'acquisition_id' => $id,
                    'sale_id'        => $id_jualan,
                    'no'             => $request->input('no' . $id_jualan),
                    'period'         => $request->input('period' . $id_jualan),
                    'period_type_id' => $request->input('period_type_id' . $id_jualan),
                    'is_stated'      => $request->input('is_stated' . $id_jualan),
                    'amount'         => ($request->input('amount' . $id_jualan)) ? money()->toMachine($request->input('amount' . $id_jualan)) : null,
                ]);
            }

            $acquisition->boxes()->createMany($bulkInsert);
        }

        if(1 == $request->confirmation){
            $workflow = WorkflowTask::where('acquisition_id', $acquisition->id)->first();
            $workflow->flow_desc = 'Keputusan : Sila teruskan dengan keputusan perolehan.';
            $workflow->flow_location = 'Keputusan';
            $workflow->url = '/evaluation/'.$acquisition->hashslug.'/edit';
            $workflow->role_id = 6;
            $workflow->update();
            
            $acquisition->status_task = config('enums.flow8');
            $acquisition->update();

            audit($acquisition, __('Rekod buka peti bagi perolehan ' .$acquisition->reference. ' : ' .$acquisition->title. ' telah muktamad.'));

            swal()->success('Buka Peti', 'Rekod Muktamad.', []);

            return redirect()->route('home');
        }

        audit($acquisition, __('Buka peti bagi perolehan ' .$acquisition->reference. ' : ' .$acquisition->title. ' telah dikemaskini.'));

        swal()->success('Buka Peti', 'Rekod telah berjaya dikemaskini.', []);

        return redirect()->route('box.edit', $acquisition->hashslug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function downloadFile($file = '')
    {
        return response()->file(storage_path('app/public/' . $file));
    }
}
