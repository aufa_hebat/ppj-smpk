<?php

namespace App\Http\Controllers;

use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Evaluation;
use App\Models\Company\Company;
use App\Models\Document;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contract.pre.evaluation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acquisition = Acquisition::where('hashslug', $id)->first();

        $data = [
            'acquisition' => $acquisition,
        ];

        return view('contract.pre.evaluation.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $acquisition = Acquisition::where('hashslug', $id)->first();
        Evaluation::firstOrCreate(['acquisition_id' => $acquisition->id]);

        $companies = new Collection();
        foreach ($acquisition->boxes as $box) {
            if ($box->is_stated) {
                $company               = new Company();
                $company->id           = $box->sale->company->id;
                $company->company_name = $box->sale->company->company_name;

                $companies->add($company);
            }
        }

        $data = [
            'acquisition' => $acquisition,
            'companies'   => $companies,
        ];

        return view('contract.pre.evaluation.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
        [
            'company_id' => 'required',
        ],
        [
            'company_id.required' => __('Sila pilih syarikat'),
        ]);
        $meeting_date = $request->input('meeting_date') ? Carbon::createFromFormat('d/m/Y', $request->input('meeting_date')) : null;
        Evaluation::where('acquisition_id', $id)->update(
            [
                'meeting_no'   => $request->input('meeting_no'),
                'meeting_date' => $meeting_date,
            ]
        );

        AppointedCompany::updateOrCreate(['acquisition_id' => $id],
            [
                'company_id'    => $request->input('company_id'),
                'offered_price' => $request->input('h_offered_price'),
            ]
        );

        $acquisition = Acquisition::find($id);
        if ($request->hasfile('document')) {
            // $this->validate($request, ['document' => 'mimes:pdf,doc,docx,txt,xls,xlsx,jpeg,png,jpg']);

            // $destinationPath = 'storage'; // upload folder in public directory
            // $file            = $request->file('document');
            // $file->move($destinationPath, $file->getClientOriginalName());

            // Document::updateOrCreate(
            //     [
            //         'document_types' => 'EVALUATION',
            //         'document_id'    => Evaluation::where('acquisition_id', $id)->first()->id,
            //     ],
            //     [
            //     'document_path'     => $destinationPath,
            //         'document_name' => $file->getClientOriginalName(),
            //         'mime'          => $file->getClientMimeType(),
            // ]);

            

            $evaluation_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $acquisition,
                'doc_type'            => 'EVALUATION',
                'doc_id'              => Evaluation::where('acquisition_id', $id)->first()->id,
            ];

            // dd($evaluation_requirements);
            common()->upload_document($evaluation_requirements);

            audit($acquisition, __('Keputusan Mesyuarat bagi perolehan ' .$acquisition->reference. ' : ' .$acquisition->title. ' telah berjaya dimuatnaik.'));

        }

        if(1 == $request->confirmation){
            $company = AppointedCompany::where('acquisition_id', $acquisition->id)->first();

            $workflow = WorkflowTask::where('acquisition_id', $acquisition->id)->first();
            $workflow->flow_desc = 'Surat Setuju Terima : Sila teruskan dengan SST.';
            $workflow->flow_location = 'SST';
            $workflow->is_claimed = 1;
            $workflow->user_id = $acquisition->user_id;
            $workflow->role_id = 4;
            $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit';
            $workflow->update();

            $acquisition->status_task = config('enums.flow9');
            $acquisition->update();

            audit($acquisition, __('Rekod keputusan bagi perolehan ' .$acquisition->reference. ' : ' .$acquisition->title. ' telah muktamad.'));

            swal()->success('Keputusan Perolehan', 'Rekod Muktamad.', []);

            return redirect()->route('home');
        }

        audit($acquisition, __('Keputusan bagi perolehan ' .$acquisition->reference. ' : ' .$acquisition->title. ' telah dikemaskini.'));

        swal()->success('Keputusan Perolehan', 'Rekod telah berjaya dikemaskini.', []);

        return redirect()->route('evaluation.edit', Acquisition::find($id)->hashslug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
