<?php

namespace App\Http\Controllers;

use App\Models\Company\BPKU;
use App\Models\Company\Bumiputra;
use App\Models\Company\CIDB;
use App\Models\Company\Company;
use App\Models\Company\KDN;
use App\Models\Company\MOF;
use App\Models\Company\SPKK;
use App\Models\Company\SSM;
use App\Models\Document;
use App\Models\StandardCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class PendaftaranSyarikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $syarikat     = Company::get();
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $std_cd_bank  = standard_code('BankAccount');

        return view('company.show', compact('syarikat', 'std_cd_state', 'std_cd_bank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Company::create($request->only('ssm_no', 'company_name', 'email', 'gst_registered_no', 'account_bank_no', 'bank_name'));

        $company->addresses()->create($request->only('primary', 'secondary', 'postcode', 'state'));

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                if (! empty($phone_numbers)) {
                    $company->phones()->create(['phone_type_id' => $key, 'phone_number' => $pn]);
                } else {
                    $company->phones()->create(['phone_type_id' => $key, 'phone_number' => '-']);
                }
            }
        }

        if (! empty($request->gst)) {
            $gst_files = Input::file('gst');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $gst_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'uploads'; // upload folder in public directory
                $NamaDokumen     = $gst_files->getClientOriginalName();
                $upload_success  = $gst_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $gst_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $company->id;
                $entry->document_types = 'GST';
                $entry->mime           = $gst_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $gst_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }

        return redirect()->route('PendaftaranSyarikat.edit', $company->hashslug);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('company.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findByHashSlug($id);

        $hashslug = $company->hashslug;
        $owner    = Company::findByHashSlug($hashslug);

        $ssm1         = SSM::findByHashSlug($id);
        $cidb1        = CIDB::findByHashSlug($id);
        $bpku1        = BPKU::findByHashSlug($id);
        $spkk1        = SPKK::findByHashSlug($id);
        $mof1         = MOF::findByHashSlug($id);
        $bumiputra1   = Bumiputra::findByHashSlug($id);
        $kdn1         = KDN::findByHashSlug($id);
        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $ssm_doc      = Document::where('document_id', $company->id)->where('document_types', 'SSM')->orderby('id', 'desc')->limit(1)->get();
        $cidb_doc     = Document::where('document_id', $company->id)->where('document_types', 'CIDB')->orderby('id', 'desc')->limit(1)->get();
        $bpku_doc     = Document::where('document_id', $company->id)->where('document_types', 'BPKU')->orderby('id', 'desc')->limit(1)->get();
        $spkk_doc     = Document::where('document_id', $company->id)->where('document_types', 'SPKK')->orderby('id', 'desc')->limit(1)->get();
        $mof_doc      = Document::where('document_id', $company->id)->where('document_types', 'MOF')->orderby('id', 'desc')->limit(1)->get();
        $bumi_doc     = Document::where('document_id', $company->id)->where('document_types', 'Bumiputra')->orderby('id', 'desc')->limit(1)->get();
        $kdn_doc      = Document::where('document_id', $company->id)->where('document_types', 'KDN')->orderby('id', 'desc')->limit(1)->get();
        $gred         = cidb_codes('grade');
        $kategori     = cidb_codes('category');
        $pengkhususan = cidb_codes('khusus');
        $db1          = standard_code('MOF_Bidang');
        $db2          = standard_code('MOF_Khusus_10000');
        $db3          = standard_code('MOF_Khusus_20000');
        $db4          = standard_code('MOF_Khusus_30000');
        $db5          = standard_code('MOF_Khusus_40000');
        $db6          = standard_code('MOF_Khusus_50000');
        $db7          = standard_code('MOF_Khusus_60000');
        $db8          = standard_code('MOF_Khusus_70000');
        $db9          = standard_code('MOF_Khusus_80000');
        $db10         = standard_code('MOF_Khusus_90000');
        $db11         = standard_code('MOF_Khusus_100000');
        $db12         = standard_code('MOF_Khusus_210000');
        $db13         = standard_code('MOF_Khusus_220000');

        if (! is_array($company->addresses)) {
            $address['primary']   = '';
            $address['secondary'] = '';
            $address['postcode']  = '';
            $address['state']     = '';
        } else {
            $address['primary']   = $company->addresses[0]->primary;
            $address['secondary'] = $company->addresses[0]->secondary;
            $address['postcode']  = $company->addresses[0]->postcode;
            $address['state']     = $company->addresses[0]->state;
        }

        if (! is_array($company->phones)) {
            $phone['office'] = '';
            $phone['mobile'] = '';
            $phone['fax']    = '';
        } else {
            $phone['office'] = $company->phones[0]->phone_number;
            $phone['mobile'] = $company->phones[1]->phone_number;
            $phone['fax']    = $company->phones[2]->phone_number;
        }

        $data = [
            'company' => $company,
            'address' => $address,
            'phone'   => $phone,
        ];

        $std_cd_state = StandardCode::where('code','KOD_NEGERI')->where('description', '!=', 'Seluruh Malaysia')->get();
        $std_cd_bank  = standard_code('BankAccount');
        $gst_doc      = Document::where('document_id', $company->id)->where('document_types', 'GST')->orderby('id', 'desc')->limit(1)->get();

        return view('company.edit', compact('company', 'std_cd_state', 'std_cd_bank', 'gst_doc', 'hashslug', 'owner', 'ssm1', 'cidb1', 'bpku1', 'spkk1', 'mof1', 'bumiputra1', 'kdn1', 'ssm_doc', 'cidb_doc', 'bpku_doc', 'spkk_doc', 'mof_doc', 'bumi_doc', 'kdn_doc', 'gred', 'kategori', 'pengkhususan', 'db1', 'db2', 'db3', 'db4', 'db5', 'db6', 'db7', 'db8', 'db9', 'db10', 'db11', 'db12', 'db13'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Company::find($id)->update($request->only('ssm_no', 'company_name', 'email', 'gst_registered_no', 'account_bank_no', 'bank_name'));

        $address = Company::find($id)->addresses();
        if($address->count() > 0){
            Company::find($id)->addresses()->update($request->only('primary', 'secondary', 'postcode', 'state'));    
        }else{
            Company::find($id)->addresses()->create($request->only('primary', 'secondary', 'postcode', 'state'));
        }
        //Company::find($id)->addresses()->updateOrCreate($request->only('primary', 'secondary', 'postcode', 'state'));

        $company = Company::find($id);
        if (! empty($company->phones[0])) {
            $company->phones[0]->phone_number = $request->input('phone_number3');
            $company->phones[0]->update();
        }
        if (! empty($company->phones[1])) {
            $company->phones[1]->phone_number = $request->input('phone_number2');
            $company->phones[1]->update();
        }
        if (! empty($company->phones[2])) {
            $company->phones[2]->phone_number = $request->input('phone_number4');
            $company->phones[2]->update();
        }

        if (! empty($request->gst)) {
            $gst_files = Input::file('gst');
            $rules     = ['file' => 'mimes:pdf,doc,docx,txt|max:10240']; //'required|mimes:pdf'
            $validator = Validator::make(['file' => $gst_files], $rules);
            if ($validator->passes()) {
                $destinationPath = 'uploads'; // upload folder in public directory
                $NamaDokumen     = $gst_files->getClientOriginalName();
                $upload_success  = $gst_files->move($destinationPath, $NamaDokumen);

                // save into database
                $extension             = $gst_files->getClientOriginalExtension();
                $entry                 = new Document();
                $entry->document_id    = $id;
                $entry->document_types = 'GST';
                $entry->mime           = $gst_files->getClientMimeType();
                $entry->document_name  = $NamaDokumen;
                $entry->document_path  = $gst_files->getFilename() . '.' . $extension;
                $entry->save();
            }
        }

        swal()->success('Kemaskini Syarikat', 'Rekod telah berjaya dikemaskini.', []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
