<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;


class LogoutXController extends Controller
{
    
    public function __invoke()
    {
         Auth::logout();
         return redirect('/login');
    }
    
   public function logoutx(Request $request) {
    Auth::logout();
    return redirect('/');
  }
   
}
