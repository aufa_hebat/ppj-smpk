<?php

namespace App\Http\Controllers;

use App\Http\Requests\CsvImportRequest;
use App\Models\CSV\Preview;
// use App\Models\VariationOrderCategory;
// use App\Models\VariationOrderElement;
// use App\Models\VariationOrderItem;
// use App\Models\VariationOrderSubItem;
use App\Models\VO\Type;
use App\Models\VO\PK\Element;
use App\Models\VO\PK\Item;
use App\Models\VO\PK\SubItem;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportVOController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function csv()
    {
        return view('import.vo.csv');
    }

    public function parse(CsvImportRequest $request)
    {
        $path = $request->file('csv_file')->getRealPath();

        if ($request->has('header')) {
            $data = Excel::load($path, function ($reader) {})->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        if (count($data) > 0) {
            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }
            $data_array = [];
            foreach ($data as $ky => $a) {
                if ('0.0' != $a['no_elemen'] && '0.0' == $a['no_item'] && '0.0' == $a['no_subitem']) {
                    $data_array[] = $data[$ky];
                }
            }
            $csv_data      = $data_array;
            $csv_data_file = Preview::create([
                'file'   => $request->file('csv_file')->getClientOriginalName(),
                'header' => $request->has('header'),
                'data'   => json_encode($data),
            ]);
        } else {
            return redirect()->back();
        }
        $sub_fields = ['no_elemen', 'no_item', 'no_subitem', 'item', 'keterangan', 'unit', 'kuantiti', 'kadarhargaseunit', 'amaun'];

        return view('import.vo.parse', compact('csv_header_fields', 'csv_data', 'csv_data_file', 'sub_fields'));
    }

    public function process(Request $request)
    {
        $messages = [];
        $status   = [];
        $data     = Preview::find($request->csv_data_file_id);
        $csv_data = json_decode($data->data, true);

        $elemen_fields = ['no_elemen', 'item', 'keterangan', 'status_pengurangan', 'status_penambahan', 'kuantiti_asal', 'kuantiti_baru', 'jumlah_asal', 'jumlah_baru', 'net'];
        $item_fields   = ['no_elemen', 'no_item', 'item', 'keterangan', 'kuantiti_asal', 'kuantiti_baru', 'jumlah_asal', 'jumlah_baru', 'net'];
        $sub_fields    = ['no_elemen', 'no_item', 'no_subitem', 'item', 'keterangan', 'unit', 'kadar_harga_seunit', 'kuantiti_asal', 'kuantiti_baru', 'jumlah_asal', 'jumlah_baru', 'net'];

        // if ('' == $request->variation_order_type_id) {
        //     $vo_category = Type::create([
        //         // 'user_id'                 => user()->id,
        //         // 'department_id'           => user()->department->id,
        //         // 'sst_id'                  => $request->sst_id,
        //         'vo_id'      => $request->vo_id,
        //         'variation_order_type_id' => $request->variation_order_type_id,
        //     ]);
        //     $variation_order_category_id = $vo_category->id;
        // } else {
        //     $variation_order_category_id = $request->variation_order_category_id;
        // }

        SubItem::where([
            ['sst_id', '=', $request->sst_id],
            ['vo_id', '=', $request->vo_id],
            // ['variation_order_type_id', '=', $request->variation_order_type_id],
            // ['variation_order_category_id', '=', $variation_order_category_id],
        ])->delete();
        Item::where([
            ['sst_id', '=', $request->sst_id],
            ['vo_id', '=', $request->vo_id],
            // ['variation_order_type_id', '=', $request->variation_order_type_id],
            // ['variation_order_category_id', '=', $variation_order_category_id],
        ])->delete();
        Element::where([
            ['sst_id', '=', $request->sst_id],
            ['vo_id', '=', $request->vo_id],
            // ['variation_order_type_id', '=', $request->variation_order_type_id],
            // ['variation_order_category_id', '=', $variation_order_category_id],
        ])->delete();

        if (! empty($csv_data)) {
            foreach ($csv_data as $row) {
                $elemenBQ = new Element();
                $itemBQ   = new Item();
                $subBQ    = new SubItem();
                if (! empty($row['no_elemen']) && ! empty($row['no_item']) && ! empty($row['no_subitem'])) {
                    // $subBQ->user_id                     = user()->id;
                    // $subBQ->department_id               = user()->department->id;
                    $subBQ->sst_id      = $request->sst_id;
                    $subBQ->vo_id       = $request->vo_id;
                    // $subBQ->variation_order_type_id     = $request->variation_order_type_id;
                    // $subBQ->variation_order_category_id = $variation_order_category_id;
                    foreach ($sub_fields as $index => $field1) {
                        if ($data->header) {
                            if ('no_elemen' == $field1) {
                                $subBQ->bq_no_element = $row['no_elemen'];
                            } elseif ('no_item' == $field1) {
                                $subBQ->bq_no_item = $row['no_item'];
                            } elseif ('no_subitem' == $field1) {
                                $subBQ->no = $row['no_subitem'];
                            } elseif ('item' == $field1) {
                                $subBQ->item = $row['item'];
                            } elseif ('keterangan' == $field1) {
                                $subBQ->description = $row['keterangan'];
                            } elseif ('unit' == $field1) {
                                $subBQ->unit = $row['unit'];
                            } elseif ('kadar_harga_seunit' == $field1) {
                                $subBQ->rate_per_unit = money()->toMachine(preg_replace('/[,]/', '', $row['kadar_harga_seunit']) ?? 0);
                            } elseif ('kuantiti_asal' == $field1) {
                                $subBQ->quantity_omission = money()->toMachine(preg_replace('/[,]/', '', $row['kuantiti_asal']) ?? 0);
                            } elseif ('kuantiti_baru' == $field1) {
                                $subBQ->quantity_addition = money()->toMachine(preg_replace('/[,]/', '', $row['kuantiti_baru']) ?? 0);
                            } elseif ('jumlah_asal' == $field1) {
                                $subBQ->amount_omission = money()->toMachine(preg_replace('/[,]/', '', $row['jumlah_asal']) ?? 0);
                            } elseif ('jumlah_baru' == $field1) {
                                $subBQ->amount_addition = money()->toMachine(preg_replace('/[,]/', '', $row['jumlah_baru']) ?? 0);
                            } elseif ('net' == $field1) {
                                $subBQ->net_amount = money()->toMachine(preg_replace('/[,]/', '', $row['net']) ?? 0);
                            } 
                            
                        } else {
                            array_push($messages, 'There is no matching');

                            return back()->with('status', $messages)->withInput();
                        }
                        $messages = 'Berjaya Disimpan';
                        $subBQ->save();
                    }
                } elseif (! empty($row['no_elemen']) && ! empty($row['no_item']) && empty($row['no_subitem'])) {
                    // $itemBQ->user_id                     = user()->id;
                    // $itemBQ->department_id               = user()->department->id;
                    $itemBQ->sst_id         = $request->sst_id;
                    $itemBQ->vo_id          = $request->vo_id;
                    // $itemBQ->variation_order_type_id     = $request->variation_order_type_id;
                    // $itemBQ->variation_order_category_id = $variation_order_category_id;
                    foreach ($item_fields as $index => $field2) {
                        if ($data->header) {
                            if ('no_elemen' == $field2) {
                                $itemBQ->bq_no_element = $row['no_elemen'];
                            } elseif ('no_item' == $field2) {
                                $itemBQ->no = $row['no_item'];
                            } elseif ('item' == $field2) {
                                $itemBQ->item = strtoupper($row['item']);
                            } elseif ('keterangan' == $field2) {
                                $itemBQ->description = $row['keterangan'];
                            } elseif ('kuantiti_asal' == $field2) {
                                $itemBQ->quantity_omission = money()->toMachine(preg_replace('/[,]/', '', $row['kuantiti_asal']) ?? 0);
                            } elseif ('kuantiti_baru' == $field2) {
                                $itemBQ->quantity_addition = money()->toMachine(preg_replace('/[,]/', '', $row['kuantiti_baru']) ?? 0);
                            } elseif ('jumlah_asal' == $field2) {
                                $itemBQ->amount_omission = money()->toMachine(preg_replace('/[,]/', '', $row['jumlah_asal']) ?? 0);
                            } elseif ('jumlah_baru' == $field2) {
                                $itemBQ->amount_addition = money()->toMachine(preg_replace('/[,]/', '', $row['jumlah_baru']) ?? 0);
                            } elseif ('net' == $field2) {
                                $itemBQ->net_amount = money()->toMachine(preg_replace('/[,]/', '', $row['net']) ?? 0);
                            }
                        } else {
                            array_push($messages, 'There is no matching');

                            return back()->with('status', $messages)->withInput();
                        }
                        $messages = 'Berjaya Disimpan';
                        $itemBQ->save();
                    }
                } elseif (! empty($row['no_elemen']) && empty($row['no_item']) && empty($row['no_subitem'])) {
                    // $elemenBQ->user_id                     = user()->id;
                    // $elemenBQ->department_id               = user()->department->id;
                    $elemenBQ->sst_id         = $request->sst_id;
                    $elemenBQ->vo_id          = $request->vo_id;
                    // $elemenBQ->variation_order_type_id     = $request->variation_order_type_id;
                    // $elemenBQ->variation_order_category_id = $variation_order_category_id;
                    foreach ($elemen_fields as $field3) {
                        if ($data->header) {
                            if ('no_elemen' == $field3) {
                                $elemenBQ->no = $row['no_elemen'];
                            } elseif ('item' == $field3) {
                                $elemenBQ->element = strtoupper($row['item']);
                            } elseif ('keterangan' == $field3) {
                                $elemenBQ->description = $row['keterangan'];
                            } elseif ('kuantiti_asal' == $field3) {
                                $elemenBQ->quantity_omission = money()->toMachine(preg_replace('/[,]/', '', $row['kuantiti_asal']) ?? 0);
                            } elseif ('kuantiti_baru' == $field3) {
                                $elemenBQ->quantity_addition = money()->toMachine(preg_replace('/[,]/', '', $row['kuantiti_baru']) ?? 0);
                            } elseif ('jumlah_asal' == $field3) {
                                $elemenBQ->amount_omission = money()->toMachine(preg_replace('/[,]/', '', $row['jumlah_asal']) ?? 0);
                            } elseif ('jumlah_baru' == $field3) {
                                $elemenBQ->amount_addition = money()->toMachine(preg_replace('/[,]/', '', $row['jumlah_baru']) ?? 0);
                            } elseif ('net' == $field3) {
                                $elemenBQ->net_amount = money()->toMachine(preg_replace('/[,]/', '', $row['net']) ?? 0);
                            } elseif ('status_penambahan' == $field3) {
                                $elemenBQ->is_new = $row['status_penambahan'];
                            }elseif ('status_pengurangan' == $field3) {
                                $elemenBQ->is_removed = $row['status_pengurangan'];
                            }
                        } else {
                            array_push($messages, 'There is no matching');

                            return back()->with('status', $messages)->withInput();
                        }
                        $messages = 'Berjaya Disimpan';
                        $elemenBQ->Save();
                    }
                }
            }
        }

        return view('import.vo.csv')->with('status', $messages);
    }
}
