<?php

namespace App\Http\Controllers\Api\Utility;

use App\Http\Controllers\Controller;
use App\Models\DefectLiabilityPeriod;
use Illuminate\Http\Request;

class LiabilityPeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'value'       => 'required',
        ]);

        $defectLiabilityPeriod = DefectLiabilityPeriod::create([
            'name'        => $request->name,
            'description' => $request->description,
            'value'       => $request->value,
        ]);

        audit(user(), __('Tempoh Liabiliti ' .$request->description. ' berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dijana.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'value'       => 'required',
        ]);

        $defectLiabilityPeriod = DefectLiabilityPeriod::find($id);

        $defectLiabilityPeriod->update([
            'name'        => $request->name,
            'description' => $request->description,
            'value'       => $request->value,
        ]);

        audit(user(), __('Tempoh Liabiliti ' .$request->description. ' berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DefectLiabilityPeriod::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
