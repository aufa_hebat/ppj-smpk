<?php

namespace App\Http\Controllers\Api\Utility;

use App\Http\Controllers\Controller;
use App\Models\BonType;
use Illuminate\Http\Request;

class BonTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
        ]);

        $bon_type = BonType::create([
            'name'      => $request->name,
            'code'      => $request->code,
            'is_active' => 1,
        ]);

        audit(user(), __('Jenis Bon ' .$request->code. ' - ' .$request->name. ' berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dijana.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
        ]);

        $bon_type = BonType::find($id);

        $bon_type->update([
            'code'      => $request->code,
            'name'      => $request->name,
            'is_active' => $request->is_active,
        ]);

        audit(user(), __('Jenis Bon ' .$request->code. ' - ' .$request->name. ' berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BonType::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
