<?php

namespace App\Http\Controllers\Api\Utility;

use App\Http\Controllers\Controller;
use App\Models\RefVoc;
use App\Models\User;
use Illuminate\Http\Request;

class VocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! empty($request->baru)) {
            if (! empty($request->exist)) {
                foreach ($request->exist as $ex) {
                    $checkJKPembuka = RefVoc::where('deleted_at', null)->get();
                    if (! empty($checkJKPembuka)) {
                        foreach ($checkJKPembuka as $a) {
                            $ida[] = $a['id'];
                        }
                        $idaS[] = $ex['id'];
                    }
                }
                $diffA = array_diff($ida, $idaS);

                foreach ($diffA as $ad) {
                    RefVoc::destroy($ad);
                }
            } else {
                //$checkJKPembuka = Approver::where('vo_id', $request->vo_id)->get();
                //Approver::whereIn('vo_id', $checkJKPembuka->pluck('vo_id'))->delete();
            }

            foreach ($request->baru as $userBaru) {
                
                if (! empty($userBaru['name'])) {
                    
                    $user = User::find($userBaru['name']);
                    if($user){
                        $approver = RefVoc::create([
                            'user_id'       =>  $user->id,
                            'position_id'   =>  $user->position->id,
                            'department_id' =>  $user->department->id,
                            'role'          =>  $userBaru['role'],
                        ]);
                       // $vo = VO::find($request->vo_id);
                        //audit($vo, __('Jawatankuasa Mesyuarat bagi ' .$vo->sst->acquisition->reference. ' : ' .$vo->sst->acquisition->title. ' telah berjaya dikemaskini.'));
                        audit(user(), __('Urusetia Jawatankuasa Perubahan Kerja' .$user->name. ' berjaya disimpan.'));
                    }
                }
            }
        }else{
            //$checkJKPembuka = Approver::where('vo_id', $request->vo_id)->get();
            //Approver::whereIn('vo_id', $checkJKPembuka->pluck('vo_id'))->delete();
        }      

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RefVoc::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
