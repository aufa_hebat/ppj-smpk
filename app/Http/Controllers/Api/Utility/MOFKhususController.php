<?php

namespace App\Http\Controllers\Api\Utility;

use Illuminate\Http\Request;
use App\Models\MOFCode;
use App\Http\Controllers\Controller;

class MOFKhususController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
            'code' => 'required',
            'name' => 'required',
        ]);

        $mof_khusus = MOFCode::create([
            'code'      => $request->code,
            'name'      => $request->name,
            'type'      => 'khusus',
            'category'      => $request->category,
        ]);

        audit(user(), __('MOF:Pengkhususan ' .$request->name. ' berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dijana.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
            'code' => 'required',
            'name' => 'required',
        ]);

        $mof_khusus = MOFCode::find($id);

        $mof_khusus->update([
            'category'      => $request->category,
            'code'      => $request->code,
            'name'      => $request->name,
        ]);

        audit(user(), __('MOF:Pengkhususan ' .$request->name. ' berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MOFCode::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
