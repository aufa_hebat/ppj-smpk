<?php

namespace App\Http\Controllers\Api\Utility;

use Illuminate\Http\Request;
use App\Models\CIDBCode;
use App\Http\Controllers\Controller;

class CIDBKhususController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
            'code' => 'required',
            'name' => 'required',
        ]);

        $cidb_category = CIDBCode::create([
            'code'      => $request->code,
            'name'      => $request->name,
            'type'      => 'khusus',
            'category'      => $request->category,
        ]);

        audit(user(), __('CIDB:Pengkhususan ' .$request->name. ' berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dijana.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
            'code' => 'required',
            'name' => 'required',
        ]);

        $cidb_khusus = CIDBCode::find($id);

        $cidb_khusus->update([
            'category'  => $request->category,
            'code'      => $request->code,
            'name'      => $request->name,
        ]);

        audit(user(), __('CIDB:Pengkhususan ' .$request->name. ' berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CIDBCode::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
