<?php

namespace App\Http\Controllers\Api\Utility;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $bank = Bank::create([
            'name' => $request->name,
        ]);

        audit(user(), __('Bank ' .$request->name. ' berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dijana.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $bank = Bank::find($id);

        $bank->update([
            'name' => $request->name,
        ]);

        audit(user(), __('Bank ' .$request->name. ' berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bank::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
