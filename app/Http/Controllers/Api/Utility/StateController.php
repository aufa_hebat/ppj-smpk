<?php

namespace App\Http\Controllers\Api\Utility;

use App\Http\Controllers\Controller;
use App\Models\StandardCode;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code'        => 'required',
            'code_short'  => 'required',
            'description' => 'required',
        ]);

        $state = StandardCode::create([
            'code'        => $request->code,
            'code_short'  => $request->code_short,
            'description' => $request->description,
            'status'      => 0,
        ]);

        audit(user(), __('Standard Code ' .$request->code_short. ' - ' .$request->description. ' berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dijana.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code'        => 'required',
            'code_short'  => 'required',
            'description' => 'required',
        ]);

        $state = StandardCode::find($id);
        $state->update([
            'code'        => $request->code,
            'code_short'  => $request->code_short,
            'description' => $request->description,
            'status'      => $request->status,
        ]);

        audit(user(), __('Standard Code ' .$request->code_short. ' - ' .$request->description. ' berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StandardCode::find($id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
