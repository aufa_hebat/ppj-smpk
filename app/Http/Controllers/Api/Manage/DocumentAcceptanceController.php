<?php

namespace App\Http\Controllers\Api\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\Sst;
use App\Models\DocumentAcceptance;
use Illuminate\Support\Carbon;

class DocumentAcceptanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'document_acceptance_at'         => 'required',
        ],$messages = [
            'document_acceptance_at.required' => 'Tarikh Penerimaan Dokumen harus diisi.',
        ]);

        $acceptance = DocumentAcceptance::create([
            'user_id'                  => user()->id,
            'sst_id'                   => $request->sst_id,
            'document_acceptance_at'   => (!empty($request->document_acceptance_at)) ? Carbon::createFromFormat('d/m/Y G:i A', $request->document_acceptance_at) : null,
            'remarks'                  => $request->remarks,
        ]);

        $sst = Sst::find($request->sst_id);

        // if($request->hasFile('uploadFileDA')){
        $acceptance_document = [
            'request_hDocumentId' => null,
            'request_hasFile'     => $request->hasFile('uploadFileDA'),
            'request_file'        => $request->file('uploadFileDA'),
            'acquisition'         => $sst->acquisition,
            'doc_type'            => 'Penerimaan Dokumen',
            'doc_id'              => $acceptance->id,
        ];
        common()->upload_document($acceptance_document);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'document_acceptance_at'         => 'required',
        ],$messages = [
            'document_acceptance_at.required' => 'Tarikh Penerimaan Dokumen harus diisi.',
        ]);

        DocumentAcceptance::findByHashSlug($id)->update([
            'document_acceptance_at'       => (!empty($request->document_acceptance_at)) ? Carbon::createFromFormat('d/m/Y G:i A', $request->document_acceptance_at) : null,
            'remarks' => $request->remarks,
            'acceptance_status' => (!empty($request->confirm)) ? $request->confirm : null,
        ]);

        $sst                = Sst::where('id',$request->sst_id)->first();

        $acceptance = DocumentAcceptance::findByHashSlug($id);

        // if($request->hasFile('uploadFileDA')){
        $acceptance_document = [
            'request_hDocumentId' => $request->hDocumentIdDA,
            'request_hasFile'     => $request->hasFile('uploadFileDA'),
            'request_file'        => $request->file('uploadFileDA'),
            'acquisition'         => $sst->acquisition,
            'doc_type'            => 'Penerimaan Dokumen',
            'doc_id'              => $acceptance->id,
        ];
        common()->upload_document($acceptance_document);
        // }

        if(!empty($request->action)){
            DocumentAcceptance::findByHashSlug($id)->update([
                'acceptance_status'    =>  '1',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
