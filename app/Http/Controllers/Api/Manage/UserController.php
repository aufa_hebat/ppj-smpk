<?php

namespace App\Http\Controllers\Api\Manage;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Models\WorkflowTeamTask;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            // 'password' => 'required|string|min:6|confirmed',
            'roles'    => 'required',
        ]);
        $data = $request->only('name', 'email', 'phone_no', 'password', 'executor_department_id', 'department_id', 'section_id', 'supervisor_id', 'position_id', 'scheme_id', 'grade_id','honourary');

        $user = User::create([
            'name'     => $data['name'],
            'honourary'     => $data['honourary'],
            'email'    => $data['email'],
            'phone_no' => $data['phone_no'],
            // 'password' => bcrypt($data['password']),
            'executor_department_id' => $data['executor_department_id'] ?? null,
            'department_id' => $data['department_id'] ?? null,
            'section_id'    => $data['section_id'] ?? null,
            'supervisor_id' => $data['supervisor_id'] ?? null,
            'position_id'   => $data['position_id'] ?? null,
            'scheme_id'     => $data['scheme_id'] ?? null,
            'grade_id'      => $data['grade_id'] ?? null,
        ]);

        if($request->department_id == 3) {
            $workflow_team = 2;
        } else {
            $workflow_team = 1;
        }

        $user_flow_task = WorkflowTeamTask::create([
            'workflow_team_id' => $workflow_team,
            'user_id' => $user->id,
        ]);

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                $user->phones()->create(['phone_type_id' => $key, 'phone_number' => $pn]);
            }
        }

        event(new Registered($user));
        //DIN test
        //$user->syncRoles($request->roles);

        return response()->api([], __('User successfully stored.'), true, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::details()->findByHashSlug($id);

        /**
         * @todo should have a transformer to do this.
         */
        $user  = collect($user->only('name', 'email', 'roles_to_string', 'roles'));
        $roles = $user->get('roles')->mapWithKeys(function ($role) {
            return [$role->id => $role->name];
        });
        $user->put('roles', $roles);

        return response()->api($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            // 'password' => 'required|string|min:6|confirmed',
        ]);

        $data = $request->only('name', 'email', 'phone_no', 'password', 'executor_department_id', 'department_id', 'section_id', 'supervisor_id', 'position_id', 'scheme_id', 'grade_id','honourary');

        $user = User::update([
            'name'                   => $data['name'],
            'honourary'              => $data['honourary'],
            'email'                  => $data['email'],
            'phone_no'               => $data['phone_no'],
            // 'password'               => bcrypt($data['password']),
            'executor_department_id' => $data['executor_department_id'],
            'department_id'          => $data['department_id'],
            'section_id'             => $data['section_id'],
            'supervisor_id'          => $data['supervisor_id'],
            'position_id'            => $data['position_id'],
            'scheme_id'              => $data['scheme_id'],
            'grade_id'               => $data['grade_id'],
        ]);

        if($request->department_id == 3) {
            $workflow_team = 2;
        } else {
            $workflow_team = 1;
        }

        $user_flow_task = WorkflowTeamTask::update([
            'workflow_team_id' => $workflow_team,
            'user_id' => $user->id,
        ]);

        foreach ($request->only('phone_number') as  $phone_numbers) {
            foreach ($phone_numbers as $key => $pn) {
                $user->phones()->update(['phone_type_id' => $key, 'phone_number' => $pn]);
            }
        }

        event(new Registered($user));
        $user->syncRoles($request->roles);

        return response()->api([], __('User successfully updated.'), true, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id == user()->hashslug) {
            return response()->api([], __('You cannot delete yourself!'), false, 401);
        }
        $user = User::findByHashSlug($id);
        if ($user->hasRole('developer')) {
            return response()->api([], __('Trust me, don\'t kill your developer!'), false, 401);
        }
        $user->delete();

        return response()->api([], __('You have successfully delete a user.'));
    }
    
    
    //find user by department id then sort with alphabet name
    public function finduserbydepartment($id)
    {
        
        $users = User::where('department_id', $id)
                ->OrderBy('name', 'ASC')
                ->get();
        return response()->json(['usersByDepartment'=>$users]);
    }
    
     //find user by department id then sort with alphabet name
    public function finduserbyid($id)
    {      
        $users = User::where('id', $id)
                ->OrderBy('name', 'ASC')
                ->get()->first();
        
              //$('#department_ids').val(''+response.data.usersById.department.name);
                         //         $('#department_idss').val(''+response.data.usersById.department_id);
                        //          $('#position_ids').val(''+response.data.usersById.position.name);
                          //        $('#position_idss').val(''+response.data.usersById.position_id);
        
        $department_ids = $users->department->name;
        $department_idss = $users->department_id;
        $position_ids = $users->position->name;
        $position_idss = $users->position_id;
        return response()->json(['usersById'=>$users , 'department_ids' => $department_ids 
                ,'department_idss' => $department_idss
                ,'position_ids' => $position_ids
                ,'position_idss' => $position_idss
                ]);
    }
    
    
    
}
