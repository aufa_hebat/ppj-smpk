<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\VO\PQ\Element;
use App\Models\VO\PQ\Item;
use App\Models\VO\PQ\SubItem;
use App\Models\VO\Type;
use App\Models\VO\VO;
use Illuminate\Http\Request;

class VariationOrderPQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vo       = VO::where('id', $request->vo_id)->first();
        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        if ('' == $request->vo_type_id) {
            $vo_type = Type::updateOrCreate([
                'vo_id'                   => $request->vo_id,
                'variation_order_type_id' => $request->variation_order_type_id,
            ]);
        }

        foreach ($request->select_pq as $all_no) {
            $no             = explode('.', $all_no);
            $bq_element_no  = $no[0];
            $bq_item_no     = $no[1];
            $bq_sub_item_no = $no[2];

            // Insert into vo_pq_elements
            $bq_element_id_name          = 'bq_element_id_' . $bq_element_no;
            $bq_element_element_name     = 'bq_element_element_' . $bq_element_no;
            $bq_element_description_name = 'bq_element_description_' . $bq_element_no;

            $vo_pq_element = Element::updateOrCreate(
                [
                    'acquisition_id' => $request->acquisition_id,
                    'bq_element_id'  => $request->$bq_element_id_name,
                    'vo_id'          => $request->vo_id,
                ],
                [
                    'no'          => $bq_element_no,
                    'element'     => $request->$bq_element_element_name,
                    'description' => $request->$bq_element_description_name,
                ]
            );

            // Insert into vo_pq_items
            $bq_item_id_name          = 'bq_item_id_' . $bq_element_no . '_' . $bq_item_no;
            $bq_item_item_name        = 'bq_item_item_' . $bq_element_no . '_' . $bq_item_no;
            $bq_item_description_name = 'bq_item_description_' . $bq_element_no . '_' . $bq_item_no;
            $bq_summary_name          = 'PQ_HURAIAN_' . $bq_element_no . '_' . $bq_item_no;
            $bq_reason_name           = 'PQ_SEBAB_' . $bq_element_no . '_' . $bq_item_no;

            $vo_pq_item = Item::updateOrCreate(
                [
                    'acquisition_id' => $request->acquisition_id,
                    'bq_item_id'     => $request->$bq_item_id_name,
                    'vo_id'          => $request->vo_id,
                ],
                [
                    'bq_element_id'    => $request->$bq_element_id_name,
                    'vo_pq_element_id' => $vo_pq_element->id,
                    'bq_no_element'    => $bq_element_no,
                    'no'               => $bq_item_no,
                    'item'             => $request->$bq_item_item_name,
                    'description'      => $request->$bq_item_description_name,
                    'summary'          => $request->$bq_summary_name,
                    'reason_required'  => $request->$bq_reason_name,
                ]
            );

            // Insert into vo_pq_sub_items
            $bq_sub_item_id_name            = 'bq_sub_item_id_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_item_name          = 'bq_sub_item_item_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_unit_name          = 'bq_sub_item_unit_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_quantity_name      = 'bq_sub_item_quantity_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_rate_per_unit_name = 'bq_sub_item_rate_per_unit_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_amount_name        = 'bq_sub_item_amount_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_quantity_omission           = 'PQ_KUANTITI_OMISSION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_quantity_addition           = 'PQ_KUANTITI_ADDITION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_amount_omission             = 'PQ_AMAUN_OMISSION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_amount_addition             = 'PQ_AMAUN_ADDITION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_net_amount                  = 'PQ_NET_AMOUNT_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_bq_reference                = 'PQ_BQ_REFERENCE_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;

            $vo_pq_sub_item = SubItem::updateOrCreate(
                [
                    'acquisition_id' => $request->acquisition_id,
                    'bq_subitem_id'  => $request->$bq_sub_item_id_name,
                    'vo_id'          => $request->vo_id,
                ],
                [
                    'bq_element_id'     => $request->$bq_element_id_name,
                    'bq_item_id'        => $request->$bq_item_id_name,
                    'vo_pq_item_id'     => $vo_pq_item->id,
                    'bq_no_element'     => $bq_element_no,
                    'bq_no_item'        => $bq_item_no,
                    'no'                => $bq_sub_item_no,
                    'item'              => $request->$bq_sub_item_item_name,
                    'unit'              => $request->$bq_sub_item_unit_name,
                    'quantity'          => '' != $request->$bq_sub_item_quantity_name ? money()->toMachine($request->$bq_sub_item_quantity_name) : money()->toMachine(0),
                    'rate_per_unit'     => '' != $request->$bq_sub_item_rate_per_unit_name ? money()->toMachine($request->$bq_sub_item_rate_per_unit_name) : money()->toMachine(0),
                    'amount'            => '' != $request->$bq_sub_item_amount_name ? money()->toMachine($request->$bq_sub_item_amount_name) : money()->toMachine(0),
                    'quantity_omission' => '' != $request->$pq_quantity_omission ? money()->toMachine($request->$pq_quantity_omission) : money()->toMachine(0),
                    'quantity_addition' => '' != $request->$pq_quantity_addition ? money()->toMachine($request->$pq_quantity_addition) : money()->toMachine(0),
                    'amount_omission'   => '' != $request->$pq_amount_omission ? money()->toMachine($request->$pq_amount_omission) : money()->toMachine(0),
                    'amount_addition'   => '' != $request->$pq_amount_addition ? money()->toMachine($request->$pq_amount_addition) : money()->toMachine(0),
                    'net_amount'        => '' != $request->$pq_net_amount ? money()->toMachine($request->$pq_net_amount) : money()->toMachine(0),
                    'bq_reference'      => $request->$pq_bq_reference,
                ]
            );
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
