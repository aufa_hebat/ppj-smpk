<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\EotBon;
use App\Models\Acquisition\Bon;
use Carbon\Carbon;
use App\Models\WorkflowTask;
use App\Models\Acquisition\Eot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExtensionBonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bon = EotBon::updateOrCreate([
            'eot_id' => $id,
        ], [
            'user_id'     => user()->id,
            'bon_old_new' => $request->input('bon_old_new'),
            'bon_type'    => $request->input('bon_type'),

            'bank_name'                   => $request->input('bank_name'),
            'bank_no'                     => $request->input('bank_no'),
            'bank_start_date'             => $request->input('bank_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_start_date')) : null,
            'bank_proposed_date'          => $request->input('bank_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_proposed_date')) : null,
            'bank_approval_received_date' => $request->input('bank_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_approval_received_date')) : null,
            'bank_received_date'          => $request->input('bank_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_received_date')) : null,
            'bank_amount'                 => $request->input('bank_amount') ? money()->toMachine($request->input('bank_amount')) : null,
            'bank_expired_date'           => $request->input('bank_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_expired_date')) : null,
            'bank_approval_proposed_date' => $request->input('bank_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_approval_proposed_date')) : null,

            'insurance_name'                   => $request->input('insurance_name'),
            'insurance_no'                     => $request->input('insurance_no'),
            'insurance_start_date'             => $request->input('insurance_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_start_date')) : null,
            'insurance_proposed_date'          => $request->input('insurance_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_proposed_date')) : null,
            'insurance_approval_received_date' => $request->input('insurance_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_approval_received_date')) : null,
            'insurance_received_date'          => $request->input('insurance_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_received_date')) : null,
            'insurance_amount'                 => $request->input('insurance_amount') ? money()->toMachine($request->input('insurance_amount')) : null,
            'insurance_expired_date'           => $request->input('insurance_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_expired_date')) : null,
            'insurance_approval_proposed_date' => $request->input('insurance_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_approval_proposed_date')) : null,

            'cheque_name'           => $request->input('cheque_name'),
            'cheque_resit_no'       => $request->input('cheque_resit_no'),
            'cheque_received_date'  => $request->input('cheque_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cheque_received_date')) : null,
            'cheque_no'             => $request->input('cheque_no'),
            'cheque_submitted_date' => $request->input('cheque_submitted_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cheque_submitted_date')) : null,

            'money_received_at' => $request->input('money_received_at') ? Carbon::createFromFormat('d/m/Y', $request->input('money_received_at')) : null,
            'money_amount'      => $request->input('money_amount') ? money()->toMachine($request->input('money_amount')) : null,
        ]);

        $acq_bon = Bon::updateOrCreate([
            'sst_id' => $bon->eot->sst->id,
        ], [
            'bank_expired_date'  => $request->input('bank_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_expired_date')) : null,
            'insurance_expired_date'           => $request->input('insurance_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_expired_date')) : null,
        ]);

        $bon_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $bon->eot->appointed_company->acquisition,
            'doc_type'            => 'EOT_BON',
            'doc_id'              => $bon->id,
        ];
        common()->upload_document($bon_requirements);

        if($bon->eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){
            audit($bon, __('Bon Pelanjutan Masa '.$bon->eot->bil.' bagi perolehan ' .$bon->eot->sst->acquisition->reference. ' : ' .$bon->eot->sst->acquisition->title. ' telah berjaya disimpan.'));

            $workflow = WorkflowTask::where('acquisition_id', $bon->eot->appointed_company->acquisition_id)->where('eot_id',$bon->eot_id)->first();
            $eot = EOT::find($id);
            $workflow->flow_desc = 'Insurans Pelanjutan Masa '.$bon->eot->bil.' : Sila teruskan dengan Insurans Pelanjutan Masa.';
            $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit#insurance';
            $workflow->flow_location = 'EOT';
            $workflow->role_id = 4;
            $workflow->is_claimed = 1;
            $workflow->workflow_team_id = 1;

            $workflow->update();
        } else {
            audit($bon, __('Bon Pelanjutan Masa bagi perolehan ' .$bon->eot->sst->acquisition->reference. ' : ' .$bon->eot->sst->acquisition->title. ' telah berjaya disimpan.'));

            $workflow = WorkflowTask::where('acquisition_id', $bon->eot->appointed_company->acquisition_id)->where('eot_id',$bon->eot_id)->first();
            $eot = EOT::find($id);
            $workflow->flow_desc = 'Insurans Pelanjutan Masa : Sila teruskan dengan Insurans Pelanjutan Masa.';
            $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit#insurance';
            $workflow->flow_location = 'EOT';
            $workflow->role_id = 4;
            $workflow->is_claimed = 1;
            $workflow->workflow_team_id = 1;

            $workflow->update();
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
