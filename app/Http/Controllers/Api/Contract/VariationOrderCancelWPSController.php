<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\VO\Active;
use App\Models\VO\Cancel;
use App\Models\VO\Element;
use Illuminate\Http\Request;

class VariationOrderCancelWPSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $count = Cancel::where('vo_id', $request->vo_id)->count();
        if ($count > 0) {
            Cancel::where('vo_id', $request->vo_id)->delete();
        }

        if(!empty($request->select_cancel_wps)){
            foreach ($request->select_cancel_wps as $keyS => $item_id) {
                foreach ($request->balance_wps as $keyB => $amount) {
                    if($keyS == $keyB){
                        $cancel = Cancel::create([
                            'user_id'       => user()->id,
                            'department_id' => user()->department->id,
                            'sst_id'        => $request->sst_id,
                            'bq_element_id' => $request->bq_element_id,
                            'bq_item_id'    => $item_id,
                            'vo_id'         => $request->vo_id,
                            'amount'        => 0-$amount,
                            'baki_amount'   => 0-$amount,
                            'paid_amount'   => 0,
                        ]);
                    }
                }
            }
        }


        // if (! empty($request->cancelWps)) {
        //     foreach($request->cancelWps as $key => $hk){

        //         Cancel::updateOrCreate([
        //             'id'    =>  $key
        //         ],[
        //             'user_id'       => user()->id,
        //             'department_id' => user()->department->id,
        //             'sst_id'        => $request->sst_id,
        //             'bq_element_id' => $request->bq_element_id,
        //             'bq_item_id'    => $item_id,
        //             'vo_id'         => $request->vo_id,
        //             'amount'        => money()->toMachine(preg_replace('/[,]/', '', $hk["'balance'"]) ?? 0),
        //         ]);
        //     }
        // }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function result(Request $request, $id)
    {

        $count = Active::where('vo_id', $request->vo_id)->count();
        if ($count > 0) {
            Active::where('vo_id', $request->vo_id)->update(['status' => '0']);
        }

        if(!empty($request->result)){
            foreach ($request->result as $item_id) {
                $active = Active::updateOrCreate([
                    'id'    => $item_id,
                ],[
                    'status'       => '1',
                ]);
            }
        }


        $count = Element::where('vo_id', $request->vo_id)->count();
        if ($count > 0) {
            Element::where('vo_id', $request->vo_id)->update(['status' => '0']);
        }

        if(!empty($request->elemen)){
            foreach ($request->elemen as $element_id) {
                $elemen = Element::updateOrCreate([
                    'id'    => $element_id,
                ],[
                    'status'       => '1',
                ]);
            }
        }

        if(!empty($request->elemenUlasan)){
            foreach($request->elemenUlasan as $key => $ul){
                $element = Element::updateOrCreate([
                    'id'    => $key,
                ],[
                    'comment'       => $ul,
                ]);
            }
        }

        if(!empty($request->ulasan)){
            foreach($request->ulasan as $key => $ul){
                $active = Active::updateOrCreate([
                    'id'    => $key,
                ],[
                    'comment'       => $ul,
                ]);
            }
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
