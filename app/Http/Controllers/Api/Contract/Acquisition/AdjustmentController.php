<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class AdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::transaction(function () use($request, $id) {
            if(!empty($request->adjust)){
                foreach($request->adjust as $ad){
                    $total_item                 = 0;
                    $total_elem                 = 0;
                    $subs                       = SubItem::findByHashSlug($ad['hashslug']);
                    $subs->rate_per_unit_adjust = money()->toMachine($ad['rate'] ?? 0);
                    $subs->amount_adjust        = money()->toMachine($ad['amt'] ?? 0);
                    $subs->updated_by           = user()->id;
                    $subs->update();
    
                    if(SubItem::where('acquisition_id',$subs->acquisition_id)->where('bq_no_element',$subs->bq_no_element)->where('bq_no_item',$subs->bq_no_item)->where('deleted_at',null)->pluck('amount_adjust')->sum() > 0){
                        $total_item =   SubItem::where('acquisition_id',$subs->acquisition_id)->where('bq_no_element',$subs->bq_no_element)->where('bq_no_item',$subs->bq_no_item)->where('deleted_at',null)->pluck('amount_adjust')->sum();
                    }else{
                        $total_item =   Item::where('acquisition_id',$subs->acquisition_id)->where('bq_no_element',$subs->bq_no_element)->where('no',$subs->bq_no_item)->where('deleted_at',null)->pluck('amount')->sum();
                    }
                    
                    if(SubItem::where('acquisition_id',$subs->acquisition_id)->where('bq_no_element',$subs->bq_no_element)->where('deleted_at',null)->pluck('amount_adjust')->sum() > 0){
                        $total_elem =   SubItem::where('acquisition_id',$subs->acquisition_id)->where('bq_no_element',$subs->bq_no_element)->where('deleted_at',null)->pluck('amount_adjust')->sum();
                    }else{
                        $total_elem =   Element::where('acquisition_id',$subs->acquisition_id)->where('no',$subs->bq_no_element)->where('deleted_at',null)->pluck('amount')->sum();
                    }
                    
                    $items = Item::where('acquisition_id',$subs->acquisition_id)->where('bq_no_element',$subs->bq_no_element)->where('no',$subs->bq_no_item)->where('deleted_at',null)->first();
                    $items->amount_adjust = $total_item;
                    $items->update();
                    
                    $elemen = Element::where('acquisition_id',$subs->acquisition_id)->where('no',$subs->bq_no_element)->where('deleted_at',null)->first();
                    $elemen->amount_adjust = $total_elem;
                    $elemen->update();
                }
            }
            if(!empty($request->item)){
                foreach($request->item as $itm){
                    $items = Item::findByHashSlug($itm['hashslug']);
                    $items->amount_adjust = money()->toMachine($itm["amt"] ?? 0);
                    $items->update();
    
                    $elemen = Element::where('acquisition_id',$items->acquisition_id)->where('no',$items->bq_no_element)->where('deleted_at',null)->first();
                    $elemen->amount_adjust = Item::where('acquisition_id',$items->acquisition_id)->where('bq_no_element',$items->bq_no_element)->where('deleted_at',null)->pluck('amount_adjust')->sum();
                    $elemen->update();
                   
                }
            }
            
            if(!empty($request->elemen)){
                foreach($request->elemen as $elm){
                    $elemen = Element::findByHashSlug($elm['hashslug']);
                    $elemen->amount_adjust = money()->toMachine($elm["amt"] ?? 0);
                    $elemen->update();
                }
            }
        }, 10);
        
        return response()->api([],__('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
