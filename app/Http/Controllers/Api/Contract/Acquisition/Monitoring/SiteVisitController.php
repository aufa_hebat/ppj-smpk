<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Monitoring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

use Carbon\Carbon;

class SiteVisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id'                 => 'required',
            'sst_id'                  => 'required',
            'department_id'           => 'required',
            'acquisition_id'          => 'required',
            'acquisition_approval_id' => 'required',
            'name'                    => 'required|max:255',
            'remarks'                 => 'required|max:255',
            'visited_by'              => 'required',
            'visit_date'          => 'required',
            'percentage_completed' => 'required',
        ]);

        $data = $request->only(
            'user_id',
            'sst_id',
            'department_id',
            'acquisition_id',
            'acquisition_approval_id',
            'name',
            'remarks',
            'visited_by',
            'percentage_completed'
            //,'visit_date'
        );
        
        
         //add update contract progress
        $id_contract_progress = $request->id_contract_progress;
        $contractProgressObj = \App\Models\Acquisition\Monitoring\ContractProgress::find($id_contract_progress);
               // ->update(['deleted' => 1]);
        
        //$data['visited_at'] = now();
        $data['visited_at'] = $contractProgressObj->on_plan_date;
        $data['visit_date'] =  Carbon::createFromFormat('d/m/Y', $request->visit_date);
        
        $site_visit = \App\Models\Acquisition\Monitoring\SiteVisit::create($data);

        $site_visit->addDocument($request, 'document');
        
       
        $contractProgressObj->update(['site_visit_id' => $site_visit->id]);
        
        $workflow = WorkflowTask::where('acquisition_id',$request->acquisition_id)->where('workflow_team_id',1)->first();
        $workflow->flow_desc = '';
        $workflow->flow_location = 'Pemantauan Projek';
        $workflow->url = '';
        $workflow->user_id = null;
        $workflow->update();
        
        return response()->api(null, __('Rekod telah berjaya ditambah.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siteVisit = \App\Models\Acquisition\Monitoring\SiteVisit::findByHashSlug($id);
        $view_visit_by = $siteVisit->visitedBy->name;
        
        $visit_date_view = $siteVisit->visit_date->format('d/m/Y');
        $visited_at_view = $siteVisit->visited_at->format('d/m/Y');
        
        return response()->json(['siteVisit'=>$siteVisit , 'view_visit_by'=>$view_visit_by,
            'visit_date_view'=>$visit_date_view,
            'visited_at_view'=>$visited_at_view
            ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Models\Acquisition\Monitoring\SiteVisit::hashslug($id)->delete();

        return response()->api(null, __('Rekod telah berjaya dihapuskan.'));
    }
    
    
    //Cetak Lawatan Tapak
    
    
    
}
