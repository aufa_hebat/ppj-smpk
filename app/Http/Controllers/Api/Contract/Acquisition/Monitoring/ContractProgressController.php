<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Monitoring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContractProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            //'site_visit_percent'  => 'required',
            //'on_site_payment_percent' => 'required'
        ]);

//        $data = $request->only(
//            'id',
//            'site_visit_percent'
//        );
               
         //add update contract progress
        $id_contract_progress = $request->id;
        $contractProgressObj = \App\Models\Acquisition\Monitoring\ContractProgress::find($id_contract_progress);
               // ->update(['deleted' => 1]);
             
        
        //update site visit
        
            
         if(!is_null($contractProgressObj->site_visit_id))  { 
            $siteVisitObj = \App\Models\Acquisition\Monitoring\SiteVisit::find($contractProgressObj->site_visit_id);
            if(!is_null($siteVisitObj)){
               $siteVisitObj->update(['percentage_completed' => $request->site_visit_percent]);
            }
         }
         
         //update onsite payment
//         if(!is_null($contractProgressObj->payment_monitor_id))  { 
//            $onSitePaymentObj = \App\Models\Acquisition\Monitoring\PaymentMonitor::find($contractProgressObj->payment_monitor_id);
//            if(!is_null($onSitePaymentObj)){
//               $onSitePaymentObj->update(['payment_percent' => $request->on_site_payment_percent]);
//            }
//         }
     
         
        $contractProgressObj->update(['on_site_payment_percent' => $request->on_site_payment_percent]);      
        return response()->api(null, __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contractProgress = \App\Models\Acquisition\Monitoring\ContractProgress::find($id);
        $siteVisit = \App\Models\Acquisition\Monitoring\SiteVisit::find($contractProgress->site_visit_id);
        
        //return response()->json
        return response()->json(['contractProgress'=>$contractProgress,'siteVisit'=>$siteVisit]);
        //return \App\Models\Acquisition\Monitoring\ContractProgress::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO -change : \App\Models\Acquisition\Monitoring\SiteVisit::hashslug($id)->delete();

        return response()->api(null, __('Rekod telah berjaya dihapuskan.'));
    }
}
