<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Monitoring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentMonitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request, [
//            'user_id'                 => 'required',
//            'sst_id'                  => 'required',
//            'department_id'           => 'required',
//            'acquisition_id'          => 'required',
//            'acquisition_approval_id' => 'required',
//            'name'                    => 'required|min:10|max:255',
//            'remarks'                 => 'required|min:10|max:255',
//            'visited_by'              => 'required',
//        ]);

        $data = $request->only(
 //           'user_id',
 //           'sst_id',
 //           'department_id',
            'acquisition_id',
 //           'acquisition_approval_id',
 //           'payment_at',
            'payment_percent'
  //          'visited_by',
  //          'percentage_completed'
        );
        
        
         //add update contract progress
        $id_contract_progress = $request->id_contract_progress;
        $contractProgressObj = \App\Models\Acquisition\Monitoring\ContractProgress::find($id_contract_progress);
               // ->update(['deleted' => 1]);
        
        //$data['visited_at'] = now();
        $data['payment_at'] = $contractProgressObj->on_plan_date;
        
        
        //$site_visit = \App\Models\Acquisition\Monitoring\SiteVisit::create($data);
        $payment_monitor = \App\Models\Acquisition\Monitoring\PaymentMonitor::create($data);
        
        
        //$site_visit->addDocument($request, 'document');
        
       
        $contractProgressObj->update(['payment_monitor_id' => $payment_monitor->id]);
        
        
        

        return response()->api(null, __('Rekod telah berjaya ditambah.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO -change : \App\Models\Acquisition\Monitoring\SiteVisit::hashslug($id)->delete();

        return response()->api(null, __('Rekod telah berjaya dihapuskan.'));
    }
}
