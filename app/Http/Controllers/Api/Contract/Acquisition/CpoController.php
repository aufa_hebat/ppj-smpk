<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Cpo;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class CpoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();
        
        $cpo = Cpo::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'user_id' => user()->id,

            'agreement_date'     => $request->input('agreement_date') ? Carbon::createFromFormat('d/m/Y', $request->input('agreement_date')) : null,
            'partial_date'       => $request->input('partial_date') ? Carbon::createFromFormat('d/m/Y', $request->input('partial_date')) : null,
            'assessment_date'    => $request->input('assessment_date') ? Carbon::createFromFormat('d/m/Y', $request->input('assessment_date')) : null,
            'cpo_start_date'     => $request->input('cpo_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cpo_start_date')) : null,
            'cpo_end_date'       => $request->input('cpo_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cpo_end_date')) : null,
            'estimation_amount'  => $request->input('estimation_amount') ? money()->toMachine($request->input('estimation_amount')) : null,
            'cpo_amount'         => $request->input('cpo_amount') ? money()->toMachine($request->input('cpo_amount')) : null,
            'lad_per_day'        => $request->input('lad_per_day') ? money()->toMachine($request->input('lad_per_day')) : null,
            'clause'             => $request->input('clause'),
            'reference_no'       => $request->input('reference_no'),
            'part_of_work_title' => $request->input('part_of_work_title'),
        ]);

        $cpo_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $cpo->sst->acquisition,
            'doc_type'            => 'CPO',
            'doc_id'              => $cpo->id,
        ];
        common()->upload_document($cpo_requirements);

        audit($cpo, __('Sijil Siap Kerja Separa bagi perolehan ' .$cpo->sst->acquisition->reference. ' : ' .$cpo->sst->acquisition->title. ' telah berjaya disimpan.'));

        $workflow = WorkflowTask::where('acquisition_id', $cpo->sst->acquisition_id)->where('workflow_team_id',1)->first();
        $workflow->flow_desc = 'Sijil Siap Kerja Separa : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'CPO';
        $workflow->url = '/post/cpo/'.$sst->hashslug.'/edit';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;

        $workflow2 = WorkflowTask::where('acquisition_id', $cpo->sst->acquisition_id)->where('workflow_team_id',2)->first();
        $workflow2->flow_location = 'CPO';

        $workflow->update();
        $workflow2->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
