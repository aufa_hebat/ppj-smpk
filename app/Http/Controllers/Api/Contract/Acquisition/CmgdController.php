<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Cmgd;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class CmgdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        $cmgd = Cmgd::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'user_id' => user()->id,

            'reference_no'    => $request->input('reference_no'),
            'mgd_date'        => $request->input('mgd_date') ? Carbon::createFromFormat('d/m/Y', $request->input('mgd_date')) : null,
            'signature_date'  => $request->input('signature_date') ? Carbon::createFromFormat('d/m/Y', $request->input('signature_date')) : null,
            'site_visit_date' => $request->input('site_visit_date') ? Carbon::createFromFormat('d/m/Y', $request->input('site_visit_date')) : null,
            'clause'          => $request->input('clause'),
        ]);

        $cmgd_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $cmgd->sst->acquisition,
            'doc_type'            => 'CMGD',
            'doc_id'              => $cmgd->id,
        ];
        common()->upload_document($cmgd_requirements);

        audit($cmgd, __('Sijil Siap Membaiki Kecacatan bagi perolehan ' .$cmgd->sst->acquisition->reference. ' : ' .$cmgd->sst->acquisition->title. ' telah berjaya disimpan.'));

        $workflow = WorkflowTask::where('acquisition_id', $cmgd->sst->acquisition_id)->where('workflow_team_id',1)->first();
        $workflow->flow_desc = 'Sijil Siap Membaiki Kecacatan : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'CMGD';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;
        $workflow->url = '/post/cmgd/'.$sst->hashslug.'/edit';

        $workflow2 = WorkflowTask::where('acquisition_id', $cmgd->sst->acquisition_id)->where('workflow_team_id',2)->first();
        $workflow2->flow_location = 'CMGD';
        $workflow2->role_id = 4;

        $workflow->update();
        $workflow2->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *site_visit_date.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
