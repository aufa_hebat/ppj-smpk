<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Cpc;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class CpcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        $cpc = Cpc::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'user_id' => user()->id,

            'dlp_start_date'  => $request->input('dlp_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('dlp_start_date')) : null,
            'dlp_expiry_date' => $request->input('dlp_expiry_date') ? Carbon::createFromFormat('d/m/Y', $request->input('dlp_expiry_date')) : null,
            'signature_date'  => $request->input('signature_date') ? Carbon::createFromFormat('d/m/Y', $request->input('signature_date')) : null,
            'test_award_date' => $request->input('test_award_date') ? Carbon::createFromFormat('d/m/Y', $request->input('test_award_date')) : null,
            'clause_1'        => $request->input('clause_1'),
            'clause_2'        => $request->input('clause_2'),
        ]);

        $cpc_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $cpc->sst->acquisition,
            'doc_type'            => 'CPC',
            'doc_id'              => $cpc->id,
        ];
        common()->upload_document($cpc_requirements);

        audit($cpc, __('Sijil Siap Kerja bagi perolehan ' .$cpc->sst->acquisition->reference. ' : ' .$cpc->sst->acquisition->title. ' telah berjaya disimpan.'));

        $workflow = WorkflowTask::where('acquisition_id', $cpc->sst->acquisition_id)->where('workflow_team_id',1)->first();
        $workflow->flow_desc = 'Sijil Siap Kerja : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'CPC';
        $workflow->url = '/post/cpc/'.$sst->hashslug.'/edit';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;

        $workflow2 = WorkflowTask::where('acquisition_id', $cpc->sst->acquisition_id)->where('workflow_team_id',2)->first();
        $workflow2->flow_location = 'CPC';

        $workflow->update();
        $workflow2->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
