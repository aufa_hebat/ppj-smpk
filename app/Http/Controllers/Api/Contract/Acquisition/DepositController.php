<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Deposit;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        $deposit = Deposit::updateOrCreate(
            [
                'sst_id' => $sst->id,
            ],
            [
                'user_id'  => user()->id,
                'bon_type' => $request->input('deposit_bon_type'),

                'reference_no'       => $request->input('deposit_reference_no'),
                'prime_cost'         => $request->input('deposit_prime_cost') ? money()->toMachine($request->input('deposit_prime_cost')) : null,
                'amount'             => $request->input('deposit_amount') ? money()->toMachine($request->input('deposit_amount')) : null,
                'total_amount'       => $request->input('deposit_total_amount') ? money()->toMachine($request->input('deposit_total_amount')) : null,
                'qualified_amount'   => $request->input('deposit_qualified_amount') ? money()->toMachine($request->input('deposit_qualified_amount')) : null,
                'application_amount' => $request->input('deposit_application_amount') ? money()->toMachine($request->input('deposit_application_amount')) : null,

                'bank_name'                   => $request->input('deposit_bank_name'),
                'bank_no'                     => $request->input('deposit_bank_no'),
                'bank_start_date'             => $request->input('deposit_bank_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_bank_start_date')) : null,
                'bank_proposed_date'          => $request->input('deposit_bank_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_bank_proposed_date')) : null,
                'bank_approval_received_date' => $request->input('deposit_bank_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_bank_approval_received_date')) : null,
                'bank_received_date'          => $request->input('deposit_bank_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_bank_received_date')) : null,
                'bank_amount'                 => $request->input('deposit_bank_amount') ? money()->toMachine($request->input('deposit_bank_amount')) : null,
                'bank_expired_date'           => $request->input('deposit_bank_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_bank_expired_date')) : null,
                'bank_approval_proposed_date' => $request->input('deposit_bank_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_bank_approval_proposed_date')) : null,

                'insurance_name'                   => $request->input('deposit_insurance_name'),
                'insurance_no'                     => $request->input('deposit_insurance_no'),
                'insurance_start_date'             => $request->input('deposit_insurance_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_insurance_start_date')) : null,
                'insurance_proposed_date'          => $request->input('deposit_insurance_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_insurance_proposed_date')) : null,
                'insurance_approval_received_date' => $request->input('deposit_insurance_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_insurance_approval_received_date')) : null,
                'insurance_received_date'          => $request->input('deposit_insurance_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_insurance_received_date')) : null,
                'insurance_amount'                 => $request->input('deposit_insurance_amount') ? money()->toMachine($request->input('deposit_insurance_amount')) : null,
                'insurance_expired_date'           => $request->input('deposit_insurance_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_insurance_expired_date')) : null,
                'insurance_approval_proposed_date' => $request->input('deposit_insurance_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('deposit_insurance_approval_proposed_date')) : null,
            ]);

        $deposit_requirements = [
            'request_hDocumentId' => $request->hDocumentId_deposit,
            'request_hasFile'     => $request->hasFile('uploadFile_deposit'),
            'request_file'        => $request->file('uploadFile_deposit'),
            'acquisition'         => $deposit->sst->acquisition,
            'doc_type'            => 'DEPOSIT',
            'doc_id'              => $deposit->id,
        ];
        common()->upload_document($deposit_requirements);

        $workflow_check = WorkflowTask::where('acquisition_id',$deposit->sst->acquisition_id)->where('flow_location','Wang Pendahuluan')->first();

        if(!empty($workflow_check)){
            $workflow = WorkflowTask::where('acquisition_id',$deposit->sst->acquisition_id)->where('flow_location','Wang Pendahuluan')->where('workflow_team_id',1)->first();
            $workflow2 = WorkflowTask::where('acquisition_id',$deposit->sst->acquisition_id)->where('flow_location','Wang Pendahuluan')->where('workflow_team_id',2)->first();
            audit($deposit, __('Wang Pendahuluan bagi perolehan ' .$deposit->sst->acquisition->reference. ' : ' .$deposit->sst->acquisition->title. ' telah berjaya dikemaskini.'));
        } else {
            $workflow = new WorkflowTask();
            $workflow2 = new WorkflowTask();
            audit($deposit, __('Wang Pendahuluan bagi perolehan ' .$deposit->sst->acquisition->reference. ' : ' .$deposit->sst->acquisition->title. ' telah berjaya disimpan.'));
        }

        $workflow->acquisition_id = $sst->acquisition->id;
        $workflow->flow_name = $sst->acquisition->reference . ' : ' . $sst->acquisition->title;
        $workflow->flow_desc = 'Wang Pendahuluan : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->url = '/contract/post/'.$sst->hashslug.'/edit#cash-deposit';
        $workflow->flow_location = 'Wang Pendahuluan';
        $workflow->is_claimed = 1;
        $workflow->user_id = $sst->user_id;
        $workflow->role_id = 4;
        $workflow->workflow_team_id = 1;

        $workflow2->acquisition_id = $sst->acquisition->id;
        $workflow2->flow_name = $sst->acquisition->reference . ' : ' . $sst->acquisition->title;
        $workflow2->flow_desc = 'Semakan : Selesai.';
        $workflow2->flow_location = 'Wang Pendahuluan';
        $workflow2->is_claimed = 1;
        $workflow2->user_id = null;
        $workflow2->workflow_team_id = 2;
        $workflow2->url = '/contract/post/'.$sst->hashslug.'/edit#cash-deposit';

        $workflow->save();
        $workflow2->save();

        return response()->api([], __('Rekod telah berjaya disimpan.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deposit = Deposit::find($id);
        if(!empty($deposit)){
            $deposit->delete();
        }
        
        return response()->api([], __('Rekod telah berjaya dipadam.'));
    }
}
