<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Document;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class ContractDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        $doc = Document::updateOrCreate(
            [
                'sst_id' => $sst->id,
            ],
            [
                'user_id' => user()->id,

                'revenue_stamp_date' => $request->input('document_revenue_stamp_date') ? Carbon::createFromFormat('d/m/Y', $request->input('document_revenue_stamp_date')) : null,
                'preparation_period' => $request->input('document_preparation_period') ? str_replace(' Hari', '', $request->input('document_preparation_period')) : null,
                'proposed_date'      => $request->input('document_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('document_proposed_date')) : null,
                'submitted_date'     => $request->input('document_submitted_date') ? Carbon::createFromFormat('d/m/Y', $request->input('document_submitted_date')) : null,
                'base_lending_rate'  => $request->input('document_blr'),
                'LAD_rate'           => $request->input('document_LAD_amount') ? money()->toMachine(str_replace('RM ', '', $request->input('document_LAD_amount'))) : null,
                'late_days'          => $request->input('document_late_days') ? str_replace(' Hari', '', $request->input('document_late_days')) : null,
        ]);

        $workflow_check = WorkflowTask::where('acquisition_id',$sst->acquisition_id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first();

        if(!empty($workflow_check)){
            $workflow = WorkflowTask::where('acquisition_id',$sst->acquisition_id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',1)->first();
            $workflow2 = WorkflowTask::where('acquisition_id',$sst->acquisition_id)->where('flow_location','Dokumen Kontrak')->where('workflow_team_id',2)->first();

            audit($doc, __('Dokumen Kontrak bagi perolehan ' .$doc->sst->acquisition->reference. ' : ' .$doc->sst->acquisition->title. ' telah berjaya dikemaskini.'));
        } else {
            $workflow = new WorkflowTask();
            $workflow2 = new WorkflowTask();

            audit($doc, __('Dokumen Kontrak bagi perolehan ' .$doc->sst->acquisition->reference. ' : ' .$doc->sst->acquisition->title. ' telah berjaya disimpan.'));
        }

        $workflow->acquisition_id = $sst->acquisition->id;
        $workflow->flow_name = $sst->acquisition->reference . ' : ' . $sst->acquisition->title;
        $workflow->flow_desc = 'Dokumen Kontrak : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->url = '/contract/post/'.$sst->hashslug.'/edit#document-contract';
        $workflow->flow_location = 'Dokumen Kontrak';
        $workflow->is_claimed = 1;
        $workflow->user_id = $sst->user_id;
        $workflow->role_id = 4;
        $workflow->workflow_team_id = 1;

        $workflow2->acquisition_id = $sst->acquisition->id;
        $workflow2->flow_name = $sst->acquisition->reference . ' : ' . $sst->acquisition->title;
        $workflow2->flow_desc = 'Semakan : Selesai.';
        $workflow2->flow_location = 'Dokumen Kontrak';
        $workflow2->is_claimed = 1;
        $workflow2->user_id = null;
        $workflow2->workflow_team_id = 2;
        $workflow2->url = '/contract/post/'.$sst->hashslug.'/edit#document-contract';

        $workflow->save();
        $workflow2->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
