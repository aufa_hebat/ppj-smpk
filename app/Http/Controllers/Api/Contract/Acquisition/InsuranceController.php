<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Insurance;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\WorkflowTask;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        if(!$request->hasFile('uploadFile_publicLiability') && !(!empty($request->hDocumentId_publicLiability) && count($request->hDocumentId_publicLiability)>0)){
            return response()->json(['type'=>'error','title'=>'Tanggungan Awam','text'=>'Sila muat naik document.']);
        }

        if(!$request->hasFile('uploadFile_work') && !(!empty($request->hDocumentId_work) && count($request->hDocumentId_work)>0)){
            return response()->json(['type'=>'error','title'=>'Insurans Kerja','text'=>'Sila muat naik document.']);
        }

        if(!$request->hasFile('uploadFile_compensation') && !(!empty($request->hDocumentId_compensation) && count($request->hDocumentId_compensation)>0)){
            return response()->json(['type'=>'error','title'=>'Insurans Pampasan Pekerja','text'=>'Sila muat naik document.']);
        }

        $sst = Sst::where('hashslug', $id)->first();

        $insurance = Insurance::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'user_id' => user()->id,

            'public_insurance_name' => $request->input('public_insurance_name'),
            'public_policy_no'      => $request->input('public_policy_no'),
            'public_policy_amount'  => $request->input('public_policy_amount') ? money()->toMachine($request->input('public_policy_amount')) : null,
            'public_received_date'  => $request->input('public_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_received_date')) : null,
            'public_start_date'     => $request->input('public_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_start_date')) : null,
            'public_expired_date'   => $request->input('public_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_expired_date')) : null,
            'public_proposed_date'  => $request->input('public_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_proposed_date')) : null,

            'work_insurance_name' => $request->input('work_insurance_name'),
            'work_policy_no'      => $request->input('work_policy_no'),
            'work_policy_amount'  => $request->input('work_policy_amount') ? money()->toMachine($request->input('work_policy_amount')) : null,
            'work_received_date'  => $request->input('work_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_received_date')) : null,
            'work_start_date'     => $request->input('work_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_start_date')) : null,
            'work_expired_date'   => $request->input('work_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_expired_date')) : null,
            'work_proposed_date'  => $request->input('work_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_proposed_date')) : null,

            'compensation_insurance_name' => $request->input('compensation_insurance_name'),
            'compensation_policy_no'      => $request->input('compensation_policy_no'),
            'compensation_policy_amount'  => $request->input('compensation_policy_amount') ? money()->toMachine($request->input('compensation_policy_amount')) : null,
            'compensation_received_date'  => $request->input('compensation_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_received_date')) : null,
            'compensation_start_date'     => $request->input('compensation_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_start_date')) : null,
            'compensation_expired_date'   => $request->input('compensation_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_expired_date')) : null,
            'compensation_proposed_date'  => $request->input('compensation_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_proposed_date')) : null,
        ]);

        $publicLiability_requirements = [
            'request_hDocumentId' => $request->hDocumentId_publicLiability,
            'request_hasFile'     => $request->hasFile('uploadFile_publicLiability'),
            'request_file'        => $request->file('uploadFile_publicLiability'),
            'acquisition'         => $insurance->sst->acquisition,
            'doc_type'            => 'INSURANCE_PUBLIC',
            'doc_id'              => $insurance->id,
        ];
        common()->upload_document($publicLiability_requirements);

        $work_requirements = [
            'request_hDocumentId' => $request->hDocumentId_work,
            'request_hasFile'     => $request->hasFile('uploadFile_work'),
            'request_file'        => $request->file('uploadFile_work'),
            'acquisition'         => $insurance->sst->acquisition,
            'doc_type'            => 'INSURANCE_WORK',
            'doc_id'              => $insurance->id,
        ];
        common()->upload_document($work_requirements);

        $compensation_requirements = [
            'request_hDocumentId' => $request->hDocumentId_compensation,
            'request_hasFile'     => $request->hasFile('uploadFile_compensation'),
            'request_file'        => $request->file('uploadFile_compensation'),
            'acquisition'         => $insurance->sst->acquisition,
            'doc_type'            => 'INSURANCE_COMPENSATION',
            'doc_id'              => $insurance->id,
        ];
        common()->upload_document($compensation_requirements);

        $workflow_check = WorkflowTask::where('acquisition_id',$insurance->sst->acquisition_id)->where('flow_location','Insurans')->first();

        if(!empty($workflow_check)){
            $workflow = WorkflowTask::where('acquisition_id',$insurance->sst->acquisition_id)->where('flow_location','Insurans')->where('workflow_team_id',1)->first();
            $workflow2 = WorkflowTask::where('acquisition_id',$insurance->sst->acquisition_id)->where('flow_location','Insurans')->where('workflow_team_id',2)->first();

            audit($insurance, __('Insurans bagi perolehan ' .$insurance->sst->acquisition->reference. ' : ' .$insurance->sst->acquisition->title. ' telah berjaya dikemaskini.'));
        } else {
            $workflow = new WorkflowTask();
            $workflow2 = new WorkflowTask();

            audit($insurance, __('Insurans bagi perolehan ' .$insurance->sst->acquisition->reference. ' : ' .$insurance->sst->acquisition->title. ' telah berjaya disimpan.'));
        }

        $workflow->acquisition_id = $sst->acquisition->id;
        $workflow->flow_name = $sst->acquisition->reference . ' : ' . $sst->acquisition->title;
        $workflow->flow_desc = 'Insurans : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->url = '/contract/post/'.$sst->hashslug.'/edit#insurance';
        $workflow->flow_location = 'Insurans';
        $workflow->is_claimed = 1;
        $workflow->user_id = $sst->user_id;
        $workflow->role_id = 4;
        $workflow->workflow_team_id = 1;

        $workflow2->acquisition_id = $sst->acquisition->id;
        $workflow2->flow_name = $sst->acquisition->reference . ' : ' . $sst->acquisition->title;
        $workflow2->flow_desc = 'Semakan : Selesai.';
        $workflow2->flow_location = 'Insurans';
        $workflow2->is_claimed = 1;
        $workflow2->user_id = null;
        $workflow2->workflow_team_id = 2;
        $workflow2->url = '/contract/post/'.$sst->hashslug.'/edit#insurance';

        $workflow->save();
        $workflow2->save();

        return response()->json(['type'=>'success','title'=>'Insuran','text'=>'Rekod telah berjaya dikemaskini.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
