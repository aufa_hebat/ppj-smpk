<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DatetimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'advertised_at' => 'required',
            'start_sale_at' => 'required',
            'closed_at' => 'required',
            'closed_sale_at' => 'required',
        ], [
            'advertised_at.required' => __('Sila Masukkan Tarikh Notis'),
            'start_sale_at.required' => __('Sila Masukkan Tarikh Mula Jualan'),
            'closed_at.required' => __('Sila Masukkan Tarikh Tutup Perolehan'),
            'closed_sale_at.required' => __('Sila Masukkan Tarikh Tutup Jualan'),
        ]);

        Acquisition::findByHashSlug($id)->update([
            'advertised_at'     => (! empty($request->advertised_at)) ? Carbon::createFromFormat('d/m/Y', $request->advertised_at) : null,
            'start_sale_at'     => (! empty($request->start_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->start_sale_at) : null,
            'closed_at'         => (! empty($request->closed_at)) ? Carbon::createFromFormat('d/m/Y', $request->closed_at) : null,
            'closed_sale_at'    => (! empty($request->closed_sale_at)) ? Carbon::createFromFormat('d/m/Y', $request->closed_sale_at) : null,
            'briefing_status'   => (isset($request->briefing_status)) ? true : false,
            'briefing_at'       => (isset($request->briefing_status) && ! empty($request->briefing_at)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->briefing_at) : null,
            'briefing_location' => $request->briefing_location,
            'visit_status'      => (isset($request->visit_status)) ? true : false,
            'visit_date_time'   => (isset($request->visit_status) && ! empty($request->visit_date_time)) ? Carbon::createFromFormat('d/m/Y g:i A', $request->visit_date_time) : null,
            'visit_location'    => $request->visit_location,
        ]);

        $acq = Acquisition::findByHashSlug($id);

        audit($acq, __('Tarikh dan masa dokumen perolehan ' .$acq->reference. ' : ' .$acq->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
