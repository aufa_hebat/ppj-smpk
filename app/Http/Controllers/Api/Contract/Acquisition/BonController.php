<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Bon;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\WorkflowTask;

class BonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        if(!$request->hasFile('uploadFile') && !(!empty($request->hDocumentId) && count($request->hDocumentId)>0)){
            return response()->json(['type'=>'error','title'=>'Bon Pelaksanaan','text'=>'Sila muat naik document.']);
        }

        $sst = Sst::where('hashslug', $id)->first();

        $bon = Bon::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'user_id'  => user()->id,
            'bon_type' => $request->input('bon_type'),

            'bank_name'          => $request->input('bank_name'),
            'bank_no'            => $request->input('bank_no'),
            'bank_start_date'    => $request->input('bank_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_start_date')) : null,
            'bank_proposed_date' => $request->input('bank_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_proposed_date')) : null,
            'bank_received_date' => $request->input('bank_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_received_date')) : null,
            'bank_amount'        => $request->input('bank_amount') ? money()->toMachine($request->input('bank_amount')) : null,
            'bank_expired_date'  => $request->input('bank_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('bank_expired_date')) : null,

            'insurance_name'          => $request->input('insurance_name'),
            'insurance_no'            => $request->input('insurance_no'),
            'insurance_start_date'    => $request->input('insurance_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_start_date')) : null,
            'insurance_proposed_date' => $request->input('insurance_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_proposed_date')) : null,
            'insurance_received_date' => $request->input('insurance_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_received_date')) : null,
            'insurance_amount'        => $request->input('insurance_amount') ? money()->toMachine($request->input('insurance_amount')) : null,
            'insurance_expired_date'  => $request->input('insurance_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('insurance_expired_date')) : null,

            'cheque_name'           => $request->input('cheque_name'),
            'cheque_resit_no'       => $request->input('cheque_resit_no'),
            'cheque_received_date'  => $request->input('cheque_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cheque_received_date')) : null,
            'cheque_no'             => $request->input('cheque_no'),
            'cheque_submitted_date' => $request->input('cheque_submitted_date') ? Carbon::createFromFormat('d/m/Y', $request->input('cheque_submitted_date')) : null,

            'money_received_at' => $request->input('money_received_at') ? Carbon::createFromFormat('d/m/Y', $request->input('money_received_at')) : null,
            'money_amount'      => $request->input('money_amount') ? money()->toMachine($request->input('money_amount')) : null,
        ]);

        $bon_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $bon->sst->acquisition,
            'doc_type'            => 'BON',
            'doc_id'              => $bon->id,
        ];
        common()->upload_document($bon_requirements);

        audit($bon, __('Bon Pelaksanaan bagi perolehan ' .$bon->sst->acquisition->reference. ' : ' .$bon->sst->acquisition->title. ' telah berjaya disimpan.'));

        $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition->id)->where('workflow_team_id',1)->first();

        $workflow->flow_desc = 'Bon Pelaksanaan : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->url = '/contract/post/'.$sst->hashslug.'/edit#bon-implementation';
        $workflow->flow_location = 'Bon Pelaksanaan';
        $workflow->is_claimed = 1;
        $workflow->user_id = $sst->user_id;
        $workflow->role_id = 4;
        $workflow->workflow_team_id = 1;

        $workflow2 = WorkflowTask::where('acquisition_id', $sst->acquisition->id)->where('workflow_team_id',2)->first();
        $workflow2->flow_location = 'Bon Pelaksanaan';
        $workflow2->role_id = 4;

        $workflow->update();
        $workflow2->update();

        return response()->json(['type'=>'success','title'=>'Bon Pelaksanaan','text'=>'Rekod telah berjaya dikemaskini.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
