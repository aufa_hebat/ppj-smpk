<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Requests\CsvImportRequest;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use App\Models\CSV\Preview;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class QuantityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $path = $request->file('csv_file')->getRealPath();

        if ($request->has('header')) {
            $data = Excel::load($path, function ($reader) {})->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        if (count($data) > 0) {
            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }
            $data_array = [];
            SubItem::where('acquisition_id', '=', $request->acquisition_id)->delete();
            Item::where('acquisition_id', '=', $request->acquisition_id)->delete();
            Element::where('acquisition_id', '=', $request->acquisition_id)->delete();
            foreach ($data as $ky => $a) {
                
                $elemenBQ = new Element();
                $itemBQ   = new Item();
                $subBQ    = new SubItem();
                $data_array[] = $data[$ky];
                
                if ('0.0' != $a['no_elemen'] && '0.0' == $a['no_item'] && '0.0' == $a['no_sub_item']) {
                    
                    $elemenBQ->acquisition_id   = $request->acquisition_id;
                    $elemenBQ->status           = 'A';
                    $elemenBQ->created_by       = user()->id;
                    $elemenBQ->no               = $a['no_elemen'];
                    $elemenBQ->element          = strtoupper($a['keterangan']);
                    $elemenBQ->description      = $a['huraian'];
                    $elemenBQ->amount           = money()->toMachine(preg_replace('/[,]/', '', $a['jumlah']) ?? 0);
                    $elemenBQ->save();
                }elseif ('0.0' != $a['no_elemen'] && '0.0' != $a['no_item'] && '0.0' == $a['no_sub_item']) {
                    $temp_el = Element::where('acquisition_id',$request->acquisition_id)->where('no',$a['no_elemen'])->where('deleted_at',null)->first();
                    $itemBQ->bq_element_id      = $temp_el->id ?? null;
                    $itemBQ->acquisition_id     = $request->acquisition_id;
                    $itemBQ->status             = 'A';
                    $itemBQ->created_by         = user()->id;
                    $itemBQ->bq_no_element      = $a['no_elemen'];
                    $itemBQ->no                 = $a['no_item'];
                    $itemBQ->item               = strtoupper($a['keterangan']);
                    $itemBQ->description        = $a['huraian'];
                    $itemBQ->amount             = money()->toMachine(preg_replace('/[,]/', '', $a['jumlah']) ?? 0);
                    $itemBQ->save();
                }elseif ('0.0' != $a['no_elemen'] && '0.0' != $a['no_item'] && '0.0' != $a['no_sub_item']) {
                    $temp_el = Element::where('acquisition_id',$request->acquisition_id)->where('no',$a['no_elemen'])->where('deleted_at',null)->first();
                    $temp_it = Item::where('acquisition_id',$request->acquisition_id)->where('bq_no_element',$a['no_elemen'])->where('no',$a['no_item'])->where('deleted_at',null)->first();
                    $subBQ->bq_element_id       = $temp_el->id ?? null;
                    $subBQ->bq_item_id          = $temp_it->id ?? null;
                    $subBQ->acquisition_id      = $request->acquisition_id;
                    $subBQ->status              = 'A';
                    $subBQ->created_by          = user()->id;
                    $subBQ->bq_no_element       = $a['no_elemen'];
                    $subBQ->bq_no_item          = $a['no_item'];
                    $subBQ->no                  = $a['no_sub_item'];
                    $subBQ->item                = $a['keterangan'];
                    $subBQ->description         = $a['huraian'];
                    $subBQ->unit                = $a['unit'];
                    $subBQ->quantity            = money()->toMachine(preg_replace('/[,]/', '', $a['kuantiti']) ?? 0);
                    $subBQ->baki_quantity       = money()->toMachine(preg_replace('/[,]/', '', $a['kuantiti']) ?? 0);
                    $subBQ->paid_quantity       = 0;
                    $subBQ->rate_per_unit       = money()->toMachine(preg_replace('/[,]/', '', $a['kadar_harga_seunit']) ?? 0);
                    $subBQ->amount              = money()->toMachine(preg_replace('/[,]/', '', $a['jumlah']) ?? 0);
                    $subBQ->save();
                }
            }
            $csv_data      = $data_array;
            $csv_data_file = Preview::create([
                'file'   => $request->file('csv_file')->getClientOriginalName(),
                'header' => $request->has('header'),
                'data'   => json_encode($data),
            ]);
        } else {
            return redirect()->back();
        }
        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function bqPaging(Request $request, $id)
    {
        if(!empty($request->pageStart)){
            foreach($request->pageStart as $keyS => $pgStart){
                $element = Element::where('id', $keyS)->update([
                    'paging_start'  => $pgStart,
                ]);
            }
        }
        if(!empty($request->pageEnd)){
            foreach($request->pageEnd as $keyE => $pgEnd){
                $element = Element::where('id', $keyE)->update([
                    'paging_end'  => $pgEnd,
                ]);
            }
        }        

        // if(!empty($request->pageItemStart)){
        //     foreach($request->pageItemStart as $keyIS => $pgIStart){
        //         $item = Item::where('id', $keyIS)->update([
        //             'paging_start'  => $pgIStart,
        //         ]);
        //     }
        // }
        // if(!empty($request->pageItemEnd)){
        //     foreach($request->pageItemEnd as $keyIE => $pgIEnd){
        //         $item = Item::where('id', $keyIE)->update([
        //             'paging_end'  => $pgIEnd,
        //         ]);
        //     }
        // }        

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
