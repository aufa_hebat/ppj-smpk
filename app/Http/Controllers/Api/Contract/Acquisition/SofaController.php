<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Acquisition\Sofa;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\WorkflowTask;

class SofaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        $sst->sofa_signature_date = (! empty($request->signature_date)) ? Carbon::createFromFormat('d/m/Y', $request->signature_date) : null;
        $sst->update();

        $appointed                      = AppointedCompany::where('acquisition_id', $sst->acquisition_id)->whereIn('company_id', $sst->pluck('company_id'))->first();
        $appointed->sofa_signature_date = (! empty($request->signature_date)) ? Carbon::createFromFormat('d/m/Y', $request->signature_date) : null;
        $appointed->update();

        $sofa = Sofa::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'signature_date' => $request->input('signature_date') ? Carbon::createFromFormat('d/m/Y', $request->input('signature_date')) : null,
        ]);

        $sofa_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $sofa->sst->acquisition,
            'doc_type'            => 'SOFA',
            'doc_id'              => $sofa->id,
        ];
        common()->upload_document($sofa_requirements);

        $workflow = WorkflowTask::where('acquisition_id', $sofa->sst->acquisition_id)->where('flow_location', 'SOFA')->first();
        $workflow->flow_desc = 'Bayaran Muktamad : Kontrak sudah berjaya.';
        $workflow->is_claimed = 1;
        $workflow->user_id = null;
        $workflow->workflow_team_id = 1;

        $workflow->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
