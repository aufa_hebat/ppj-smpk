<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Approval;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use App\Models\Acquisition\Approval\Financial;
use Illuminate\Http\Request;

class BudgetCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'business_area'    => 'required',
            'cost_center'      => 'required',
            'fund'             => 'required',
            'funded_programme' => 'required',
            'gl_account'       => 'required',
            'functional_area'  => 'required',
        ], [
            'business_area.required' => __('Sila Masukkan Business Area'),
            'cost_center.required'       => __('Sila Masukkan Cost Center'),
            'fund.required'       => __('Sila Masukkan Fund'),
            'funded_programme.required'       => __('Sila Masukkan Funded Programme'),
            'gl_account.required'       => __('Sila Masukkan GL Account'),
            'functional_area.required'       => __('Sila Masukkan Functional Area'),
        ]);

        $approval = Approval::findByHashSlug($request->hashslug);

        $kodBudget = $approval->financials()->where([
            ['business_area_id', $request->business_area],
            ['functional_area_id', $request->functional_area],
            ['cost_center_id', $request->cost_center],
            ['fund_id', $request->fund],
            ['funded_programme_id', $request->funded_programme],
            ['gl_account_id', $request->gl_account],
        ])->count();

        if ($kodBudget > 0) {
            return response()->json(['message'=>'Rekod yang dimasukkan telah ada.','type'=>'E']);
        }else{
            
        }

        $approval->financials()->create([
            'business_area_id'    => $request->business_area,
            'functional_area_id'  => $request->functional_area,
            'cost_center_id'      => $request->cost_center,
            'fund_id'             => $request->fund,
            'funded_programme_id' => $request->funded_programme,
            'gl_account_id'       => $request->gl_account,
        ]);

        audit($approval, __('Maklumat kod bajet perolehan ' .$approval->title. ' telah berjaya ditambah.'));
        
        return response()->json(['message'=>'Rekod telah berjaya dijana.', 'type'=>'S']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'business_area_id'    => 'required',
            'cost_center_id'      => 'required',
            'fund_id'             => 'required',
            'funded_programme_id' => 'required',
            'functional_area_id'  => 'required',
            'gl_account_id'       => 'required',
        ], [
            'business_area.required' => __('Sila Masukkan Business Area'),
            'cost_center.required'       => __('Sila Masukkan Cost Center'),
            'fund.required'       => __('Sila Masukkan Fund'),
            'funded_programme.required'       => __('Sila Masukkan Funded Programme'),
            'gl_account.required'       => __('Sila Masukkan GL Account'),
            'functional_area.required'       => __('Sila Masukkan Functional Area'),
        ]);

        $financial = Financial::findByHashSlug($id);

        $kodBudget = $financial->approval->financials()->where([
            ['business_area_id', $request->business_area],
            ['functional_area_id', $request->functional_area],
            ['cost_center_id', $request->cost_center],
            ['fund_id', $request->fund],
            ['funded_programme_id', $request->funded_programme],
            ['gl_account_id', $request->gl_account],
        ])->count();

        if ($kodBudget > 0) {
            //return response()->api([], __('Rekod yang dimasukkan telah ada.'));
            return response()->json(['message'=>'Rekod yang dimasukkan telah ada.','type'=>'E']);
        }


        //$financial = Financial::findByHashSlug($id);

        $financial->update([
            'business_area_id'    => $request->business_area_id,
            'cost_center_id'      => $request->cost_center_id,
            'fund_id'             => $request->fund_id,
            'funded_programme_id' => $request->funded_programme_id,
            'functional_area_id'  => $request->functional_area_id,
            'gl_account_id'       => $request->gl_account_id,
        ]);

        audit($financial, __('Maklumat kod bajet perolehan telah berjaya dikemaskini.'));

        //return response()->api([], __('Rekod telah berjaya dikemaskini.'));
        return response()->json(['message'=>'Rekod telah berjaya dikemaskini.', 'type'=>'S']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $financial = Financial::findByHashSlug($id);

        audit($financial, __('Maklumat kod bajet perolehan telah berjaya dihapuskan.'));

        $financial->delete();

        return response()->api([], __('Rekid telah berjaya dihapuskan.'));
    }
}
