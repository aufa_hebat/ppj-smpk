<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Approval;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'allocation_resource_id' => 'required',
            'est_cost_project'       => 'required|min:1',
        ], [
            'allocation_resource_id.required' => __('Sila Masukkan Sumber peruntukan'),
            'est_cost_project.required'       => __('Sila Masukkan Anggaran kos projek'),
        ]);

        Approval::hashslug($id)->update([
            'allocation_resource_id' => $request->allocation_resource_id,
            'loyalty_code'           => trim($request->loyalty_code) ?? null,
            'est_cost_project'       => money()->toMachine($request->est_cost_project),
            'other'                  => $request->other,
        ]);

        $approval = Approval::findByHashSlug($id);

        audit($approval, __('Maklumat kewangan perolehan ' .$approval->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
