<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Approval;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use App\Models\Singleton\SingletonApproval;
use App\Models\Department;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'               => 'required|min:5',
            'acquisition_type_id' => 'required',
            'period_length'       => 'required|min:1',
            'period_type_id'      => 'required',
            'location_id'         => 'required',
        ],[
            'title.required'                    => __('Sila Masukkan Tajuk Perolehan'),
            'acquisition_type_id.required'      => __('Sila Masukkan Jenis Perolehan'),
            'period_length.required'            => __('Sila Masukkan Tempoh Perolehan'),
            'period_type_id.required'           => __('Sila Masukkan Jenis Tempoh'),
            'location_id.required'      => __('Sila Masukkan Lokasi'),
        ]);
        
        /*
        $approval = Approval::create([
            'user_id'                   => user()->id,
            'department_id'             => null !== optional(user()->department)->id ? optional(user()->department)->id : Department::ALL_ID, // All
            'section_id'                => user()->section_id,
            'executor_department_id'    => user()->executor_department_id,
            'acquisition_type_id'       => $request->acquisition_type_id,
            'period_type_id'            => $request->period_type_id,
            'title'                     => str_replace("'", '`', strtoupper($request->title)),
            'description'               => str_replace("'", '`', $request->description),
            'year'                      => $request->year,
            'file_reference'            => $request->file_reference,
            'period_length'             => $request->period_length,
            'est_cost_project'          => money()->toMachine('0'),
        ]);
        */
         //DIN test singleton
        $approval = SingletonApproval::save($request);

        foreach ($request->input('location_id') as $key => $value) {
            $approval->locations()->create([
                'acquisition_approval_id' => $approval->id,
                'location_id'             => $value,
            ]);
        }

        audit($approval, __('Kelulusan Perolehan ' .str_replace("'", '`', strtoupper($request->title)). ' telah berjaya disimpan.'));

        return response()->api(
            ['redirect' => route('contract.pre.edit', $approval->hashslug)],
            __('Rekod telah berjaya dijana.')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'               => 'required|min:5',
            'acquisition_type_id' => 'required',
            'period_length'       => 'required|min:1',
            'period_type_id'      => 'required',
            'location_id'         => 'required',
        ],[
            'title.required'                    => __('Sila Masukkan Tajuk Perolehan'),
            'acquisition_type_id.required'      => __('Sila Masukkan Jenis Perolehan'),
            'period_length.required'            => __('Sila Masukkan Tempoh Perolehan'),
            'period_type_id.required'           => __('Sila Masukkan Jenis Tempoh'),
            'location_id.required'              => __('Sila Masukkan Lokasi'),
        ]);

        Approval::hashslug($id)->update($request->except('token', 'location_id'));

        Approval::hashslug($id)->update([
            'title'       => str_replace("'", '`', strtoupper($request->title)),
            'description' => str_replace("'", '`', $request->description),
        ]);

        $approval = Approval::findByHashSlug($id);
        $approval->locations()->where('acquisition_approval_id', $approval->id)->delete();
        foreach ($request->input('location_id') as $key => $value) {
            $approval->locations()->create([
                'acquisition_approval_id' => $approval->id,
                'location_id'             => $value,
            ]);
        }

        audit($approval, __('Maklumat perincian perolehan ' .str_replace("'", '`', strtoupper($request->title)). ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
