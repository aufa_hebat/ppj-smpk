<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Approval;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use Illuminate\Http\Request;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'prepared_at'        => 'required',
            'verified_at'        => 'required',
            'approved_at'        => 'required',
            'finance_officer_id' => 'required',
            'authority_id'       => 'required',
        ], [
            'prepared_at.required'        => __('Sila Masukkan Tarikh Disediakan'),
            'verified_at.required'        => __('Sila Masukkan Tarikh Disemak'),
            'approved_at.required'        => __('Sila Masukkan Tarikh Kelulusan'),
            'finance_officer_id.required' => __('Sila Masukkan Nama Pegawai Pengulas Jab. Kewangan'),
            'authority_id.required'       => __('Sila Masukkan Kelulusan'),
        ]);
        $data = $request->only(
            'prepared_at', 'verified_at', 'approved_at',
            'finance_officer_id', 'verified_by', 'authority_id'
        );
        $data['prepared_at'] = ! empty($data['prepared_at']) ? \Carbon\Carbon::createFromFormat('d/m/Y', $data['prepared_at']) : null;
        $data['verified_at'] = ! empty($data['verified_at']) ? \Carbon\Carbon::createFromFormat('d/m/Y', $data['verified_at']) : null;
        $data['approved_at'] = ! empty($data['approved_at']) ? \Carbon\Carbon::createFromFormat('d/m/Y', $data['approved_at']) : null;

        Approval::hashslug($id)->update($data);
        
        $approval = Approval::findByHashSlug($id);

        // $approval->addDocuments($request, ['uploadFile']);
        

        $approval_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $approval,
            'doc_type'            => 'ACQ_APRV',
            'doc_id'              => $approval->id,
        ];
        common()->upload_document_aprvl($approval_requirements);

        audit($approval, __('Maklumat kelulusan perolehan ' .$approval->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
