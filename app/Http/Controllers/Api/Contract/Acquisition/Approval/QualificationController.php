<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Approval;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use Illuminate\Http\Request;

class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (1 == $request->show_cidb_mof) {
            $ssm = $request->ssm1;
        } elseif (2 == $request->show_cidb_mof) {
            $ssm = $request->ssm2;
        }
        Approval::findByHashSlug($id)->update([
            'mof'             => $request->mof ?? 0,
            'cidb'            => $request->cidb ?? 0,
            'ssm'             => $ssm ?? 0,
            'bpku_bumiputera' => $request->bpku_bumiputera ?? 0,
            'spkk'            => $request->spkk ?? 0,
            'bumiputera'      => $request->bumiputera ?? 0,
        ]);
        $approval = Approval::findByHashSlug($id);

        audit($approval, __('Maklumat kelayakan perolehan ' .$approval->title. ' telah berjaya dikemaskini.'));

        if($approval->acquisition_type_id == 1 || $approval->acquisition_type_id == 3){
            return $this->updateMOFQualification($request, $approval);
        }else{
            return $this->updateCIDBQualification($request, $approval);
        }

        return response()->json(['message'=>'Rekod telah berjaya dikemaskini.', 'type'=>'S']);
    }

    private function updateCIDBQualification(Request $request, Approval $approval){

        $arr = [];

        $totalGradeCode      = 0;
        $totalCategoryCode   = 0;
        $totalKhususCode     = 0;
        $totalGradeStatus    = 0;
        $totalCategoryStatus = 0;
        $totalKhususStatus   = 0;

        $totalGradeCode      = (!empty($request->grade_code) && $request->grade_code != null) ? count($request->grade_code) : 0;
        $totalCategoryCode   = (!empty($request->category_code) && $request->category_code != null) ? count($request->category_code) : 0;
        $totalKhususCode     = (!empty($request->khusus_code) && $request->khusus_code != null) ? count($request->khusus_code) : 0;
        $totalGradeStatus    = (!empty($request->grade_status)) ? count($request->grade_status) : 0;
        $totalCategoryStatus = (!empty($request->category_status)) ? count($request->category_status) : 0;
        $totalKhususStatus   = (!empty($request->khusus_status)) ? count($request->khusus_status) : 0;
        
        if($totalGradeCode == 0  || $totalCategoryCode == 0 || $totalKhususCode == 0){
            $errGred = '';
            $errCategory = '';
            $errKhusus = '';

            if($totalGradeCode == 0){
                $errGred = 'Gred';    
            }
            if($totalCategoryCode == 0){
                $errCategory = ' Kategori';
            }
            if($totalKhususCode == 0){
                $errKhusus = ' Pengkhususan';
            }            

            return response()->json(['message'=>'Sila Masukkan Maklumat Kelayakan [' . $errGred . $errCategory . $errKhusus .']','type'=>'E']);

        }else if ((0 != $totalGradeCode && ($totalGradeCode != ($totalGradeStatus + 1))) || (0 != $totalCategoryCode && ($totalCategoryCode != ($totalCategoryStatus + 1))) || (0 != $totalKhususCode && ($totalKhususCode != ($totalKhususStatus + $totalCategoryCode)))) {
            return response()->json(['message'=>'Maklumat Kelayakan Tidak Lengkap.','type'=>'E']);
        } else {
            if ($approval->cidbQualifications()->count() > 0) {
                $approval->cidbQualifications()->delete();
            }
            
            //TODO : checking last tick
            $countGradeCode = 0;
            foreach ($request->grade_code as $key1 => $value1) {
                ++$countGradeCode;
            }
        }

        if (! empty($request->grade_code)) {
            foreach ($request->grade_code as $key1 => $value1) {
                $statusInsert = false;
                if (! empty($request->grade_status)) {
                    foreach ($request->grade_status as $key2 => $value2) {
                        if ($key1 == $key2) {
                            $approval->cidbQualifications()->updateOrCreate([
                                'cidb_code_id' => $key1,
                                'status'       => $value2,
                            ]);
                            $statusInsert = true;
                        }
                    }

                    if (false == $statusInsert) {
                        $approval->cidbQualifications()->updateOrCreate([
                            'cidb_code_id' => $key1,
                            'status'       => -1,
                        ]);
                        $statusInsert = true;
                    }
                } else {
                    $approval->cidbQualifications()->updateOrCreate([
                        'cidb_code_id' => $key1,
                        'status'       => -1,
                    ]);
                }
            }
        }

        if (! empty($request->category_code)) {
            foreach ($request->category_code as $key1 => $value1) {
                $statusInsert = false;
                if (! empty($request->category_status)) {
                    foreach ($request->category_status as $key2 => $value2) {
                        if ($key1 == $key2) {
                            $approval->cidbQualifications()->updateOrCreate([
                                'cidb_code_id' => $key1,
                                'status'       => $value2,
                            ]);
                            $statusInsert = true;
                        }
                    }

                    if (false == $statusInsert) {
                        $approval->cidbQualifications()->updateOrCreate([
                            'cidb_code_id' => $key1,
                            'status'       => -1,
                        ]);
                        $statusInsert = true;
                    }
                } else {
                    $approval->cidbQualifications()->updateOrCreate([
                        'cidb_code_id' => $key1,
                        'status'       => -1,
                    ]);
                }
            }
        }

        if (! empty($request->khusus_code)) {
            foreach ($request->khusus_code as $key1 => $value1) {
                $statusInsert = false;
                if (! empty($request->khusus_status)) {
                    foreach ($request->khusus_status as $key2 => $value2) {
                        if ($key1 == $key2) {
                            $approval->cidbQualifications()->updateOrCreate([
                                'cidb_code_id' => $key1,
                                'status'       => $value2,
                            ]);
                            $statusInsert = true;
                        }
                    }

                    if (false == $statusInsert) {
                        $approval->cidbQualifications()->updateOrCreate([
                            'cidb_code_id' => $key1,
                            'status'       => -1,
                        ]);
                        $statusInsert = true;
                    }
                } else {
                    $approval->cidbQualifications()->updateOrCreate([
                        'cidb_code_id' => $key1,
                        'status'       => -1,
                    ]);
                }
            }
        }

        return response()->json(['message'=>'Rekod telah berjaya dikemaskini.', 'type'=>'S']);
    }

    private function updateMOFQualification(Request $request, Approval $approval){

        $arr = [];

        $totalCategoryCode   = 0;
        $totalKhususCode     = 0;

        $totalCategoryStatus = 0;
        $totalKhususStatus   = 0;

        $totalCategoryCode   = (!empty($request->mof_category_code) && $request->mof_category_code != null) ? count($request->mof_category_code) : 0;
        $totalKhususCode     = (!empty($request->mof_khusus_code) && $request->mof_khusus_code != null) ? count($request->mof_khusus_code) : 0;

        $totalCategoryStatus = (!empty($request->mof_category_status) && $request->mof_category_status != null) ? count($request->mof_category_status) : 0;
        $totalKhususStatus   = (!empty($request->mof_khusus_status) && $request->mof_khusus_status != null) ? count($request->mof_khusus_status) : 0;

        if($totalCategoryCode == 0 || $totalKhususCode == 0){
            return response()->json(['message'=>'Sila Masukkan Maklumat Kelayakan.','type'=>'E']);
        }elseif ((0 != $totalCategoryCode && ($totalCategoryCode != ($totalCategoryStatus + 1))) || 
            (0 != $totalKhususCode && ($totalKhususCode != ($totalKhususStatus + $totalCategoryCode)))) {
                return response()->json(['message'=>'Data MOF Tidak Lengkap.','type'=>'E']);
        } else{
            if ($approval->mofQualifications()->count() > 0) {
                $approval->mofQualifications()->delete();
            }
        }


        if (! empty($request->mof_category_code)) {
            foreach ($request->mof_category_code as $key1 => $value1) {
                $statusInsert = false;
                if (! empty($request->mof_category_status)) {
                    foreach ($request->mof_category_status as $key2 => $value2) {
                        if ($key1 == $key2) {
                            $approval->mofQualifications()->updateOrCreate([
                                'mof_code_id' => $key1,
                                'status'       => $value2,
                            ]);
                            $statusInsert = true;
                        }
                    }

                    if (false == $statusInsert) {
                        $approval->mofQualifications()->updateOrCreate([
                            'mof_code_id' => $key1,
                            'status'       => -1,
                        ]);
                        $statusInsert = true;
                    }
                } else {
                    $approval->mofQualifications()->updateOrCreate([
                        'mof_code_id' => $key1,
                        'status'       => -1,
                    ]);
                }
            }
        }

        if (! empty($request->mof_khusus_code)) {
            foreach ($request->mof_khusus_code as $key1 => $value1) {
                $statusInsert = false;
                if (! empty($request->mof_khusus_status)) {
                    foreach ($request->mof_khusus_status as $key2 => $value2) {
                        if ($key1 == $key2) {
                            $approval->mofQualifications()->updateOrCreate([
                                'mof_code_id' => $key1,
                                'status'       => $value2,
                            ]);
                            $statusInsert = true;
                        }
                    }

                    if (false == $statusInsert) {
                        $approval->mofQualifications()->updateOrCreate([
                            'mof_code_id' => $key1,
                            'status'       => -1,
                        ]);
                        $statusInsert = true;
                    }
                } else {
                    $approval->mofQualifications()->updateOrCreate([
                        'mof_code_id' => $key1,
                        'status'       => -1,
                    ]);
                }
            }
        }

        return response()->json(['message'=>'Rekod telah berjaya dikemaskini.', 'type'=>'S']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
