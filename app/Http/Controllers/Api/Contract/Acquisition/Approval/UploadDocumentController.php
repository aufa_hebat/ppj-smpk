<?php

namespace App\Http\Controllers\Api\Contract\Acquisition\Approval;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use Illuminate\Http\Request;

class UploadDocumentController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'documents' => 'required',
        ]);

        $approval = Approval::findByHashSlug($request->hashslug);
        $approval->addDocuments($request, ['documents']);

        audit($approval, __('Memuatnaik dokumen kelulusan perolehan bagi no rujukan ' . $approval->reference));

        return response()->api([], __('Berjaya memuatnaik dokumen kelulusan perolehan.'));
    }

    public function upload(Request $request, $id)
    {        
        $approval = Approval::findByHashSlug($id);       

        $approval_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $approval,
            'doc_type'            => 'ACQ_APRV',
            'doc_id'              => $approval->id,
        ];
        common()->upload_document_aprvl($approval_requirements);

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }


    public function destroy($id, $doc)
    {
        $approval = Approval::findByHashSlug($id);
        $document = $approval->documents()->where('hashslug', $doc);

        if ($document) {
            $document->each(function ($d) use ($approval) {
                $approval->deleteMedia($d->id);
            });
        }

        return response()->api([], __('Berjaya menghapuskan dokumen kelulusan perolehan.'));
    }
}
