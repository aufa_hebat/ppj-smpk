<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Acquisition::findByHashSlug($id)->update([
            'document_price'       => money()->toMachine(preg_replace('/[,]/', '', $request->document_price) ?? 0),
//            'gst'                  => $request->gst,
//            'gst_price'            => money()->toMachine(preg_replace('/[,]/', '', $request->gst_price) ?? 0),
//            'total_document_price' => money()->toMachine(preg_replace('/[,]/', '', $request->total_document_price) ?? 0),
        ]);

        $acq = Acquisition::findByHashSlug($id);

        audit($acq, __('Harga dokumen perolehan ' .$acq->reference. ' : ' .$acq->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
