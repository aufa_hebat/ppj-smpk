<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\Company\Company;
use Illuminate\Http\Request;

class SubContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        // delete all subcons then create new
        if ($sst->subContract->count() > 0) {
            foreach ($sst->subContract as $sub) {
                $sub->delete();
            }
        }

        $bulkInsert = [];
        if ($request->get('counter') > 0) {
            for ($i = 1; $i < $request->get('counter') + 1; ++$i) {
                if (! empty($request->get('company' . $i)) && ! empty($request->get('amount' . $i))) {
                    array_push($bulkInsert, [
                        'user_id'    => user()->id,
                        'sst_id'     => $sst->id,
                        'company_id' => $request->input('company' . $i),
                        'amount'     => ($request->get('amount' . $i)) ? money()->toMachine($request->get('amount' . $i)) : null,
                        'description'=> $request->input('description' . $i),
                    ]);
                }
            }

            $sst->subContract()->createMany($bulkInsert);
        }
        return response()->api([], $bulkInsert);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
