<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Sale;
use App\Models\EmailScheduler;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;


class CancelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $this->validate($request, [
            'cancelled_at'     => 'required',
            'cancelled_reason' => 'required',
        ], [
            'cancelled_at.required'     => __('Tarikh pembatalan perolehan diperlukan'),
            'cancelled_reason.required' => __('Sebab pembatalan perolehan diperlukan'),
        ]);

        dd($request->hasFile('document'));

        $request->cancelled_at = ! empty($request->cancelled_at) ? \Carbon\Carbon::createFromFormat('d/m/Y', $request->cancelled_at) : null;
        $acquisition = Acquisition::findByHashSlug($id);
        Acquisition::findByHashSlug($id)->update([
            'cancelled_at'     => $request->cancelled_at,
            'cancelled_reason' => $request->cancelled_reason,
            'status_id'        => $request->status_id,
        ]);

        if ($request->hasFile('document')) {
            $cancel_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $acquisition,
                'doc_type'            => 'ACQ_CANCEL',
                'doc_id'              => $acquisition->id,
            ];
            common()->upload_document($cancel_requirements);
        }

        $sales = Sale::where([
            ['acquisition_id', '=', $id],
        ])->get();


        if(!empty($sales)){
            $recipientsTo = $acquisition->user->email;
            $recipientsFrom = user()->email;
            foreach($sales as $sale){
                if(empty($recipientsTo)){
                    $recipientsTo = $sale->company->email;
                }else{
                    $recipientsTo = $recipientsTo . ',' . $sale->company->email;
                }                
            }
            $subject = "Notis Pembatalan Perolehan " . $acquisition->reference;
            $content = "Tuan,<br/><br/> Perolehan " . $acquisition->reference ." terpaksa dibatalkan kerana " . $request->cancelled_reason;
            $content = $content . "<br/><br/> Terima Kasih." . $recipientsTo;
            $status = "NEW";
            $counter = 0;

            EmailScheduler::create([
                'status'    =>  $status,
                'to'        =>  $recipientsTo,
                'subject'   =>  $subject,
                'text'      =>  $content,
                'counter'   =>  $counter,
            ]);
        }

        WorkflowTask::where('acquisition_id',$acquisition->id)->delete();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function updateAcqCancel(Request $request, $id)
    {
        $this->validate($request, [
            'cancelled_at'     => 'required',
            'cancelled_reason' => 'required',
        ], [
            'cancelled_at.required'     => __('Tarikh pembatalan perolehan diperlukan'),
            'cancelled_reason.required' => __('Sebab pembatalan perolehan diperlukan'),
        ]);

        $request->cancelled_at = ! empty($request->cancelled_at) ? \Carbon\Carbon::createFromFormat('d/m/Y', $request->cancelled_at) : null;
        $acquisition = Acquisition::findByHashSlug($id);
        Acquisition::findByHashSlug($id)->update([
            'cancelled_at'     => $request->cancelled_at,
            'cancelled_reason' => $request->cancelled_reason,
            'status_id'        => $request->status_id,
        ]);

        if ($request->hasFile('document')) {
            $cancel_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $acquisition,
                'doc_type'            => 'ACQ_CANCEL',
                'doc_id'              => $acquisition->id,
            ];
            common()->upload_document($cancel_requirements);
        }

        $sales = Sale::where([
            ['acquisition_id', '=', $id],
        ])->get();


        if(!empty($sales)){
            $recipientsTo = $acquisition->user->email;
            $recipientsFrom = user()->email;
            foreach($sales as $sale){
                if(empty($recipientsTo)){
                    $recipientsTo = $sale->company->email;
                }else{
                    $recipientsTo = $recipientsTo . ',' . $sale->company->email;
                }                
            }
            $subject = "Notis Pembatalan Perolehan " . $acquisition->reference;
            $content = "Tuan,<br/><br/> Perolehan " . $acquisition->reference ." terpaksa dibatalkan kerana " . $request->cancelled_reason;
            $content = $content . "<br/><br/> Terima Kasih." . $recipientsTo;
            $status = "NEW";
            $counter = 0;

            EmailScheduler::create([
                'status'    =>  $status,
                'to'        =>  $recipientsTo,
                'subject'   =>  $subject,
                'text'      =>  $content,
                'counter'   =>  $counter,
            ]);
        }

        WorkflowTask::where('acquisition_id',$acquisition->id)->delete();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
