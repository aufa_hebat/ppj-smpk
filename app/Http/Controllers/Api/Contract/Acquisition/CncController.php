<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Cnc;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class CncController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sst = Sst::where('hashslug', $id)->first();

        $cnc = Cnc::updateOrCreate([
            'sst_id' => $sst->id,
        ], [
            'user_id' => user()->id,

            'reference_no'    => $request->input('reference_no'),
            'lad_amount'      => $request->input('lad_amount') ? money()->toMachine($request->input('lad_amount')) : null,
            'signature_date'  => $request->input('signature_date') ? Carbon::createFromFormat('d/m/Y', $request->input('signature_date')) : null,
            'test_award_date' => $request->input('test_award_date') ? Carbon::createFromFormat('d/m/Y', $request->input('test_award_date')) : null,
            'clause_1'        => $request->input('clause_1'),
            'clause_2'        => $request->input('clause_2'),
            'assessment_date' => $request->input('assessment_date') ? Carbon::createFromFormat('d/m/Y', $request->input('assessment_date')) : null,
            'blr_rate'        => $request->input('blr_rate') ? money()->toMachine($request->input('blr_rate')) : null,
            'days_passed'     => $request->input('days_passed'),
            'lad_per_day'     => $request->input('lad_per_day') ? money()->toMachine($request->input('lad_per_day')) : null,
        ]);

        $cnc_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $cnc->sst->acquisition,
            'doc_type'            => 'CNC',
            'doc_id'              => $cnc->id,
        ];
        common()->upload_document($cnc_requirements);

        audit($cnc, __('Sijil Tidak Siap Kerja bagi perolehan ' .$cnc->sst->acquisition->reference. ' : ' .$cnc->sst->acquisition->title. ' telah berjaya disimpan.'));

        $workflow = WorkflowTask::where('acquisition_id', $cnc->sst->acquisition_id)->where('workflow_team_id',1)->first();
        $workflow->flow_desc = 'Sijil Tidak Siap Kerja : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->flow_location = 'CNC';
        $workflow->url = '/post/cnc/'.$sst->hashslug.'/edit';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;

        $workflow2 = WorkflowTask::where('acquisition_id', $cnc->sst->acquisition_id)->where('workflow_team_id',2)->first();
        $workflow2->flow_location = 'CNC';
        $workflow2->role_id = 4;

        $workflow->update();
        $workflow2->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
