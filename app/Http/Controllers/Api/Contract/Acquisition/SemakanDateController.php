<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SemakanDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(! empty($request->closed_at)){
            Acquisition::findByHashSlug($id)->update([
                'closed_at'         =>  Carbon::createFromFormat('d/m/Y', $request->closed_at),
            ]);
        }
        
        if(! empty($request->closed_sale_at)){
            Acquisition::findByHashSlug($id)->update([
                'closed_sale_at'    => Carbon::createFromFormat('d/m/Y', $request->closed_sale_at),
            ]);
        }
        

        $acq = Acquisition::findByHashSlug($id);

        audit($acq, __('Tarikh dan masa dokumen perolehan ' .$acq->reference. ' : ' .$acq->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
