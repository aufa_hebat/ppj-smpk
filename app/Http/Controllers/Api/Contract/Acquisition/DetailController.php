<?php

namespace App\Http\Controllers\Api\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'acquisition_category_id' => 'required',
            'title' => 'required'
        ], [
            'acquisition_category_id.required' => __('Sila Pilih Kategori Perolehan'),
            'title.required' =>  __('Sila Masukkan Tajuk Perolehan'),
        ]);
        $checks = Acquisition::findByHashSlug($id);
        if ($checks->acquisition_category_id != $request->acquisition_category_id) {
            // $cnt = (counter()->counter_acquisition ?? 0) + 1;
            // counter()->update(['counter_acquisition' => $cnt]);

            $cntr = explode('/', $checks->reference);
            $cnt  = $cntr['3'];

            $getRef = user()->getAcquisitionReferenceCode($cnt, $checks->approval->year, $request->acquisition_category_id);
            Acquisition::findByHashSlug($id)->update([
                'title'                   => str_replace("'", '`', strtoupper($request->title)),
                'acquisition_category_id' => $request->acquisition_category_id,
                'reference'               => $getRef,
            ]);
        } else {
            Acquisition::findByHashSlug($id)->update([
                'title'     => str_replace("'", '`', strtoupper($request->title)),
                'reference' => $request->reference,
            ]);
        }

        audit($checks, __('Perincian dokumen perolehan ' .$checks->reference. ' : ' .$checks->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
