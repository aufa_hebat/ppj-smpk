<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\EotApprove;
use App\Models\Acquisition\EotDetail;
use App\Models\Acquisition\Review;
use Carbon\Carbon;
use App\Models\WorkflowTask;
use Illuminate\Http\Request;

class ExtensionApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'bil_meeting'                => 'required|string|max:15',
          'eot_approve_meeting_date'   => 'required',
          'eot_approve_period'         => 'required|integer|max:10000',
          'eot_approve_period_type_id' => 'required|integer|max:5',
          'eot_approve_approval'       => 'required',
        ],$messages = [
          'bil_meeting.required' => 'Bilangan Mesyuarat harus diisi.',
          'eot_approve_meeting_date.required' => 'Tarikh Mesyuarat J/K Sebutharga harus diisi.',
          'eot_approve_period.required' => 'Kelulusan Tempoh Lanjutan Kontrak harus dipilih.',
          'eot_approve_period.integer' => 'Kelulusan Tempoh Lanjutan Kontrak harus dalam bilangan (nombor).',
          'eot_approve_period.max' => 'Kelulusan Tempoh Lanjutan Kontrak tidak boleh melebihi 10,000.',
          'eot_approve_period_type_id.required' => 'Jenis Tempoh harus dipilih.',
          'eot_approve_approval.required' => 'Keputusan harus dipilih.',
        ]);

        if (! empty($request->eot_approve_meeting_date)) {
          $meeting_date = Carbon::createFromFormat('d/m/Y', $request->eot_approve_meeting_date);
        } else {
          $meeting_date = Carbon::now();
        }

        $eot = Eot::findByHashSlug($request->eot_hashslug);

        if ('first' == $request->eot_approve_id) {

          $tempID                  = null;
          $tempIDP                 = null;
          $ida                     = [];
          $idaS                    = [];
          $idp                     = [];
          $idpS                    = [];
          $hashslug                = $request->hashslug;

          if(user()->current_role_login == 'administrator') {
            if (! empty($request->exist)) { 
              foreach ($request->exist as $ex) {
                $checkDetails = EotDetail::where('eot_id', $request->eot_id)->get();
                if(!empty($checkDetails)){
                  foreach ($checkDetails as $p) {
                    $idp[] = $p['id'];
                  }
                }
                if (! empty($ex['reason'])) {
                  $details                    = EotDetail::find($ex['id']);
                  $details->eot_id            = $request->eot_id;
                  $details->user_id           = user()->id;
                  $details->reasons           = $ex['reason'];
                  $details->clause            = $ex['clauses'];
                  $details->periods           = $ex['period'];
                  $details->period_type_id    = $ex['period_type'];
                  $details->save();
                  $tempIDP = $details->id;
                }
                      
                $idpS[] = $ex['id'];
              }
              $diffP = array_diff($idp, $idpS);
              foreach ($diffP as $pr) {
                EotDetail::destroy($pr);
              }
            } 

            if (! empty($request->baru)) {

              foreach ($request->baru as $baru) {
                if (! empty($baru['reason'])) {
                  $details                  = new EotDetail();
                  $details->eot_id          = $request->eot_id;
                  $details->user_id         = user()->id;
                  $details->reasons         = $baru['reason'];
                  $details->clause          = $baru['clauses'];
                  $details->periods         = $baru['period'];
                  $details->period_type_id  = $baru['period_type'];
                  $details->save();
                  $tempIDP = $details->id;
                }
              }
            }
          }

          $approval = EotApprove::create([
            'user_id'           => user()->id,
            'bil_meeting'       => $request->bil_meeting,
            'eot_id'            => $request->eot_id,
            'period'            => $request->eot_approve_period,
            'period_type_id'    => $request->eot_approve_period_type_id,
            'meeting_date'      => $meeting_date,
            'approval'          => $request->eot_approve_approval,
            'approved_end_date' => Carbon::createFromFormat('d/m/Y', $request->approved_end_date),
          ]);

          $eot_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $eot->appointed_company->acquisition,
            'doc_type'            => 'EOT',
            'doc_id'              => $approval->id,
          ];
          common()->upload_document($eot_requirements);

          if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){

            $workflow = WorkflowTask::where('acquisition_id', $eot->appointed_company->acquisition_id)->where('eot_id',$request->eot_id)->first();
            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';
            $workflow->flow_location = 'EOT';
            $workflow->role_id = 6;
            $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#eot_review';
            $workflow->update();

            audit($eot, __('Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' bagi perolehan ' .$eot->sst->acquisition->reference. ' : ' .$eot->sst->acquisition->title. ' telah berjaya dikemaskini.'));
          } else {

            $workflow = WorkflowTask::where('acquisition_id', $eot->appointed_company->acquisition_id)->where('eot_id',$request->eot_id)->first();
            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila beri ulasan untuk proses penyemakan.';
            $workflow->flow_location = 'EOT';
            $workflow->role_id = 6;
            $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#eot_review';
            $workflow->update();

            audit($eot, __('Perakuan Kelambatan & Lanjutan Kontrak bagi perolehan ' .$eot->sst->acquisition->reference. ' : ' .$eot->sst->acquisition->title. ' telah berjaya dikemaskini.'));
          }
        } else {

          $tempID                  = null;
          $tempIDP                 = null;
          $ida                     = [];
          $idaS                    = [];
          $idp                     = [];
          $idpS                    = [];
          $hashslug                = $request->hashslug;

          if(user()->current_role_login == 'administrator') {
            if (! empty($request->exist)) { 
              foreach ($request->exist as $ex) {
                $checkDetails = EotDetail::where('eot_id', $request->eot_id)->get();
                if(!empty($checkDetails)){
                  foreach ($checkDetails as $p) {
                    $idp[] = $p['id'];
                  }
                }
                if (! empty($ex['reason'])) {
                  $details                    = EotDetail::find($ex['id']);
                  $details->eot_id            = $request->eot_id;
                  $details->user_id           = user()->id;
                  $details->reasons           = $ex['reason'];
                  $details->clause            = $ex['clauses'];
                  $details->periods           = $ex['period'];
                  $details->period_type_id    = $ex['period_type'];
                  $details->save();
                  $tempIDP = $details->id;
                }
                      
                $idpS[] = $ex['id'];
              }
              $diffP = array_diff($idp, $idpS);
              foreach ($diffP as $pr) {
                EotDetail::destroy($pr);
              }
            } 

            if (! empty($request->baru)) {

              foreach ($request->baru as $baru) {
                if (! empty($baru['reason'])) {
                  $details                  = new EotDetail();
                  $details->eot_id          = $request->eot_id;
                  $details->user_id         = user()->id;
                  $details->reasons         = $baru['reason'];
                  $details->clause          = $baru['clauses'];
                  $details->periods         = $baru['period'];
                  $details->period_type_id  = $baru['period_type'];
                  $details->save();
                  $tempIDP = $details->id;
                }
              }
            }
          }

          $approval = EotApprove::where('id', $request->eot_approve_id)->update([
            'user_id'           => user()->id,
            'eot_id'            => $request->eot_id,
            'period'            => $request->eot_approve_period,
            'period_type_id'    => $request->eot_approve_period_type_id,
            'meeting_date'      => $meeting_date,
            'approval'          => $request->eot_approve_approval,
            'approved_end_date' => Carbon::createFromFormat('d/m/Y', $request->approved_end_date),
          ]);

          $eot_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $eot->appointed_company->acquisition,
            'doc_type'            => 'EOT',
            'doc_id'              => $request->eot_approve_id,
          ];
          common()->upload_document($eot_requirements);

          if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){

            $workflow = WorkflowTask::where('acquisition_id', $eot->appointed_company->acquisition_id)->where('eot_id',$request->eot_id)->first();
            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila beri ulasan untuk proses penyemakan.';
            $workflow->flow_location = 'EOT';
            $workflow->role_id = 6;
            $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#eot_review';
            $workflow->update();

            audit($eot, __('Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' bagi perolehan ' .$eot->sst->acquisition->reference. ' : ' .$eot->sst->acquisition->title. ' telah berjaya dikemaskini.'));
          } else {

            $workflow = WorkflowTask::where('acquisition_id', $eot->appointed_company->acquisition_id)->where('eot_id',$request->eot_id)->first();
            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila beri ulasan untuk proses penyemakan.';
            $workflow->flow_location = 'EOT';
            $workflow->role_id = 6;
            $workflow->url = '/extension/extension_approve/'.$eot->hashslug.'/edit#eot_review';
            $workflow->update();

            audit($eot, __('Perakuan Kelambatan & Lanjutan Kontrak bagi perolehan ' .$eot->sst->acquisition->reference. ' : ' .$eot->sst->acquisition->title. ' telah berjaya dikemaskini.'));
          }
        }

        return response()->api([], __('Rekod berjaya dikemaskini'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
