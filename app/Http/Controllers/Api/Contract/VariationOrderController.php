<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\RefVoc;
use App\Models\VO\Approver;
use App\Models\VO\VO;
use App\Models\VO\Type;
use App\Models\BillOfQuantity\Element;
use App\Models\BillOfQuantity\Item;
use App\Models\BillOfQuantity\SubItem;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class VariationOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->type)){
            return response()->json(['type' => 'E', 'message' => __('Sila Pilih Jenis Permohonan Perubahan Kerja')]);
        }

        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($request->id);
        $vo_count = VO::where('sst_id', $sst->id)->where('type', null)->where('deleted_at', null)->count();
        $vo       = VO::create([
            'sst_id'        => $sst->id,
            'user_id'       => user()->id,
            'department_id' => user()->department->id,
            'no'            => $vo_count + 1,
        ]);

        if (! empty($request->type)) {
            foreach ($request->type as $key => $value) {
                $vo->types()->updateOrCreate([
                    'vo_id'                     => $vo->id,
                    'variation_order_type_id'   => $value,
                ]);

            }
        }

        // if($vo->no == 1){
        //     $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->first();
        //     $workflow->ppk_id = $vo->id;
        //     $workflow->flow_desc = 'Perubahan Kerja PPK '.$vo->no.' : Sila tekan butang hantar untuk proses penyemakan.';
        //     $workflow->user_id = user()->id;
        //     $workflow->flow_location = 'PPK';
        //     $workflow->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
        //     $workflow->update();

        //     $vo->sst->acquisition->status_task = config('enums.flowALT2');
        //     $vo->sst->acquisition->update();
        // } else {
            $workflow = new WorkflowTask();
            
            $workflow->acquisition_id = $vo->sst->acquisition_id;
            $workflow->ppk_id = $vo->id;
            $workflow->flow_name = $vo->sst->acquisition->reference . ' : ' . $vo->sst->acquisition->title;
            $workflow->flow_desc = 'Perubahan Kerja PPK '.$vo->no.' : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->flow_location = 'PPK';
            $workflow->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
            $workflow->user_id = user()->id;
            $workflow->role_id = 4;
            $workflow->is_claimed = 1;
            $workflow->workflow_team_id = 1;

            $workflow2 = new WorkflowTask();
            $workflow2->acquisition_id = $vo->sst->acquisition_id;
            $workflow2->ppk_id = $vo->id;
            $workflow2->flow_name = $vo->sst->acquisition->reference . ' : ' . $vo->sst->acquisition->title;
            $workflow2->flow_desc = 'Perubahan Kerja PPK '.$vo->no.' : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow2->flow_location = 'PPK';
            $workflow2->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
            $workflow2->role_id = 4;
            $workflow2->is_claimed = 1;
            $workflow2->workflow_team_id = 2;

            $workflow->save();
            $workflow2->save();

            $vo->sst->acquisition->status_task = config('enums.flowALT2');
            $vo->sst->acquisition->update();
        // }

        return response()->json(['type' => 'S', 'hashslug' => $vo->hashslug, 'message' => __('Rekod Telah Berjaya Disimpan.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vo = VO::withDetails()->findByHashSlug($id);

        if ($vo->types()->count() > 0) {

            $types = $vo->types;
            
            foreach($types as $type){                
                $isFound = false;
                if (! empty($request->type)) {
                    foreach ($request->type as $key => $value) {
                        if($type->variation_order_type_id == $value){
                            $isFound = true;
                        }
                    }
                    if(!$isFound){                           

                        \App\Models\VO\SubItem::where('vo_type_id', '=', $type->id)->delete();
                        \App\Models\VO\Item::where('vo_type_id', '=', $type->id)->delete();
                        \App\Models\VO\Element::where('vo_type_id', '=', $type->id)->delete();

                        Type::where('id', '=', $type->id)->delete();
                    }
                }
            }
        }

        if (! empty($request->type)) {
            foreach ($request->type as $key => $value) {
                $vo->types()->updateOrCreate([
                    'vo_id'                     => $vo->id,
                    'variation_order_type_id'   => $value,
                ]);
            }
        }

        $vo_requirements = [
            'request_hDocumentId' => $request->hDocumentId,
            'request_hasFile'     => $request->hasFile('uploadFile'),
            'request_file'        => $request->file('uploadFile'),
            'acquisition'         => $vo->sst->acquisition,
            'doc_type'            => 'VO/' . $vo->no . '/VO_PPK',
            'doc_id'              => $vo->id,
        ];
        common()->upload_document($vo_requirements);

        $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id',$vo->id)->where('flow_location','PPK')->first();
        $workflow->ppk_id = $vo->id;
        $workflow->flow_desc = 'Perubahan Kerja PPK '.$vo->no.' : Sila tekan butang hantar untuk proses penyemakan.';
        $workflow->user_id = user()->id;
        $workflow->flow_location = 'PPK';
        $workflow->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
        $workflow->update();

        return response()->json(['hashslug' => $vo->hashslug, 'message' => __('Rekod telah berjaya dikemaskini.')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vo = VO::findByHashSlug($id);

        \App\Models\VO\SubItem::where('vo_id', '=', $vo->id)->delete();
        \App\Models\VO\Item::where('vo_id', '=', $vo->id)->delete();
        \App\Models\VO\Element::where('vo_id', '=', $vo->id)->delete();
        \App\Models\VO\SubItem::where('vo_id', '=', $vo->id)->delete();
        \App\Models\VO\Active::where('vo_id', '=', $vo->id)->delete();
        \App\Models\VO\Type::where('vo_id', '=', $vo->id)->delete();

        VO::where('id', $vo->id)->delete();

        $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id',$vo->id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }

    public function sync($id)
    {
        $type = Type::find($id);

        $voElements = \App\Models\VO\Element::where([
            ['vo_type_id', '=', $type->id]
        ]);

        \App\Models\VO\SubItem::where('vo_type_id', '=', $type->id)->delete();
        \App\Models\VO\Item::where('vo_type_id', '=', $type->id)->delete();
        \App\Models\VO\Element::where('vo_type_id', '=', $type->id)->delete();

        $elements = Element::where('acquisition_id', '=', $type->vo->sst->acquisition->id)->get();
        $items = Item::where('acquisition_id', '=', $type->vo->sst->acquisition->id)->get();
        $subItems = SubItem::where('acquisition_id', '=', $type->vo->sst->acquisition->id)->get();

        foreach($elements as $element){
            $voElement = \App\Models\VO\Element::create([
                'sst_id'    => $type->vo->sst->id,
                'bq_element_id' =>  $element->id,
                'vo_id'         =>  $type->vo->id,
                'vo_type_id'    =>  $type->id,
                'no'            =>  $element->no,
                'element'       =>  $element->element,
                'description'   =>  $element->description,
                'amount_omission' =>  $element->amount,
            ]);

            foreach($items as $item){
                if($item->bq_no_element == $element->no){
                    $voItem = \App\Models\VO\Item::create([
                        'sst_id'            => $type->vo->sst->id,
                        'bq_element_id'     =>  $element->id,
                        'bq_item_id'        =>  $item->id,
                        'vo_id'             =>  $type->vo->id,
                        'vo_type_id'        =>  $type->id,
                        'bq_no_element'     =>  $item->bq_no_element,
                        'no'                =>  $item->no,
                        'item'              =>  $item->item,
                        'description'       =>  $item->description,
                        'amount_omission'   =>  $item->amount,
                        'vo_element_id'     =>  $voElement->id,
                    ]);

                    foreach($subItems as $subItem){
                        if($subItem->bq_no_element == $element->no && $subItem->bq_no_item == $item->no){
                            \App\Models\VO\SubItem::create([
                                'sst_id'            => $type->vo->sst->id,
                                'vo_id'             =>  $type->vo->id,
                                'vo_type_id'        =>  $type->id,
                                'bq_element_id'     =>  $element->id,
                                'bq_item_id'        =>  $item->id,
                                'bq_subitem_id'     =>  $subItem->id,
                                'bq_no_element'     =>  $subItem->bq_no_element,
                                'bq_no_item'        =>  $subItem->bq_no_item,
                                'no'                =>  $subItem->no,
                                'item'              =>  $subItem->item,
                                'description'       =>  $subItem->description,
                                'quantity_omission' =>  $subItem->quantity,
                                'amount_omission'   =>  $subItem->amount,
                                'unit'              =>  $subItem->unit,
                                'rate_per_unit'     =>  $subItem->rate_per_unit,
                                'vo_item_id'        =>  $voItem->id,
                            ]);                            
                        }
                    }
                }

            }
        }
        return response()->json(['hashslug' => $type->vo->hashslug, 'message' => __('Rekod telah berjaya ditambah.')]);
    }

    public function ppkMeeting(Request $request, $id)
    {
        if(! empty($request)){
            $vo = VO::updateOrCreate([
                'id'    =>  $id
            ],[
                'mtg_no'        =>  $request->mtg_no,
                'mtg_year'      =>  $request->mtg_year,
                'ppk_kod_1'     =>  $request->ppk_kod_1,
                'ppk_kod_2'     =>  $request->ppk_kod_2,
                'ppk_kod_3'     =>  $request->ppk_kod_3
            ]);

            $approver = Approver::where('vo_id', $request->vo_id)->where('deleted_at', null)->get();
            if($approver->count() == 0){
                $vocDefault = RefVoc::where('deleted_at', null)->get();
                if(!empty($vocDefault)){
                    foreach($vocDefault as $def){
                        $approver = Approver::create([
                            'vo_id'         =>  $vo->id,
                            'user_id'       =>  $def->user_id,
                            'position_id'   =>  $def->position_id,
                            'department_id' =>  $def->department_id,
                            'role'          =>  $def->role,
                        ]);
                    }
                }
            }
        }

        if(!empty($request)){
            audit($vo, __('Mesyuarat Permohonan Perubahan Kerja Bil.' .$request->mtg_no. ' ' .$request->mtg_year. ' bagi ' .$vo->sst->acquisition->reference. ' : ' .$vo->sst->acquisition->title. ' telah berjaya dikemaskini.'));
        } else {
            audit($vo, __('Mesyuarat Permohonan Perubahan Kerja Bil.' .$request->mtg_no. ' ' .$request->mtg_year. ' bagi ' .$vo->sst->acquisition->reference. ' : ' .$vo->sst->acquisition->title. ' telah berjaya disimpan.'));
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    public function ppkApprover(Request $request, $id)
    {
        if (! empty($request->baru)) {
            
            if (! empty($request->exist)) {
                foreach ($request->exist as $ex) {
                    $checkJKPembuka = Approver::where('vo_id', $request->vo_id)->get();
                    if (! empty($checkJKPembuka)) {
                        foreach ($checkJKPembuka as $a) {
                            $ida[] = $a['id'];
                        }
                        $idaS[] = $ex['id'];
                    }
                }
                $diffA = array_diff($ida, $idaS);

                foreach ($diffA as $ad) {
                    Approver::destroy($ad);
                }
            } else {
                $checkJKPembuka = Approver::where('vo_id', $request->vo_id)->get();
                Approver::whereIn('vo_id', $checkJKPembuka->pluck('vo_id'))->delete();
            }

            foreach ($request->baru as $userBaru) {
                
                if (! empty($userBaru['name'])) {
                    
                    $user = User::find($userBaru['name']);
                    if($user){
                        $approver = Approver::create([
                            'vo_id'         =>  $request->vo_id,
                            'user_id'          =>  $user->id,
                            'position_id'   =>  $user->position->id,
                            'department_id' =>  $user->department->id,
                            'role'          =>  $userBaru['role'],
                        ]);
                        $vo = VO::find($request->vo_id);
                        audit($vo, __('Jawatankuasa Mesyuarat bagi ' .$vo->sst->acquisition->reference. ' : ' .$vo->sst->acquisition->title. ' telah berjaya dikemaskini.'));
                    }
                }
            }
        }else{
            $checkJKPembuka = Approver::where('vo_id', $request->vo_id)->get();
            Approver::whereIn('vo_id', $checkJKPembuka->pluck('vo_id'))->delete();
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    public function uploadPpk(Request $request, $id)
    {
        $vo = VO::find($id);

        if(!empty($request->action)){
            VO::find($id)->update([
                'status'    =>  '1',
            ]);
        }
        

        if ($request->hasFile('document')) {
            $sst_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $vo->sst->acquisition,
                'doc_type'            => 'VO/' . $vo->no . '/PPK_RPT',
                'doc_id'              => $vo->id,
            ];
            common()->upload_document($sst_requirements);
        }

        // $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id',$vo->id)->first();
        // $workflow->ppk_id = $vo->id;
        // $workflow->flow_desc = 'Perubahan Kerja PPK '.$vo->no.' : Sila tekan butang hantar untuk proses penyemakan.';
        // $workflow->user_id = null;
        // $workflow->flow_location = 'PPK';
        // $workflow->url = '/post/variation-order/'.$vo->hashslug.'/ppk_edit#ppk-doc';
        // $workflow->update();
        $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id',$vo->id)->where('flow_location','PPK')->first();
        $workflow->ppk_id = $vo->id;
        $workflow->flow_desc = 'Perubahan Kerja PPK '.$vo->no.' : Sila sahkan keputusan PPK.';
        $workflow->user_id = null;
        $workflow->is_claimed = 0;
        $workflow->flow_location = 'PPK';
        $workflow->role_id = 12;
        $workflow->url = '/post/variation-order-result/'.$vo->hashslug.'/ppk_amend';
        $workflow->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    public function uploadPpkResult(Request $request, $id)
    {
        $vo = VO::find($id);

        if ($request->hasFile('document')) {
            $sst_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $vo->sst->acquisition,
                'doc_type'            => 'VO/' . $vo->no . '/PPK_RSLT_RPT',
                'doc_id'              => $vo->id,
            ];
            common()->upload_document($sst_requirements);
        }

        if(!empty($request->action)){
            $voNew = VO::find($id);
            if($voNew->doc_ppk_result()->count() > 0){
                $vo->update([
                    'status'    =>  '2',
                ]);
            } 
        }

        $workflow = WorkflowTask::where('acquisition_id', $vo->sst->acquisition_id)->where('ppk_id',$vo->id)->where('flow_location','PPK')->first();
        $workflow->ppk_id = $vo->id;
        $workflow->flow_desc = '';
        $workflow->user_id = null;
        $workflow->is_claimed = 1;
        $workflow->flow_location = 'PPK';
        $workflow->url = '/post/variation-order-result/'.$vo->hashslug.'/ppk_amend';
        $workflow->update();

        audit($vo, __('Dokumen Keputusan PPK bagi ' .$vo->sst->acquisition->reference. ' : ' .$vo->sst->acquisition->title. ' telah berjaya dimuat naik.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
