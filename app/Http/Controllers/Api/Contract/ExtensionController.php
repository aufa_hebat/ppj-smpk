<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Models\Singleton\SingletonEOT;
use App\Models\Acquisition\Eot;
use App\Models\Acquisition\EotDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\Document;

class ExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $appointed_company = AppointedCompany::where('id', $request->appointed_company_id)->first();

        if($appointed_company->acquisition->approval->acquisition_type_id == '2'){
            $this->validate($request, [
                'period'         => 'required|integer|max:10000',
                'period_type_id' => 'required|integer|max:5',
                'section' => 'required',
            ],$messages = [
                'period.required' => 'Permohonan Tempoh Perakuan Kelambatan dan Pelanjutan Masa harus diisi.',
                'period.integer' => 'Permohonan Tempoh Perakuan Kelambatan dan Pelanjutan Masa harus dalam bilangan (nombor).',
                'period.max' => 'Permohonan Tempoh Perakuan Kelambatan dan Pelanjutan Masa tidak boleh melebihi 10,000.',
                'period_type_id.required' => 'Jenis Tempoh harus dipilih.',
                'section.required' => 'Bahagian harus dipilih.',
            ]);
        } else {
            $this->validate($request, [
                'period'         => 'required|integer|max:10000',
                'period_type_id' => 'required|integer|max:5',
            ],$messages = [
                'period.required' => 'Permohonan Tempoh Lanjutan Kontrak harus diisi.',
                'period.integer' => 'Permohonan Tempoh Lanjutan Kontrak harus dalam bilangan (nombor).',
                'period.max' => 'Permohonan Tempoh Lanjutan Kontrak tidak boleh melebihi 10,000.',
                'period_type_id.required' => 'Jenis Tempoh harus dipilih.',
            ]);
        }

        //DIN test singleton
        $eot = SingletonEOT::save($request);

        $support_document = [
            'request_hDocumentId' => null,
            'request_hasFile'     => $request->hasFile('uploadFileSokongan'),
            'request_file'        => $request->file('uploadFileSokongan'),
            'acquisition'         => $eot->acquisition,
            'doc_type'            => 'SOKONGAN',
            'doc_id'              => $eot->id,
        ];
        common()->upload_document($support_document);

        $executif_report = [
            'request_hDocumentId' => null,
            'request_hasFile'     => $request->hasFile('uploadFileLaporan'),
            'request_file'        => $request->file('uploadFileLaporan'),
            'acquisition'         => $eot->acquisition,
            'doc_type'            => 'EKSEKUTIF',
            'doc_id'              => $eot->id,
        ];
        common()->upload_document($executif_report);

        $president_acquisition = [
            'request_hDocumentId' => null,
            'request_hasFile'     => $request->hasFile('uploadFileKertas'),
            'request_file'        => $request->file('uploadFileKertas'),
            'acquisition'         => $eot->acquisition,
            'doc_type'            => 'KERTAS_TIMBANGAN',
            'doc_id'              => $eot->id,
        ];
        common()->upload_document($president_acquisition);

        if($appointed_company->acquisition->approval->acquisition_type_id == 2){
            audit($eot, __('Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' bagi perolehan ' .$eot->sst->acquisition->reference. ' : ' .$eot->sst->acquisition->title. ' telah berjaya disimpan.'));
        } else {
            audit($eot, __('Perakuan Kelambatan & Lanjutan Kontrak bagi perolehan ' .$eot->sst->acquisition->reference. ' : ' .$eot->sst->acquisition->title. ' telah berjaya disimpan.'));
        }

        // if($eot->bil == 1){
        //     $workflow = WorkflowTask::where('acquisition_id', $appointed_company->acquisition_id)->first();
        //     $workflow->eot_id = $eot->id;
        //     $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila tekan butang hantar untuk proses penyemakan.';
        //     $workflow->user_id = user()->id;
        //     $workflow->flow_location = 'EOT';
        //     $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit';
        //     $workflow->update();

        //     $appointed_company->acquisition->status_task = config('enums.flowALT1');
        //     $appointed_company->acquisition->update();
        // } else {
        $workflow = new WorkflowTask();
        $workflow->acquisition_id = $appointed_company->acquisition_id;
        $workflow->eot_id = $eot->id;
        $workflow->flow_name = $appointed_company->acquisition->reference . ' : ' . $appointed_company->acquisition->title;
        if($appointed_company->acquisition->approval->acquisition_type_id == 2){
            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila tekan butang hantar untuk proses penyemakan.';
        } else {
            $workflow->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila tekan butang hantar untuk proses penyemakan.';
        }
        
        $workflow->flow_location = 'EOT';
        $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit';
        $workflow->user_id = user()->id;
        $workflow->role_id = 4;
        $workflow->is_claimed = 1;
        $workflow->workflow_team_id = 1;

        $workflow2 = new WorkflowTask();
        $workflow2->acquisition_id = $appointed_company->acquisition_id;
        $workflow2->eot_id = $eot->id;
        $workflow2->flow_name = $appointed_company->acquisition->reference . ' : ' . $appointed_company->acquisition->title;
        if($appointed_company->acquisition->approval->acquisition_type_id == 2){
            $workflow2->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila tekan butang hantar untuk proses penyemakan.';
        } else {
            $workflow2->flow_desc = 'Perakuan Kelambatan & Lanjutan Kontrak : Sila tekan butang hantar untuk proses penyemakan.';
        }
        $workflow2->flow_location = 'EOT';
        $workflow2->url = '/extension/extension_edit/'.$eot->hashslug.'/edit';
        $workflow2->role_id = 4;
        $workflow2->is_claimed = 1;
        $workflow2->workflow_team_id = 2;

        $workflow->save();
        $workflow2->save();

        $appointed_company->acquisition->status_task = config('enums.flowALT1');
        $appointed_company->acquisition->update();
        // }

        // return redirect()->back();
        // return response()->api([], __('Rekod telah berjaya dikemaskini.'));

        return response()->json(['hashslug' => $eot->hashslug ?? null, 'message' => __('Rekod telah berjaya dikemaskini.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $appointed_company = Eot::find($id);

        if($appointed_company->appointed_company->acquisition->approval->acquisition_type_id == '2'){
            $this->validate($request, [
                'period'         => 'required|integer|max:10000',
                'period_type_id' => 'required|integer|max:5',
                'section' => 'required',
            ],$messages = [
                'period.required' => 'Permohonan Tempoh Perakuan Kelambatan dan Pelanjutan Masa harus diisi.',
                'period.integer' => 'Permohonan Tempoh Perakuan Kelambatan dan Pelanjutan Masa harus dalam bilangan (nombor).',
                'period.max' => 'Permohonan Tempoh Perakuan Kelambatan dan Pelanjutan Masa tidak boleh melebihi 10,000.',
                'period_type_id.required' => 'Jenis Tempoh harus dipilih.',
                'section.required' => 'Bahagian harus dipilih.',
            ]);
        } else {
            $this->validate($request, [
                'period'         => 'required|integer|max:10000',
                'period_type_id' => 'required|integer|max:5',
            ],$messages = [
                'period.required' => 'Permohonan Tempoh Lanjutan Kontrak harus diisi.',
                'period.integer' => 'Permohonan Tempoh Lanjutan Kontrak harus dalam bilangan (nombor).',
                'period.max' => 'Permohonan Tempoh Lanjutan Kontrak tidak boleh melebihi 10,000.',
                'period_type_id.required' => 'Jenis Tempoh harus dipilih.',
            ]);
        }

        $eot = Eot::where('id', $id)
            ->update([
                'period'            => $request->period,
                'period_type_id'    => $request->period_type_id,
                'extended_end_date' => Carbon::createFromFormat('d/m/Y', $request->extended_end_date),
                'section'           => $request->section,
        ]);

        $tempID                  = null;
        $tempIDP                 = null;
        $ida                     = [];
        $idaS                    = [];
        $idp                     = [];
        $idpS                    = [];
        $hashslug                = $request->hashslug;

        if (! empty($request->exist)) { 
            foreach ($request->exist as $ex) {
                $checkDetails = EotDetail::where('eot_id', $id)->get();
                if(!empty($checkDetails)){
                    foreach ($checkDetails as $p) {
                        $idp[] = $p['id'];
                    }
                }
                if (! empty($ex['reason'])) {
                    $details                    = EotDetail::find($ex['id']);
                    $details->eot_id            = $id;
                    $details->user_id           = user()->id;
                    $details->reasons           = $ex['reason'];
                    $details->clause            = $ex['clauses'];
                    $details->periods           = $ex['period'];
                    $details->period_type_id    = $ex['period_type'];
                    $details->save();
                    $tempIDP = $details->id;
                }
                
                $idpS[] = $ex['id'];
            }
            $diffP = array_diff($idp, $idpS);
            foreach ($diffP as $pr) {
                EotDetail::destroy($pr);
            }
        } 

        if (! empty($request->baru)) {

            foreach ($request->baru as $baru) {
                if (! empty($baru['reason'])) {
                    $details              = new EotDetail();
                    $details->eot_id = $id;
                    $details->user_id     = user()->id;
                    $details->reasons = $baru['reason'];
                    $details->clause  = $baru['clauses'];
                    $details->periods = $baru['period'];
                    $details->period_type_id  = $baru['period_type'];
                    $details->save();
                    $tempIDP = $details->id;
                }
            }
        }

        $eots = EOT::find($id);
        if(!empty($eots->acquisition)){
            $support_document = [
                'request_hDocumentId' => $request->hDocumentIdSokongan,
                'request_hasFile'     => $request->hasFile('uploadFileSokongan'),
                'request_file'        => $request->file('uploadFileSokongan'),
                'acquisition'         => $eots->acquisition,
                'doc_type'            => 'SOKONGAN',
                'doc_id'              => $eots->id,
            ];
            common()->upload_document($support_document);

            $executif_report = [
                'request_hDocumentId' => $request->hDocumentIdLaporan,
                'request_hasFile'     => $request->hasFile('uploadFileLaporan'),
                'request_file'        => $request->file('uploadFileLaporan'),
                'acquisition'         => $eots->acquisition,
                'doc_type'            => 'EKSEKUTIF',
                'doc_id'              => $eots->id,
            ];
            common()->upload_document($executif_report);

            $president_acquisition = [
                'request_hDocumentId' => $request->hDocumentIdKertas,
                'request_hasFile'     => $request->hasFile('uploadFileKertas'),
                'request_file'        => $request->file('uploadFileKertas'),
                'acquisition'         => $eots->acquisition,
                'doc_type'            => 'KERTAS_TIMBANGAN',
                'doc_id'              => $eots->id,
            ];
            common()->upload_document($president_acquisition); 
        }

        if($appointed_company->appointed_company->acquisition->approval->acquisition_type_id == '2'){
            audit($eots, __('Perakuan Kelambatan & Lanjutan Kontrak '.$eots->bil.' bagi perolehan ' .$eots->sst->acquisition->reference. ' : ' .$eots->sst->acquisition->title. ' telah berjaya dikemaskini.'));
        } else {
            audit($eots, __('Perakuan Kelambatan & Lanjutan Kontrak bagi perolehan ' .$eots->sst->acquisition->reference. ' : ' .$eots->sst->acquisition->title. ' telah berjaya dikemaskini.'));
        }

        return response()->api([], __('Rekod berjaya dikemaskini'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eot = EOT::findByHashSlug($id);
        $detail = EotDetail::where('eot_id', $eot->id)->delete();
        $eot->delete();

        return response()->api([], __('Rekod berjaya dihapus'));
    }

    public function upload(Request $request, $id)
    {
        $eots = EOT::find($id);

        if(!empty($request->document)){
            $eot_result = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $eots->acquisition,
                'doc_type'            => 'KEPUTUSAN_EOT',
                'doc_id'              => $eots->id,
            ];
            common()->upload_document($eot_result); 
        }


        if($eots->appointed_company->acquisition->approval->acquisition_type_id == '2'){
            audit($eots, __('Keputusan EOT '.$eots->bil.' bagi perolehan ' .$eots->acquisition->reference. ' : ' .$eots->acquisition->title. ' telah berjaya dimuatnaik.'));
        } else {
            audit($eots, __('Keputusan EOT bagi perolehan ' .$eots->acquisition->reference. ' : ' .$eots->acquisition->title. ' telah berjaya dimuatnaik.'));
        }

        $workflow = WorkflowTask::where('acquisition_id', $eots->acquisition_id)->where('eot_id',$id)->first();

        $workflow->flow_desc = '';
        $workflow->url = '';
        $workflow->flow_location = 'EOT'; 
        $workflow->is_claimed = 1;
        $workflow->user_id = null;
        $workflow->workflow_team_id = 1;
        $workflow->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
    
    public function delete($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
}
