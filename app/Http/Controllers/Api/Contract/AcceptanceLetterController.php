<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\AppointedCompany;

class AcceptanceLetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $acq = Acquisition::find($request->acquisition_id);
        if ('first' == $id) {
            $sst = Sst::create([
                'user_id'                  => user()->id,
                'department_id'            => user()->department->id,
                'acquisition_id'           => $request->acquisition_id,
                'company_id'               => $request->company_id,
                'period'                   => $request->period,
                'period_type_id'           => $request->period_type_id,
                'contract_no'              => ('' != $request->contract_no ? $request->contract_no : $acq->reference),
                'company_owner_id'         => $request->company_owner_id,
                'start_working_date'       => ('' != $request->start_working_date ? Carbon::createFromFormat('d/m/Y', $request->start_working_date) : ''),
                'end_working_date'         => ('' != $request->end_working_date ? Carbon::createFromFormat('d/m/Y', $request->end_working_date) : ''),
                'letter_date'              => ('' != $request->letter_date ? Carbon::createFromFormat('d/m/Y', $request->letter_date) : Carbon::now()),
                'defects_liability_period' => $request->defects_liability_period,
                'sl1m_allowance'           => ('' != $request->sl1m_allowance ? money()->toMachine($request->sl1m_allowance) : money()->toMachine(0)),
                'sl1m_entity'              => $request->sl1m_entity,
                'document_blr'             => ('' != $request->document_blr ? money()->toMachine($request->document_blr) : money()->toMachine(0)),
                'document_LAD_amount'      => ('' != $request->document_LAD_amount ? money()->toMachine($request->document_LAD_amount) : money()->toMachine(0)),
                'bon_amount'               => money()->toMachine($request->bon_amount ?? 0),
                'ins_pli_start_date'       => ('' != $request->ins_pli_start_date ? Carbon::createFromFormat('d/m/Y', $request->ins_pli_start_date) : Carbon::now()),
                'ins_pli_end_date'         => ('' != $request->ins_pli_end_date ? Carbon::createFromFormat('d/m/Y', $request->ins_pli_end_date) : Carbon::now()),
                'ins_pli_amount'           => money()->toMachine($request->ins_pli_amount ?? 0),
                'ins_work_start_date'      => ('' != $request->ins_work_start_date ? Carbon::createFromFormat('d/m/Y', $request->ins_work_start_date) : Carbon::now()),
                'ins_work_end_date'        => ('' != $request->ins_work_end_date ? Carbon::createFromFormat('d/m/Y', $request->ins_work_end_date) : Carbon::now()),
                'ins_work_amount'          => money()->toMachine($request->ins_work_amount ?? 0),
                'ins_ci_start_date'        => ('' != $request->ins_ci_start_date ? Carbon::createFromFormat('d/m/Y', $request->ins_ci_start_date) : Carbon::now()),
                'ins_ci_end_date'          => ('' != $request->ins_ci_end_date ? Carbon::createFromFormat('d/m/Y', $request->ins_ci_end_date) : Carbon::now()),
                'ins_ci_amount'            => money()->toMachine($request->ins_ci_amount ?? 0),
                'report_type_id'           => $request->report_type_id,
                // 'vp_sign_date'             => ('' != $request->vp_sign_date ? Carbon::createFromFormat('d/m/Y', $request->vp_sign_date) : Carbon::now()),
                // 'al_issue_date'            => ('' != $request->al_issue_date ? Carbon::createFromFormat('d/m/Y', $request->al_issue_date) : Carbon::now()),
                // 'al_contractor_date'       => ('' != $request->al_contractor_date ? Carbon::createFromFormat('d/m/Y', $request->al_contractor_date) : Carbon::now()),
            ]);
            audit($sst, __('Surat Setuju Terima bagi perolehan ' .$sst->acquisition->reference. ' : ' .$sst->acquisition->title. ' telah berjaya disimpan.'));
            $company = AppointedCompany::where('acquisition_id', $request->acquisition_id)->first();

            $workflow = WorkflowTask::where('acquisition_id', $request->acquisition_id)->first();
            $workflow->flow_desc = 'Surat Setuju Terima : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->role_id = 4;
            $workflow->flow_location = 'SST';
            $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
            $workflow->update();

            $acquisition = Acquisition::find($request->acquisition_id);

            $acquisition->status_task = config('enums.flow9');
            $acquisition->update();
        } else {
            $sst = Sst::findByHashSlug($id);
            $sst->update([
                'user_id'                  => user()->id,
                'acquisition_id'           => $request->acquisition_id,
                'company_id'               => $request->company_id,
                'period'                   => $request->period,
                'period_type_id'           => $request->period_type_id,
                'contract_no'              => ('' != $request->contract_no ? $request->contract_no : $acq->reference),
                'company_owner_id'         => $request->company_owner_id,
                'start_working_date'       => ('' != $request->start_working_date ? Carbon::createFromFormat('d/m/Y', $request->start_working_date) : ''),
                'end_working_date'         => ('' != $request->end_working_date ? Carbon::createFromFormat('d/m/Y', $request->end_working_date) : ''),
                'letter_date'              => ('' != $request->letter_date ? Carbon::createFromFormat('d/m/Y', $request->letter_date) : Carbon::now()),
                'defects_liability_period' => $request->defects_liability_period,
                'sl1m_allowance'           => (null != $request->sl1m_allowance ? money()->toMachine($request->sl1m_allowance) : money()->toMachine('0')),
                'sl1m_entity'              => $request->sl1m_entity,
                'document_blr'             => ('' != $request->document_blr ? money()->toMachine($request->document_blr) : money()->toMachine(0)),
                'document_LAD_amount'      => ('' != $request->document_LAD_amount ? money()->toMachine($request->document_LAD_amount) : money()->toMachine(0)),
                'bon_amount'               => money()->toMachine($request->bon_amount ?? 0),
                'ins_pli_start_date'       => ('' != $request->ins_pli_start_date ? Carbon::createFromFormat('d/m/Y', $request->ins_pli_start_date) : Carbon::now()),
                'ins_pli_end_date'         => ('' != $request->ins_pli_end_date ? Carbon::createFromFormat('d/m/Y', $request->ins_pli_end_date) : Carbon::now()),
                'ins_pli_amount'           => money()->toMachine($request->ins_pli_amount ?? 0),
                'ins_work_start_date'      => ('' != $request->ins_work_start_date ? Carbon::createFromFormat('d/m/Y', $request->ins_work_start_date) : Carbon::now()),
                'ins_work_end_date'        => ('' != $request->ins_work_end_date ? Carbon::createFromFormat('d/m/Y', $request->ins_work_end_date) : Carbon::now()),
                'ins_work_amount'          => money()->toMachine($request->ins_work_amount ?? 0),
                'ins_ci_start_date'        => ('' != $request->ins_ci_start_date ? Carbon::createFromFormat('d/m/Y', $request->ins_ci_start_date) : Carbon::now()),
                'ins_ci_end_date'          => ('' != $request->ins_ci_end_date ? Carbon::createFromFormat('d/m/Y', $request->ins_ci_end_date) : Carbon::now()),
                'ins_ci_amount'            => money()->toMachine($request->ins_ci_amount ?? 0),
                'report_type_id'           => $request->report_type_id,
                // 'vp_sign_date'             => ('' != $request->vp_sign_date ? Carbon::createFromFormat('d/m/Y', $request->vp_sign_date) : Carbon::now()),
                // 'al_issue_date'            => ('' != $request->al_issue_date ? Carbon::createFromFormat('d/m/Y', $request->al_issue_date) : Carbon::now()),
                // 'al_contractor_date'       => ('' != $request->al_contractor_date ? Carbon::createFromFormat('d/m/Y', $request->al_contractor_date) : Carbon::now()),
            ]);
            audit($sst, __('Surat Setuju Terima bagi perolehan ' .$sst->acquisition->reference. ' : ' .$sst->acquisition->title. ' telah berjaya dikemaskini.'));
            $company = AppointedCompany::where('acquisition_id', $request->acquisition_id)->first();

            $workflow = WorkflowTask::where('acquisition_id', $request->acquisition_id)->first();
            $workflow->flow_desc = 'Surat Setuju Terima : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->flow_location = 'SST';
            $workflow->role_id = 4;
            $workflow->url = '/acceptance-letter/'.$company->hashslug.'/edit#sst';
            $workflow->update();

            $acquisition = Acquisition::find($request->acquisition_id);

            $acquisition->status_task = config('enums.flow9');
            $acquisition->update();
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Acquisition::hashslug($id)->delete();
        return response()->api([], __('Rekod telah berjaya dipadamkan.'));
    }

    public function upload(Request $request, $id)
    {
        $this->validate($request, [
            'vp_sign_date' => 'required',
            'al_issue_date' => 'required',
            'al_contractor_date' => 'required'
        ], [
            'vp_sign_date.required' => __('Sila Masukkan Tarikh Tandatangan Naib Presiden'),
            'al_issue_date.required' => __('Sila Masukkan Tarikh SST Dikeluarkan'),
            'al_contractor_date.required' => __('Sila Masukkan Tarikh Terima SST Dari Kontraktor'),
        ]);

        $sst = Sst::find($id);

        Sst::find($id)->update([
            'vp_sign_date'       => ('' != $request->vp_sign_date ? Carbon::createFromFormat('d/m/Y', $request->vp_sign_date) : Carbon::now()),
            'al_issue_date'      => ('' != $request->al_issue_date ? Carbon::createFromFormat('d/m/Y', $request->al_issue_date) : Carbon::now()),
            'al_contractor_date' => ('' != $request->al_contractor_date ? Carbon::createFromFormat('d/m/Y', $request->al_contractor_date) : Carbon::now()),
        ]);

        if ($request->hasFile('document')) {
            $sst_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $sst->acquisition,
                'doc_type'            => 'SST',
                'doc_id'              => $sst->id,
            ];
            common()->upload_document($sst_requirements);

            //$sst->status = 3;
            $sst->update();
        }

        // nota: 
        // 1 -> semakan
        // 2 -> selesai semakan
        // 3 -> selesai upload sst
        if(!empty($request->action)){
            Sst::find($id)->update([
                'status'    =>  '3',
            ]);
            
            $workflow = WorkflowTask::where('acquisition_id', $sst->acquisition_id)->first();
            // $workflow->flow_desc = 'Sijil Bayaran Interim : Sila teruskan dengan IPC.';
            // $workflow->user_id = user()->id;
            // $workflow->flow_location = 'IPC';
            // $workflow->url = '/post/ipc/'.$sst->hashslug.'/edit';
            // $workflow->update();

            $workflow->flow_desc = 'Dokumen Kontrak : Sila teruskan dengan Dokumen Kontrak.';
            $workflow->url = '/contract/post/'.$sst->hashslug.'/edit#bon-implementation';
            $workflow->flow_location = 'Dokumen Kontrak'; 
            $workflow->is_claimed = 1;
            $workflow->user_id = user()->id;
            $workflow->role_id = 4;
            $workflow->workflow_team_id = 1;
            $workflow->update();

            $sst->acquisition->status_task = config('enums.flow10');
            $sst->acquisition->update();
        }


        audit($sst, __('Muat Naik Kelulusan Surat Setuju Terima bagi perolehan ' .$sst->acquisition->reference. ' : ' .$sst->acquisition->title. ' telah berjaya disimpan.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
