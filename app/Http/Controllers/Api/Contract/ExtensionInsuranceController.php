<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\EotInsurance;
use App\Models\Acquisition\Insurance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use App\Models\Acquisition\Eot;
use Illuminate\Support\Facades\Storage;

class ExtensionInsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $insurance = EotInsurance::updateOrCreate([
            'eot_id' => $id,
        ], [
            'user_id'           => user()->id,
            'insurance_old_new' => $request->input('insurance_old_new'),

            'public_insurance_name'         => $request->input('public_insurance_name'),
            'public_policy_no'              => $request->input('public_policy_no'),
            'public_policy_amount'          => $request->input('public_policy_amount') ? money()->toMachine($request->input('public_policy_amount')) : null,
            'public_received_date'          => $request->input('public_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_received_date')) : null,
            'public_start_date'             => $request->input('public_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_start_date')) : null,
            'public_expired_date'           => $request->input('public_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_expired_date')) : null,
            'public_proposed_date'          => $request->input('public_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_proposed_date')) : null,
            'public_approval_proposed_date' => $request->input('public_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_approval_proposed_date')) : null,
            'public_approval_received_date' => $request->input('public_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_approval_received_date')) : null,

            'work_insurance_name'         => $request->input('work_insurance_name'),
            'work_policy_no'              => $request->input('work_policy_no'),
            'work_policy_amount'          => $request->input('work_policy_amount') ? money()->toMachine($request->input('work_policy_amount')) : null,
            'work_received_date'          => $request->input('work_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_received_date')) : null,
            'work_start_date'             => $request->input('work_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_start_date')) : null,
            'work_expired_date'           => $request->input('work_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_expired_date')) : null,
            'work_proposed_date'          => $request->input('work_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_proposed_date')) : null,
            'work_approval_proposed_date' => $request->input('work_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_approval_proposed_date')) : null,
            'work_approval_received_date' => $request->input('work_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_approval_received_date')) : null,

            'compensation_insurance_name'         => $request->input('compensation_insurance_name'),
            'compensation_policy_no'              => $request->input('compensation_policy_no'),
            'compensation_policy_amount'          => $request->input('compensation_policy_amount') ? money()->toMachine($request->input('compensation_policy_amount')) : null,
            'compensation_received_date'          => $request->input('compensation_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_received_date')) : null,
            'compensation_start_date'             => $request->input('compensation_start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_start_date')) : null,
            'compensation_expired_date'           => $request->input('compensation_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_expired_date')) : null,
            'compensation_proposed_date'          => $request->input('compensation_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_proposed_date')) : null,
            'compensation_approval_proposed_date' => $request->input('compensation_approval_proposed_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_approval_proposed_date')) : null,
            'compensation_approval_received_date' => $request->input('compensation_approval_received_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_approval_received_date')) : null,
        ]);

        $acq_insurance = Insurance::updateOrCreate([
            'sst_id' => $insurance->eot->sst->id,
        ], [
            'public_expired_date'           => $request->input('public_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('public_expired_date')) : null,
            'work_expired_date'           => $request->input('work_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('work_expired_date')) : null,
            'compensation_expired_date'           => $request->input('compensation_expired_date') ? Carbon::createFromFormat('d/m/Y', $request->input('compensation_expired_date')) : null,
        ]);

        $publicLiability_requirements = [
            'request_hDocumentId' => $request->hDocumentId_publicLiability,
            'request_hasFile'     => $request->hasFile('uploadFile_publicLiability'),
            'request_file'        => $request->file('uploadFile_publicLiability'),
            'acquisition'         => $insurance->eot->appointed_company->acquisition,
            'doc_type'            => 'EOT_INSURANCE_PUBLIC',
            'doc_id'              => $insurance->id,
        ];
        common()->upload_document($publicLiability_requirements);

        $work_requirements = [
            'request_hDocumentId' => $request->hDocumentId_work,
            'request_hasFile'     => $request->hasFile('uploadFile_work'),
            'request_file'        => $request->file('uploadFile_work'),
            'acquisition'         => $insurance->eot->appointed_company->acquisition,
            'doc_type'            => 'EOT_INSURANCE_WORK',
            'doc_id'              => $insurance->id,
        ];
        common()->upload_document($work_requirements);

        $compensation_requirements = [
            'request_hDocumentId' => $request->hDocumentId_compensation,
            'request_hasFile'     => $request->hasFile('uploadFile_compensation'),
            'request_file'        => $request->file('uploadFile_compensation'),
            'acquisition'         => $insurance->eot->appointed_company->acquisition,
            'doc_type'            => 'EOT_INSURANCE_COMPENSATION',
            'doc_id'              => $insurance->id,
        ];
        common()->upload_document($compensation_requirements);

        if($insurance->eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){
            audit($insurance, __('Insurans Pelanjutan Masa '.$insurance->eot->bil.' bagi perolehan ' .$insurance->eot->sst->acquisition->reference. ' : ' .$insurance->eot->sst->acquisition->title. ' telah berjaya disimpan.'));
        } else {
            audit($insurance, __('Insurans Pelanjutan Masa bagi perolehan ' .$insurance->eot->sst->acquisition->reference. ' : ' .$insurance->eot->sst->acquisition->title. ' telah berjaya disimpan.'));
        }

        $workflow = WorkflowTask::where('acquisition_id', $insurance->eot->appointed_company->acquisition_id)->where('eot_id',$insurance->eot_id)->first();
        $eot = EOT::find($id);
        if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2'){
            $workflow->flow_desc = 'Keputusan Perakuan Kelambatan & Lanjutan Kontrak '.$eot->bil.' : Sila muatnaik keputusan EOT.';
        } else {
            $workflow->flow_desc = 'Keputusan Perakuan Kelambatan & Lanjutan Kontrak : Sila muatnaik keputusan EOT.';
        }
        $workflow->url = '/extension/extension_edit/'.$eot->hashslug.'/edit#upload';
        $workflow->flow_location = 'EOT';
        $workflow->is_claimed = 1;
        $workflow->user_id = $eot->user_id;
        $workflow->role_id = 4;
        $workflow->workflow_team_id = 1;

        $workflow->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
