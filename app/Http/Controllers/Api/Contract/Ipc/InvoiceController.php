<?php

namespace App\Http\Controllers\Api\Contract\Ipc;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\IpcBq;
use App\Models\Acquisition\IpcInvoice;
use App\Models\BillOfQuantity\SubItem;
use App\Models\VO\SubItem as WpsSub;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class InvoiceController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
    }

    public function show($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        DB::transaction(function () use($id) {
            $inv = IpcInvoice::findByHashSlug($id);
            if(!empty($inv) && !empty($inv->ipcBq) && $inv->ipcBq->count() > 0){
                foreach($inv->ipcBq as $bq){
                    if($bq->status == 'N'){
                        $subs = SubItem::find($bq->bq_subitem_id);
                        $subs->baki_quantity = $subs->baki_quantity + $bq->quantity;
                        $subs->paid_quantity = $subs->paid_quantity - $bq->quantity;
                        $subs->save();
                    }else{
                        $subs = WpsSub::find($bq->bq_subitem_id);
                        $subs->baki_quantity = $subs->baki_quantity + $bq->quantity;
                        $subs->paid_quantity = $subs->paid_quantity - $bq->quantity;
                        $subs->save();
                    }
                }
            }
            IpcBq::where('ipc_invoice_id', $inv->id)->where('deleted_at',null)->delete();
            IpcInvoice::where('id', $inv->id)->where('deleted_at',null)->delete();
            Document::where('document_types','INVOICE/'.$inv->id)->where('document_id',$inv->id)->where('deleted_at',null)->delete();
        
        }, 10);
        
        
        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
}
