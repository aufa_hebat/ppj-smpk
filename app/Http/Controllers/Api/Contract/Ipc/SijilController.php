<?php

namespace App\Http\Controllers\Api\Contract\Ipc;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\IpcBq;
use App\Models\Acquisition\IpcVo;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Acquisition\IpcMaterial;
use App\Models\BillOfQuantity\SubItem;
use App\Models\VO\PPJHK\SubItem as VoSub;
use App\Models\VO\SubItem as WpsSub;
use App\Models\VO\Cancel;
use App\Models\Acquisition\Review;
use App\Models\Document;
use App\Models\Singleton\SingletonIpc;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\WorkflowTask;
use Illuminate\Support\Facades\DB;

class SijilController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'evaluation_at' => 'required',
        ], [
            'evaluation_at.required' => __('Tarikh Penilaian Diperlukan'),
        ]);
        
        DB::transaction(function () use($request) {
            if(!empty($request->sst_id)){
            
                $ipcs = SingletonIpc::save($request);
                
                $workflow = new WorkflowTask();
                $workflow->acquisition_id = $ipcs->sst->acquisition_id;
                $workflow->ipc_id = $ipcs->id;
                $workflow->flow_name = $ipcs->sst->acquisition->reference . ' : ' . $ipcs->sst->acquisition->title;
                $workflow->flow_desc = 'Sijil Bayaran Interim ' .$ipcs->ipc_no. ' : Sila teruskan dengan IPC.';
                $workflow->flow_location = 'IPC';
                $workflow->url = '/post/ipc/'.$ipcs->hashslug.'/ipc_invoice_edit#interim-invois';
                $workflow->user_id = user()->id;
                $workflow->role_id = 4;
                $workflow->is_claimed = 1;
                $workflow->workflow_team_id = 1;
    
                $workflow2 = new WorkflowTask();
                $workflow2->acquisition_id = $ipcs->sst->acquisition_id;
                $workflow2->ipc_id = $ipcs->id;
                $workflow2->flow_name = $ipcs->sst->acquisition->reference . ' : ' . $ipcs->sst->acquisition->title;
                $workflow2->flow_desc = 'Sijil Bayaran Interim ' .$ipcs->ipc_no. ' : Sila teruskan dengan IPC.';
                $workflow2->flow_location = 'IPC';
                $workflow2->url = '/post/ipc/'.$ipcs->hashslug.'/ipc_invoice_edit#interim-invois';
                $workflow2->is_claimed = 1;
                $workflow2->workflow_team_id = 2;
    
                $workflow->save();
                $workflow2->save();
    
                $ipcs->sst->acquisition->status_task = config('enums.flow11');
                $ipcs->sst->acquisition->update();
                  
            }
        }, 10);
        
        return response()->json(['hashslug' => $ipcs->hashslug ?? null, 'message' => __('Rekod telah berjaya ditambah.')]);
    }

    public function show($id)
    {
    }

    public function update(Request $request, $id)
    {
//        dd($request);  
//        dd($request->vo);
//        $this->validate($request, [
//            'fund_id' => 'required',
//        ], [
//            'fund_id.required' => __('Maklumat Kewangan Diperlukan'),
//        ]);
        DB::transaction(function () use($request,$id) {
            Ipc::findByHashSlug($id)->update([
                'short_text'                        => $request->short_text ?? '',
                'evaluation_at'                     => (! empty($request->evaluation_at)) ? Carbon::createFromFormat('d/m/Y', $request->evaluation_at) : null,
                'long_text'                         => $request->long_text ?? '',
                'last_ipc'                          => $request->last_ipc ?? 'n',
                'demand_amount'                     => money()->toMachine($request->demand_amount ?? 0),
                'compensation_amount'               => money()->toMachine($request->compensation_amount ?? 0),
                'lad_day'                           => $request->lad_day ?? 0,
                'lad_rate'                          => money()->toMachine($request->lad_rate ?? 0),
                'advance_amount'                    => money()->toMachine($request->advance_amount ?? 0),
                'give_advance_amount'               => money()->toMachine($request->give_advance_amount ?? 0),
                'wjp_amount'                        => money()->toMachine($request->wjp_amount ?? 0),
                'wjp_lepas_amount'                  => money()->toMachine($request->wjp_lepas_amount ?? 0),
                'working_change_amount'             => money()->toMachine($request->working_change_amount ?? 0),
                'new_contract_amount'               => money()->toMachine($request->new_contract_amount ?? 0),
                'credit_no'                         => $request->credit_no ?? '',
                'credit'                            => money()->toMachine($request->credit ?? 0),
                'credit_at'                         => (! empty($request->credit_at)) ? Carbon::createFromFormat('d/m/Y', $request->credit_at) : null,
                'credit_receive_at'                 => (! empty($request->credit_receive_at)) ? Carbon::createFromFormat('d/m/Y', $request->credit_receive_at) : null,
            ]);
    
            $ipc = Ipc::findByHashSlug($id);
            $ida = [];
            $idS = [];
            if (! empty($request->exist)) {
                foreach ($request->exist as $ex) {
                    $checkMaterial = IpcMaterial::where('ipc_id', $ipc->id)->where('deleted_at', null)->get();
                    if (! empty($checkMaterial)) {
                        foreach ($checkMaterial as $c) {
                            $ida[] = $c['id'];
                        }
                        $idS[] = $ex['material'];
                    }
                }
                $diff = array_diff($ida, $idS);
    
                foreach ($diff as $ad) {
                    IpcMaterial::destroy($ad);
                }
            } else {
                IpcMaterial::where('ipc_id', $ipc->id)->where('deleted_at', null)->delete();
            }
    
            if (! empty($request->material)) {
                foreach ($request->material as $material) {
                    if (! empty($material['desc'])) {
                        $ipc_mat = new IpcMaterial();
    
                        $ipc_mat->user_id     = user()->id;
                        $ipc_mat->ipc_id      = $ipc->id          ?? 0;
                        $ipc_mat->sst_id      = $request->sst_id  ?? 0;
                        $ipc_mat->description = $material['desc'] ?? '';
                        $ipc_mat->unit        = $material['unit'] ?? '';
                        $ipc_mat->quantity    = money()->toMachine($material['quantity'] ?? 0);
                        $ipc_mat->rate        = money()->toMachine($material['rate'] ?? 0);
                        $ipc_mat->amount      = money()->toMachine($material['amount'] ?? 0);
    
                        $ipc_mat->save();
                    }
                }
            }
            
            if($request->hasFile('documentMat')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentMat'),
                    'request_file'        => $request->file('documentMat'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/MATERIAL/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
            
            $ipc->update([
                'material_on_site_amount' => money()->toMachine($request->material_on_site_amount ?? 0),
            ]);
            //save invoice
            if(!empty($request->inv)){
                foreach($request->inv as $inv){
                    if($inv['edit'] == 'y'){//for editing invoice
                        IpcInvoice::findByHashSlug($inv['id'])->update([
                            'invoice_no'                          => $inv['invoice_no'] ?? '',
                            'invoice_at'                          => (! empty($inv['invoice_at'])) ? Carbon::createFromFormat('d/m/Y', $inv['invoice_at']) : null,
                            'invoice_receive_at'                  => (! empty($inv['invoice_receive_at'])) ? Carbon::createFromFormat('d/m/Y', $inv['invoice_receive_at']) : null,
                            'ref_text'                            => $inv['ref_text'] ?? '',
                            'company_id'                          => $inv['company_id'] ?? '',
                            'status_contractor'                   => $inv['status_contractor'] ?? 1,
                            'acquisition_approval_financial_id'   => $inv['fund_id'] ?? '',
                            'demand_amount'                       => money()->toMachine($inv['demand_amount'] ?? 0),
                            'invoice'                             => money()->toMachine($inv['invoice'] ?? 0),
                            'sst_amount'                          => money()->toMachine($inv['sst_amount'] ?? 0),
                            'acquisition_sub_contract_id'         => (!empty($inv['subkon_id']) && 0 != $inv['subkon_id'])? $inv['subkon_id']:null,
                            'bulan_status'                        => $inv['bulan_status'] ?? null,
                        ]);
                        $invoice = IpcInvoice::findByHashSlug($inv['id']);
                        //save bq
                        if (! empty($request->adjust)) {
    //                        dd($request->adjust);
                            foreach ($request->adjust as $adjust) {
                                if (! empty($adjust['id']) && 0 != $adjust['id']) {
                                    if ((empty($adjust['edit']) || 0 == $adjust['edit']) && round($adjust['amount']) != "0") {
                                        $sub_item = null;
                                        if($adjust['status'] == 'N'){
                                            $sub_item              = SubItem::find($adjust['id']);
                                        }else{
                                            $sub_item              = WpsSub::find($adjust['id']);
                                        }
                                        
                                        $ipc_bq = new IpcBq();
    
                                        $ipc_bq->user_id           = user()->id;
                                        $ipc_bq->ipc_invoice_id    = $invoice->id              ?? 0;
                                        $ipc_bq->sst_id            = $request->sst_id          ?? 0;
                                        $ipc_bq->bq_subitem_id     = $adjust['id']             ?? 0;
                                        $ipc_bq->bq_item_id        = ($sub_item != null)? $sub_item->bq_item_id : 0;
                                        $ipc_bq->bq_element_id     = ($sub_item != null)? $sub_item->bq_element_id : 0;
                                        $ipc_bq->status            = $adjust['status']         ?? 'N';
                                        $ipc_bq->quantity          = money()->toMachine($adjust['quantity'] ?? 0);
                                        $ipc_bq->amount            = money()->toMachine($adjust['amount'] ?? 0);
    
                                        $ipc_bq->save();
                                        if($sub_item != null){
                                            $sub_item->baki_quantity   = $sub_item->baki_quantity - $ipc_bq->quantity;
                                            $sub_item->paid_quantity   = $sub_item->paid_quantity + $ipc_bq->quantity;
                                            $sub_item->update();
                                        }
                                        
                                    } else {// kemaskini data
                                        $ipc_bqs                   = IpcBq::find($adjust['edit']);
                                        if(round($adjust['amount']) != "0"){//update value baru
                                            
                                            if($adjust['status'] == 'N'){
                                                $sub_item1                  = SubItem::find($adjust['id']);
                                                $sub_item1->baki_quantity   = $sub_item1->baki_quantity + $ipc_bqs->quantity - money()->toMachine($adjust['quantity'] ?? 0);
                                                $sub_item1->paid_quantity   = $sub_item1->paid_quantity - $ipc_bqs->quantity + money()->toMachine($adjust['quantity'] ?? 0);
                                                $sub_item1->update();
                                            }else{
                                                $sub_item2                  = WpsSub::find($adjust['id']);
                                                $sub_item2->baki_quantity   = $sub_item2->baki_quantity + $ipc_bqs->quantity - money()->toMachine($adjust['quantity'] ?? 0);
                                                $sub_item2->paid_quantity   = $sub_item2->paid_quantity - $ipc_bqs->quantity + money()->toMachine($adjust['quantity'] ?? 0);
                                                $sub_item2->update();
                                            }
                                            $ipc_bqs->quantity         = money()->toMachine($adjust['quantity'] ?? 0);
                                            $ipc_bqs->amount           = money()->toMachine($adjust['amount'] ?? 0);
                                            $ipc_bqs->update();
                                        }
                                        
                                    }
                                }else{//previous ade data but edit and untick
                                    if (!empty($adjust['edit']) && round($adjust['amount']) == "0") {
                                        $ipc_bq                   = IpcBq::find($adjust['edit']);
                                        if(!empty($ipc_bq)){
                                            if($adjust['status'] == 'N'){
                                                $sub_item1                  = SubItem::find($ipc_bq->bq_subitem_id);
                                                $sub_item1->baki_quantity   = $sub_item1->baki_quantity + $ipc_bq->quantity;
                                                $sub_item1->paid_quantity   = $sub_item1->paid_quantity - $ipc_bq->quantity;
                                                $sub_item1->update();
                                            }else{
                                                $sub_item2                  = WpsSub::find($ipc_bq->bq_subitem_id);
                                                $sub_item2->baki_quantity   = $sub_item2->baki_quantity + $ipc_bq->quantity;
                                                $sub_item2->paid_quantity   = $sub_item2->paid_quantity - $ipc_bq->quantity;
                                                $sub_item2->update();
                                            }
                                            $ipc_bq->delete();
                                        }
                                    }
                                }
                            }
                        }
                        //save bq
                    }else{//for new invoice save
                        $invoice = IpcInvoice::create([
                            'user_id'                               => user()->id,
                            'sst_id'                                => $request->sst_id ?? '',
                            'ipc_id'                                => $request->ipc_id ?? '',
                            'invoice_no'                            => $inv['invoice_no'] ?? '',
                            'invoice_at'                            => (! empty($inv['invoice_at'])) ? Carbon::createFromFormat('d/m/Y', $inv['invoice_at']) : null,
                            'invoice_receive_at'                    => (! empty($inv['invoice_receive_at'])) ? Carbon::createFromFormat('d/m/Y', $inv['invoice_receive_at']) : null,
                            'ref_text'                              => $inv['ref_text'] ?? '',
                            'company_id'                            => $inv['company_id'] ?? '',
                            'status_contractor'                     => $inv['status_contractor'] ?? 1,
                            'acquisition_approval_financial_id'     => $inv['fund_id'] ?? '',
                            'demand_amount'                         => money()->toMachine($inv['demand_amount'] ?? 0),
                            'invoice'                               => money()->toMachine($inv['invoice'] ?? 0),
                            'sst_amount'                            => money()->toMachine($inv['sst_amount'] ?? 0),
                            'acquisition_sub_contract_id'           => (!empty($inv['subkon_id']) && 0 != $inv['subkon_id'])? $inv['subkon_id']:null,
                            'bulan_status'                          => $inv['bulan_status'] ?? null,
                            'status'                                => 1,
                        ]);
                        $ipc->update([
                            'demand_amount' => $ipc->demand_amount + money()->toMachine($request->demand_amount ?? 0),
                        ]);
                        
                        $temp_id = $invoice->id;
                        //save bq
                        if (! empty($request->adjust)) {
                            foreach ($request->adjust as $adjust) {
                                if (! empty($adjust['id']) && 0 != $adjust['id']) {//for tick checkbox
                                    if ((empty($adjust['edit']) || 0 == $adjust['edit']) && round($adjust['amount']) != "0") {//for new bq or vo peruntukan
                                        $ipc_bq = new IpcBq();
                                        $sub_item = null;
                                        if($adjust['status'] == 'N'){
                                            $sub_item                  = SubItem::find($adjust['id']);
                                        }else{
                                            $sub_item                  = WpsSub::find($adjust['id']);
                                        }
                                        
                                        $ipc_bq->user_id            = user()->id;
                                        $ipc_bq->ipc_invoice_id     = $temp_id ?? 0;
                                        $ipc_bq->sst_id             = $request->sst_id     ?? 0;
                                        $ipc_bq->bq_subitem_id      = $adjust['id']        ?? 0;
                                        $ipc_bq->bq_item_id         = ($sub_item != null)? $sub_item->bq_item_id : 0;
                                        $ipc_bq->bq_element_id      = ($sub_item != null)? $sub_item->bq_element_id : 0;
                                        $ipc_bq->status             = $adjust['status']         ?? 'N';
                                        $ipc_bq->quantity           = money()->toMachine($adjust['quantity'] ?? 0);
                                        $ipc_bq->amount             = money()->toMachine($adjust['amount'] ?? 0);
    
                                        $ipc_bq->save();
                                        
                                        if($sub_item != null){
                                            $sub_item->baki_quantity   = $sub_item->baki_quantity - $ipc_bq->quantity;
                                            $sub_item->paid_quantity   = $sub_item->paid_quantity + $ipc_bq->quantity;
                                            $sub_item->update();
                                        }
                                    }
                                }
                            }
                        }
                        //save bq
                    }
                    $acq = Acquisition::find($request->acquisition_id);
                    if($request->hasFile('document'.$inv['running'])){
                        $inv_requirements = [
                            'request_hDocumentId' => null,
                            'request_hasFile'     => $request->hasFile('document'.$inv['running']),
                            'request_file'        => $request->file('document'.$inv['running']),
                            'acquisition'         => $acq,
                            'doc_type'            => 'INVOICE/'.$invoice->id,
                            'doc_id'              => $invoice->id,
                        ];
                        common()->upload_document($inv_requirements);
                    }
                }
            }
            //save invoice
            //save updated calc 
            $ipc     = Ipc::findByHashSlug($id);
            $adv     = ipc($ipc)->getAdvBalikAmount();
            $wjp     = ipc($ipc)->getWjpAmount();
            $tahanan = ipc($ipc)->getMaterialTahanAmount();
            
            Ipc::findByHashSlug($id)->update([
                'give_advance_amount'               => $adv['tarik_adv'],
                'wjp_amount'                        => $wjp['wjp'],
                'material_tahanan_amount'           => $tahanan,
            ]);
            //save vo 
            if(!empty($request->vo)){
                foreach($request->vo as $vos){
                    if (! empty($vos['subid']) && 0 != $vos['subid']) {
                        if (!empty($vos['check']) && round($vos['amt']) != "0") {//for tick checkbox
                            if(!empty($vos['ipc_vo_id']) && $vos['ipc_vo_id'] != null ){//kemaskini bcoz got ipc vo number
                                $vo_edit_sub                 = VoSub::find($vos['subid']);
                                $ipc_vo_edit                 = IpcVo::find($vos['ipc_vo_id']);
                                
                                $vo_edit_sub->baki_quantity    = $vo_edit_sub->baki_quantity + $ipc_vo_edit->quantity - money()->toMachine($vos['qty'] ?? 0);
                                $vo_edit_sub->paid_quantity    = $vo_edit_sub->paid_quantity - $ipc_vo_edit->quantity + money()->toMachine($vos['qty'] ?? 0);
                                $vo_edit_sub->update();
                                
                                $ipc_vo_edit->quantity          = money()->toMachine($vos['qty'] ?? 0);
                                $ipc_vo_edit->amount            = money()->toMachine($vos['amt'] ?? 0);
    
                                $ipc_vo_edit->update();
                                
                                
                            }else{//new save ipc vo
                                
                                $vo_sub                 = VoSub::find($vos['subid']);
                                $ipc_vo = new IpcVo();
    
                                $ipc_vo->user_id           = user()->id;
                                $ipc_vo->ipc_id            = $request->ipc_id ?? 0;
                                $ipc_vo->sst_id            = $request->sst_id     ?? 0;
                                $ipc_vo->status            = 'V';
                                $ipc_vo->ppjhk_sub_item_id = $vos['subid']        ?? 0;
                                $ipc_vo->ppjhk_item_id     = $vo_sub->ppjhk_item_id     ?? 0;
                                $ipc_vo->ppjhk_element_id  = $vo_sub->ppjhk_element_id  ?? 0;
                                $ipc_vo->quantity          = money()->toMachine($vos['qty'] ?? 0);
                                $ipc_vo->amount            = money()->toMachine($vos['amt'] ?? 0);
    
                                $ipc_vo->save();
    
                                $vo_sub->baki_quantity    = $vo_sub->baki_quantity - $ipc_vo->quantity;
                                $vo_sub->paid_quantity    = $vo_sub->paid_quantity + $ipc_vo->quantity;
                                $vo_sub->update();
                            }
                        }else{//for untick
                            if(!empty($vos['ipc_vo_id']) && $vos['ipc_vo_id'] != null){//previous ada save ipc vo
                                    $ipc_vo_edit                 = IpcVo::find($vos['ipc_vo_id']);
                                    $vo_edit_sub                 = VoSub::find($ipc_vo_edit->ppjhk_sub_item_id);
                                    
                                    if(!empty($vo_edit_sub)){//check existing ppjhk vo and delete data
                                        $vo_edit_sub->baki_quantity    = $vo_edit_sub->baki_quantity + $ipc_vo_edit->quantity;
                                        $vo_edit_sub->paid_quantity    = $vo_edit_sub->paid_quantity - $ipc_vo_edit->quantity;
                                        $vo_edit_sub->update();
    
                                        $ipc_vo_edit->delete();
                                    }
                                    
                            }
                        }
                    }
                    
                }
            }
            //save vo
            
            //save updated calc 
            $ipc    = Ipc::findByHashSlug($id);
            $demand = ipc($ipc)->getDemandAmount();
            Ipc::findByHashSlug($id)->update([
                'demand_amount'    => $demand['demand'],
            ]);
            
            //save upload attachment
            if($request->hasFile('documentPerkeso')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentPerkeso'),
                    'request_file'        => $request->file('documentPerkeso'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/PERKESO/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
    
            if($request->hasFile('documentLevi')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentLevi'),
                    'request_file'        => $request->file('documentLevi'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/LEVI/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
    
            if($request->hasFile('documentSijil')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentSijil'),
                    'request_file'        => $request->file('documentSijil'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/SIJIL_IPC/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
    
            if($request->hasFile('documentSummary')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentSummary'),
                    'request_file'        => $request->file('documentSummary'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/SUMMARY_IPC/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
    
            if($request->hasFile('documentStatus')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentStatus'),
                    'request_file'        => $request->file('documentStatus'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/STATUS_IPC/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
            
            if($request->hasFile('documentDetail')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentDetail'),
                    'request_file'        => $request->file('documentDetail'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/DETAIL_IPC/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
            if($request->hasFile('documentSofa')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentSofa'),
                    'request_file'        => $request->file('documentSofa'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/SOFA_IPC/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
            if($request->hasFile('documentStatuori')){
                $inv_requirements = [
                    'request_hDocumentId' => null,
                    'request_hasFile'     => $request->hasFile('documentStatuori'),
                    'request_file'        => $request->file('documentStatuori'),
                    'acquisition'         => $ipc->sst->acquisition,
                    'doc_type'            => 'IPC/STATUORI_IPC/'.$ipc->id,
                    'doc_id'              => $ipc->id,
                ];
                common()->upload_document($inv_requirements);
            }
            //save upload attachment
        });
        
        
        return response()->api([], __('Rekod telah berjaya disimpan.'));
    }

    public function destroy($id)
    {
        DB::transaction(function () use($id) {
            $ipc = Ipc::findByHashSlug($id);
            $inv = IpcInvoice::where('ipc_id', $ipc->id)->where('deleted_at',null)->get();
            if(!empty($inv) && $inv->count() > 0){
                foreach($inv as $invoice){
                    if(!empty($invoice->ipcBq) && $invoice->ipcBq->count() > 0){
                        foreach($invoice->ipcBq as $bq){
                            if($bq->status == 'N'){
                                $subs                  = SubItem::find($bq->bq_subitem_id);
                                if($subs != null){
                                    $subs->baki_quantity   = $subs->baki_quantity + $bq->quantity;
                                    $subs->paid_quantity   = $subs->paid_quantity - $bq->quantity;
                                    $subs->update();
                                }
                            }else{
                                $subs                  = WpsSub::find($bq->bq_subitem_id);
                                if($subs != null){
                                    $subs->baki_quantity   = $subs->baki_quantity + $bq->quantity;
                                    $subs->paid_quantity   = $subs->paid_quantity - $bq->quantity;
                                    $subs->update();
                                }
                            }
                        }
                    }
                    IpcBq::where('ipc_invoice_id', $invoice->id)->where('deleted_at',null)->delete();
                    Document::where('document_types','INVOICE/'.$invoice->id)->where('document_id',$invoice->id)->where('deleted_at',null)->delete();
                    IpcInvoice::where('id', $invoice->id)->where('deleted_at',null)->delete();
                }
            }
        
            if(!empty($ipc->ipcVo) && $ipc->ipcVo->count() > 0){
                foreach($ipc->ipcVo as $vo){
                    
                    $sub_v = VoSub::find($vo->ppjhk_sub_item_id);
                    $sub_v->baki_quantity = $sub_v->baki_quantity + $vo->quantity;
                    $sub_v->paid_quantity = $sub_v->paid_quantity - $vo->quantity;
                    $sub_v->update();
                }
            }
            IpcVo::where('ipc_id', $ipc->id)->delete();
            IpcMaterial::where('ipc_id', $ipc->id)->delete();
            Document::where('document_types','IPC/MATERIAL/'.$ipc->id)->where('document_id',$ipc->id)->delete();
            Document::where('document_types','IPC/PERKESO/'.$ipc->id)->where('document_id',$ipc->id)->delete();
            Document::where('document_types','IPC/LEVI/'.$ipc->id)->where('document_id',$ipc->id)->delete();
            Document::where('document_types','IPC/SIJIL_IPC/'.$ipc->id)->where('document_id',$ipc->id)->delete();
            Document::where('document_types','IPC/SUMMARY_IPC/'.$ipc->id)->where('document_id',$ipc->id)->delete();
            Document::where('document_types','IPC/STATUS_IPC/'.$ipc->id)->where('document_id',$ipc->id)->delete();
            Ipc::where('id', $ipc->id)->delete();

            WorkflowTask::where('acquisition_id', $ipc->sst->acquisition_id)->where('ipc_id',$ipc->id)->delete();
        });
        
        
        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }
    
    public function delete_attach($id)
    {
        DB::transaction(function () use($id) {
            Document::hashslug($id)->delete();
        });
        
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
}
