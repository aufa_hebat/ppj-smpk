<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\VariationOrderCategory;
use App\Models\VO\Element;
use App\Models\VO\Item;
use App\Models\VO\SubItem;
use App\Models\VO\Type;
use App\Models\VO\VO;
use Illuminate\Http\Request;
use App\Models\VO\Active;
use App\Models\Acquisition\AppointedCompany;

class VariationOrderVOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'no'            => 'required',
            'element'       => 'required',
            'description'   => 'required',
            'vo_id'         => 'required',
            'vo_type_id'    => 'required',
            'sst_id'        => 'required',
        ]);

        $element = Element::create([
            'no'            => $request->no,
            'element'       => $request->element,
            'description'   => $request->description,
            'vo_id'         => $request->vo_id,
            'vo_type_id'    => $request->vo_type_id,
            'sst_id'        => $request->sst_id,
        ]);

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function createVoElement(Request $request, $id)
    {
        $voType = Type::findByHashSlug($id);
        $wps = null;

        if($request->activeId){
            $wps = Active::findByHashSlug($request->activeId)->id;
        }
        

        $lastElement = Element::where([
            ['vo_id', '=', $voType->vo->id],
            ['vo_type_id', '=', $voType->id],
            ['sst_id', '=', $voType->vo->sst->id]
        ])->orderBy('no', 'desc')->first();

        if($lastElement == null){
            $newNo = 1;
        }else{
            $newNo = $lastElement->no + 1;
        }  

        $element = Element::updateOrCreate([
            'id'    => substr($request->id,1)
        ],
        [
            'no'            => $newNo,
            'element'       => str_replace("'","`",$request->name),
            'description'   => str_replace("'","`",$request->description),
            'vo_id'         => $voType->vo->id,
            'vo_type_id'    => $voType->id,
            'sst_id'        => $voType->vo->sst->id,
            'vo_active_id'  => $wps
        ]);

        // return response()->api([], __('Rekod telah berjaya dikemaskini.'));

        return response()->json(['id'=>$element->id]);
    }

    public function createVoItem(Request $request, $id)
    {
        $voType = Type::findByHashSlug($id);
        $element = Element::find(substr($request->pid,1));

        $lastItem = Item::where([
            ['vo_id', '=', $voType->vo->id],
            ['vo_type_id', '=', $voType->id],
            ['sst_id', '=', $voType->vo->sst->id],
            ['vo_element_id', '=', substr($request->pid,1)],
        ])->orderBy('no', 'desc')->first();

        if($lastItem == null){
            $newNo = 1;
        }else{
            $newNo = $lastItem->no + 1;
        }        

        $item = Item::updateOrCreate([
            'id'    => substr($request->id,1)
        ],
        [
            'no'            => $newNo,
            'item'          => str_replace("'", "`",$request->name),
            'description'   => str_replace("'", "`",$request->description),
            'vo_id'         => $voType->vo->id,
            'vo_type_id'    => $voType->id,
            'sst_id'        => $voType->vo->sst->id,
            'vo_element_id' => substr($request->pid,1),
            'vo_active_id'  => $element->vo_active_id,
        ]);

        // return response()->api([], __('Rekod telah berjaya dikemaskini.'));
        return response()->json(['id'=>$item->id]);
    }

    public function createVoSubItem(Request $request, $id)
    {
        $voType = Type::findByHashSlug($id);
        $sst = $voType->vo->sst;
        $item = Item::find(substr($request->pid,1));
        $appointed = AppointedCompany::where([
                ['acquisition_id', $sst->acquisition_id],
                ['company_id', $sst->company_id],
            ])->first();

        $lastSubItem = SubItem::where([
            ['vo_id', '=', $voType->vo->id],
            ['vo_type_id', '=', $voType->id],
            ['sst_id', '=', $voType->vo->sst->id],
            ['vo_item_id', '=', substr($request->pid,1)],
        ])->orderBy('no', 'desc')->first();

        if($lastSubItem == null){
            $newNo = 1;
        }else{
            $newNo = $lastSubItem->no + 1;
        }       

        $subItem = SubItem::updateOrCreate([
            'id'    => substr($request->id,1)
        ],
        [
            'no'            =>  $newNo,
            'item'          =>  str_replace("'","`",$request->name),  //str_replace("'", '`', $request->description),
            'description'   =>  str_replace("'", "`",$request->description),
            'vo_id'         =>  $voType->vo->id,
            'vo_type_id'    =>  $voType->id,
            'sst_id'        =>  $voType->vo->sst->id,
            'vo_item_id'    =>  substr($request->pid,1),
            'unit'          =>  !empty($request->unit) ? $request->unit : "",
            'rate_per_unit'     =>  !empty($request->rate) ? money()->toMachine($request->rate) : "0",
            'quantity_omission' =>  !empty($request->qtyOM) ? money()->toMachine($request->qtyOM) : "0" ,
            'amount_omission'   =>  !empty($request->ttlOM) ? money()->toMachine($request->ttlOM) : "0",
            'quantity_addition' =>  !empty($request->qtyAD) ? money()->toMachine($request->qtyAD) : "0" ,
            'baki_quantity'     =>  !empty($request->qtyAD) ? money()->toMachine($request->qtyAD) : "0" ,
            'amount_addition'   =>  !empty($request->ttlAD) ? money()->toMachine($request->ttlAD) : "0",
            'percent_addition'  =>  !empty($request->net) ? (money()->toMachine($request->net) * 100 / $appointed->offered_price) : "0",
            'net_amount'    =>  !empty($request->net) ? money()->toMachine($request->net) : "0",
            'bq_reference'  =>  $request->ref,
            'status_lock'   =>  $request->typeBQ,
            'vo_active_id'  =>  $item->vo_active_id,
        ]);
    

        // 1. check if type == wps(3),
        // 2. if type == 3, calculate balanced from wps (balanced = totalAmount - totalUsed) 
        // 3. if balanced -ve, wps_status = null, if balanced +ve, wps_status = 1
        // 4. add new column named 'balanced_wps' in table vo_subItem
        // 5. if balanced is not 0, bring it to ppjhk

        $statusWps = 0;
        $totalAmount = 0;
        $totalUsed = 0;
        $totalPk = 0;
        $balanced = 0; 
        $baki = 0;       
        $percentage = 0;
        $maximumPk = 0;
        $defaultMaximumPk = 0;

        if($voType->variation_order_type->id == 3){                        
            $subItem->update(['wps_status'    =>  0]);
        }

        if($voType->variation_order_type->id == 2){

            /*
                Jika tender = 20%, sebut harga = 10%
                Jika 20% dari harga tender  > 1 juta, ambil yang paling minimum
                Jika 10% dari harga sebut harga  > 100k, ambil yang paling minimum ==> harga maksimum untuk pk
                Jika penggunaan pk melebihi harga maksimum pk, keluarkan alert message "penggunaan melebihi had"
            */

            if($sst->acquisition->category->name == 'Tender Terbuka' || $sst->acquisition->category->name == 'Tender Terhad'){
                $percentage = 0.1;     
                $defaultMaximumPk = 100000000;           
            }else if($sst->acquisition->category->name == 'Sebut Harga' || $sst->acquisition->category->name == 'Sebut Harga B'){
                $percentage = 0.2;
                $defaultMaximumPk = 10000000;
            }

            $maximumPk = $appointed->offered_price * $percentage;
            if($maximumPk > $defaultMaximumPk){
                $maximumPk = $defaultMaximumPk;
            }

            $ppkList = VO::where('sst_id', $sst->id)->where('deleted_at', null)->get();          
            foreach($ppkList as $ppkL){                
                if($ppkL->vo_elements->count() > 0){
                    foreach($ppkL->vo_elements as $ppkVoE){                        
                        if(($ppkVoE->vo_type->variation_order_type->id == $voType->variation_order_type->id)&&($ppkVoE->status == null || $ppkVoE->status == '1')){ 
                            $totalPk = 0;  
                            
                            if($ppkVoE->status == '1' && $ppkVoE->ppjhk_id != null){
                                $totalPk = \App\Models\VO\PPJHK\Element::where('ppjhk_id', $ppkVoE->ppjhk_id)->where('vo_element_id', $ppkVoE->id)->pluck('net_amount')->sum();
                            }else{
                                foreach($ppkVoE->items as $ppkVoI){
                                    $totalPk = $totalPk + $ppkVoI->subItems()
                                                                            //->where('vo_type_id', $voType->id)
                                                                            //->where('id', '!=', $subItem->id)
                                                                            ->pluck('net_amount')->sum();
                                }
                            }
                            
                            if($totalPk > 0){
                                $totalUsed = $totalUsed + $totalPk;
                            }
                        }
                    }                    
                }                
            }

            $balanced = $maximumPk - $totalUsed;           
        }

        $item_omission = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('amount_omission')->sum();
        $item_addition = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('amount_addition')->sum();
        $item_net = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('net_amount')->sum();
        $item_wps = SubItem::where('vo_item_id', '=', substr($request->pid,1))->where('wps_status', '1')->count();

        Item::find($subItem->vo_item_id)->update(
        [
            'amount_omission'   =>  $item_omission,
            'amount_addition'   =>  $item_addition,
            'net_amount'        =>  $item_net,
            'percent_addition'  =>  $item_net * 100 / $appointed->offered_price,
            'wps_status'        => ($item_wps > 0)? 1:0,
        ]);
        $item = Item::find($subItem->vo_item_id);
        $element_omission = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_omission')->sum();
        $element_addition = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_addition')->sum();
        $element_net = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('net_amount')->sum();
        $element_wps = Item::where('vo_element_id', '=', $item->vo_element_id)->where('wps_status', '1')->count();

        Element::find($item->vo_element_id)->update(
        [
            'amount_omission'   =>  $element_omission,
            'amount_addition'   =>  $element_addition,
            'net_amount'        =>  $element_net,
            'percent_addition'  =>  $element_net * 100 / $appointed->offered_price,
            'wps_status'        => ($element_wps > 0)? 1:0,
        ]);

        if($baki > 0){
            $statusWps = 1;
        }

        return response()->json(['id'=>$subItem->id, 'used' => money()->toHuman($totalUsed,2), 'baki' => money()->toHuman($balanced,2), 'statusWps' => $statusWps]);
    }

    public function deleteVoSubItem($id)
    {
        $subItem = SubItem::find(substr($id,1));
        $items = $subItem->vo_item;

        if(!empty($subItem)){
            $subItem->delete();

            $item_omission = SubItem::where('vo_item_id', '=', $items->id)->pluck('amount_omission')->sum();
            $item_addition = SubItem::where('vo_item_id', '=', $items->id)->pluck('amount_addition')->sum();
            $item_net = SubItem::where('vo_item_id', '=', $items->id)->pluck('net_amount')->sum();
            $item_wps = SubItem::where('vo_item_id', '=', $items->id)->where('wps_status', '1')->count();
    
            $item = Item::updateOrCreate([
                'id'    =>  $items->id
            ],
            [
                'amount_omission'   =>  $item_omission,
                'amount_addition'   =>  $item_addition,
                'net_amount'        =>  $item_net,
                'wps_status'        => ($item_wps > 0)? 1:0,
            ]);
    
            $element_omission = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_omission')->sum();
            $element_addition = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_addition')->sum();
            $element_net = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('net_amount')->sum();
            $element_wps = Item::where('vo_element_id', '=', $item->vo_element_id)->where('wps_status', '1')->count();
    
            $element = Element::updateOrCreate([
                'id'    =>  $item->vo_element_id
            ],
            [
                'amount_omission'   =>  $element_omission,
                'amount_addition'   =>  $element_addition,
                'net_amount'        =>  $element_net,
                'wps_status'        => ($element_wps > 0)? 1:0,
            ]);
        }
        return response()->api([], __('Rekod telah berjaya dipadam.'));
    }

    public function deleteVoItem($id)
    {
        $item = Item::find(substr($id,1));
        if(!empty($item)){
            $item->delete();
        }
        return response()->api([], __('Rekod telah berjaya dipadam.'));
    }

    public function deleteVoElement($id)
    {
        $element = Element::find(substr($id,1));
        if(!empty($element)){
            $element->delete();
        }
        return response()->api([], __('Rekod telah berjaya dipadam.'));
    }

    public function createVoPkSubItem(Request $request, $id)
    {        
        $voType = Type::findByHashSlug($id);
        $sst = $voType->vo->sst;
        $item = Item::find(substr($request->pid,1));
        $appointed = AppointedCompany::where([
                ['acquisition_id', $sst->acquisition_id],
                ['company_id', $sst->company_id],
            ])->first();

        $lastSubItem = SubItem::where([
            ['vo_id', '=', $voType->vo->id],
            ['vo_type_id', '=', $voType->id],
            ['sst_id', '=', $voType->vo->sst->id],
            ['vo_item_id', '=', substr($request->pid,1)],
        ])->orderBy('no', 'desc')->first();

        if($lastSubItem == null){
            $newNo = 1;
        }else{
            $newNo = $lastSubItem->no + 1;
        }       

        $subItem = SubItem::updateOrCreate([
            'id'    => substr($request->id,1)
        ],
        [
            'no'            =>  $newNo,
            'item'          =>  str_replace("'","`",$request->name),  //str_replace("'", '`', $request->description),
            'description'   =>  str_replace("'", "`",$request->description),
            'vo_id'         =>  $voType->vo->id,
            'vo_type_id'    =>  $voType->id,
            'sst_id'        =>  $voType->vo->sst->id,
            'vo_item_id'    =>  substr($request->pid,1),
            'unit'          =>  !empty($request->unit) ? $request->unit : "",
            'bq_reference'  =>  $request->ref,
            'status_lock'   =>  $request->typeBQ,
            'vo_active_id'  =>  $item->vo_active_id,
        ]);
    

        /*
                Jika tender = 10%, sebut harga = 20%
                Jika 10% dari harga tender  > 1 juta, ambil yang paling minimum
                Jika 20% dari harga sebut harga  > 100k, ambil yang paling minimum ==> harga maksimum untuk pk
                Jika penggunaan pk melebihi harga maksimum pk, keluarkan alert message "penggunaan melebihi had"
        */

        
        $statusPk = 0;
        $totalAmount = 0;
        $totalUsed = 0;
        $totalPk = 0;
        $balanced = 0;
        $percentage = 0;
        $maximumPk = 0;
        $defaultMaximumPk = 0;        

        if($voType->variation_order_type->id == 2){

            if($sst->acquisition->category->name == 'Tender Terbuka' || $sst->acquisition->category->name == 'Tender Terhad'){
                $percentage = 0.1;     
                $defaultMaximumPk = 100000000;           
            }else if($sst->acquisition->category->name == 'Sebut Harga' || $sst->acquisition->category->name == 'Sebut Harga B'){
                $percentage = 0.2;
                $defaultMaximumPk = 10000000;
            }

            $maximumPk = $appointed->offered_price * $percentage;
            if($maximumPk > $defaultMaximumPk){
                $totalAmount = $defaultMaximumPk;
            }else{
                $totalAmount = $maximumPk;
            }

            
            $ppkList = VO::where('sst_id', $sst->id)->where('deleted_at', null)->get();    
                  
            foreach($ppkList as $ppkL){                
                if($ppkL->vo_elements->count() > 0){
                    foreach($ppkL->vo_elements as $ppkVoE){     
                        $totalPk = 0;                   
                        if(($ppkVoE->vo_type->variation_order_type->id == $voType->variation_order_type->id)&&($ppkVoE->status == null || $ppkVoE->status == '1')){ 
                            foreach($ppkVoE->items as $ppkVoI){
                                $totalPk = $totalPk + $ppkVoI->subItems()
                                                                        //->where('vo_type_id', $voType->id)
                                                                        //->where('id', '!=', $subItem->id)
                                                                        ->pluck('net_amount')->sum();
                            }
                        }
                        

                        if($totalPk > 0){
                            $totalUsed = $totalUsed + $totalPk;
                        }
                    }                    
                }                
            }
            $balanced = $totalAmount - $totalUsed;            
            
            if($request->net != null){                
                if(money()->toMachine($request->net) > $balanced){
                    $statusPk = 0;

                    $totalUsed = $totalUsed + $subItem->net_amount;
                    $balanced = $totalAmount - $totalUsed;

                }else{
                    $statusPk = 1;
                    
                    //baru tambah 30 May 2019
                    $totalUsed = $totalUsed + money()->toMachine($request->net);
                    $balanced = $totalAmount - $totalUsed;
                }

                // 14 August 2019 - Pn Rohani request bagi lepas jer walaupun dah lebih had
                $subItem->update([
                    'rate_per_unit'     =>  !empty($request->rate) ? money()->toMachine($request->rate) : "0",
                    'quantity_omission' =>  !empty($request->qtyOM) ? money()->toMachine($request->qtyOM) : "0" ,
                    'amount_omission'   =>  !empty($request->ttlOM) ? money()->toMachine($request->ttlOM) : "0",
                    'quantity_addition' =>  !empty($request->qtyAD) ? money()->toMachine($request->qtyAD) : "0" ,
                    'baki_quantity'     =>  !empty($request->qtyAD) ? money()->toMachine($request->qtyAD) : "0" ,
                    'amount_addition'   =>  !empty($request->ttlAD) ? money()->toMachine($request->ttlAD) : "0",
                    'percent_addition'  =>  !empty($request->net) ? (money()->toMachine($request->net) * 100 / $appointed->offered_price) : "0",
                    'net_amount'    =>  !empty($request->net) ? money()->toMachine($request->net) : "0",
                ]);                                   
            }            
        }

        $item_omission = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('amount_omission')->sum();
        $item_addition = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('amount_addition')->sum();
        $item_net = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('net_amount')->sum();
        $item_wps = SubItem::where('vo_item_id', '=', substr($request->pid,1))->where('wps_status', '1')->count();

        Item::find($subItem->vo_item_id)->update(
        [
            'amount_omission'   =>  $item_omission,
            'amount_addition'   =>  $item_addition,
            'net_amount'        =>  $item_net,
            'percent_addition'  =>  $item_net * 100 / $appointed->offered_price,
            'wps_status'        => ($item_wps > 0)? 1:0,
        ]);
        $item = Item::find($subItem->vo_item_id);
        $element_omission = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_omission')->sum();
        $element_addition = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_addition')->sum();
        $element_net = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('net_amount')->sum();
        $element_wps = Item::where('vo_element_id', '=', $item->vo_element_id)->where('wps_status', '1')->count();

        Element::find($item->vo_element_id)->update(
        [
            'amount_omission'   =>  $element_omission,
            'amount_addition'   =>  $element_addition,
            'net_amount'        =>  $element_net,
            'percent_addition'  =>  $element_net * 100 / $appointed->offered_price,
            'wps_status'        => ($element_wps > 0)? 1:0,
        ]);

        return response()->json(['id'=>$subItem->id, 'used' => money()->toCommon($totalUsed,2), 'baki' => money()->toCommon($balanced,2), 'statusPk' => $statusPk]);
    }

    public function createVoWpsSubItem(Request $request, $id)
    {        
        $voType = Type::findByHashSlug($id);
        $sst = $voType->vo->sst;
        $item = Item::find(substr($request->pid,1));
        $appointed = AppointedCompany::where([
                ['acquisition_id', $sst->acquisition_id],
                ['company_id', $sst->company_id],
            ])->first();

        $lastSubItem = SubItem::where([
            ['vo_id', '=', $voType->vo->id],
            ['vo_type_id', '=', $voType->id],
            ['sst_id', '=', $voType->vo->sst->id],
            ['vo_item_id', '=', substr($request->pid,1)],
        ])->orderBy('no', 'desc')->first();

        if($lastSubItem == null){
            $newNo = 1;
        }else{
            $newNo = $lastSubItem->no + 1;
        }       

        $subItem = SubItem::updateOrCreate([
            'id'    => substr($request->id,1)
        ],
        [
            'no'            =>  $newNo,
            'item'          =>  str_replace("'","`",$request->name),  //str_replace("'", '`', $request->description),
            'description'   =>  str_replace("'", "`",$request->description),
            'vo_id'         =>  $voType->vo->id,
            'vo_type_id'    =>  $voType->id,
            'sst_id'        =>  $voType->vo->sst->id,
            'vo_item_id'    =>  substr($request->pid,1),
            'unit'          =>  !empty($request->unit) ? $request->unit : "",
            'bq_reference'  =>  $request->ref,
            'status_lock'   =>  $request->typeBQ,
            'vo_active_id'  =>  $item->vo_active_id,
        ]);
    

        // 1. check if type == wps(3),
        // 2. if type == 3, calculate balanced from wps (balanced = totalAmount - totalUsed) 
        // 3. if balanced -ve, wps_status = null, if balanced +ve, wps_status = 1
        // 4. add new column named 'balanced_wps' in table vo_subItem
        // 5. if balanced is not 0, bring it to ppjhk

        
        $statusWps = 0;
        $totalAmount = 0;
        $totalUsed = 0;
        $balanced = 0;

        if($voType->variation_order_type->id == 3){

            //$totalAmount = Active::find($subItem->vo_item->vo_element->vo_active_id)->item->amount;
            $totalAmount = Active::find($subItem->vo_item->vo_element->vo_active_id)->amount;
            
            $ppkList = VO::where('sst_id', $sst->id)->get();            
            foreach($ppkList as $ppkL){                
                if($ppkL->vo_elements->count() > 0){
                    foreach($ppkL->vo_elements as $ppkVoE){                        
                        if($ppkVoE->status == null || $ppkVoE->status == '1'){    
                            foreach($ppkVoE->items as $ppkVoI){
                                $totalUsed = $totalUsed + $ppkVoI->subItems()
                                                                        ->where('wps_status', 1)
                                                                        ->where('vo_active_id', $subItem->vo_active_id)
                                                                        ->where('id', '!=', $subItem->id)
                                                                        ->pluck('net_amount')->sum();
                            }
                        }
                    }                    
                }                
            }
            $balanced = $totalAmount - $totalUsed;            
            
            if($request->net != null){                
                if(money()->toMachine($request->net) > $balanced){
                    $statusWps = 0;

                    $totalUsed = $totalUsed + $subItem->net_amount;
                    $balanced = $totalAmount - $totalUsed;

                }else{
                    $statusWps = 1;
                    
                    //baru tambah 25 Feb 2019
                    $totalUsed = $totalUsed + money()->toMachine($request->net);
                    $balanced = $totalAmount - $totalUsed;

                    $subItem->update([
                        'wps_status'    =>  $statusWps,
                        'rate_per_unit'     =>  !empty($request->rate) ? money()->toMachine($request->rate) : "0",
                        'quantity_omission' =>  !empty($request->qtyOM) ? money()->toMachine($request->qtyOM) : "0" ,
                        'amount_omission'   =>  !empty($request->ttlOM) ? money()->toMachine($request->ttlOM) : "0",
                        'quantity_addition' =>  !empty($request->qtyAD) ? money()->toMachine($request->qtyAD) : "0" ,
                        'baki_quantity'     =>  !empty($request->qtyAD) ? money()->toMachine($request->qtyAD) : "0" ,
                        'amount_addition'   =>  !empty($request->ttlAD) ? money()->toMachine($request->ttlAD) : "0",
                        'percent_addition'  =>  !empty($request->net) ? (money()->toMachine($request->net) * 100 / $appointed->offered_price) : "0",
                        'net_amount'    =>  !empty($request->net) ? money()->toMachine($request->net) : "0",
                    ]);
                } 
                  
            }            
        }

        $item_omission = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('amount_omission')->sum();
        $item_addition = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('amount_addition')->sum();
        $item_net = SubItem::where('vo_item_id', '=', substr($request->pid,1))->pluck('net_amount')->sum();
        $item_wps = SubItem::where('vo_item_id', '=', substr($request->pid,1))->where('wps_status', '1')->count();

        Item::find($subItem->vo_item_id)->update(
        [
            'amount_omission'   =>  $item_omission,
            'amount_addition'   =>  $item_addition,
            'net_amount'        =>  $item_net,
            'percent_addition'  =>  $item_net * 100 / $appointed->offered_price,
            'wps_status'        => ($item_wps > 0)? 1:0,
        ]);
        $item = Item::find($subItem->vo_item_id);
        $element_omission = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_omission')->sum();
        $element_addition = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('amount_addition')->sum();
        $element_net = Item::where('vo_element_id', '=', $item->vo_element_id)->pluck('net_amount')->sum();
        $element_wps = Item::where('vo_element_id', '=', $item->vo_element_id)->where('wps_status', '1')->count();

        Element::find($item->vo_element_id)->update(
        [
            'amount_omission'   =>  $element_omission,
            'amount_addition'   =>  $element_addition,
            'net_amount'        =>  $element_net,
            'percent_addition'  =>  $element_net * 100 / $appointed->offered_price,
            'wps_status'        => ($element_wps > 0)? 1:0,
            'status'            => 1,
        ]);

        return response()->json(['id'=>$subItem->id, 'used' => money()->toCommon($totalUsed,2), 'baki' => money()->toCommon($balanced,2), 'statusWps' => $statusWps]);
    }


    public function submitWps(Request $request, $id)
    {
        VO::findByHashSlug($id)->update(
        [
            'status'    => 2,
        ]);

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

}
