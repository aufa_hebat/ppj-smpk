<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Briefing;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;
use App\Models\Company\Company;

class SiteVisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!empty($id)){
            $visit = Briefing::findByHashSlug($id);
            if(!empty($visit) && $visit->count() > 0){
                $visit->site_visit     = $request->site_visit;
                if($request->site_visit == 1){
                    $visit->visit_staff_id =  user()->id;
                    
                }else{
                    $visit->visit_staff_id = null;
                }
                $visit->update();
            
                $workflow = WorkflowTask::where('acquisition_id',$visit->acquisition->id)->first();
                $workflow->flow_desc = 'Jualan : Sila pastikan jualan perolehan wujud dan isi nombor resit.';
                $workflow->flow_location = 'Jualan';
                $workflow->is_claimed = 1;
                $workflow->user_id = null;
                $workflow->url = '/sale';
                $workflow->update();

                $visit->acquisition->status_task = config('enums.flow6');
                $visit->acquisition->update();
            }
            $company          = Company::withDetails()->find($visit->company_id);

            audit($visit, __('Kehadiran Lawatan Tapak oleh ' .$company->company_name. ' bagi perolehan ' .$visit->acquisition->reference. ' : ' .$visit->acquisition->title. ' telah berjaya ditambah.'));
        }
        

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    public function upload(Request $request, $id){
        $acq = Acquisition::findByHashSlug($id);
        
        if(!empty($acq)){
            if(!empty($acq) && $acq->count() > 0){
                if($request->hasFile('document')){
                    $inv_requirements = [
                        'request_hDocumentId' => null,
                        'request_hasFile'     => $request->hasFile('document'),
                        'request_file'        => $request->file('document'),
                        'acquisition'         => $acq,
                        'doc_type'            => 'ACQ_VISIT',
                        'doc_id'              => $acq->id,
                    ];
                    common()->upload_document($inv_requirements);
                }
            }
        }

        audit($acq, __('Lampiran Lawatan Tapak bagi perolehan ' .$acq->reference. ' : ' .$acq->title. ' telah berjaya dimuatnaik.'));
        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Acquisition::hashslug($id)->delete();
        return response()->api([], __('Rekod telah berjaya dipadamkan.'));
    }
    
        public function delete($id)
    {
        Document::hashslug($id)->delete();
        return response()->api([], __('Lampiran telah berjaya dibuang.'));
    }
}
