<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\BillOfQuantity\Item;
use App\Models\VO\Active;
use App\Models\VO\Cancel;
use App\Models\VO\Element;
use App\Models\VO\VO;
use Illuminate\Http\Request;

class VariationOrderActiveWPSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $count = Active::where('vo_id', $request->vo_id)->count();

        if ($count > 0) {
            Active::where('vo_id', $request->vo_id)->delete();
        }

        if(!empty($request->select_active_wps)){
            foreach ($request->select_active_wps as $item_id) {
                $item = Item::find($item_id);
                $active = Active::create([
                    'user_id'       => user()->id,
                    'department_id' => user()->department->id,
                    'sst_id'        => $request->sst_id,
                    'bq_element_id' => $request->bq_element_id,
                    'bq_item_id'    => $item_id,
                    'vo_id'         => $request->vo_id,
                    'amount'        => $item->amount_adjust,
                ]);
            }
        }


        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function result(Request $request, $id)
    {

        // Aktifkan Wang Peruntukkan Sementara
        $count = Active::where('vo_id', $request->vo_id)->count();
        if ($count > 0) {
            Active::where('vo_id', $request->vo_id)->update(['status' => '0']);
        }

        if(!empty($request->result)){
            foreach ($request->result as $item_id) {
                $active = Active::updateOrCreate([
                    'id'    => $item_id,
                ],[
                    'status'       => '1',
                ]);

            }
        }

        // Perubahan Kerja
        $count = Cancel::where('vo_id', $request->vo_id)->count();
        if ($count > 0) {
            Cancel::where('vo_id', $request->vo_id)->update(['status' => '0']);
        }

        if(!empty($request->batal)){
            foreach ($request->batal as $item_id) {
                $cancel = Cancel::updateOrCreate([
                    'id'    => $item_id,
                ],[
                    'status'       => '1',
                ]);
            }
        }

        // Wang Peruntukkan Sementara dan Kos Prima
        $count = Element::where('vo_id', $request->vo_id)->count();
        if ($count > 0) {
            Element::where('vo_id', $request->vo_id)->update(['status' => '0']);
        }

        if(!empty($request->elemen)){
            foreach ($request->elemen as $element_id) {
                $elemen = Element::updateOrCreate([
                    'id'    => $element_id,
                ],[
                    'status'       => '1',
                ]);
            }
        }

        // Pengiraan Semula Kuantiti Sementara (Provisional Quantity)
        if(!empty($request->elemenUlasan)){
            foreach($request->elemenUlasan as $key => $ul){
                $element = Element::updateOrCreate([
                    'id'    => $key,
                ],[
                    'comment'       => str_replace("'", '`', $ul),
                ]);
            }
        }

        // Kesilapan Keterangan Kerja dan Kuantiti
        if(!empty($request->ulasan)){
            foreach($request->ulasan as $key => $ul){
                $active = Active::updateOrCreate([
                    'id'    => $key,
                ],[
                    'comment'       => str_replace("'", '`', $ul),
                ]);
            }
        }

        // Batalkan Wang Peruntukkan Sementara
        if(!empty($request->ulasanBatal)){
            foreach($request->ulasanBatal as $key => $ul){
                $cancel = Cancel::updateOrCreate([
                    'id'    => $key,
                ],[
                    'comment'       => str_replace("'", '`', $ul),
                ]);
            }
        }

        $vo = VO::find($request->vo_id);
        audit($vo, __('Keputusan mesyuarat bagi ' .$vo->sst->acquisition->reference. ' : ' .$vo->sst->acquisition->title. ' telah berjaya dikemaskini.'));

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
