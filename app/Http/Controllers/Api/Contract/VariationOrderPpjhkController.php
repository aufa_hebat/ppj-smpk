<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\VO\PPJHK\Ppjhk;
use App\Models\VO\Approver;
use App\Models\VO\VO;
use App\Models\VO\Type;
use App\Models\VO\Element;
use App\Models\VO\Item;
use App\Models\VO\SubItem;
use App\Models\VO\Cancel;
use Illuminate\Http\Request;
use App\Models\WorkflowTask;

class VariationOrderPpjhkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sst      = \App\Models\Acquisition\Sst::findByHashSlug($request->id);

        $ppjhk_count = Ppjhk::where('sst_id', $sst->id)->count();
        $ppjhk       = Ppjhk::create([
            'sst_id'        => $sst->id,
            'user_id'       => user()->id,
            'department_id' => user()->department->id,
            'no'            => $ppjhk_count + 1,
        ]);

        if (! empty($request->element)) {
            $this->createPpjhkElemen($request, $ppjhk);
        }

        if (! empty($request->cancel)) {
            $this->createPpjhkCancel($request, $ppjhk);
        }

        // if($ppjhk->no == 1){
        //     $workflow = WorkflowTask::where('acquisition_id', $ppjhk->sst->acquisition_id)->first();
        //     $workflow->ppjhk_id = $ppjhk->id;
        //     $workflow->flow_desc = 'Perubahan Kerja PPJHK '.$ppjhk->no.' : Sila tekan butang hantar untuk proses penyemakan.';
        //     $workflow->user_id = user()->id;
        //     $workflow->flow_location = 'PPJHK';
        //     $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit';
        //     $workflow->update();

        //     $ppjhk->sst->acquisition->status_task = config('enums.flowALT2');
        //     $ppjhk->sst->acquisition->update();
        // } else {
            $workflow = new WorkflowTask();
            $workflow->acquisition_id = $ppjhk->sst->acquisition_id;
            $workflow->ppjhk_id = $ppjhk->id;
            $workflow->flow_name = $ppjhk->sst->acquisition->reference . ' : ' . $ppjhk->sst->acquisition->title;
            $workflow->flow_desc = 'Perubahan Kerja PPJHK '.$ppjhk->no.' : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->flow_location = 'PPJHK';
            $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit#detail';
            $workflow->user_id = user()->id;
            $workflow->role_id = 4;
            $workflow->is_claimed = 1;
            $workflow->workflow_team_id = 1;

            $workflow2 = new WorkflowTask();
            $workflow2->acquisition_id = $ppjhk->sst->acquisition_id;
            $workflow2->ppjhk_id = $ppjhk->id;
            $workflow2->flow_name = $ppjhk->sst->acquisition->reference . ' : ' . $ppjhk->sst->acquisition->title;
            $workflow2->flow_desc = 'Perubahan Kerja PPJHK '.$ppjhk->no.' : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow2->flow_location = 'PPJHK';
            $workflow2->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit#detail';
            $workflow2->role_id = 4;
            $workflow2->is_claimed = 1;
            $workflow2->workflow_team_id = 2;

            $workflow->save();
            $workflow2->save();

            $ppjhk->sst->acquisition->status_task = config('enums.flowALT2');
            $ppjhk->sst->acquisition->update();
        // }

        return response()->json(['hashslug' => $ppjhk->hashslug, 'message' => __('Rekod telah berjaya ditambah.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ppjhk = Ppjhk::withDetails()->findByHashSlug($id);
        
        if (! empty($request->element)) {            
            if ($ppjhk->elements()->count() > 0) { 
                foreach($ppjhk->elements as $ppjhkElement){                                  
                    $isFound = false;
                                           
                    foreach ($request->element as $key => $value) {
                        if($ppjhkElement->vo_element_id == $value){
                            $isFound = true;
                        }
                    }
                        
                    if(!$isFound){                               
                        \App\Models\VO\PPJHK\SubItem::where('vo_element_id', '=', $ppjhkElement->vo_element_id)->delete();
                        \App\Models\VO\PPJHK\Item::where('vo_element_id', '=', $ppjhkElement->vo_element_id)->delete();
                        \App\Models\VO\PPJHK\Element::where('vo_element_id', '=', $ppjhkElement->vo_element_id)->delete();
    
                        Element::updateOrCreate([
                            'id'    => $ppjhkElement->vo_element_id
                        ],[
                            'ppjhk_status'  => null,
                            'ppjhk_id'      => null
                        ]);
                    }
                    
                }
            }

            $this->createPpjhkElemen($request, $ppjhk);
        }

        if (! empty($request->cancel)) {
            if ($ppjhk->cancels()->count() > 0) { 
                foreach($ppjhk->cancels as $ppjhkCancel){                                  
                    $isFound = false;
                                           
                    foreach ($request->cancel as $key => $value) {
                        if($ppjhkCancel->id == $value){
                            $isFound = true;
                        }
                    }
                        
                    if(!$isFound){                               
                        Cancel::updateOrCreate([
                            'id'    => $ppjhkCancel->id
                        ],[
                            'ppjhk_status'  => null,
                            'ppjhk_id'      => null
                        ]);
                    }
                    
                }
            }

            $this->createPpjhkCancel($request, $ppjhk);
        }

        // if($ppjhk->no == 1){
            $workflow = WorkflowTask::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id',$ppjhk->id)->where('flow_location','PPJHK')->first();
            $workflow->ppjhk_id = $ppjhk->id;
            $workflow->flow_desc = 'Perubahan Kerja PPJHK '.$ppjhk->no.' : Sila tekan butang hantar untuk proses penyemakan.';
            $workflow->user_id = user()->id;
            $workflow->role_id = 4;
            $workflow->flow_location = 'PPJHK';
            $workflow->is_claimed = 1;
            $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit#detail';
            $workflow->update();
        // } else {
        //     $workflow = new WorkflowTask();
        //     $workflow->acquisition_id = $ppjhk->sst->acquisition_id;
        //     $workflow->ppjhk_id = $ppjhk->id;
        //     $workflow->flow_name = $ppjhk->sst->acquisition->reference . ' : ' . $ppjhk->sst->acquisition->title;
        //     $workflow->flow_desc = 'Perubahan Kerja PPJHK '.$ppjhk->no.' : Sila tekan butang hantar untuk proses penyemakan.';
        //     $workflow->flow_location = 'PPJHK';
        //     $workflow->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit';
        //     $workflow->user_id = user()->id;
        //     $workflow->is_claimed = 1;
        //     $workflow->workflow_team_id = 1;

        //     $workflow2 = new WorkflowTask();
        //     $workflow2->acquisition_id = $ppjhk->sst->acquisition_id;
        //     $workflow2->ppjhk_id = $ppjhk->id;
        //     $workflow2->flow_name = $ppjhk->sst->acquisition->reference . ' : ' . $ppjhk->sst->acquisition->title;
        //     $workflow2->flow_desc = 'Perubahan Kerja PPJHK '.$ppjhk->no.' : Sila tekan butang hantar untuk proses penyemakan.';
        //     $workflow2->flow_location = 'PPJHK';
        //     $workflow2->url = '/post/variation-order-ppjhk/'.$ppjhk->hashslug.'/edit';
        //     $workflow2->is_claimed = 1;
        //     $workflow2->workflow_team_id = 2;

        //     $workflow->save();
        //     $workflow2->save();
        // }

        return response()->json(['hashslug' => $ppjhk->hashslug, 'message' => __('Rekod telah berjaya dikemaskini.')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ppjhk = Ppjhk::withDetails()->findByHashSlug($id);

        if ($ppjhk->elements()->count() > 0) { 
            foreach($ppjhk->elements as $ppjhkElement){                                  
                $isFound = false;
                                       
                foreach ($request->element as $key => $value) {
                    if($ppjhkElement->vo_element_id == $value){
                        $isFound = true;
                    }
                }
                    
                if(!$isFound){                               
                    \App\Models\VO\PPJHK\SubItem::where('vo_element_id', '=', $ppjhkElement->vo_element_id)->delete();
                    \App\Models\VO\PPJHK\Item::where('vo_element_id', '=', $ppjhkElement->vo_element_id)->delete();
                    \App\Models\VO\PPJHK\Element::where('vo_element_id', '=', $ppjhkElement->vo_element_id)->delete();

                    Element::updateOrCreate([
                        'id'    => $ppjhkElement->vo_element_id
                    ],[
                        'ppjhk_status'  => null,
                        'ppjhk_id'      => null
                    ]);
                }                
            }
        }

        if ($ppjhk->cancels()->count() > 0) { 
            foreach($ppjhk->cancels as $ppjhkCancel){                                  
                $isFound = false;
                                       
                foreach ($request->cancel as $key => $value) {
                    if($ppjhkCancel->id == $value){
                        $isFound = true;
                    }
                }
                    
                if(!$isFound){                               
                    Cancel::updateOrCreate([
                        'id'    => $ppjhkCancel->id
                    ],[
                        'ppjhk_status'  => null,
                        'ppjhk_id'      => null
                    ]);
                }
                
            }
        }

        Ppjhk::where('id', $ppjhk->id)->delete();

        return response()->api([], __('Rekod telah berjaya dihapuskan.'));
    }

    public function amend(Request $request, $id)
    { 
        
        if (! empty($request->ppjhk)) {
            foreach($request->ppjhk as $key => $hk){

                \App\Models\VO\PPJHK\SubItem::updateOrCreate([
                    'id'    =>  $key
                ],[
                    'rate_per_unit'     => money()->toMachine(preg_replace('/[,]/', '', $hk["'kadar'"]) ?? 0),
                    'quantity_ppjhk'    => money()->toMachine(preg_replace('/[,]/', '', $hk["'kuantiti'"]) ?? 0),
                    'baki_quantity'     => money()->toMachine(preg_replace('/[,]/', '', $hk["'totalQty'"]) ?? 0),
                    'paid_quantity'     => 0,
                    'amount_ppjhk'      => money()->toMachine(preg_replace('/[,]/', '', $hk["'jumlah'"]) ?? 0),
                    'net_amount'        => money()->toMachine(preg_replace('/[,]/', '', $hk["'nett'"]) ?? 0),
                    'wps_status'        => '1', //!empty($hk["'wps'"]) ? $hk["'wps'"] : null  //!empty($request->net) ? money()->toMachine($request->net) : "0",
                    'total_quantity'    => money()->toMachine(preg_replace('/[,]/', '', $hk["'totalQty'"]) ?? 0),
                    'status_lock'       => $hk["'status_lock'"]
                ]);
            }
        }

        $element = \App\Models\VO\PPJHK\Element::findByHashSlug($id);
        foreach($element->ppjhkItems as $item){
            $item_addition = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $item->id)->where('wps_status', 1)->pluck('amount_ppjhk')->sum();
            $item_net = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $item->id)->where('wps_status', 1)->pluck('net_amount')->sum();
            
            \App\Models\VO\PPJHK\Item::updateOrCreate([
                'id'    =>  $item->id
            ],[
                'amount_ppjhk'     =>  $item_addition,
                'net_amount'        => $item_net,
            ]);
        }

        $element_addition = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $element->id)->pluck('amount_ppjhk')->sum();
        $element_net = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $element->id)->pluck('net_amount')->sum();
        \App\Models\VO\PPJHK\Element::updateOrCreate([
            'id'    =>  $element->id
        ],[
            'amount_ppjhk'     =>  $element_addition,
            'net_amount'        => $element_net,
        ]);

        return response()->json(['hashslug' => $element->ppjhk->hashslug, 'message' => __('Rekod telah berjaya dikemaskini.')]);
    }


    private function createPpjhkElemen(Request $request, $ppjhk){

        
        foreach ($request->element as $key => $value) {
            $element = Element::find($value);

            $balanced = 0;
            foreach($element->items as $item){
                $balanced = $balanced + $item->subItems()->where('wps_status', '0')->orWhere('wps_status', null)->pluck('net_amount')->sum();
            }

            $ppjhkE = $ppjhk->elements()->where('vo_element_id', $element->id)->first();
            $ppjhkElement = $ppjhk->elements()->updateOrCreate([
                'vo_element_id' =>  $element->id
            ],[
                'vo_element_id' =>  $element->id,
                'ppjhk_id'      =>  $ppjhk->id,
                'sst_id'        =>  $ppjhk->sst->id,
                // 'amount_ppjhk'  =>  !empty($balanced) ? $balanced : "0",
                // 'net_amount'    =>  !empty($balanced) ? $balanced : "0",
                //'amount_ppjhk'  =>  !empty($ppjhkE->amount_ppjhk) ? $ppjhkE->amount_ppjhk : $balanced,
                //'net_amount'    =>  !empty($ppjhkE->net_amount) ? $ppjhkE->net_amount : $balanced
            ]);
            

            foreach($element->items as $keyI => $item){
                $ppjhkI = $ppjhk->items()
                    ->where('vo_element_id', $element->id)
                    ->where('vo_item_id', $item->id)->first();
                
                $ppjhkItem = $ppjhk->items()->updateOrCreate([
                    'vo_element_id'     =>  $element->id,
                    'vo_item_id'        =>  $item->id
                ],[
                    'vo_element_id'     =>  $element->id,
                    'vo_item_id'        =>  $item->id,
                    'ppjhk_id'          =>  $ppjhk->id,
                    'sst_id'            =>  $ppjhk->sst->id,
                    'ppjhk_element_id'  =>  $ppjhkElement->id,
                    // 'amount_ppjhk'      =>  $item->subItems()->where('wps_status', '0')->orWhere('wps_status', null)->pluck('net_amount')->sum(),
                    // 'net_amount'      =>  $item->subItems()->where('wps_status', '0')->orWhere('wps_status', null)->pluck('net_amount')->sum(),
                    //'amount_ppjhk'      =>  !empty($ppjhkI->amount_ppjhk) ? $ppjhkI->amount_ppjhk : $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum(),
                    //'net_amount'        =>  !empty($ppjhkI->net_amount) ? $ppjhkI->net_amount : $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum(),
                ]);


                foreach($item->subItems as $keyS => $subItem){
                    if($subItem->wps_status == 0 || $subItem->wps_status == null){
                        $ppjhkS = $ppjhk->subItems()
                        ->where('vo_element_id', $element->id)
                        ->where('vo_item_id', $item->id)
                        ->where('vo_sub_item_id', $subItem->id)
                        ->first();
                    
                        $ppjhk->subItems()->updateOrCreate([
                            'vo_element_id'     =>  $element->id,
                            'vo_item_id'        =>  $item->id,
                            'vo_sub_item_id'    =>  $subItem->id,
                        ],[
                            'vo_element_id'     =>  $element->id,
                            'vo_item_id'        =>  $item->id,
                            'vo_sub_item_id'    =>  $subItem->id,
                            'vo_active_id'      =>  $subItem->vo_active_id,
                            'ppjhk_id'          =>  $ppjhk->id,
                            'sst_id'            =>  $ppjhk->sst->id,
                            'ppjhk_element_id'  =>  $ppjhkElement->id,
                            'ppjhk_item_id'     =>  $ppjhkItem->id,
                            'rate_per_unit'     =>  !empty($ppjhkS->rate_per_unit) ? $ppjhkS->rate_per_unit : $subItem->rate_per_unit,
                            'quantity_ppjhk'    =>  !empty($ppjhkS->quantity_ppjhk) ? $ppjhkS->quantity_ppjhk : $subItem->quantity_addition,
                            'amount_ppjhk'      =>  !empty($ppjhkS->amount_ppjhk) ? $ppjhkS->amount_ppjhk : $subItem->amount_addition,
                            'net_amount'        =>  !empty($ppjhkS->net_amount) ? $ppjhkS->net_amount : $subItem->net_amount,
                            'wps_status'        =>  $subItem->wps_status,
                            'total_quantity'    =>  !empty($ppjhkS->total_quantity) ? $ppjhkS->total_quantity : ($subItem->quantity_addition - $subItem->quantity_omission),
                            'baki_quantity'     =>  !empty($ppjhkS->total_quantity) ? $ppjhkS->total_quantity : ($subItem->quantity_addition - $subItem->quantity_omission),
                            'paid_quantity'     => 0,
                            'status_lock'       =>  !empty($ppjhkS->status_lock) ? $ppjhkS->status_lock : $subItem->status_lock,
                        ]); 
                    }               
                }

                
                
                $item_addition = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $ppjhkItem->id)->pluck('amount_ppjhk')->sum();
                $item_net = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $ppjhkItem->id)->pluck('net_amount')->sum();


                //to do
                \App\Models\VO\PPJHK\Item::updateOrCreate([
                    'id'    =>  $ppjhkItem->id
                ],[
                    'amount_ppjhk'     =>  $item_addition,
                    'net_amount'        => $item_net,
                ]);
            }

            $element_addition = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $ppjhkElement->id)->pluck('amount_ppjhk')->sum();
            $element_net = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $ppjhkElement->id)->pluck('net_amount')->sum();
            \App\Models\VO\PPJHK\Element::updateOrCreate([
                'id'    =>  $ppjhkElement->id
            ],[
                'amount_ppjhk'     =>  $element_addition,
                'net_amount'        => $element_net,
            ]);

            Element::updateOrCreate([
                'id'    =>  $value
            ],[
                'ppjhk_status'  =>  '1',
                'ppjhk_id'      =>  $ppjhk->id
            ]);

            
            // foreach($element->items as $item){
                
            //     $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
            // }
        }

        // foreach ($request->element as $key => $value){
        //     $element = \App\Models\VO\PPJHK\Element::where('vo_element_id', $value)->first();
        //     foreach($element->ppjhkItems as $item){
        //         $item_addition = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $item->id)->where('wps_status', null)->pluck('amount_ppjhk')->sum();
        //         $item_net = $item_addition - $item->voItem->amount_omission;
        //         \App\Models\VO\PPJHK\Item::updateOrCreate([
        //             'id'    =>  $item->id
        //         ],[
        //             'amount_ppjhk'     =>  $item_addition,
        //             'net_amount'        => $item_net,
        //         ]);
        //     }
    
        //     $element_addition = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $element->id)->pluck('amount_ppjhk')->sum();
        //     $element_net = $element_addition - $element->voElement->amount_omission;
        //     \App\Models\VO\PPJHK\Element::updateOrCreate([
        //         'id'    =>  $element->id
        //     ],[
        //         'amount_ppjhk'     =>  $element_addition,
        //         'net_amount'        => $element_net,
        //     ]);

        //     Element::updateOrCreate([
        //         'id'    =>  $value
        //     ],[
        //         'ppjhk_status'  =>  '1',
        //         'ppjhk_id'      =>  $element->ppjhk->id
        //     ]);
        // }

        // foreach ($request->element as $key => $value){
        //     $element = \App\Models\VO\PPJHK\Element::where('vo_element_id', $value)->first();
        //     foreach($element->ppjhkItems as $item){
        //         $item_addition = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $item->id)->where('wps_status', null)->pluck('amount_ppjhk')->sum();
        //         $item_net = $item_addition - $item->voItem->amount_omission;
        //         \App\Models\VO\PPJHK\Item::updateOrCreate([
        //             'id'    =>  $item->id
        //         ],[
        //             'amount_ppjhk'     =>  $item_addition,
        //             'net_amount'        => $item_net,
        //         ]);
        //     }
    
        //     $element_addition = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $element->id)->pluck('amount_ppjhk')->sum();
        //     $element_net = $element_addition - $element->voElement->amount_omission;
        //     \App\Models\VO\PPJHK\Element::updateOrCreate([
        //         'id'    =>  $element->id
        //     ],[
        //         'amount_ppjhk'     =>  $element_addition,
        //         'net_amount'        => $element_net,
        //     ]);

        //     Element::updateOrCreate([
        //         'id'    =>  $value
        //     ],[
        //         'ppjhk_status'  =>  '1',
        //         'ppjhk_id'      =>  $element->ppjhk->id
        //     ]);
        // }
    }

    private function createPpjhkElemenWps(Request $request, $ppjhk){           
        foreach ($request->element as $key => $value) {
            $element = Element::find($value);
        
            $balanced = 0;
            foreach($element->items as $item){
                $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
            }
        
            $ppjhkE = $ppjhk->elements()->where('vo_element_id', $element->id)->first();
            $ppjhkElement = $ppjhk->elements()->updateOrCreate([
                'vo_element_id' =>  $element->id
            ],[
                'vo_element_id' =>  $element->id,
                'ppjhk_id'      =>  $ppjhk->id,
                'sst_id'        =>  $ppjhk->sst->id,
                'amount_ppjhk'  =>  !empty($balanced) ? $balanced : "0",
                'net_amount'  =>  !empty($balanced) ? $balanced : "0",
                //'amount_ppjhk'  =>  !empty($ppjhkE->amount_ppjhk) ? $ppjhkE->amount_ppjhk : $balanced,
                //'net_amount'    =>  !empty($ppjhkE->net_amount) ? $ppjhkE->net_amount : $balanced
            ]);
                    
            foreach($element->items as $keyI => $item){
                $ppjhkI = $ppjhk->items()
                    ->where('vo_element_id', $element->id)
                    ->where('vo_item_id', $item->id)->first();

                $ppjhkItem = $ppjhk->items()->updateOrCreate([
                    'vo_element_id'     =>  $element->id,
                    'vo_item_id'        =>  $item->id
                ],[
                    'vo_element_id'     =>  $element->id,
                    'vo_item_id'        =>  $item->id,
                    'ppjhk_id'          =>  $ppjhk->id,
                    'sst_id'            =>  $ppjhk->sst->id,
                    'ppjhk_element_id'  =>  $ppjhkElement->id,
                    'amount_ppjhk'      =>  $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum(),
                    'net_amount'      =>  $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum(),
                    //'amount_ppjhk'      =>  !empty($ppjhkI->amount_ppjhk) ? $ppjhkI->amount_ppjhk : $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum(),
                    //'net_amount'        =>  !empty($ppjhkI->net_amount) ? $ppjhkI->net_amount : $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum(),
                ]);
                
                foreach($item->subItems as $keyS => $subItem){
                    if($subItem->wps_status == 0 || $subItem->wps_status == null){
                        $ppjhkS = $ppjhk->subItems()
                            ->where('vo_element_id', $element->id)
                            ->where('vo_item_id', $item->id)
                            ->where('vo_sub_item_id', $subItem->id)
                            ->first();
                            
                        $ppjhk->subItems()->updateOrCreate([
                            'vo_element_id'     =>  $element->id,
                            'vo_item_id'        =>  $item->id,
                            'vo_sub_item_id'    =>  $subItem->id,
                        ],[
                            'vo_element_id'     =>  $element->id,
                            'vo_item_id'        =>  $item->id,
                            'vo_sub_item_id'    =>  $subItem->id,
                            'vo_active_id'      =>  $subItem->vo_active_id,
                            'ppjhk_id'          =>  $ppjhk->id,
                            'sst_id'            =>  $ppjhk->sst->id,
                            'ppjhk_element_id'  =>  $ppjhkElement->id,
                            'ppjhk_item_id'     =>  $ppjhkItem->id,
                            'rate_per_unit'     =>  !empty($ppjhkS->rate_per_unit) ? $ppjhkS->rate_per_unit : $subItem->rate_per_unit,
                            'quantity_ppjhk'    =>  !empty($ppjhkS->quantity_ppjhk) ? $ppjhkS->quantity_ppjhk : $subItem->quantity_addition,
                            'amount_ppjhk'      =>  !empty($ppjhkS->amount_ppjhk) ? $ppjhkS->amount_ppjhk : $subItem->amount_addition,
                            'net_amount'        =>  !empty($ppjhkS->net_amount) ? $ppjhkS->net_amount : $subItem->net_amount,
                            'wps_status'        =>  $subItem->wps_status,
                            'total_quantity'    =>  !empty($ppjhkS->quantity_ppjhk) ? $ppjhkS->quantity_ppjhk : $subItem->quantity_addition,
                        ]); 
                    }               
                }
            }
        
            Element::updateOrCreate([
                'id'    =>  $value
            ],[
                'ppjhk_status'  =>  '1',
                'ppjhk_id'      =>  $ppjhk->id
            ]);
        
                    
            // foreach($element->items as $item){            
                //     $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
            // }
        }
        
        // foreach ($request->element as $key => $value){
            //     $element = \App\Models\VO\PPJHK\Element::where('vo_element_id', $value)->first();
            //     foreach($element->ppjhkItems as $item){
            //         $item_addition = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $item->id)->where('wps_status', null)->pluck('amount_ppjhk')->sum();
            //         $item_net = $item_addition - $item->voItem->amount_omission;
            
            //         \App\Models\VO\PPJHK\Item::updateOrCreate([
            //             'id'    =>  $item->id
            //         ],[
            //             'amount_ppjhk'     =>  $item_addition,
            //             'net_amount'        => $item_net,
            //         ]);
            //     }
            
            //     $element_addition = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $element->id)->pluck('amount_ppjhk')->sum();
            //     $element_net = $element_addition - $element->voElement->amount_omission;
            //     \App\Models\VO\PPJHK\Element::updateOrCreate([
            //         'id'    =>  $element->id
            //     ],[
            //         'amount_ppjhk'     =>  $element_addition,
            //         'net_amount'        => $element_net,
            //     ]);
        
            //     Element::updateOrCreate([
            //         'id'    =>  $value
            //     ],[
            //         'ppjhk_status'  =>  '1',
            //         'ppjhk_id'      =>  $element->ppjhk->id
            //     ]);
            // }
        
            // foreach ($request->element as $key => $value){
            //     $element = \App\Models\VO\PPJHK\Element::where('vo_element_id', $value)->first();
            //     foreach($element->ppjhkItems as $item){
            //         $item_addition = \App\Models\VO\PPJHK\SubItem::where('ppjhk_item_id', $item->id)->where('wps_status', null)->pluck('amount_ppjhk')->sum();
            //         $item_net = $item_addition - $item->voItem->amount_omission;
            
            //         \App\Models\VO\PPJHK\Item::updateOrCreate([
            //             'id'    =>  $item->id
            //         ],[
            //             'amount_ppjhk'     =>  $item_addition,
            //             'net_amount'        => $item_net,
            //         ]);
            //     }
            
            //     $element_addition = \App\Models\VO\PPJHK\Item::where('ppjhk_element_id', $element->id)->pluck('amount_ppjhk')->sum();
            //     $element_net = $element_addition - $element->voElement->amount_omission;
            
            //     \App\Models\VO\PPJHK\Element::updateOrCreate([
            //         'id'    =>  $element->id
            //     ],[
            //         'amount_ppjhk'     =>  $element_addition,
            //         'net_amount'        => $element_net,
            //     ]);
        
            //     Element::updateOrCreate([
            //         'id'    =>  $value
            //     ],[
            //         'ppjhk_status'  =>  '1',
            //         'ppjhk_id'      =>  $element->ppjhk->id
            //     ]);
            // }
    }

    private function createPpjhkCancel(Request $request, $ppjhk){
        foreach ($request->cancel as $key => $value) {
            Cancel::updateOrCreate([
                'id'    =>  $value,
            ],[
                'ppjhk_status'  =>  1,
                'ppjhk_id'      =>  $ppjhk->id
            ]);
        }
    }

    public function uploadPpjhk(Request $request, $id)
    {
        $ppjhk = Ppjhk::find($id);

        if(!empty($request->action)){                          
            foreach($ppjhk->elements as $ppjhkElement){
                if($ppjhkElement->voElement->ppjhk_status == '1'){
                    $voActives = \App\Models\VO\Active::where('id', '=', $ppjhkElement->voElement->vo_active_id)->first();
                    if($voActives != null){
                        if($voActives->count() > 0){
                            $totalAmount = 0;
                            $totalAmount = $voActives->amount + $ppjhkElement->net_amount;
                            /*
                            * Wait for confirmation from bpub 
                            * Kalau wps tambahan, perlu masuk masuk sebagai lum sum atau terus ke ipc bila approved
                            * 04-Nov-2019
                            */

                            // $voActives->update([
                            //     'amount'    =>  $totalAmount
                            // ]);
                        }
                    }
                }
            }
            Ppjhk::find($id)->update([
                'status'    =>  '1',
            ]);
        }

        if ($request->hasFile('document')) {
            $ppjhk_requirements = [
                'request_hDocumentId' => null,
                'request_hasFile'     => $request->hasFile('document'),
                'request_file'        => $request->file('document'),
                'acquisition'         => $ppjhk->sst->acquisition,
                'doc_type'            => 'PPJHK/' . $ppjhk->no . '/PPJHK_RPT',
                'doc_id'              => $ppjhk->id,
            ];
            common()->upload_document($ppjhk_requirements);
        }

        $workflow = WorkflowTask::where('acquisition_id', $ppjhk->sst->acquisition_id)->where('ppjhk_id',$ppjhk->id)->where('flow_location','PPJHK')->first();
        $workflow->ppjhk_id = $ppjhk->id;
        $workflow->flow_desc = '';
        $workflow->user_id = null;
        $workflow->is_claimed = 1;
        $workflow->flow_location = 'PPJHK';
        $workflow->update();

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }
}
