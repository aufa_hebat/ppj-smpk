<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\VO\KKK\Element;
use App\Models\VO\KKK\Item;
use App\Models\VO\KKK\SubItem;
use App\Models\VO\Type;
use App\Models\VO\VO;
use Illuminate\Http\Request;

class VariationOrderKKKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vo            = VO::where('id', $request->vo_id)->first();
        $BQElemen      = $vo->sst->acquisition->bqElements;
        $BQItem        = $vo->sst->acquisition->bqItems;
        $BQSub         = $vo->sst->acquisition->bqSubItems;
        $item_id_array = [];

        if ('' == $request->vo_type_id) {
            $vo_type = Type::updateOrCreate([
                'vo_id'                   => $request->vo_id,
                'variation_order_type_id' => $request->variation_order_type_id,
            ]);
        }

        foreach ($request->select_wi as $all_no) {
            $no             = explode('.', $all_no);
            $bq_element_no  = $no[0];
            $bq_item_no     = $no[1];
            $bq_sub_item_no = $no[2];

            // Insert into vo_kkk_elements
            $bq_element_id_name          = 'bq_element_id_' . $bq_element_no;
            $bq_element_element_name     = 'bq_element_element_' . $bq_element_no;
            $bq_element_description_name = 'bq_element_description_' . $bq_element_no;

            $vo_kkk_element = Element::updateOrCreate(
                [
                    'acquisition_id' => $request->acquisition_id,
                    'bq_element_id'  => $request->$bq_element_id_name,
                    'vo_id'          => $request->vo_id,
                ],
                [
                    'no'          => $bq_element_no,
                    'element'     => $request->$bq_element_element_name,
                    'description' => $request->$bq_element_description_name,
                ]
            );

            // Insert into vo_kkk_items
            $bq_item_id_name          = 'bq_item_id_' . $bq_element_no . '_' . $bq_item_no;
            $bq_item_item_name        = 'bq_item_item_' . $bq_element_no . '_' . $bq_item_no;
            $bq_item_description_name = 'bq_item_description_' . $bq_element_no . '_' . $bq_item_no;
            $bq_item_amount_name      = 'bq_item_amount_' . $bq_element_no . '_' . $bq_item_no;
            $bq_summary_name          = 'WI_HURAIAN_' . $bq_element_no . '_' . $bq_item_no;
            $bq_reason_name           = 'WI_SEBAB_' . $bq_element_no . '_' . $bq_item_no;

            $vo_kkk_item = Item::updateOrCreate(
                [
                    'acquisition_id' => $request->acquisition_id,
                    'bq_item_id'     => $request->$bq_item_id_name,
                    'vo_id'          => $request->vo_id,
                ],
                [
                    'bq_element_id'     => $request->$bq_element_id_name,
                    'vo_kkk_element_id' => $vo_kkk_element->id,
                    'bq_no_element'     => $bq_element_no,
                    'no'                => $bq_item_no,
                    'item'              => $request->$bq_item_item_name,
                    'description'       => $request->$bq_item_description_name,
                    'amount'            => '' != $request->$bq_item_amount_name ? money()->toMachine($request->$bq_item_amount_name) : money()->toMachine(0),
                    'summary'           => $request->$bq_summary_name,
                    'reason_required'   => $request->$bq_reason_name,
                ]
            );
            $item_id_array[] = $vo_kkk_item->id; // to be use in calculating amount & percentage

            // Insert into vo_kkk_sub_items
            $bq_sub_item_id_name            = 'bq_sub_item_id_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_item_name          = 'WI_ITEM_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_unit_name          = 'bq_sub_item_unit_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_quantity_name      = 'bq_sub_item_quantity_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_rate_per_unit_name = 'bq_sub_item_rate_per_unit_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $bq_sub_item_amount_name        = 'bq_sub_item_amount_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_quantity_omission           = 'WI_KUANTITI_OMISSION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_quantity_addition           = 'WI_KUANTITI_ADDITION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_amount_omission             = 'WI_AMAUN_OMISSION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_amount_addition             = 'WI_AMAUN_ADDITION_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_net_amount                  = 'WI_NET_AMOUNT_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;
            $pq_bq_reference                = 'WI_BQ_REFERENCE_' . $bq_element_no . '_' . $bq_item_no . '_' . $bq_sub_item_no;

            $vo_kkk_sub_item = SubItem::updateOrCreate(
                [
                    'acquisition_id' => $request->acquisition_id,
                    'bq_subitem_id'  => $request->$bq_sub_item_id_name,
                    'vo_id'          => $request->vo_id,
                ],
                [
                    'bq_element_id'     => $request->$bq_element_id_name,
                    'bq_item_id'        => $request->$bq_item_id_name,
                    'vo_kkk_item_id'    => $vo_kkk_item->id,
                    'bq_no_element'     => $bq_element_no,
                    'bq_no_item'        => $bq_item_no,
                    'no'                => $bq_sub_item_no,
                    'item'              => $request->$bq_sub_item_item_name,
                    'unit'              => $request->$bq_sub_item_unit_name,
                    'quantity'          => '' != $request->$bq_sub_item_quantity_name ? money()->toMachine($request->$bq_sub_item_quantity_name) : money()->toMachine(0),
                    'rate_per_unit'     => '' != $request->$bq_sub_item_rate_per_unit_name ? money()->toMachine($request->$bq_sub_item_rate_per_unit_name) : money()->toMachine(0),
                    'amount'            => '' != $request->$bq_sub_item_amount_name ? money()->toMachine($request->$bq_sub_item_amount_name) : money()->toMachine(0),
                    'quantity_omission' => '' != $request->$pq_quantity_omission ? money()->toMachine($request->$pq_quantity_omission) : money()->toMachine(0),
                    'quantity_addition' => '' != $request->$pq_quantity_addition ? money()->toMachine($request->$pq_quantity_addition) : money()->toMachine(0),
                    'amount_omission'   => '' != $request->$pq_amount_omission ? money()->toMachine($request->$pq_amount_omission) : money()->toMachine(0),
                    'amount_addition'   => '' != $request->$pq_amount_addition ? money()->toMachine($request->$pq_amount_addition) : money()->toMachine(0),
                    'net_amount'        => '' != $request->$pq_net_amount ? money()->toMachine($request->$pq_net_amount) : money()->toMachine(0),
                    'bq_reference'      => $request->$pq_bq_reference,
                ]
            );
        }

        // Updating vo_kkk_items amount & percentage
        $unique_item_id_array = array_unique($item_id_array);
        $total_omission       = 0;
        $total_addition       = 0;
        $total_amount         = 0;
        foreach ($unique_item_id_array as $item_id) {
            $new_vo_kkk_item = Item::where('id', $item_id)->first();
            foreach ($new_vo_kkk_item->vo_kkk_sub_items as $sub_item) {
                $total_omission = $total_omission + str_replace(',', '', money()->toCommon($sub_item->amount_omission ?? 0, 2));
                $total_addition = $total_addition + str_replace(',', '', money()->toCommon($sub_item->amount_addition ?? 0, 2));
            }

            $total_amount = $total_addition - $total_omission;
            $percentage   = ($total_amount / str_replace(',', '', money()->toCommon($new_vo_kkk_item->amount ?? 0, 2))) * 100;
            if (strpos($percentage, '-')) {
                $updated_vo_kkk_item = Item::where('id', $item_id)->update([
                    'amount_omission'  => money()->toMachine($$total_amount),
                    'percent_omission' => $percentage,
                ]);
            } else {
                $updated_vo_kkk_item = Item::where('id', $item_id)->update([
                    'amount_addition'  => money()->toMachine($total_amount),
                    'percent_addition' => $percentage,
                ]);
            }
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
