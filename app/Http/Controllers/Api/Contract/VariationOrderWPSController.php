<?php

namespace App\Http\Controllers\Api\Contract;

use App\Http\Controllers\Controller;
use App\Models\VariationOrderCategory;
use App\Models\VO\Element;
use App\Models\VO\Item;
use App\Models\VO\SubItem;
use App\Models\VO\Type;
use Illuminate\Http\Request;

class VariationOrderWPSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vo       = VariationOrder::where('id', $request->variation_order_id)->first();
        $BQElemen = $vo->sst->acquisition->bqElements;
        $BQItem   = $vo->sst->acquisition->bqItems;
        $BQSub    = $vo->sst->acquisition->bqSubItems;

        if ('' == $request->variation_order_category_id) {
            $vo_category = VariationOrderCategory::create([
                'user_id'                 => user()->id,
                'department_id'           => user()->department->id,
                'sst_id'                  => $request->sst_id,
                'variation_order_id'      => $request->variation_order_id,
                'variation_order_type_id' => $request->variation_order_type_id,
            ]);
            $variation_order_category_id = $vo_category->id;
        } else {
            $variation_order_category_id = $request->variation_order_category_id;
        }

        $vo_element = VariationOrderElement::create([
            'user_id'                     => user()->id,
            'department_id'               => user()->department->id,
            'sst_id'                      => $request->sst_id,
            'variation_order_id'          => $request->variation_order_id,
            'variation_order_type_id'     => $request->variation_order_type_id,
            'variation_order_category_id' => $variation_order_category_id,
            'bq_element_id'               => $request->bq_element_id,
            'no'                          => $request->bq_element_no,
            'name'                        => $request->bq_element_element,
            'reason_required'             => $request->bq_element_description,
        ]);

        foreach ($BQElemen as $bq) {

            $isProSumFound = common()->checkProSum($bq->element);
            
            if($isProSumFound){
            //if ('PROVISIONAL SUMS' == $bq->element || 'PROVISIONAL SUM' == $bq->element || 'PROV SUM' == $bq->element || 'PROV. SUM' == $bq->element || 'PROV.SUM' == $bq->element) {
                foreach ($BQItem as $it) {
                    if ($bq->no == $it->bq_no_element && $bq->acquisition_id == $it->acquisition_id) {
                        $vo_item = VariationOrderItem::create([
                            'user_id'                     => user()->id,
                            'department_id'               => user()->department->id,
                            'sst_id'                      => $request->sst_id,
                            'variation_order_id'          => $request->variation_order_id,
                            'variation_order_type_id'     => $request->variation_order_type_id,
                            'variation_order_category_id' => $variation_order_category_id,
                            'variation_order_element_id'  => $vo_element->id,
                            'bq_item_id'                  => $it->id,
                            'no'                          => $it->no,
                            'name'                        => $it->item,
                            'reason_required'             => $it->description,
                        ]);

                        $row_name          = 'wps_subitem_no' . $it->no;
                        $row_element       = 'wps_subitem_element' . $it->no;
                        $row_unit          = 'wps_subitem_unit' . $it->no;
                        $row_quantity      = 'wps_subitem_quantity' . $it->no;
                        $row_rate_per_unit = 'wps_subitem_rate_per_unit' . $it->no;
                        $row_amount        = 'wps_subitem_amount' . $it->no;
                        $count             = 0;
                        foreach ($request->$row_name as $new_wps_item_no) {
                            $vo_sub_item = VariationOrderSubItem::create([
                                'user_id'                     => user()->id,
                                'department_id'               => user()->department->id,
                                'sst_id'                      => $request->sst_id,
                                'variation_order_id'          => $request->variation_order_id,
                                'variation_order_type_id'     => $request->variation_order_type_id,
                                'variation_order_category_id' => $variation_order_category_id,
                                'variation_order_element_id'  => $vo_element->id,
                                'variation_order_item_id'     => $vo_item->id,
                                'no'                          => $count + 1,
                                'name'                        => $request->$row_element[$count],
                                'unit'                        => $request->$row_unit[$count],
                                'quantity'                    => $request->$row_quantity[$count],
                                'rate_per_unit'               => $request->$row_rate_per_unit[$count],
                                'estimated_additional_amount' => $request->$row_amount[$count],
                            ]);
                            ++$count;
                        }
                    }
                }
            }
        }

        return response()->api([], __('Rekod telah berjaya dikemaskini.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
