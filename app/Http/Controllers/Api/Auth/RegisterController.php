<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use JWTAuth;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create([
            'name'                   => $request->get('name'),
            'email'                  => $request->get('email'),
            'password'               => bcrypt($request->get('password')),
            'executor_department_id' => $data['executor_department_id'],
            'department_id'          => $data['department_id'],
            'section_id'             => $data['section_id'],
            'supervisor_id'          => $data['supervisor_id'],
            'position_id'            => $data['position_id'],
            'scheme_id'              => $data['scheme_id'],
            'grade_id'               => $data['grade_id'],
        ]);

        event(new Registered($user));

        $token = JWTAuth::fromUser($user);

        return response()->api($token, 'Registration successful.', true, 201);
    }
}
