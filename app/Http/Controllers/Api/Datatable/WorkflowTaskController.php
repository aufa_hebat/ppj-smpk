<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use App\Models\WorkflowTask;
// use App\Transformers\Datatable\WorkflowTaskTransformer as Transformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class WorkflowTaskController extends Controller
{
    public function __invoke(Request $request)
    {
        //this query for not claimed task only means it is in bucket
        //get current login
        $currentUserId = user()->id;
        $isClaimed     = true;
      
        /* role
         administrator = 1
         developer  2
         pengesah = 3
         penyedia = 4
         penyemak = 5
         urusetia = 6
         user = 7
         ketuajabatan = 8
         ketuabahagian = 9
         ketuaunit = 10
         kaunter = 11
         urusetiavo = 12
        */
        
         // $query = WorkflowTask::where('is_claimed', $isClaimed)             
         //                  ->where('user_id', $currentUserId)            
         //                  ->latest('updated_at')->get();
        
        // 14.2.2019 : Add filter query by $workflow->flow_location = '?';
         /*
        if(user()->current_role_login == 'administrator') {
            //this query is for normal role
           
        }else
        
        */
        if(user()->current_role_login == 'pengesah'){
            //TODO          
            $query  =   WorkflowTask::where('is_claimed', $isClaimed)             
                        ->where('user_id', $currentUserId)->where('role_id',3)            
                        ->latest('updated_at')->get();
        }else if(user()->current_role_login == 'penyedia'){
            //TODO          
            $query  =   WorkflowTask::where('is_claimed', $isClaimed)             
                        ->where('user_id', $currentUserId)->where('role_id',4)            
                        ->latest('updated_at')->get();
        }else if(user()->current_role_login == 'penyemak'){
            //TODO        
            $query  =   WorkflowTask::where('is_claimed', $isClaimed)             
                        ->where('user_id', $currentUserId)->where('role_id',5)            
                        ->latest('updated_at')->get();  
        }else if(user()->current_role_login == 'urusetia'){
            //TODO 
            $query  =   WorkflowTask::where('is_claimed', $isClaimed)             
                        ->where('user_id', $currentUserId)->where('role_id',6)            
                        ->latest('updated_at')->get();         
        }else if(user()->current_role_login == 'urusetiavo'){
            //TODO 
            $query  =   WorkflowTask::where('is_claimed', $isClaimed)             
                        ->where('user_id', $currentUserId)->where('role_id',12)            
                        ->latest('updated_at')->get();         
        }else {
            //TODO 
            $query  =   WorkflowTask::where('is_claimed', $isClaimed)             
                        ->where('user_id', $currentUserId)            
                        ->latest('updated_at')->get();         
        }

        $builder = new Collection();
        foreach ($query as $task) {
            $builder->push([
                'id'  => $task->id,
                'flow_name' => $task->flow_name,
                'url'       => $task->url,
                'flow_desc' => $task->flow_desc,
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
