<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use Illuminate\Http\Request;

class CertificateController extends Controller
{
    public function __invoke(Request $request)
    {
        $company = Company::findByHashSlug($request->hashslug);

        $query = Owner::whereHas('company', function ($query) use ($company) {
            $query->where('company_id', $company->id);
        })->datatable();

        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new OwnerTransformer())
            ->toJson();
    }
}
