<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Transformers\Datatable\CompanyTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;

class CompanyController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Company::where('company_name','!=',null)->get();

        $todayDate = Carbon::now()->format('Y-m-d');

        $builder = new Collection();
        foreach ($query as $company) {
            if(empty($company->ssm_start_date)){
            	$status_company = '-';
            } elseif(!empty($company->ssm_end_date) && $company->ssm_end_date >= $todayDate){
	            	$status_company = 'Aktif';
            } elseif(!empty($company->ssm_start_date) && $company->ssm_end_date == null){
                    $status_company = 'Aktif';
            } else {
            	$status_company = 'Tidak Aktif';
            }

            $builder->push([
                'hashslug'     => $company->hashslug,
                'company_name' => $company->company_name,
                'email'     => $company->email,
                'status_company'    => $status_company,
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
