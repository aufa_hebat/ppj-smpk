<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use App\Transformers\Datatable\UserTransformer;

class UserController extends Controller
{
    public function __invoke(Request $request)
    {
        //$query = [];
//        $query = User::where('name','!=',null)->get();
//        
//         return app('datatables')
//        ->collection($query)
//        ->setTransformer(new UserTransformer())
//        ->toJson();
        
        //return app('datatables')->eloquent(User::datatable())->toJson();
        
        return app('datatables')->eloquent(User::with(['position','department','section','scheme','grade','supervisor'])->datatable())->setTransformer(new UserTransformer())->toJson();
    }
}
