<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Transformers\Datatable\BoxTransformer;
use Illuminate\Support\Carbon;

class AssessmentController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Acquisition::where('cancelled_at',null)->where('deleted_at',null)->with('approval')->select('acquisitions.*')->latest();

        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new BoxTransformer())
            ->toJson();
    }
}
