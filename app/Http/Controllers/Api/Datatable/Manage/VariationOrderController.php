<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\VO\VO;
use App\Transformers\Datatable\VOTransformer as Transformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class VariationOrderController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Sst::withDetails()
            ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
            ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
            ->get();

        $builder = new Collection();
        foreach ($query as $row) {
            $builder->push([
                'hashslug'          => $row->hashslug,
                'no_kontrak'        => $row->acquisition->reference,
                'tajuk'             => $row->acquisition->title,
                'syarikat_terpilih' => $row->company->company_name,
            ]);
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
