<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\Company\Owner;
use App\Transformers\Datatable\OwnerTransformer;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    public function __invoke(Request $request)
    {
        $company = Company::findByHashSlug($request->hashslug);

        $query = Owner::whereHas('company', function ($query) use ($company) {
            $query->where('company_id', $company->id);
        })->get();

        $builder = new Collection();
        foreach ($query as $owner) {
            $type = '';
            if($owner->cidb_cert == 1){
                $type = 'CIDB';
            }
            if($owner->mof_cert == 1){
                $type = 'MOF';
            }
            if($owner->kdn_cert == 1){
                $type = 'KDN';
            }
            if($owner->cidb_cert == 1 && $owner->mof_cert == 1){
                $type = 'CIDB, MOF';
            }
            if($owner->cidb_cert == 1 && $owner->kdn_cert == 1){
                $type = 'CIDB, KDN';
            }
            if($owner->mof_cert == 1 && $owner->kdn_cert == 1){
                $type = 'MOF, KDN';
            }
            if($owner->cidb_cert == 1 && $owner->mof_cert == 1 && $owner->kdn_cert == 1){
                $type = 'CIDB, MOF, KDN';
            }

            $builder->push([
                'hashslug'     => $owner->hashslug,
                'name' => $owner->name,
                'ic_number'     => $owner->ic_number,
                'email'    => $owner->email,
                'type'    => $type,
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
