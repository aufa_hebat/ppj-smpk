<?php

namespace App\Http\Controllers\Api\Datatable\Manage;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\DocumentAcceptance;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use App\Models\Acquisition\Ipc;

class DocumentAcceptanceListController extends Controller
{
    public function __invoke(Request $request)
    {

        $sst = Sst::findByHashSlug($request->hashslug);

        $query = DocumentAcceptance::where('sst_id', $sst->id)->latest()->get();

        /**
         * @todo this should be in transforer
         *
         * @var Collection
         */
        $builder = new Collection();
        foreach ($query as $da) {
            // $displayshows = 'none';
            // $displayedits = '';
            // if($da->acceptance_status == 1){
            //     $displayshows = '';
            //     $displayedits = 'none';
            // }
            $builder->push([
                'hashslug'          => $da->hashslug,
                'document_acceptance_at'        => !empty($da->document_acceptance_at) ? Carbon::createFromFormat('Y-m-d G:i:s', $da->document_acceptance_at)->format("d/m/Y G:i A") : "-",
                'remarks'             => $da->remarks,
                'user'                => $da->user->name
                // 'displayshows'        => $displayshows,
                // 'displayedits'        => $displayedits,
            ]);
        }
            // dd($builder);

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
