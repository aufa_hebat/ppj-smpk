<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use Illuminate\Http\Request;
use App\Models\CIDBCode;
use App\Http\Controllers\Controller;

class CIDBCategoryController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = CIDBCode::where('type','category')->orderby('code', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
