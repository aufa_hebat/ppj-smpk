<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\PeriodType;
use Illuminate\Http\Request;

class PeriodTypeController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = PeriodType::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
