<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\BonType;
use Illuminate\Http\Request;

class BonTypeController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = BonType::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
