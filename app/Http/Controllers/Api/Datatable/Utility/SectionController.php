<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function __invoke(Request $request)
    {
        $query1 = Section::join('departments', 'category', '=', 'departments.code')->select('sections.id');
        $query  = Section::whereNotIn('id', $query1);

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
