<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Type;
use Illuminate\Http\Request;

class AcquisitionTypeController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Type::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
