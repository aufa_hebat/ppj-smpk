<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\StandardCode;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = StandardCode::where([
            ['code', 'KOD_NEGERI'],
        ])->orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
