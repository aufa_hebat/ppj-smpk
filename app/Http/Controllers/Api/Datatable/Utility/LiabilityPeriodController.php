<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\DefectLiabilityPeriod;
use Illuminate\Http\Request;

class LiabilityPeriodController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = DefectLiabilityPeriod::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
