<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval\Authority;
use App\Transformers\Datatable\AuthorityTransformer;
use Illuminate\Http\Request;

class AuthoritiesController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Authority::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new AuthorityTransformer())
            ->toJson();
    }
}
