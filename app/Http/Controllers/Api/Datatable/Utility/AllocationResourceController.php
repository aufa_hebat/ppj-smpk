<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AllocationResource;
use Illuminate\Http\Request;

class AllocationResourceController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = AllocationResource::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
