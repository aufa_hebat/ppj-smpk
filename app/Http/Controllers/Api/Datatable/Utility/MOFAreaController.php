<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use Illuminate\Http\Request;
use App\Models\MOFCode;
use App\Http\Controllers\Controller;

class MOFAreaController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = MOFCode::where('type','category')->orderby('code', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
