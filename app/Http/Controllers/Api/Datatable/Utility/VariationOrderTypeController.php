<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\VariationOrderType;
use Illuminate\Http\Request;

class VariationOrderTypeController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = VariationOrderType::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
