<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Honorary;
use Illuminate\Http\Request;

class HonoraryController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Honorary::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
