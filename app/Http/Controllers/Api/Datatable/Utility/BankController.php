<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Bank::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
