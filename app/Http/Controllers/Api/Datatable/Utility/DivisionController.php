<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Http\Request;

class DivisionController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Section::join('departments', 'category', '=', 'departments.code')->select('sections.*');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
