<?php

namespace App\Http\Controllers\Api\Datatable\Utility;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Category;
use Illuminate\Http\Request;

class AcquisitionCategoryController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Category::orderby('id', 'asc');

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
