<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\Review;
use App\Transformers\Datatable\AcquisitionTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class BriefingController extends Controller
{
    public function __invoke(Request $request)
    {
        if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer'){
            $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at', null);
                })->datatable()->where([
                ['briefing_status', true],
                ['cancelled_at', null],
                ['deleted_at', null]
            ])->latest()->get();
        }//staff bpub, dalam list baru cari project owner
        elseif((user()->current_role_login == 'penyemak' || user()->current_role_login == 'pengesah') && user()->executor_department_id == 25){
            $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at', null);
                })->whereDate('advertised_at', '<=', Carbon::now()->format('Y-m-d') . '%')->whereDate('closed_at', '>=', Carbon::now()->format('Y-m-d') . '%')
                ->whereHas('reviews', function ($que) {
                    $que->where('task_by',user()->id)->where('status','Selesai')->latest();
                })->datatable()->where([
                ['briefing_status', true],
                ['cancelled_at', null],
                ['deleted_at', null]
            ])->latest()->get();
        }elseif(user()->current_role_login == 'penyedia'){
            $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at', null);
                })->whereDate('briefing_at', '<=', Carbon::now()->format('Y-m-d') . '%')->whereDate('closed_at', '>=', Carbon::now()->format('Y-m-d') . '%')
                ->whereHas('reviews', function ($que) {
                    $que->where('status','Selesai')->latest();
                })->datatable()->where([
                ['user_id', user()->id],
                ['briefing_status', true],
                ['cancelled_at', null],
                ['deleted_at', null]
            ])->latest()->get();
        }else{
            $query = Acquisition::whereHas('approval', function ($query) {
                $query->where('department_id', user()->department->id)->where('deleted_at',null)->latest();
            })->whereDate('briefing_at', '<=', Carbon::now()->format('Y-m-d') . '%')->whereDate('closed_at', '>=', Carbon::now()->format('Y-m-d') . '%')->where([
                ['briefing_status', true],
                ['cancelled_at', null],
                ['deleted_at', null],
            ])->whereHas('reviews', function ($que) {
                $que->where('status','Selesai')->latest();
            })->datatable()->get();
        }
        $builder = new Collection();
        foreach ($query as $acquisition) {
            
            $builder->push([
                        'hashslug'    => $acquisition->hashslug,
                        'reference'   => $acquisition->reference,
                        'title'       => $acquisition->title,
                        'briefing_at' => (!empty($acquisition->briefing_at))? $acquisition->briefing_at->format('d/m/Y'):null,
                        'updated_at'  => $acquisition->updated_at->format('d/m/Y'),
                        'display'     => '',
                        'department'  => $acquisition->approval->department->name,
                    ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
        
//        return app('datatables')
//            ->eloquent(Acquisition::datatable()->whereDate('advertised_at', '<=', Carbon::now()->format('Y-m-d') . '%')->whereDate('closed_at', '>=', Carbon::now()->format('Y-m-d') . '%')->where([
//                ['briefing_status', true],
//                ['cancelled_at', null],
//            ])->latest())
//            ->setTransformer(new AcquisitionTransformer())
//            ->toJson();
    }
}
