<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use App\Transformers\Datatable\AppointedCompanyTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

class AcceptanceLetterController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = [];
        
        $tag = $request->tag;
        if($tag == '6MonthToEnd'){
            //TODO - just keluarkan 6 month to end value
        $ProjectProgressProcessor = new \App\Processors\Charts\ProjectProgressProcessorPersonal();
        
        $sst = $ProjectProgressProcessor->contractsSixMonthToEnd(); 
            //return null;
        $sstIdArray = $sst->pluck('acquisition_id');
        //$sstIdArray = array(1);
        
        
        //sst find by
         $query = AppointedCompany::withDetails()                                
                   ->whereIn('acquisition_id', $sstIdArray)
                  //->whereHas('acquisition', function ($query) {
                        //$query-> whereIn('id', $sstIdArray);
                   // })
                //->latest()
                    ->get();
        
            
        }
        else if($tag == 'ContractOnGoing'){
            
        $ProjectProgressProcessor = new \App\Processors\Charts\ProjectProgressProcessorPersonal();      
        $sst2 = $ProjectProgressProcessor->contractsOnGoing(); 
            //return null;
        $sstIdArray2 = $sst2->pluck('acquisition_id');
        
        
        //$sstIdArray = array(1);
           
        //sst find by
         $query = AppointedCompany::withDetails()                                
                   ->whereIn('acquisition_id', $sstIdArray2)
                  //->whereHas('acquisition', function ($query) {
                        //$query-> whereIn('id', $sstIdArray);
                   // })
                //->latest()
                    ->get();    
         
        }else if($tag =='contractsNotSignedYet'){
            
        $ProjectProgressProcessor = new \App\Processors\Charts\ProjectProgressProcessorPersonal();      
        $sst2 = $ProjectProgressProcessor->contractsNotSignedYet(); 
            //return null;
        $sstIdArray2 = $sst2->pluck('acquisition_id');
        
        
        //$sstIdArray = array(1);
           
        //sst find by
         $query = AppointedCompany::withDetails()                                
                   ->whereIn('acquisition_id', $sstIdArray2)
                  //->whereHas('acquisition', function ($query) {
                        //$query-> whereIn('id', $sstIdArray);
                   // })
                //->latest()
                    ->get();      
        }else if($tag == 'contractsDoneButNotSofaYet'){
            
            $ProjectProgressProcessor = new \App\Processors\Charts\ProjectProgressProcessorPersonal();      
        $sst2 = $ProjectProgressProcessor->contractsDoneButNotSofaYet(); 
            //return null;
        $sstIdArray2 = $sst2->pluck('acquisition_id');
             
        //$sstIdArray = array(1);       
        //sst find by
         $query = AppointedCompany::withDetails()                                
                   ->whereIn('acquisition_id', $sstIdArray2)
                  //->whereHas('acquisition', function ($query) {
                        //$query-> whereIn('id', $sstIdArray);
                   // })
                //->latest()
                    ->get();    
            
        }
         
        else{
            
        
            if(!empty(user()->department_id) && !empty(user()->executor_department_id)){  
                if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                    $query = AppointedCompany::withDetails()
                            //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                            //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                            ->where(function($query){
                                $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                            })                    
                            ->where(function($query){
                                $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                            }) 
                            ->whereHas('acquisition', function ($query) {
                                $query->where('deleted_at',null);
                            })
                            ->latest()
                            ->get();
                }else if(user()->current_role_login == 'ketuajabatan'){
                    //ketua department
                    $query = AppointedCompany::withDetails()
                            //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                            //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                            ->where(function($query){
                                $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                            })                    
                            ->where(function($query){
                                $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                            }) 
                            ->whereHas('acquisition', function ($query) {
                                $query->where('department_id', user()->department_id)
                                        ->where('deleted_at',null);
                            })
                            ->latest()
                            ->get();
                }else if(user()->current_role_login == 'ketuabahagian'){
                    //user ketua bahagian
                    $query = AppointedCompany::withDetails()
                            //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                            //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                            ->where(function($query){
                                $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                            })                    
                            ->where(function($query){
                                $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                            }) 
                            ->whereHas('acquisition', function ($query) {
                                $query->where('department_id', user()->department_id)
                                ->where('executor_department_id','=',user()->executor_department_id)
                                ->where('deleted_at',null);
                            })
                            ->latest()
                            ->get();
                }else if(user()->current_role_login == 'penyedia'){
                    //user penyedia
                    $query = AppointedCompany::withDetails()
                            //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                            //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                            ->where(function($query){
                                $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                            })                    
                            ->where(function($query){
                                $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                            }) 
                            ->whereHas('acquisition', function ($query) {
                                $query->where('user_id', user()->id)
                                    ->where('deleted_at',null)
                                    ->where('cancelled_at',null);
                                })
                            ->latest()
                            ->get();

                }else {   
                    //user biasa , section
                    $query = AppointedCompany::withDetails()
                            //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                            //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                            ->where(function($query){
                                $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                            })                    
                            ->where(function($query){
                                $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                            }) 
                            ->whereHas('acquisition', function ($query) {
                                $query
                                // ->where('department_id', user()->department_id)
                                // ->where('executor_department_id','=',user()->executor_department_id)
                                // ->where('section_id','=',user()->section_id)
                                // ->where('deleted_at',null)
                                ->where(function($q) {
                                    $q->where([
                                        ['department_id',user()->department_id],
                                        ['executor_department_id',user()->executor_department_id],
                                        ['section_id',user()->section_id],
                                    ])->orWhereHas('history_semakan_sst', function ($que) {
                                        $que->where('requested_by',user()->id)->latest();
                                    });
                                });
                            })
                            ->latest()
                            ->get();
                }
            }
        
        }
//        $query = AppointedCompany::withDetails()
//            ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
//            ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
//            ->latest()
//            ->get();

        return app('datatables')
        ->collection($query)
        ->setTransformer(new AppointedCompanyTransformer())
        ->toJson();
    }
}
