<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class EvaluationController extends Controller
{
    public function __invoke(Request $request)
    {
        //if admin, keluarkan semua
         if(user()->current_role_login == 'administrator'){
             $query = Acquisition::with(['approval'])->get();
         }else{       
            $query = Acquisition::with(['approval', 'appointed', 'appointed.company'])->where('status_task','Keputusan')->get();
         }

        $builder = new Collection();
        foreach ($query as $acquisition) {
            if($acquisition->boxes->count() > 0){
                $prices    = '';
                $companies = '';
                if (! empty($acquisition->appointed)) {
                    foreach ($acquisition->appointed as $appointed) {
                        $prices .= money()->toHuman($appointed->offered_price) . ' ';

                        if (! empty($appointed->company)) {
                            $companies .= $appointed->company->company_name . ' ';
                        }
                    }
                }

                $builder->push([
                    'hashslug'          => $acquisition->hashslug,
                    'no_perolehan'      => $acquisition->reference,
                    'tajuk'             => $acquisition->title,
                    'syarikat_terpilih' => $companies,
                    'harga_bersih'      => $prices,
                    //'tarikh_perolehan'  => Carbon::createFromFormat('Y-m-d H:i:s', $acquisition->created_at)->format('d/m/Y'),
                    'tarikh_perolehan'  => Carbon::createFromFormat('Y-m-d H:i:s', $acquisition->closed_at)->format('d/m/Y'),
                ]);
            }
            
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
