<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class IpcController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = [];
        if(!empty(user()->department_id)){   
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                    //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->where(function($query){
                        $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                    })                    
                    ->where(function($query){
                        $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                    }) 
                    ->whereHas('acquisition', function ($query) {
                        $query->where('deleted_at',null);
                    })
                    ->latest()
                    ->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                    //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->where(function($query){
                        $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                    })                    
                    ->where(function($query){
                        $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                    }) 
                    ->whereHas('acquisition', function ($query) {
                        $query->where('department_id', user()->department_id)
                        ->where('deleted_at',null);
                    })
                    ->latest()
                    ->get();
            }else if(user()->current_role_login == 'ketuabahagian' && !empty(user()->executor_department_id)){
                //user ketua bahagian
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                    //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->where(function($query){
                        $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                    })                    
                    ->where(function($query){
                        $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                    }) 
                    ->whereHas('acquisition', function ($query) {
                        $query->where('department_id', user()->department_id)
                        ->where('executor_department_id','=',user()->executor_department_id)
                        ->where('deleted_at',null);
                    })
                    ->latest()
                    ->get();
            }elseif(user()->current_role_login == 'penyedia') {   
                //user biasa , section
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                    //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->where(function($query){
                        $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                    })                    
                    ->where(function($query){
                        $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                    }) 
                    ->whereHas('acquisition', function ($query) {
                        $query->where([
                                ['user_id',user()->id],
                                ['deleted_at',null],
                                ['cancelled_at',null],
                            ]);
                    })->where('user_id',user()->id)
                    ->latest()
                    ->get();
            }
            else {   
                //user biasa , section
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                    //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                    //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                    ->where(function($query){
                        $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                    })                    
                    ->where(function($query){
                        $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                    }) 
                    ->whereHas('acquisition', function ($query) {
                        $query->where('deleted_at',null)->where('cancelled_at',null)
                            ->orWhere([
                                ['department_id',user()->department_id],
                                ['executor_department_id',user()->executor_department_id],
                                ['section_id',user()->section_id],
                            ])
                            ->orWhereHas('history_semakan_ipc', function ($que) {
                                $que->where('requested_by',user()->id)->latest();
                            });
                    })
                    ->latest()
                    ->get();
            }
            
        }
        /**
         * @todo this should be in transforer
         *
         * @var Collection
         */
        $builder = new Collection();
        foreach ($query as $sst) {
            // if ($sst->has_bon_documents && $sst->has_insurance_documents) {
            if ($sst->has_bon_documents) {
                $builder->push([
                    'hashslug'          => $sst->hashslug,
                    'no_kontrak'        => $sst->contract_no,
                    'tajuk'             => $sst->acquisition->title,
                    'syarikat_terpilih' => $sst->company->company_name,
                    'tarikh_mula'       => optional($sst->start_working_date)->format(config('datetime.datetimepicker.date')),
                    'date_update'       => optional($sst->updated_at)->format(config('datetime.datetimepicker.date')),
                ]);
            }
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
