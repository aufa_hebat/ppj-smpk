<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\Acquisition\Warning;
use Illuminate\Http\Request;

class WarningController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;

        $sst   = Sst::findByHashSlug($request->hashslug);
        $query = Warning::where('sst_id', $sst->id)->datatable();

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
