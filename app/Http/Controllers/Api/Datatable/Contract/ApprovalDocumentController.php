<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use App\Transformers\Datatable\MediaTransformer as Transformer;
use Illuminate\Http\Request;

class ApprovalDocumentController extends Controller
{
    public function __invoke(Request $request)
    {
        $approval = Approval::withDetails()->latest()->findByHashSlug($request->hashslug);

        return app('datatables')
            ->collection($approval->documents())
            ->setTransformer(new Transformer())
            ->toJson();
    }
}
