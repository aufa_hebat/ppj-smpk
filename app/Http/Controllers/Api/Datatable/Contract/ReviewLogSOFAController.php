<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use App\Models\ReviewLog;

class ReviewLogSOFAController extends Controller
{
    public function __invoke(Request $request)
    {
        $acquisition_id = $request->acquisition_id;
        $query          = ReviewLog::where('acquisition_id', $acquisition_id)->where('type', 'SOFA')->orderBy('id', 'desc')->get();

        $builder = new Collection();
        foreach ($query as $bil => $logreview) {
            $date = '';
            $date1 = '';
            if (! empty($logreview->requested_at)) {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $logreview->requested_at)->format('d/m/Y h:i A');
            }
            if (! empty($logreview->approved_at)) {
                $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $logreview->approved_at)->format('d/m/Y h:i A');
            }

            if($logreview->department == 'BPUB'){
                $penyemak = 'Belum Dipilih oleh BPUB';
            } else {
                $penyemak = 'Belum Dipilih oleh Jabatan Undang-Undang';
            }
            $jabatan = '';
            if(! empty($logreview->requested_by)){
                $penyemak = $logreview->request->name;
                $jabatan  = $logreview->request->department->name;
            }

            $builder->push([
                    'hashslug' => $logreview->acquisition_id,
                    'no'       => $bil + 1,
                    'penyemak' => $penyemak,
                    'jabatan'  => $jabatan,
                    'semakan'  => $logreview->remarks,
                    'tarikh'   => $date1,
                    'tarikh1'   => $date,
                    'status'   => $logreview->reviews_status,
                ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
