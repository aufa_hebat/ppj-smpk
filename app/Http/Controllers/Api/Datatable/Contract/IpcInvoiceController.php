<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Ipc;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class IpcInvoiceController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;
        $query    = Ipc::whereHas('sst', function ($query) use ($hashslug) {
            $query->where('hashslug', $hashslug)->latest();
        })->get();

        $builder = new Collection();
        if (! empty($query)) {
            foreach ($query as $k => $row) {
                $status_remark = '';
                if (1 == $row->status) {
                    $status_remark = 'Draf';
                } elseif (2 == $row->status) {
                    $status_remark = 'Semakan';
                } elseif (3 == $row->status) {
                    $status_remark = 'Sedang diproses di SAP';
                } elseif (4 == $row->status) {
                    $status_remark = 'Sudah Dibayar';
                }

                $builder->push([
                    'id'            => $row->id,
                    'running_no'    => $k + 1,
                    'hashslug'      => $row->hashslug,
                    'ipc_no'        => $row->ipc_no ?? '0',
                    'status'        => $status_remark,
                    'short_text'    => $row->short_text,
                    'demand_amount' => (! empty($row->demand_amount)) ? money()->toCommon($row->demand_amount, 2) : '0',
                ]);
            }
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
