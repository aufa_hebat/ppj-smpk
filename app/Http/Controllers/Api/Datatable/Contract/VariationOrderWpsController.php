<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\VO\VO;
use App\Transformers\Datatable\VOTransformer as Transformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class VariationOrderWpsController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;

        if (user()->hasAnyRole(['urusetia'])) {
            $query    = VO::whereHas('sst', function ($query) use ($hashslug) {
                $query->where('hashslug', $hashslug)->latest();
            })->where('status', 1)->get();
        }else{
            $query    = VO::whereHas('sst', function ($query) use ($hashslug) {
                $query->where('hashslug', $hashslug)->latest();
            })->where('type', 'WPS')->get();
        }

        $builder = new Collection();
        foreach ($query as $k => $row) {
            $status = '';
            $statusDetail = '';
            if('' == $row->status){
                $status = '';
                $statusDetail = 'Draft';
            }else{
                $status = 'none';
                if($row->status == '9'){
                    $statusDetail = 'Semakan';
                }else if($row->status == '1'){
                    $statusDetail = 'Keputusan Mesyuarat';
                }else if($row->status == '2'){
                    $statusDetail = 'Selesai';
                }
            }            

            $builder->push([
                'id'                => $row->id,
                'running_no'        => $k + 1,
                'hashslug'          => $row->hashslug,
                'no_kontrak'        => $row->sst->contract_no,
                'tajuk'             => $row->sst->acquisition->title,
                'syarikat_terpilih' => $row->sst->company->company_name,
                'ppk_no'               => $row->no ?? '0',
                'display'           => $status,
                'status_detail'      => $statusDetail,
                'created_at'        => $row->created_at->format('d/m/Y'),
                'updated_at'        => $row->updated_at->format('d/m/Y'),
                'amount'            => money()->toCommon($row->vo_elements->where('deleted_at', null)->pluck('net_amount')->sum(), 2),
            ]);
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
