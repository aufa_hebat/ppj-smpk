<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Models\Acquisition\AppointedCompany;
use App\Transformers\Datatable\AppointedCompanyTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ExtensionController extends Controller
{
    public function __invoke(Request $request)
    {

        if (user()->hasAnyRole(['developer', 'administrator'])) {

            $query = AppointedCompany::withCount(['eot' => function ($query) {
                $query->doesntHave('eot_approve');
            }])->where('offered_price','>=','20000000')->whereHas('acquisition.sst', function ($query) {
                $query->whereDate('start_working_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_working_date', '>=', Carbon::now()->format('Y-m-d'));
            })->orderBy('eot_count', 'desc')->get();

        } elseif(user()->hasAnyRole('penyedia')) {

            $query = AppointedCompany::whereHas('acquisition', function ($query) {
                $query->where('user_id' , user()->id);
            })->withCount(['eot' => function ($query) {
                $query->doesntHave('eot_approve');
            }])->where('offered_price','>=','20000000')->whereHas('acquisition.sst', function ($query) {
                $query->whereDate('start_working_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_working_date', '>=', Carbon::now()->format('Y-m-d'));
            })->orderBy('eot_count', 'desc')->get();

        } elseif(user()->hasAnyRole('urusetia')) {

            $query = AppointedCompany::whereHas('eot', function ($query) {
                $query->where('status' , 3);
            })->withCount(['eot' => function ($query) {
                $query->doesntHave('eot_approve');
            }])->where('offered_price','>=','20000000')->whereHas('acquisition.sst', function ($query) {
                $query->whereDate('start_working_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_working_date', '>=', Carbon::now()->format('Y-m-d'));
            })->orderBy('eot_count', 'desc')->get();

        } elseif(user()->hasAnyRole('pengesah')) {

            $query = AppointedCompany::whereHas('acquisition', function ($query) {
                $query->where([
                                ['department_id',user()->department_id],
                                ['executor_department_id',user()->executor_department_id],
                            ]);
            })->withCount(['eot' => function ($query) {
                $query->doesntHave('eot_approve');
            }])->where('offered_price','>=','20000000')->whereHas('acquisition.sst', function ($query) {
                $query->whereDate('start_working_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_working_date', '>=', Carbon::now()->format('Y-m-d'));
            })->orderBy('eot_count', 'desc')->get();

        } else {

            $query = AppointedCompany::whereHas('acquisition', function ($query) {
                $query->where([
                                ['department_id',user()->department_id],
                                ['executor_department_id',user()->executor_department_id],
                                ['section_id',user()->section_id],
                            ]);
            })->withCount(['eot' => function ($query) {
                $query->doesntHave('eot_approve');
            }])->where('offered_price','>=','20000000')->whereHas('acquisition.sst', function ($query) {
                $query->whereDate('start_working_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('end_working_date', '>=', Carbon::now()->format('Y-m-d'));
            })->orderBy('eot_count', 'desc')->get();

        }

        $builder = new Collection();
        foreach ($query as $k => $row) {
            $type2first = '';   //kerja
            $type2firstadd = '';   //kerja
            $edit = ''; //kerja
            $type2next = '';    //bekalan & perkhidmatan
            $typeother = '';    //bekalan & perkhidmatan
            $edit2 = ''; //bekalan & perkhidmatan
            if(2 == $row->acquisition->approval->acquisition_type_id){
                $typeother = 'none';
                $type2next = 'none';
                $edit2 = 'none';
                if($row->eot->pluck('status')->last() == ''){
                    if( (!empty( $row->eot->pluck('eot_approve')->last() ) && null != $row->eot->pluck('eot_approve')->last()->approval) ) {

                    } elseif(empty($row->eot->first())) {

                    } else {
                        $type2firstadd = 'none';
                    }
                } else {
                    $edit = 'none';
                    if( (!empty( $row->eot->pluck('eot_approve')->last() ) && null != $row->eot->pluck('eot_approve')->last()->approval) ) {

                    } elseif(empty($row->eot->first())) {

                    } else {
                        $type2firstadd = 'none';
                    }
                }
                
            }elseif(1 == $row->acquisition->approval->acquisition_type_id || 3 == $row->acquisition->approval->acquisition_type_id || 4 == $row->acquisition->approval->acquisition_type_id){
                if($row->eot->pluck('status')->last() == ''){
                    if(!empty($row->eot->first())){
                        $type2next = 'none';
                        $type2first = 'none';
                        $type2firstadd = 'none';
                        $edit = 'none';
                    } else {
                        $type2first = 'none';
                        $type2firstadd = 'none';
                        $typeother = 'none';
                        $edit = 'none';
                    }
                } else {
                    $edit = 'none';
                    $edit2 = 'none';
                    if(!empty($row->eot->first())){
                        $type2next = 'none';
                        $type2first = 'none';
                        $type2firstadd = 'none';
                    } else {
                        $type2first = 'none';
                        $type2firstadd = 'none';
                        $typeother = 'none';
                    }
                }
            }

            $builder->push([
                'hashslug'          => $row->hashslug,
                'eot_hashslug'      => $row->eot->pluck('hashslug'),
                'acquisition_title' => $row->acquisition->title,
                'company_name'      => $row->company->company_name,
                'reference'         => $row->acquisition->reference,
                'created_at'        => optional($row->created_at)->format(config('datetime.datetimepicker.date')),
                'display'           => $type2first,
                'display2'          => $typeother,
                'display3'          => $type2next,
                'display4'          => $type2firstadd,
                'display5'          => $edit,
                'display6'          => $edit2,
            ]);
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
