<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\AppointedCompany;
use Illuminate\Http\Request;

class BonController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = AppointedCompany::with('acquisition', 'company')->select('appointed_company.*')->latest();

        return app('datatables')
            ->eloquent($query)
            ->toJson();
    }
}
