<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use App\Transformers\Datatable\BudgetCodeTransformer as Transformer;
use Illuminate\Http\Request;

class BudgetCodeController extends Controller
{
    public function __invoke(Request $request)
    {
        $approval = Approval::withDetails()->latest()->findByHashSlug($request->hashslug);

        return app('datatables')
            ->collection($approval->financials)
            ->setTransformer(new Transformer())
            ->toJson();
    }
}
