<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Approval;
use App\Transformers\Datatable\AcquisitionApprovalTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PreController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = [];
        
        //DIN currentRole , please different by ketuajabatan , ketuabahagian , dan normal
        
        
        
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                $query = Approval::where('deleted_at',null)->latest()->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                $query = Approval::where('department_id', user()->department_id)
                        ->where('deleted_at',null)->latest()->get();
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                $query = Approval::where('department_id', user()->department_id)
                        ->where('executor_department_id','=',user()->executor_department_id)
                        ->where('deleted_at',null)->latest()->get();
            }else if(user()->current_role_login == 'penyedia'){   
                //user biasa , section
                $query = Approval::where('user_id', user()->id)
                        ->where('deleted_at',null)->latest()->get();
            }
            else {   
                //user biasa , section
                $query = Approval::where('department_id', user()->department_id)
                        ->where('executor_department_id','=',user()->executor_department_id)
                        ->where('section_id','=',user()->section_id)
                        ->where('deleted_at',null)->latest()->get();
            }
        }
        // return app('datatables')
        //     ->eloquent($query)
        //     ->setTransformer(new AcquisitionApprovalTransformer())
        //     ->toJson();

        // $query = Approval::where('user_id', user()->id)->get();

        $builder = new Collection();
        foreach ($query as $approval) {
            $builder->push([
                        'hashslug'          => $approval->hashslug,
                        'reference'         => $approval->reference,
                        'title'             => $approval->title,
                        'created_at'        => $approval->created_at->format('d/m/Y'),
                        'updated_at'        => $approval->updated_at->format('d/m/Y'),
                        'acquisition_total' => $approval->acquisitions->count(),
                        'display'           => '',
                        'department'      => $approval->department->name,
                    ]);
        }

        return app('datatables')
                    ->collection($builder)
                    ->toJson();
    }
}
