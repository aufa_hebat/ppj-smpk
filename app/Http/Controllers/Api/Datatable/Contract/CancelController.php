<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Transformers\Datatable\AcquisitionTransformer;
use Illuminate\Http\Request;

class CancelController extends Controller
{
    public function __invoke(Request $request)
    {
        $acq = Acquisition::get();

        $query = Acquisition::datatable()->where([
            ['status_id', 2],
        ])->latest();

        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new AcquisitionTransformer())
            ->toJson();
    }
}
