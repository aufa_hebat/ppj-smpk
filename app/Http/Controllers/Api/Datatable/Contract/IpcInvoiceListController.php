<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\IpcInvoice;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class IpcInvoiceListController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;
        $query    = IpcInvoice::whereHas('ipc', function ($query) use ($hashslug) {
            $query->where('hashslug', $hashslug)->latest();
        })->get();

        $builder = new Collection();
        if (! empty($query)) {
            foreach ($query as $k => $row) {
                $comp_name = Company::where('id', $row->company_id)->pluck('company_name')->first();
                $builder->push([
                    'id'         => $row->id,
                    'running_no' => $k + 1,
                    'hashslug'   => $row->hashslug,
                    'invoice_no' => $row->invoice_no ?? '0',
                    'company'    => $comp_name,
                    'invoice'    => (! empty($row->invoice)) ? money()->toCommon($row->invoice, 2) : '0',
                ]);
            }
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
