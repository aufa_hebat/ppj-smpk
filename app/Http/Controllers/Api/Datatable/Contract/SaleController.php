<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Transformers\Datatable\SaleTransformer;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function __invoke(Request $request)
    {
        $ic_number = $request->ic_number;
        $query     = Company::whereHas('owners', function ($query) use ($hashslug) {
            $query->where('ic_number', $ic_number)->latest();
        })->datatable();

        return app('datatables')
//            ->eloquent(Sale::datatable()->where('sales_status', true))
//            ->eloquent(Acquisition::datatable())
            ->eloquent($query)
            ->setTransformer(new SaleTransformer())
            ->toJson();
    }
}
