<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Transformers\Datatable\AcquisitionTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

class AcquisitionController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = [];
        //() Add to filter
        $tag = $request->tag;
        
         //$tag = 'contractsNotAwardYet';
        
         if($tag == 'contractsNotAwardYet'){
            
             $ProjectProgressProcessor = new \App\Processors\Charts\ProjectProgressProcessorPersonal();
        
             $query = $ProjectProgressProcessor->contractsNotAwardYet();
             //return null;
         }
         else{
        
        //$acq = Acquisition::get();
        
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer'){
                $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at',null)->latest();
                })->where([
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->datatable()->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at',null)->where('department_id', user()->department->id)->latest();
                })->where([
                    ['department_id',user()->department_id],
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->datatable()->get();
            }
            else if(user()->current_role_login == 'penyedia'){
                //user penyedia
                $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at',null)->where('department_id', user()->department->id)->latest();
                })->where([
                    ['user_id',user()->id],
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->datatable()->get();
            }else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at',null)->where('department_id', user()->department->id)->latest();
                })->where([
                    ['department_id',user()->department_id],
                    ['executor_department_id',user()->executor_department_id],
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->datatable()->get();
            }
            else {   
                //user biasa , section
                $query = Acquisition::whereHas('approval', function ($query) {
                    $query->where('deleted_at',null)->latest();
                })->where([
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->where(function($q) {
                    $q->where([
                        ['department_id',user()->department_id],
                        ['executor_department_id',user()->executor_department_id],
                        ['section_id',user()->section_id],
                    ])->orWhereHas('reviews', function ($que) {
                        $que->where('requested_by',user()->id)->where('type','Acquisitions')->latest();
                    });
                })
                ->datatable()->get();
            }
        }
         }

        $builder = new Collection();
        foreach ($query as $acquisition) {
            $status = '';
            // if ('' == $acquisition->status_task || 'Draft Penyedia' == $acquisition->status_task) {
            //     $status = '';
            // } elseif ('Semakan Dokumen Perolehan' == $acquisition->status_task) {
            //     if(user()->current_role_login == 'pengesah'){
            //         $status = '';
            //     } else {
            //         $status = 'none';
            //     }
            // } else {
            //     $status = 'none';
            // }

            if ('' == $acquisition->status_task || 'Draft Penyedia' == $acquisition->status_task) {
                $status = '';
            } else {
                $status = 'none';
            }
            $status_a = '';
            if ('' == $acquisition->status_task || 'Draft' == $acquisition->status_task ||
                'Semakan' == $acquisition->status_task || 'Taklimat' == $acquisition->status_task || 
                'Cetak Notis' == $acquisition->status_task || 'Jualan' == $acquisition->status_task ||
                'Lawatan Tapak' == $acquisition->status_task || 'Buka Peti' == $acquisition->status_task) {
                $status_a = '';
            }else{
                $status_a = 'none';
            }
            
            $builder->push([
                        'hashslug'   => $acquisition->hashslug,
                        'reference'  => $acquisition->reference,
                        'created_at' => $acquisition->created_at->format('d/m/Y'),
                        'updated_at' => $acquisition->updated_at->format('d/m/Y'),
                        'display'    => $status,
                        'display_a'  => $status_a,
                        'title'      => $acquisition->title,
                        'status'     => $acquisition->status_task,
                        'department' => $acquisition->approval->department->name,
                    ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();

         /*
        $builder = new Collection();
        
        
        foreach ($query as $acquisition) {
            $status = '';
//            if ('' == $acquisition->status_task || 'Draft Penyedia' == $acquisition->status_task) {
//                $status = '';
//            } else {
//                $status = 'none';
//            }
            
            //update status based on row , need to check 
//            $date_now = Carbon::now()->format('Y-m-d');
//            $date_closed = $acquisition->closed_at;
//            $isDateValidForOpenBox = false;
//            if ((!is_null($date_closed)) && $date_closed <= $date_now) {
//                 $isDateValidForOpenBox = true;
//            }
            
            
//            
//            if($isDateValidForOpenBox){
//                $status = 'BUKA PETI';
//            }else if($acquisition->status_task == 'Semakan Dokumen Perolehan'){
//                $status = 'DOKUMEN PEROLEHAN';
//            }else{
//                $status = 'UNDIFined';
//            }
            
            //DIN status :
            //$status = '-';
            

            $builder->push([
                        'hashslug'   => $acquisition->hashslug,
                        'reference'  => $acquisition->reference,
                        'title'      => $acquisition->title,
                        'created_at' => $acquisition->created_at->format('d/m/Y'),
                        'updated_at' => $acquisition->updated_at->format('d/m/Y'),
                        'display'    => $acquisition->status_task,
                        'department' => $acquisition->approval->department->name
                    ]);
        }
          * 
          */

         //return app('datatables')
            // ->eloquent($query)
             //->setTransformer(new AcquisitionTransformer())
             //->toJson();
        
        //  return app('datatables')
        // ->collection($query)
        // ->setTransformer(new AcquisitionTransformer())
        // ->toJson();

//        return app('datatables')
//            ->collection($builder)
//           ->toJson();
    }
}
