<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\VO\PPJHK\Ppjhk;
use App\Transformers\Datatable\VOTransformer as Transformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class VariationOrderPpjhkController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;
        $query    = Ppjhk::whereHas('sst', function ($query) use ($hashslug) {
            $query->where('hashslug', $hashslug)->latest();
        })->get();

        $builder = new Collection();
        foreach ($query as $k => $row) {

            $status = '';
            $statusDetail = '';
            if('' == $row->status){
                $status = '';
                $statusDetail = 'Draf';
            }else{
                $status = 'none';
                if($row->status == '9'){
                    $statusDetail = 'Semakan';
                }else if($row->status == '1'){
                    $statusDetail = 'Selesai';
                }
            }

            $builder->push([
                'id'                => $row->id,
                'running_no'        => $k + 1,
                'hashslug'          => $row->hashslug,
                'no_kontrak'        => $row->sst->contract_no,
                'tajuk'             => $row->sst->acquisition->title,
                'syarikat_terpilih' => $row->sst->company->company_name,
                'ppjhk_no'               => $row->no ?? '0',
                'display'           => $status,
                'status_detail'      => $statusDetail,
            ]);
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
