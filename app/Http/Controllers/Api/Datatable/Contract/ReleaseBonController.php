<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use Illuminate\Http\Request;
use App\Models\Acquisition\Bon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ReleaseBonController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Bon::where('sst_id',$request->sst_id)->get();

        $builder = new Collection();
        foreach ($query as $k => $row) {

            $builder->push([
                'hashslug'          => $row->hashslug,
                'bil'          		=> $k + 1,
                'bank_no' 			=> $row->bank_no,
                'bank_start_date'   => Carbon::createFromFormat('Y-m-d', $row->bank_start_date)->format('d/m/Y'),
                'bank_expired_date' => Carbon::createFromFormat('Y-m-d', $row->bank_expired_date)->format('d/m/Y'),
                'money_amount'      => money()->toHuman($row->money_amount) . ' ',
            ]);
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
