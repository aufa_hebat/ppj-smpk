<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Transformers\Datatable\SstTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TerminationController extends Controller
{
    public function __invoke(Request $request)
    {
        return app('datatables')
            ->collection(Sst::withDetails()->whereDate('termination_at', '<', Carbon::now()->format('Y-m-d') . '%')->latest()->get())
            ->setTransformer(new SstTransformer())
            ->toJson();
    }
}
