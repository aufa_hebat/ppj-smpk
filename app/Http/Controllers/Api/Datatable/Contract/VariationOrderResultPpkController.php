<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\VO\VO;
use App\Transformers\Datatable\VOTransformer as Transformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class VariationOrderResultPpkController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;
        $query    = VO::whereHas('sst', function ($query) use ($hashslug) {
            $query->where('hashslug', $hashslug)->latest();
        })->where('type', '!=', 'WPS')->whereIn('status', ['1', '2'])->get();   //1 - InProgress, 2- Completed]

        $builder = new Collection();
        foreach ($query as $k => $row) {

            $status = '';
            $statusDetail = '';
            if('1' == $row->status){
                $status = '';
                $statusDetail = 'Keputusan Mesyuarat';
            }else{
                $status = 'none';
                $statusDetail = 'Selesai';
            }

            $builder->push([
                'id'                => $row->id,
                'running_no'        => $k + 1,
                'hashslug'          => $row->hashslug,
                'no_kontrak'        => $row->sst->contract_no,
                'tajuk'             => $row->sst->acquisition->title,
                'syarikat_terpilih' => $row->sst->company->company_name,
                'ppk_no'               => $row->no ?? '0',
                'display'           => $status,
                'status_detail'      => $statusDetail,
            ]);
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
