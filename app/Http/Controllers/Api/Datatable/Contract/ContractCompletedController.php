<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ContractCompletedController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Sst::withDetails()
            ->whereDate('sofa_signature_date', '<=', Carbon::now()->format('Y-m-d') . '%')
            ->get();

        $builder = new Collection();
        foreach ($query as $row) {
            $builder->push([
                'hashslug'          => $row->hashslug,
                'no_kontrak'        => $row->acquisition->reference,
                'tajuk'             => $row->acquisition->title,
                'syarikat_terpilih' => $row->company->company_name,
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
