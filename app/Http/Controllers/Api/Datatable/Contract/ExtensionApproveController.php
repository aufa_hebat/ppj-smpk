<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Eot;
use App\Transformers\Datatable\EotTransformer;
use Illuminate\Http\Request;

class ExtensionApproveController extends Controller
{
    public function __invoke(Request $request)
    {
        return app('datatables')
            ->collection(Eot::where('appointed_company_id', '=', $request->appointed_company_id)->get())
            ->setTransformer(new EotTransformer())
            ->toJson();
    }
}
