<?php

namespace App\Http\Controllers\Api\Datatable\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use Illuminate\Http\Request;

class MonitoringController extends Controller
{
    
    //TODO remove terminate and batal from list
    
    public function __invoke(Request $request)
    {
        
        //TODO DIN - Query Based On Hirearchy
         if(!empty(user()->department_id) && !empty(user()->executor_department_id)){          
            //if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                //$query = Approval::where('deleted_at',null)->latest()->get();
                // $query = Sst::all();
                 $query = \App\Models\Acquisition\Sst::
                          whereHas('acquisition',function($q) {
                              $q->where('status_id', 1); 
                           })->get();
                 
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('deleted_at',null)->latest()->get();
                $query = \App\Models\Acquisition\Sst::
                          where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('status_id', 1); 
                           })->get();
                      
               
                
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                             
                //user ketua bahagian
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                       // ->where('deleted_at',null)->latest()->get();
                $query = \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id)
                                      ->where('status_id', 1); 
                           })->get();
                       
            }
            else {   
                //user biasa , section
                //$query = Approval::where('department_id', user()->department_id)
                        //->where('executor_department_id','=',user()->executor_department_id)
                        //->where('section_id','=',user()->section_id)
                        //->where('deleted_at',null)->latest()->get();
                
                $query = \App\Models\Acquisition\Sst::
                    where('department_id', user()->department_id)
                        -> whereHas('acquisition',function($q) {
                              $q->where('executor_department_id', user()->executor_department_id)
                                ->where('section_id', user()->section_id)
                                      ->where('status_id', 1);
                           })->get();
                      
            }
        }else{
            return null;
        }
        
        
        //$query = Sst::all();
        
        

        $builder = collect();
        foreach ($query as $row) {
            $builder->push([
                'hashslug'          => $row->hashslug,
                'no_kontrak'        => $row->acquisition->reference,
                'tajuk'             => $row->acquisition->title,
                'syarikat_terpilih' => $row->company->company_name,
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
