<?php

namespace App\Http\Controllers\Api\Datatable\Contract\Acquisition;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class LawReviewController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Review::where('status', 'S6')->distinct()->get(['acquisition_id']);

        $builder = new Collection();
        foreach ($query as $review) {
            $progress = '';
            if (0 == $review->acquisition->reviews->where('status', 'S6')->sortbydesc('id')->first()->progress) {
                $progress = 'Belum Serah Tugas';
                $rev      = '-';
            } elseif (1 == $review->acquisition->reviews->where('status', 'S6')->sortbydesc('id')->first()->progress) {
                $progress = 'Selesai Serah Tugas';
                $rev      = $review->acquisition->reviews->where('status', 'S6')->sortbydesc('id')->first()->lawtask->name;
            }

            $builder->push([
                    'hashslug'     => $review->acquisition_id,
                    'no_perolehan' => $review->acquisition->reference,
                    'tajuk_perolehan' => $review->acquisition->title,
                    'progress'     => $progress,
                    'assign_to'    => $rev,
                ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
