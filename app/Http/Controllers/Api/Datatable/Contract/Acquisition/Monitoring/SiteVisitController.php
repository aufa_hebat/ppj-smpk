<?php

namespace App\Http\Controllers\Api\Datatable\Contract\Acquisition\Monitoring;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Monitoring\SiteVisit;
use App\Transformers\Datatable\MonitoringSiteVisitTransformer as Transformer;
use Illuminate\Http\Request;

class SiteVisitController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;

        $query = SiteVisit::query()
            ->whereHas('sst', function ($query) use ($hashslug) {
                $query->where('hashslug', $hashslug);
            })
            ->orderby('visit_date', 'asc')        
            ->datatable();

        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new Transformer())
            ->toJson();
    }
}
