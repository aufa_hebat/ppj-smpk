<?php

namespace App\Http\Controllers\Api\Datatable\Contract\Acquisition\Monitoring;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Monitoring\PaymentMonitor;
use App\Transformers\Datatable\PaymentMonitorTransformer as Transformer;
use Illuminate\Http\Request;
use App\Models\Acquisition\Sst;

class PaymentMonitorController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->get('hashslug');

        //find by acquisition id , this id need get from hashlug of        
        //find SST by hashlug
        $sst = Sst::where('hashslug', $hashslug)->first();
        
        $query = PaymentMonitor::
                where('acquisition_id', $sst->acquisition_id)->datatable();

        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new Transformer())
            ->toJson();
    }
}
