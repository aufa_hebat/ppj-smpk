<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ReleaseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $query = [];
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){   
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                        ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->whereHas('acquisition', function ($query) {
                            $query->where('deleted_at',null);
                        })
                        ->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                        ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->whereHas('acquisition', function ($query) {
                            $query->where('department_id', user()->department_id)
                            ->where('deleted_at',null);
                        })
                        ->get();
            }else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                        ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->whereHas('acquisition', function ($query) {
                            $query->where('department_id', user()->department_id)
                            ->where('executor_department_id','=',user()->executor_department_id)
                            ->where('deleted_at',null);
                        })
                        ->get();
            }else {   
                //user biasa , section
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval'])
                        ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->whereHas('acquisition', function ($query) {
                            $query->where('department_id', user()->department_id)
                            ->where('executor_department_id','=',user()->executor_department_id)
                            ->where('section_id','=',user()->section_id)
                            ->where('deleted_at',null);
                        })
                        ->get();
            }
        }
//        $query = Sst::with(['acquisition', 'company', 'acquisition.approval'])
//                        ->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
//                        ->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
//                        ->get();

            $builder = new Collection();
            foreach ($query as $row) {
                $builder->push([
                    'hashslug'          => $row->hashslug,
                    'no_kontrak'        => $row->acquisition->reference,
                    'tajuk'             => $row->acquisition->title,
                    'syarikat_terpilih' => $row->company->company_name,
                ]);
            }

            return app('datatables')
                ->collection($builder)
                ->toJson();
    }
}
