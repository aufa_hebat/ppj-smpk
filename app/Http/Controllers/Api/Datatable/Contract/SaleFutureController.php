<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class SaleFutureController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Acquisition::select('acquisitions.*')->whereHas('approval', function ($query) {
                    $query->where('deleted_at', null)->where('cancelled_at',null);
                })->where('deleted_at',null)->whereDate('start_sale_at', '>', Carbon::now()->format('Y-m-d') . '%')->latest()->get();

        $builder = new Collection();
        foreach ($query as $acq) {
            $builder->push([
                'hashslug'       => $acq->hashslug,
                'reference'      => $acq->reference,
                'title'          => $acq->title,
                'start_sale_at'  => date('d/m/Y', strtotime($acq->start_sale_at)),
                'closed_sale_at' => date('d/m/Y', strtotime($acq->closed_sale_at)),
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
