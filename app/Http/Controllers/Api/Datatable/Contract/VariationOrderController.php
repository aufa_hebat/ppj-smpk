<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Sst;
use App\Models\VO\VO;
use App\Transformers\Datatable\VOTransformer as Transformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class VariationOrderController extends Controller
{
    public function __invoke(Request $request)
    {
       $query = [];
       if(!empty(user()->department_id) && !empty(user()->executor_department_id)){   
           if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer' || user()->current_role_login == 'urusetia'){
               $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                        //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->where(function($query){
                            $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                        })                    
                        ->where(function($query){
                            $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                        }) 
                       ->whereHas('acquisition', function ($query) {
                           $query->where('deleted_at',null);
                       })
                       ->latest()
                       ->get();
           }else if(user()->current_role_login == 'ketuajabatan'){
               //ketua department
               $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                        //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->where(function($query){
                            $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                        })                    
                        ->where(function($query){
                            $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                        }) 
                       ->whereHas('acquisition', function ($query) {
                           $query->where('department_id', user()->department_id)
                           ->where('deleted_at',null);
                       })
                       ->latest()
                       ->get();
           }else if(user()->current_role_login == 'ketuabahagian' && !empty(user()->executor_department_id)){
               //user ketua bahagian
               $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                        //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->where(function($query){
                            $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                        })                    
                        ->where(function($query){
                            $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                        }) 
                       ->whereHas('acquisition', function ($query) {
                           $query->where('department_id', user()->department_id)
                           ->where('executor_department_id','=',user()->executor_department_id)
                           ->where('deleted_at',null);
                       })
                       ->latest()
                       ->get();
            }elseif(user()->current_role_login == 'penyedia') {   
                //user biasa , section
                $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                        //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->where(function($query){
                            $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                        })                    
                        ->where(function($query){
                            $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                        })                    
                        ->whereHas('acquisition', function ($query) {
                            $query->where([
                                ['user_id',user()->id],
                                ['deleted_at',null],
                                ['cancelled_at',null],
                            ]);
                        })->where('user_id',user()->id)
                        ->latest()
                        ->get();                       
           }else {   
               //user biasa , section
               $query = Sst::with(['acquisition', 'company', 'acquisition.approval', 'bon', 'insurance', 'deposit'])
                        //->whereDate('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at')
                        //->whereDate('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date')
                        ->where(function($query){
                            $query->where('termination_at', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('termination_at');
                        })                    
                        ->where(function($query){
                            $query->where('sofa_signature_date', '>=', Carbon::now()->format('Y-m-d') . '%')->orWhereNull('sofa_signature_date');
                        }) 
                        ->whereHas('acquisition', function ($query) {
                            $query->where('deleted_at',null)->where('cancelled_at',null)
                                ->orWhere([
                                    ['department_id',user()->department_id],
                                    ['executor_department_id',user()->executor_department_id],
                                    ['section_id',user()->section_id],
                                ])
                                ->orWhereHas('history_semakan_ipc', function ($que) {
                                $que->where('requested_by',user()->id)->latest();
                       });
               })
               ->latest()
               ->get();
           }
       }


        $builder = new Collection();
        foreach ($query as $row) {
            
            // if ($row->has_bon_documents && $row->has_insurance_documents && $row->acquisition->bqSubItems->pluck('amount_adjust')->sum() > 0) {
            if ($row->has_bon_documents && $row->acquisition->bqSubItems->pluck('amount_adjust')->sum() > 0) {
                $builder->push([
                    'hashslug'          => $row->hashslug,
                    'no_kontrak'        => $row->contract_no,
                    'tajuk'             => $row->acquisition->title,
                    'syarikat_terpilih' => $row->company->company_name,
                ]);
            }
        }
        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
