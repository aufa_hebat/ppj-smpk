<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Transformers\Datatable\AcquisitionTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class DocumentController extends Controller
{
    public function __invoke(Request $request)
    {
        $hashslug = $request->hashslug;
        $query = [];
        if(!empty(user()->department_id) && !empty(user()->executor_department_id)){  
            if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer'){
                $query = Acquisition::whereHas('approval', function ($query) use ($hashslug) {
                    $query->where('hashslug', $hashslug)->where('deleted_at',null)->latest();
                })->where([
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->get();
            }else if(user()->current_role_login == 'ketuajabatan'){
                //ketua department
                $query = Acquisition::whereHas('approval', function ($query) use ($hashslug) {
                    $query->where('hashslug', $hashslug)->where('department_id', user()->department_id)->where('deleted_at',null)->latest();
                })->where([
                    ['department_id',user()->department_id],
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->get();
            }
            else if(user()->current_role_login == 'ketuabahagian'){
                //user ketua bahagian
                $query = Acquisition::whereHas('approval', function ($query) use ($hashslug) {
                    $query->where('hashslug', $hashslug)->where('department_id', user()->department_id)->where('deleted_at',null)->latest();
                })->where([
                    ['department_id',user()->department_id],
                    ['executor_department_id',user()->executor_department_id],
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->get();
            }else if(user()->current_role_login == 'penyedia'){
                //user ketua bahagian
                $query = Acquisition::whereHas('approval', function ($query) use ($hashslug) {
                    $query->where('hashslug', $hashslug)->where('department_id', user()->department_id)->where('deleted_at',null)->latest();
                })->where([
                    ['user_id',user()->id],
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->get();
            }
            else {   
                //user biasa , section
                $query = Acquisition::whereHas('approval', function ($query) use ($hashslug) {
                    $query->where('hashslug', $hashslug)->where('deleted_at',null)->latest();
                })->where([
                    ['deleted_at', null],
                    ['status_id', 1],
                    // ['cancelled_at', null],
                ])->where(function($q) {
                    $q->where([
                        ['department_id',user()->department_id],
                        ['executor_department_id',user()->executor_department_id],
                        ['section_id',user()->section_id],
                    ])->orWhereHas('reviews', function ($que) {
                        $que->where('requested_by',user()->id)->where('type','Acquisitions')->latest();
                    });
                })
                ->get();
            }
        }

        $builder = new Collection();
        foreach ($query as $acquisition) {
            $status = '';
            if ('' == $acquisition->status_task || 'Draft Penyedia' == $acquisition->status_task) {
                $status = '';
            } else {
                $status = 'none';
            }
            $status_a = '';
            if ('' == $acquisition->status_task || 'Draft' == $acquisition->status_task ||
                'Semakan' == $acquisition->status_task || 'Taklimat' == $acquisition->status_task || 
                'Cetak Notis' == $acquisition->status_task || 'Jualan' == $acquisition->status_task ||
                'Lawatan Tapak' == $acquisition->status_task || 'Buka Peti' == $acquisition->status_task) {
                $status_a = '';
            }else{
                $status_a = 'none';
            }

            $builder->push([
                        'hashslug'   => $acquisition->hashslug,
                        'reference'  => $acquisition->reference,
                        'title'      => $acquisition->title,
                        'created_at' => $acquisition->created_at->format('d/m/Y'),
                        'updated_at' => $acquisition->updated_at->format('d/m/Y'),
                        'display'    => $status,
                        'display_a'  => $status_a,
                        'department' => $acquisition->approval->department->name,
                    ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
