<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Eot;
use App\Transformers\Datatable\EotApproveTransformer;
use Illuminate\Http\Request;

class ExtensionEditController extends Controller
{
    public function __invoke(Request $request)
    {
        if (user()->hasAnyRole(['developer', 'administrator', 'urusetia'])) {
            $eot = Eot::where('appointed_company_id', $request->appointed_company_id)->withDetails()->latest();
        } else {
            $eot = Eot::where('appointed_company_id', $request->appointed_company_id)->withDetails()->latest();
        }

        return app('datatables')
            ->eloquent($eot)
            ->setTransformer(new EotApproveTransformer())
            ->toJson();
    }
}
