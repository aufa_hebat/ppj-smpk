<?php

namespace App\Http\Controllers\Api\Datatable\Contract;

use App\Http\Controllers\Controller;
use App\Models\Acquisition\Acquisition;
use App\Transformers\Datatable\BoxTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BoxController extends Controller
{
    public function __invoke(Request $request)
    {
        //$query = Acquisition::where('cancelled_at',null)->where('deleted_at',null)->whereDate('closed_at','<=',Carbon::now()->format('Y-m-d'))->where('status_task','Buka Peti')->with('approval')->with('evaluation')->select('acquisitions.*')->latest();

        if(user()->currentRole() == 'administrator'){
            //all()
            $query = Acquisition::select('acquisitions.*')->latest();
        }else{
        
        //Remove Date closed_at
        $query = Acquisition::where('cancelled_at',null)->where('deleted_at',null)->where('status_task','Buka Peti')->with('approval')->with('evaluation')->select('acquisitions.*')->latest();
        
        }
        return app('datatables')
            ->eloquent($query)
            ->setTransformer(new BoxTransformer())
            ->toJson();
    }
}
