<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Transformers\Datatable\TaskTransformer as Transformer;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __invoke(Request $request)
    {
        return app('datatables')
            ->eloquent(Task::datatable())
            ->setTransformer(new Transformer())
            ->toJson();
    }
}
