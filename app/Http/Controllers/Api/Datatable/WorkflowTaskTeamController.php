<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use App\Models\WorkflowTask;
// use App\Transformers\Datatable\WorkflowTaskTransformer as Transformer;
use Illuminate\Http\Request;
use App\Models\Acquisition\Ipc;
use Illuminate\Support\Collection;

/**
 * Description of WorkflowTaskTeamController.
 *
 * @author MITSB
 */
class WorkflowTaskTeamController extends Controller
{
    public function __invoke(Request $request)
    {

        // Penanda Semakan
        // S1 = Semakan Penyemak JP
        // S2 = Semakan Pengesah JP
        // N = Penyerahan Tugasan
        // S3 = Semakan Penyemak 1 BPUB
        // S4 = Semakan Penyemak 2 BPUB
        // S5 = Semakan Pengesah BPUB
        // S6 = Semakan Penyemak Undang-undang
        // S7 = Semakan Pengesah Undang-undang
        // S8 = Semakan Penyemak Kewangan
        // S9 = Semakan Pengesah Kewangan

        //this query for not claimed task only means it is in bucket
        //get current login
        $currentUserId = user()->id;

        //set not claimed
        $isClaimed = false;

        //find team by id
        $workflowTeamTask = \App\Models\WorkflowTeamTask::where('user_id', $currentUserId);

        //pluck is get array of team_id
        $teams = $workflowTeamTask->pluck('workflow_team_id');

        //query by team
        $query = [];
        if(user()->current_role_login == 'urusetia'){

            $query = WorkflowTask::where('is_claimed', $isClaimed)
            ->whereIn('workflow_team_id', $teams)->where('flow_location','N')
            ->orWhere('flow_location','Buka Peti')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)
            ->orWhere('flow_location','Keputusan')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)
            ->orWhere('flow_location','EOT')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)
            ->latest('updated_at')->get();

        } elseif(user()->current_role_login == 'pengesah'){

            // $workflow = WorkflowTask::whereIn('workflow_team_id', $teams)->where('flow_location','IPC')->orwhere('flow_location','S4')->orwhere('flow_location','S5')->get();
            // if(!empty($workflow)){
            //     $ipc = IPC::where('id',$workflow->pluck('ipc_id'))->first();
            
            //     if(!empty($ipc)){
            //         $text =  'Semakan IPC '.$ipc->ipc_no.' : Sila beri ulasan untuk proses pengesahan.';
            //     } else {
            //         $text = '';
            //     }
            // }

            $query = WorkflowTask::where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)->where('flow_location','S5')
            ->orWhere('flow_location','S4')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)
            ->orWhere('flow_location','IPC')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)
            ->orWhere('flow_location','PPK')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)
            ->orWhere('flow_location','PPJHK')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)->latest('updated_at')->get();

        } elseif(user()->current_role_login == 'penyemak'){

            // $workflow = WorkflowTask::whereIn('workflow_team_id', $teams)->where('flow_location','IPC')->get();
            // if(!empty($workflow)){
            //     $ipc = IPC::where('id',$workflow->pluck('ipc_id'))->first();
            //     if(!empty($ipc)){
            //         $text =  'Semakan IPC '.$ipc->ipc_no.' : Sila beri ulasan untuk proses penyemakan (Khas untuk Pegawai Kewangan).';
            //     } else {
            //         $text = '';
            //     }
            // }
            
            $query = WorkflowTask::where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)->where('flow_location','S8')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)->latest('updated_at')->get();

        } elseif(user()->current_role_login == 'urusetiavo'){

            $query = WorkflowTask::where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)->where('flow_location','PPK')->where('is_claimed', $isClaimed)->whereIn('workflow_team_id', $teams)->latest('updated_at')->get();
            
        }

        $builder = new Collection();
        foreach ($query as $task) {
            $builder->push([
                'id'  => $task->id,
                'flow_name' => $task->flow_name,
                'url'       => $task->url,
                'flow_desc' => $task->flow_desc,
            ]);
        }

        return app('datatables')
            ->collection($builder)
            ->toJson();
    }
}
