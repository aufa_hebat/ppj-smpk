<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SyncController extends Controller
{
    public function __invoke(Request $request)
    {
        \Artisan::call('sync:all');

        return response()->api();
    }
}
