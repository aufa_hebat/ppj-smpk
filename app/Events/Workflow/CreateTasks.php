<?php

namespace App\Events\Workflow;

use App\Models\Acquisition\Acquisition;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreateTasks
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Acquisition.
     *
     * @var \App\Models\Acquisition\Acquisition
     */
    public $acquisition;

    /**
     * Field name to create task.
     *
     * @var string
     */
    public $workflow_field;

    /**
     * Field value to fetch.
     *
     * @var string
     */
    public $workflow_value;

    /**
     * Create a new event instance.
     */
    public function __construct(Acquisition $acquisition, $workflow_field, $workflow_value)
    {
        $this->acquisition    = $acquisition;
        $this->workflow_field = $workflow_field;
        $this->workflow_value = $workflow_value;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
