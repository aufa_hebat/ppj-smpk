<?php

namespace App\Utilities;

use App\Models\Acquisition\Approval;
use App\Models\Document;
use App\Traits\HasMakeStatic;
use Illuminate\Support\Facades\Storage;
use App\Models\Acquisition\Review;

class Common
{
    use HasMakeStatic;

    public function upload_document(array $upload)
    {
        $destinationPath = str_replace('/', '-', $upload['acquisition']->approval->reference) . '/' .
                str_replace('/', '-', $upload['acquisition']->reference) . '/' .
                $upload['doc_type']; // upload folder in storage directory

        $doc = Document::where('document_id', $upload['doc_id'])->where('document_types', $upload['doc_type']);

        if (! empty($upload['request_hDocumentId']) && count($upload['request_hDocumentId']) > 0) {
            $deleteStorage = $doc->whereNotIn('id', $upload['request_hDocumentId']);
            foreach ($deleteStorage->get() as $file) {
                Storage::disk('public')->Delete($destinationPath . '/' . $file->document_name);
            }
            $doc->delete();
        } else {
            Storage::disk('public')->deleteDirectory($destinationPath);
            $doc->delete();
        }

        if ($upload['request_hasFile']) {
            if(is_array($upload['request_file'])){
                foreach ($upload['request_file'] as $file) {
                    $file->move(storage_path('app/public/' . $destinationPath), $file->getClientOriginalName());
                    Document::Create(
                            [
                                'document_types' => $upload['doc_type'],
                                'document_id'    => $upload['doc_id'],
                                'document_path'  => $destinationPath,
                                'document_name'  => $file->getClientOriginalName(),
                                'mime'           => $file->getClientMimeType(),
                            ]);
                }
            }else{
                $upload['request_file']->move(storage_path('app/public/' . $destinationPath), $upload['request_file']->getClientOriginalName());
                Document::Create(
                        [
                            'document_types' => $upload['doc_type'],
                            'document_id'    => $upload['doc_id'],
                            'document_path'  => $destinationPath,
                            'document_name'  => $upload['request_file']->getClientOriginalName(),
                            'mime'           => $upload['request_file']->getClientMimeType(),
                        ]);
            }
            
        }
    }

    public function upload_document_aprvl(array $upload)
    {
        $destinationPath = str_replace('/', '-', $upload['acquisition']->reference) . '/' .
                str_replace('/', '-', $upload['acquisition']->reference) . '/' .
                $upload['doc_type']; // upload folder in storage directory

        echo $destinationPath;

        $doc = Document::where('document_id', $upload['doc_id'])->where('document_types', $upload['doc_type']);

        if (! empty($upload['request_hDocumentId']) && count($upload['request_hDocumentId']) > 0) {
            $deleteStorage = $doc->whereNotIn('id', $upload['request_hDocumentId']);
            foreach ($deleteStorage->get() as $file) {
                Storage::disk('public')->Delete($destinationPath . '/' . $file->document_name);
            }
            $doc->delete();
        } else {
            Storage::disk('public')->deleteDirectory($destinationPath);
            $doc->delete();
        }

        if ($upload['request_hasFile']) {
            foreach ($upload['request_file'] as $file) {
                $file->move(storage_path('app/public/' . $destinationPath), $file->getClientOriginalName());
                Document::Create(
                        [
                            'document_types' => $upload['doc_type'],
                            'document_id'    => $upload['doc_id'],
                            'document_path'  => $destinationPath,
                            'document_name'  => $file->getClientOriginalName(),
                            'mime'           => $file->getClientMimeType(),
                        ]);
            }
        }
    }

    public function getKelayakanCIDB(int $approval_id)
    {
        $approval = Approval::find($approval_id);

        $cidb             = '';
        $atatusGrade      = '';
        $atatusCategoryB  = '';
        $atatusCategoryCE = '';
        $atatusCategoryM  = '';
        $atatusCategoryE  = '';
        $atatusCategoryF  = '';
        $atatusKhususB    = '';
        $atatusKhususCE   = '';
        $atatusKhususM    = '';
        $atatusKhususE    = '';
        $atatusKhususF    = '';
        $catB             = '';
        $catM             = '';
        $catE             = '';
        $catCE            = '';
        $catF            = '';

        if (! empty($approval) && $approval->cidbQualifications->count() > 0) {
            foreach ($approval->cidbQualifications as $index => $row) {
                if ('grade' == $row->code->type) {
                    if (0 == $index) {
                        $cidb = 'Gred';
                    }

                    $cidb = $cidb . ' ' . $atatusGrade . ' ' . $row->code->code;

                    if (1 == $row->status) {
                        $atatusGrade = 'Dan';
                    } else {
                        $atatusGrade = 'Atau';
                    }
                } else {
                    if ('category' == $row->code->type) {
                        if ('B' == $row->code->code) {
                            $catB = 'Kategori B';

                            if (1 == $row->status) {
                                $atatusCategoryB = 'Dan';
                            } else {
                                $atatusCategoryB = 'Atau';
                            }
                        } elseif ('CE' == $row->code->code) {
                            $catCE = 'Kategori CE';

                            if (1 == $row->status) {
                                $atatusCategoryCE = 'Dan';
                            } else {
                                $atatusCategoryCE = 'Atau';
                            }
                        } elseif ('M' == $row->code->code) {
                            $catM = 'Kategori M';

                            if (1 == $row->status) {
                                $atatusCategoryM = 'Dan';
                            } else {
                                $atatusCategoryM = 'Atau';
                            }
                        } elseif ('E' == $row->code->code) {
                            $catE = 'Kategori E';

                            if (1 == $row->status) {
                                $atatusCategoryE = 'Dan';
                            } else {
                                $atatusCategoryE = 'Atau';
                            }
                        } elseif ('F' == $row->code->code) {
                            $catF = 'Kategori F';

                            if (1 == $row->status) {
                                $atatusCategoryF = 'Dan';
                            } else {
                                $atatusCategoryF = 'Atau';
                            }
                        }
                    }

                    if ('khusus' == $row->code->type) {
                        if ('B' == $row->code->category) {
                            $catB = $catB . ' ' . $atatusKhususB . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususB = 'Dan';
                            } else {
                                $atatusKhususB = 'Atau';
                            }
                        } elseif ('CE' == $row->code->category) {
                            $catCE = $catCE . ' ' . $atatusKhususCE . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususCE = 'Dan';
                            } else {
                                $atatusKhususCE = 'Atau';
                            }
                        } elseif ('M' == $row->code->category) {
                            $catM = $catM . ' ' . $atatusKhususM . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususM = 'Dan';
                            } else {
                                $atatusKhususM = 'Atau';
                            }
                        } elseif ('E' == $row->code->category) {
                            $catE = $catE . ' ' . $atatusKhususE . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususE = 'Dan';
                            } else {
                                $atatusKhususE = 'Atau';
                            }
                        } elseif ('F' == $row->code->category) {
                            $catF = $catF . ' ' . $atatusKhususF . ' ' . $row->code->code;

                            if (1 == $row->status) {
                                $atatusKhususF = 'Dan';
                            } else {
                                $atatusKhususF = 'Atau';
                            }
                        }
                    }
                }
            }
            if (! empty($catB)) {
                $cidb = $cidb . ', ' . $catB;
            }
            if (! empty($catCE)) {
                if (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catCE;
                } else {
                    $cidb = $cidb . ', ' . $catCE;
                }
            }
            if (! empty($catM)) {
                if (! empty($catCE)) {
                    $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catM;
                } elseif (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catM;
                } else {
                    $cidb = $cidb . ', ' . $catM;
                }
            }
            if (! empty($catE)) {
                if (! empty($catM)) {
                    $cidb = $cidb . ', ' . $atatusCategoryM . ' ' . $catE;
                } elseif (! empty($catCE)) {
                    $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catE;
                } elseif (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catE;
                } else {
                    $cidb = $cidb . ', ' . $catE;
                }
            }
            if (! empty($catF)) {
                if (! empty($catE)) {
                    $cidb = $cidb . ', ' . $atatusCategoryE . ' ' . $catF;
                } elseif (! empty($catM)) {
                    $cidb = $cidb . ', ' . $atatusCategoryM . ' ' . $catF;
                } elseif (! empty($catCE)) {
                    $cidb = $cidb . ', ' . $atatusCategoryCE . ' ' . $catF;
                } elseif (! empty($catB)) {
                    $cidb = $cidb . ', ' . $atatusCategoryB . ' ' . $catF;
                } else {
                    $cidb = $cidb . ', ' . $catF;
                }
            }
        }

        return $cidb;
    }

    // public function getKelayakanCIDBView(int $approval_id)
    // {
    //     $approval = Approval::find($approval_id);

    //     $cidb             = '';
    //     $atatusGrade      = '';
    //     $atatusCategoryB  = '';
    //     $atatusCategoryCE = '';
    //     $atatusCategoryM  = '';
    //     $atatusCategoryE  = '';
    //     $atatusCategoryF  = '';
    //     $atatusKhususB    = '';
    //     $atatusKhususCE   = '';
    //     $atatusKhususM    = '';
    //     $atatusKhususE    = '';
    //     $atatusKhususF    = '';
    //     $catB             = '';
    //     $catM             = '';
    //     $catE             = '';
    //     $catCE            = '';
    //     $catF            = '';
    //     $br               = '';

    //     if ($approval->cidbQualifications->count() > 0) {
    //         foreach ($approval->cidbQualifications as $index => $row) {
    //             if ('grade' == $row->code->type) {
    //                 if (0 == $index) {
    //                     $cidb = '';
    //                 }

    //                 $cidb = $cidb . ' ' . $atatusGrade . ' ' . $row->code->code;

    //                 if (1 == $row->status) {
    //                     $atatusGrade = 'Dan';
    //                 } else {
    //                     $atatusGrade = 'Atau';
    //                 }
    //             } else {
    //                 if ('category' == $row->code->type) {
    //                     if ('B' == $row->code->code) {
    //                         $catB = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryB = 'Dan';
    //                         } else {
    //                             $atatusCategoryB = 'Atau';
    //                         }
    //                     } elseif ('CE' == $row->code->code) {
    //                         $catCE = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryCE = 'Dan';
    //                         } else {
    //                             $atatusCategoryCE = 'Atau';
    //                         }
    //                     } elseif ('M' == $row->code->code) {
    //                         $catM = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryM = 'Dan';
    //                         } else {
    //                             $atatusCategoryM = 'Atau';
    //                         }
    //                     } elseif ('E' == $row->code->code) {
    //                         $catE = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryE = 'Dan';
    //                         } else {
    //                             $atatusCategoryE = 'Atau';
    //                         }
    //                     } elseif ('F' == $row->code->code) {
    //                         $catF = 'Bidang ' . $row->code->code . ' Pengkhususan ';

    //                         if (1 == $row->status) {
    //                             $atatusCategoryF = 'Dan';
    //                         } else {
    //                             $atatusCategoryF = 'Atau';
    //                         }
    //                     }
    //                 }

    //                 if ('khusus' == $row->code->type) {
    //                     if ('B' == $row->code->category) {
    //                         $catB = $catB . ' ' . $atatusKhususB . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususB = 'Dan';
    //                         } else {
    //                             $atatusKhususB = 'Atau';
    //                         }
    //                     } elseif ('CE' == $row->code->category) {
    //                         $catCE = $catCE . ' ' . $atatusKhususCE . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususCE = 'Dan';
    //                         } else {
    //                             $atatusKhususCE = 'Atau';
    //                         }
    //                     } elseif ('M' == $row->code->category) {
    //                         $catM = $catM . ' ' . $atatusKhususM . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususM = 'Dan';
    //                         } else {
    //                             $atatusKhususM = 'Atau';
    //                         }
    //                     } elseif ('E' == $row->code->category) {
    //                         $catE = $catE . ' ' . $atatusKhususE . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususE = 'Dan';
    //                         } else {
    //                             $atatusKhususE = 'Atau';
    //                         }
    //                     } elseif ('F' == $row->code->category) {
    //                         $catF = $catF . ' ' . $atatusKhususF . ' ' . $row->code->code;

    //                         if (1 == $row->status) {
    //                             $atatusKhususF = 'Dan';
    //                         } else {
    //                             $atatusKhususF = 'Atau';
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //         if (! empty($catB)) {
    //             $cidb = $cidb . '<br/>' . $catB;
    //         }
    //         if (! empty($catCE)) {
    //             if (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catCE;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catCE;
    //             }
    //         }
    //         if (! empty($catM)) {
    //             if (! empty($catCE)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryCE . '<br/> ' . $catM;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catM;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catM;
    //             }
    //         }
    //         if (! empty($catE)) {
    //             if (! empty($catM)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryM . '<br/> ' . $catE;
    //             } elseif (! empty($catCE)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryCE . '<br/> ' . $catE;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catE;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catE;
    //             }
    //         }
    //         if (! empty($catF)) {
    //             if (! empty($catE)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryE . '<br/> ' . $catF;
    //             } elseif (! empty($catM)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryM . '<br/> ' . $catF;
    //             } elseif (! empty($catCE)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryCE . '<br/> ' . $catF;
    //             } elseif (! empty($catB)) {
    //                 $cidb = $cidb . ' ' . $atatusCategoryB . '<br/> ' . $catF;
    //             } else {
    //                 $cidb = $cidb . '<br/> ' . $catF;
    //             }
    //         }
    //     }

    //     return $cidb;
    // }


    public function getKelayakanCIDBView(int $approval_id)
    {
        $approval = Approval::find($approval_id);

        $collectionGradeCode = collect();
        $collectionGradeName = collect();
        $collectionGradeStatus = collect();

        $collectionCategoryCode = collect();
        $collectionCategoryName = collect();
        $collectionCategoryStatus = collect();

        $collectionKhususCat = collect([]);
        $collectionKhususCode = collect([]);
        $collectionKhususName = collect([]);
        $collectionKhususStatus = collect([]);

        if ($approval->cidbQualifications->count() > 0) {
            foreach ($approval->cidbQualifications as $index => $row) {
                if ('grade' == $row->code->type) {
                    $collectionGradeCode = $collectionGradeCode->concat([$row->code->code]);
                    $collectionGradeName = $collectionGradeName->concat([$row->code->name]);
                    $collectionGradeStatus = $collectionGradeStatus->concat([$row->status]);
                }elseif('category' == $row->code->type){
                    $collectionCategoryCode = $collectionCategoryCode->concat([$row->code->code]);
                    $collectionCategoryName = $collectionCategoryName->concat([$row->code->name]);
                    $collectionCategoryStatus = $collectionCategoryStatus->concat([$row->status]);
                }elseif ('khusus' == $row->code->type) {
                    $collectionKhususCat = $collectionKhususCat->concat([$row->code->category]);
                    $collectionKhususCode = $collectionKhususCode->concat([$row->code->code]);
                    $collectionKhususName = $collectionKhususName->concat([$row->code->name]);
                    $collectionKhususStatus = $collectionKhususStatus->concat([$row->status]);
                }
            }
        }

        $cidbGrade = '';
        $gradeStatus = '';
        $gradeCombine = '';
        $cidbBidang = '';
        $bidangStatus = '';
        $cidbKhusus = '';
        $status = '';
        $cidbCombine = '';
        $cidbLoop = '';        

        for($g = 0; $g< $collectionGradeCode->count(); $g++){
            if($g == 0) {
                $cidbGrade = 'Gred :  ' . $collectionGradeCode[$g] . ' ';
            }else {
                $cidbGrade = $cidbGrade . $collectionGradeCode[$g] . ' ';
            }

            if($collectionGradeStatus[$g] == '0')
                $gradeStatus = 'ATAU ';
            elseif($collectionGradeStatus[$g] == '1')
                $gradeStatus = 'DAN ';
            else
                $gradeStatus = '';

            $gradeCombine = $cidbGrade . $gradeStatus;
        }

        if($collectionGradeCode->count() > 0){
            $gradeCombine = $gradeCombine . '<br/>';
        }
        
        
        for($c = 0; $c< $collectionCategoryCode->count(); $c++){
            $cidbBidang = 'Kategori :  ' . $collectionCategoryCode[$c] . ' ';

            if($collectionCategoryStatus[$c] == '0')
                $bidangStatus = 'ATAU ';
            elseif($collectionCategoryStatus[$c] == '1')
                $bidangStatus = 'DAN ';
            else
                $bidangStatus = '';

            $cidbKhusus = '';
            for($k = 0; $k < $collectionKhususCat->count(); $k++){
                if($collectionKhususCat[$k] == $collectionCategoryCode[$c]){
                    
                    if($collectionKhususStatus[$k] == '0')
                        $status = 'ATAU ';
                    elseif($collectionKhususStatus[$k] == '1')
                        $status = 'DAN ';
                    else
                        $status = '';

                    $cidbKhusus = $cidbKhusus . ' ' . $collectionKhususName[$k] . ' ' . $status;
                }
            }
            $cidbCombine = $cidbBidang . $cidbKhusus . $bidangStatus . '';
            $cidbLoop = $cidbLoop . $cidbCombine . '<br/>';
        }

        $cidbLoop = $gradeCombine . $cidbLoop;
        
        return $cidbLoop;
    }

    public function getKelayakanMOF(int $approval_id)
    {
        $approval = Approval::find($approval_id);
        $collectionCategoryCode = collect();
        $collectionCategoryName = collect();
        $collectionCategoryStatus = collect();
        $collectionKhususCat = collect([]);
        $collectionKhususCode = collect([]);
        $collectionKhususName = collect([]);
        $collectionKhususStatus = collect([]);


        if ($approval->mofQualifications->count() > 0) {
            foreach ($approval->mofQualifications as $index => $row) {

                if ('category' == $row->code->type) {
                    $collectionCategoryCode = $collectionCategoryCode->concat([$row->code->code]);
                    $collectionCategoryName = $collectionCategoryName->concat([$row->code->name]);
                    $collectionCategoryStatus = $collectionCategoryStatus->concat([$row->status]);
                }elseif ('khusus' == $row->code->type) {
                    $collectionKhususCat = $collectionKhususCat->concat([$row->code->category]);
                    $collectionKhususCode = $collectionKhususCode->concat([$row->code->code]);
                    $collectionKhususName = $collectionKhususName->concat([$row->code->name]);
                    $collectionKhususStatus = $collectionKhususStatus->concat([$row->status]);
                }
            }            
        }

        $mofBidang = '';
        $bidangStatus = '';
        $mofKhusus = '';
        $status = '';
        $mofCombine = '';
        $mofLoop = '';
        
        for($c = 0; $c< $collectionCategoryCode->count(); $c++){
            $mofBidang = 'Bidang : ' . $collectionCategoryCode[$c] . ' ';

            if($collectionCategoryStatus[$c] == '0')
                $bidangStatus = 'ATAU';
            elseif($collectionCategoryStatus[$c] == '1')
                $bidangStatus = 'DAN';
            else
                $bidangStatus = '';

            $mofKhusus = 'Pengkhususan ';
            for($k = 0; $k < $collectionKhususCat->count(); $k++){
                if($collectionKhususCat[$k] == $collectionCategoryCode[$c]){
                    
                    if($collectionKhususStatus[$k] == '0')
                        $status = 'ATAU';
                    elseif($collectionKhususStatus[$k] == '1')
                        $status = 'DAN';
                    else
                        $status = '';

                    $mofKhusus = $mofKhusus . ' ' . $collectionKhususCode[$k] . ' ' . $status;
                }
            }
            $mofCombine = $mofBidang . $mofKhusus . $bidangStatus . ', ';
            $mofLoop = $mofLoop . $mofCombine;

        }

        return $mofLoop;
    }


    public function getKelayakanMOFView(int $approval_id)
    {
        $approval = Approval::find($approval_id);
        $collectionCategoryCode = collect();
        $collectionCategoryName = collect();
        $collectionCategoryStatus = collect();
        $collectionKhususCat = collect([]);
        $collectionKhususCode = collect([]);
        $collectionKhususName = collect([]);
        $collectionKhususStatus = collect([]);


        if ($approval->mofQualifications->count() > 0) {
            foreach ($approval->mofQualifications as $index => $row) {

                if ('category' == $row->code->type) {
                    $collectionCategoryCode = $collectionCategoryCode->concat([$row->code->code]);
                    $collectionCategoryName = $collectionCategoryName->concat([$row->code->name]);
                    $collectionCategoryStatus = $collectionCategoryStatus->concat([$row->status]);
                }elseif ('khusus' == $row->code->type) {
                    $collectionKhususCat = $collectionKhususCat->concat([$row->code->category]);
                    $collectionKhususCode = $collectionKhususCode->concat([$row->code->code]);
                    $collectionKhususName = $collectionKhususName->concat([$row->code->name]);
                    $collectionKhususStatus = $collectionKhususStatus->concat([$row->status]);
                }
            }            
        }

        $mofBidang = '';
        $bidangStatus = '';
        $mofKhusus = '';
        $status = '';
        $mofCombine = '';
        $mofLoop = '';
        
        for($c = 0; $c< $collectionCategoryCode->count(); $c++){
            $mofBidang = 'Bidang ' . $collectionCategoryCode[$c] . ' ';

            if($collectionCategoryStatus[$c] == '0')
                $bidangStatus = 'ATAU';
            elseif($collectionCategoryStatus[$c] == '1')
                $bidangStatus = 'DAN';
            else
                $bidangStatus = '';

            $mofKhusus = 'Pengkhususan ';
            for($k = 0; $k < $collectionKhususCat->count(); $k++){
                if($collectionKhususCat[$k] == $collectionCategoryCode[$c]){
                    
                    if($collectionKhususStatus[$k] == '0')
                        $status = 'ATAU';
                    elseif($collectionKhususStatus[$k] == '1')
                        $status = 'DAN';
                    else
                        $status = '';

                    $mofKhusus = $mofKhusus . ' ' . $collectionKhususCode[$k] . ' ' . $status;
                }
            }
            $mofCombine = $mofBidang . $mofKhusus . $bidangStatus . '<br/>';
            $mofLoop = $mofLoop . $mofCombine;

        }

        return $mofLoop;
    }
    
    public function isReviewApproved(int $acquisition_id,string $type, string $document_contract_type){
        $approved = false;
        $hasApproval = Review::where('status','Selesai')
                                ->where('acquisition_id', $acquisition_id)
                                ->where('document_contract_type', $document_contract_type)
                                ->where('type',$type)->first();

        if(!empty($hasApproval)){
            $approved = true;
        }
        
        return $approved;
    }

    public function getHariInMalay(int $day)
    {
        $hari = "";

        if($day == '0'){
            $hari = "Ahad";
        }elseif($day == '1'){
            $hari = "Isnin";
        }elseif($day == '2'){
            $hari = "Selasa";
        }elseif($day == '3'){
            $hari = "Rabu";
        }elseif($day == '4'){
            $hari = "Khamis";
        }elseif($day == '5'){
            $hari = "Jumaat";
        }elseif($day == '6'){
            $hari = "Sabtu";
        }

        return $hari;
    }

    public function getBulanInMalay(string $month)
    {
        $bulan = "";

        if($month == 'January'){
            $bulan = "Januari";
        }elseif($month == 'February'){
            $month = "Febuari";
        }elseif($month == 'March'){
            $bulan = "Mac";
        }elseif($month == 'April'){
            $bulan = "April";
        }elseif($month == 'May'){
            $bulan = "Mei";
        }elseif($month == 'June'){
            $bulan = "Jun";
        }elseif($month == 'July'){
            $bulan = "Julai";
        }elseif($month == 'August'){
            $bulan = "Ogos";
        }elseif($month == 'September'){
            $bulan = "September";
        }elseif($month == 'October'){
            $bulan = "Oktober";
        }elseif($month == 'November'){
            $bulan = "November";
        }elseif($month == 'December'){
            $bulan = "Disember";
        }

        return $bulan;
    }

    public function convertTempohInEnglish(string $tempoh)
    {
        $tempohEng = "";

        if($tempoh == 'Hari'){
            $tempohEng = "days";
        }elseif($tempoh == 'Bulan'){
            $tempohEng = "months";
        }elseif($tempoh == 'Minggu'){
            $tempohEng = "weeks";
        }elseif($tempoh == 'Tahun'){
            $tempohEng = "years";
        }

        return $tempohEng;
    }

    public function checkProSum(string $element)
    {
        $wordsArray = array('PROVISIONAL SUMS','PROVISIONAL SUM', 'PROV SUM', 'PROV. SUM', 'PROV.SUM');
        $matchFound = "";
        $matchFound = preg_match_all(
            "/\b(" . implode($wordsArray,"|") . ")\b/i", 
            $element, 
            $matches
        );

        return $matchFound;
    }


    public function getInsuranTanggunganAwam($acqCat, $offeredPrice){
        $insTA = 0;
        if($acqCat == 'Sebut Harga' || $acqCat == 'Sebut Harga B'){
           if($offeredPrice <= 5000000){
               $insTA = 1000000;
           }else if($offeredPrice > 5000000 && $offeredPrice <= 10000000){
               $insTA = 2500000;
           }else if($offeredPrice > 10000000 && $offeredPrice <= 20000000){
               $insTA = 5000000;
           }else if($offeredPrice > 20000000 && $offeredPrice <= 50000000){
               $insTA = 10000000;
           }
        }else if($acqCat == 'Tender Terhad' || $acqCat == 'Tender Terbuka'){
           if($offeredPrice > 50000000 && $offeredPrice <= 500000000){
               $insTA = 20000000;
           }else if($offeredPrice > 500000000 && $offeredPrice <= 2000000000){                
               $insTA = 50000000;
           }else if($offeredPrice > 2000000000 && $offeredPrice <= 5000000000){
               $insTA = 100000000;
           }else if($offeredPrice > 5000000000){
               $insTA = 2000000000;
           }
        }
        return $insTA;
    }

   public function getInsuranPampasanPekerja($appointed){

       return getPriceWithoutWps($appointed) * 0.1;
   }

   public function getPriceWithoutWps($appointed){

       if($appointed->acquisition->bqElements->count() > 0){
           foreach($appointed->acquisition->bqElements as $element){
               $isProSumFound = common()->checkProSum($element->element);
               if($isProSumFound){
                   $amountWithoutWPS = $appointed->offered_price - $element->amount;                    
               }
           }
           if($amountWithoutWPS == 0){
               $amountWithoutWPS = $appointed->offered_price;
           }
       }else{
           $amountWithoutWPS = $appointed->offered_price;
       }
       return $amountWithoutWPS;
   }
}
