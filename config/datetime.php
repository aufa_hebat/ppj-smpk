<?php

return [
    'datetimepicker' => [
        'date'      => 'd/m/Y',
        'time'      => 'g:i A',
        'date_time' => 'd/m/Y g:i A',
    ],
    'display' => [
        'date'      => 'DD/MM/YYYY',
        'time'      => 'hh:mm:ss A',
        'date_time' => 'DD/MM/YYYY hh:mm:ss A',
    ],
    'dashboard' => 'd-M-Y h:m A',
    'chart'     => 'M-Y',
    'file'      => 'Ymd',
];
