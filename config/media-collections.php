<?php


return [
    'acquisition' => [
        'approval'    => 'acquisition-approval-documents',
        'acquisition' => 'acquisition-documents',
        'box'         => 'acquisition-box-documents',
        'result'      => 'acquisition-result-documents',
        'activity'    => 'acquisition-activity',
    ],
    'user' => [
        'avatar' => 'avatar',
    ],
    'default' => 'all-media',
];
