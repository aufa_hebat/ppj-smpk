<?php

return [
    'salt'     => env('HASHIDS_SALT', env('APP_KEY')),
    'length'   => env('HASHIDS_LENGTH', 32),
    'alphabet' => env('HASHIDS_ALPHABET', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'),
];
