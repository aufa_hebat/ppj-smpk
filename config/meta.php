<?php

return [
    // 'author'      => env('META_AUTHOR', env('APP_URL')),
    'author'      => env('META_AUTHOR', 'https://dcoins.ppj.gov.my'),
    'keywords'    => env('META_KEYWORDS', 'COINS PPJ'),
    'description' => env('META_DESCRIPTION', 'Sistem Pengurusan Kontrak (COINS) Perbadanan Putrajaya'),
    // 'title'       => env('META_TITLE', env('APP_URL')),
    'title'       => env('META_TITLE', 'https://dcoins.ppj.gov.my'),
];
