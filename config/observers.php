<?php


return [
    /*
    |--------------------------------------------------------------------------
    | Observers
    |--------------------------------------------------------------------------
    |
    | This value is the list of models being observed by the observer class.
    | The observer will observe any events triggered by eloquent and of course,
    | to which event the observer want to listen to - created, creating, updating,
    | updated, deleting or deleted.
    |
     */
    \App\Observers\CreatedByObserver::class => [
        // Observe who create record
    ],
    \App\Observers\ReferenceObserver::class => [
        \App\Models\Acquisition\Acquisition::class,
        \App\Models\Acquisition\Approval::class,
    ],
    \App\Observers\TaskObserver::class => [
        \App\Models\Acquisition\Acquisition::class,
        \App\Models\Acquisition\Approval::class,
        \App\Models\Acquisition\Sst::class,
    ],
    \App\Observers\HashidsObserver::class => [
        \App\Models\User::class,
        \App\Models\Task::class,
        \Spatie\MediaLibrary\Media::class,

        /*
         * Acquisition
         */
        \App\Models\Acquisition\Activity::class,
        \App\Models\Acquisition\Acquisition::class,
        \App\Models\Acquisition\AppointedCompany::class,
        \App\Models\Acquisition\Approval::class,
        \App\Models\Acquisition\Briefing::class,
        \App\Models\Acquisition\Approval\Financial::class,
        \App\Models\Acquisition\Approval\Location::class,
        \App\Models\Acquisition\Approval\CIDBQualification::class,
        \App\Models\Acquisition\Box::class,
        \App\Models\Acquisition\Box\Officer::class,
        \App\Models\Acquisition\AcquisitionBriefing::class,
        \App\Models\Acquisition\Evaluation::class,
        \App\Models\Acquisition\Temp::class,
        \App\Models\Acquisition\Sst::class,
        \App\Models\Acquisition\Bon::class,
        \App\Models\Acquisition\Insurance::class,
        \App\Models\Acquisition\Deposit::class,
        \App\Models\Acquisition\Document::class,
        \App\Models\Acquisition\SubContract::class,
        \App\Models\Acquisition\Eot::class,
        \App\Models\Acquisition\EotDetail::class,
        \App\Models\Acquisition\EotApprove::class,
        \App\Models\Acquisition\EotInsurance::class,
        \App\Models\Acquisition\EotBon::class,
        \App\Models\Acquisition\Monitoring\SiteVisit::class,

        \App\Models\Sale::class,

        /*
         * Addendums
         */
        \App\Models\Addendum\Addendum::class,
        \App\Models\Addendum\Item::class,

        /*
         * BillOfQuantity
         */
        \App\Models\BillOfQuantity\Element::class,
        \App\Models\BillOfQuantity\Item::class,
        \App\Models\BillOfQuantity\SubItem::class,

        /*
         * CIDB
         */
        \App\Models\CIDB\Category::class,
        \App\Models\CIDB\Grade::class,
        \App\Models\CIDB\Khusus::class,

        /*
         * Company
         */
        \App\Models\Company\Company::class,
        \App\Models\Company\MOF::class,
        \App\Models\Company\Owner::class,
        \App\Models\Company\Representative::class,

        /*
         * Document
         */
        \App\Models\Document::class,

        /*
         * Task
         */
        \App\Models\Task::class,

        /*
         * Verify
         */
        \App\Models\Verify\History::class,
        \App\Models\Verify\Law::class,
        \App\Models\Verify\LawHistory::class,
        \App\Models\Verify\Remark::class,

        /*
         * Commitee
         */
        \App\Models\Committee\Assessment::class,
        \App\Models\Committee\Approval::class,

        /*
         * IPC
         */
        \App\Models\Acquisition\Ipc::class,
        \App\Models\Acquisition\IpcInvoice::class,
        \App\Models\Acquisition\IpcMaterial::class,
        \App\Models\Acquisition\IpcBq::class,
        \App\Models\Acquisition\IpcVo::class,
        /*
         * Termination
         */
         \App\Models\Acquisition\Warning::class,
         \App\Models\Acquisition\Termination::class,

         /*
          * Variation Order
          */
         \App\Models\VariationOrderType::class,

         /*
          * VO
          */
        \App\Models\VO\VO::class,
        \App\Models\VO\Type::class,
        \App\Models\VO\Active::class,
        \App\Models\VO\Cancel::class,
        \App\Models\VO\Element::class,
        \App\Models\VO\Item::class,
        \App\Models\VO\SubItem::class,
        \App\Models\VO\Approver::class,

         /*
          * PPJHK
          */
        \App\Models\VO\PPJHK\Ppjhk::class,
        \App\Models\VO\PPJHK\Element::class,
        \App\Models\VO\PPJHK\Item::class,
        \App\Models\VO\PPJHK\SubItem::class,        

        \App\Models\VO\KKK\Element::class,
        \App\Models\VO\PK\Element::class,
        \App\Models\VO\PQ\Element::class,
        \App\Models\VO\WPS\Element::class,
        \App\Models\VO\KKK\Item::class,
        \App\Models\VO\PK\Item::class,
        \App\Models\VO\PQ\Item::class,
        \App\Models\VO\WPS\Item::class,
        \App\Models\VO\KKK\SubItem::class,
        \App\Models\VO\PK\SubItem::class,
        \App\Models\VO\PQ\SubItem::class,
        \App\Models\VO\WPS\SubItem::class,

        /*
         * Certificates
         */
        \App\Models\Acquisition\Cnc::class,
        \App\Models\Acquisition\Cpc::class,
        \App\Models\Acquisition\Cmgd::class,
        \App\Models\Acquisition\Cpo::class,

        /*
         * Sofa
         */
        \App\Models\Acquisition\Sofa::class,
        
        /*
         * MOF
         */
        \App\Models\Acquisition\Approval\MOFQualification::class,
        
        /*
         * Document Acceptance
         */
        \App\Models\DocumentAcceptance::class,
    ],
];
