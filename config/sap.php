<?php


return [
    'endpoint' => env('SAP_ENDPOINT'),
    'vendor'   => env('SAP_VENDOR'),
];
