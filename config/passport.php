<?php


return [
    /*
     * By default, Laravel Passport Token will expire after 24 hours
     */
    'tokens_expire_in' => env('PASSPORT_TOKEN_EXPIRE_IN', 24),

    /*
     * By default, Laravel Passport Refresh Token will expire after 24 hours
     */
    'refresh_tokens_expire_in' => env('PASSPORT_REFRESH_TOKEN_EXPIRE_IN', 24),
];
