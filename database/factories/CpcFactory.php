<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Cpc::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'         => $user->id,
        'dlp_start_date'  => \Carbon\Carbon::now()->format('Y-m-d'),
        'dlp_expiry_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        'signature_date'  => \Carbon\Carbon::now()->format('Y-m-d'),
        'test_award_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        'clause_1'        => 'Klausa' . $faker->randomElement(range(1, 100)),
        'clause_2'        => 'Klausa' . $faker->randomElement(range(1, 100)),
    ];
});
