<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Acquisition::class, function (Faker $faker) {
    $prices = range(10000, 200000);

    $total_document_price = $document_price = $faker->randomElement($prices);
    $department_estimation = round(0.8 * $total_document_price, 0);
    $date = \Carbon\Carbon::now();

    $advertised_at = $date->addWeeks(1);

    $start_sale_at = $advertised_at->addWeeks(1);
    $closed_sale_at = $start_sale_at->addWeeks(2);

    $closed_at = $closed_sale_at;

    return [
        'title'                   => $faker->sentence,
        'visit_location'          => $faker->sentence,
        'acquisition_category_id' => $faker->randomElement(range(1, 4)),

        /*
         * Price
         */
        'document_price'        => $document_price,
        'total_document_price'  => $total_document_price,
        'department_estimation' => $department_estimation,

        /*
         * Time
         */
        'closed_at'      => $closed_at,
        'advertised_at'  => $advertised_at,
        'start_sale_at'  => $start_sale_at,
        'closed_sale_at' => $closed_sale_at,
    ];
});
