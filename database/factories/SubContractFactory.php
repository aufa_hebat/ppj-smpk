<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\SubContract::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();
    $company = companies()->shuffle()->first();

    return [
        'user_id'    => $user->id,
        'company_id' => $company->id,
        'amount'     => $faker->randomElement(range(50000, 90000)),
        'description'=> 'aaaaaaaaa',
    ];
});
