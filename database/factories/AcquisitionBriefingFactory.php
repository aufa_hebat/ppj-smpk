<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\AcquisitionBriefing::class, function (Faker $faker) {
    return [
        'briefing_staff_id' => $faker->randomElement(range(1, 8)),
        'company_id'        => $faker->randomElement(range(1, 3)),

        /*
         * Status
         */
        'site_visit' => true,

        /*
         * Staff
         */
        'visit_staff_id' => $faker->randomElement(range(1, 8)),
    ];
});
