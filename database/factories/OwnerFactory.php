<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Company\Owner::class, function (Faker $faker) {
    return [
        'name'      => $faker->name,
        'cidb_cert' => $faker->randomElement(range(0, 1)),
        'mof_cert'  => $faker->randomElement(range(0, 1)),
        'kdn_cert'  => $faker->randomElement(range(0, 1)),
    ];
});
