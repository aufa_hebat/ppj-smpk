<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Approval::class, function (Faker $faker) {
    $department_id = $faker->randomElement(departments()->pluck('id')->toArray());
    $acquisition_type_id = $faker->randomElement(acq_types()->pluck('id')->toArray());
    $period_type_id = $faker->randomElement(period_types()->pluck('id')->toArray());

    return [
        'acquisition_type_id' => $acquisition_type_id,
        'department_id'       => $department_id,
        'title'               => $faker->sentence,
        'year'                => $faker->randomElement([2018, 2019, 2020]),
        'description'         => $faker->sentence,
        'period_type_id'      => $period_type_id,
    ];
});
