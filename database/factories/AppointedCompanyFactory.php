<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\AppointedCompany::class, function (Faker $faker) {
    return [
        'offered_price' => $faker->randomElement(range(1000, 50000)),
    ];
});
