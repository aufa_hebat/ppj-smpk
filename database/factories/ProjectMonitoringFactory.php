<?php

use Faker\Generator as Faker;

// site visit
$factory->define(\App\Models\Acquisition\Monitoring\SiteVisit::class, function (Faker $faker) {
    return [
        'user_id'    => $faker->randomElement(range(10, 20)),
        'name'       => $faker->sentence,
        'remarks'    => $faker->sentence,
        'visited_by' => $faker->randomElement(range(100, 200)),
        'visited_at' => \Carbon\Carbon::now()->addMonths($faker->randomElement(range(1, 12))),
    ];
});

$factory->define(\App\Models\Acquisition\Activity::class, function (Faker $faker) {
    $start_range = 0; // should be overwrite
    $end_range = 100; // should be overwrite

    $scheduled_started = \Carbon\Carbon::now()->addMonths(range(1, 12));
    $scheduled_ended = $scheduled_started->copy()->addWeeks(range(1, 12));
    $scheduled_duration = $scheduled_started->diffInDays($scheduled_ended);
    $scheduled_completion = rand($start_range, $end_range);

    $current_started = $scheduled_started->copy()->addMonths(range(1, 2));
    $current_ended = $current_started->copy()->addWeeks(range(1, 12));
    $current_duration = $current_started->diffInDays($current_ended);
    $current_completion = rand($start_range, $end_range);

    $actual_started = $scheduled_started->copy()->addMonths(range(1, 2));
    $actual_ended = $actual_started->copy()->addWeeks(range(1, 12));
    $actual_duration = $actual_started->diffInDays($actual_ended);
    $actual_completion = rand($start_range, $end_range);

    return [
        'parent_id' => $faker->boolean(65),
        'name'      => $faker->sentence,

        'scheduled_started_at' => $scheduled_started,
        'scheduled_ended_at'   => $scheduled_ended,
        'scheduled_duration'   => $scheduled_duration,
        'scheduled_completion' => $scheduled_completion,

        'current_started_at' => $current_started,
        'current_ended_at'   => $current_ended,
        'current_duration'   => $current_duration,
        'current_completion' => $current_completion,

        'actual_started_at' => $actual_started,
        'actual_ended_at'   => $actual_ended,
        'actual_duration'   => $actual_duration,
        'actual_completion' => $actual_completion,
    ];
});
