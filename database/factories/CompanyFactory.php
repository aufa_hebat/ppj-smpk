<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Company\Company::class, function (Faker $faker) {
    return [
        'company_name'    => $faker->company,
        'ssm_no'          => $faker->randomNumber(6) . '-X',
        'ssm_start_date'  => \Carbon\Carbon::now(),
        'ssm_end_date'    => \Carbon\Carbon::now()->addYear(),
        'email'           => $faker->unique()->safeEmail,
        'account_bank_no' => $faker->unique()->bankAccountNumber,
        'bank_name'       => banks()->random()->name,
    ];
});
