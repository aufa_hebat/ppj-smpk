<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\IpcMaterial::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'     => $user->id,
        'description' => $faker->sentence,
        'unit'        => 'kg',
        'quantity'    => $faker->randomElement(range(1, 20)),
        'rate'        => $faker->randomElement(range(0.1, 5.0)),
        'amount'      => $faker->randomElement(range(100, 50000)),
    ];
});
