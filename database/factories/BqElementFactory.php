<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\BillOfQuantity\Element::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();
    return [
        'no'            => $faker->randomElement(range(1, 5)),
        'element'       => $faker->sentence,
        'description'   => $faker->sentence,
        'amount'        => $faker->randomElement(range(1000, 50000)),
        'amount_adjust' => $faker->randomElement(range(1000, 50000)),
        'created_by'    => $user->id,
    ];
});
