<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\BillOfQuantity\Item::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();
    return [
        'no'                => $faker->randomElement(range(1, 10)),
        'item'              => $faker->sentence,
        'bq_no_element'     => $faker->randomElement(range(1, 5)),
        'description'       => $faker->sentence,
        'amount'            => $faker->randomElement(range(1000, 5000)),
        'amount_adjust'     => $faker->randomElement(range(1000, 5000)),
        'created_by'        => $user->id,
    ];
});
