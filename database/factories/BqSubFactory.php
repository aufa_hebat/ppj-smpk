<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\BillOfQuantity\SubItem::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();
    return [
        'no'                   => $faker->randomElement(range(1, 10)),
        'item'                 => $faker->sentence,
        'bq_no_element'        => $faker->randomElement(range(1, 5)),
        'bq_no_item'           => $faker->randomElement(range(1, 10)),
        'description'          => $faker->sentence,
        'unit'                 => $faker->word,
        'quantity'             => $faker->randomElement(range(10, 50)),
        'rate_per_unit'        => $faker->randomElement(range(1, 20)),
        'amount'               => $faker->randomElement(range(100, 500)),
        'unit'                 => $faker->word,
        'quantity'             => $faker->randomElement(range(10, 50)),
        'rate_per_unit_adjust' => $faker->randomElement(range(1, 20)),
        'amount_adjust'        => $faker->randomElement(range(100, 500)),
        'created_by'           => $user->id,
    ];
});
