<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Cmgd::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'         => $user->id,
        'signature_date'  => \Carbon\Carbon::now()->format('Y-m-d'),
        'site_visit_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        'mgd_date'        => \Carbon\Carbon::now()->format('Y-m-d'),
        'clause'          => 'Klausa' . $faker->randomElement(range(1, 100)),
    ];
});
