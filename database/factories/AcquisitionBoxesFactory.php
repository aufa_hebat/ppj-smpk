<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Box::class, function (Faker $faker) {
    return [
        'no'             => '1',
        'period'         => $faker->randomElement(range(1, 50)),
        'period_type_id' => $faker->randomElement(range(1, 4)),
        'is_stated'      => '1',
        'amount'         => $faker->randomElement(range(10000, 200000)),
    ];
});
