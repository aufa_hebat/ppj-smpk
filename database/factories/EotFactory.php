<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Eot::class, function (Faker $faker) {
    return [
        'period_type_id' => 3, // month
        'bil'            => $faker->randomDigitNotNull,
        'section'        => $faker->randomDigitNotNull,
    ];
});

$factory->define(\App\Models\Acquisition\EotDetail::class, function (Faker $faker) {
    return [
        'reasons'         => $faker->sentence,
        'clause'          => $faker->sentence,
        'extended_period' => $faker->randomDigitNotNull,
        'periods' => $faker->randomDigitNotNull,
        'period_type_id' => $faker->randomElement(range(1,4)),
    ];
});

$factory->define(\App\Models\Acquisition\EotInsurance::class, function (Faker $faker) {
    $range = range(1000, 2000);

    return [
        'public_policy_no'              => $faker->randomNumber(6, true),
        'public_policy_amount'          => $faker->randomElement($range),
        'public_received_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'public_start_date'             => now()->addMonths($faker->randomElement(range(1, 10))),
        'public_expired_date'           => now()->addMonths($faker->randomElement(range(1, 10))),
        'public_proposed_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'public_approval_proposed_date' => now()->addMonths($faker->randomElement(range(1, 10))),
        'public_approval_received_date' => now()->addMonths($faker->randomElement(range(1, 10))),

        'work_policy_no'              => $faker->randomNumber(6, true),
        'work_policy_amount'          => $faker->randomElement($range),
        'work_received_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'work_start_date'             => now()->addMonths($faker->randomElement(range(1, 10))),
        'work_expired_date'           => now()->addMonths($faker->randomElement(range(1, 10))),
        'work_proposed_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'work_approval_proposed_date' => now()->addMonths($faker->randomElement(range(1, 10))),
        'work_approval_received_date' => now()->addMonths($faker->randomElement(range(1, 10))),

        'compensation_policy_no'              => $faker->randomNumber(6, true),
        'compensation_policy_amount'          => $faker->randomElement($range),
        'compensation_received_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'compensation_start_date'             => now()->addMonths($faker->randomElement(range(1, 10))),
        'compensation_expired_date'           => now()->addMonths($faker->randomElement(range(1, 10))),
        'compensation_proposed_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'compensation_approval_proposed_date' => now()->addMonths($faker->randomElement(range(1, 10))),
        'compensation_approval_received_date' => now()->addMonths($faker->randomElement(range(1, 10))),
    ];
});

$factory->define(\App\Models\Acquisition\EotBon::class, function (Faker $faker) {
    return [
        'bon_type' => bon_types()->shuffle()->first()->id,

        'bank_name'                   => banks()->shuffle()->first()->id,
        'bank_no'                     => $faker->randomNumber(6, true),
        'bank_amount'                 => $faker->randomElement(range(10000, 20000)),
        'bank_received_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'bank_start_date'             => now()->addMonths($faker->randomElement(range(1, 10))),
        'bank_expired_date'           => now()->addMonths($faker->randomElement(range(1, 10))),
        'bank_proposed_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'bank_approval_proposed_date' => now()->addMonths($faker->randomElement(range(1, 10))),
        'bank_approval_received_date' => now()->addMonths($faker->randomElement(range(1, 10))),

        'insurance_name'                   => insurances()->shuffle()->first()->id,
        'insurance_no'                     => $faker->randomNumber(6, true),
        'insurance_amount'                 => $faker->randomElement(range(10000, 20000)),
        'insurance_received_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'insurance_start_date'             => now()->addMonths($faker->randomElement(range(1, 10))),
        'insurance_expired_date'           => now()->addMonths($faker->randomElement(range(1, 10))),
        'insurance_proposed_date'          => now()->addMonths($faker->randomElement(range(1, 10))),
        'insurance_approval_proposed_date' => now()->addMonths($faker->randomElement(range(1, 10))),
        'insurance_approval_received_date' => now()->addMonths($faker->randomElement(range(1, 10))),

        'cheque_name'           => insurances()->shuffle()->first()->id,
        'cheque_no'             => $faker->randomNumber(6, true),
        'cheque_resit_no'       => $faker->randomNumber(6, true),
        'cheque_submitted_date' => $faker->randomElement(range(10000, 20000)),
        'cheque_received_date'  => now()->addMonths($faker->randomElement(range(1, 10))),
        'money_amount'          => $faker->randomElement(range(10000, 20000)),
        'money_received_at'     => now()->addMonths($faker->randomElement(range(1, 10))),
    ];
});

$factory->define(\App\Models\Acquisition\EotApprove::class, function (Faker $faker) {
    return [
        'bil_meeting'       => $faker->randomNumber(2, true),
        'meeting_date'      => now()->addMonths($faker->randomElement(range(1, 10))),
        'approved_end_date' => now()->addMonths($faker->randomElement(range(1, 10))),
        'approval'          => $faker->boolean(45),
        'period_type_id'    => period_types()->shuffle()->first()->id,
        'period'            => $faker->randomElement(range(1, 10)),
    ];
});
