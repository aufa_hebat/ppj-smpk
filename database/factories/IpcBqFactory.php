<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\IpcBq::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'  => $user->id,
        'quantity' => $faker->randomElement(range(10, 50)),
        'amount'   => $faker->randomElement(range(10000, 50000)),
    ];
});
