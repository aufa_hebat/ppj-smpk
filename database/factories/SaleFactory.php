<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Sale::class, function (Faker $faker) {
    $sales_date = \Carbon\Carbon::now();

    return [
        'company_id' => $faker->randomElement(range(1, 5)),
        /*
         * Numbering
         */
        'series_no'  => $faker->randomElement(range(1000, 2000)),
        'receipt_no' => 'RES' . $faker->randomElement(range(10000, 20000)),

        /*
         * Status
         */
//        'sales_status' => true,

        /*
         * Time
         */
        'sales_date' => $sales_date,
    ];
});
