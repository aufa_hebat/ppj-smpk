<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Insurance::class, function (Faker $faker) {
    $range = range(50000, 90000);
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id' => $user->id,

        'public_insurance_name'         => $faker->randomElement(range(1, 20)),
        'public_policy_no'              => $faker->randomNumber(null, false),
        'public_policy_amount'          => $faker->randomElement($range),
        'public_received_date'          => \Carbon\Carbon::now(),
        'public_start_date'             => \Carbon\Carbon::now(),
        'public_expired_date'           => \Carbon\Carbon::now(),
        'public_proposed_date'          => \Carbon\Carbon::now(),
        'public_approval_proposed_date' => \Carbon\Carbon::now(),
        'public_approval_received_date' => \Carbon\Carbon::now(),

        'work_insurance_name'         => $faker->randomElement(range(1, 20)),
        'work_policy_no'              => $faker->randomNumber(null, false),
        'work_policy_amount'          => $faker->randomElement($range),
        'work_received_date'          => \Carbon\Carbon::now(),
        'work_start_date'             => \Carbon\Carbon::now(),
        'work_expired_date'           => \Carbon\Carbon::now(),
        'work_proposed_date'          => \Carbon\Carbon::now(),
        'work_approval_proposed_date' => \Carbon\Carbon::now(),
        'work_approval_received_date' => \Carbon\Carbon::now(),

        'compensation_insurance_name'         => $faker->randomElement(range(1, 20)),
        'compensation_policy_no'              => $faker->randomNumber(null, false),
        'compensation_policy_amount'          => $faker->randomElement($range),
        'compensation_received_date'          => \Carbon\Carbon::now(),
        'compensation_start_date'             => \Carbon\Carbon::now(),
        'compensation_expired_date'           => \Carbon\Carbon::now(),
        'compensation_proposed_date'          => \Carbon\Carbon::now(),
        'compensation_approval_proposed_date' => \Carbon\Carbon::now(),
        'compensation_approval_received_date' => \Carbon\Carbon::now(),
    ];
});
