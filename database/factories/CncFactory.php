<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Cnc::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'         => $user->id,
        'lad_amount'      => $faker->randomElement(range(50000, 90000)),
        'signature_date'  => \Carbon\Carbon::now()->format('Y-m-d'),
        'test_award_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        'clause_1'        => 'Klausa' . $faker->randomElement(range(1, 100)),
        'clause_2'        => 'Klausa' . $faker->randomElement(range(1, 100)),
        'assessment_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        'blr_rate'        => $faker->randomElement(range(0.00, 100)),
        'days_passed'     => $faker->randomElement(range(0, 365)),
        'lad_amount'      => $faker->randomElement(range(50000, 90000)),
    ];
});
