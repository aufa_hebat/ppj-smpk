<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Evaluation::class, function (Faker $faker) {
    return [
        'meeting_no' => $faker->randomElement(range(1, 1000)),
    ];
});
