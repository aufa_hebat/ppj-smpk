<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Document::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'            => $user->id,
        'revenue_stamp_date' => \Carbon\Carbon::now(),
        'preparation_period' => $faker->randomNumber(null, false),
        'proposed_date'      => \Carbon\Carbon::now(),
        'submitted_date'     => \Carbon\Carbon::now(),
        'base_lending_rate'  => $faker->randomFloat(null, 0, 100),
        'LAD_rate'           => $faker->randomElement(range(50000, 90000)),
        'late_days'          => $faker->randomNumber(null, false),
    ];
});
