<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Cpo::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'         => $user->id,
        'agreement_date'  => \Carbon\Carbon::now()->format('Y-m-d'),
        'partial_date'    => \Carbon\Carbon::now()->format('Y-m-d'),
        'assessment_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        'cpo_start_date'  => \Carbon\Carbon::now()->format('Y-m-d'),
        'cpo_end_date'    => \Carbon\Carbon::now()->format('Y-m-d'),

        'estimation_amount' => $faker->randomElement(range(50000, 90000)),
        'cpo_amount'        => $faker->randomElement(range(50000, 90000)),
        'lad_per_day'       => $faker->randomElement(range(50000, 90000)),
    ];
});
