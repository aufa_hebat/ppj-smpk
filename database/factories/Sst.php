<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Sst::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();
    $company_owner_id = $faker->randomElement(company_owners()->pluck('id')->toArray());
    $start_date = \Carbon\Carbon::now();
    $end_date = $start_date->addWeeks(4);
    $range = range(50000, 90000);

    return [
        'user_id'                  => $user->id,
        'department_id'            => $user->department->id,
        'company_owner_id'         => $company_owner_id,
        'period'                   => $faker->randomElement(range(1, 30)),
        'period_type_id'           => $faker->randomElement(range(1, 4)),
        'contract_no'              => $faker->word,
        'start_working_date'       => $start_date,
        'end_working_date'         => $end_date,
        'letter_date'              => $start_date,
        'defects_liability_period' => '6',
        'sl1m_entity'              => $faker->randomElement(range(10, 20)),
        'bon_amount'               => $faker->randomElement($range),
        'ins_pli_start_date'       => $start_date,
        'ins_pli_end_date'         => $end_date,
        'ins_pli_amount'           => $faker->randomElement($range),
        'ins_work_start_date'      => $start_date,
        'ins_work_end_date'        => $end_date,
        'ins_work_amount'          => $faker->randomElement($range),
        'ins_ci_start_date'        => $start_date,
        'ins_ci_end_date'          => $end_date,
        'ins_ci_amount'            => $faker->randomElement($range),
        'vp_sign_date'             => $start_date,
        'al_issue_date'            => $end_date,
        'al_contractor_date'       => $start_date,
    ];
});
