<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\IpcInvoice::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();
    $company = companies()->shuffle()->first();

    return [
        'user_id'    => $user->id,
        'company_id' => $company->id,
        'status'     => 1,
        'invoice_no' => $faker->randomElement(range(1000, 5000)),
        'invoice'    => $faker->randomElement(range(50000, 90000)),
    ];
});
