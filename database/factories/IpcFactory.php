<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Ipc::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'    => $user->id,
        'ipc_no'     => $faker->randomElement(range(1, 50)),
        'status'     => 1,
        'short_text' => $faker->sentence,
    ];
});
