<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Deposit::class, function (Faker $faker) {
    $range = range(50000, 90000);
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'                     => $user->id,
        'prime_cost'                  => $faker->randomNumber(null, false),
        'amount'                      => $faker->randomElement($range),
        'total_amount'                => $faker->randomElement($range),
        'qualified_amount'            => $faker->randomElement($range),
        'bon_type'                    => 1,
        'bank_id'                     => $faker->randomElement(range(1, 30)),
        'bank_name'                   => $faker->randomElement(range(1, 30)),
        'bank_no'                     => $faker->randomNumber(null, false),
        'bank_amount'                 => $faker->randomElement($range),
        'bank_received_date'          => \Carbon\Carbon::now(),
        'bank_start_date'             => \Carbon\Carbon::now(),
        'bank_expired_date'           => \Carbon\Carbon::now(),
        'bank_proposed_date'          => \Carbon\Carbon::now(),
        'bank_approval_proposed_date' => \Carbon\Carbon::now(),
        'bank_approval_received_date' => \Carbon\Carbon::now(),
    ];
});
