<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Acquisition\Bon::class, function (Faker $faker) {
    $user = users()->where('department_id', '!=', null)->shuffle()->first();

    return [
        'user_id'           => $user->id,
        'bon_type'          => 4,
        'money_amount'      => $faker->randomElement(range(50000, 90000)),
        'money_received_at' => \Carbon\Carbon::now()->format('Y-m-d'),
    ];
});
