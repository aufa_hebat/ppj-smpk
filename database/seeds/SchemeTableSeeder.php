<?php

use Illuminate\Database\Seeder;

class SchemeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'A',
            'B',
            'C',
            'F',
            'FA',
            'FT',
            'G',
            'H',
            'J',
            'JA',
            'JUSA A',
            'JUSA B',
            'JUSA C',
            'KP',
            'L',
            'N',
            'Q',
            'S',
            'U',
            'V',
            'W',
            'WA',
        ];

        foreach ($data as $key => $value) {
            \App\Models\Scheme::create(['name' => $value]);
        }
    }
}
