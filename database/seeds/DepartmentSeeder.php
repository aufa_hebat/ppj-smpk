<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    use \App\Traits\TruncateTableTrait;

    protected $truncate = [
        'departments',
    ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->truncateTables();

        $data = [
            [
                'code'  => 'ALL',
                'name'  => 'Semua',
                'color' => '#00ba06',
            ], [
                'code'  => 'JA',
                'name'  => 'Audit Dan Kualiti Ansurans',
                'color' => '#497263',
            ], [
                'code'  => 'JU',
                'name'  => 'Undang-Undang',
                'color' => '#579DD0',
            ], [
                'code'  => 'JL',
                'name'  => 'Lanskap dan Taman',
                'color' => '#2549C8',
            ], [
                'code'  => 'JK',
                'name'  => 'Perkhidmatan Korporat',
                'color' => '#445DAD',
            ], [
                'code'  => 'JP',
                'name'  => 'Perancangan Bandar',
                'color' => '#67DAE9',
            ], [
                'code'  => 'JR',
                'name'  => 'Perkhidmatan Bandar',
                'color' => '#5FE713',
            ], [
                'code'  => 'JB',
                'name'  => 'Kejuruteraan & Penyelengaraan Bandar',
                'color' => '#1F9926',
            ], [
                'code'  => 'JW',
                'name'  => 'Kewangan',
                'color' => '#45E278',
            ], [
                'code'  => 'LL',
                'name'  => 'Lain-Lain',
                'color' => '#b2b2b2',
            ],
        ];

        foreach ($data as $datum) {
            Department::create($datum)->counter()->create();
        }
    }
}
