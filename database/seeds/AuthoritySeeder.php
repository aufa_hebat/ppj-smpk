<?php

use App\Models\Acquisition\Approval\Authority;
use Illuminate\Database\Seeder;

class AuthoritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['code' => 'NPJU', 'name' => 'Naib Presiden Jabatan Undang-Undang'],
            ['code' => 'NPJL', 'name' => 'Naib Presiden Jabatan Lanskap dan Taman'],
            ['code' => 'NPJPK', 'name' => 'Naib Presiden Jabatan Perkhidmatan Korporat'],
            ['code' => 'NPJPB', 'name' => 'Naib Presiden Jabatan Perancangan Bandar'],
            ['code' => 'NPJAA', 'name' => 'Naib Presiden Jabatan Audit dan dan Kualiti Ansurans'],
            ['code' => 'NPJPB', 'name' => 'Naib Presiden Jabatan Perkhidmatan Bandar'],
            ['code' => 'NPJKP', 'name' => 'Naib Presiden Jabatan Kejuruteraan & Penyelengaraan'],
            ['code' => 'NPJKW', 'name' => 'Naib Presiden Jabatan Kewangan'],
            ['code' => 'NP', 'name' => 'Naib Presiden Perbadanan Putrajaya'],
            ['code' => 'P', 'name' => 'Presiden Perbadanan Putrajaya'],
            ['code' => 'MOF', 'name' => 'Kementerian Kewangan Malaysia'],
            ['code' => 'KWP', 'name' => 'Kementerian Wilayah Persekutuan'],
        ];

        foreach ($data as $datum) {
            Authority::create($datum);
        }
    }
}
