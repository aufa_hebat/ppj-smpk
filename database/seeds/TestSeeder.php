<?php

use App\Traits\SeedingProgressBar;
use App\Traits\TruncateTableTrait;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    use SeedingProgressBar, TruncateTableTrait;

    public $truncate = [
        'acquisition_approvals',
        'acquisitions',
        'sales',
        'companies',
        'company_owners',
        'acquisition_boxes',
        'acquisition_evaluations',
        'appointed_company',
        'ssts',
        'acquisition_activities',
        'acquisition_bons',
        'acquisition_insurances',
        'acquisition_deposits',
        'acquisition_documents',
        'acquisition_sub_contracts',
        'ipcs',
        'ipc_invoices',
        'ipc_materials',
        'ipc_bqs',
        'vo',
        'vo_actives',
        'vo_kkk_elements',
        'vo_kkk_items',
        'vo_kkk_sub_items',
        'vo_pk_elements',
        'vo_pk_items',
        'vo_pk_sub_items',
        'vo_pq_elements',
        'vo_pq_items',
        'vo_pq_sub_items',
        'vo_wps_elements',
        'vo_wps_items',
        'vo_wps_sub_items',
        'cpcs',
        'cncs',
        'cmgds',
        'site_visits',
        'tasks',
    ];

    public $data;

    protected $seeders = [
        'truncateTables' => false,
        'seedTestData'   => false,
    ];

    private function seedTestData()
    {
        collect(glob(database_path('/seeds/test/*.php')))
            ->each(function ($path) {
                $this->data = collect(require $path);
                $method = 'seedTestData' . studly_case(basename($path, '.php'));
                $this->{$method}();
            });
    }

    /**
     * @todo Clean up using Eloquent way
     */
    private function seedTestDataAcquisitionApprovals()
    {
        $str_slug_fqcn = strtolower(str_replace('\\', '-', \App\Models\Acquisition\Approval::class));
        $this->data->each(function ($datum) use ($str_slug_fqcn) {
            $approval = $datum;

            $pegawai_pelaksana = data_get($approval, 'pegawai_pelaksana');

            $user = \App\Models\User::has('department')
                ->with('department')
                ->where('name', 'like', '%' . $pegawai_pelaksana . '%')
                ->first();

            $department_id = data_get($user, 'department.id');

            if ($user && $department_id) {
                $timestamp = time() + \App\Models\Acquisition\Approval::count() + 1;
                $hashslug = hashids($str_slug_fqcn)->encode($timestamp);

                $data = [
                    data_get($approval, 'kelayakan'),
                    data_get($approval, 'nama_projek'),
                    data_get($approval, 'tahun'),
                    $department_id,
                    1,
                    3,
                    data_get($approval, 'tempoh_kontrak'),
                    10, // ppj
                    data_get($user, 'id'),
                    data_get($approval, 'no_perolehan'),
                    $hashslug,
                    now()->format('Y-m-d H:i:s'),
                    now()->format('Y-m-d H:i:s'),
                ];

                \DB::insert('insert into `acquisition_approvals` (`description`, `title`, `year`, `department_id`, `acquisition_type_id`, `period_type_id`, `period_length`, `authority_id`, `user_id`, `reference`, `hashslug`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                    $data);
            }
        });

        // generate task for each acq. approval
        \App\Models\Acquisition\Approval::all()->each(function ($approval) {
            task('pra-kontrak-kelulusan-perolehan-maklumat-kelulusan')
                ->user($approval->user_id)
                ->create($approval);
            if (isset($approval->task_is_done) && true == $approval->task_is_done) {
                task('pra-kontrak-kelulusan-perolehan-maklumat-kelulusan')
                    ->user($approval->user_id)
                    ->markAsDone($approval);
            }
        });
    }
}
