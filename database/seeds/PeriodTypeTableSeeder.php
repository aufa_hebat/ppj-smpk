<?php

use App\Models\PeriodType;
use Illuminate\Database\Seeder;

class PeriodTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            1 => 'Hari',
            2 => 'Minggu',
            3 => 'Bulan',
            4 => 'Tahun',
        ];

        foreach ($data as $key => $value) {
            PeriodType::create(['name' => $value, 'value' => $key]);
        }
    }
}
