<?php

use App\Traits\SeedingProgressBar;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use SeedingProgressBar;

    protected $seeders = [
        BonTypeTableSeeder::class               => true,
        StatusTableSeeder::class                => true,
        StandardCodesTableSeeder::class         => true,
        PeriodTypeTableSeeder::class            => true,
        DepartmentSeeder::class                 => true,
        SectionTableSeeder::class               => true,
        PositionTableSeeder::class              => true,
        SchemeTableSeeder::class                => true,
        GradeTableSeeder::class                 => true,
        BudgetCodeSeeder::class                 => true,
        BankSeeder::class                       => true,
        LocationSeeder::class                   => true,
        AuthoritySeeder::class                  => true,
        AllocationResourceTableSeeder::class    => true,
        CIDBCodeSeeder::class                   => true,
        WorkflowSeeder::class                   => true,
        AcquistionCategoryTableSeeder::class    => true,
        AcquistionTypeTableSeeder::class        => true,
        RolesAndPermissionsSeeder::class        => true,
        DefaultUserSeeder::class                => true,
        InsuranceSeeder::class                  => true,
        VariationOrderSeeder::class             => true,
        RefIpcChecklistSeeder::class            => true,
        RefDocumentChecklistSeeder::class       => true,
        TerminationTypeSeeder::class            => true,
        DefectLiabilityPeriodTableSeeder::class => true,
        MOFCodeSeeder::class                   => true,
    ];
}
