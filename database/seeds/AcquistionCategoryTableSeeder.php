<?php

use App\Models\Acquisition\Category;
use Illuminate\Database\Seeder;

class AcquistionCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Sebut Harga B',
            'Sebut Harga',
            'Tender Terbuka',
            'Tender Terhad',
            'Reka dan Bina',
            'Rundingan Terus',
            'RFI/RFP',
            'Perunding',
        ];

        foreach ($data as $datum) {
            Category::create(['name' => $datum]);
        }
    }
}
