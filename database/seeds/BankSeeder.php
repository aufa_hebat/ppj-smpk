<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Affin Islamic Bank Berhad',
            'Al Rajhi Banking & Investment Corporation (Malaysia) Berhad',
            'Alliance Islamic Bank Berhad',
            'AmBank Islamic Berhad',
            'Asian Finance Bank Berhad',
            'Bank Islam Malaysia Berhad',
            'Bank Kerjasama Rakyat Malaysia Berhad',
            'Bank Muamalat Malaysia Berhad',
            'Bank of America',
            'Bank of China(Malaysia) Berhad',
            'Bank of Tokyo Mitsubishi UFJ (M) Berhad',
            'Bank Pertanian Malaysia Berhad (Agrobank)',
            'Bank Simpanan Nasional',
            'BNP Paribas Malaysia',
            'CIMB Islamic Bank Berhad',
            'Citibank Berhad',
            'Deutsche Bank (Malaysia) Berhad',
            'Hong Leong Islamic Bank Berhad',
            'HSBC Amanah Malaysia Berhad',
            'Industrial & Commercial Bank of China',
            'J.P. Morgan Chase Bank Berhad',
            'Kuwait Finance House (Malaysia) Berhad',
            'Mizuho Corperate Bank Malaysia',
            'Maybank Islamic Berhad',
            'OCBC Al-Amin Bank Berhad',
            'Public Islamic Bank Berhad',
            'RHB Islamic Bank Berhad',
            'Standard Chartered Saadiq Berhad',
            'Sumitomo Mitsui Banking Corporation Malaysia BHD',
            'United Overseas Bank Berhad',
        ];

        foreach ($data as $datum) {
            \App\Models\Bank::create(['name' => $datum]);
        }
    }
}
