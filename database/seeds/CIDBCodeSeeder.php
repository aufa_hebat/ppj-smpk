<?php

use App\Models\CIDBCode;
use Illuminate\Database\Seeder;

class CIDBCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['code' => 'G1', 'name' => 'Gred G1', 'category' => '', 'type' => 'grade'],
            ['code' => 'G2', 'name' => 'Gred G2', 'category' => '', 'type' => 'grade'],
            ['code' => 'G3', 'name' => 'Gred G3', 'category' => '', 'type' => 'grade'],
            ['code' => 'G4', 'name' => 'Gred G4', 'category' => '', 'type' => 'grade'],
            ['code' => 'G5', 'name' => 'Gred G5', 'category' => '', 'type' => 'grade'],
            ['code' => 'G6', 'name' => 'Gred G6', 'category' => '', 'type' => 'grade'],
            ['code' => 'G7', 'name' => 'Gred G7', 'category' => '', 'type' => 'grade'],
            ['code' => 'B', 'name' => 'BANGUNAN (B)', 'category' => '', 'type' => 'category'],
            ['code' => 'B01', 'name' => 'IBS: Sistem konkrit pasang', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B02', 'name' => 'IBS: Sistem kerangka keluli', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B03', 'name' => 'Pemulihan dan pemuliharaan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B04', 'name' => 'Kerja-kerja pembinaan bangunan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B05', 'name' => 'Kerja cerucuk', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B06', 'name' => 'Kerja pembaikan struktur konkrit', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B07', 'name' => 'Hiasan dalaman', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B08', 'name' => 'Pemasangan bahan kalis air', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B09', 'name' => 'Lanskap dalam bangunan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B10', 'name' => 'Sistem paip air dalaman', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B11', 'name' => 'Pemasangan papan tanda pada bangunan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B12', 'name' => 'Kerja pemasangan kaca', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B13', 'name' => 'Pemasangan jubin', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B14', 'name' => 'Kerja-kerja cat', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B15', 'name' => 'Pemasangan bumbung', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B16', 'name' => 'Kolam renang', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B17', 'name' => 'Kerja prestressing dan post tensioning', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B18', 'name' => 'Kerja-kerja logam', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B19', 'name' => 'IBS: Sistem formwork', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B20', 'name' => 'Sistem paip gas dalam bangunan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B21', 'name' => 'Pemasangan perancah', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B22', 'name' => 'IBS: Sistem blok', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B23', 'name' => 'IBS: Sistem kerangka kayu', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B24', 'name' => 'Kerja penyenggaraan bangunan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B25', 'name' => 'Penyambungan paip persendirian ke pembetung (sewerage)', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B26', 'name' => 'Kerja-kerja meroboh', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B27', 'name' => 'Perkhidmatan penyengaraan sistem bekalan air atau sistem pembetungan', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'B28', 'name' => 'Kerja-kerja ubahsuai', 'category' => 'B', 'type' => 'khusus'],
            ['code' => 'CE', 'name' => 'KEJURUTERAAN AWAM (CE)', 'category' => '', 'type' => 'category'],
            ['code' => 'CE01', 'name' => 'Pembinaan jalan dan pavemen', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE02', 'name' => 'Pembinaan Jambatan dan jeti', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE03', 'name' => 'Struktur merin', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE04', 'name' => 'Empangan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE05', 'name' => 'Terowong dan sokong bawah', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE06', 'name' => 'Struktur saliran, pengaliran dan kawalan banjir', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE07', 'name' => 'Landasan rel', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE08', 'name' => 'Sistem perlindungan dan penstabilan cerun', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE09', 'name' => 'Saluran utama paip minyak atau gas', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE10', 'name' => 'Kerja cerucuk', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE11', 'name' => 'Kerja Pembaikan struktur konkrit', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE12', 'name' => 'Kerja penyiasatan tanah', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE13', 'name' => 'Pemasangan papan iklan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE14', 'name' => 'Landskap diluar bangunan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE15', 'name' => 'Pelantar minyak dan gas', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE16', 'name' => 'Kerja-kerja penyenggaraan struktur dibawah air', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE17', 'name' => 'Lapangan terbang', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE18', 'name' => 'Tebusguna tanah', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE19', 'name' => 'Sistem pembentungan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE20', 'name' => 'Sistem bekalan air', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE21', 'name' => 'Pembinaan kejuruteraan awam', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE22', 'name' => 'Trek padang permainan sintetik', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE23', 'name' => 'Kerja prestressing dan post tensioning', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE24', 'name' => 'Struktur menara', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE25', 'name' => 'Kerja-kerja meletup', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE26', 'name' => 'Struktur berukir (Sculptured structures)', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE27', 'name' => 'Kerja penebatan haba/refractory', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE28', 'name' => 'Acuan konkrit khusus', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE29', 'name' => 'Pemasangan perancah E29 Kerja-Kerja Mencantum Kabel 33KV Sehingga 66KV', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE30', 'name' => 'Kerja-kerja pensatabilan tanah E30 Kerja-Kerja Mencantum Kabel Sehingga 132KV', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE31', 'name' => 'Struktur laluan kabel bawah tanah', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE32', 'name' => 'Kerja penyenggaraan kejuruteraan awam', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE33', 'name' => 'Telaga tiub E33 Janakuasa Voltan Tinggi Sehingga 33KV', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE34', 'name' => 'Kerja pemasangan konkrit pratuang', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE35', 'name' => 'Ujian konkrit', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE36', 'name' => 'Kerja-kerja tanah', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE37', 'name' => 'Kerja serombong stesen kuasa', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE38', 'name' => 'Penyenggaraan sistem pembetungan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE39', 'name' => 'Penyelengaraan sistem bekalan air', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE40', 'name' => 'Kerja-kerja pengorekan dan kawalan hakisan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE41', 'name' => 'Kerja membina takungan air', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE42', 'name' => 'Kerja-kerja mengecat jalan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'CE43', 'name' => 'Perabot jalan', 'category' => 'CE', 'type' => 'khusus'],
            ['code' => 'M', 'name' => 'MEKANIKAL (M)', 'category' => '', 'type' => 'category'],
            ['code' => 'M01', 'name' => 'Sistem hawa dingin dan pengedaran udara', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M02', 'name' => 'Sistem pencegahan dan perlindungan Kebakaran', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M03', 'name' => 'Lift dan eskalator', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M04', 'name' => 'Sistem automasi bangunan', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M05', 'name' => 'Sistem untuk bengkel, kilang kuari dan sebagainya', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M06', 'name' => 'Sistem peralatan perubatan', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M07', 'name' => 'Sistem peralatan dapur', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M08', 'name' => 'Sistem loji dandang dan tekanan berapi', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M09', 'name' => 'Sistem pemampatan dan penjanaan berasaskan mekanikal', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M10', 'name' => 'Sistem pendingin untuk kuasa penjanaan', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M11', 'name' => 'Pembinaan dan rawatan khusus', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M12', 'name' => 'Loji khusus', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M13', 'name' => 'Struktur pengerudian luar pantai', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M14', 'name' => 'Sistem kawalan pencemaran', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M15', 'name' => 'Kelengkapan mekanikal pelbagai', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M16', 'name' => 'Kren menara', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M17', 'name' => 'Sistem Peralatan dobi', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M18', 'name' => 'Sistem air panas', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M19', 'name' => 'Pemasangan kelengkapan loji', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M20', 'name' => 'Penyenggaraan am mekanikal', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M21', 'name' => 'Kerja-kerja kimpalan', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M22', 'name' => 'Sistem Pam', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'M23', 'name' => 'Sistem SCADA dan Telemetri', 'category' => 'M', 'type' => 'khusus'],
            ['code' => 'E', 'name' => 'ELEKTRIK (E)', 'category' => '', 'type' => 'category'],
            ['code' => 'E01', 'name' => 'Sistem bunyi', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E02', 'name' => 'Sistem pengawasan dan keselamatan', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E03', 'name' => 'Sistem automasi bangunan', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E04', 'name' => 'Pemasangan voltan rendah', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E05', 'name' => 'Pemasangan voltan tinggi sehingga 11KV', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E06', 'name' => 'Sistem pencahayaan khas', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E07', 'name' => 'Sistem telekomunikasi dalaman', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E08', 'name' => 'Sistem telekomunikasi luaran', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E09', 'name' => 'Sistem pemasangan peralatan perubatan', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E10', 'name' => 'Sistem bekalan kuasa tanpa gangguan', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E11', 'name' => 'Kerja am elektrik', 'category' => 'E', 'type' => 'khusus'],
            ['code' => 'E12', 'name' => 'Papan tanda elektrik', 'category' => 'E', 'type' => 'khusus'],
        ];

        foreach ($data as $key => $value) {
            CIDBCode::create($value);
        }
    }
}
