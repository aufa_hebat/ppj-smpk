<?php

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['code' => '1', 'name' => 'Presint 1'],
            ['code' => '2', 'name' => 'Presint 2'],
            ['code' => '3', 'name' => 'Presint 3'],
            ['code' => '4', 'name' => 'Presint 4'],
            ['code' => '5', 'name' => 'Presint 5'],
            ['code' => '6', 'name' => 'Presint 6'],
            ['code' => '7', 'name' => 'Presint 7'],
            ['code' => '8', 'name' => 'Presint 8'],
            ['code' => '9', 'name' => 'Presint 9'],
            ['code' => '10', 'name' => 'Presint 10'],
            ['code' => '11', 'name' => 'Presint 11'],
            ['code' => '12', 'name' => 'Presint 12'],
            ['code' => '13', 'name' => 'Presint 13'],
            ['code' => '14', 'name' => 'Presint 14'],
            ['code' => '15', 'name' => 'Presint 15'],
            ['code' => '16', 'name' => 'Presint 16'],
            ['code' => '17', 'name' => 'Presint 17'],
            ['code' => '18', 'name' => 'Presint 18'],
            ['code' => '19', 'name' => 'Presint 19'],
            ['code' => '20', 'name' => 'Presint 20'],
            ['code' => '21', 'name' => 'Keseluruhan Putrajaya'],
        ];

        foreach ($data as $key => $value) {
            Location::create($value);
        }
    }
}
