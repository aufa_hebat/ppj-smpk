<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['name' => 'Aktif', 'value' => 1],
            ['name' => 'Batal', 'value' => 2],
            ['name' => 'Ditamatkan', 'value' => 3],
        ];

        foreach ($data as $datum) {
            \App\Models\Status::create($datum);
        }
    }
}
