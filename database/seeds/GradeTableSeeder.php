<?php

use Illuminate\Database\Seeder;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['name' => '5',	'gred_int' => 5],
            ['name' => '7',	'gred_int' => 7],
            ['name' => '11',	'gred_int' => 11],
            ['name' => '14',	'gred_int' => 14],
            ['name' => '17',	'gred_int' => 17],
            ['name' => '19',	'gred_int' => 19],
            ['name' => '22',	'gred_int' => 22],
            ['name' => '26',	'gred_int' => 26],
            ['name' => '27',	'gred_int' => 27],
            ['name' => '28',	'gred_int' => 28],
            ['name' => '29',	'gred_int' => 29],
            ['name' => '30',	'gred_int' => 30],
            ['name' => '32',	'gred_int' => 32],
            ['name' => '36',	'gred_int' => 36],
            ['name' => '38',	'gred_int' => 38],
            ['name' => '41',	'gred_int' => 41],
            ['name' => '42',	'gred_int' => 42],
            ['name' => '44',	'gred_int' => 44],
            ['name' => '48',	'gred_int' => 48],
            ['name' => '52',	'gred_int' => 52],
            ['name' => '54',	'gred_int' => 54],
            ['name' => 'A',	'gred_int' => 703],
            ['name' => 'B',	'gred_int' => 702],
            ['name' => 'C',	'gred_int' => 701],
        ];

        foreach ($data as $key => $value) {
            \App\Models\Grade::create($value);
        }
    }
}
