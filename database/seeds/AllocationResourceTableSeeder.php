<?php

use App\Models\Acquisition\AllocationResource;
use Illuminate\Database\Seeder;

class AllocationResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['code' => 'PPJ', 'name' => 'Perbadanan Putrajaya'],
            ['code' => 'KWP', 'name' => 'Kementerian Wilayah Persekutuan'],
            ['code' => 'LL', 'name' => 'Lain-Lain'],
        ];

        foreach ($data as $datum) {
            AllocationResource::create($datum);
        }
    }
}
