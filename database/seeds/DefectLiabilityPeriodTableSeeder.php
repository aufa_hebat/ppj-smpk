<?php

use Illuminate\Database\Seeder;

class DefectLiabilityPeriodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            [
                'name'        => 1,
                'description' => '6 bulan',
                'value'       => 6,
            ],
            [
                'name'        => 2,
                'description' => '12 bulan',
                'value'       => 12,
            ],
        ];

        foreach ($data as $datum) {
            \App\Models\DefectLiabilityPeriod::create($datum);
        }
    }
}
