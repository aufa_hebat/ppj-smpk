<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            ['category' => 'JA', 'code' => 'JA01', 'name' => 'Audit Dalam'],
            ['category' => 'JU', 'code' => 'JU01', 'name' => 'Undang-Undang'],
            ['category' => 'JK', 'code' => 'JK01', 'name' => 'Pentadbiran'],
            ['category' => 'JK', 'code' => 'JK02', 'name' => 'Teknologi Maklumat Dan Komunikasi'],
            ['category' => 'JK', 'code' => 'JK03', 'name' => 'Komunikasi Korporat'],
            ['category' => 'JK', 'code' => 'JK04', 'name' => 'Sumber Manusia'],
            ['category' => 'JK', 'code' => 'JK05', 'name' => 'Pengurusan Strategik'],
            ['category' => 'JR', 'code' => 'JR01', 'name' => 'Komuniti, Pembangunan Perniagaan Dan Pelesenan'],
            ['category' => 'JR', 'code' => 'JR02', 'name' => 'Komuniti'],
            ['category' => 'JR', 'code' => 'JR03', 'name' => 'Penguatkuasa'],
            ['category' => 'JR', 'code' => 'JR04', 'name' => 'Kesihatan Persekitaran'],
            ['category' => 'JP', 'code' => 'JP01', 'name' => 'Pembangunan Tanah Dan Kelulusan Pelan'],
            ['category' => 'JP', 'code' => 'JP02', 'name' => 'Alam Sekitar, Tasik Dan Wetland'],
            ['category' => 'JP', 'code' => 'JP03', 'name' => 'Seni Bina Dan Inspektorat Bangunan'],
            ['category' => 'JP', 'code' => 'JP04', 'name' => 'Pembangunan Mampan'],
            ['category' => 'JL', 'code' => 'JL01', 'name' => 'Taman Dan Rekreasi'],
            ['category' => 'JL', 'code' => 'JL02', 'name' => 'Interpretif Dan Pendidikan Taman'],
            ['category' => 'JL', 'code' => 'JL03', 'name' => 'Pengurusan Dan Kawalan Landskap'],
            ['category' => 'JL', 'code' => 'JL04', 'name' => 'Botani'],
            ['category' => 'JB', 'code' => 'JB01', 'name' => 'Pengurusan Projek'],
            ['category' => 'JB', 'code' => 'JB02', 'name' => 'Jalan'],
            ['category' => 'JB', 'code' => 'JB03', 'name' => 'Pengurusan Fasiliti'],
            ['category' => 'JW', 'code' => 'JW01', 'name' => 'Pengurusan Kewangan'],
            ['category' => 'JW', 'code' => 'JW02', 'name' => 'Penilaian Dan Pengurusan Aset'],
            ['category' => 'JW', 'code' => 'JW03', 'name' => 'Perolehan Dan Ukur Bahan'],
            ['category' => 'JW', 'code' => 'JW04', 'name' => 'Pelaburan Dan Pengurusan Anak Syarikat'],
            ['category' => 'JU01', 'code' => 'JU0101', 'name' => 'Pendakwaan Dan Litigasi'],
            ['category' => 'JK01', 'code' => 'JK0101', 'name' => 'Pentadbiran Am'],
            ['category' => 'JK01', 'code' => 'JK0102', 'name' => 'Keselamatan Dan Pengurusan Stor'],
            ['category' => 'JK02', 'code' => 'JK0201', 'name' => 'Keselamatan ,Infrastruktur ICT'],
            ['category' => 'JK02', 'code' => 'JK0202', 'name' => 'Sistem Pengurusan Maklumat'],
            ['category' => 'JK02', 'code' => 'JK0203', 'name' => 'Portal Dan Multimedia'],
            ['category' => 'JK03', 'code' => 'JK0301', 'name' => 'Acara, Kesenian Dan Pelancongan'],
            ['category' => 'JK03', 'code' => 'JK0302', 'name' => 'Komunikasi Strategik'],
            ['category' => 'JK04', 'code' => 'JK0401', 'name' => 'Pengurusan Sumber Manusia'],
            ['category' => 'JK04', 'code' => 'JK0402', 'name' => 'Pembangunan Sumber Manusia'],
            ['category' => 'JK04', 'code' => 'JK0403', 'name' => 'Integriti'],
            ['category' => 'JK05', 'code' => 'JK0501', 'name' => 'Perancangan Strategik'],
            ['category' => 'JK05', 'code' => 'JK0502', 'name' => 'Pengurusan Kualiti'],
            ['category' => 'JR01', 'code' => 'JR0101', 'name' => 'Pembangunan Perniagaan'],
            ['category' => 'JR02', 'code' => 'JR0201', 'name' => 'Pusat Pembelajaran Kejiranan'],
            ['category' => 'JR02', 'code' => 'JR0202', 'name' => 'Sukan Dan Rekreasi'],
            ['category' => 'JR02', 'code' => 'JR0203', 'name' => 'Pengurusan Kompleks Kejiranan Dan Kemudahan Komuniti'],
            ['category' => 'JR03', 'code' => 'JR0301', 'name' => 'Penguatkuasaan'],
            ['category' => 'JR03', 'code' => 'JR0302', 'name' => 'Berkuda'],
            ['category' => 'JR03', 'code' => 'JR0303', 'name' => 'Keselamatan Jalan Raya Dan Depo'],
            ['category' => 'JR04', 'code' => 'JR0401', 'name' => 'Kawalan Keselamatan Makanan'],
            ['category' => 'JR04', 'code' => 'JR0402', 'name' => 'Kawalan Kebersihan'],
            ['category' => 'JR04', 'code' => 'JR0403', 'name' => 'Kawalan Vektor Dan Persekitaran'],
            ['category' => 'JP01', 'code' => 'JP0101', 'name' => 'Urusetia Pusat Setempat'],
            ['category' => 'JP01', 'code' => 'JP0102', 'name' => 'Kelulusan Perancangan Bandar'],
            ['category' => 'JP01', 'code' => 'JP0103', 'name' => 'Kelulusan Infra'],
            ['category' => 'JP01', 'code' => 'JP0104', 'name' => 'Kelulusan Landskap'],
            ['category' => 'JP01', 'code' => 'JP0105', 'name' => 'Kawalan Perancangan Rekabentuk Dan Permit'],
            ['category' => 'JP01', 'code' => 'JP0106', 'name' => 'Pengurusan Tanah Dan Gis'],
            ['category' => 'JP01', 'code' => 'JP0107', 'name' => 'PPA1M'],
            ['category' => 'JP02', 'code' => 'JP0201', 'name' => 'Pengurusan Ekosistem Bandar'],
            ['category' => 'JP02', 'code' => 'JP0202', 'name' => 'Pengurusan Alam Sekitar'],
            ['category' => 'JP03', 'code' => 'JP0301', 'name' => 'Inspektorat Bangunan'],
            ['category' => 'JP03', 'code' => 'JP0302', 'name' => 'Bangunan Hijau, Penyelidikan Dan Penerbitan Senibina'],
            ['category' => 'JP03', 'code' => 'JP0303', 'name' => 'Rekabentuk Sejagat, Akses Audit Dan Pengurusan Maklumat Senibina'],
            ['category' => 'JP04', 'code' => 'JP0401', 'name' => 'Local Agenda 21 (La21) Putrajaya'],
            ['category' => 'JP04', 'code' => 'JP0402', 'name' => 'Bandar Hijau Putrajaya'],
            ['category' => 'JP04', 'code' => 'JP0403', 'name' => 'Rancangan Pemajuan Dan Penyelidikan'],
            ['category' => 'JP04', 'code' => 'JP0404', 'name' => 'Bandar Selamat'],
            ['category' => 'JB01', 'code' => 'JB0101', 'name' => 'Perkhidmatan  Kejuruteraan  Awam  Dan Struktur'],
            ['category' => 'JB01', 'code' => 'JB0102', 'name' => 'Senibina'],
            ['category' => 'JB01', 'code' => 'JB0103', 'name' => 'Perkhidmatan Kejuruteraan  Mekanikal , Elektrik'],
            ['category' => 'JB02', 'code' => 'JB0201', 'name' => 'Jalan'],
            ['category' => 'JB02', 'code' => 'JB0202', 'name' => 'Utiliti'],
            ['category' => 'JB03', 'code' => 'JB0301', 'name' => 'Senggara Bangunan'],
            ['category' => 'JB03', 'code' => 'JB0302', 'name' => 'Perkhidmatan Bangunan'],
            ['category' => 'JL01', 'code' => 'JL0101', 'name' => 'Persekitaran Hijau / La21'],
            ['category' => 'JL01', 'code' => 'JL0102', 'name' => 'Perancangan Dan Pembangunan Taman'],
            ['category' => 'JL01', 'code' => 'JL0103', 'name' => 'Hortikultur Dan Hidupan Liar'],
            ['category' => 'JL02', 'code' => 'JL0201', 'name' => 'Seksyen Operasi Dan Interpretif Taman'],
            ['category' => 'JL02', 'code' => 'JL0202', 'name' => 'Seksyen Pemasaran Dan Pendidikan Taman'],
            ['category' => 'JL03', 'code' => 'JL0301', 'name' => 'Seksyen Taman'],
            ['category' => 'JL03', 'code' => 'JL0302', 'name' => 'Seksyen Jalan Protokol Dan Pokok Ameniti'],
            ['category' => 'JL03', 'code' => 'JL0303', 'name' => 'Seksyen Boulevard Dan Promenade'],
            ['category' => 'JL03', 'code' => 'JL0304', 'name' => 'Seksyen Presint Kejiranan'],
            ['category' => 'JL03', 'code' => 'JL0305', 'name' => 'Seksyen Kawalan Landskap'],
            ['category' => 'JL04', 'code' => 'JL0401', 'name' => 'Seksyen Konservasi Tumbuhan Hidup Dan Pengurusan Herbarium'],
            ['category' => 'JL04', 'code' => 'JL0402', 'name' => 'Seksyen Pembelajaran Dan Rekreasi Botani'],
            ['category' => 'JW01', 'code' => 'JW0101', 'name' => 'Pembayaran Dan Hasil'],
            ['category' => 'JW02', 'code' => 'JW0201', 'name' => 'Penilaian'],
            ['category' => 'JW02', 'code' => 'JW0202', 'name' => 'Pengurusan Aset'],
            ['category' => 'JW02', 'code' => 'JW0203', 'name' => 'COB'],
            ['category' => 'JW03', 'code' => 'JW0301', 'name' => 'Tender Dan Kontrak'],
            ['category' => 'JW03', 'code' => 'JW0302', 'name' => 'Ukur Bahan'],
            ['category' => 'JW03', 'code' => 'JW0303', 'name' => 'Perolehan Belakan Dan Perkhidmatan'],
        ];

        foreach ($data as $key => $value) {
            \App\Models\Section::create($value);
        }
    }
}
