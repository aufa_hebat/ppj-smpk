<?php

use App\Models\VariationOrderType;
use Illuminate\Database\Seeder;

class VariationOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Aktifkan Wang Peruntukkan Sementara',
            'Perubahan Kerja',
            'Wang Peruntukkan Sementara dan Kos Prima',
            'Pengiraan Semula Kuantiti Sementara (Provisional Quantity)',
            'Kesilapan Keterangan Kerja dan Kuantiti',
            'Wang Peruntukkan Sementara Tidak Digunakan',
        ];

        foreach ($data as $datum) {
            VariationOrderType::updateOrCreate([
                'name' => $datum,
            ]);
        }
    }
}
