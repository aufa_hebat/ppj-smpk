<?php

use Illuminate\Database\Seeder;

class RefDocumentChecklistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Sijil Bayaran Interim (IPC), Ringkasan Bayaran, Status Bayaran Interim, Butiran Bayaran**',
            'Surat Tuntutan/Inbois/Inbois Cukai (perlu ada bukti Cop Pemerimaan oleh Jabatan)**',
            'Salinan Surat Setuju Terima (LOA)**',
            'Salinan Polisi Insurans**',
            'Salinan Resit Premium INsurans (Sekiranya Polisi masih belum  diperolehi)*',
            'Salinan Bon Pelaksanaan*',
            'Surat Syarikat Pemfaktoran (Factoring Company Letter), Perjanjian yang telah diluluskan oleh Jabatan Kewangan & Tax Invoise(perlu ada cop oleh Factoring Company)*',
            'Salinan Bill Of Quantities(BQ)**',
            'Pendaftaran Pertubuhan Keselamatan Sosial (PERKESO)**',
            'Pendaftaran Cukai Barang & Perkhidmatan (GST)*',
            'Salinan Resit Levi (untuk tender kerja RM500,000 ke atas sahaja)**',
        ];

        foreach ($data as $datum) {
            \App\Models\RefDocumentChecklist::create(['name' => $datum]);
        }
    }
}
