<?php

use App\Models\TerminationType;
use Illuminate\Database\Seeder;

class TerminationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
             ['code' => 'T001', 'name' => 'Kemungkiran Oleh Kontraktor'],
             ['code' => 'T002', 'name' => 'Penamatan Bersama Disebabkan Penangguhan'],
             ['code' => 'T003', 'name' => 'Penamatan Disebabkan Kepentingan Negara'],
             ['code' => 'T004', 'name' => 'Penamatan Disebabkan Rasuah'],
             ['code' => 'T005', 'name' => 'Lain-lain'],
         ];

        foreach ($data as $datum) {
            TerminationType::create($datum);
        }
    }
}
