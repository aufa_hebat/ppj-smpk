<?php

use Illuminate\Database\Seeder;

class BonTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            [
                'code'      => 1,
                'name'      => 'Jaminan Bank',
                'is_active' => true,
            ],
            [
                'code'      => 2,
                'name'      => 'Jaminan Insuran',
                'is_active' => true,
            ],
            [
                'code'      => 3,
                'name'      => 'Bank Deraf (Banker\'s Cheque)',
                'is_active' => true,
            ],
            [
                'code'      => 4,
                'name'      => 'Wang Jaminan Pelaksanaan',
                'is_active' => true,
            ],
        ];

        foreach ($data as $datum) {
            \App\Models\BonType::create($datum);
        }
    }
}
