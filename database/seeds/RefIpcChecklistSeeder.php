<?php

use Illuminate\Database\Seeder;

class RefIpcChecklistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Butiran Maklumat Kontrak**',
            'Perakuan Akaun Dan Bayaran Muktamad**',
            'Sijil Bayaran Interim (IPC Akhir)',
            'Ringkasan Bayaran, Status Bayaran Interim, Butiran Bayaran**',
            'Surat Tuntutan/Inbois/Inbois Cukai (perlu ada bukti Cop Pemerimaan oleh Jabatan)**',
            'Perakuan Pelarasan Jumlah Harga Kontrak (PPJHK)',
            'Salinan Sijil Siap Kerja (CPC) Salinan Perakuan Siap Kerja-Kerja Penyelenggaraan (CCMW)',
            'Salinan Lanjutan Masa (EOT)',
            'Salinan Sijil Tidak Siap Kerja/CNC/LAD',
            'Salinan Sijil Siap Membaiki Kecacatan (CMGD)',
            'Surat Pengecualian LAD',
            'Salinan Sijil-Sijil IPC terdahulu yang telah dibayar**',
            'Akuan Statutori(Akuan Bersumpah)**',
        ];

        foreach ($data as $datum) {
            \App\Models\RefIpcChecklist::create(['name' => $datum]);
        }
    }
}
