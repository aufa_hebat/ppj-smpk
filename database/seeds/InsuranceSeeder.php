<?php

use Illuminate\Database\Seeder;

class InsuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'AIG MALAYSIA INSURANCE BERHAD',
            'AXA AFFIN GENERAL INSURANCE COMPANY (MALAYSIA) BERHAD',
            'ALLIANZ GENERAL INSURANCE COMPANY (MALAYSIA) BERHAD',
            'AMGENERAL INSURANCE BERHAD',
            'BERJAYA SOMPO INSURANCE BERHAD',
            'CHUBB INSURANCE MALAYSIA BERHAD',
            'DANAJAMIN NASIONAL BERHAD',
            'GREAT EASTERN GENERAL INSURANCE (MALAYSIA) BERHAD',
            'LIBERTY INSURANCE BERHAD',
            'LONPAC INSURANCE BERHAD',
            'MPI GENERALI INSURANS BERHAD',
            'MSIG INSURANCE (MALAYSIA) BHD',
            'PACIFIC & ORIENT INSURANCE CO. BERHAD',
            'PACIFIC INSURANCE BERHAD',
            'PROGRESSIVE INSURANCE BERHAD',
            'QBE INSURANCE (MALAYSIA) BERHAD',
            'RHB INSURANCE BERHAD',
            'TOKIO MARINE INSURANCE (MALAYSIA) BERHAD',
            'TUNE INSURANCE MALAYSIA BERHAD',
            'ZURICH GENERAL INSURANCE MALAYSIA BERHAD',
        ];

        foreach ($data as $datum) {
            \App\Models\Insurance::create(['name' => $datum]);
        }
    }
}
