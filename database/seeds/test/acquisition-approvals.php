<?php


return [
    [
        'no_perolehan'       => 'PPJ/JB/SH(S)/28/2017',
        'nama_projek'        => 'CADANGAN PEMBINAAN 8 UNIT GERAI PENJAJA DAN LAIN-LAIN KERJA BERKAITAN DI SEBAHAGIAN REZAB KAWASAN KOLAM TADAHAN AIR PRESINT 14, WILAYAH PERSEKUTUAN PUTRAJAYA.',
        'gred_cidb'          => 'G2',
        'tahun'              => 2017,
        'jabatan_pelaksana'  => 'Kejuruteraan Dan Penyelenggaraan',
        'pegawai_pelaksana'  => 'HALINA MAJMIN BINTI ABDUL HALIM',
        'kelayakan'          => 'Sijil Perakuan Pendaftaran Kontraktor Gred G2, Kategori B Pengkhususan B04 DAN Kategori CE, Pengkhususan CE21, Sijil Perolehan Kerja Kerajaan (SPKK), DAN Sijil Taraf Bumiputera  Serta Surat Wakil Kuasa yang ditandatangani oleh PEMILIK SYARIKAT',
        'nama_penyelia_bpub' => 'Muhammad Fadzli Yahaya',
        'tempoh_kontrak'     => '6', // BULAN
        'kuasa_melulus'      => 'P', // PPJ
    ],
    [
        'no_perolehan'       => 'PPJ/T/JK/30/2017',
        'nama_projek'        => 'KERJA-KERJA MENAIKTARAF, MEMBEKAL, MEMASANG DAN MENGUJI-TERIMA SISTEM KAMERA LITAR TERTUTUP (CCTV) UNTUK PUTRAJAYA COMMAND CENTER DI PERBADANAN PUTRAJAYA SECARA TERDER TERBUKA',
        'gred_cidb'          => 'G2',
        'tahun'              => 2017,
        'jabatan_pelaksana'  => 'Kejuruteraan Dan Penyelenggaraan',
        'pegawai_pelaksana'  => 'MUSABRI',
        'kelayakan'          => 'Berdaftar dengan Kementerian Kewangan di bawah kod bidang 210102, 210104, 210105, 210106 DAN 220402,  Serta Surat Wakil Kuasa yang ditandatangani oleh PEMILIK SYARIKAT',
        'nama_penyelia_bpub' => 'Noor Aishah Othman',
        'tempoh_kontrak'     => '42', // BULAN
        'kuasa_melulus'      => 'P',
    ],
];
