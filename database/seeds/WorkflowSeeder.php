<?php

use App\Models\Workflow;
use Illuminate\Database\Seeder;

class WorkflowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Pra-Kontrak' => [
                'Kelulusan Perolehan' => [
                    'Maklumat Perolehan',
                    'Maklumat Kelayakan',
                    'Maklumat Kewangan',
                    'Maklumat Kelulusan',
                ],
                'Dokumen Perolehan' => [
                    'Maklumat Kelulusan Perolehan',
                    'Maklumat Sebut Harga',
                    'Senarai Kuantiti (BQ)',
                    'Semakan Jabatan Pelaksana',
                    'Semakan BPUB',
                    'Semakan Undang-Undang',
                ],
                'Taklimat' => [
                    'Daftar Kehadiran Taklimat',
                ],
                'Lawatan Tapak' => [
                    'Daftar Pegawai Bertugas',
                ],
                'Jualan' => [
                    'Penjualan Dokumen',
                ],
                'Buka Peti' => [
                    'Maklumat Buka Peti',
                ],
                'Kelulusan Perolehan' => [
                    'Maklumat Kelulusan',
                ],
                'Jawatankuasa Pembuka' => [
                    'Maklumat Ahli Jawatankuasa',
                ],
                'JawatanKuasa Penilai' => [
                    'Maklumat Ahli Jawatankuasa',
                ],
                'Pembatalan Perolehan' => [
                    'Maklumat Pembatalan Perolehan',
                ],
            ],
            'Post-Kontrak' => [
                'Surat Setuju Terima (SST)' => [
                    'Surat Setuju Terima (SST)',
                    'Semakan Jabatan Pelaksana',
                    'Semakan BPUB',
                    'Semakan Undang-Undang',
                ],
                'Dokumen Kontrak' => [
                    'Bon Pelaksanaan',
                    'Insuran',
                    'Pelarasan Harga (BQ)',
                    'Subkontraktor',
                    'Wang Pendahuluan',
                    'Penyediaan Dokumen',
                    'Semakan Bon - Jabatan Pelaksana',
                    'Semakan Bon - BPUB',
                    'Semakan Bon - JU',
                    'Semakan Insuran - Jabatan Pelaksana',
                    'Semakan Insuran - BPUB',
                    'Semakan Insuran - JU',
                    'Semakan Penyediaan Dokumen Kontrak - Pelaksana',
                    'Semakan Penyediaan Dokumen Kontrak - BPUB',
                    'Semakan Penyediaan Dokumen Kontrak - JU',
                    'Semakan Wang Pendahuluan - Pelaksana',
                    'Semakan Wang Pendahuluan - BPUB',
                    'Semakan Wang Pendahuluan - JU',
                ],
                'Sijil Siap Kerja (CPC)' => [
                    'Semakan Sijil - Jabatan Pelaksana',
                    'Semakan Sijil - BPUB',
                ],
                'Sijil Tidak Siap Kerja (CNC)' => [
                    'Semakan Sijil - Jabatan Pelaksana',
                    'Semakan Sijil - BPUB',
                ],
                'Sijil Membaiki Kecacatan (CMGD)' => [
                    'Semakan Sijil - Jabatan Pelaksana',
                    'Semakan Sijil - BPUB',
                ],
                'Sijil Separa Siap (CPO) Semakan Sijil' => [
                    'Jabatan Pelaksana',
                    'Semakan Sijil - BPUB',
                ],
                'Sijil Bayaran Interim' => [
                    'Semakan Jabatan Pelaksana',
                    'Semakan BPUB',
                    'Semakan BPK',
                ],
                'Perubahan Kerja (VO)-PPK' => [
                    'Semakan Jabatan Pelaksana',
                    'Semakan BPUB',
                ],
                'Perubahan Kerja (VO)-PPJHK' => [
                    'Semakan Jabatan Pelaksana',
                    'Semakan BPUB',
                ],
                'Perakuan Kelambatan dan Pelanjutan Masa (EOT)' => [
                    'Semakan Jabatan Pelaksana',
                    'Semakan BPUB',
                ],
                'Pemantauan Projek Fizikal' => [
                    'Pemantauan Projek (Penjanaan Graf)',
                    'CPC (Sijil Siap Kerja)',
                    'CMGD (Sijil Siap Membaiki Kecacatan)',
                    'CNC (Sijil Tak Siap Kerja)',
                    'NCC/NCR (Sijil Ketidak Patuhan)',
                    'Perubahan Kerja (VO)',
                    'Pelanjutan Tempoh(EOT)',
                ],
                'Pemantauan Kemajuan Pembayaran' => [
                    'Pemantauan Pembayaran(Penjanaan Graf)',
                    'Bayaran Interim (IPC)',
                    'Akaun Muktamad (SOFA)',
                    'Gantirugi tertentu dan ditetapkan (LAD)',
                ],
                'Penamatan Kontrak' => [
                    'Penamatan Kontrak',
                ],
            ],
        ];

        foreach ($data as $category => $sections) {
            // pra or post
            foreach ($sections as $section => $names) {
                // kelulusan perolehan
                foreach ($names as $name) {
                    Workflow::updateOrCreate([
                        'slug'     => str_slug($category . '-' . $section . '-' . $name),
                        'category' => $category,
                        'section'  => $section,
                        'name'     => $name,
                    ]);
                }
            }
        }
    }
}
