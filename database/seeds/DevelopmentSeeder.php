<?php

use App\Traits\SeedingProgressBar;
use App\Traits\TruncateTableTrait;
use Illuminate\Database\Seeder;

class DevelopmentSeeder extends Seeder
{
    use SeedingProgressBar, TruncateTableTrait;

    public $truncate = [
        'acquisitions',
        'sales',
        'companies',
        'company_owners',
        'acquisition_boxes',
        'acquisition_evaluations',
        'appointed_company',
        'ssts',
        'acquisition_activities',
        'acquisition_bons',
        'acquisition_insurances',
        'acquisition_deposits',
        'acquisition_documents',
        'acquisition_sub_contracts',
        'ipcs',
        'ipc_invoices',
        'ipc_materials',
        'bq_elements',
        'bq_items',
        'bq_subitem',
        'ipc_bqs',
        'vo',
        'vo_actives',
        'vo_types',
        'cpcs',
        'cncs',
        'cmgds',
        'site_visits',
        'cpos',
        'eot_approves',
        'eot_bons',
        'eot_details',
        'eot_insurances',
        'eots',
        'tasks',
    ];

    protected $seeders = [
        'truncateTables'              => false,
        'seedCompany'                 => false,
        'seedOwners'                  => false,
        'seedAcquisitionApprovals'    => false,
        'seedAcquisitions'            => false,
        'seedAcquisitionBriefings'    => false,
        'seedSales'                   => false,
        'seedAcquisitionBoxes'        => false,
        'seedAcquisitionEvaluation'   => false,
        'seedAppointedCompany'        => false,
        'seedSst'                     => false,
        'seedAcquisitionActivities'   => false,
        'seedAcquisitionBons'         => false,
        'seedAcquisitionInsurances'   => false,
        'seedAcquisitionDeposits'     => false,
        'seedAcquisitionDocuments'    => false,
        'seedAcquisitionSubContracts' => false,
        'seedIpcs'                    => false,
        'seedIpcInvoices'             => false,
        'seedIpcMaterials'            => false,
        'seedBqElements'              => false,
        'seedBqItems'                 => false,
        'seedBqSubItems'              => false,
        'seedIpcBqs'                  => false,
        'seedVariationOrders'         => false,
        'seedCpcs'                    => false,
        'seedCncs'                    => false,
        'seedCmgds'                   => false,
        'seedSiteVisits'              => false,
        'seedCpos'                    => false,
        'seedEots'                    => false,
    ];

    public function seedAcquisitionActivities()
    {
        \App\Models\Acquisition\Sst::limit(5)->get()->each(function ($sst) {
            $start_range = 0;
            $end_range = 4;
            for ($i = 0; $i < 25; ++$i) {
                $scheduled_started = \Carbon\Carbon::now()->addMonths(range(1, 12));
                $scheduled_ended = $scheduled_started->copy()->addWeeks(range(1, 12));
                $scheduled_duration = $scheduled_started->diffInDays($scheduled_ended);
                $scheduled_completion = rand($start_range, $end_range);

                $current_started = $scheduled_started->copy()->addMonths(range(1, 2));
                $current_ended = $current_started->copy()->addWeeks(range(1, 12));
                $current_duration = $current_started->diffInDays($current_ended);
                $current_completion = rand($start_range, $end_range);

                $actual_started = $scheduled_started->copy()->addMonths(range(1, 2));
                $actual_ended = $actual_started->copy()->addWeeks(range(1, 12));
                $actual_duration = $actual_started->diffInDays($actual_ended);
                $actual_completion = rand($start_range, $end_range);

                $activity = factory(\App\Models\Acquisition\Activity::class)
                    ->make([
                        'acquisition_approval_id' => $sst->acquisition->approval->id,
                        'acquisition_id'          => $sst->acquisition->id,
                        'sst_id'                  => $sst->id,
                        'company_id'              => $sst->company_id,

                        'scheduled_started_at' => $scheduled_started,
                        'scheduled_ended_at'   => $scheduled_ended,
                        'scheduled_duration'   => $scheduled_duration,
                        'scheduled_completion' => $scheduled_completion,

                        'current_started_at' => $current_started,
                        'current_ended_at'   => $current_ended,
                        'current_duration'   => $current_duration,
                        'current_completion' => $current_completion,

                        'actual_started_at' => $actual_started,
                        'actual_ended_at'   => $actual_ended,
                        'actual_duration'   => $actual_duration,
                        'actual_completion' => $actual_completion,
                    ]);

                if ($activity->parent_id) {
                    $activity->parent_id = optional(\App\Models\Acquisition\Activity::where('sst_id', $sst->id)->inRandomOrder()->first())->id;
                } else {
                    $activity->parent_id = null;
                }
                $activity->save();
                $start_range += 4;
                $end_range += 4;
                if ($end_range > 100) {
                    $end_range = 100;
                }
            }
        });
    }

    private function seedCompany()
    {
        factory(\App\Models\Company\Company::class, 3)->create();
    }

    private function seedOwners()
    {
        \App\Models\Company\Company::all()->each(function ($company) {
            factory(\App\Models\Company\Owner::class, 2)
                ->create([
                    'company_id' => $company->id,
                ]);
        });
    }

    private function seedAcquisitionApprovals()
    {
        \App\Models\User::whereNotNull('department_id')->limit(5)->get()
            ->each(function ($user) {
                $approval = factory(\App\Models\Acquisition\Approval::class)
                    ->create([
                        'user_id' => $user->id,
                    ]);
            });
    }

    private function seedAcquisitions()
    {
        \App\Models\Acquisition\Approval::all()->each(function ($acquisition_approval) {
            $acquisition = factory(\App\Models\Acquisition\Acquisition::class)
                ->create([
                    'user_id'                 => $acquisition_approval->user_id,
                    'department_id'           => $acquisition_approval->department_id,
                    'acquisition_approval_id' => $acquisition_approval->id,
                ]);
        });
    }

    private function seedBqElements()
    {
        \App\Models\Acquisition\Acquisition::all()->each(function ($acq) {
            factory(\App\Models\BillOfQuantity\Element::class)
                ->create([
                    'acquisition_id' => $acq->id,
                ]);
        });
    }

    private function seedBqItems()
    {
        \App\Models\Acquisition\Acquisition::all()->each(function ($acq) {
            factory(\App\Models\BillOfQuantity\Item::class)
                ->create([
                    'acquisition_id' => $acq->id,
                    'bq_element_id'  =>  $acq->bqElements->pluck('id')->first(),
                ]);
        });
    }

    private function seedBqSubItems()
    {
        \App\Models\Acquisition\Acquisition::all()->each(function ($acq) {
            factory(\App\Models\BillOfQuantity\SubItem::class)
                ->create([
                    'acquisition_id' => $acq->id,
                    'bq_element_id'  =>  $acq->bqElements->pluck('id')->first(),
                    'bq_item_id'     =>  $acq->bqItems->pluck('id')->first(),
                ]);
        });
    }
    
    private function seedAcquisitionBriefings()
    {
        \App\Models\Acquisition\Acquisition::all()->each(function ($acquisition) {
            factory(\App\Models\Acquisition\AcquisitionBriefing::class)
                ->create([
                    'user_id'        => $acquisition->user_id,
                    'acquisition_id' => $acquisition->id,
                ]);
        });
    }

    private function seedSales()
    {
        \App\Models\Acquisition\AcquisitionBriefing::all()->each(function ($acquisitionBriefings) {
            factory(\App\Models\Sale::class)
                ->create([
                    'user_id'        => $acquisitionBriefings->user_id,
                    'acquisition_id' => $acquisitionBriefings->acquisition_id,
                    'company_id'     => $acquisitionBriefings->company_id,
            ]);
        });
    }

    private function seedAcquisitionBoxes()
    {
        \App\Models\Sale::all()->each(function ($sale) {
            factory(\App\Models\Acquisition\Box::class)
                ->create([
                    'user_id'        => $sale->user_id,
                    'acquisition_id' => $sale->acquisition_id,
                    'sale_id'        => $sale->id,
                ]);
        });
    }

    private function seedAcquisitionEvaluation()
    {
        \App\Models\Acquisition\Box::all()->each(function ($box) {
            factory(\App\Models\Acquisition\Evaluation::class)
                ->create([
                    'acquisition_id' => $box->acquisition_id,
                ]);
        });
    }

    private function seedAppointedCompany()
    {
        \App\Models\Acquisition\Evaluation::all()->each(function ($eval) {
            factory(\App\Models\Acquisition\AppointedCompany::class)
                ->create([
                    'acquisition_id' => $eval->acquisition_id,
                    'company_id'     => $eval->acquisition->briefings->pluck('company_id')->first(),
                ]);
        });
    }

    private function seedSst()
    {
        \App\Models\Acquisition\AppointedCompany::all()->each(function ($appointed) {
            factory(\App\Models\Acquisition\Sst::class)
                ->create([
                    'acquisition_id' => $appointed->acquisition_id,
                    'company_id'     => $appointed->acquisition->briefings->pluck('company_id')->first(),
                ]);
        });
    }

    private function seedAcquisitionBons()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Bon::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedAcquisitionInsurances()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Insurance::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedAcquisitionDeposits()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Deposit::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedAcquisitionDocuments()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Document::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedAcquisitionSubContracts()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\SubContract::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedIpcs()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Ipc::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedIpcInvoices()
    {
        \App\Models\Acquisition\Ipc::all()->each(function ($ipc) {
            factory(\App\Models\Acquisition\IpcInvoice::class)
                ->create([
                    'sst_id' => $ipc->sst->id,
                    'ipc_id' => $ipc->id,
                ]);
        });
    }

    private function seedIpcMaterials()
    {
        \App\Models\Acquisition\Ipc::all()->each(function ($ipc) {
            factory(\App\Models\Acquisition\IpcMaterial::class)
                ->create([
                    'sst_id' => $ipc->sst_id,
                    'ipc_id' => $ipc->id,
                ]);
        });
    }

    private function seedIpcBqs()
    {
        \App\Models\Acquisition\IpcInvoice::all()->each(function ($invoice) {
            factory(\App\Models\Acquisition\IpcBq::class)
                ->create([
                    'sst_id'                => $invoice->ipc->id,
                    'ipc_invoice_id'        => $invoice->id,
                    'bq_subitem_id' => $invoice->sst->acquisition->bqSubItems->pluck('id')->first(),
                ]);
        });
    }

    private function seedVariationOrders()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\VO\VO::class)
                ->create([
                    'user_id'       => $sst->user_id,
                    'department_id' => $sst->department_id,
                    'sst_id'        => $sst->id,
                ]);
        });
    }

    private function seedCpcs()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Cpc::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedCncs()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Cnc::class)
                ->create([
                    'sst_id'       => $sst->id,
                    'reference_no' => $sst->acquisition->approval->file_reference,
                ]);
        });
    }

    private function seedCmgds()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Cmgd::class)
                ->create([
                    'sst_id'       => $sst->id,
                    'reference_no' => $sst->acquisition->approval->file_reference,
                ]);
        });
    }

    private function seedSiteVisits()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Monitoring\SiteVisit::class)
                ->create([
                    'sst_id'                  => $sst->id,
                    'department_id'           => $sst->department->id,
                    'acquisition_id'          => $sst->acquisition_id,
                    'acquisition_approval_id' => $sst->acquisition->approval->id,
                    'percentage_completed'    => 25.0,
                ]);
        });
    }

    private function seedCpos()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Cpo::class)
                ->create([
                    'sst_id' => $sst->id,
                ]);
        });
    }

    private function seedEots()
    {
        \App\Models\Acquisition\Sst::all()->each(function ($sst) {
            factory(\App\Models\Acquisition\Eot::class)
                ->create([
                    'user_id'              => $sst->user_id,
                    'department_id'        => $sst->department_id,
                    'sst_id'               => $sst->id,
                    'acquisition_id'       => $sst->acquisition_id,
                    'appointed_company_id' => $sst->company_id,
                    'period'               => 3,
                    'original_end_date'    => $sst->end_working_date,
                    'extended_end_date'    => $sst->end_working_date->addMonths(3),
                ]);
        });

        \App\Models\Acquisition\Eot::all()->each(function ($eot) {
            factory(\App\Models\Acquisition\EotDetail::class)
                ->create([
                    'user_id' => $eot->user_id,
                    'eot_id'  => $eot->id,
                ]);

            factory(\App\Models\Acquisition\EotInsurance::class)
                ->create([
                    'public_insurance_name'       => insurances()->shuffle()->first()->id,
                    'work_insurance_name'         => insurances()->shuffle()->first()->id,
                    'compensation_insurance_name' => insurances()->shuffle()->first()->id,
                    'user_id'                     => $eot->user_id,
                    'eot_id'                      => $eot->id,
                ]);

            factory(\App\Models\Acquisition\EotBon::class)
                ->create([
                    'user_id' => $eot->user_id,
                    'eot_id'  => $eot->id,
                ]);

            factory(\App\Models\Acquisition\EotApprove::class)
                ->create([
                    'user_id' => $eot->user_id,
                    'eot_id'  => $eot->id,
                ]);
        });
    }
}
