<?php

use App\Models\Acquisition\Type;
use Illuminate\Database\Seeder;

class AcquistionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            'Bekalan',
            'Kerja',
            'Perkhidmatan',
            'Penyelenggaraan',
        ];

        foreach ($data as $key => $value) {
            Type::create(['name' => $value]);
        }
    }
}
