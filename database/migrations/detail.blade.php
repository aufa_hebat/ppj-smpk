{{--  <style type="text/css">
        thead {
            display: table-header-group;
        }
        tfoot {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
        }
</style>  --}}
@php
    $letter = collect(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']);
    $roman = collect(['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix', 'x']);
@endphp
<table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center" page-break-inside: auto>
    <tr>
        <td style="text-align:center;">
            <span style="text-transform:uppercase; font-size:14px;">
                Perbadanan Putrajaya<br/>Permohonan Perubahan Kerja No. {{ $vo->no }}<br/>
            </span>
            <span style="text-transform:capitalize; font-size:10px; font-style:italic;">
                (Dokumen Bahagian Perolehan &amp; Ukur Bahan ini untuk edaran dalaman sahaja)
            </span>
        </td>
    </tr>
</table>
                     
<div style="font-size:12px; border:1; padding: 5px 5px;">
    BAHAGIAN B : MAKLUMAT MENGENAI PERUBAHAN KERJA YANG DIPOHON 
    <table border="0" style="width: 98%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
        <thead>
            <tr style="text-align:center;">
                <td class="bordered" rowspan="2">Bil.</td>
                <td class="bordered" rowspan="2">Huraian Perubahan Kerja</td>
                <td class="bordered" rowspan="2">Sebab Perubahan Diperlukan</td>
                <td class="bordered" colspan="4">Anggaran (RM)*</td>                    
            </tr>
            <tr style="text-align:center;">
                <td class="bordered">Tambahan (a)</td>
                <td class="bordered">%</td>
                <td class="bordered">Kurangan (b)</td>
                <td class="bordered">%</td>
            </tr>
        </thead>
        <tbody> 
            @foreach($types as $keyT => $type)                                     
                <tr>
                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding:10px 10px;" valign="top">{{ $letter[$keyT] }}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;">
                        <span style="text-transform:uppercase;text-decoration:underline;">{{ $type->name }}</span>                                
                    </td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                </tr>
                @php
                    $totalOmission = 0.0;
                    $totalAddition = 0.0;
                    $total = 0.0;
                    $totalElement = 0;
                @endphp

                @foreach($vo->types as $vot)
                    @if($vot->variation_order_type_id == $type->id || ($vot->variation_order_type_id == 1 && $type->id == 3))
                        @if($vot->variation_order_type_id == 1)
                            @foreach($vo->actives as $key=>$active)
                                <tr>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" valign="top">{{ $roman[$key] }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">
                                        {{ $active->item->item }}
                                        <br/>
                                        ({{ money()->toHuman($active->item->amount ?? "0", 2 ) }})
                                    </td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">
                                        Peruntukan sementara untuk diaktifkan
                                    </td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00%</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00%</td> 
                                </tr>
                            @endforeach
                        @endif

                        @foreach($Elemen as $key=>$el)
                            @if($el->vo_type_id == $vot->id)
                                @php
                                    $totalElement = $totalElement + 1;
                                @endphp
                                <tr>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px; text-align: center;" valign="top">{{ $roman[$totalElement - 1] }}</td>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"><span>{{ $el->element }}</span></td>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"><span>{{ $el->description }}</span></td>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                        @if($el->amount_addition > $el->amount_omission)
                                            <span>{{ money()->toCommon($el->net_amount ?? "0", 2) }}</span>
                                        @endif
                                    </td>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"></td>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                        @if($el->amount_addition < $el->amount_omission)
                                            <span>({{ abs(money()->toCommon($el->net_amount ?? "0", 2)) }})</span>
                                        @endif
                                    </td>
                                    <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"></td>                                        
                                </tr>

                                @php
                                    $totalOmission = $totalOmission + $el->amount_omission;
                                    $totalAddition = $totalAddition + $el->amount_addition;
                                    $total = $total + $el->net_amount;                                        
                                @endphp
                            @endif
                        @endforeach
                    @endif
                @endforeach
                @if($totalElement == 0 && $type->id != 1)
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"><span> - Tiada - </span></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>                                        
                    </tr>
                @endif
                <tr>
                    <td colspan="3" class="bordered" style="text-align:left; padding:10px 10px;">Jumlah : 
                        @if($totalAddition < $totalOmission) 
                            <span style="text-decoration: line-through;">Tambahan</span> / Pengurangan
                        @elseif($totalAddition > $totalOmission) 
                            Tambahan / <span style="text-decoration: line-through;">Pengurangan</span>
                        @else
                            Tambahan / Pengurangan
                        @endif
                    </td>
                    <td class="bordered" style="text-align:right; padding:10px 10px;">
                        @if($totalAddition > $totalOmission)
                            {{ money()->toCommon($total ?? "0", 2) }}
                        @endif
                    </td>
                    <td class="bordered"></td>
                    <td class="bordered" style="text-align:right; padding:10px 10px;">
                        @if($totalAddition < $totalOmission)
                            {{ money()->toCommon($total ?? "0", 2) }}
                        @endif
                    </td>
                    <td class="bordered"></td>                        
                </tr> 

            @endforeach
        </tbody>
    </table>
</div>
<table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
    <tr>
        <td style="font-size:12px;"><br/></td>
    </tr>
    <tr>
        <td style="font-size:12px;"><span style="text-transform:uppercase; font-weight:bold;">Pengesahan oleh Jabatan Bertanggungjawab</span></td>
    </tr>
    <tr>
        <td style="font-size:12px;">
            <table border="1" style="width: 98%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
                <tr>
                    <td width="33%" style="text-align:center;">Disediakan Oleh:</td>
                    <td width="33%" style="text-align:center;">Disemak Oleh:</td>
                    <td width="33%" style="text-align:center;">Disokong Oleh: (Naib Presiden)</td>
                </tr>
                <tr>
                    <td>
                        <br/><br/><br/><br/>
                        Nama : <br/>
                        Jawatan : <br/>
                        Bahagian  : <br/>
                        Jabatan : <br/>
                        Tarikh : 
                    </td>
                    <td>
                        <br/><br/><br/><br/>
                        Nama : <br/>
                        Jawatan : <br/>
                        Bahagian  : <br/>
                        Jabatan : <br/>
                        Tarikh : 
                    </td>
                    <td>
                        <br/><br/><br/><br/>
                        Nama : <br/>
                        Jawatan : <br/>
                        Bahagian  : <br/>
                        Jabatan : <br/>
                        Tarikh : 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>