<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCncsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cncs', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');

            $table->string('reference_no', 32)->nullable();
            $table->amount('lad_amount');
            $table->date('signature_date')->nullable();
            $table->date('test_award_date')->nullable();
            $table->string('clause_1', 100)->nullable();
            $table->string('clause_2', 100)->nullable();
            $table->date('assessment_date')->nullable();
            $table->amount('blr_rate');
            $table->integer('days_passed')->nullable();
            $table->amount('lad_per_day');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('cncs');
    }
}
