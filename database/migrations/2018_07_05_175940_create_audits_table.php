<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('old_values')->nullable();
            $table->mediumText('new_values')->nullable();
            $table->string('event')->nullable();
            $table->string('auditable_id')->nullable();
            $table->string('auditable_type')->nullable();
            $table->string('user_id')->nullable();
            $table->string('user_type')->nullable();
            $table->string('url')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('tags')->nullable();
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('audits');
    }
}
