<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowTaskTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('workflow_task', function (Blueprint $table) {
            $table->increments('id');
            $table->belongsTo('acquisitions');
            $table->integer('eot_id')->nullable();
            $table->integer('ipc_id')->nullable();
            $table->integer('ppk_id')->nullable();
            $table->integer('ppjhk_id')->nullable();
            $table->integer('warning_id')->nullable();
            $table->string('flow_name', 255)->nullable();
            $table->string('flow_desc', 255)->nullable();
            $table->string('flow_location', 255)->nullable();
            $table->is('claimed', false);
            $table->nullableBelongsTo('users');
            $table->integer('role_id')->nullable();
            $table->nullableBelongsTo('workflow_team');
            $table->string('url', 255)->nullable();
            // $table->string('department', 255)->nullable();
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('workflow_task');
    }
}
