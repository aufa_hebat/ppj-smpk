<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEotsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('eots', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();
            $table->belongsTo('departments');

            /*
             * Foreign Key
             */
            $table->belongsTo('appointed_company');
            $table->belongsTo('period_types');
            $table->belongsTo('ssts');
            $table->belongsTo('acquisitions');

            /*
             * EOT Standard Details
             */
            $table->string('bil', 15)->nullable();
            $table->integer('period')->nullable();
            $table->date('original_end_date')->nullable();
            $table->date('extended_end_date')->nullable();
            $table->integer('section')->nullable()->comment('1 = Kesuluruhan, 2 = Sebahagian');
            $table->integer('status')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('eots');
    }
}
