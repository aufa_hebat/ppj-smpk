<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteVisitsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('site_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();

            $table->belongsTo('acquisition_approvals');
            $table->belongsTo('acquisitions');
            $table->belongsTo('departments');
            $table->belongsTo('ssts');

            $table->name();
            $table->remarks();
            $table->by('users', 'visited');
            $table->at('visited');
            $table->date('visit_date');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('site_visits');
    }
}
