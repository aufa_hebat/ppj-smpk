<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpcsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ipcs', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            /*
             * foreign key
            */
            $table->user();
            $table->nullableBelongsTo('ssts');
            $table->integer('ipc_no')->nullable();
            $table->string('penalty')->comment('penultimate if exist')->nullable();
            $table->string('last_ipc', 2)->comment('n for no, y for yes')->nullable();
            $table->string('bulan_status')->comment('untuk report status')->nullable();
            /*
             * plus minus amount
             */
            $table->amount('demand_amount');
            $table->amount('material_on_site_amount');
            $table->amount('material_tahanan_amount');
            $table->amount('working_change_amount');
            $table->amount('new_contract_amount');
            $table->amount('advance_amount');
            $table->amount('wjp_amount');
            $table->amount('wjp_lepas_amount');
            $table->amount('bank_cash_lepas_amount');
            $table->amount('give_advance_amount');
            $table->amount('compensation_amount');
            $table->amount('amount_before_gst');
            $table->amount('gst_amount');
            $table->amount('gst');
            $table->amount('amount_after_gst');
            $table->amount('lad_day');
            $table->amount('lad_rate');
            /*
             * debit credit amount
             */
            $table->string('debit_no')->nullable();
            $table->string('credit_no')->nullable();
            $table->amount('debit');
            $table->amount('credit');
            $table->at('debit');
            $table->at('credit');
            $table->at('debit_receive');
            $table->at('credit_receive');
            /*
             * status field
             */
            $table->integer('status')->comment('1 draf, 2 semakan, 3 pending SAP, 4 success SAP');
            /*
             * sap field
             */
            $table->string('long_text', 50)->nullable();
            $table->string('short_text', 25)->nullable();
            $table->string('sap_doc_data')->nullable();
            /*
             * tarikh penilaian
             */
            $table->at('evaluation');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ipcs');
    }
}
