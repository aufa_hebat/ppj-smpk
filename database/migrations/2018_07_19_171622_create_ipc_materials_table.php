<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpcMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ipc_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();
            $table->nullableBelongsTo('ipcs');
            $table->nullableBelongsTo('ssts');
            $table->description();
            $table->amount();
            $table->string('unit')->nullable();
            $table->amount('quantity');
            $table->amount('rate');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ipc_materials');
    }
}
