<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSstsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ssts', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            /*
             * Foreign Key
             */
            $table->user();
            $table->belongsTo('departments');
            $table->belongsTo('acquisitions');
            $table->belongsTo('companies');
            $table->belongsTo('company_owners')->nullable();
            $table->belongsTo('period_types');

            /*
             * Maklumat Perolehan
             */
            $table->string('contract_no', 32)->nullable();
            $table->integer('report_type_id')->nullable();

            /*
             * SST/LOA
             */
            $table->date('start_working_date')->nullable();
            $table->date('end_working_date')->nullable();
            $table->date('letter_date')->nullable();
            $table->integer('defects_liability_period')->nullable();
            $table->amount('sl1m_allowance')->nullable();
            $table->integer('sl1m_entity')->nullable();
            $table->amount('document_blr')->nullable();
            $table->amount('document_LAD_amount');
            $table->integer('period')->nullable();

            /*
             * Bon
             */
            $table->amount('bon_amount');

            /*
             * Insurans
             */
            $table->date('ins_pli_start_date')->comment('Public Liability Insurance')->nullable();
            $table->date('ins_pli_end_date')->nullable();
            $table->amount('ins_pli_amount');
            $table->date('ins_work_start_date')->comment('Work Insurance')->nullable();
            $table->date('ins_work_end_date')->nullable();
            $table->amount('ins_work_amount');
            $table->date('ins_ci_start_date')->comment('Compensation Insurance')->nullable();
            $table->date('ins_ci_end_date')->nullable();
            $table->amount('ins_ci_amount');

            /*
             * Kelulusan
             */
            $table->date('vp_sign_date')->comment('Vice President Signature Date')->nullable();
            $table->date('al_issue_date')->comment('Acceptance Letter Issue Date')->nullable();
            $table->date('al_contractor_date')->comment('Acceptance Letter From Contractor Date')->nullable();
            $table->integer('status')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ssts');
    }
}
