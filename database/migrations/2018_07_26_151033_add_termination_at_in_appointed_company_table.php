<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTerminationAtInAppointedCompanyTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('appointed_company', function (Blueprint $table) {
            $table->nullableBelongsTo('termination_types');
            $table->date('termination_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('appointed_company', function (Blueprint $table) {
            $table->dropForeign(['termination_type_id']);
            $table->dropColumn('termination_type_id');
            $table->dropColumn('termination_at');
        });
    }
}
