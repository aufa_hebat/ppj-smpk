<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentAcceptancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_acceptances', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            /*
             * foreign key
            */
            $table->user();
            $table->nullableBelongsTo('ssts');

            $table->dateTime('document_acceptance_at')->nullable();
            $table->string('remarks', 255)->nullable();
            $table->integer('acceptance_status')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_acceptances');
    }
}
