<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpcInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ipc_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            /*
             * foreign key
            */
            $table->user();
            $table->nullableBelongsTo('ssts');
            $table->nullableBelongsTo('ipcs');
            $table->nullableBelongsTo('companies');
            $table->nullableBelongsTo('acquisition_approval_financials'); // fund_id
            $table->nullableBelongsTo('acquisition_sub_contracts');
            $table->string('bulan_status')->comment('untuk report status')->nullable();
            /*
             * invois amount
             */
            $table->string('invoice_no')->nullable();
            $table->amount('invoice');
            $table->amount('demand_amount');
            $table->at('invoice');
            $table->at('invoice_receive');
            /*
             * status field
             */
            $table->integer('status')->comment('3 pending SAP, 4 success SAP');
            $table->integer('status_contractor')->comment('1 main contractor, 2 subcontractor')->default('1');
            $table->string('sap_doc_data')->nullable();
            $table->string('sap_bank_no')->comment('cheque no return from sap')->nullable();
            $table->string('sap_date')->comment('cheque no return from sap')->nullable();
            /*
             * sap field
             */
            $table->string('ref_text', 16)->nullable();
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ipc_invoices');
    }
}
