<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarningsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('warnings', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            /* ref table */
            $table->nullableBelongsTo('ssts');
            $table->nullableBelongsTo('termination_types');

            $table->string('fail_ref_no')->nullable();
            $table->string('warning_letter_no')->nullable();
            $table->string('clause')->nullable();
            $table->string('sub_clause')->nullable();
            $table->string('site_meeting_no')->nullable();

            $table->percent('scheduled_completion');
            $table->percent('actual_completion');

            $table->date('warning_letter_at')->nullable();
            $table->date('site_meeting_at')->nullable();

            /*
             * status field
             */
             $table->integer('status')->comment('1 draf, 2 semakan, 3 success');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('warnings');
    }
}
