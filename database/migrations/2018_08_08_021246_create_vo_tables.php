<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoTables extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->createVo();
        $this->createVoType();
        $this->createVoActives();
        $this->createVoCancels();
        // $this->createKKK();
        // $this->createPK();
        // $this->createPQ();
        // $this->createWPS();
        $this->createBQ();
        $this->createApprover();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        $tables = [
            'vo_kkk_sub_items',
            'vo_kkk_items',
            'vo_kkk_elements',
            'vo_pk_sub_items',
            'vo_pk_items',
            'vo_pk_elements',
            'vo_pq_sub_items',
            'vo_pq_items',
            'vo_pq_elements',
            'vo_wps_sub_items',
            'vo_wps_items',
            'vo_wps_elements',
            'vo_sub_items',
            'vo_items',
            'vo_elements',
            'vo_actives',
            'vo',
            'vo_types',
        ];
        foreach ($tables as $table) {
            Schema::dropIfExists($table);
        }
        Schema::enableForeignKeyConstraints();
    }

    private function createKKK()
    {
        Schema::create('vo_kkk_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('vo');
            $table->integer('no')->nullable();
            $table->description('element');
            $table->description();
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');
            $table->standardTime();
        });

        Schema::create('vo_kkk_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('vo');
            $table->belongsTo('vo_kkk_elements');

            $table->integer('bq_no_element')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->description('summary');
            $table->description('reason_required');
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('vo_kkk_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('bq_subitem');
            $table->belongsTo('vo');
            $table->belongsTo('vo_kkk_items');

            $table->integer('bq_no_element')->nullable();
            $table->integer('bq_no_item')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->string('unit', 10)->nullable();

            $table->amount('quantity');
            $table->amount('rate_per_unit');
            $table->amount();

            $table->string('status_lock', 45)->nullable();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });
    }

    private function createPK()
    {
        Schema::create('vo_pk_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('vo');
            $table->integer('no')->nullable();
            $table->description('element');
            $table->description();
            $table->amount();

            $table->is('new', false);
            $table->is('removed', false);

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');
            $table->standardTime();
        });

        Schema::create('vo_pk_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('vo');

            $table->integer('bq_no_element')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->description('summary');
            $table->description('reason_required');
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('vo_pk_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('bq_subitem');
            $table->belongsTo('vo');

            $table->integer('bq_no_element')->nullable();
            $table->integer('bq_no_item')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->string('unit', 10)->nullable();

            $table->amount('quantity');
            $table->amount('rate_per_unit');
            $table->amount();

            $table->string('status_lock', 45)->nullable();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });
    }

    private function createPQ()
    {
        Schema::create('vo_pq_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('vo');
            $table->integer('no')->nullable();
            $table->description('element');
            $table->description();
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');
            $table->standardTime();
        });

        Schema::create('vo_pq_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('vo');
            $table->belongsTo('vo_pq_elements');

            $table->integer('bq_no_element')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->description('summary');
            $table->description('reason_required');
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('vo_pq_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('bq_subitem');
            $table->belongsTo('vo');
            $table->belongsTo('vo_pq_items');

            $table->integer('bq_no_element')->nullable();
            $table->integer('bq_no_item')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->string('unit', 10)->nullable();

            $table->amount('quantity');
            $table->amount('rate_per_unit');
            $table->amount();

            $table->string('status_lock', 45)->nullable();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });
    }

    private function createWPS()
    {
        Schema::create('vo_wps_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('vo');
            $table->integer('no')->nullable();
            $table->description('element');
            $table->description();
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('vo_wps_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('vo');

            $table->integer('bq_no_element')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->description('summary');
            $table->description('reason_required');
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('vo_wps_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('bq_subitem');
            $table->belongsTo('vo');

            $table->integer('bq_no_element')->nullable();
            $table->integer('bq_no_item')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->string('unit', 10)->nullable();

            $table->amount('quantity');
            $table->amount('rate_per_unit');
            $table->amount();

            $table->string('status_lock', 45)->nullable();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();
            $table->integer('ppk_id')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });
    }

    private function createVoType()
    {
        Schema::create('vo_types', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('vo');
            $table->belongsTo('variation_order_types');
            $table->standardTime();
        });
    }

    private function createVo()
    {
        Schema::create('vo', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('users');
            $table->belongsTo('departments');
            $table->belongsTo('ssts');
            $table->unsignedInteger('no')
                ->nullable()
                ->comment('Each project has their own counter.');
            $table->integer('status')
                ->nullable()
                ->comment('0-Draft 1-Send');
            $table->string('mtg_no')->nullable();
            $table->string('mtg_year')->nullable();
            $table->string('ppk_kod_1')->nullable();
            $table->string('ppk_kod_2')->nullable();
            $table->string('ppk_kod_3')->nullable();                
            $table->string('type', 10)->nullable(); 
            $table->standardTime();
        });
    }

    private function createVoActives()
    {
        Schema::create('vo_actives', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('users');
            $table->belongsTo('departments');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('vo');
            $table->integer('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->amount();
            $table->standardTime();
        });
    }

    private function createVoCancels()
    {
        Schema::create('vo_cancels', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('users');
            $table->belongsTo('departments');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements');
            $table->belongsTo('bq_items');
            $table->belongsTo('vo');
            $table->integer('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->amount();
            $table->amount('baki_amount');
            $table->amount('paid_amount');
            $table->integer('ppjhk_status')->nullable();
            $table->integer('ppjhk_id')->nullable();            
            $table->standardTime();
        });
    }


    private function createBQ()
    {
        Schema::create('vo_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements')->nullable();
            $table->belongsTo('vo');
            $table->belongsTo('vo_types');
            $table->nullableBelongsTo('vo_actives');
            $table->integer('no')->nullable();
            $table->description('element');
            $table->description();
            $table->description('comment')->nullable();
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();
            $table->integer('status')->nullable();
            $table->integer('ppjhk_status')->nullable();
            $table->integer('ppjhk_id')->nullable();
            $table->integer('wps_status')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');
            $table->standardTime();
        });

        Schema::create('vo_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements')->nullable();
            $table->belongsTo('bq_items')->nullable();
            $table->belongsTo('vo');
            $table->belongsTo('vo_types');
            $table->nullableBelongsTo('vo_actives');
            $table->belongsTo('vo_elements');

            $table->integer('bq_no_element')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->description('summary');
            $table->description('reason_required');
            $table->amount();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();
            $table->integer('wps_status')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('vo_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('bq_elements')->nullable();
            $table->belongsTo('bq_items')->nullable();
            $table->belongsTo('bq_subitem')->nullable();
            $table->belongsTo('vo');
            $table->belongsTo('vo_types');
            $table->nullableBelongsTo('vo_actives');
            $table->belongsTo('vo_items');

            $table->integer('bq_no_element')->nullable();
            $table->integer('bq_no_item')->nullable();
            $table->integer('no')->nullable();
            $table->description('item');
            $table->description();
            $table->string('unit', 10)->nullable();

            $table->amount('quantity');
            $table->amount('rate_per_unit');
            $table->amount();

            $table->string('status_lock', 45)->nullable();

            $table->amount('quantity_omission')->comment('quantity omission');
            $table->amount('quantity_addition')->comment('quantity addition');
            $table->amount('amount_omission')->comment('amount omission');
            $table->amount('amount_addition')->comment('amount addition');
            $table->percent('percent_omission')->comment('percent omission');
            $table->percent('percent_addition')->comment('percent addition');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->string('bq_reference')->nullable();
            $table->integer('wps_status')->nullable();
            $table->amount('paid_quantity')->comment('quantity paid on ipc');
            $table->amount('baki_quantity')->comment('quantity baki on ipc');

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });
    }

    private function createApprover()
    {
        Schema::create('vo_approvers', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            $table->belongsTo('vo')->nullable();
            $table->belongsTo('departments')->nullable();
            $table->belongsTo('users')->nullable();
            $table->belongsTo('positions')->nullable();
            $table->string('role', 40)->nullable();
            $table->belongsTo('ssts')->nullable();

            $table->date('appointment_date')->nullable();
            $table->string('approval', 100)->nullable();
            $table->string('category', 40)->nullable();

            $table->standardTime();
        });
    }
}
