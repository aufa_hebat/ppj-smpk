<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionDepositsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');

            $table->string('reference_no', 255)->nullable();
            $table->amount('prime_cost')->nullable();
            $table->amount('amount');
            $table->amount('total_amount');
            $table->amount('qualified_amount');
            $table->amount('application_amount');
            $table->integer('bon_type')->nullable();

            /*
             * Bank Guarantee
             */
            $table->nullableBelongsTo('banks');
            $table->integer('bank_name')->nullable();
            $table->string('bank_no', 40)->nullable();
            $table->amount('bank_amount');
            $table->date('bank_received_date')->nullable();
            $table->date('bank_start_date')->nullable();
            $table->date('bank_expired_date')->nullable();
            $table->date('bank_proposed_date')->nullable();
            $table->date('bank_approval_proposed_date')->nullable();
            $table->date('bank_approval_received_date')->nullable();

            /*
             * Insurance Guarantee
             */
            $table->nullableBelongsTo('insurances');
            $table->integer('insurance_name')->nullable();
            $table->string('insurance_no', 40)->nullable();
            $table->amount('insurance_amount');
            $table->date('insurance_received_date')->nullable();
            $table->date('insurance_start_date')->nullable();
            $table->date('insurance_expired_date')->nullable();
            $table->date('insurance_proposed_date')->nullable();
            $table->date('insurance_approval_proposed_date')->nullable();
            $table->date('insurance_approval_received_date')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_deposits');
    }
}
