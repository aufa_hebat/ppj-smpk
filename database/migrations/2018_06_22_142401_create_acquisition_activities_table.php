<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->hashslug();

            $table->nullableBelongsTo('acquisition_activities', 'parent_id', true);
            $table->belongsTo('acquisition_approvals');
            $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('companies');

            $table->string('no')->nullable();
            $table->name();

            $table->percent('scheduled_completion')->nullable();
            $table->at('scheduled_started');
            $table->at('scheduled_ended');
            $table->unsignedInteger('scheduled_duration')->nullable();

            $table->percent('current_completion')->nullable();
            $table->at('current_started');
            $table->at('current_ended');
            $table->unsignedInteger('current_duration')->nullable();

            $table->percent('actual_completion')->nullable();
            $table->at('actual_started');
            $table->at('actual_ended');
            $table->unsignedInteger('actual_duration')->nullable();

            $table->percent('deficit');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_activities');
    }
}
