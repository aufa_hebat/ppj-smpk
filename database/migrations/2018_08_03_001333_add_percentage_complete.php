<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPercentageComplete extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('site_visits', function (Blueprint $table) {
            $table->percent('percentage_completed')->after('remarks');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('site_visits', function (Blueprint $table) {
            $table->dropColumn('percentage_completed');
        });
    }
}
