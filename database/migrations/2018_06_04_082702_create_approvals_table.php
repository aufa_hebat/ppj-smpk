<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('acquisitions');
            $table->nullableBelongsTo('departments')->onDelete('SET NULL');
            $table->nullableBelongsTo('positions')->onDelete('SET NULL');

            $table->date('appointment_date')->nullable();
            $table->string('name', 255)->nullable();

            $table->string('approval', 100)->nullable();
            $table->string('role', 40)->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('approvals');
    }
}
