<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEotApprovesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('eot_approves', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();

            /*
             * Foreign Key
             */
            $table->belongsTo('eots');
            $table->belongsTo('period_types');

            /*
             * EOT Approve Standard Details
             */
            $table->string('bil_meeting', 15)->nullable();
            $table->integer('period')->nullable();
            $table->date('meeting_date')->nullable();
            $table->date('approved_end_date')->nullable();
            $table->integer('approval')->nullable()->comment('1 = LULUS, 2 = GAGAL');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('eot_approves');
    }
}
