<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcqusitionApprovalCidbQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_approval_cidb', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('acquisition_approvals');
            $table->belongsTo('cidb_codes');
            $table->boolean('status')->comment('0 for OR, 1 for AND');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_approval_cidb');
    }
}
