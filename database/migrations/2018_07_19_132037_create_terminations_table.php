<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTerminationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('terminations', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            $table->nullableBelongsTo('ssts');

            $table->integer('notice_type')->nullable();
            $table->date('notice_at')->nullable();
            $table->percent('scheduled_completion');
            $table->percent('actual_completion');
            $table->string('clause')->nullable();
            $table->string('sub_clause')->nullable();
            /* create on 29 July 2019*/
            $table->string('description')->nullable();
            $table->date('termination_at')->nullable();
                        /*
             * status field
             */
             $table->integer('status')->comment('1 draf, 2 semakan, 3 success');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('terminations');
    }
}
