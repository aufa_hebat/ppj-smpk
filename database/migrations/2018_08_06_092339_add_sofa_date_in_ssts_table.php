<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSofaDateInSstsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('ssts', function (Blueprint $table) {
            $table->date('sofa_signature_date')->nullable()->after('al_contractor_date');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('ssts', function (Blueprint $table) {
            $table->dropColumn('sofa_signature_date');
        });
    }
}
