<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionApprovalsTemp extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_approvals_temp', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();

            /*
             * Foreign Keys
             */
            $table->belongsTo('acquisition_approvals');
            $table->nullableBelongsTo('users', 'approved_by')->onDelete('SET NULL');
            $table->nullableBelongsTo('users', 'reviewed_by')->onDelete('SET NULL');

            $table->string('allocation_resources', 50)->nullable();
            $table->string('estimated_cost', 255)->nullable();
            $table->string('loyalty_code', 255)->nullable();

            $table->at('prepared_at');
            $table->at('checked_at');
            $table->at('approved_at');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_approvals_temp');
    }
}
