<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSapVendorMappingsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('sap_vendor_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->belongsTo('companies');
            $table->string('ssm_no', 40)->index()->nullable()->comment('connection from this system company table');
            $table->integer('vendor_id')->comment('connection from SAP table');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('sap_vendor_mappings');
    }
}
