<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEotDetailsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('eot_details', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();

            /*
             * Foreign Key
             */
            $table->belongsTo('eots');
            $table->belongsTo('period_types');

            /*
             * EOT Standard Details
             */
            $table->string('reasons', 100)->nullable();
            $table->string('clause', 100)->nullable();
            $table->integer('periods')->nullable();
            $table->string('extended_period', 100)->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('eot_details');
    }
}
