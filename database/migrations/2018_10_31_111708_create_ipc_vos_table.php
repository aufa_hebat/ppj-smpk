<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpcVosTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ipc_vos', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();
            $table->nullableBelongsTo('ipcs');
            $table->integer('ppjhk_sub_item_id')->unsigned()->nullable();
            $table->foreign('ppjhk_sub_item_id')->references('id')->on('ppjhk_sub_items')->onDelete('cascade');
            $table->integer('ppjhk_item_id')->unsigned()->nullable();
            $table->foreign('ppjhk_item_id')->references('id')->on('ppjhk_items')->onDelete('cascade');
            $table->integer('ppjhk_element_id')->unsigned()->nullable();
            $table->foreign('ppjhk_element_id')->references('id')->on('ppjhk_elements')->onDelete('cascade');
            $table->integer('vo_cancel_id')->unsigned()->nullable();
            $table->foreign('vo_cancel_id')->references('id')->on('vo_cancels')->onDelete('cascade');
            $table->nullableBelongsTo('ssts');
            $table->amount('quantity');
            $table->amount('amount');
            $table->string('status')->comment('V for vo and C for cancel')->default('V');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ipc_vos');
    }
}
