<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcquisitionApprovalMofQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acquisition_approval_mof', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('acquisition_approvals');
            $table->belongsTo('mof_codes');
            $table->boolean('status')->comment('0 for OR, 1 for AND');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_approval_mof');
    }
}
