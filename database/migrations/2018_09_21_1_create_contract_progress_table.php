<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractProgressTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('contract_progress', function (Blueprint $table) {
            $table->increments('id');
            $table->belongsTo('acquisitions');
            $table->date('on_plan_date')->nullable();
            $table->percent('on_plan_date_percent')->nullable();
            
            //link to sit visits : $table->integer('on_site_date_percent')->nullable();
            $table->belongsTo('site_visits')->nullable();
            //$table->belongsTo('payment_monitor')->nullable();
            //change to manual update
            $table->percent('on_site_payment_percent')->nullable();
            
            $table->percent('on_plan_payment_percent')->nullable();
            
            
            
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('contract_progress');
    }
}
