<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetCodesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('budget_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->name('category')->comment('Business Area, Fund, Funded Programme, Cost Center, Functional Area, GL Account');
            $table->name('code');
            $table->name();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('budget_codes');
    }
}
