<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpcsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cpcs', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');

            $table->date('dlp_start_date')->nullable();
            $table->date('dlp_expiry_date')->nullable();
            $table->date('signature_date')->nullable();
            $table->date('test_award_date')->nullable();
            $table->string('clause_1', 100)->nullable();
            $table->string('clause_2', 100)->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('cpcs');
    }
}
