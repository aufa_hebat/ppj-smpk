<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSofaDateInAppointedCompanyTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('appointed_company', function (Blueprint $table) {
            $table->date('sofa_signature_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('appointed_company', function (Blueprint $table) {
            $table->dropColumn('sofa_signature_date');
        });
    }
}
