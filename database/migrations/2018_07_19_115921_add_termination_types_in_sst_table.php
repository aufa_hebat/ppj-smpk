<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTerminationTypesInSstTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('ssts', function (Blueprint $table) {
            $table->nullableBelongsTo('termination_types');
            $table->at('termination')->after('al_contractor_date');
            $table->integer('type_suspension')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('ssts', function (Blueprint $table) {
            $table->dropForeign(['termination_type_id']);
            $table->dropColumn('termination_type_id');
            $table->dropColumn('termination_at');
        });
    }
}
