<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->slug();

            /*
             * Foreign Key
             */
            $table->nullableBelongsTo('users', 'supervisor_id')->onDelete('SET NULL');
            $table->nullableBelongsTo('sections', 'executor_department_id')->onDelete('SET NULL');
            $table->nullableBelongsTo('positions')->onDelete('SET NULL');
            $table->nullableBelongsTo('departments')->onDelete('SET NULL');
            $table->nullableBelongsTo('sections')->onDelete('SET NULL');
            $table->nullableBelongsTo('schemes')->onDelete('SET NULL');
            $table->nullableBelongsTo('grades')->onDelete('SET NULL');

            $table->string('name');
            $table->string('ic', 25)->unique()->index()->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone_no', 25)->nullable();
            $table->string('honourary')->nullable();
            
            $table->string('current_role_login');

            $table->rememberToken();
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
