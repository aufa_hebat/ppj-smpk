<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpjhkTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createPpjhk();
        $this->createPpjhkBQ();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        $tables = [
            'ppjhk_sub_items',
            'ppjhk_items',
            'ppjhk_elements',
            'ppjhk',
        ];
        foreach ($tables as $table) {
            Schema::dropIfExists($table);
        }
        Schema::enableForeignKeyConstraints();
    }


    private function createPpjhk()
    {
        Schema::create('ppjhk', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('users');
            $table->belongsTo('departments');
            $table->belongsTo('ssts');
            $table->unsignedInteger('no')
                ->nullable()
                ->comment('Each project has their own counter.');
            $table->integer('status')
                ->nullable()
                ->comment('0-Draft 1-Send');
            $table->standardTime();
        });
    }


    private function createPpjhkBQ()
    {
        Schema::create('ppjhk_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('ppjhk');
            $table->belongsTo('vo_elements');            
            $table->amount('quantity_ppjhk')->comment('quantity ppjhk');
            $table->amount('amount_ppjhk')->comment('amount ppjhk');
            $table->amount('percent_ppjhk')->comment('percent ppjhk');
            $table->amount('net_amount')->comment('net omission / addition');
            
            $table->integer('status')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');
            $table->standardTime();
        });

        Schema::create('ppjhk_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('ppjhk');
            $table->belongsTo('vo_items');
            $table->belongsTo('vo_elements');
            $table->belongsTo('ppjhk_elements');
            $table->nullableBelongsTo('vo_actives');

            $table->amount('quantity_ppjhk')->comment('quantity ppjhk');
            $table->amount('amount_ppjhk')->comment('amount ppjhk');
            $table->amount('percent_ppjhk')->comment('percent ppjhk');
            $table->amount('net_amount')->comment('net omission / addition');

            $table->integer('status')->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });

        Schema::create('ppjhk_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            // $table->belongsTo('acquisitions');
            $table->belongsTo('ssts');
            $table->belongsTo('ppjhk');
            $table->belongsTo('vo_sub_items');
            $table->belongsTo('vo_items');
            $table->belongsTo('vo_elements');
            $table->belongsTo('ppjhk_elements');
            $table->belongsTo('ppjhk_items');
            $table->nullableBelongsTo('vo_actives');

            $table->amount('rate_per_unit');
            $table->amount('quantity_ppjhk')->comment('quantity ppjhk');
            $table->amount('amount_ppjhk')->comment('amount ppjhk');
            $table->amount('percent_ppjhk')->comment('percent ppjhk');
            $table->amount('net_amount')->comment('net omission / addition');
            $table->amount('baki_quantity')->comment('jumlah kuantiti yg boleh dibayar');
            $table->amount('paid_quantity')->comment('jumlah kuantiti yg sudah dibayar');
            $table->integer('status')->nullable();   
            $table->integer('wps_status')->nullable();        
            $table->amount('total_quantity')->comment('quantity addition - quantity omission');
            $table->string('status_lock', 45)->nullable();

            $table->nullableBelongsTo('users', 'created_by');
            $table->nullableBelongsTo('users', 'updated_by');

            $table->standardTime();
        });
    }
}
