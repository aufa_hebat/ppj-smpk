<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiguresTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('report_figures', function (Blueprint $table) {
            $table->increments('id');
            $table->name('category');
            $table->name('header');
            $table->name('icon');
            $table->name('content_color');
            $table->name('content');
            $table->name('footer');
            $table->json('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('report_figures');
    }
}
