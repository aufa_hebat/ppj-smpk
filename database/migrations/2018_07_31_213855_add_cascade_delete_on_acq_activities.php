<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCascadeDeleteOnAcqActivities extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('acquisition_activities', function (Blueprint $table) {
            $table->dropForeign('acquisition_activities_parent_id_foreign');
            $table->referenceOn('parent_id', 'acquisition_activities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('acquisition_activities', function (Blueprint $table) {
            $table->dropForeign('acquisition_activities_parent_id_foreign');
            $table->referenceOn('parent_id', 'acquisition_activities');
        });
    }
}
