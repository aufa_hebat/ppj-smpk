<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefVocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_vocs', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            $table->belongsTo('departments')->nullable();
            $table->belongsTo('users')->nullable();
            $table->belongsTo('positions')->nullable();
            $table->string('role', 40)->nullable();
            $table->integer('status')
            ->nullable()
            ->comment('0-InActive 1-Active');
            $table->date('appointment_date')->nullable();
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_vocs');
    }
}
