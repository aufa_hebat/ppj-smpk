<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSofasTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('sofas', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');
            $table->date('signature_date')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('sofas');
    }
}
