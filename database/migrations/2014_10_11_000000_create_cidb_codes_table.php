<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCidbCodesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cidb_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->name('code');
            $table->name('category');
            $table->name();
            $table->name('type'); // gred/khusus/category
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('cidb_codes');
    }
}
