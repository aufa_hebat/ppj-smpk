<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewLogsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('review_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->belongsTo('acquisitions');
            $table->integer('eot_id')->nullable();
            $table->integer('ipc_id')->nullable();
            $table->integer('ppk_id')->nullable();
            $table->integer('ppjhk_id')->nullable();
            $table->integer('warning_id')->nullable();
            $table->by('users', 'created')->nullable()->comment('acquisitions creator id');
            $table->by('users', 'requested')->nullable();
            $table->at('requested')->nullable();
            $table->by('users', 'approved')->nullable();
            $table->at('approved')->nullable();
            $table->description('remarks')->nullable();
            $table->description('quiry_remarks')->nullable()->comment('for S3 AND S4 only');
            $table->string('reviews_status', 255)->nullable();
            $table->boolean('progress')->default(false)->comment('0, before Urusetia. 1, after Urusetia');
            $table->string('status', 10)->nullable()->comment('eg: S1-S7');
            $table->string('department', 100)->comment('eg: BPUB/Undang-Undang');
            $table->by('users', 'task')->nullable()->comment('id after being assign');
            $table->by('users', 'law_task')->nullable()->comment('id after being assign by law');
            $table->string('type', 100)->comment('eg: acquisitions/sst/contract/eot/etc...');
            $table->string('document_contract_type', 100)->nullable()->comment('eg: for document contract type');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('review_logs');
    }
}
