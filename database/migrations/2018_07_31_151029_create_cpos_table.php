<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCposTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cpos', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');

            $table->date('agreement_date')->nullable();
            $table->date('partial_date')->nullable();
            $table->date('assessment_date')->nullable();
            $table->date('cpo_start_date')->nullable();
            $table->date('cpo_end_date')->nullable();

            $table->string('reference_no', 100)->nullable();
            $table->string('clause', 100)->nullable();
            $table->string('part_of_work_title', 255)->nullable();

            $table->amount('estimation_amount');
            $table->amount('cpo_amount');
            $table->amount('lad_per_day');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('cpos');
    }
}
