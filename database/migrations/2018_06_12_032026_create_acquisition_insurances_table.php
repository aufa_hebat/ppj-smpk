<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');

            /*
             * Public Liability
             */
            $table->integer('public_insurance_name')->nullable();
            $table->string('public_policy_no', 40)->nullable();
            $table->amount('public_policy_amount');
            $table->date('public_received_date')->nullable();
            $table->date('public_start_date')->nullable();
            $table->date('public_expired_date')->nullable();
            $table->date('public_proposed_date')->nullable();
            $table->date('public_approval_proposed_date')->nullable();
            $table->date('public_approval_received_date')->nullable();

            /*
             * For work
             */
            $table->integer('work_insurance_name')->nullable();
            $table->string('work_policy_no', 40)->nullable();
            $table->amount('work_policy_amount');
            $table->date('work_received_date')->nullable();
            $table->date('work_start_date')->nullable();
            $table->date('work_expired_date')->nullable();
            $table->date('work_proposed_date')->nullable();
            $table->date('work_approval_proposed_date')->nullable();
            $table->date('work_approval_received_date')->nullable();

            /*
             * Compensation
             */
            $table->integer('compensation_insurance_name')->nullable();
            $table->string('compensation_policy_no', 40)->nullable();
            $table->amount('compensation_policy_amount');
            $table->date('compensation_received_date')->nullable();
            $table->date('compensation_start_date')->nullable();
            $table->date('compensation_expired_date')->nullable();
            $table->date('compensation_proposed_date')->nullable();
            $table->date('compensation_approval_proposed_date')->nullable();
            $table->date('compensation_approval_received_date')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_insurances');
    }
}
