<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcceptancesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_acceptances', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();
            $table->by('users', 'verified')->nullable();
            $table->by('users', 'rejected')->nullable();
            $table->at('verified')->nullable();
            $table->at('rejected')->nullable();
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_acceptances');
    }
}
