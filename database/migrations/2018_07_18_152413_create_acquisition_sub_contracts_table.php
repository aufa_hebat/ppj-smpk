<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionSubContractsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_sub_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');
            $table->belongsTo('companies');
            $table->amount('amount');
            $table->string('description');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_sub_contracts');
    }
}
