<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->user();
            $table->hashslug();
            $table->belongsTo('workflows');
            $table->unsignedBigInteger('taskable_id')->index();
            $table->string('taskable_type');

            $table->is('done', false);
            $table->at('done');
            $table->by('users', 'done');
            $table->remarks('done_remarks');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
