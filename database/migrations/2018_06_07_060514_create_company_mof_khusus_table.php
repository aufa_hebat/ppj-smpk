<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyMofKhususTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('company_mof_khusus', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->integer('user_id')->nullable();

            /*
             * Foreign Keys
             */
            $table->integer('company_id')->nullable();
            $table->integer('areas_id')->nullable();

            $table->string('khusus', 40)->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('company_mof_khusus');
    }
}
