<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            $table->belongsTo('acquisitions');
            $table->string('department_id', 255)->nullable();
            $table->string('position_id')->nullable();

            $table->date('appointment_date')->nullable();
            $table->string('name', 255)->nullable();

            $table->string('approval', 100)->nullable();
            $table->string('role', 40)->nullable();
            $table->string('category', 40)->nullable();
            $table->string('agency_name', 255)->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
