<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChartsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('report_charts', function (Blueprint $table) {
            $table->increments('id');
            $table->name('category');
            $table->name('type');
            $table->name('title');
            $table->json('labels');
            $table->json('datasets');
            $table->name('legend_position');
            $table->json('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('report_charts');
    }
}
