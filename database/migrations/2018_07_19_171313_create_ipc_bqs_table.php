<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpcBqsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ipc_bqs', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();
            $table->user();
            $table->nullableBelongsTo('ipc_invoices');
            // $table->nullableBelongsTo('bq_subitem');
            $table->integer('bq_subitem_id')->nullable();
            $table->integer('bq_item_id')->unsigned()->nullable();
            $table->foreign('bq_item_id')->references('id')->on('bq_items')->onDelete('cascade');
            $table->integer('bq_element_id')->unsigned()->nullable();
            $table->foreign('bq_element_id')->references('id')->on('bq_elements')->onDelete('cascade');
            $table->nullableBelongsTo('ssts');
            $table->amount('quantity');
            $table->amount('amount');
            $table->string('status')->comment('N for normal and V for VO')->default('N');
            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ipc_bqs');
    }
}
