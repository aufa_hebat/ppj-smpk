<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_type', function (Blueprint $table) {
            $table->increments('id');
            $table->hashslug();

            $table->string('module', 40)->nullable();
            $table->integer('acquisition_type_id')->nullable()->comment('refer acquisition_type table');
            $table->string('name', 200)->nullable();


            $table->integer('status')
            ->nullable()
            ->comment('0-InActive 1-Active');

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_type');
    }
}
