<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('acquisition_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->user();
            $table->hashslug();

            /*
             * Foreign Keys
             */
            $table->belongsTo('ssts');

            $table->date('revenue_stamp_date')->nullable();
            $table->integer('preparation_period')->nullable();
            $table->integer('late_days')->nullable();
            $table->date('proposed_date')->nullable();
            $table->date('submitted_date')->nullable();
            $table->decimal('base_lending_rate', 5, 2)->nullable();
            $table->amount('LAD_rate')->nullable();

            $table->standardTime();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('acquisition_documents');
    }
}
