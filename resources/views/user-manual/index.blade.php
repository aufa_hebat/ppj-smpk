@extends('layouts.admin')

@push('scripts')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush

@section('content')

    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @slot('card_body')
                    <div class="row">
                        <div class="col-12">
                            <span style="text-transform:uppercase;text-decoration:underline;">Manual Pra-Kontrak</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2" style="text-align:center;">
                            1.
                        </div>
                        <div class="col-10">                            
                            <a href="{{ asset('storage/manual/PRA_Kontrak.pdf') }}" target="_blank">Pra Kontrak&nbsp;<i class="fe fe-download"></i></a>                            
                        </div>
                    </div>
                    <div class="row"><div class="col-12"><br/></div></div>
                    <div class="row">
                        <div class="col-2" style="text-align:center;">
                            2.
                        </div>
                        <div class="col-10">
                            <a href="{{ asset('storage/manual/Pasca_Kontrak.pdf') }}" target="_blank">Pasca Kontrak&nbsp;<i class="fe fe-download"></i></a>                                                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2" style="text-align:center;">

                        </div>
                        <div class="col-10">

                        </div>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>

@endsection