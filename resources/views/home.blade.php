@extends('layouts.admin')

@push('scripts')
	@include('components.charts.utils')
	<script>
		jQuery(document).ready(function($) {
			$(document).on('click', '.sync', function(event) {
				event.preventDefault();
				axios.post(route('api.report.sync')).then(function(response){
					redirect(route('home'));
				})
			});
		});
	</script>
@endpush

@section('content')
	@if(user()->current_role_login == ''|| user()->current_role_login == 'user')
		<div class="row justify-content-center">  
		  	<div class="col">
			  	@component('components.card')
				  	@slot('card_body')
						<div style="font-size:20px; text-align:center; color:royalblue;">Harap Maaf. <br/>
							Anda tidak dibenarkan untuk mengakses sistem ini.<br/>
							Sila hubungi administrator untuk maklumat lanjut.<br/>
							Terima Kasih
						</div>
				  	@endslot
				@endcomponent
			</div>
		</div>
	@else
		@include('users.tasks')
                <!-- Telah dibuang, kerana kita sync guna engine
		<div class="row">
			<div class="col">
				<div data-balloon="Sync Chart Data" data-balloon-pos="up"
					class="btn btn-sm btn-success float-right sync">
					@icon('fe fe-refresh-cw')
				</div>
			</div>
		</div> -->
{{--	<h4>Pemantauan Projek</h4>
	@include('contract.post.monitoring.partials.dashboard')--}}


		{{-- <h4>Jualan</h4>
		@include('sales.partials.dashboard') --}}
		<h4>Perolehan</h4>
		@include('contract.pre.partials.dashboard')
		<h4>Pengguna</h4>
		@include('manage.users.partials.dashboard')
		<h4>Syarikat</h4>
		@include('manage.company.partials.dashboard')
	@endif
@endsection
