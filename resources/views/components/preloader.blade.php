<style>
	.loader-container {
		position: fixed;
		left: 0%;
		top: 0%;
		z-index: 9000;
		background-color: rgba(255, 255, 255, 0.5);
		width: 100%;
		height: 100%;
		overflow: auto;
	}
	.loader {
		position: absolute;
		left: calc(50vw - 20px);
		top: calc(50vh - 20px);
		border: 2px solid rgba(158, 158, 158, 0.5); 
		border-top: 2px solid #3097D1;
		border-radius: 50%;
		width: 40px;
		height: 40px;
		animation: spin 1s linear infinite;
	}
	@keyframes spin {
		0% { transform: rotate(360deg); }
		100% { transform: rotate(0deg); }
	}
</style>
<div class="loader-container" style="display: none;">
	<div class="loader"></div>
</div>