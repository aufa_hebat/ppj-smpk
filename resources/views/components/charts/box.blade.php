<div class="card border-0">
	<div class="card-body p-3 text-center">
		<div class="text-right text-{{ $header_color or 'green' }}">
			{{ $header or '' }}
			<i class="{{ $icon or '' }}"></i>
		</div>
		<div class="h4 m-0 text-{{ $content_color or 'black' }}"">
			<canvas id="{{ $chart_id or 'chart-id' }}"></canvas>
			{{ $content or '' }}
		</div>
		<div class="mb-4 text-{{ $footer_color or 'muted' }}"">
			{{ $footer or '' }}
		</div>
	</div>
</div>