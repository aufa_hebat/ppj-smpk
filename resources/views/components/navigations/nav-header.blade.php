@guest
{{--  <div class="header py-0">  --}}
<div class="">
@else
<div class="header-bg py-0">
{{--  <div class="">      --}}
@endguest

    <div class="container">
        <div class="d-flex">
            {{-- Header Brand --}}
            <a class="header-brand" href="@auth {{ route('home') }} @else {{ url('/') }} @endauth">
                @include('components.logo')
            </a>
            @guest
                <div class="float-center" style="padding-top:20px;">
                    <span style="font-family: Georgia, serif; font-size:28px; color:#0c4da2; font-weight:bold;">Sistem Maklumat Pengurusan Kontrak (COINS)</span>
                </div>
            @endguest
            {{-- Right Navigation --}}
            <div class="d-flex order-lg-2 ml-auto">
                <div class="nav-item d-none d-md-flex">
                    
                    @includeWhen(sizeof(locales()) > 1, 'components.language')
                </div>
                <div class="nav-item d-none d-md-flex">  
                    @guest
                        <div class="float-right" style="padding-top:10px;">
                            <a href="{{ route('registration.companies.index') }}" id="register" style="color:#fff; padding:10px 10px;" class="header_button ">{{ __('Pendaftaran Syarikat') }}</a>
                            {{-- <a id="register" style="color:#fff; padding:10px 10px;" class="header_button " class="btn btn-sm btn-outline-default" href="{{ route('registration.companies.index') }}">{{ __('Daftar Syarikat') }}</a> --}}
                            {{-- <a href="#" id="register" style="color:#fff; padding:10px 10px;" class="header_button " data-toggle="modal" data-target="#modalDisclaimer">{{ __('Pendaftaran Syarikat') }}</a> --}}
                            {{--  <a href="{{ url('/') }} " style="color:#fff; padding:10px 10px;" class="header_button">{{ __('Laman Utama') }}</a>  --}}
                            {{--  <a href="{{ route('login') }}" style="color:#fff; padding:10px 10px;" class="header_button">{{ __('Log Masuk') }}</a>  --}}
                        </div>
                        {{--  <a class="btn btn-sm btn-outline-default" href="{{ route('login') }}">{{ __('Log Masuk') }}</a>  --}}
                    @else
                        <div class="dropdown">
                            <a class="nav-link pr-0 leading-none dropdown-toggle" id="nav-dropdown" 
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="avatar">
                                    <i class="fe fe-user text-light"></i>
                                </span>
                                <span class="ml-2 d-none d-lg-block">
                                    <span class="text-default">{{ user()->name }}</span>
                                    <small class="text-muted d-block mt-1"><span style="color:#fff">{{ user()->rolesToString() }}</span></small>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"  aria-labelledby="nav-dropdown">
                                <a class="dropdown-item" href="{{ route('user.show') }}">
                                    <i class="dropdown-icon fe fe-user"></i> {{ __('Profil') }}
                                </a> <a class="dropdown-item" href="{{ route('lasdjlaksdjasdj2') }}">
                                    <i class="dropdown-icon fe fe-user"></i> {{ __('Tukar Role') }}
                                </a> 
                                 {{--  <a href="{{ route('user.avatar.show') }}" 
                                    class="dropdown-item">
                                    <i class="dropdown-icon fas fa-user-circle"></i> {{ __('Muatnaik Avatar') }}
                                </a>  --}}
                                <a href="{{ route('user.password.show') }}" 
                                    class="dropdown-item" >
                                    <i class="dropdown-icon fas fa-lock"></i> {{ __('Sekuriti') }}
                                </a>
                                @if(user()->current_role_login == 'administrator')
                                    <a href="{{ route('user.logs') }}" 
                                        class="dropdown-item">
                                        <i class="dropdown-icon fas fa-list"></i> {{ __('Log') }}
                                    </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}" 
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                  <i class="dropdown-icon fe fe-log-out"></i> {{ __('Keluar') }}
                                </a>
                            </div>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>