<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs border-0 flex-column flex-lg-row  justify-content-center ">
					<li class="nav-item">
						<a href="{{ route('home') }}" class="nav-link"><i class="fe fe-home text-dark"></i> Utama</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>