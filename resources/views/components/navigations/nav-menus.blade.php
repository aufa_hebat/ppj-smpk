<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs border-0 flex-column flex-lg-row  justify-content-center ">
					<li class="nav-item">
						<a href="{{ route('home') }}" class="nav-link"><i class="fe fe-home text-dark"></i> Utama</a>
					</li>
                    @can('menu-urus_index')
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
								@icon('fe fe-grid text-primary') Urus
							</a>
							<div class="dropdown-menu dropdown-menu-arrow">
								@can('urus-pengguna_index')
									<a href="{{ route('manage.users.index') }}" class="dropdown-item ">Pengguna</a>
								@endcan
								@can('urus-jawatankuasa-penilai_index')	
									<a href="{{ route('manage.committee.assessments.index') }}" class="dropdown-item ">Jawatan Kuasa Penilai</a>
								@endcan
								@can('urus-jawatankuasa-pembuka_index')
									<a href="{{ route('manage.committee.approvals.index') }}" class="dropdown-item ">Jawatan Kuasa Pembuka</a>
								@endcan							
								@if(user()->current_role_login == 'administrator' || (user()->department_id == 9 && user()->executor_department_id == 25))     
									<a href="{{ route('manage.companies.index') }}" class="dropdown-item ">Syarikat</a> 
								@endif
								@can('urus-penyerahan-tugas_index')
									<a href="{{ route('acquisition.review.index') }}" class="dropdown-item ">Penyerahan Tugas</a>
								@endcan
								@if(user()->current_role_login == 'administrator' || user()->current_role_login == 'Kaunter BPK') 
									<a href="{{ route('manage.document_acceptances.index') }}" class="dropdown-item ">Penerimaan Dokumen</a>
								@endif
								@can('urus-pembatalan-perolehan_index')
									<a href="{{ route('acquisition.cancel.index') }}" class="dropdown-item ">Senarai Pembatalan Perolehan</a>
								@endcan
								@can('urus-pembatalan-kontrak_index')
									<a href="{{ route('termination.termination.index') }}" class="dropdown-item ">Senarai Penamatan Kontrak</a>
								@endcan
                                                        	@can('urus-kontrak-tamat_index')
									<a href="{{ route('contract.post.contract-completed.index') }}" class="dropdown-item ">Senarai Kontrak Selesai</a>
								@endcan
								@can('acl_index')
									<a href="{{ route('manage.acl.index') }}" class="dropdown-item ">Kawalan Akses</a>
								@endcan                                                        
                                                        	@can('utiliti_index')
									<a href="{{ route('manage.utility.index') }}" class="dropdown-item ">Utiliti</a>
                                                        	@endcan                                                        
                                                        	@can('urus-jawatankuasa-vo_index')
									<a href="{{ route('contract.post.variation-order-result.index') }}" class="dropdown-item ">Perubahan Kontrak</a>
                                                         	@endcan
							</div>
						</li>
					@endcan
                    @can('menu-pra-kontrak_index')
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
								<i class="fe fe-file text-primary"></i> Pra-Kontrak
							</a>
							<div class="dropdown-menu dropdown-menu-arrow">
                                @can('kelulusan-perolehan_index')
									<a href="{{ route('contract.pre.index') }}" class="dropdown-item ">Senarai Kelulusan Perolehan</a>
                                @endcan
                                @can('perolehan_index')
                                    <a href="{{ route('acquisition.acquisition.index') }}" class="dropdown-item ">Senarai Dokumen Perolehan</a>
                                @endcan
                                @can('buka-peti_index')                                                        
									<a href="{{ route('box.index') }}" class="dropdown-item">Buka Peti</a>                                                            
                                @endcan
                                @can('keputusan_index')
									<a href="{{ route('evaluation.index') }}" class="dropdown-item ">Keputusan</a>
                                @endcan
							</div>
						</li>
                    @endcan
                    @can('menu-paska-kontrak_index')
						<li class="nav-item">
							<a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
								<i class="fe fe-file text-primary"></i> Pasca-Kontrak
							</a>
							<div class="dropdown-menu dropdown-menu-arrow">
                                @can('surat-setuju-terima_index')
									<a href="{{ route('acceptance-letter.index') }}" class="dropdown-item ">Surat Setuju Terima</a>
                                @endcan
                                @can('dokumen-kontrak_index')
									<a href="{{ route('contract.post.index') }}" class="dropdown-item ">Dokumen Kontrak</a>
                                @endcan
                                @can('pemantauan-projek_index')
									<a href="{{ route('acquisition.monitoring.index') }}" class="dropdown-item ">Pemantauan Projek</a>
                                @endcan
                                @can('sijil-bayaran-interim_index')
									<a href="{{ route('contract.post.ipc.index') }}" class="dropdown-item ">Sijil Bayaran Interim</a>
                                @endcan
                                @can('perubahan-kerja_index')
									<a href="{{ route('contract.post.variation-order.index') }}" class="dropdown-item ">Perubahan Kontrak</a>	
                                @endcan
                                @can('perakuan-kelambatan-dan-lanjutan-kontrak_index')
									<a href="{{ route('extension.extension.index') }}" class="dropdown-item ">Sijil Kelambatan & Lanjutan Kontrak</a>	
                                @endcan
                                @can('bayaran-muktamad_index')
									<a href="{{ route('contract.post.sofa.index') }}" class="dropdown-item ">Bayaran Muktamad</a>
                                @endcan
                                @can('pelepasan-bon_index')
									<a href="{{ route('contract.post.release.index') }}" class="dropdown-item ">Pelepasan Bon</a>
                                @endcan
							</div>
						</li>
                    @endcan
                    @can('menu-taklimat_index')
						<li class="nav-item">
                            @can('taklimat_index')
								<a href="{{ route('briefing.index') }}" class="nav-link">
									<i class="fe fe-list text-danger"></i> Taklimat
								</a>
                            @endcan
						</li>
                    @endcan                                                                      
                    @can('menu-lawatan-tapak_index')
                        <!-- Add one more filter, just BPUB only can view -->
                        @if(user()->current_role_login == 'administrator' || (( user()->current_role_login == 'pengesah' 
                            || user()->current_role_login == 'penyedia' 
                            || user()->current_role_login == 'penyemak') && user()->executor_department_id == 25))
                            {{-- pegawai bpub --}}
                            @if(user()->current_role_login == 'administrator' || user()->department->id == '9')
								<li class="nav-item">
                                    @can('lawatan-tapak_index')
										<a href="{{ route('site-visit.index') }}" class="nav-link">
											<i class="fe fe-clipboard text-primary"></i> Lawatan Tapak
										</a>
					            	@endcan
                                </li>      
                            @endif
                        @endif                                                                                                                  
                    @endcan                                                                                
                    @can('menu-jualan_index')
						<li class="nav-item dropdown">
                            @can('jualan_index')
								<a href="{{ route('sale.index') }}" class="nav-link"><i class="fe fe-dollar-sign text-success"></i> Jualan</a>
                            @endcan
						</li>
                    @endcan
                    @can('menu-laporan_index')
						{{-- <li class="nav-item">
                            @can('laporan_index')
								<!--<a href="{{ route('reports.index') }}" class="nav-link"><i class="fe fe-clipboard text-warning"></i> Laporan</a>-->
                                @if(user()->department_id == 9 && user()->executor_department_id == 23)                                               
                                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
										<i class="fe fe-clipboard text-warning"></i> Laporan
									</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('reporting.index') }}" class="dropdown-item ">Laporan Prestasi</a>
                                    </div>
                                @endif
                            @endcan 
						</li> --}}
					@endcan
					<li class="nav-item">
						<a href="{{ route('user_manual.index') }}" class="nav-link"><i class="fe fe-book-open text-dark"></i> Manual Pengguna</a>
					</li>					
				</ul>
			</div>
		</div>
	</div>
</div>