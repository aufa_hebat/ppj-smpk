@push('scripts')
	<script>
		jQuery(document).ready(function($) {
            $('#datetimepicker-{{ $id }}').datetimepicker(@isset($config) @json($config) @endisset);
		});
	</script>
@endpush

<div class="form-group row">
    <label for="{{ snake_case($input_label) }}" 
        class="{{ $input_label_class or 'col col-form-label' }}">
        {{ __($input_label) }}
        @isset($required)
            <span style="color:red;"> * </span>
        @endisset        
    </label>
    <div class="input-group date {{ $input_container_class or 'col' }}" id="datetimepicker-{{ $id }}" data-target-input="nearest">
        <input type="text" 
            @isset($readonly)
            readonly="true" 
            @endisset
            class=" datetimepicker-input form-control{{ $errors->has(snake_case($input_label)) ? ' is-invalid' : '' }} {{ $input_classes or '' }}"
            data-target="#datetimepicker-{{ $id }}"
            @isset($required) required @endisset 
            @isset($name)
                name="{{ $name }}" 
            @else
                name="{{ snake_case($input_label) }}" 
            @endisset
            @isset($id)
                id="{{ $id }}" 
            @else
                id="{{ snake_case($input_label) }}" 
            @endisset
            @isset($value)
                value="{{ $value }}"
            @endisset
            @isset($onkeypress)
                onkeypress="{{$onkeypress}}"
            @endisset            
            autofocus/>
        <div class="input-group-append" data-target="#datetimepicker-{{ $id }}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fe fe-calendar"></i></div>
        </div>
    </div>
    @if ($errors->has(snake_case($input_label)))
        <span class="invalid-feedback">
            <strong>{{ $errors->first(snake_case($input_label)) }}</strong>
        </span>
    @endif
</div>