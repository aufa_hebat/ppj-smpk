<div class="form-group row">
    <label for="{{ snake_case($input_label) }}" 
        class="{{ $input_label_class or 'col col-form-label' }}">
        {{ __($input_label) }}
    </label>

    <div class="{{ $input_container_class or 'col-md-12' }}">
        <input 
            @isset($readonly)
            readonly="true" 
            @endisset
            type="{{ $type or 'file' }}" 
            placeholder="@isset($placeholder) {{ __($placeholder) }} @else {{ __($input_label) }} @endisset" 
            class="form-control{{ $errors->has(snake_case($input_label)) ? ' is-invalid' : '' }} {{ $input_classes or '' }}" 
            
            @isset($id)
                id="{{ $id }}" 
            @else
                id="{{ snake_case($input_label) }}" 
            @endisset

            @isset($name)
                name="{{ $name }}" 
            @else
                name="{{ snake_case($input_label) }}" 
            @endisset

            value="{{ old(snake_case($input_label)) }}" 
            
            @isset($accept)
                accept="{{ $accept }}"
            @endisset
            
            @isset($required) required @endisset autofocus>

            @isset($multiple)
                <div class="btn btn-danger btn-sm float-right" onclick="$(this).parent().parent().remove()">{{ __('Batal') }}</div>
            @endisset

        @if ($errors->has(snake_case($input_label)))
            <span class="invalid-feedback">
                <strong>{{ $errors->first(snake_case($input_label)) }}</strong>
            </span>
        @endif
    </div>
</div>