@if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
    <div class="btn-group float-right">
        {{ html()->a(route($route), __('Batal'))->class('btn btn-outline-default') }}

        <button class="btn btn-primary {{ $form_submit_btn_class or 'submit-action-btn' }}">
            @icon('fe fe-save')
            {{ __('Kemaskini') }}
        </button>
    </div>
@endif