<div class="form-group row">
    @if($input_label != '')
    <label for="{{ snake_case($input_label) }}" 
        class="{{ $input_label_class or 'col col-form-label' }}">
        {{ __($input_label) }}
        @isset($required)
            <span style="color:red;"> * </span>
        @endisset
    </label>
    @endisset
    <div class="{{ $input_container_class or 'col' }}">
        <input 
            @isset($readonly)
            readonly="true" 
            @endisset
            type="{{ $type or 'text' }}" 
            placeholder="@isset($placeholder) {{ __($placeholder) }} @else {{ __($input_label) }} @endisset" 
            class="form-control{{ $errors->has(snake_case($input_label)) ? ' is-invalid' : '' }} {{ $input_classes or '' }}" 
            
            @isset($id)
                id="{{ $id }}" 
            @else
                id="{{ snake_case($input_label) }}" 
            @endisset

            @isset($name)
                name="{{ $name }}" 
            @else
                name="{{ snake_case($input_label) }}" 
            @endisset
            
            @isset($onkeyup)
                onkeyup="{{$onkeyup}}"
            @endisset

            @isset($onkeypress)
                onkeypress="{{$onkeypress}}"
            @endisset
            
            @isset($step)
                step="{{$step}}"
            @endisset

            @isset($value)
                value="{{$value}}"
            @endisset
            
            @isset($readonly)
                readonly="{{$readonly}}"
            @endisset
            
            @isset($maxlength)
                maxlength="{{$maxlength}}"
            @endisset

            @isset($min)
                min="{{$min}}"
            @endisset

            @isset($style)
                style="{{$style}}"
            @endisset

            value="{{ old(snake_case($input_label)) }}" 

            @isset($required) 
                required 
            @endisset 

            autofocus>

        @if ($errors->has(snake_case($input_label)))
            <span class="invalid-feedback">
                <strong>{{ $errors->first(snake_case($input_label)) }}</strong>
            </span>
        @endif

        @isset($note)
            <span style="font-size:12px;">
                {{$note}}
            </span>
        @endisset
    </div>
</div>