@guest
    <div class="float-left" style="padding-top:10px;font-size:30px;">
        <img height="{{ $height or '80px' }}" src="{{ asset('images/logo.png') }}" class="pr-2" alt="{{ config('app.name') }}">
        &nbsp;&nbsp; 
        {{--  <span style="font-family: Georgia, serif; font-size:28px; color:#0c4da2; font-weight:bold;">Sistem Maklumat Pengurusan Kontrak</span>  --}}
        {{--  <img height="{{ $height or '50px' }}" src="{{ asset('images/coins-1.png') }}" class="pr-2">  --}}
        {{--  <img height="{{ $height or '70px' }}" src="{{ asset('images/logo_jata_negara.png') }}" class="pr-2">  --}}
    </div>
    
@else
    <div style="height:70px;" height="{{ $height or '70px' }}">&nbsp;</div>
@endguest