<div class="table-responsive">
	<table 
		class="table table-sm table-transparent table-hover {{ $table_classes or '' }}" 
		id="{{ $table_id or 'table-id' }}">
		@isset($thead)
			<thead>
				{{ $thead ?? '' }}
			</thead>
		@endisset

		@isset($tbody)
			<tbody>
				{{ $tbody ?? '' }}
			</tbody>
		@endisset

		@isset($tfoot)
			<tfoot>
				{{ $tfoot ?? '' }}
			</tfoot>
		@endisset
	</table>
</div>