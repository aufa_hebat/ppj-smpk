@component('components.card')
    @slot('card_body')
    	<div class="text-center">
    		@icon('fe fa-2x fe-alert-triangle text-danger')
	        <p class="font-weight-light">
	        	Maaf, tiada maklumat terkini.
	        </p>
    	</div>
    @endslot
@endcomponent