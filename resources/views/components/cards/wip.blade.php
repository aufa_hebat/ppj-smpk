@component('components.card')
    @slot('card_body')
    	<div class="text-center">
    		@icon('fe fa-2x fe-alert-triangle text-warning')
	        <p class="font-weight-light">
	        	{{ $message or 'Maaf, masih dalam pembangunan.' }}
	        </p>
    	</div>
    @endslot
@endcomponent