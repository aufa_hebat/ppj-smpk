<div class="tab-pane {{ isset($active) ? 'show active' : 'fade' }}" 
	id="{{ $id }}" role="tabpanel" 
	aria-labelledby="{{ $id }}-tab">
	{{ $content }}
</div>