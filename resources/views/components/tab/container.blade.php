<div class="tab-content {{ $tab_classes or '' }}" 
	id="{{ $id }}-tab-content">
    {{ $tabs }}
</div>