{{--  <h1 class="page-title pb-4">{{ __($title) }}</h1>  --}}
@component('components.pages.title-sub')
    @slot('title_sub_content')
        @php 
            $new_eot_date = null;

            if(!empty($appointed->eot) && !empty($appointed->eot->pluck('eot_approve')) && !empty($appointed->eot->pluck('eot_approve')->where('approval', '=', '1') && $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->count() > 0)){
                $new_eot_date   = Carbon::CreateFromFormat('Y-m-d H:i:s', $appointed->eot->pluck('eot_approve')->where('approval', '=', '1')->last()->approved_end_date)->format('d/m/Y');
            }
        @endphp
        <span class="font-weight-bold">Tajuk: </span>{{ $sst->acquisition->title }}
        <br>
        <span class="font-weight-bold">No. Kontrak: </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>{{ money()->toHuman($appointed->offered_price) }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ $sst->start_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period.' '.$sst->period_type->name }}&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ $sst->end_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
        @if(!empty($new_eot_date))
            <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
        @endif
    @endslot
@endcomponent