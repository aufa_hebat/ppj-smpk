<div class="row">
    <div class="col justify-content-center {{ $classes or 'text-center' }}">
        @component('components.card')
            @slot('card_body')
                {{ $title_sub_content }}
            @endslot
        @endcomponent
    </div>
</div>