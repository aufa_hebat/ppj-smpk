{{--  @component('layouts.auth')
	@slot('auth_content')
		<form class="card"  method="POST" action="{{ route('login') }}">
			@csrf
			<div class="card-body p-6">
		  	<div class="card-title text-center" style="font-size:14px; font-family:Verdana; font-weight:bold;">{{ __('Sistem Maklumat Pengurusan Kontrak') }}</div>
		  	<hr>
		  	<div class="form-group">
					<label class="form-label">{{ __('ID Pengguna') }}</label>
					<input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Masukkan ID Pengguna">
					@if ($errors->has('email'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
		  	</div>
		  	<div class="form-group">
					<label class="form-label">
			  		Kata Laluan
			  		<a href="{{ route('password.request') }}" class="float-right small">{{ __('Lupa Kata Laluan') }}</a>
					</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Kata Laluan">
					@if ($errors->has('password'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
		  	</div>
		  	<div class="form-group">
					
		  	</div>
		  	<div class="form-footer">
					<button type="submit" class="btn btn-primary btn-block">{{ __('Log Masuk') }}</button>
		  	</div>
			</div>
	  </form>
	  <div class="text-center text-muted">
		
	  </div>
	@endslot
@endcomponent  --}}

@extends('layouts.auth')
@push('scripts')
<script>
	jQuery(document).ready(function($) {
		redirect(route('welcome'));
	});
</script>
@endpush