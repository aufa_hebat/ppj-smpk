@extends('layouts.app')

@push('styles')
	<style>
		tr, td {
			padding: 4px 4px 4px 4px !important;
			margin: 4px 4px 4px 4px !important;
		}
	</style>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.box.partials.scripts')
			@component('components.card')
				@slot('card_body')
					@include( $reportViewPath )
				@endslot
				@slot('card_footer')
					<div class="pull-right btn-group">
						<a href="{{ url()->previous() }}"
						   class="btn btn-default border-primary">
							{{ __('Kembali') }}
						</a>
						<a href="{{ route($routeName, ['hashslug'=> $hashslug, 'download'=>'pdf'] ) }}"
						   class="btn btn-success">
							@icon('fe fe-download') {{ __('Muat Turun') }}
						</a>
					</div>

				@endslot
			@endcomponent
		</div>
	</div>
@endsection