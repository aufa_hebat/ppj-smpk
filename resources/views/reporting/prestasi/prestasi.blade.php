@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {
        });
    </script>
@endpush   
@section('content')
    @component('components.card')
        @slot('card_body')
        
        <h4>Laporan Prestasi Pembayaran Bil....... bagi Bulan {{ $getMonth }} {{ $getYear }} Berakhir pada 30 {{ $getMonth }} {{ $getYear }}</h4>
            <div  class="table-responsive table-bordered">
                <table class="table table-bordered" id="table_resit" width="100%" cellspacing="0">
                    <thead>
                        <tr style="text-align: center">
                            <th rowspan="2" width="5%">Bil</th>
                            <th rowspan="2" width="20%">Pejabat</th>
                            <th rowspan="2" width="15%">Aktiviti</th>
                            <th colspan="3" width="20%">(a) Dalam tempoh 7 hari</th>
                            <th colspan="3" width="20%">(b) Dalam tempoh 8 hingga 14 hari</th>
                            <th colspan="3" width="20%">(c) Melebihi 14 hari</th>
                        </tr>
                        <tr style="text-align: center">
                            <th width="5%">Bil</th>
                            <th width="10%">RM</th>
                            <th width="5%">%</th>
                            <th width="5%">Bil</th>
                            <th width="10%">RM</th>
                            <th width="5%">%</th>
                            <th width="5%">Bil</th>
                            <th width="10%">RM</th>
                            <th width="5%">%</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($allocation))
                            @foreach($allocation as $bil=>$resource)
                            <tr>
                                <td rowspan="2">{{ $bil+1 }}</td>
                                <td rowspan="2">{{ $resource->name }}</td>
                                <td>Mengurus</td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt1 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @php $cnt1++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt1 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt1 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @if($cnt1 == 0)
                                                    @php $cnt1 = $in_7['amount']; @endphp
                                                @else
                                                    @php $cnt1 = $cnt1 + $in_7['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt1,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt2 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @php $cnt2++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt2 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt2 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @if($cnt2 == 0)
                                                    @php $cnt2 = $in_l_14['amount']; @endphp
                                                @else
                                                    @php $cnt2 = $cnt2 + $in_l_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt2,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt3 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @php $cnt3++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt3 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt3 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @if($cnt3 == 0)
                                                    @php $cnt3 = $in_m_14['amount']; @endphp
                                                @else
                                                    @php $cnt3 = $cnt3 + $in_m_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt3,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Pembangunan</td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt4 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @php $cnt4++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt4 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt4 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @if($cnt1 == 0)
                                                    @php $cnt4 = $in_7['amount']; @endphp
                                                @else
                                                    @php $cnt4 = $cnt4 + $in_7['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt4,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt5 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @php $cnt5++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt5 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt5 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @if($cnt5 == 0)
                                                    @php $cnt5 = $in_l_14['amount']; @endphp
                                                @else
                                                    @php $cnt5 = $cnt5 + $in_l_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt5,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt6 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @php $cnt6++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt6 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt6 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @if($cnt6 == 0)
                                                    @php $cnt6 = $in_m_14['amount']; @endphp
                                                @else
                                                    @php $cnt6 = $cnt6 + $in_m_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt6,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                            @endforeach
                        @endif
                        @if(!empty($get_other))
                            @foreach($get_other as $bil_o=>$other)
                            <tr>
                                <td rowspan="2">{{ ($bil+1)+($bil_o+1) }}</td>
                                <td rowspan="2">{{ $other }}</td>
                                <td>Mengurus</td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt7 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @php $cnt7++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt7 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt7 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @if($cnt7 == 0)
                                                    @php $cnt7 = $in_7['amount']; @endphp
                                                @else
                                                    @php $cnt7 = $cnt7 + $in_7['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt7,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt8 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @php $cnt8++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt8 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt8 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @if($cnt8 == 0)
                                                    @php $cnt8 = $in_l_14['amount']; @endphp
                                                @else
                                                    @php $cnt8 = $cnt8 + $in_l_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt8,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt9 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @php $cnt9++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt9 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt9 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @if($cnt9 == 0)
                                                    @php $cnt9 = $in_m_14['amount']; @endphp
                                                @else
                                                    @php $cnt9 = $cnt9 + $in_m_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt9,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Pembangunan</td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt10 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @php $cnt10++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt10 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_7))
                                        @php $cnt10 = 0 ; @endphp
                                        @foreach($inv_7 as $in_7)
                                            @if($in_7['type'] == $resource->id)
                                                @if($cnt10 == 0)
                                                    @php $cnt10 = $in_7['amount']; @endphp
                                                @else
                                                    @php $cnt10 = $cnt1 + $in_7['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt10,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt11 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @php $cnt11++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt11 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_less_14))
                                        @php $cnt11 = 0 ; @endphp
                                        @foreach($inv_less_14 as $in_l_14)
                                            @if($in_l_14['type'] == $resource->id)
                                                @if($cnt11 == 0)
                                                    @php $cnt11 = $in_l_14['amount']; @endphp
                                                @else
                                                    @php $cnt11 = $cnt11 + $in_l_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt11,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt12 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @php $cnt12++; @endphp
                                            @endif
                                        @endforeach
                                        {{ $cnt12 }}
                                    @else
                                    0
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($inv_more_14))
                                        @php $cnt12 = 0 ; @endphp
                                        @foreach($inv_more_14 as $in_m_14)
                                            @if($in_m_14['type'] == $resource->id)
                                                @if($cnt12 == 0)
                                                    @php $cnt12 = $in_m_14['amount']; @endphp
                                                @else
                                                    @php $cnt12 = $cnt12 + $in_m_14['amount']; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($cnt12,2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        @endslot
    @endcomponent
@endsection