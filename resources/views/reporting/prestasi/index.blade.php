@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $("#table_prestasi").DataTable();
        });
    </script>
@endpush
@section('content')
    <div class="row">
        <div class="col">
            <h5>Senarai Laporan Prestasi Kewangan</h5>
            @component('components.card')
                @slot('card_body')
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="table_prestasi" class="table table-sm table-transparent table-hover">
                                    <thead>
                                        <th>Bil</th>
                                        <th>BULAN PEMBAYARAN</th>
                                    </thead>
                                    <tbody>
                                        @if(!empty($repot))
                                            @foreach($repot as $c => $rpt)
                                            <tr>
                                                <td>{{ $c+1 }}</td>
                                                <td><a href="{{ route('reporting.prestasi', ['month'=>$rpt->months,'year'=>$rpt->years]) }}">Laporan Prestasi Kewangan Pembayaran Bil bagi Bulan {{ $rpt->months }} dan Tahun {{ $rpt->years }}</a></td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection