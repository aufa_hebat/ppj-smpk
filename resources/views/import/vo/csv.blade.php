<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('importvo.parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include('components.forms.file', [
                                'input_label' => __('Lampiran Senarai Kuantiti'),
                                'id' => 'csv_file',
                                'name' => 'csv_file',
                                'required' => 'true',
                                'accept' => '.csv',
                            ])
                            <input type="checkbox" name="header" checked hidden> 

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Muat Naik
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    Sila gunakan <strong><a href="{{ asset('storage/bq_sample.csv') }}">lampiran</a></strong> ini
                                </div>
                            </div>
                            @if (session('status'))
                                <div class="alert alert-danger">
                                    @foreach (session('status') as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <script type="text/javascript">
                                @if(!empty($status) && $status == "Berjaya Disimpan")
                                    window.top.location.reload();
                                @endif
                            </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>