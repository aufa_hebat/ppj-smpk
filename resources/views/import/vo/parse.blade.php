<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="panel panel-default" style="width:100%">
            <div class="panel-body table-responsive">
                <form id="process_form" class="form-horizontal" method="POST" action="{{ route('importvo.process') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file->id }}">
                    <input type="text" name="sst_id" id="sst_id" hidden>
                    <input type="text" name="vo_id" id="vo_id" hidden>
                    {{--  <input type="text" name="variation_order_type_id" id="variation_order_type_id" hidden>  --}}
                    <input type="text" name="variation_order_type_id" id="variation_order_type_id" value="2" hidden>
                    <table class="table table-responsive">
                        @if (isset($csv_header_fields))
                        <tr>
                            @foreach ($csv_header_fields as $csv_header_field)
                                <th>{{ $csv_header_field }}</th>
                            @endforeach
                        </tr>
                        @endif
                        @foreach ($csv_data as $row)
                            <tr>
                            @foreach ($row as $key => $value)
                                <td>{{ $value }}</td>
                            @endforeach
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-primary">Muat Naik</button>
                    <script type="text/javascript">
                        window.onload = function () {
                            document.getElementById("sst_id").value = window.parent.document.getElementById('sst_id').value;;
                            document.getElementById("vo_id").value = window.parent.document.getElementById('vo_id').value;
                            //document.getElementById("variation_order_type_id").value = window.parent.document.getElementById('variation_order_type_id').value;
                            //document.getElementById("variation_order_category_id").value = window.parent.document.getElementById('variation_order_category_id').value;
                        }
                    </script>
                </form>
            </div>
        </div>
    </body>
</html>
