{{--  @extends('layouts.app')  --}}
@extends('layouts.public.app')
<?php
//echo $_SERVER['REQUEST_URI'];
        
//  if ($request->session()->has('users')) {
//    //
//      echo 'ada users';
//  } 

function GoToNow ($url){
    echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
}

$user = Auth::user();

if(isset($user)){
    
    //logout
    //echo 'should logout';
    GoToNow('mylogout');
}



?>
@section('content')
    {{--  <div class="container">  --}}
    <div>
        <div class="row">
            <div class="col-lg-9 col-md-8 col-xs-8" style="padding:0px 0px;">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('images/picture-4.jpg') }}" alt="First slide">
                            <div class="carousel-caption animated slideInLeft">
                                <h2 style="font-size: 30px;">
                                    Informasi Setempat Perolehan Sebutharga/Tender
                                </h2>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/picture-2.jpg') }}" alt="Second slide">
                            <div class="carousel-caption animated slideInLeft">
                                <h2 style="font-size: 30px;">
                                    Informasi Setempat Perolehan Sebutharga/Tender
                                </h2>                                
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/picture-3.jpg') }}" alt="Third slide">
                            <div class="carousel-caption animated slideInLeft">
                                <h2 style="font-size: 30px;">
                                    Informasi Setempat Perolehan Sebutharga/Tender
                                </h2>                                
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="col-lg-3 col-md-4 col-xs-4" style="padding:0px 0px;">                
                <form class="card" method="POST" style="margin-bottom: 0.5rem" action="{{ route('login') }}">
                    <br/>
                    @csrf
                    <div class="card-body p-6">
                        <div class="card-title text-center" style="font-size:14px; font-family:Verdana; font-weight:bold;">{{ __('Sistem Maklumat Pengurusan Kontrak (COINS)') }}</div>
                        <hr>
                        <div class="form-group">
                            <label class="form-label">{{ __('ID Pengguna') }}&nbsp;<span style="color:red">*</span></label>
                            <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Masukkan ID Pengguna" required
                                oninvalid="this.setCustomValidity('Sila Masukkan ID Pengguna')" oninput="setCustomValidity('')">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                             <label class="form-label">Kata Laluan&nbsp;<span style="color:red">*</span></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Kata Laluan" required 
                                oninvalid="this.setCustomValidity('Sila Masukkan Katalaluan')" oninput="setCustomValidity('')">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary btn-block">{{ __('Log Masuk') }}</button>
                        </div>
                    </div>
                    <br/>
                </form>
                <table style="padding:7px 7px; text-align:center;" width="100%">
                    <tr>
                        <td style="font-size:11px; text-align:center; padding:10px 10px;">
                            <span>
                                Best viewed using latest Google Chrome version with resolution of 1280 x 768 pixels
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

            {{--  <div class="col-lg-4 col-md-4 col-xs-12">
                <form class="card" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="card-body p-6">
                        <div class="card-title text-center" style="font-size:14px; font-family:Verdana; font-weight:bold;">{{ __('Sistem Maklumat Pengurusan Kontrak') }}</div>
                        <hr>
                        <div class="form-group">
                            <label class="form-label">{{ __('ID Pengguna') }}&nbsp;<span style="color:red">*</span></label>
                            <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Masukkan ID Pengguna" required
                                oninvalid="this.setCustomValidity('Sila Masukkan ID Pengguna')" oninput="setCustomValidity('')">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-label">
                                Kata Laluan &nbsp;<span style="color:red">*</span>
                                <a href="{{ route('password.request') }}" class="float-right small">{{ __('Lupa Kata Laluan') }}</a>
                            </label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Kata Laluan" required 
                                oninvalid="this.setCustomValidity('Sila Masukkan Katalaluan')" oninput="setCustomValidity('')">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }} />
                                <span class="custom-control-label">{{ __('Ingat saya') }}</span>
                            </label>
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary btn-block">{{ __('Log Masuk') }}</button>
                        </div>
                    </div>
                </form>
            </div>  --}}
        </div>
        {{--  <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                        <form class="card" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="card-body p-6">
                                <div class="card-title text-center" style="font-size:14px; font-family:Verdana; font-weight:bold;">{{ __('Sistem Pengurusan Kontrak') }}</div>
                                <hr>
                                <div class="form-group">
                                    <label class="form-label">{{ __('ID Pengguna') }}&nbsp;<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Masukkan ID Pengguna" required
                                        oninvalid="this.setCustomValidity('Sila Masukkan ID Pengguna')" oninput="setCustomValidity('')">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label">
                                        Kata Laluan &nbsp;<span style="color:red">*</span>
                                        <a href="{{ route('password.request') }}" class="float-right small">{{ __('Lupa Kata Laluan') }}</a>
                                    </label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Kata Laluan" required 
                                        oninvalid="this.setCustomValidity('Sila Masukkan Katalaluan')" oninput="setCustomValidity('')">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }} />
                                        <span class="custom-control-label">{{ __('Ingat saya') }}</span>
                                    </label>
                                </div>
                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">{{ __('Log Masuk') }}</button>
                                </div>
                            </div>
                        </form>
            </div>
        </div>  --}}
    </div>
@endsection
