<div class="row">
    <div class="col-12">
        <h5>Syarikat Yang Hadir Taklimat</h5>
        <div class="table-responsive table-bordered">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="5%">Bil.</th>
                        <th width="20%">Nama Syarikat</th>
                        <th width="45%">Alamat</th>
                        <th width="15%">Nama</th>
                        <th width="10%">No Kad Pengenalan</th>
                        <th width="5%">Wakil/Pemilik</th>
                        <th width="5%">Kehadiran</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($acquisition) && !empty($acquisition->briefings) && $acquisition->briefings->count() > 0)
                        @foreach($acquisition->briefings as $b => $brief)
                            <tr>
                                <td>{{ $b+1 }}</td>
                                <td>{{ $brief->company->company_name }}</td>
                                <td>{{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->primary:',' }}
                                    {{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->secondary:',' }} 
                                    {{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->postcode:',' }} 
                                    {{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->state:'' }}
                                </td>
                                <td>{{ ($brief->attendant_status == '1')? $brief->owner->name:$brief->agent->name }}</td>
                                <td>{{ ($brief->attendant_status == '1')? $brief->owner->ic_number:$brief->agent->ic_number }}</td>
                                <td>{{ ($brief->attendant_status == 1)? "Pemilik":"Wakil" }}</td>
                                <td>{{ ($brief->attendant_status == 3)? "Tidak Hadir":"Hadir" }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
    </div>
</div>
