@extends('layouts.admin')

@push('styles')
	<style>

	</style>
@endpush

@push('scripts')
	<script>
            
        jQuery(document).ready(function($) {
            @if($brief->special_permission_status == '1')
                $('#special_permission_status').attr('checked',true);
                $('#special_permission_status_yes').toggle();
                $('#permission_remark').val('{{ $brief->permission_remark }}');
            @endif
            $('#attendant_status').val('{{ $brief->attendant_status }}');
            @if($brief->attendant_status == '1')
                $('#attendance_status_company_owner').show();
            @elseif($brief->attendant_status == '2')
                $('#attendance_status_company_agent').show();
                $('#company_agent_id').val('{{ $brief->company_owner_id }}');
            @endif
            @if($brief->attendant_status == '3')
                $('#status').val('3');
                $('#status_no').show();
                $('#status_remark').val('{{ $brief->status_remark }}');
            @else
                $('#status').val('1');
                $('#status_no').hide();
            @endif
            
            $('#special_permission_status').click(function(){
                if ($('#special_permission_status').val('on')) 
                {
                    $('#special_permission_status_yes').toggle();
                }
                else 
                {
                    $('#special_permission_status_yes').toggle();
                }
            });
            
            $('#attendant_status').on('change',function(){
                if ($('#attendant_status').val() == '1') 
                {
                    $('#attendance_status_company_agent').hide();
                    $('#attendance_status_company_owner').show();
                }
                else if ($('#attendant_status').val() == '2')  
                {
                    $('#attendance_status_company_agent').show();
                    $('#attendance_status_company_owner').hide();
                }
                else{
                    $('#attendance_status_company_agent').hide();
                    $('#attendance_status_company_owner').hide();
                }
            });
                
            $('#new_agent').click(function(){

                if ($('#new_agent').val('on')) 
                {
                    $('#new_company_agent').toggle();
                    $('#company_agent_name').prop('required',true);
                    $('#company_agent_ic').prop('required',true);
                    $('#company_agent_phone').prop('required',true);
                }
                else 
                {
                    $('#new_company_agent').toggle();
                }
            });
            
            $('#status').on('change',function(){
                if ($('#status').val() == '3') 
                {
                    $('#status_no').show();
                }
                else{
                    $('#status_no').hide();
                }
            });
        });
	</script>
@endpush

@php
    $acqCategory = "";

    if($acquisition->category->id == 1 || $acquisition->category->id == 2) {
        $acqCategory = "Sebut Harga";
    }elseif($acquisition->category->id == 3 || $acquisition->category->id == 4) {
        $acqCategory = "Tender";
    }
@endphp

@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        
        <span class="font-weight-bold">Tajuk : </span>{{ $acquisition->title }}
        <br>
        <span class="font-weight-bold" >No. {{$acqCategory}} : </span>{!! $acquisition->reference !!}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh & Masa Taklimat : </span>{{ $acquisition->briefing_at->format(config('datetime.datetimepicker.date_time')) }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempat Taklimat : </span>{!! $acquisition->briefing_location !!}
        <br>
        <span class="font-weight-bold">Syarat Kelayakan : </span>
            @if(!empty($acquisition) && !empty($acquisition->approval) && ($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3))
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->isNotEmpty())
                    @php $cnt1 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->mofQualifications as $cnt1=>$cetak5)                    
                        @if($cetak5->code->type == 'category')
                        @php $cnt1 = $cnt1 + 1; @endphp
                            {{$cetak5->code->name}}
                            @if($cetak5->status == 1)
                                <b>DAN</b>
                            @elseif($cetak5->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt2 = 0; @endphp
                    @foreach($acquisition->approval->mofQualifications as $cnt2=>$cetak6)
                        @if($cetak6->code->type == 'khusus')
                        @php $cnt2 = $cnt2 + 1; @endphp
                            {{$cetak6->code->code}}
                            @if($cetak6->status == 1)
                                <b>DAN</b>
                            @elseif($cetak6->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                        
                        @endif
                    @endforeach   
                    <br />
                @endif
            @else
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->isNotEmpty())
                    @php $cnt3 = 0; @endphp
                    Gred : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt3=>$cetak)
                        @if($cetak->code->type == 'grade')
                        @php $cnt3 = $cnt3 + 1; @endphp
                            {{$cetak->code->code}} 
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif                    
                    @endforeach 
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    @php $cnt4 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt4=>$cetak)                    
                        @if($cetak->code->type == 'category')
                        @php $cnt4 = $cnt4 + 1; @endphp
                            {{$cetak->code->name}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt5 = 0; @endphp
                    @foreach($acquisition->approval->cidbQualifications as $cnt5=>$cetak)
                        @if($cetak->code->type == 'khusus')
                        @php $cnt5 = $cnt5 + 1; @endphp
                            {{$cetak->code->code}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                      
                        @endif
                    @endforeach                                 
                    <br />
                @endif
            @endif
    @endslot
@endcomponent
    <form method="POST" action="{{ route('briefing.update',[$brief->hashslug]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type="text" name="acquisition_id" value="{{ $acquisition->id }}" hidden>
        <input type="text" name="acq_hash" value="{{ $acquisition->hashslug }}" hidden>
        <input type="text" name="hashslug" value="{{ $brief->hashslug }}" hidden>
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @component('components.card')
                    @slot('card_body')
                        <div class="row">
                            <div class="col-12">
                                Syarikat
                                <select id="company_id" name="company_id" class="select2 form-control ">
                                    <option value="{{ $brief->company_id }}" selected>{{ $brief->company->company_name }}</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div id="company_details" class="" >
                            <h5>Maklumat Syarikat</h5>
                            <div class="row">
                                @if(!empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->count() > 0)
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">Gred</div>
                                        <div id="grade">{!! $gred !!}</div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">Kategori</div>
                                        <div id="category">{!! $cat !!}</div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">Khusus</div>
                                        <div id="special">{!! $khusus !!}</div>
                                    </div>
                                </div>
                                @elseif(!empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->count() > 0)
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">Kategori</div>
                                        <div id="category">{!! $catm !!}</div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">Khusus</div>
                                        <div id="special">{!! $khususm !!}</div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">No. Pendaftaran SSM</div>
                                        <div id="ssm">{{ $brief->company->ssm_no ?? '' }}</div>
                                    </div>
                                </div>
                                @if(!empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->count() > 0)
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">No. Pendaftaran CIDB</div>
                                        <div id="cidb">{{ $brief->company->cidb_no ?? '' }}</div>
                                    </div>
                                </div>
                                @elseif(!empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->count() > 0)
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">No. Pendaftaran MOF</div>
                                        <div id="mof">{{ $brief->company->mof_no ?? '' }}</div>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-4 col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">No. Tel (Pejabat)</div>
                                        <div id="tel_pejabat">{{ $brief->company->phones[0]->phone_number ?? '' }}</div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">No. Tel (Bimbit)</div>
                                        <div id="tel_bimbit">{{ $brief->company->phones[1]->phone_number ?? '' }}</div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <div class="text-muted">No. Tel (Faks)</div>
                                        <div id="tel_faks">{{ $brief->company->phones[2]->phone_number ?? '' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                @include('components.forms.checkbox', [
                                    'label' => __('Status Kebenaran Khas'),
                                    'id' => 'special_permission_status',
                                    'name' => 'special_permission_status',
                                    'checked' => '',
                                ]) 
                            </div>
                        </div>
                        <br>
                        <div id="special_permission_status_yes" style="display: none">
                            <div class="row">
                                <div class="col-12">
                                    @include('components.forms.input', [
                                        'input_label' => __('Sila nyatakan'),
                                        'id' => 'permission_remark',
                                        'name' => 'permission_remark',
                                    ]) 
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                Status Hadir Taklimat
                                <select id="attendant_status" name="attendant_status" class="select2 form-control ">
                                    <option value="">Sila Pilih</option>
                                    <option value="1">Pemilik</option>
                                    <option value="2">Wakil</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div id="attendance_status_company_owner" style="display: none">
                            <div class="row">
                                <div class="col-12">
                                    Senarai Pemilik
                                    <select id="company_owner_id" name="company_owner_id" class="select2 form-control ">
                                        @if($owner !== null)
                                        @foreach($owner as $own)
                                            <option value="{{ $own->id }}">{{ $own->name }}&nbsp;&nbsp;&nbsp;{{ $own->ic_number }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div id="attendance_status_company_agent" style="display: none">
                            <div class="row">
                                <div class="col-12">
                                    Senarai Wakil (Lama)
                                    <select id="company_agent_id" name="company_agent_id" class="select2 form-control ">
                                        @if($agent !== null)
                                        @foreach($agent as $agn)
                                            <option value="{{ $agn->id }}">{{ $agn->name }}&nbsp;&nbsp;&nbsp;{{ $agn->ic_number }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-12">
                                    @include('components.forms.checkbox', [
                                        'label' => __('Wakil Baru'),
                                        'id' => 'new_agent',
                                        'name' => 'new_agent',
                                        'checked' => '',
                                    ]) 
                                </div>
                            </div>
                            <div id="new_company_agent" style="display: none">
                                <div class="row">
                                    <div class="col-12">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Wakil (Baru)'),
                                            'id' => 'company_agent_name',
                                            'name' => 'company_agent_name',
                                        ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        @include('components.forms.input', [
                                            'input_label' => __('No. KP Wakil (Baru)'),
                                            'id' => 'company_agent_ic',
                                            'name' => 'company_agent_ic',
                                            'maxlength' => '12',
                                        ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        @include('components.forms.input', [
                                            'input_label' => __('No. Telefon Wakil (Baru)'),
                                            'id' => 'company_agent_phone',
                                            'name' => 'company_agent_phone',
                                        ])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                Status
                                <select id="status" name="status" class="select2 form-control ">
                                    <option value="">Sila Pilih</option>
                                    <option value="1">Hadir</option>
                                    <option value="3">Tidak Hadir</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div id="status_no" style="display: none">
                            <div class="row">
                                <div class="col-12">
                                    @include('components.forms.input', [
                                        'input_label' => __('Sila nyatakan sebab'),
                                        'id' => 'status_remark',
                                        'name' => 'status_remark',
                                    ]) 
                                </div>
                            </div>
                        </div>
                    @endslot
                    @slot('card_footer')
                        <div class="btn-group float-right">
                            <a href="{{ route('briefing.listing', $acquisition->hashslug) }}"
                               class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                            </a>
                            <button type="submit" class="btn btn-primary">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </div>
        </div>
    </form>
@endsection