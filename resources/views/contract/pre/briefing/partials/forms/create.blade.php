
<div class="row">
    <div class="col-12">
        @include('components.forms.select', [
            'input_label' => __('Syarikat'),
            'options' => companies()->pluck('company_name', 'hashslug'),
            'id' => 'company_id',
            'name' => 'company_id',
        ])
    </div>
</div>
<br>
<div id="company_details" class="" style="display: none">
    <h5>Maklumat Syarikat</h5>
    <div class="row">
        <div id="gred" class="col-md-4 col-lg-4" style="display: none">
            <div class="form-group">
                <div class="text-muted">Gred</div>
                <div id="grade"></div>
            </div>
        </div>
        <div id="kategori" class="col-md-4 col-lg-4" style="display: none">
            <div class="form-group">
                <div class="text-muted">Kategori</div>
                <div id="category"></div>
            </div>
        </div>
        <div id="khusus" class="col-md-4 col-lg-4" style="display: none">
            <div class="form-group">
                <div class="text-muted">Khusus</div>
                <div id="special"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <div class="text-muted">No. Pendaftaran SSM</div>
                <div id="ssm"></div>
            </div>
        </div>
        <div id="cidb_name" class="col-md-4 col-lg-4" style="display: none">
            <div class="form-group">
                <div class="text-muted">No. Pendaftaran CIDB</div>
                <div id="cidb"></div>
            </div>
        </div>
        <div id="mof_name" class="col-md-4 col-lg-4" style="display: none">
            <div class="form-group">
                <div class="text-muted">No. Pendaftaran MOF</div>
                <div id="mof"></div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <div class="text-muted">No. Tel (Pejabat)</div>
                <div id="tel_pejabat"></div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <div class="text-muted">No. Tel (Bimbit)</div>
                <div id="tel_bimbit"></div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <div class="text-muted">No. Tel (Faks)</div>
                <div id="tel_faks"></div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-12">
        @include('components.forms.checkbox', [
            'label' => __('Status Kebenaran Khas'),
            'id' => 'special_permission_status',
            'name' => 'special_permission_status',
            'checked' => '',
        ]) 
    </div>
</div>
<br>
<div id="special_permission_status_yes" style="display: none">
    <div class="row">
        <div class="col-12">
            @include('components.forms.input', [
                'input_label' => __('Sila nyatakan'),
                'id' => 'permission_remark',
                'name' => 'permission_remark',
            ]) 
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-12">
        Status Hadir Taklimat
        <select id="attendant_status" name="attendant_status" class="select2 form-control ">
            <option value="">Sila Pilih</option>
            <option value="1">Pemilik</option>
            <option value="2">Wakil</option>
        </select>
    </div>
</div>
<br>
<div id="attendance_status_company_owner" style="display: none">
    <div class="row">
        <div class="col-12">
            @include('components.forms.select', [
                'input_label' => __('Senarai Pemilik'),
                'options' => '',
                'id' => 'company_owner_id',
                'name' => 'company_owner_id',
            ])
        </div>
    </div>
</div>
<br>
<div id="attendance_status_company_agent" style="display: none">
    <div class="row">
        <div class="col-12">
            @include('components.forms.select', [
                'input_label' => __('Senarai Wakil (Lama)'),
                'options' => '',
                'id' => 'company_agent_id',
                'name' => 'company_agent_id',
            ])
        </div>
        <div class="col-12">
            @include('components.forms.checkbox', [
                'label' => __('Wakil Baru'),
                'id' => 'new_agent',
                'name' => 'new_agent',
                'checked' => '',
            ]) 
        </div>
    </div>
    <div id="new_company_agent" style="display: none">
        <div class="row">
            <div class="col-12">
                @include('components.forms.input', [
                    'input_label' => __('Nama Wakil (Baru)'),
                    'id' => 'company_agent_name',
                    'name' => 'company_agent_name',
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @include('components.forms.input', [
                    'input_label' => __('No. KP Wakil (Baru)'),
                    'id' => 'company_agent_ic',
                    'name' => 'company_agent_ic',
                    'maxlength' => '12',
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @include('components.forms.input', [
                    'input_label' => __('No. Telefon Wakil (Baru)'),
                    'id' => 'company_agent_phone',
                    'name' => 'company_agent_phone',
                ])
            </div>
        </div>
    </div>
</div>
