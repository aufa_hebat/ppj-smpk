@extends('layouts.admin')

@push('styles')
	<style>

	</style>
@endpush

@push('scripts')
<script>
    var routes = {
        show:'api.company.profile.show',
        store: 'api.company.profile.store',
        update: 'api.company.profile.update',
        destroy: 'api.company.profile.destroy',
    };

    jQuery(document).ready(function($) {
        @if(is_object($briefing))
            @foreach($briefing as $brief)
                $("#company_id option[value='{{ $brief->company->hashslug }}']").remove();
            @endforeach
        @endif
        $(document).on('change', '#company_id', function(event) {
            event.preventDefault();
            var id = $( "#company_id" ).val();
            axios.get(route(routes.show, id)).then(response => {
                var grade_string = '';
                var category_string = '';var category_stringm = '';
                var special_string = '';var special_stringm = '';

                $( "#ssm" ).html(response.data.data.ssm_no);
                @if(!empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->count() > 0)
                    $("#gred").show();
                    var grade = response.data.data.cidbgrade;
                    for( x = 0; x < grade.length; x++ )
                    {
                        grade_string += '<div>'+grade[x].grades+'</div>';
                    }
                    $( "#grade" ).html(grade_string);
                     
                    $("#category").show();
                    var category = response.data.data.cidbcategory;
                    for( x = 0; x < category.length; x++ )
                    {
                        category_string += '<div>'+category[x].categories+'</div>';
                        var special = category[x]; console.log(category);
                        for( y = 0; y < special.cidbspecial.length; y++ )
                        {
                                special_string += '<div>'+special.cidbspecial[y].khusus+'</div>';
                        }
                    }
                    $( "#category" ).html(category_string);

                    $("#khusus").show();
                    $( "#special" ).html(special_string);
                    
                    $("#cidb_name").show();
                    $("#cidb").html(response.data.data.cidb_no);
                    
                @elseif(!empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->count() > 0)
                    $("#kategori").show();
                    
                    var categorym = response.data.data.mofcourse;
                    for( z = 0; z < categorym.length; z++ )
                    {
                        category_stringm += '<div>'+categorym[z].areas+'</div>';
                        var specialm = categorym[z]; 
                        for( p = 0; p < specialm.syarikat_m_o_f_khusus.length; p++ )
                        {
                                special_stringm += '<div>'+specialm.syarikat_m_o_f_khusus[p].khusus+'</div>';
                        }
                    }
                    $( "#category" ).html(category_stringm);
                    
                    $("#khusus").show();
                    $( "#special" ).html(special_stringm);
                    
                    $("#mof_name").show();
                    
                    $( "#mof" ).html(response.data.data.mof_no);
                @endif

                for(a=0; a<response.data.data.phones.length; a++)
                {
                    if(response.data.data.phones[a].phone_type_id == 2)
                    {
                        $( "#tel_bimbit" ).html(response.data.data.phones[a].phone_number);
                    }
                    if(response.data.data.phones[a].phone_type_id == 3)
                    {
                        $( "#tel_pejabat" ).html(response.data.data.phones[a].phone_number);
                    }
                    if(response.data.data.phones[a].phone_type_id == 4)
                    {
                        $( "#tel_faks" ).html(response.data.data.phones[a].phone_number);
                    }
                }

                $("#company_owner_id").empty(); // remove old options
                $.each(response.data.data.owners, function() {
                    $("#company_owner_id").append($("<option></option>")
                        .attr("value", this.id).text(this.name));
                });

                $("#company_agent_id").empty(); // remove old options
                $.each(response.data.data.agents, function() {
                    $("#company_agent_id").append($("<option></option>")
                    .attr("value", this.id).text(this.name));
                });

                $('#company_details').show();
            }).catch(error => console.error(error));
        });

        $('#special_permission_status').click(function(){
            if ($('#special_permission_status').val('on')) 
            {
                $('#special_permission_status_yes').toggle();
            }
            else 
            {
                $('#special_permission_status_yes').toggle();
            }
        });

        $('#attendant_status').on('change',function(){
            if ($('#attendant_status').val() == '1') 
            {
                $('#attendance_status_company_agent').hide();
                $('#attendance_status_company_owner').show();
            }
            else if ($('#attendant_status').val() == '2')  
            {
                $('#attendance_status_company_agent').show();
                $('#attendance_status_company_owner').hide();
            }
        });

        $('#new_agent').click(function(){
            if ($('#new_agent').val('on')) 
            {
                $('#new_company_agent').toggle();
                $('#company_agent_name').prop('required',true);
                $('#company_agent_ic').prop('required',true);
                $('#company_agent_phone').prop('required',true);
            }
            else 
            {
                $('#new_company_agent').toggle();
            }
        });
    });
</script>
@endpush

@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{{ $acquisition->title }}
        <br>
        <span class="font-weight-bold" >No. Sebut Harga : </span>{!! $acquisition->reference !!}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh & Masa Taklimat : </span>{{ $acquisition->briefing_at->format(config('datetime.datetimepicker.date_time')) }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempat Taklimat : </span>{!! $acquisition->briefing_location !!}
        <br>
        <span class="font-weight-bold">Syarat Kelayakan : </span>
            @if(!empty($acquisition) && !empty($acquisition->approval) && ($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3))
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->isNotEmpty())
                    @php $cnt1 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->mofQualifications as $cnt1=>$cetak5)                    
                        @if($cetak5->code->type == 'category')
                        @php $cnt1 = $cnt1 + 1; @endphp
                            {{$cetak5->code->name}}
                            @if($cetak5->status == 1)
                                <b>DAN</b>
                            @elseif($cetak5->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt2 = 0; @endphp
                    @foreach($acquisition->approval->mofQualifications as $cnt2=>$cetak6)
                        @if($cetak6->code->type == 'khusus')
                        @php $cnt2 = $cnt2 + 1; @endphp
                            {{$cetak6->code->code}}
                            @if($cetak6->status == 1)
                                <b>DAN</b>
                            @elseif($cetak6->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                        
                        @endif
                    @endforeach   
                    <br />
                @endif
            @else
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->isNotEmpty())
                    @php $cnt3 = 0; @endphp
                    Gred : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt3=>$cetak)
                        @if($cetak->code->type == 'grade')
                        @php $cnt3 = $cnt3 + 1; @endphp
                            {{$cetak->code->code}} 
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif                    
                    @endforeach 
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    @php $cnt4 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt4=>$cetak)                    
                        @if($cetak->code->type == 'category')
                        @php $cnt4 = $cnt4 + 1; @endphp
                            {{$cetak->code->name}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt5 = 0; @endphp
                    @foreach($acquisition->approval->cidbQualifications as $cnt5=>$cetak)
                        @if($cetak->code->type == 'khusus')
                        @php $cnt5 = $cnt5 + 1; @endphp
                            {{$cetak->code->code}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                      
                        @endif
                    @endforeach                                 
                    <br />
                @endif
            @endif
    @endslot
@endcomponent
    <form method="POST" action="{{ route('briefing.store') }}">
        @csrf
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @component('components.card')
                    @slot('card_body')
                        @include('contract.pre.briefing.partials.forms.create')
                    @endslot
                    @slot('card_footer')
                        <input type="hidden" name="acquisition_id" value="{{ $acquisition->id }}">
                        <input type="hidden" name="acq_hash" value="{{ $acquisition->hashslug }}">
                        <div class="btn-group float-right">
                            <a href="{{ route('briefing.listing', $acquisition->hashslug) }}"
                               class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                            </a>
                            <button type="submit" class="btn btn-primary">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                            </button>
                        </div>
                    @endslot
                @endcomponent
            </div>
        </div>
    </form>
@endsection