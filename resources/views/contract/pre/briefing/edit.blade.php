@extends('layouts.admin')

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
	<script>

        jQuery(document).ready(function($) {
            $('#dataTable').DataTable();
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = {{ $acquisition->id }};
                var route_name = 'api.contract.briefing.update';
                var form_id = 'briefing_form';
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('', response.data.message, 'success');
                    location.reload();
                });
            });
        });
	</script>
@endpush

@php
    $acqCategory = "";

    if($acquisition->category->id == 1 || $acquisition->category->id == 2) {
        $acqCategory = "Sebut Harga";
    }elseif($acquisition->category->id == 3 || $acquisition->category->id == 4) {
        $acqCategory = "Tender";
    }
@endphp

@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        
        <span class="font-weight-bold">Tajuk : </span>{{ $acquisition->title }}
        <br>
        <span class="font-weight-bold" >No. {{$acqCategory}} : </span>{!! $acquisition->reference !!}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh & Masa Taklimat : </span>{{ $acquisition->briefing_at->format(config('datetime.datetimepicker.date_time')) }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempat Taklimat : </span>{!! $acquisition->briefing_location !!}
        <br>
        <span class="font-weight-bold">Syarat Kelayakan : </span>
            @if(!empty($acquisition) && !empty($acquisition->approval) && ($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3))
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->isNotEmpty())
                    @php $cnt1 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->mofQualifications as $cnt1=>$cetak5)                    
                        @if($cetak5->code->type == 'category')
                        @php $cnt1 = $cnt1 + 1; @endphp
                            {{$cetak5->code->name}}
                            @if($cetak5->status == 1)
                                <b>DAN</b>
                            @elseif($cetak5->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt2 = 0; @endphp
                    @foreach($acquisition->approval->mofQualifications as $cnt2=>$cetak6)
                        @if($cetak6->code->type == 'khusus')
                        @php $cnt2 = $cnt2 + 1; @endphp
                            {{$cetak6->code->code}}
                            @if($cetak6->status == 1)
                                <b>DAN</b>
                            @elseif($cetak6->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                        
                        @endif
                    @endforeach   
                    <br />
                @endif
            @else
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->isNotEmpty())
                    @php $cnt3 = 0; @endphp
                    Gred : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt3=>$cetak)
                        @if($cetak->code->type == 'grade')
                        @php $cnt3 = $cnt3 + 1; @endphp
                            {{$cetak->code->code}} 
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif                    
                    @endforeach 
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    @php $cnt4 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt4=>$cetak)                    
                        @if($cetak->code->type == 'category')
                        @php $cnt4 = $cnt4 + 1; @endphp
                            {{$cetak->code->name}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt5 = 0; @endphp
                    @foreach($acquisition->approval->cidbQualifications as $cnt5=>$cetak)
                        @if($cetak->code->type == 'khusus')
                        @php $cnt5 = $cnt5 + 1; @endphp
                            {{$cetak->code->code}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                      
                        @endif
                    @endforeach                                 
                    <br />
                @endif
            @endif
    @endslot
@endcomponent
<div class="row">
    
    @component('components.card', ['card_classes' => 'col-12'])
        @slot('card_body')
            @if((empty($acquisition->sst) || $acquisition->sst->count() < 1) && $show_add == true)
            <a href="{{route('briefing.create',['id'=>$acquisition->hashslug])}}" class="btn btn-primary border-primary" style="float:right"><i class="fe fe-plus"></i>Tambah Kehadiran</a>
            @endif
            <br><br>
            <h5>Senarai Syarikat yang Hadir Taklimat: </h5>
            <div class="table-responsive table-bordered">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th width="5%">Bil.</th>
                        <th width="20%">Nama Syarikat</th>
                        <th width="45%">Alamat</th>
                        <th width="15%">Nama</th>
                        <th width="10%">No Kad Pengenalan</th>
                        <th width="5%">Wakil/Pemilik</th>
                        <th width="5%">Kehadiran</th>
                        <th width="5%">Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($acquisition) && !empty($acquisition->briefings) && $acquisition->briefings->count() > 0)
                        @foreach($acquisition->briefings as $b => $brief)
                            <tr>
                                <td>{{ $b+1 }}</td>
                                <td>{{ $brief->company->company_name }}</td>
                                <td>{{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->primary:',' }}
                                    {{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->secondary:',' }} 
                                    {{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->postcode:',' }} 
                                    {{ (!empty($brief->company) && !empty($brief->company->addresses)) ? $brief->company->addresses[0]->state:'' }}
                                </td>
                                <td>{{ ($brief->attendant_status == '1')? $brief->owner->name:$brief->agent->name }}</td>
                                <td>{{ ($brief->attendant_status == '1')? $brief->owner->ic_number:$brief->agent->ic_number }}</td>
                                <td>@if($brief->attendant_status == 1)
                                        Pemilik
                                    @elseif($brief->attendant_status == 2)
                                        Wakil
                                    @endif
                                </td>
                                <td>{{ ($brief->attendant_status == 3)? "Tidak Hadir":"Hadir" }}</td>
                                <td>
                                    <div class="text-center">
                                        @if((empty($acquisition->sst) || $acquisition->sst->count() < 1) && $show_add == true)
                                        <a class="dropdown-item edit-action-btn" style="cursor: pointer;" href="{{ route('briefing.edit',[$brief->hashslug]) }}">
                                            <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                        </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        @endslot
        @slot('card_footer')
                <input type="hidden" name="acquisition_id" value="{{ $acquisition->id }}">
                <div class="btn-group float-right">
                    <a href="{{ route('briefing.index') }}"
                       class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                    </a>
                    <a href="{{ route('brief_attendance',['hashslug' => $acquisition->hashslug]) }}" target="_blank"
                            class="btn btn-success border-success">
                            @icon('fe fe-printer'){{ __(' Cetak') }}
                    </a>
                </div>
        @endslot
    @endcomponent
</div>
    
@endsection