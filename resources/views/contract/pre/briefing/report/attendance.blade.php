<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
        .toprightline {
            border-top: hidden;
            border-right:hidden;
        }
        .topleftline {
            border-top: hidden;
            border-left:hidden;
        }
        .rightline {
            border-right:hidden;
        }
        .leftline {
            border-left:hidden;
        }

        .pagenum:before {
            content: counter(page);                
        }

        footer {
            position: fixed; 
            bottom: -5px; 
            left: 0px; 
            right: 0px;
            height: 30px; 
            color: black;
            text-align: center;
            line-height: 35px;	
        }
        
</style>
@if(!empty($acquisition))
    <div class="title" style="text-align:center; font-weight: bold">
        <img style="width: 100px; height: 100px;" src="{{ logo($print) }}"><br><br><br>
        <label style="text-transform: uppercase;">SENARAI KEHADIRAN BAGI TAKLIMAT {{ $acquisition->approval->type->name }} DAN LAWATAN TAPAK</label><br><br>
        <label style="text-transform: uppercase;">TAJUK 
            @if(strstr( $acquisition->category->name, "Tender"))
            TENDER
            @else
            SEBUT HARGA
            @endif
        </label><br><br>
        <label style="text-transform: uppercase;">{!! $acquisition->title !!}</label><br><br>
        <label >NO. 
            @if(strstr( $acquisition->category->name, "Tender"))
            TENDER
            @else
            SEBUT HARGA
            @endif
            : {{ $acquisition->reference }}</label>
    </div>
    <footer>
        <div class="pagenum-container">Page <span class="pagenum"></span></div>
    </footer>
    <br/><br/>
    <div class="content" style="text-align:center;">
        <table border="1" style="width: 100%;padding: 4px; border-collapse: collapse;" class="content">
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">TARIKH</th>
                <td colspan="3" style="width: 80%;padding: 4px;">{{ (!empty($acquisition->briefing_at))? date('d/m/Y', strtotime($acquisition->briefing_at)):null }}</td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">MASA</th>
                <td colspan="3" style="width: 80%;padding: 4px;">
                    @if(!empty($acquisition->briefing_at))
                        {{ date('g:i', strtotime($acquisition->briefing_at)) }}
                        {{ (date('A', strtotime($acquisition->briefing_at)) == "AM")? "PAGI":"PETANG" }}
                    @endif
                </td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">TEMPAT TAKLIMAT </th>
                <td colspan="3" style="width: 80%;padding: 4px;">{{ $acquisition->briefing_location }}</td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">LOKASI LAWATAN TAPAK </th>
                <td colspan="3" style="width: 80%;padding: 4px;" valign="top">{{ $acquisition->visit_location }}</td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">PEGAWAI PELAKSANA 1 </th>
                <td style="width: 40%;padding: 4px;">@if(!empty($acquisition->approval->user)){{ $acquisition->approval->user->name }}@endif</td>
                <th style="width: 20%;font-weight: bold;padding: 4px;">SAMB. TEL.</b></th>
                <td style="width: 20%;padding: 4px;">@if(!empty($acquisition->approval->user)){{ $acquisition->approval->user->phone_no }}@endif</td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">PEGAWAI PELAKSANA 2 </th>
                <td style="width: 40%;padding: 4px;">@if(!empty($acquisition->approval->user->supervisor)){{ $acquisition->approval->user->supervisor->name }}@endif</td>
                <th style="width: 20%;font-weight: bold;padding: 4px;">SAMB. TEL. </th>
                <td style="width: 20%;padding: 4px;">@if(!empty($acquisition->approval->user->supervisor)){{ $acquisition->approval->user->supervisor->phone_no }}@endif</td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">PEGAWAI BPUB 1 </th>
                <td style="width: 40%;padding: 4px;">{{ $acquisition->reviews->where('status','S4')->first()->task->name ?? '' }}</td>
                <th style="width: 20%;font-weight: bold;padding: 4px;">SAMB. TEL.</th>
                <td style="width: 20%;padding: 4px;">{{ $acquisition->reviews->where('status','S4')->first()->task->phone_no ?? '' }}</td>
            </tr>
            <tr>
                <th style="width: 20%;font-weight: bold;padding: 4px;">PEGAWAI BPUB 2 </th>
                <td style="width: 40%;padding: 4px;">{{ $acquisition->reviews->where('status','S4')->first()->task->supervisor->name ?? '' }}</td>
                <th style="width: 20%;font-weight: bold;padding: 4px;">SAMB. TEL. </th>
                <td style="width: 20%;padding: 4px;">{{ $acquisition->reviews->where('status','S4')->first()->task->supervisor->phone_no ?? '' }}</td>
            </tr>            
        </table>
    </div>
    <div class="breakNow" />
    <div>
        <table border="1" style="width: 100%;padding: 4px; border-collapse: collapse;" class="content">
            <tr>
                <th colspan="2" style="padding:4px">TAJUK PEROLEHAN </th>
                <td style="text-transform: uppercase;;padding:4px" colspan="4">{!! $acquisition->title !!}</td>
            </tr>
            <tr>
                <th colspan="2" style="text-transform: uppercase;padding:4px" >
                    @if(strstr( $acquisition->category->name, "Tender"))
                        TENDER
                    @else
                        SEBUT HARGA
                    @endif                        
                </th>
                <td colspan="4" style="padding:4px">{{ $acquisition->reference }}</td>
            </tr>
            <tr>
                <th rowspan="2" style="width: 1%;padding:4px" align="center">BIL</th>
                <th rowspan="2" style="width: 20%;padding:4px" align="center">NAMA</th>
                <th rowspan="2" style="width: 35%;padding:4px" align="center">NAMA & ALAMAT SYARIKAT</th>
                <th rowspan="2" style="width: 20%;padding:4px" align="center">NO TELEFON / FAKS</th>
                <th colspan="2" style="width: 24%;padding:4px" align="center">TANDATANGAN</th>
            </tr>
            <tr>
                <th style="width: 20%" align="center">TAKLIMAT</th>
                <th style="width: 22%" align="center">LAWATAN TAPAK</th>
            </tr>
            
            @if(!empty($acquisition['briefings']))
                @php $cnt = 0; @endphp
                @foreach($acquisition['briefings'] as $bil => $briefing)
                    
                    @if($briefing->attendant_status != 3)
                        @php $cnt++; @endphp

                        <tr>
                            <td align="center" style="padding:4px">{{ $cnt }}</td>
                            <td style="padding:4px">
                                @if($briefing->attendant_status == '1')
                                    @if(!empty($briefing->owner))
                                        {{ $briefing->owner->name }}
                                        (Pemilik)
                                    @endif
                                @elseif ($briefing->attendant_status == '2')
                                    @if(!empty($briefing->owner))
                                        {{ $briefing->agent->name }}
                                        (Wakil)
                                    @endif
                                @endif
                            </td>
                            <td style="padding:4px">{{ $briefing->company->company_name }}<br>
                                @if(!empty($briefing->company->addresses[0]->primary))
                                    {{ $briefing->company->addresses[0]->primary }}
                                @endif<br>
                                @if(!empty($briefing->company->addresses[0]->secondary))
                                    {{ $briefing->company->addresses[0]->secondary }}
                                @endif<br>
                                @if(!empty($briefing->company->phones[0]))
                                    {{'Telefon: '.$briefing->company->phones[0]->phone_number }}
                                @endif<br>
                                @if(!empty($briefing->company->phones[2]->phone_number))
                                    {{'Faks: '.$briefing->company->phones[2]->phone_number }}
                                @endif
                            </td>
                            <td style="padding:4px">No Telefon: 
                                @if($briefing->attendant_status == '1')
                                    @if(!empty($briefing->owner))
                                        @if(!empty($briefing->owner->phones[0]))
                                            {{ $briefing->owner->phones[0]->phone_number }}
                                        @endif
                                    @endif
                                @elseif ($briefing->attendant_status == '2')
                                    @if(!empty($briefing->owner))
                                        {{ $briefing->agent->telephone_num }}
                                    @endif
                                @endif
                                <br/><br/>
                                No Faks:
                                @if($briefing->attendant_status == '1')
                                    @if(!empty($briefing->owner))
                                        @if(!empty($briefing->owner->phones[2]))
                                            {{ $briefing->owner->phones[2]->phone_number }}
                                        @endif
                                    @endif
                                @endif
                            </td>
                            <td></td>
                            <td></td>
                        </tr>                    
                        {{--  <tr>
                            <td style="padding:4px">No Faks:
                                @if($briefing->attendant_status == '1')
                                    @if(!empty($briefing->owner))
                                        @if(!empty($briefing->owner->phones[2]))
                                            {{ $briefing->owner->phones[2]->phone_number }}
                                        @endif
                                    @endif
                                @endif
                            </td>
                        </tr>  --}}
                    @endif
                @endforeach
            @endif
        </table>
    </div>
    <div>
        <table width="100%">
            <tr>
                <td>
                    <table border="1" style="width: 100%;border-collapse: collapse;" class="content">
                        <tr>
                            <th colspan="2" style="width: 50%;padding:4px">TAKLIMAT SEBUT HARGA/TENDER</th>
                            <th colspan="2" style="width: 50%;padding:4px">LAWATAN TAPAK</th>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:4px">Sebanyak ({{ $acquisition['briefings']->where('attendant_status','!=',3)->count() ?? '0' }}) 
                                @if(strstr( $acquisition->category->name, "Tender"))
                                    Petender
                                @elseif(strstr( $acquisition->category->name, "Sebut Harga"))
                                    Penyebut Harga
                                @else
                                    Pembeli
                                @endif
                                telah menghadiri taklimat pada
                            </td>
                            <td colspan="2" style="padding:4px">Sebanyak (&nbsp;&nbsp;&nbsp;) 
                                @if(strstr( $acquisition->category->name, "Tender"))
                                    Petender
                                @elseif(strstr( $acquisition->category->name, "Sebut Harga"))
                                    Penyebut Harga
                                @else
                                    Pembeli
                                @endif
                                telah menghadiri Lawatan Tapak pada
                            </td>
                        </tr>
                        <tr>
                            <td class="toprightline" style="width: 25%;padding:4px">Tarikh : 
                                @if($acquisition->briefing_status == 1 && $acquisition->briefing_at != null)
                                    {{ date('d/m/Y', strtotime($acquisition->briefing_at)) }}
                                @endif
                            </td>
                            <td class="topleftline" style="width: 25%;padding:4px">Masa : 
                                @if($acquisition->briefing_status == 1 && $acquisition->briefing_at != null)
                                    {{ date('g:i', strtotime($acquisition->briefing_at)) }}
                                    {{ (date('A', strtotime($acquisition->briefing_at)) == "AM")? "PAGI":"PETANG" }}
                                @endif
                            </td>
                            <td class="toprightline" style="width: 25%;padding:4px">Tarikh : 
                                @if($acquisition->visit_status == 1 && $acquisition->visit_date_time != null)
                                    {{ date('d/m/Y', strtotime($acquisition->visit_date_time)) }}
                                @endif
                            </td>
                            <td class="topleftline" style="width: 25%;padding:4px">Masa : 
                                @if($acquisition->visit_status == 1 && $acquisition->visit_date_time != null)
                                    {{ date('g:i', strtotime($acquisition->visit_date_time)) }}
                                    {{ (date('A', strtotime($acquisition->visit_date_time)) == "AM")? "PAGI":"PETANG" }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="rightline" style="text-align: center">
                                <br><br><br><br>                                
                                _________________________________<br>
                                {{ $acquisition->user->name }}<br>
                                {{ $acquisition->user->position->name }}
                            </td>
                            <td align="right" class="leftline" style="text-align: center">
                                <br><br><br><br>
                                <span style="text-align:center">
                                    _________________________________<br>
                                    {{ $acquisition->reviews->where('status','S3')->pluck('task')->pluck('name')->first() ?? null }}<br>
                                    {{ $acquisition->reviews->where('status','S3')->pluck('task')->pluck('position')->pluck('name')->first() ?? null }}<br>
                                </span>
                            </td>
                            <td colspan="2" align="center" style="text-align: center">
                                <br><br><br><br>
                                _________________________________<br>
                                (<span style="margin-left: 200px">&nbsp;</span>)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
@endif