@extends('layouts.admin')

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.briefing.partials.scripts')
			@component('components.forms.hidden-form', 
				[
					'id' => 'destroy-record-form',
					'action' => route('briefing.destroy', 'dummy')
				])
				@slot('inputs')
					@method('DELETE')
				@endslot
			@endcomponent
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-management',
							'route_name' => 'api.datatable.contract.briefing',
							'columns' => [
								['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'title', 'title' => __('Tajuk Perolehan'), 'defaultContent' => '-'],
								['data' => 'briefing_at', 'title' => __('Tarikh Taklimat'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Rujukan'),__('Tajuk Perolehan'),__('Tarikh Taklimat'), __('table.action')
							],
							'actions' => minify(view('contract.pre.briefing.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection