<html>
        <head>
            <style type="text/css">
                @page {
                    margin: 40px 40px 40px 90px;
                }
                div.breakNow { page-break-inside:avoid; page-break-after:always; }
                .title {
                    font-size: 14px;
                    font-family: "Arial, Helvetica, sans-serif";
                }
    
                .content {
                    font-size: 11.5px;
                    font-family: "Arial, Helvetica, sans-serif";
                }
    
                .content1 {
                    font-size: 11px;
                    font-family: "Arial, Helvetica, sans-serif";
                }
            </style>
        </head>
        <body>
            @php $bilangan = 0; @endphp
            <center>
                <img style="width: 100px; height: 100px;" src="{{ logo($print) }}">
                <br/>
                <span style="text-transform: uppercase; font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;" >Kenyataan Tender</span>
                <br/>
                <span style="text-transform: uppercase; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; ">(Kontraktor Bumiputera Sahaja) </span>
                <br/><br/>
                <span style="text-transform: uppercase; text-align: justify; font-weight:bold;" class="content">{{ $acq->title }}</span>
                <br/><br/>
                <span style="font-weight:bold;" class="content">(NO. TENDER : {{ $acq->reference }})</span>
            </center>
            <hr/>
            <table width="100%" border="0" class="content">
                <tr>
                    <td width="5%" style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td width="95%" style="text-align:justify; padding:5px 5px;">
                        <span style="text-align:justify">
                            Tender adalah dipelawa daripada Kontraktor/Syarikat bumiputera yang mempunyai Sijil Perolehan Kerja Kerajaan (SPKK) 
                            berdaftar dengan Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM) dalam gred, kategori dan pengkhususan 
                            yang berkaitan serta masih dibenarkan membuat tawaran pada masa ini, serta mempunyai pengalaman, keupayaan 
                            teknikal dan kewangan yang mencukupi bagi melaksanakan projek tersebut di atas. Kontraktor/Syarikat yang berminat untuk 
                            menyertai tender ini boleh layari laman <u>https://dcoins.ppj.gov.my</u> bagi tujuan pendaftaran kontraktor/syarikat.
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td style="text-align:justify; padding:5px 5px;">
                        <span style="text-align: justify;">
                            Maklumat Tender serta kelayakan asas Kontraktor/Syarikat adalah seperti berikut:
                        </span>
                    </td>
                </tr>
            </table>
    
            <table width="100%" border="1" style="border-collapse: collapse;padding: 4 4;" class="content1">
                <thead>
                    <tr style="background-color: #d9d9d9; text-align:center">
                        <th style="text-transform: uppercase;" valign="top" width="20%">Syarat Memasuki Tender</th>
                        <th style="text-transform: uppercase;" valign="top" width="20%">Tarikh Dan Tempat Taklimat / Lawatan Tapak</th>
                        <th style="text-transform: uppercase;" valign="top" width="20%">Tarikh Dan Tempat Dokumen Mula Dijual</th>
                        <th style="text-transform: uppercase;" valign="top" width="20%">Harga Dokumen, Bentuk Bayaran &amp; Bayaran Atas Nama</th>
                        <th style="text-transform: uppercase;" valign="top" width="20%">Tarikh, waktu &amp; Tempat Tender DiTutup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td valign="top" style="text-align:center">
                            <span>
                                @if($acq->approval->type->id == 2 || $acq->approval->type->id == 4)
    
                                    @foreach($acq->approval->cidbQualifications as $bil => $greds)
                                        @if($greds->code->type == 'grade')
                                            @if($bil == 0)<b>Gred</b>: @endif
                                            {{$greds->code->code}} 
                                            @if($greds->status == 1)
                                                dan
                                            @elseif($greds->status == 0)
                                                atau
                                            @endif
                                        @endif         
                                    @endforeach    
                                    <br/>                           
                                    @foreach($acq->approval->cidbQualifications as $cetak)
                                        @if($cetak->code->type == 'category')
                                            <b>Kategori</b>: {{$cetak->code->code}}
                                            @if($cetak->status == 1)
                                                dan
                                            @elseif($cetak->status == 0)
                                                atau
                                            @endif
                                        @endif                                
                                        
                                        @if($cetak->code->type == 'khusus')
                                            <b>Khusus</b>: {{$cetak->code->code}}
                                            @if($cetak->status == 1)
                                                dan
                                            @elseif($cetak->status == 0)
                                                atau
                                            @endif                        
                                        @endif
                                    @endforeach
    
                                @elseif($acq->approval->type->id == 1 || $acq->approval->type->id == 3)
    
                                    @foreach($acq->approval->mofQualifications as $index => $mof )                                   
                                        @if($mof->code->type == 'khusus')
                                            @if($index == 0) 
                                                <b>Khusus : </b><br/>
                                            @endif
    
                                            {{ $mof->code->name }}
    
                                            @if($mof->status == 1)
                                                <b>dan</b>
                                            @elseif($mof->status == 0)
                                                <b>atau</b>
                                            @endif 
    
                                            <br/>
    
                                        @endif
                                    @endforeach
    
                                @endif
                            </span>    
    
    
    
                            <br/><br/>Dan<br/><br/>
                            <span>
                                {{--  @if($acq->approval->bumiputera == 1) Bumiputera @else Bukan Bumiputera @endif,<br/>  --}}
                                <b>Berdaftar dengan Bahagian Pembangunan Kontraktor Dan Usahawan (BPKU) Untuk Taraf Bumiputera</b>
                                {{--  @if($acq->approval->mof == '1')Kementerian Kewangan Malaysia (MOF), @endif 
                                @if($acq->approval->cidb == '1')Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM/CIDB), @endif 
                                @if($acq->approval->spkk == '1')Sijil Perolehan Kerja Kerajaan (SPKK) @endif                            
                                @if($acq->approval->bpku_bumiputera == '1')Bahagian Pembangunan Kontraktor & Usahawan (BPKU), @endif   --}}

                            </span>
                        </td>
                        <td valign="top" style="text-align:center">
                            <span style="font-weight:bold">
                                {{(!empty($acq->briefing_at))? "(" . \Carbon::intlFormat($acq->briefing_at) . ")":""}}  
                                @if(!empty($acq->briefing_at))<br/>@endif
                                
                                @php
                                    if(!empty($acq->briefing_at)){
                                        $day = date('N', strtotime($acq->briefing_at));
                                        $hariTaklimat = "";
    
                                        $hariTaklimat = common()->getHariInMalay($day);
    
                                        echo "(" . $hariTaklimat . ")";
                                    }
                                @endphp
                                @if(!empty($acq->briefing_at))<br/>@endif
    
                                ({{(!empty($acq->briefing_at))? date('g:i', strtotime($acq->briefing_at)):""}} 
                                
                                @if(!empty($acq->briefing_at))
                                    {{ (date('A', strtotime($acq->briefing_at)) == "AM")? "pagi":"petang" }})
                                @endif
    
                                @if(!empty($acq->briefing_at))<br/><br/>@endif
                            </span>
                            {{$acq -> briefing_location ?? ''}}                        
                        </td>
                        <td valign="top" style="text-align:center">
                            <span style="font-weight:bold;">
                                @php
                                    if(!empty($acq->start_sale_at) && !empty($acq->closed_sale_at)){
                                        $dayMula = date('N', strtotime($acq->start_sale_at));
                                        $dayTamat = date('N', strtotime($acq->closed_sale_at));
    
                                        $hariMula = "";
                                        $hariTamat = "";
    
                                        $hariMula = common()->getHariInMalay($dayMula);
                                        $hariTamat = common()->getHariInMalay($dayTamat);
                                    }
                                @endphp
    
                                @if(!empty($acq->start_sale_at) && !empty($acq->closed_sale_at))
                                    {{ \Carbon::intlFormat($acq->start_sale_at) }} ({{$hariMula}}) <br/> hingga <br/> {{ \Carbon::intlFormat($acq->closed_sale_at) }} ({{$hariTamat}})
                                @endif
                                
                            </span>
                            @if(!empty($acq->start_sale_at) && !empty($acq->closed_sale_at))<br/>@endif
                            <span>
                                Bahagian Perolehan & Ukur Bahan, Jabatan Kewangan, Aras 1, Blok C, Perbadanan Putrajaya, 
                                Kompleks Perbadanan Putrajaya, 24, Persiaran Perdana, Presint 3, 62675 Putrajaya
                            </span>
                        </td>
                        <td valign="top" style="text-align:center">
                            @if($acq->document_price > 0)
                                <span>
                                    @php
                                        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                                        $amaun = money()->toCommon($acq->document_price ?? "0" , 2);
                                        $format = explode('.',$amaun);
                                        $formats = substr($amaun,-2);
                                        $partringgit = str_replace(',', '', $format[0]);
                                        $partsen = $format[1];
                                        $partkosongsen = (int)$formats;
    
                                        if($partkosongsen == '.00'){
                                            /*  echo $f->format($partringgit) . " SAHAJA";  */
                                        }
                                        else{
                                            /* echo $f->format($partringgit) . " DAN " . $f->format($partkosongsen) . " SEN SAHAJA";  */
                                        }
                                    @endphp	
                                </span>
                                <span style="font-weight:bold;">
                                    Ringgit Malaysia
                                    <br/>
                                    {{ money()->toHuman($acq->document_price)}}
                                    <br/><br/>
                                    (Tidak Dikembalikan)
                                </span>
                            
                                <br/><br/>
                                Tunai/Draf Bank
                                <br/><br/>
                                Perbadanan Putrajaya
                            
                            @endif
                        </td>
                        <td valign="top" style="text-align:center">
                            <span style="font-weight:bold;">
                                {{ (!empty($acq->closed_at)) ? \Carbon::intlFormat($acq->closed_at):"" }}
                                
                                @php
                                    $hariTutupTender = "";
    
                                    if(!empty($acq->closed_at)){
                                        $dayTutupTender = date('N', strtotime($acq->closed_at));                                    
    
                                        $hariTutupTender = common()->getHariInMalay($dayTutupTender);
    
                                        echo "(" . $hariTutupTender . ")";
                                    }
                                @endphp
                                <br/> 12.00 tengah hari
                            </span> 
                            <br/><br/>
                            <span>
                                Bahagian Perolehan & Ukur Bahan, Jabatan Kewangan, Aras 1, Blok C, Perbadanan Putrajaya, 
                                Kompleks Perbadanan Putrajaya, 24, Persiaran Perdana, Presint 3, 62675 Putrajaya
                            </span> <br/>
              
                        </td>
                    </tr>
                </tbody>
            </table>
    
            <table width="100%" border="0" class="content">
                @if($acq->experience_status == 1)
                <tr>
                    <td width="5%" style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td width="95%" style="text-align:justify; padding:5px 5px;">
                        <span style="text-align:justify">
                            Kontraktor/Syarikat juga mesti memenuhi kriteria kelayakan seperti berikut:-
                                <br/> - &nbsp; Mempunyai pengalaman dan pernah menyiapkan kerja-kerja yang berkaitan bernilai sekurang-kurangnya 
                            {{ money()->toHuman($acq->experience_value ?? "0" , 2) }} bagi ({{$acq->experience_duration ?? '0'}}) kontrak 
                            dalam tempoh 5 tahun terdahulu.
                        </span>
                    </td>
                </tr>
                @endif
                <tr>
                    <td style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td style="text-align:justify; padding:5px 5px;" valign="top">
                        <span style="text-align: justify;">
                            Dokumen Meja Tender boleh disemak dan Dokumen Tender boleh diperolehi di pejabat tersebut pada waktu pejabat.
                        </span>                    
                    </td>
                </tr>
                @if($acq->briefing_status == 1)
                <tr>
                    <td style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td style="text-align:justify; padding:5px 5px;" valign="top">
                        <span style="text-align: justify;">
                            {{--  Taklimat Tender yang disusuli dengan lawatan tapak akan diadakan pada jam
                            <span style="font-weight: bold;">
                                {{(!empty($acq->briefing_at))? date('g:i', strtotime($acq->briefing_at)):""}}
                                {{ (date('A', strtotime($acq->briefing_at)) == "AM")? "pagi":"petang" }},
                                {{(!empty($acq->briefing_at))? \Carbon::intlFormat($acq->briefing_at):""}} 
                            </span>
                             bersamaan hari 
                            <span style="font-weight:bold;">
                                {{ $hariTaklimat }}
                            </span>
                             di   --}}
                            <span style="font-weight:bold;">
                                 {{--  {{$acq -> briefing_location ?? ''}}.   --}}
                                 Kehadiran PEMILIK SYARIKAT (SEBAGAIMANA NAMA DI DALAM SIJIL SSM/BORANG 9, 24 &amp; 49) 
                             </span>
                             adalah 
                            <span style="font-weight:bold;">
                                 DIWAJIBKAN 
                            </span>
                            (Sila bawa bersama Sijil Asal dan Cap Syarikat) dan Dokumen Tender hanya akan dikeluarkan kepada 
                            kontraktor/syarikat yang menghadiri taklimat dan lawatan tapak tersebut.
                        </span>                    
                    </td>
                </tr>  
                @endif
                <tr>
                    <td style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td style="text-align:justify; padding:5px 5px;" valign="top">
                        <span style="text-align: justify;">
                            Dokumen tender hanya akan dikeluarkan kepada wakil-wakil Kontraktor/Syarikat yang sah sahaja dimana wakil-wakil 
                            Kontraktor/Syarikat adalah <span style="text-transform: uppercase; font-weight: bold">diwajibkan</span> untuk 
                            membawa surat perwakilan kuasa yang ditandatangani oleh pemilik syarikat.  Wakil-wakil Kontraktor/Syarikat juga hendaklah 
                            membawa dokumen-dokumen <span style="font-weight: bold;">asal dan satu (1) salinan Sijil Pendaftaran 
                            Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM), Sijil Perolehan Kerja Kerajaan (SPKK), dan Sijil Pengiktirafan Taraf Bumiputera (BPKU). </span>
                            Salinan Sijil-sijil Pendaftaran BPKU boleh diterima jika disahkan oleh Pengarah BPKU.
                        </span>                    
                    </td>
                </tr>  
                <tr>
                    <td style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td style="text-align:justify; padding:5px 5px;" valign="top">
                        <span style="text-align: justify;">
                            Tender ini akan ditutup pada <span style="font-weight: bold;">{{ (!empty($acq->closed_at)) ? \Carbon::intlFormat($acq->closed_at):"" }} </span>
                            bersamaan hari <span style="font-weight:bold;">{{ $hariTutupTender }}, jam 12.00 tengah hari. </span>  
                            Dokumen Tender yang dilengkapkan dan dimasukkan ke dalam sampul berlakri serta bertanda dengan tajuk projek, hendaklah dimasukkan ke dalam Peti Tender yang 
                            dikhaskan di tempat dan sebelum waktu yang ditetapkan seperti di atas.
                        </span>                    
                    </td>
                </tr>  
                <tr>
                    <td style="text-align:left; padding:5px 5px;" valign="top">{{ $bilangan = $bilangan + 1 }}.</td>
                    <td style="text-align:justify; padding:5px 5px;" valign="top">
                        <span style="text-align: justify;">
                            Petender hendaklah menanggung sendiri segala perbelanjaan yang berkaitan dengan urusan Tender ini.  Pembayaran secara cek 
                            persendirian untuk Dokumen Tender tidak akan diterima.  Bayaran dokumen Tender tidak akan dikembalikan.
                        </span>                    
                    </td>
                </tr>                                           
            </table>
            <div style="text-align:center">
                <span style="text-align: left; text-transform: uppercase;" class="content">
                    Kenyataan ini juga terdapat di Laman Web Perbadanan Putrajaya Di Alamat :
                </span>
                <u>www.ppj.gov.my</u>
            </div>
        </body>
    </html>