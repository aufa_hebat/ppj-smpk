<!DOCTYPE html>
<html>
    <head>
        <META http-equiv="X-UA-Compatible" content="IE=8" />
        <style type="text/css">

            @page {
                margin: 60px 60px 60px 100px;
                size: A4;
            }

            @media print {
                html, body {
                    width: 210mm;
                    height: 297mm;
                }
            }

            thead { display: table-header-group; }
            tbody {
                display: table-row-group;
                height: 100%;
            }
            tfoot { display: table-footer-group; }
            table tr{page-break-inside:avoid;}

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 150px;
                color: black;
                text-align: right;
            }
    
            body{
                font-size: 12px;   
                font-family: "Arial, Helvetica, sans-serif";
                counter-reset:page -1;        
            } 
    
            footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: center;
                line-height: 35px;	
            }

            .pagenum:before {
                content: counter(page);                
            }
                
            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
    
            section {
               counter-reset:page;
            } 
    
            section .pagenum:after {                
                content: counter(page);
                counter-increment: page;
            }
    
            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .footers {
                position: fixed;
                left: 0px;
                bottom: 100px;
                right: 0px;
            }

        </style>
    </head>
    <body>
        <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>
        <p class="MsoNormal title" align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal; font-weight:bold;'>SUMMARY OF TENDER</p>
        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal; font-weight:bold;'>No. {{$acq->category->name ?? ''}} : {{ $acq->reference ?? ''}}</p>
        <p class=MsoNormal align=center style="text-transform:uppercase; font-weight:bold; text-align: center;">{{$acq->title ?? ''}}</p>
    
        <center>
            <table border="0" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
                <thead>
                    <tr>
                        <th style="width: 4%;" class="bordered">ELEMENT</th>
                        <th style="width: 55%;" class="bordered">DESCRIPTION</th>
                        <th style="width: 20%; text-align:center;" class="bordered">PAGE</th>
                        <th style="width: 20%;  text-align:center;"class="bordered">AMOUNT (RM)</th> 
                    </tr>
                </thead>                
                <tbody>
                    <?php $total_amount = 0 ?>
                    @foreach($acq->bqElements as $bil => $row)
                        <?php $sum_element = DB::table('bq_subitem')->select(DB::raw('SUM(amount) as Total'))->where('acquisition_id','=',$row->acquisition_id)->where('bq_no_element','=',$row->no)->first();?>
                                
                        <tr>
                            <td class="bordered" style="text-align: center;"><b>{{ $row->no }}.0</b></td>
                            <td class="bordered"><b>{{$row->element ?? ''}}</b></td>
                            <!--<td class="bordered"><center><b>%%CH{{$bil + 1}}%% - %%CH{{$bil + 1}}{{$bil + 1}}%%</b></center></td>-->
                            <td class="bordered"><center><b>{{ $row->paging_start }}</b></center></td>
                            <td class="bordered" align="right">
                                @if('PROVISIONAL SUMS' == $row->element || 'PROVISIONAL SUM' == $row->element || 'PROV SUM' == $row->element || 'PROV. SUM' == $row->element || 'PROV.SUM' == $row->element)
                                    <b>{{ money()->toCommon($sum_element->Total ?? "0" , 2) }}</b>
                                @endif
                            </td> 
                            <?php $total_amount = $total_amount + $sum_element->Total ?>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr class="title">
                        <td colspan="3" class="bordered" style="text-transform:uppercase; padding-top:15px; padding-bottom:15px;">
                            <b>TOTAL CARRIED TO FORM OF TENDER</b>
                        </td>
                        <td align="right" class="bordered">
                            {{--  <b>{{ money()->toCommon($total_amount ?? "0" , 2) }}</b>  --}}
                        </td>
                    </tr>
                </tfoot>
            </table>
            <br/>
            <div style="text-align: left;">
                Ringgit Malaysia : 
                <label class=MsoNormal style='color: black;font-size: 13px;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;text-transform: uppercase'>
                        
                    {{--  @php    
                        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                        $amaun = money()->toCommon($total_amount ?? "0" , 2);
                        $format = explode('.',$amaun);
                        $formats = substr($amaun,-2);
                        $partringgit = str_replace(',', '', $format[0]);
                        $partsen = $format[1];
                        $partkosongsen = (int)$formats;
    
                        if($partkosongsen == '.00'){
                            echo $f->format($partringgit) . " SAHAJA";
                        }
                        else{
                            echo $f->format($partringgit) . " DAN " . $f->format($partkosongsen) . " SEN SAHAJA";
                        }
    
                    @endphp	
                        
                    (RM {{ money()->toCommon($total_amount ?? "0" , 2) }})  --}}
                </label><br>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>Bertarikh : _____________ haribulan _____________ {{$acq->approval->year}}</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>Tempoh siap Kerja : {{$acq->approval->period_length}} {{$acq->approval->period_type->name}}</p>
            </div>
            <br/><br/><br/>
            <table style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center">
                <tr>
                    <td style="width: 53%;"><br>__________________________________________</td>
                    <td style="width: 47%;"><br>__________________________________________</td>
                </tr>
                <tr>
                    <td style="text-align:center">Signature of Contractor</td>
                    <td style="text-align:center">Signature of Witness</td>
                </tr>
                <tr>
                    <td><br>NRIC &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : _____________________________</td>
                    <td><br>NRIC &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : _____________________________</td>
                </tr>                
                <tr>
                    <td><br>Name in full : _____________________________</td>
                    <td><br>Name in full : _____________________________</td>
                </tr>

                <tr>
                    <td><br>Duty authorized to sign for and on behalf of: </td>
                    <td><br>Duty authorized to sign for and on behalf of: </td>
                </tr>
                <tr>
                    <td colspan="2"><br/><br/><br/><br/></td>
                </tr>
                <tr>
                    <td><br><br>__________________________________________</td>
                    <td><br><br>__________________________________________</td>
                </tr>
                <tr>
                    <td style="text-align:center">Contractor seal or chop</td>
                    <td style="text-align:center">Contractor seal or chop</td>
                </tr>
                <tr>
                    <td><br>Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ______________________________</td>
                    <td><br>Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ______________________________</td>
                </tr>
                <tr>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ______________________________</td>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ______________________________</td>
                </tr>
                <tr>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ______________________________</td>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ______________________________</td>
                </tr>
                <tr>
                    <td><br>No. Tel &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ______________________________</td>
                    <td><br>No. Tel &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ______________________________</td>
                </tr>
            </table>                        
        </center>

        <div class="breakNow"></div>

        @if(!empty($acq->bqElements))
            <footer>
                <div class="pagenum-container">Page BQ <span class="pagenum"></span></div>
            </footer>
        @endif  
        
        <?php $total_amount = 0 ?>
        @foreach($acq->bqElements as $bil => $row)  

            @if($bil > 0)
                <div class="breakNow"></div> 
            @endif

            @php $itemBq = $bil + 1 @endphp                
            
            <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><b>No. {{$acq->category->name ?? ''}} : {{ $acq->reference ?? ''}}</b></p><br>
            <p class=MsoNormal align=center style="text-transform:uppercase;"><b>{{$acq->title ?? ''}}</b></p>
            <?php $sum_element = DB::table('bq_subitem')->select(DB::raw('SUM(amount) as total'))->where('acquisition_id','=',$row->acquisition_id)->where('bq_no_element','=',$row->no)->first();?> 
    
            <section>  
                <table style="width: 100%;border-collapse: collapse;padding: 7px 7px;">
                    <thead style="display: table-header-group;">
                        <tr style="padding: 10px 10px;">
                            <th style="width: 10%;padding: 10px 10px;" class="bordered">BIL.</th>
                            <th class="bordered">DESCRIPTION</th>
                            <th style="width: 7%; text-align:center;" class="bordered">UNIT</th>
                            <th style="width: 8%; text-align:center;" class="bordered">QTY</th> 
                            <th style="width: 12%; text-align:center;" class="bordered">RATE</th>
                            <th style="width: 16%; text-align:center;" class="bordered">AMOUNT (RM)</th>
                        </tr>
                    </thead>
                    <tbody style="display: table-row-group;">
                        <tr style="padding: 7px 7px;">                            
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px; text-align:left;" valign="top"><b>{{ $row->no ?? ''}}.0</b></td>
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"><b><u>{{ $row->element ?? ''}}</u></b> <br> {{$row->description ?? ''}}</td>
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                        </tr>
                        @foreach($bq_item->where('bq_no_element',$row->no) as $bil_item => $item)
                            <tr style="padding: 7px 7px;">
                                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;  text-align:center;" valign="top">&nbsp;&nbsp;<b>{{ $item->bq_no_element }}.{{ $item->no }}</b></td>
                                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"><b>{{ $item->item }}</b><br>{{ $item->description }}</td>
                                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                            </tr>  
                            @foreach($bq_sub->where('bq_no_element',$row->no)->where('bq_no_item',$item->no) as $bil_sub => $sub)
                                <tr style="padding: 7px 7px;">
                                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;  text-align:right;" valign="top">{{ $sub->bq_no_element }}.{{ $sub->bq_no_item }}.{{ $sub->no }}</td>
                                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;">
                                        {{ $sub->item }}
                                    </td>
                                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; text-align:center; padding: 7px 7px;">{{ $sub->unit }}</td>
                                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; text-align:center; padding: 7px 7px;">{{ str_replace(".00","",money()->toCommon($sub->quantity ?? "0", 2)) }}</td>
                                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; text-align:right; padding: 7px 7px;">
                                        @if('PROVISIONAL SUMS' == $sub->bqElement->element || 'PROVISIONAL SUM' == $sub->bqElement->element || 'PROV SUM' == $sub->bqElement->element || 'PROV. SUM' == $sub->bqElement->element || 'PROV.SUM' == $sub->bqElement->element)
                                            {{ money()->toCommon($sub->rate_per_unit ?? "0" , 2) }}
                                        @endif
                                    </td>
                                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; text-align:right; padding: 7px 7px;">
                                        @if('PROVISIONAL SUMS' == $sub->bqElement->element || 'PROVISIONAL SUM' == $sub->bqElement->element || 'PROV SUM' == $sub->bqElement->element || 'PROV. SUM' == $sub->bqElement->element || 'PROV.SUM' == $sub->bqElement->element)
                                            {{ money()->toCommon($sub->amount ?? "0" , 2) }}
                                        @endif
                                    </td>
                                </tr>
                                @if(!empty($sub->description))
                                    <tr style="padding: 7px 7px;">
                                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;">{{$sub->description}}</td>
                                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding: 7px 7px;"></td>
                                    </tr>  
                                @endif                                                                                
                            @endforeach
                        @endforeach
                        <?php $total_amount = $total_amount + $sum_element->total ?> 
                    </tbody>
                    <tfoot style="display: table-footer-group;">
                        <tr style="padding: 7px 7px;">
                            <th class="bordered" colspan="5" style="padding: 10px 10px;"><b>CARRIED TO COLLECTION</b></th>
                            <th class="bordered" style="text-align:right">
                                @if('PROVISIONAL SUMS' == $item->bqElement->element || 'PROVISIONAL SUM' == $item->bqElement->element || 'PROV SUM' == $item->bqElement->element || 'PROV. SUM' == $item->bqElement->element || 'PROV.SUM' == $item->bqElement->element)
                                    {{ money()->toCommon($sum_element->total ?? "0" , 2) }}
                                @endif
                            </th>
                        </tr>
                    </tfoot>
                </table>

                <div class="breakNow"></div> 

                <?php $sum_element = DB::table('bq_subitem')->select(DB::raw('SUM(amount) as total'))->where('acquisition_id','=',$row->acquisition_id)->where('bq_no_element','=',$row->no)->first();?> 
                <p class=MsoNormal align=center style="text-transform:uppercase;"><b>{{$acq->title}}</b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal; font-weight:bold;'>No. {{$acq->category->name ?? ''}} : {{ $acq->reference ?? ''}}</p>

                <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;">
                    <thead>
                        <tr>
                            <th style="width: 7%; padding: 10px 10px;">BIL.</th>
                            <th style="width: 70%;">COLLECTION PAGE</th>
                            <th style="width: 23%; text-align:center;">AMOUNT (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td height="800px" valign="top" align="center">&nbsp;
                                <br/><br/>
                                {{ $row->no }}.0
                            </td>
                            <td valign="top">
                                <u><b>COLLECTION</b></u>
                                <br/><br/>
                                {{ $row->element }}
                                <br/><br/>
                                @php                                    
                                    $start = $row->paging_start;
                                    $end = $row->paging_end;
                                    $i = 0;
                                        
                                    for($i = $start; $i <= $end; $i++){
                                        echo '&nbsp;&nbsp;&nbsp;Page BQ ' . $i;
                                        echo '<br/><br/>';
                                    }                                
                                @endphp                                
                            </td>
                            <td></td>
                        </tr>                                                
                        <tr>
                            <td colspan="2" style="padding:10px 10px;"><b>TOTAL CARRIED TO SUMMARY OF TENDER</b></td>
                            <td align="right">
                                @if('PROVISIONAL SUMS' == $row->element || 'PROVISIONAL SUM' == $row->element || 'PROV SUM' == $row->element || 'PROV. SUM' == $row->element || 'PROV.SUM' == $row->element)
                                    {{ money()->toCommon($sum_element->total ?? "0" , 2) }}
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>                
            </section>
        @endforeach
    </body>
</html>