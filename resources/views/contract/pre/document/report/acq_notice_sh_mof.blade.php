<html>
    <head>
        <style type="text/css">
            @page {
                margin: 70px 100px 70px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
    
            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
        </style>
    </head>
    <body>
        <center>
            <img style="width: 100px; height: 100px;" src="{{ logo($print) }}">
            <br>
            <span class="title" style="text-transform: uppercase; font-weight: bold;">NOTIS SEBUT HARGA</span>
            <br/>
            <span class="title" style="text-transform: uppercase; font-weight: bold;">PERBADANAN PUTRAJAYA</span>
            <br/><br/>
            <span class="content" style="font-weight: bold;">No. Sebut Harga : {{ $acq->reference ?? ''}}</span>
            <br/><br/>
                <span class="content" style="font-weight: bold;">Tajuk Sebut Harga : </span>
                <br/><br/>
                <span class="content" style="font-weight: bold;">{{$acq->title ?? ''}}</span>
                <br/><br/>
        </center>
    
        @php $bil = 0; @endphp
        <table style="width: 100%;border-collapse: collapse;padding: 4 4;" class="content" align="center">
            <tr>
                <td style="width: 10%;" valign="top">{{ $bil = $bil + 1 }}.</td>
                <td valign="top" class="MsoNormal" style="text-align: justify;line-height:normal;">
                        Sebut harga adalah dipelawa daripada 
                        <span style="font-weight: bold;">Kontraktor 
                            @if($acq->approval->type->id == 2)
                                @if($acq->approval->bpku_bumiputera == 1) Bumiputera @else Bukan Bumiputera @endif
                            @elseif($acq->approval->type->id == 1 || $acq->approval->type->id == 3)
                                @if($acq->approval->bumiputera == 1) Bumiputera @else Bukan Bumiputera @endif
                            @endif
                        </span> 
                        yang berdaftar dengan 
                        <span style="font-weight: bold;">
                            @if($acq->approval->mof == '1')Kementerian Kewangan Malaysia (KKM/MOF), @endif 
                            @if($acq->approval->cidb == '1')Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM/CIDB), @endif 
                            @if($acq->briefing_status == 1)
                                @if($acq->approval->ssm == '1')Suruhanjaya Syarikat Malaysia (SSM), @endif 
                            @endif
                            @if($acq->approval->bpku_bumiputera == '1')Bahagian Pembangunan Kontraktor & Usahawan (BPKU), @endif 
                            @if($acq->briefing_status == 1)
                                @if($acq->approval->spkk == '1')Sijil Perolehan Kerja Kerajaan (SPKK) @endif
                            @endif
                        </span> 
                        beralamat di 
                        <span style="font-weight: bold;">
                            Wilayah Persekutuan Putrajaya, Wilayah Persekutuan Kuala Lumpur dan Selangor sahaja
                        </span> 
                        
                        @if($acq->approval->type->id == 2 || $acq->approval->type->id == 4)
                            untuk
                        @else
                            di bawah <span style="font-weight: bold;">Kod Bidang </span>
                        @endif
                            
                        {!! $cidb !!}
    
                        untuk melaksanakan projek tersebut 
                        @if($acq->experience_status == 1) diatas serta 
                            <span style="font-weight: bold;">mempunyai pengalaman dan keupayaan dalam menyiapkan kerja-kerja yang berkaitan bernilai sekurang-kurangya RM{{ money()->toCommon($acq->experience_value ?? "0" , 2) }} 
                                bagi 
                                @if($acq->experience == '2')
                                    kontrak terkumpul 
                                @else
                                    ({{$acq->experience_duration ?? '0'}}) kontrak 
                                @endif
                                dalam tempoh (5) tahun terdahulu.
                            </span>
                        @endif
                        <br/><br/>
                    </td>
                </tr>
                @if($acq->briefing_status == 1)
                    <tr>
                        <td valign="top">{{ $bil = $bil + 1 }}.</td>
                        <td valign="top" style="text-align: justify;line-height:normal">
                            Taklimat sebut harga akan diadakan pada {{(!empty($acq->briefing_at))? \Carbon::intlFormat($acq->briefing_at):""}}
                            @php
                                if(!empty($acq->briefing_at)){
                                    $day = date('N', strtotime($acq->briefing_at));
                                    $hariTaklimat = "";
        
                                    $hariTaklimat = common()->getHariInMalay($day);
        
                                    echo "(" . $hariTaklimat . "),";
                                }
                            @endphp
                            jam 
                            {{(!empty($acq->briefing_at))? date('g:i', strtotime($acq->briefing_at)):""}} 
                            {{ (date('A', strtotime($acq->briefing_at)) == "AM")? "pagi":"petang" }}
        
                            dan bertempat di 
                            <span style="font-weight: bold;">{{$acq -> briefing_location ?? ''}}.  
                                Kehadiran PEMILlK SYARIKAT (SEBAGAIMANA NAMA DALAM SIJIL SSM / BORANG 9, 24 & 49 adalah 
                                DIWAJIBKAN (Sila bawa bersama Sijil Asal SSM / BORANG 9, 24 & 49, 
                                Sijil Akuan Pendaftaran Syarikat Bumiputera (KKM/MOF), Sijil Akuan Pendaftaran Syarikat (KKM/MOF) dan Cap Syarikat)
                            </span>) 
                            Penyebut harga yang menghadiri taklimat @if($acq->visit_status == 1) dan lawatan tapak @endif
                            sahaja layak untuk membeli dokumen sebut harga.
                            <br/><br/>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td valign="top">{{ $bil = $bil + 1 }}.</td>
                    <td valign="top" style="text-align: justify;line-height:normal">
                        Borang-borang sebut harga boleh didapati semasa waktu pejabat pada 
                        {{(!empty($acq->start_sale_at))? \Carbon::intlFormat($acq->start_sale_at):""}} 
                        
                        @php
                            if(!empty($acq->start_sale_at)){
                                $day = date('N', strtotime($acq->start_sale_at));
                                $hariMulaJualan = "";
    
                                $hariMulaJualan = common()->getHariInMalay($day);
    
                                echo "(" . $hariMulaJualan . ")";
                            }
                        @endphp
    
                        hingga {{(!empty($acq->closed_sale_at))? \Carbon::intlFormat($acq->closed_sale_at):""}} 
                        
                        @php
                            if(!empty($acq->closed_sale_at)){
                                $day = date('N', strtotime($acq->closed_sale_at));
                                $hariTamatJualan = "";
    
                                $hariTamatJualan = common()->getHariInMalay($day);
    
                                echo "(" . $hariTamatJualan . ")";
                            }
                        @endphp
    
                        dengan bayaran secara tunai sebanyak <span style="font-weight: bold;">RM {{ money()->toCommon($acq -> document_price ?? "0" , 2) }} </span>di 
                        <strong>Aras 1, Bahagian Perolehan dan Ukur Bahan, Jabatan
                        Kewangan, Blok C, Kompleks Perbadanan Putrajaya,  24, Persiaran Perdana, Presint 3, 62675 Wilayah Persekutuan Putrajaya.</strong>
                        <br/><br/>
                    </td>
                </tr>
                <tr>
                    <td valign="top">{{ $bil = $bil + 1 }}.</td>
                    <td valign="top" style="text-align: justify;line-height:normal">
                        Kontraktor dikehendaki membawa dokumen-dokumen <span style="font-weight: bold;">Asal</span>
                        dan <span style="font-weight: bold;">Satu (1) Salinan</span> sijil yang berkaitan. Wakil-wakil kontraktor adalah 
                        <span style="font-weight: bold;">diwajibkan</span> untuk membawa surat perwakilan kuasa yang ditandatangani oleh 
                        <span style="font-weight: bold;">Pemilik Syarikat</span> untuk membeli dokumen sebut harga.
                        <br/><br/>
                    </td>
                </tr>
                <tr>
                    <td valign="top">@if($acq->briefing_status == 1) 5. @else 4. @endif</td>
                    <td valign="top" style="text-align: justify;line-height:normal">
                        Semua Borang Sebut harga yang telah lengkap diisi hendaklah
                        dikembalikan kepada alamat seperti yang tertera di atas tidak lewat dari Jam <span style="font-weight: bold;">12.00</span> 
                        tengahari pada {{(!empty($acq -> closed_at)) ? \Carbon::intlFormat($acq -> closed_at):''}} 
                        
                        @php
                            if(!empty($acq->closed_at)){
                                $day = date('N', strtotime($acq->closed_at));
                                $hariTutupPerolehan = "";
    
                                $hariTutupPerolehan = common()->getHariInMalay($day);
    
                                echo "(" . $hariTutupPerolehan . ")";
                            }
                        @endphp
                        .
                    </td>
                </tr>
            </table>
            <br/><br/><br/>
            <center>
                <span class="content" style='text-align:center;line-height:normal'>
                    ____________________________________<br/>
                    ({{$np_honor}}&nbsp;{{$np_dept}})<br>Naib Presiden<br>Jabatan {{$acq->approval->department->name}}<br>Perbadanan Putrajaya<br>
                </span>
            </center>
        </body>
    </html>