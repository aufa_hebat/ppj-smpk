@if($acq->approval->type->id == 2 || $acq->approval->type->id == 4)  
    @include('contract.pre.document.report.acq_notice_tender_cidb', ['acq' => $acq])
@else
    @include('contract.pre.document.report.acq_notice_tender_mof', ['acq' => $acq])
@endif