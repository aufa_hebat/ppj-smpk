<html>
    <head>
        <style type="text/css">
            /** Define the margins of your page **/
            @page {
                margin: 50px 60px 50px 100px;
                size: A4;
            }
    
            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 150px;
                color: black;
                text-align: right;
            }
    
            body{
                font-size: 12px;   
                font-family: "Arial, Helvetica, sans-serif";
                counter-reset:page -1;        
            } 
    
            footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: center;
                line-height: 35px;	
            }

            /*footer .pagenum:before {
                counter-increment: page;
                content: counter(page);
            }*/

            .pagenum:before {
                content: counter(page);                
            }
                
            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
    
            section {
               counter-reset:page;
            } 
    
            section .pagenum:after {                
                content: counter(page);
                counter-increment: page;
            }
    
            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }

        </style>          
    </head>
            
    <body>  
    
        <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>
        <p class="MsoNormal title" align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal; font-weight:bold;'>RINGKASAN SEBUT HARGA</p>
        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal; font-weight:bold;'><b>No. {{$acq->category->name ?? ''}} : {{ $acq->reference ?? ''}}</b></p>
        <p class=MsoNormal align=center style="text-transform:uppercase; font-weight:bold; text-align: center;"><b>{{$acq->title ?? ''}}</b></p>
            
        <center>
            <table border="0" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
                <thead>
                    <tr>
                        <th style="width: 4%; padding:10px 10px;" class="bordered">ELEMEN</th>
                        <th style="width: 55%;" class="bordered">BUTIRAN</th>
                        <th style="width: 10%; text-align:center;" class="bordered">MUKA SURAT</th>
                        <th style="width: 30%;  text-align:center;"class="bordered">JUMLAH (RM)</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php $total_amount = 0 ?>
                    @foreach($acq->bqElements as $bil => $row)
                        <?php $sum_element = DB::table('bq_subitem')->select(DB::raw('SUM(amount) as Total'))->where('acquisition_id','=',$row->acquisition_id)->where('bq_no_element','=',$row->no)->where('deleted_at', '!=', null)->first();?>
                        
                        <tr>
                            <td class="bordered" style="text-align: center; padding:5px 5px;"><b>{{$row->no}}.0</b></td>
                            <td class="bordered" style="padding:5px 5px;"><b>{{$row->element ?? ''}}</b></td>
                            <!--<td class="bordered"><center><b>%%CH{{$bil + 1}}%% - %%CH{{$bil + 1}}{{$bil + 1}}%%</b></center></td>-->
                            <td class="bordered" style="padding:5px 5px;"><center><b>{{ $row->paging_start }}</b></center></td>
                            <td class="bordered" align="right" style="padding:5px 5px;">
                                {{--  @if('PROVISIONAL SUMS' == $row->element || 'PROVISIONAL SUM' == $row->element || 'PROV SUM' == $row->element || 'PROV. SUM' == $row->element || 'PROV.SUM' == $row->element)  --}}
                                @php
                                    $wordsArray = array('PROVISIONAL SUMS','PROVISIONAL SUM', 'PROV SUM', 'PROV. SUM', 'PROV.SUM');
                                    $string = $row->element;
                                    $matchFound = "";
                                    $matchFound = preg_match_all(
                                        "/\b(" . implode($wordsArray,"|") . ")\b/i", 
                                        $string, 
                                        $matches
                                    );

                                @endphp
                                @if($matchFound)                                
                                    <b>{{ money()->toCommon($sum_element->Total ?? "0" , 2) }}</b>
                                @endif
                            </td> 
                            <?php $total_amount = $total_amount + $sum_element->Total ?>
                        </tr>
                    @endforeach
                    <tr class="title">
                        <td colspan="3" class="bordered" style="text-transform:uppercase; padding-top:10px; padding-bottom:10px;">
                            <b>JUMLAH KESELURUHAN DIBAWA KE BORANG SEBUT HARGA</b>
                        </td>
                        <td align="right" class="bordered">
                            {{--  <b>{{ money()->toCommon($total_amount ?? "0" , 2) }}</b>  --}}
                        </td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <div style="text-align: left;">
                Ringgit Malaysia : .........................................................................................................................................................<br/><br/>
                .....................................................................................................................................................................................
                <label class=MsoNormal style='color: black;font-size: 13px;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;text-transform: uppercase'>
                        
                    {{--  @php    
                        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                        $amaun = money()->toCommon($total_amount ?? "0" , 2);
                        $format = explode('.',$amaun);
                        $formats = substr($amaun,-2);
                        $partringgit = str_replace(',', '', $format[0]);
                        $partsen = $format[1];
                        $partkosongsen = (int)$formats;
    
                        if($partkosongsen == '.00'){
                            echo $f->format($partringgit) . " SAHAJA";
                        }
                        else{
                            echo $f->format($partringgit) . " DAN " . $f->format($partkosongsen) . " SEN SAHAJA";
                        }
    
                    @endphp	
                        
                    (RM {{ money()->toCommon($total_amount ?? "0" , 2) }})  --}}
                </label><br>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>Bertarikh : _____________ haribulan _____________ {{$acq->approval->year}}</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>Tempoh siap kerja : {{$acq->approval->period_length}} {{$acq->approval->period_type->name}}</p>
            </div>
            <table style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center">
                <tr>
                    <td style="width: 53%;"><br>__________________________________________</td>
                    <td style="width: 47%;"><br>__________________________________________</td>
                </tr>
                <tr>
                    <td align="center">Tandatangan @if($acq->category->name == 'Sebut Harga' || $acq->category->name == 'Sebut Harga B') Penyebut Harga @else Petender @endif</td>
                    <td align="center">Tandatangan Saksi</td>
                </tr>
                <tr>
                    <td><br>Nama Penuh : _____________________________</td>
                    <td><br>Nama Penuh : _____________________________</td>
                </tr>
                <tr>
                    <td><br>No. K/P &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : _____________________________</td>
                    <td><br>No. K/P &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : _____________________________</td>
                </tr>
                <tr>
                    <td><br>Atas Sifat &nbsp;&nbsp;&nbsp; : _____________________________</td>
                    <td><br>Pekerjaan &nbsp;&nbsp;&nbsp;&nbsp; : _____________________________</td>
                </tr>
                <tr>
                    <td>Yang Diberi kuasa dengan sempurna untuk <br/>menandatangani @if($acq->category->name == 'Sebut Harga' || $acq->category->name == 'Sebut Harga B') sebutharga @else tender @endif ini</td>
                    <td><br>Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : _____________________________</td>
                </tr>
                <tr>
                    <td><br><br>__________________________________________</td>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _____________________________</td>
                </tr>
                <tr>
                    <td>Meterai dan Cop @if($acq->category->name == 'Sebut Harga' || $acq->category->name == 'Sebut Harga B') Penyebut Harga @else Tender @endif</td>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _____________________________</td>
                </tr>
                <tr>
                    <td><br>Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ______________________________</td>
                    <td></td>
                </tr>
                <tr>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ______________________________</td>
                    <td></td>
                </tr>
                <tr>
                    <td><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ______________________________</td>
                    <td></td>
                </tr>
                <tr>
                    <td><br>No. Tel &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ______________________________</td>
                    <td></td>
                </tr>
            </table>
        </center>
    
        <div class="breakNow"></div>
        @if(!empty($acq->bqElements))
            <footer>
                <div class="pagenum-container">Page BQ <span class="pagenum"></span></div>
            </footer>
        @endif

        <?php $total_amount = 0 ?>
        @foreach($acq->bqElements as $bil => $row)  

            @if($bil > 0)
            <div class="breakNow"></div> 
            @endif


            @php $itemBq = $bil + 1 @endphp    
            
            
            <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><b>No. {{$acq->category->name ?? ''}} : {{ $acq->reference ?? ''}}</b></p><br>
            <p class=MsoNormal align=center style="text-transform:uppercase;"><b>{{$acq->title ?? ''}}</b></p>
            <?php $sum_element = DB::table('bq_subitem')->select(DB::raw('SUM(amount) as total'))->where('acquisition_id','=',$row->acquisition_id)->where('bq_no_element','=',$row->no)->where('deleted_at', '!=', null)->first();?> 
    
            <section>  
                {{--  <footer>
                    <div class="pagenum-container">B.Q {{ $itemBq}}.<span class="pagenum"></span></div>
                </footer>  --}}
 
                    
                <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;">
                    <thead>
                        <tr>
                            <th style="width: 10%; padding: 10px 10px;s">BIL</th>
                            <th>BUTIRAN</th>
                            <th style="width: 7%; text-align:center;">UNIT</th>
                            <th style="width: 8%; text-align:center;">KUANTITI</th> 
                            <th style="width: 12%; text-align:center;">KADAR HARGA (RM)</th>
                            <th style="width: 16%; text-align:center;">AMAUN (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <script type="text/php">                            
                                $GLOBALS['chapters']['{{$bil + 1}}'] = $pdf->get_page_number();
                            </script>                                
                            <td style="padding: 7px 7px; " valign="top"><b>{{ $row->no ?? ''}}</b></td>
                            <td style="padding: 7px 7px; " valign="top"><b><u>{{ $row->element ?? ''}}</u></b> <br> {{$row->description ?? ''}}</td>
                            <td style="padding: 7px 7px; "></td>
                            <td style="padding: 7px 7px; "></td>
                            <td style="padding: 7px 7px; "></td>
                            <td style="padding: 7px 7px; "></td>
                        </tr>
                            
                        @foreach($bq_item->where('bq_no_element',$row->no) as $bil_item => $item)
                            <tr>
                                <td style="padding: 7px 7px; " valign="top">&nbsp;&nbsp;<b>{{ $item->bq_no_element }}.{{ $item->no }}</b></td>
                                <td style="padding: 7px 7px; " valign="top"><b>{{ $item->item }}</b><br>{{ $item->description }}</td>
                                <td style="padding: 7px 7px; "></td>
                                <td style="padding: 7px 7px; "></td>
                                <td style="padding: 7px 7px; "></td>
                                <td style="padding: 7px 7px; "></td>
                            </tr>
                                
                            @foreach($bq_sub->where('bq_no_element',$row->no)->where('bq_no_item',$item->no) as $bil_sub => $sub)
                                @php
                                    $wordsArray = array('PROVISIONAL SUMS','PROVISIONAL SUM', 'PROV SUM', 'PROV. SUM', 'PROV.SUM');
                                    $string = $sub->bqElement->element;
                                    $matchFound = "";
                                    $matchFound = preg_match_all(
                                        "/\b(" . implode($wordsArray,"|") . ")\b/i", 
                                        $string, 
                                        $matches
                                    );
                                @endphp
                                <tr>
                                    <td style="text-align:right; padding: 7px 7px;" valign="top">{{ $sub->bq_no_element }}.{{ $sub->bq_no_item }}.{{ $sub->no }}</td>
                                    <td style="padding: 7px 7px; " valign="top">
                                        {{ $sub->item }}
                                        @if(!empty($sub->description))
                                            <br/><br/>
                                            {{$sub->description}}
                                        @endif
                                    </td>
                                    <td style="text-align:center; padding: 7px 7px;" valign="top">{{ $sub->unit }}</td>
                                    <td style="text-align:center; padding: 7px 7px;" valign="top">{{ str_replace(".00","",money()->toCommon($sub->quantity ?? "0", 2)) }}</td>
                                    <td style="text-align:right; padding: 7px 7px;" valign="top">
                                        {{--  @if('PROVISIONAL SUMS' == $sub->bqElement->element || 'PROVISIONAL SUM' == $sub->bqElement->element || 'PROV SUM' == $sub->bqElement->element || 'PROV. SUM' == $sub->bqElement->element || 'PROV.SUM' == $sub->bqElement->element)  --}}
                                        @if($matchFound)
                                            {{ money()->toCommon($sub->rate_per_unit ?? "0" , 2) }}
                                        @endif
                                    </td>
                                    <td style="padding: 7px 7px; " align="right" valign="top">
                                        {{--  @if('PROVISIONAL SUMS' == $sub->bqElement->element || 'PROVISIONAL SUM' == $sub->bqElement->element || 'PROV SUM' == $sub->bqElement->element || 'PROV. SUM' == $sub->bqElement->element || 'PROV.SUM' == $sub->bqElement->element)  --}}
                                        @if($matchFound)
                                            {{ money()->toCommon($sub->amount ?? "0" , 2) }}
                                        @endif
                                    </td>
                                </tr>     
                            @endforeach 
                        @endforeach
                                        
                        <?php $total_amount = $total_amount + $sum_element->total ?>
                                        
                        <tr>
                            <td colspan="5" style="padding:10px 10px;"><b>CARRIED TO COLLECTION</b></td>
                            <td align="right">
                                {{--  @if('PROVISIONAL SUMS' == $item->bqElement->element || 'PROVISIONAL SUM' == $item->bqElement->element || 'PROV SUM' == $item->bqElement->element || 'PROV. SUM' == $item->bqElement->element || 'PROV.SUM' == $item->bqElement->element)  --}}
                                @php
                                    $wordsArray = array('PROVISIONAL SUMS','PROVISIONAL SUM', 'PROV SUM', 'PROV. SUM', 'PROV.SUM');
                                    $string = $item->bqElement->element;
                                    $matchFound = "";
                                    $matchFound = preg_match_all(
                                        "/\b(" . implode($wordsArray,"|") . ")\b/i", 
                                        $string, 
                                        $matches
                                    );
                                @endphp
                                @if($matchFound)                                
                                    {{ money()->toCommon($sum_element->total ?? "0" , 2) }}
                                @endif
                            </td>
                        </tr>   
                    </tbody>
                </table>
    
                <div class="breakNow"></div> 
    
                <?php $sum_element = DB::table('bq_subitem')->select(DB::raw('SUM(amount) as total'))->where('acquisition_id','=',$row->acquisition_id)->where('bq_no_element','=',$row->no)->where('deleted_at', '!=', null)->first();?> 
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal; font-weight:bold;'>No. {{$acq->category->name ?? ''}} : {{ $acq->reference ?? ''}}</p>                
                <p class=MsoNormal align=center style="text-transform:uppercase;"><b>{{$acq->title}}</b></p>
                <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;">
                    <thead>
                        <tr>
                            <th style="width: 7%; padding: 10px 10px;">BIL</th>
                            <th>KENYATAAN KERJA</th>
                            <th style="width: 23%; text-align:center;">AMAUN (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td height="820px">&nbsp;</td>
                            <td valign="top">
                                <u><b>KOLEKSI</b></u>
                                <br/><br/>
                                @php
                                
                                    $start = $row->paging_start;
                                    $end = $row->paging_end;
                                    $i = 0;
                                    for($i = $start; $i <= $end; $i++){
                                        echo '&nbsp;&nbsp;&nbsp;Page BQ ' . $i;
                                        echo '<br/><br/>';
                                    }                                
                                @endphp                                
                            </td>
                            <td></td>
                        </tr>                                                
                        <tr>
                            <td colspan="2" style="padding:10px 10px;"><b>TOTAL CARRIED TO SUMMARY</b></td>
                            <td align="right">
                                {{--  @if('PROVISIONAL SUMS' == $row->element || 'PROVISIONAL SUM' == $row->element || 'PROV SUM' == $row->element || 'PROV. SUM' == $row->element || 'PROV.SUM' == $row->element)  --}}
                                @php
                                    $wordsArray = array('PROVISIONAL SUMS','PROVISIONAL SUM', 'PROV SUM', 'PROV. SUM', 'PROV.SUM');
                                    $string = $row->element;
                                    $matchFound = "";
                                    $matchFound = preg_match_all(
                                        "/\b(" . implode($wordsArray,"|") . ")\b/i", 
                                        $string, 
                                        $matches
                                    );
                                @endphp
                                @if($matchFound)                                
                                    {{ money()->toCommon($sum_element->total ?? "0" , 2) }}
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>

            </section>
        @endforeach             
    </body>
</html>