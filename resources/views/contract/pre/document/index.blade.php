@extends('layouts.admin')

@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk Kelulusan Perolehan:</span> {{ $approval->title }}
            <br>
            <span class="font-weight-bold">No. Kelulusan Perolehan:</span> {{ $approval->reference }}
        @endslot
    @endcomponent
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.pre.document.partials.scripts')
            @component('components.forms.hidden-form', 
                [
                    'id' => 'destroy-record-form',
                    'action' => route('acquisition.document.destroy', 'dummy')
                ])
                @slot('inputs')
                    @method('DELETE')
                @endslot
            @endcomponent
            @component('components.card')
                @if(user()->current_role_login == 'penyedia')
                    @slot('card_title')
                        <a href="{{ route('acquisition.document.create',['hashslug'=>$hashslug]) }}" class="btn btn-primary float-right">
                            @icon('fe fe-plus') {{ __('Perolehan Baru') }}
                        </a>
                    @endslot
                @endif
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'perolehan',
                            'param' => 'hashslug=' . $hashslug,
                            'route_name' => 'api.datatable.contract.document',
                            'columns' => [
                                    ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                    ['data' => 'title', 'title' => __('Tajuk Perolehan'), 'defaultContent' => '-'],
                                    ['data' => 'department', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                    ['data' => 'updated_at', 'title' => __('table.updated_at'), 'defaultContent' => '-'],
                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                    __('No. Perolehan'), __('Tajuk Perolehan'),  __('table.created_at'), __('table.updated_at'), __('table.action')
                            ],
                            'actions' => minify(view('contract.pre.document.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection