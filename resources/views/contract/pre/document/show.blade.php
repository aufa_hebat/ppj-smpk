@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript">
        var description_editor;

        jQuery(document).ready(function($) {  
            
            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_ats').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_ats').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_ats').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $("#hideshow1").hide();
            $("#hideshow2").hide();
            $("#hideshow3").hide();
            $('#acquisition_approval_title').val('{{(!empty($approval->reference))? $approval->reference:''}} : {!! str_replace(["\r\n","\r","\n"],"\\n ",$approval->title) !!}');
            $('#acquisition_approval_id').val('{{ $acquisition->acquisition_approval_id }}');
            $('#acquisition_category_id').val('{{ $acquisition->acquisition_category_id }}').prop('disabled',true);
            $('#reference').val('{{ $acquisition->reference }}').prop('readonly',true);
            $('#title').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$acquisition->title) !!}').prop('readonly',true);

            $('#document_price').val('{{ money()->toCommon($acquisition->document_price ?? "0" , 2) }}').prop('readonly',true);
//            $('#gst').val('{{ $acquisition->gst }}').prop('readonly',true);
//            $('#total_document_price').val('{{ money()->toCommon($acquisition->total_document_price ?? "0" , 2) }}').prop('readonly',true);
            
            @if(!empty($acquisition->advertised_at))
                @if(user()->department_id == 9 && user()->executor_department_id == 25 && user()->current_role_login == 'pengesah' && $acquisition->status_task == 'Semakan Dokumen Perolehan')
                    $('#advertised_at').val('{{ $acquisition->advertised_at->format("d/m/Y") }}');
                @else
                    $('#advertised_at').val('{{ $acquisition->advertised_at->format("d/m/Y") }}').prop('readonly',true);
                @endif
            @endif

            @if(!empty($acquisition->extended_closed_sale_at))
            $('#addenda_ext').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$acquisition->extended_closed_sale_at)->format('d/m/Y')  }}');
            @endif
            @if(!empty($acquisition->closed_at))
                $('#addenda_close').val('{{ $acquisition->closed_at->format("d/m/Y") }}').prop('readonly',true);
            @endif            

            @if(!empty($acquisition->start_sale_at))
                @if(user()->department_id == 9 && user()->executor_department_id == 25 && user()->current_role_login == 'pengesah' && $acquisition->status_task == 'Semakan Dokumen Perolehan')
                    $('#start_sale_at').val('{{ $acquisition->start_sale_at->format("d/m/Y") }}');
                @else
                    $('#start_sale_at').val('{{ $acquisition->start_sale_at->format("d/m/Y") }}').prop('readonly',true);
                @endif
            @endif
            @if(!empty($acquisition->closed_at))
                @if(user()->department_id == 9 && user()->executor_department_id == 25 && user()->current_role_login == 'pengesah' && $acquisition->status_task == 'Semakan Dokumen Perolehan')
                    $('#closed_at').val('{{ $acquisition->closed_at->format("d/m/Y") }}');
                @else
                    $('#closed_at').val('{{ $acquisition->closed_at->format("d/m/Y") }}').prop('readonly',true);
                @endif
            @endif
            @if(!empty($acquisition->closed_sale_at))
                @if(user()->department_id == 9 && user()->executor_department_id == 25 && user()->current_role_login == 'pengesah' && $acquisition->status_task == 'Semakan Dokumen Perolehan')
                    $('#closed_sale_at').val('{{ $acquisition->closed_sale_at->format("d/m/Y") }}');
                @else
                    $('#closed_sale_at').val('{{ $acquisition->closed_sale_at->format("d/m/Y") }}').prop('readonly',true);
                @endif
            @endif
            @if(!empty($acquisition->briefing_at))
            $('#briefing_at').val('{{ $acquisition->briefing_at->format("d/m/Y g:i A") }}').prop('readonly',true);
            @endif
            @if(!empty($acquisition->visit_date_time))
            $('#visit_date_time').val('{{ $acquisition->visit_date_time->format("d/m/Y g:i A") }}').prop('readonly',true);
            @endif
            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($s6) && $s6 = $review)
                @if($s6->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6->remarks) !!}');
                @endif
            @elseif(!empty($s7) && $s7 = $review)
                @if($s7->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif
            
            $('#experience_value').val('{{money()->toCommon($acquisition->experience_value ?? "0" , 2)}}').prop('readonly',true);
            $('#experience_duration').val('{{$acquisition->experience_duration}}').prop('readonly',true);

            $('#briefing_status').val('{{ $acquisition->briefing_status }}').prop('readonly',true);
            $('#briefing_location').val('{!! $acquisition->briefing_location !!}').prop('readonly',true);
            $('#visit_status').val('{{ $acquisition->visit_status }}').prop('readonly',true);
            $('#visit_location').val('{!! $acquisition->visit_location !!}').prop('readonly',true);
            $('#document_sale_location').val('{{ $acquisition->document_sale_location }}').prop('readonly',true);
            
            $(".experience").prop('readonly', true);
            $(".briefing").prop('readonly', true);
            $(".visit").prop('readonly', true);
            $(".viewornot").hide();

            //tab remain stay after refresh
            $('#documents-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#documents-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh
            
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    
                    swal('{!! __('Maklumat Perolehan') !!}', response.data.message, 'success');
                    location.reload();
                    
                });
            });
        });
    </script>
@endpush
@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk: </span>{{ $acquisition->approval->title }}
        <br>
        <span class="font-weight-bold">No. Perolehan: </span>{{ $acquisition->reference }}
    @endslot
@endcomponent
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="documents-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#document-details" role="tab" aria-controls="document-details" aria-selected="false">
                    @icon('fe fe-search')&nbsp;{{ __('Perinci') }}
                </a>
            </li>
            
            @if(user()->department_id == '3')
            @else
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-briefing" role="tab" aria-controls="document-briefing" aria-selected="false">
                        @icon('fe fe-message-square')&nbsp;{{ __('Taklimat') }}
                    </a>
                </li>

                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-visit" role="tab" aria-controls="document-visit" aria-selected="false">
                        @icon('fe fe-corner-up-right')&nbsp;{{ __('Lawatan Tapak') }}
                    </a>
                </li>
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-quantity" role="tab" aria-controls="document-quantity" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Senarai Kuantiti') }}
                    </a>
                </li>

                {{--  @if(user()->current_role_login == 'penyedia')  --}}
                @if(user()->id == $acquisition->user_id)
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-quantity-page" role="tab" aria-controls="document-quantity-page" aria-selected="false">
                            @icon('fe fe-file')&nbsp;{{ __('Halaman Cetakan') }}
                        </a>
                    </li>            
                @endif 
            @endif

            @if(!empty($review))
                
                @if(user()->id == $review->created_by)
                    @role('penyedia')
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                                @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                            </a>
                        </li>
                    @endrole
                @else
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                        </a>
                    </li>

                    @if(user()->current_role_login == 'administrator')
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews" role="tab" aria-controls="document-log-reviews" aria-selected="false">
                                @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan') }}
                            </a>
                        </li>
                    @endif
                @endif
                @if((user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'Acquisitions') 
                || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S4') && $review->status != 'Selesai') 
                || (!empty($semakan4) && user()->id == $semakan4->approved_by && user()->id != $review->created_by && (empty($semakan5) || $reviewlog->status != 'S5') && $review->status != 'Selesai') 
                || (!empty($semakan5) && user()->id == $semakan5->approved_by && user()->id != $review->created_by && (empty($cetaknotisbpub) || $reviewlog->status != 'CNBPUB') && $review->status != 'Selesai') 
                || (!empty($semakan6) && user()->id == $semakan6->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S7') && $review->status != 'Selesai') 
                || (!empty($semakan7) && user()->id == $semakan7->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'CNUU') && $review->status != 'Selesai'))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-review" role="tab" aria-controls="document-review" aria-selected="false">
                            @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                        </a>
                    </li>
                @endif
            @endif

            @if(user()->department_id == '3')
            @else
                @if((user()->department_id == 9 && user()->executor_department_id == 25) || (user()->current_role_login == 'developer') || (user()->current_role_login == 'administrator'))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-sales" role="tab" aria-controls="document-sales" aria-selected="false">
                            @icon('fe fe-dollar-sign')&nbsp;{{ __('Jualan') }}
                        </a>
                    </li>
                @endif
                @if(!empty($acquisition) && $acquisition->count() > 0 && !empty($acquisition->extended_closed_sale_at) && $acquisition->extended_closed_sale_at != null)
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-addenda" role="tab" aria-controls="document-addenda" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Addenda') }}
                    </a>
                </li>   
                @endif
            @endif         

            
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                         @component('components.tab.content', ['id' => 'document-details', 'active' => true])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.details', ['acquisition' => $acquisition])
                                <hr>
                                <form id="semakdate-form">
                                    @include('contract.pre.document.partials.forms.date-time', ['acquisition' => $acquisition])
                                </form>
                                @if(user()->department_id == 9 && user()->executor_department_id == 25 && user()->current_role_login == 'pengesah' && $acquisition->status_task == 'Semakan Dokumen Perolehan')
                                    @include('contract.pre.document.partials.forms.submit', [
                                        'hashslug'=> $acquisition->hashslug,
                                        'route_name' => 'api.contract.acquisition.semakdate.update',
                                        'form' => 'semakdate-form',
                                    ])
                                @endif
                                <br>
                                <hr>
                                @include('contract.pre.document.partials.forms.price', ['acquisition' => $acquisition])
                                <hr>
                                @include('contract.pre.document.partials.forms.location', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-briefing'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.briefing', ['acquisition' => $acquisition])
                                @if(!empty($acquisition->briefings) && ((user()->department_id == 9 && user()->executor_department_id == 25) || user()->current_role_login == 'developer' || user()->current_role_login == 'administrator'))
                                <hr>
                                @include('contract.pre.document.partials.forms.brief_list', ['acquisition' => $acquisition])
                                @endif
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-visit'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.visit', ['acquisition' => $acquisition])
                                @if(!empty($acquisition->briefings) && ((user()->department_id == 9 && user()->executor_department_id == 25) || user()->current_role_login == 'developer' || user()->current_role_login == 'administrator'))
                                <hr>
                                @include('contract.pre.document.partials.forms.visit_list', ['acquisition' => $acquisition])
                                @endif
                            @endslot
                        @endcomponent
                        
                        @component('components.tab.content', ['id' => 'document-quantity'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.quantity', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-quantity-page'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.quantity_page', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-addenda'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.addenda', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent                        

                        @if(!empty($acquisition->sales))
                        @component('components.tab.content', ['id' => 'document-sales'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.sales', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent
                        @endif

                        @component('components.tab.content', ['id' => 'document-review'])
                            @slot('content')
                                @include('contract.pre.review.partials.forms.create', ['acquisition' => $acquisition])
                                
                                <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                        @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                    </button>
                                    <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                        @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                    </button>
                                    <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>
                            @endslot
                        @endcomponent

                        <div class="tab-pane fade" id="document-log-reviews" role="tabpanel" aria-labelledby="document-log-reviews-tab">
                            <form id="log-form">
                                <div class="row justify-content-center w-100">
                                    <div class="col-12 w-100">
                                        @include('contract.pre.box.partials.scripts')
                                        @component('components.card')
                                            @slot('card_body')
                                                @component('components.datatable', 
                                                    [
                                                        'table_id' => 'contract-pre-box',
                                                        'route_name' => 'api.datatable.contract.review-log',
                                                        'param' => 'acquisition_id=' . $acquisition->id,
                                                        'columns' => [
                                                            ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                            ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                            ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                            ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                        ],
                                                        'headers' => [
                                                            __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                        ],
                                                        'actions' => minify('')
                                                    ]
                                                )
                                                @endcomponent
                                            @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        @component('components.tab.content', ['id' => 'document-reviews'])
                            @slot('content')

                                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                                    @if(!empty($review))
                                        @if(user()->current_role_login == 'penyedia')
                                            {{-- penyedia --}}
                                            @if($review->created_by == user()->id)

                                                @if(!empty($cnuu) && !empty($cnuu->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                    Ulasan Semakan Oleh {{$cnuu->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl7 as $s7log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s7log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                {{-- @if(!empty($semakan7) && !empty($semakan7->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                                                                    Ulasan Semakan Oleh {{$semakan7->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl6 as $s6log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s6log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif --}}

                                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl5 as $s5log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                {{-- @if(!empty($semakan5) && !empty($semakan5->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                    Ulasan Semakan Oleh {{$semakan5->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl4 as $s4log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan4) && !empty($semakan4->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                    Ulasan Semakan Oleh {{$semakan4->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl3 as $s3log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif  --}}

                                                @if(!empty($semakan3zero) && !empty($semakan3zero->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                    Ulasan Semakan Oleh {{$semakan3zero->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl2 as $s2log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan2) && !empty($semakan2->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                    Ulasan Semakan Oleh {{$semakan2->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl1 as $s1log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif
                                        @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                            {{-- pegawai bpub --}}
                                            @if(user()->department->id == '9')

                                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl5 as $s5log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan5) && !empty($semakan5->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                    Ulasan Semakan Oleh {{$semakan5->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl4 as $s4log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan4) && !empty($semakan4->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                    Ulasan Semakan Oleh {{$semakan4->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl3 as $s3log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif

                                            {{-- pegawai pelaksana --}}
                                            @if(!empty($semakan3zero) && !empty($semakan3zero->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3zero->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2) && !empty($semakan2->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                            {{-- pegawai undang-undang --}}
                                            @if(user()->department->id == '3')

                                                @if(!empty($cnuu) && !empty($cnuu->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                    Ulasan Semakan Oleh {{$cnuu->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl7 as $s7log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s7log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan7) && !empty($semakan7->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                    Ulasan Semakan Oleh {{$semakan7->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl6 as $s6log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s6log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif

                                            {{-- pegawai pelaksana --}}
                                            @if(!empty($semakan3zero) && !empty($semakan3zero->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3zero->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2) && !empty($semakan2->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            {{-- pegawai undang-undang --}}
                                            @if(user()->department->id == '3')

                                                @if(!empty($cnuu) && !empty($cnuu->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                    Ulasan Semakan Oleh {{$cnuu->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl7 as $s7log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s7log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan7) && !empty($semakan7->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                    Ulasan Semakan Oleh {{$semakan7->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl6 as $s6log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s6log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif

                                            {{-- pegawai bpub --}}
                                            @if(user()->department->id == '9')

                                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl5 as $s5log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan5) && !empty($semakan5->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                    Ulasan Semakan Oleh {{$semakan5->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl4 as $s4log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan4) && !empty($semakan4->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                    Ulasan Semakan Oleh {{$semakan4->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl3 as $s3log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                
                                                @if(!empty($semakan3zero) && !empty($semakan3zero->requested_by))
                                                    @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                        Ulasan Semakan Oleh {{$semakan3zero->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl2 as $s2log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif

                                            @endif

                                            {{-- pegawai pelaksana --}}
                                            @if(!empty(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department) && $review->create->department->id == user()->department->id)
                                                
                                                @if(!empty($semakan3zero) && !empty($semakan3zero->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                    Ulasan Semakan Oleh {{$semakan3zero->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl2 as $s2log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if(!empty($semakan2) && !empty($semakan2->requested_by))
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                    Ulasan Semakan Oleh {{$semakan2->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl1 as $s1log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endif
                                        @endif
                                    @endif
                                
                                </div>

                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('acquisition.document.index', ['hashslug'=>$approval->hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>

                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection