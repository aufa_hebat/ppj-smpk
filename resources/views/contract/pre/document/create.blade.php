@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {
//            $('#reference').val('{{ user()->getAcquisitionReferenceCode($cnt,$approval->year) }}');
            $('#document_sale_location').val('Kaunter Jualan Sebut Harga Blok C Aras 1 Bahagian Perolehan dan Ukur Bahan, Jabatan Kewangan Perbadanan Putrajaya');
            $('#acquisition_approval_title').val('{{(!empty($approval->reference))? $approval->reference:''}} : {!! str_replace(["\r\n","\r","\n"],"\\n ",$approval->title) !!}');
            $(".viewornotprint").hide();

            $('#acquisition_category_id').change(function() {
                
            });
        });
    </script>
@endpush

@section('content')
@include('components.forms.assets.datetimepicker')
<div class="row">
    <div class="col-12">
        <form method="POST" action="{{ route('acquisition.document.store') }}">
            @csrf 
            <input type="hidden" name="hashslug" id="hashslug" value="{{ $hashslug }}"/>
            <input type="hidden" name="acquisition_approval_id" 
                id="acquisition_approval_id" 
                value="{{ $approval->hashslug }}"/>
            @component('components.card')
                @slot('card_body')
                    @include('contract.pre.document.partials.forms.details')
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('acquisition.document.index', ['hashslug'=>$hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Batal') }}
                        </a>

                        @if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
                            <button type="submit" class="btn btn-primary">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                            </button>
                        @endif
                    </div>
                @endslot
                @endslot
            @endcomponent
        </form>
    </div>
</div>
@endsection