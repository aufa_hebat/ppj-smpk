@component('components.actions_documents')
	@slot('append_action')
	
		<a class="dropdown-item document-action-btn" style="cursor: pointer; display: '+data.{{'display_a'}}+'"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			@icon('fe fe-file') {{ __('Addenda') }} 
		</a>
		@role('urusetia')
		<a class="dropdown-item cancel-action-btn" style="cursor: pointer;"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			@icon('fas fa-ban') {{ __('Pembatalan Perolehan') }}
		</a>
		@endrole
	@endslot
@endcomponent