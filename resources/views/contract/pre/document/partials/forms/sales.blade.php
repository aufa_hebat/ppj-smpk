<div class="row">
    <div class="col-12">
        <h5>Senarai Syarikat yang Membeli Dokumen</h5>
        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th style="width: 10px">Bil</th>
                <th>Nama Syarikat</th>
                <th>Status Pembeli</th>
                <th>Nama Wakil/Pemilik</th>
                <th>No. Kad Pengenalan</th>
            </tr>
            </thead>
            <tbody>
                @foreach($acquisition->sales as $key => $jual)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$jual->company->company_name}}</td>
                        <td>
                            @if($jual->buyer_status == 1)
                                Pemilik
                            @elseif($jual->buyer_status == 2)
                                Wakil
                            @endif
                        </td>
                        <td>
                            @if($jual->buyer_status == 1)
                                {{ $jual->company->owners->pluck('name')->first() }}
                            @elseif($jual->buyer_status == 2)
                                {{ $jual->company->agents->where('ic_number', $jual->ic_number)->pluck('name')->first() }}
                            @endif
                        </td>
                        <td>{{ $jual->ic_number }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="btn-group float-right">
    <a href="{{ route('sale_list',['hashslug' => $acquisition->hashslug]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Jualan') }}
    </a>
    <a href="{{ route('sale_folder',['hashslug' => $acquisition->hashslug]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Folder Jualan') }}
    </a>
    <a href="{{ route('sale_folder_verify',['hashslug' => $acquisition->hashslug]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Semak') }}
    </a>
</div>