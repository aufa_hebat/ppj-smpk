<div class="row">
    <div class="col-12">
        <h4>Syarikat Yang Hadir Lawatan Tapak</h4>
        @if(!empty($acquisition->doc_sitevisit))
            <div class="col-12">
                Lampiran : 
                    <a href="/download/{{ $acquisition->doc_sitevisit->document_path }}/{{ $acquisition->doc_sitevisit->document_name }}" target="_blank">{{ $acquisition->doc_sitevisit->document_name }}</a>
            </div>
        <br>
        @endif
        
        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th style="width: 10px">Bil</th>
                <th>Nama Syarikat</th>
                <th>Status Hadir</th>
                <th>Nama Wakil/Pemilik</th>
                <th>No. Kad Pengenalan</th>
                <th>Pegawai Bertugas</th>
            </tr>
            </thead>
            <tbody>
                @foreach($acquisition->briefings as $key => $briefing)
                    @if($briefing->attendant_status != 3 && $briefing->site_visit == 1)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$briefing->company->company_name}}</td>
                            <td>
                                @if($briefing->attendant_status == 1)
                                    Pemilik
                                @elseif($briefing->attendant_status == 2)
                                    Wakil
                                @endif
                            </td>
                            <td>
                                @if($briefing->attendant_status == 1)
                                    {{ $briefing->owner->name }}
                                @elseif($briefing->attendant_status == 2)
                                    {{ $briefing->agent->name }}
                                @endif
                            </td>
                            <td>
                                @if($briefing->attendant_status == 1)
                                    {{ $briefing->owner->ic_number }}
                                @elseif($briefing->attendant_status == 2)
                                    {{ $briefing->agent->ic_number }}
                                @endif
                            </td>
                            <td>
                                @if(!empty($briefing->visit_staff))
                                {{ $briefing->visit_staff->name }}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>