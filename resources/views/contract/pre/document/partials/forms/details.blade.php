<h5>Perinci</h5>
<div class="btn-group float-right viewornotprint">
    <a href="{{ route('acq_notice',['hashslug' => $acquisition->hashslug ?? 0,'np_dept' =>$np_dept->name ?? '', 'np_honor' => $np_dept->honourary ?? '']) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Notis') }}
    </a>
    <a href="{{ route('acq_bq',['hashslug' => $acquisition->hashslug ?? 0]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Kuantiti') }}
    </a>
</div>
@include('components.forms.textarea', [
    'input_label' => __('Tajuk Kelulusan Perolehan'),
    'id' => 'acquisition_approval_title',
    'name' => 'acquisition_approval_title',
    'style' => 'height:100px; text-transform: uppercase;',
    'readonly' => true,
])
<a href="{{ route('contract.pre.show', $acquisition->approval->hashslug ?? $approval->hashslug) }}" class="btn btn-primary float-right" target="_blank" >Kelulusan Perolehan</a>
@include('components.forms.textarea', [
    'input_label' => __('Tajuk Perolehan'),
    'id' => 'title',
    'name' => 'title',
    'style' => 'height:100px; text-transform: uppercase;',
    'required' => true,
]) 
@include('components.forms.select', [
    'input_label' => __('Kategori Perolehan'),
    'options' => $acq_cat,
    'id' => 'acquisition_category_id',
    'name' => 'acquisition_category_id',
    'required' => true,
])
@include('components.forms.input', [
    'input_label' => __('No Perolehan'),
    'id' => 'reference',
    'name' => 'reference',
    'readonly' => true,
])