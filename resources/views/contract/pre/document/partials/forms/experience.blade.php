<h5>Pengalaman</h5>
<div class="float-right viewornot">
    @include('components.forms.switch', [
        'id' => 'experience_status', 
        'name' => 'experience_status', 
        'label' => 'Pengalaman',
        'checked' => old('experience_status', (!empty($acquisition))? $acquisition->experience_status:false)
    ])
</div>
@include('components.forms.input', [
    'input_label' => __('Keupayaan dalam menyiapkan kerja-kerja yang berkaitan bernilai sekurang-kurangya RM'),
    'id' => 'experience_value',
    'name' => 'experience_value',
    'input_classes' => 'experience money',
    'type' 			=> 'text',
])

<div class="row">
    <div class="col-4">
        @include('components.forms.select', [
            'input_label' => __('kategori'),
            'id' => 'experience',
            'name' => 'experience',
            'input_classes' => 'experience_flag',
            'options' => ['1' => 'bilangan', '2' => 'terkumpul'],
        ])
    </div>
</div>
<div class="row" id="bilangan">
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => __('bagi  '),
            'id' => 'experience_duration',
            'name' => 'experience_duration',
            'input_classes' => 'experience',
        ])
    </div>
</div>
<div class="row" id="terkumpul">
    <div class="col-4">
        
    </div>
</div>
<span> kontrak dalam tempoh (5) tahun terdahulu </span>