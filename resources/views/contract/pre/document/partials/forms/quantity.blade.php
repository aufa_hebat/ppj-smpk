@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
<script type="text/javascript">
        jQuery(document).ready(function($) {  
            $(document).on('click', '.submit-muat-naik', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var form = document.forms[form_id];
                var data = new FormData(form);
                axios.post(route(route_name, id), data).then(response => { 
                    swal('{!! __('Maklumat Perolehan') !!}', 'Senarai Kuantiti berjaya dimuatnaik.', 'success');
                    location.reload();
                });
            });
        });
</script>
@endpush
<h5>Senarai Kuantiti</h5>
<form id="muat_naik" class="form-horizontal" method="POST" action="{{ route('import.parse') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="checkbox" name="header" checked hidden> 
    <input type="text" name="acquisition_id" id="acquisition_id" value="{{ $acquisition->id }}" hidden>
    <input type="text" name="hashslug" id="hashslug" value="{{ $acquisition->hashslug }}" hidden>
    <div id="hideshow1" class="row">
        <div class="col-10 input-group">
            <input id="subFile" type="text"  class="form-control" readonly>
            <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile" name="csv_file" style="display: none" accept=".csv">                                      
        </div>
        <div  class="col-2">
            <button type="submit" class="btn btn-primary submit-muat-naik" 
                    data-form="muat_naik" data-route="import.parse">
                Muat Naik
            </button>
        </div>
        
    </div>
    <small id="hideshow2" class="float-right"><b>Format : .csv &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    <div id="hideshow3" class="form-group">
        <div class="col-md-8 col-md-offset-4">
            Sila gunakan <strong><a href="{{ asset('storage/bq_sample.xls') }}">lampiran</a></strong> ini
        </div>
    </div>
</form>
@if(!empty($BQElemen))
<div id="table" class="table-editable table-bordered border-0">
    @php
        $sumBq = 0;
    @endphp

    @foreach($BQElemen->sortBy('no') as $bq)
        @php $sumBq = $sumBq + $bq->amount; @endphp
    <div class="mb-0 card" >
        <div class="card-header" role="tab" id="heading{{ $bq->id }}">
            <h4 class="card-title cardElemen">
                <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{ $bq->id }}" href="#collapse{{ $bq->id }}" aria-expanded="true" aria-controls="collapse{{ $bq->id }}">
                    <i class="mr-2 fa fa-folder-open"></i><u>{{ $bq->no }}&nbsp;:&nbsp;{{ $bq->element }} &nbsp;&nbsp;&nbsp;
                        {{--  @if(user()->id == $bq->acquisition->user_id || (user()->executor_department_id == 25 && user()->hasRole('pengesah') == true)  --}}
                        @if(user()->id == $bq->acquisition->user_id || ((user()->executor_department_id == 25 && user()->current_role_login == 'pengesah') == true)
                        || user()->id == $acquisition->user->supervisor->id || user()->id == $acquisition->user->supervisor->supervisor->id)
                        RM{{money()->toCommon($bq->amount ?? 0,2 ) }}
                        @endif
                    </u>
                </a>
            </h4>
        </div>
        <div id="collapse{{ $bq->id }}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $bq->id }}" >
            <div class="card-body">
                <div class="row">
                    <div id="semakanBq" class="col-md-12 table-responsive">
                        <p>@if(!empty($bq->description)) <u>{{ $bq->description }}</u> @endif</p>
                        <table class="table1  table-no-border" width="100%" cellspacing="0">
                            @foreach($bq->bqItems->sortBy('no') as $it)                             
                            <thead >
                                <th style="width:100%;border:none" >
                                    <br>
                                    <h5>{{ $it->bq_no_element }}.{{ $it->no }}&nbsp;:&nbsp;{{ $it->item }} &nbsp;&nbsp;&nbsp;
                                        {{--  @if(user()->id == $bq->acquisition->user_id || (user()->executor_department_id == 25 && user()->hasRole('pengesah') == true)  --}}
                                        @if(user()->id == $bq->acquisition->user_id || ((user()->executor_department_id == 25 && user()->current_role_login == 'pengesah') == true)
                                        || user()->id == $acquisition->user->supervisor->id || user()->id == $acquisition->user->supervisor->supervisor->id)
                                        RM{{money()->toCommon($it->amount ?? 0,2 ) }}
                                        @endif
                                    </h5><br>
                                    <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                </th>
                            </thead>
                            <tbody class="list">
                                <tr style="width:100%;border:none">
                                    <td class="ulasanBQs">
                                        <table id="ulasan_bq{{ $it->id }}" class="table table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <th style="width:5%">NO SUB ITEM</th>
                                                <th style="width:30%">SUB ITEM</th>
                                                <th style="width:25%">KETERANGAN</th>
                                                <th style="width:10%">UNIT</th>
                                                <th style="width:10%">KUANTITI</th>
                                                <th style="width:10%">HARGA SEUNIT</th>
                                                <th style="width:10%">AMAUN</th>
                                            </thead>
                                            <tbody>
                                            @foreach($it->bqSubItems->sortBy('no') as $key=>$bqs)
                                            <tr>
                                                <td>{{ $bqs->bq_no_element }}.{{ $bqs->bq_no_item }}.{{ $bqs->no }}</td>
                                                <td>{{ $bqs->item }}</td>
                                                <td>{{ $bqs->description }}</td>
                                                <td>{{ $bqs->unit }}</td>
                                                <td>
                                                    @if($bqs->quantity > 0 || $bqs->quantity < 0)
                                                        @php $quan = strlen($bqs->quantity); @endphp
                                                        {{ substr($bqs->quantity, 0, ($quan - 2) ) }}
                                                    @else
                                                        0
                                                    @endif
                                                </td>
                                                <td>
                                                    {{--  @if(user()->id == $bqs->acquisition->user_id || (user()->executor_department_id == 25 && user()->hasRole('pengesah') == true)   --}}
                                                    @if(user()->id == $bqs->acquisition->user_id || ((user()->executor_department_id == 25 && user()->current_role_login == 'pengesah') == true)
                                                    || user()->id == $acquisition->user->supervisor->id || user()->id == $acquisition->user->supervisor->supervisor->id)
                                                    {{ money()->toCommon($bqs->rate_per_unit ?? 0,2 ) }}
                                                    @endif
                                                </td>
                                                {{--  <td>@if(user()->id == $bqs->acquisition->user_id || (user()->executor_department_id == 25 && user()->hasRole('pengesah') == true)  --}}
                                                <td>
                                                        @if(user()->id == $bqs->acquisition->user_id || ((user()->executor_department_id == 25 && user()->current_role_login == 'pengesah') == true)
                                                    || user()->id == $acquisition->user->supervisor->id || user()->id == $acquisition->user->supervisor->supervisor->id)
                                                    {{ money()->toCommon($bqs->amount ?? 0,2 ) }}
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        @push('scripts')
                                        <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function($) {
                                                $("#ulasan_bq{{ $it->id }}").DataTable({
                                                    order: false
                                                });
                                            });
                                        </script>
                                        @endpush
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

    <div class="mb-0 card" >
        <div class="card-header">
            <h4 class="card-title cardElemen">
                <i class="mr-2 fa fa-folder-open"></i>
                &nbsp;&nbsp;&nbsp;&nbsp;Jumlah Keseluruhan : &nbsp;&nbsp;&nbsp;
                {{--  @if(user()->id == $acquisition->user_id || (user()->executor_department_id == 25 && user()->hasRole('pengesah') == true)  --}}
                @if(user()->id == $acquisition->user_id || ((user()->executor_department_id == 25 && user()->current_role_login == 'pengesah') == true)
                || user()->id == $acquisition->user->supervisor->id || user()->id == $acquisition->user->supervisor->supervisor->id)
                {{ money()->toHuman($sumBq ?? 0,2 ) }}
                @endif
            </h4>
        </div>
    </div>

</div>
@endif