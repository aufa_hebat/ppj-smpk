<h5>Tarikh dan Masa</h5>
<div class="row">
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Notis'),
            'id' => 'advertised_at',
            'name' => 'advertised_at',
            'required' => true,
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Tutup Perolehan'),
            'id' => 'closed_at',
            'name' => 'closed_at',
            'required' => true,
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])     
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mula Jualan'),
            'id' => 'start_sale_at',
            'name' => 'start_sale_at',
            'required' => true,
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Tutup Jualan'),
            'id' => 'closed_sale_at',
            'name' => 'closed_sale_at',
            'required' => true,
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
</div>