
<div class="alert alert-danger text-center" id="brief_alert" style="display:none">
    @icon('fe fe-alert-octagon') Tarikh taklimat kurang 5 hari dari tarikh notis
</div>
<h5>Taklimat</h5>
<div class="float-right viewornot">
    @include('components.forms.switch', [
        'id' => 'briefing_status', 
        'name' => 'briefing_status', 
        'label' => 'Taklimat',
        'checked' => old('briefing_status', (!empty($acquisition))? $acquisition->briefing_status: false)
    ])
</div>

@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Dan Masa Taklimat'),
    'id' => 'briefing_at',
    'name' => 'briefing_at',
    'input_classes' => 'briefing',
    'config' => [
        'format' => config('datetime.display.date_time'),
        'sideBySide' => true,
        'inline'    => true,
    ]
])
@include('components.forms.input', [
    'input_label' => __('Tempat Taklimat'),
    'id' => 'briefing_location',
    'name' => 'briefing_location',
    'input_classes' => 'briefing',
])