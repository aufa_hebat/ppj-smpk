<h5>Harga</h5>
<div class="row">
	<div class="col-12">
    @include('components.forms.input', [
            'input_label' 	=> __('Harga Dokumen (RM)'),
            'id' 			=> 'document_price',
            'name' 			=> 'document_price',
            'type' 			=> 'text',
            'input_classes'	=> 'money',
    ])
	</div>
<!--	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => __('SST%'),
			'id' => 'gst',
			'name' => 'gst',
                        'type' => 'number',
                        'step' => '0',
                        'onkeyup' => 'calculate()',
		])
	</div>
    <input type="number" class="form-control input-lg" step="0.01"  id="gst_price" name="gst_price" hidden>
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => __('Jumlah Harga Dokumen(RM)'),
			'id' => 'total_document_price',
			'name' => 'total_document_price',
            'type' => 'text',
            'step' => '0.01',
            'onkeyup' => 'calculate()',
            'readonly' => 'readonly',
			'input_classes'	=> 'money',
		])
	</div>-->
</div>