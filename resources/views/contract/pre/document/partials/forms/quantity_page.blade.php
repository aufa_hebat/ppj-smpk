@push('styles')
<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            @if(count($BQElemen) > 0)
                @foreach($BQElemen as $elemen)
                    $('#pagestart-{{ $elemen->id }}').val(' {{ $elemen->paging_start }} ');
                    $('#pageend-{{ $elemen->id }}').val(' {{ $elemen->paging_end }} ');
                @endforeach
            @endif

            @if(count($BQItem) > 0)
                @foreach($BQItem as $item)
                    $('#pageitemstart-{{ $item->id }}').val(' {{ $item->paging_start }} ');
                    $('#pageitemend-{{ $item->id }}').val(' {{ $item->paging_end }} ');
                @endforeach
            @endif

            // UPDATE MUKA SURAT
            $(document).on('click', '.save-page-bq-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->id }}';
                var hashslug = '{{ $acquisition->hashslug }}';
                var route_name = 'api.contract.acquisition.bq-paging';
                var form_id = 'page_bq_form';
                var data = $('#' + form_id).serialize();
    
                axios.post(route(route_name, id), data).then(response => {
                    swal('Halaman Cetakan Senarai Kuantiti', response.data.message, 'success');
                });
            });            
        });
    </script>
@endpush

<h5>Halaman Cetakan Senarai Kuantiti</h5>
<div class="btn-group float-right">
    <a href="{{ route('acq_bq',['hashslug' => $acquisition->hashslug ?? 0]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Kuantiti') }}
    </a>
</div>
<form id="page_bq_form">
    <input type="hidden" name="acquisition_id" id="acquisition_id" value="{{ $acquisition->id }}">
    <table id="table" class="table-bordered" width="100%">
        <thead>
            <tr>
                <td width="6%">No.</td>
                <td width="70%">Elemen</td>
                {{--  <td>Jumlah</td>  --}}
                <td width="12%">Dari M/S</td>
                <td width="12%">Hingga M/S</td>
            </tr>
        </thead>
        <tbody>
            @if(!empty($BQElemen))
                @foreach($BQElemen as $key => $bq)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $bq->element }}</td>
                        {{--  <td>{{ money()->toCommon($bq->amount ?? 0,2 ) }}</td>  --}}
                        <td>
                            @include('components.forms.input', [
                                'input_label' => '',
                                'id' => 'pagestart-'.$bq->id,
                                'name' => 'pageStart[' . kebab_case($bq->id) .']',
                            ])                            
                        </td>
                        <td>
                            @include('components.forms.input', [
                                'input_label' => '',
                                'id' => 'pageend-'.$bq->id,
                                'name' => 'pageEnd[' . kebab_case($bq->id) .']',
                            ])                            
                        </td>                        
                    </tr>
                    {{--  @foreach($BQItem as $it)  
                        @if($bq->no == $it->bq_no_element && $bq->acquisition_id == $it->acquisition_id)   
                            <tr>
                                <td>&nbsp;&nbsp;{{$it->bq_no_element}}.{{$it->no}}</td>
                                <td>{{$it->item}}</td>
                                <td>
                                    @include('components.forms.input', [
                                        'input_label' => '',
                                        'id' => 'pageitemstart-'.$it->id,
                                        'name' => 'pageItemStart[' . kebab_case($it->id) .']',
                                    ])                            
                                </td>
                                <td>
                                    @include('components.forms.input', [
                                        'input_label' => '',
                                        'id' => 'pageitemend-'.$it->id,
                                        'name' => 'pageItemEnd[' . kebab_case($it->id) .']',
                                    ])                            
                                </td>                                
                            </tr>
                        @endif
                    @endforeach  --}}
                @endforeach
            @else
                <tr><td colspan="4" style="text-align:center;">Tiada Maklumat</td></tr>
            @endif
        </tbody>
    </table>
    <div class="btn-group float-right">
        <button type="submit" class="float-right btn btn-primary save-page-bq-action-btn">
            @icon('fe fe-save') {{ __('Simpan') }}
        </button>
    </div>    
</form>
