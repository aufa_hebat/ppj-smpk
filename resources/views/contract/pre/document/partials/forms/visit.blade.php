<h5>Lawatan Tapak</h5>
<div class="float-right viewornot">
    @include('components.forms.switch', [
        'id' => 'visit_status', 
        'name' => 'visit_status', 
        'label' => 'Lawatan Tapak',
        'checked' => old('visit_status', (!empty($acquisition))? $acquisition->visit_status:false)
    ])
</div>
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Dan Masa Lawatan Tapak'),
    'id' => 'visit_date_time',
    'name' => 'visit_date_time',
    'input_classes' => 'visit',
    'config' => [
        'format' => config('datetime.display.date_time'),
        'sideBySide' => true,
        'inline'    => true,
    ]
])
@include('components.forms.input', [
    'input_label' => __('Tempat Lawatan Tapak'),
    'id' => 'visit_location',
    'name' => 'visit_location',
    'input_classes' => 'visit',
])