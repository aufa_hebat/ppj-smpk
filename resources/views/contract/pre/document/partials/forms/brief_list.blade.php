<div class="row">
    <div class="col-12">
        <h5>Syarikat Yang Hadir Taklimat</h5>
        <a href="{{ route('brief_attendance',['hashslug' => $acquisition->hashslug]) }}" target="_blank"
                class="btn btn-success border-success float-right">
                @icon('fe fe-printer'){{ __(' Cetak') }}
        </a>
        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th style="width: 10px">Bil</th>
                <th>Nama Syarikat</th>
                <th>Status Hadir</th>
                <th>Nama Wakil/Pemilik</th>
                <th>No. Kad Pengenalan</th>
            </tr>
            </thead>
            <tbody>
                @php $key = 0; @endphp
                @foreach($acquisition->briefings as $briefing)
                    @if($briefing->attendant_status != 3)
                        @php $key++; @endphp
                        <tr>
                            <td>{{$key}}</td>
                            <td>{{$briefing->company->company_name}}</td>
                            <td>
                                @if($briefing->attendant_status == 1)
                                    Pemilik
                                @elseif($briefing->attendant_status == 2)
                                    Wakil
                                @endif
                            </td>
                            <td>
                                @if($briefing->attendant_status == 1)
                                    {{ $briefing->owner->name }}
                                @elseif($briefing->attendant_status == 2)
                                    {{ $briefing->agent->name }}
                                @endif
                            </td>
                            <td>
                                @if($briefing->attendant_status == 1)
                                    {{ $briefing->owner->ic_number }}
                                @elseif($briefing->attendant_status == 2)
                                    {{ $briefing->agent->ic_number }}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>