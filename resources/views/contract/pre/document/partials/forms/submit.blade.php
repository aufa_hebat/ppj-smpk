<div class="btn-group float-right">
    
	{{ html()->a(route('acquisition.document.index',['hashslug'=>$hashslug]), __('Kembali'))->class('btn btn-default border-primary') }}
	
	@if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
		@isset($route_name)
		<button type="submit" class="btn btn-primary float-middle submit-action-btn" 
			data-route="{{ $route_name }}"
			data-form="{{ $form }}">
			@icon('fe fe-save') {{ __('Simpan') }}
		</button>
		@endisset
		@isset($route_send)
		<button class="btn btn-default float-middle border-default" 
			data-route="{{ $route_send }}"
			data-form="{{ $form_send }}">
			@icon('fe fe-send') {{ __('Teratur') }}
		</button>
		@endisset
	@endif
	
</div>