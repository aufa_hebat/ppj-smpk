@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('acquisition.document.show', id));
		});
		$(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('acquisition.document.edit', id));
		});
        $(document).on('click', '.document-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('doc.addenda.index', {hashslug:id}));
		});
    $(document).on('click', '.file-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('acquisition.document.edit', id));
		});
		$(document).on('click', '.review-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('acquisition.review.show', id));
		});
		$(document).on('click', '.cancel-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('acquisition.cancel.edit', id));
		});
		$(document).on('click', '.destroy-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: '{!! __('Amaran') !!}',
			  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: '{!! __('Ya') !!}',
			  cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  if (result.value) {
			  	$('#destroy-record-form').attr('action', route('acquisition.document.destroy', id))
				$('#destroy-record-form').submit();
			  }
			});
		});
	});
</script>
@endpush