@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/previous_next.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {  

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });
            $('.money').mask('000,000,000.00', {reverse: true});

            $('.select2').select2();
            $('#acquisition_approval_title').val('{{(!empty($approval->reference))? $approval->reference:''}} : {!! str_replace(["\r\n","\r","\n"],"\\n ",$approval->title) !!}');
            $('#acquisition_approval_id').val('{{ $acquisition->acquisition_approval_id }}');
            $('#acquisition_category_id').val('{{ $acquisition->acquisition_category_id }}');
            $('#acquisition_category_id').trigger('change');
            $('#reference').val('{{ $acquisition->reference }}');
            $('#title').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$acquisition->title) !!}');
            $('#document_sale_location').val('{{ $acquisition->document_sale_location }}');
            
            $('#document_price').val('{{ money()->toCommon($acquisition->document_price ?? "0" , 2) }}');
//            $('#gst').val('{{ $acquisition->gst }}');
//            $('#gst_price').val('{{ money()->toCommon($acquisition->gst_price ?? "0" , 2) }}');
//            $('#total_document_price').val('{{ money()->toCommon($acquisition->total_document_price ?? "0" , 2) }}');
            $(".viewornotprint").hide();
            
            $('#datetimepicker-advertised_at').datetimepicker({ format:'DD/MM/YYYY', useCurrent : false });
            $('#datetimepicker-start_sale_at').datetimepicker({ format:'DD/MM/YYYY', useCurrent : false });
            $('#datetimepicker-closed_at').datetimepicker({ format:'DD/MM/YYYY', useCurrent : false });
            $('#datetimepicker-closed_sale_at').datetimepicker({ format:'DD/MM/YYYY', useCurrent : false });
           
            
            @if(!empty($acquisition->advertised_at))
            $('#advertised_at').val('{{ $acquisition->advertised_at->format("d/m/Y") }}');
            @endif
            @if(!empty($acquisition->start_sale_at))
            $('#start_sale_at').val('{{ $acquisition->start_sale_at->format("d/m/Y") }}');
            @endif
            @if(!empty($acquisition->closed_at))
            $('#closed_at').val('{{ $acquisition->closed_at->format("d/m/Y") }}');
            @endif
            @if(!empty($acquisition->closed_sale_at))
            $('#closed_sale_at').val('{{ $acquisition->closed_sale_at->format("d/m/Y") }}');
            @endif
            @if(!empty($acquisition->briefing_at))
            $('#briefing_at').val('{{ $acquisition->briefing_at->format("d/m/Y g:i A") }}');
            @endif
            @if(!empty($acquisition->visit_date_time))
            $('#visit_date_time').val('{{ $acquisition->visit_date_time->format("d/m/Y g:i A") }}');
            @endif
                       
            $('#datetimepicker-briefing_at').datetimepicker({ format:'DD/MM/YYYY hh:mm:ss A',inline:true, sideBySide: true, useCurrent : false  });
            $('#datetimepicker-visit_date_time').datetimepicker({ format:'DD/MM/YYYY hh:mm:ss A',inline:true, sideBySide: true, useCurrent : false });
            
            $('#acquisition_category_id').change(function() {
                
            });
            
            //onchange bile start sale
            $("#datetimepicker-start_sale_at").on("change.datetimepicker", function (selected) {
                var cnt_closed = 0;// count for tarikh tutup sebut harga
                var cnt_sale_end = 0;// count for tarikh tutup jualan
                var cnt_sale_tender = '{{ substr_count($acquisition->category->name, 'Tender') }}';
            
                if(cnt_sale_tender > 0){
                    cnt_closed = 22;
                    //cnt_sale_end = 15;//15 hari termasuk hari jual
                    cnt_sale_end = 14; // 15 hari termasuk hari jual (15 - 1)
                }else{
                    @if(config('enums.Sebut Harga') == $acquisition->category->id || config('enums.Sebut Harga B') == $acquisition->category->id)
                        cnt_closed = 8;
                        //cnt_sale_end = 4;//4 hari termasuk hari jual
                        cnt_sale_end = 3;//4 hari termasuk hari jual (4 - 1)
                    @else
                        cnt_closed = 4;
                        //cnt_sale_end = 3;//3 hari termasuk hari jual
                        cnt_sale_end = 2;//3 hari termasuk hari jual (2 - 1)
                    @endif
            
                }
                //bace bile baru buka after tambah perolehan baru
                if(selected.date == undefined){
                    var start_sale_at, start_sale_at_mmt, minDate, minDate_end, closed_at, closed_at_mmt;

                    @if(!empty($acquisition->start_sale_at))
                        start_sale_at = "{{ $acquisition->start_sale_at->format("d/m/Y") }}";
                        start_sale_at_mmt = moment(start_sale_at,'DD/MM/YYYY'); 
                        minDate = moment(start_sale_at_mmt).add(cnt_closed, 'days');
                        minDate_end = moment(start_sale_at_mmt).add(cnt_sale_end, 'days');
                        
                        $('#datetimepicker-closed_sale_at').datetimepicker('minDate', minDate_end);
                        $('#datetimepicker-closed_at').datetimepicker('minDate', minDate);
                    @endif

                    @if(!empty($acquisition->closed_at))
                        closed_at = "{{ $acquisition->closed_at->format("d/m/Y") }}";
                        closed_at_mmt = moment(closed_at,'DD/MM/YYYY');
                        
                        $('#datetimepicker-closed_sale_at').datetimepicker('maxDate',closed_at_mmt);
                    @endif

                    
                }else{
                    
                //bace bile baru buka after untuk kemaskini perolehan dan mase onclick calendar start jualan
                    var check_sale   = null;
                    var check_date   = moment(selected.date).format('DD/MM/YYYY');
                    var newDT        = moment(selected.date).format('DD/MM/YYYY hh:mm:ss A');
                    
                    @if(!empty($acquisition->start_sale_at))
                        check_sale = "{{ $acquisition->start_sale_at->format("d/m/Y") }}";
                    @endif
                    //only onchange start sale xsame dengan previous save date
                    if(check_sale != null && check_sale != check_date){
                        $('#briefing_at').val(newDT);
                        $('#visit_date_time').val(newDT);
                        $('#datetimepicker-visit_date_time').val(newDT);
                    }
                    
//                    clearClosedSaleAt(selected.date);
                    
                    var minDate1 = moment(selected.date).add(cnt_closed, 'days');
                    var minDate_end1 = moment(selected.date).add(cnt_sale_end, 'days');
                    var newDC = moment(selected.date).add(cnt_sale_end,'days').format('DD/MM/YYYY');
                    var newDat = moment(selected.date).add(cnt_closed,'days').format('DD/MM/YYYY');
                    $('#closed_sale_at').val(newDC);
                    $('#closed_at').val(newDat);
                    $('#datetimepicker-closed_at').datetimepicker('minDate', minDate1);
                    $('#datetimepicker-closed_sale_at').datetimepicker('minDate', minDate_end1);
                }
                
            });
            //onchange tarikh tutup perolehan
            $("#datetimepicker-closed_at").on("change.datetimepicker", function (selected) {
                if(selected.date != undefined){
//                    clearClosedAt(selected.date);
                    $('#datetimepicker-closed_sale_at').datetimepicker('maxDate', selected.date);
                }               
            });
            
            $('#experience_value').val('{{ money()->toCommon($acquisition->experience_value ?? "0", 2) }}');
            $('#experience_duration').val('{{ $acquisition->experience_duration }}');
            $('#experience').val('{{ $acquisition->experience }}').change();
            $('#experience_status').val('{{ $acquisition->experience_status ?? "0" }}');

            $('#briefing_status').val('{{ $acquisition->briefing_status ?? "0" }}');
            $('#briefing_location').val('{{ $acquisition->briefing_location }}');
            $('#visit_status').val('{{ $acquisition->visit_status ?? "0" }}');
            $('#visit_location').val('{{ $acquisition->visit_location }}');


            
            $(document).on('change', '#experience_status', function(event) {
                event.preventDefault();
                $('.experience').prop('readonly', !this.checked);
                $('.experience_flag').prop('disabled', !this.checked);
            });

            $(document).on('change', '#experience', function(event){
                event.preventDefault();

                if(this.value == '2'){
                    bilangan.hidden = true;
                    terkumpul.hidden = false;
                }
                else if(this.value == '1'){
                    bilangan.hidden = false;
                    terkumpul.hidden = true;
                }                
            });
            // $('#experience').change(function() {
            //     var bilangan = document.getElementById('bilangan');
            //     var terkumpul = document.getElementById('terkumpul');

            //     if(this.value == '2'){
            //         bilangan.hidden = true;
            //         terkumpul.hidden = false;
            //     }
            //     else if(this.value == '1'){
            //         bilangan.hidden = false;
            //         terkumpul.hidden = true;
            //     }
            // });
          
            $(document).on('change', '#briefing_status', function(event) {
                event.preventDefault();
                
                $('.briefing').prop('readonly', !this.checked);
                if(!this.checked){
                    $('#briefing_at').val('');
                    $('#briefing_location').val('');
                    $('#brief_alert').hide();
                }else{
                    var jual_date = $('#start_sale_at').val();
                    $('#briefing_at').val(jual_date);
                    
                    var ads = $('#advertised_at').val();
                    var a = moment(ads,'DD/MM/YYYY');
                    var brief = $('#briefing_at').val();
                    var b = moment(brief, 'DD/MM/YYYY');
                    var diff = b.diff(a,'days');
                    if(diff <= 5 ){
                        $('#brief_alert').show();
                    }else{
                        $('#brief_alert').hide();
                    }
                }
            });
            $(document).on('change', '#visit_status', function(event) {
                event.preventDefault();
                
                $('.visit').prop('readonly', !this.checked);
                if(!this.checked){
                    $('#visit_date_time').val('');
                    $('#visit_location').val('');
                }else{
                    var jual_date = $('#briefing_at').val();
                    $('#visit_date_time').val(jual_date);
                }
            });
            
            $("#datetimepicker-briefing_at").on("change.datetimepicker", function (selected) {
                
                var ads = $('#advertised_at').val();
                var a = moment(ads,'DD/MM/YYYY');
                var b = moment(selected.date);
                var diff = b.diff(a,'days');
                
                if(diff <= 5 ){
                    $('#brief_alert').show();
                }else{
                    $('#brief_alert').hide();
                }
                
            });
            
            $("#datetimepicker-start_sale_at").trigger('change');
            $("#datetimepicker-closed_at").trigger('change');
            $('#experience_status').trigger('change');
            $('#experience').trigger('change');
            //$('#briefing_status').trigger('change');
            //$('#visit_status').trigger('change');
            $(".importBtn").on('click',function(){
                $(this).closest("form").attr("action", "{{ route('import.parse') }}");
            });
            //tab remain stay after refresh
            $('#documen-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
//            alert(window.location.hash);
            var hash = "";
            var h_qty = "{{ old('tab') }}";
            
                if(h_qty != null && h_qty != ''){
                    hash = h_qty;
                }else{
                    hash = window.location.hash;
                }
            $('#documen-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh
            
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    
                    swal('{!! __('Maklumat Perolehan') !!}', response.data.message, 'success');
                    location.reload();
                    
                });
            });
        });

        function clearClosedSaleAt(selectedDate){
            
            @if(!empty($acquisition->start_sale_at))
                var newDate = moment(selectedDate, 'DD/MM/YYYY');
                var currentDate = moment("{{ $acquisition->start_sale_at->format("d/m/Y") }}", 'DD/MM/YYYY');
                if(newDate.diff(currentDate, "days") != 0){
//                    document.getElementById('closed_sale_at').value = "";
                    document.getElementById('closed_at').value = "";
                }  
            @endif          
        }

        function clearClosedAt(selectedDate){
            @if(!empty($acquisition->closed_at))
                var newDate = moment(selectedDate, 'DD/MM/YYYY');
                var currentDate = moment("{{ $acquisition->closed_at->format("d/m/Y") }}", 'DD/MM/YYYY');

                if(newDate.diff(currentDate, "days") != 0){
                    document.getElementById('closed_sale_at').value = "";
                }   
            @endif         
        }
        
        function calculate(){
            var hargadokumen = document.getElementById('document_price').value;
            var GST = document.getElementById('gst').value / 100;           
            var x = hargadokumen.replace(new RegExp(',', 'g'), '');
            var y = GST;
            var resultGST = x * y;
            var result = Number(x) + (x * y);
            document.getElementById('gst_price').value = parseFloat(resultGST).toFixed(2);
            document.getElementById('total_document_price').value = parseFloat(result).toFixed(2);
        }

        function getKategori(obj) {
            var bilangan = document.getElementById('bilangan');
            var terkumpul = document.getElementById('terkumpul');

            if(obj.value == '1'){
                bilangan.hidden = true;
                terkumpul.hidden = false;
            }
            else if(obj.value == '2'){
                bilangan.hidden = false;
                terkumpul.hidden = true;
            }
        }
        
    </script>
@endpush

@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk: </span>{{ $acquisition->title }}
        <br>
        <span class="font-weight-bold">No. Perolehan: </span>{{ $acquisition->reference }}
    @endslot
@endcomponent
<div class="row" ref="msgContainer" id="tab_button">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="documen-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#document-details" role="tab" aria-controls="document-details" aria-selected="false">
                    @icon('fe fe-search')&nbsp;{{ __('Perinci') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-datetime" role="tab" aria-controls="document-datetime" aria-selected="false">
                    @icon('fe fe-calendar')&nbsp;{{ __('Tarikh dan Masa') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-financial" role="tab" aria-controls="document-financial" aria-selected="false">
                    @icon('fe fe-dollar-sign')&nbsp;{{ __('Harga') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-location" role="tab" aria-controls="document-location" aria-selected="false">
                    @icon('fe fe-map-pin')&nbsp;{{ __('Lokasi') }}
                </a>
            </li>

            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-experience" role="tab" aria-controls="document-experience" aria-selected="false">
                    @icon('fe fe-list')&nbsp;{{ __('Pengalaman') }}
                </a>
            </li>
            
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-quantity" role="tab" aria-controls="document-quantity" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Senarai Kuantiti') }}
                </a>
            </li>

            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-quantity-page" role="tab" aria-controls="document-quantity-page" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Halaman Cetakan') }}
                </a>
            </li>             

            @if(!empty($review) && (user()->id == $review->created_by))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews" role="tab" aria-controls="document-log-reviews" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif
            @endif
        </ul>
    </div>
    @component('components.card', ['card_classes' => 'col-10'])
        @slot('card_body')
            <div class="tab-content" id="acquisition-tab-content">
                <div class="tab-pane fade show active" id="document-details" role="tabpanel" aria-labelledby="acquisition-details-tab">
                    <form id="detail-form">
                        @include('contract.pre.document.partials.forms.details',['acquisition' => $acquisition])
                    </form>
                    @include('contract.pre.document.partials.forms.submit', [
                        'hashslug'=> $approval->hashslug,
                        'route_name' => 'api.contract.acquisition.details.update',
                        'form' => 'detail-form',
                    ])
                </div>
                <div class="tab-pane fade" id="document-datetime" role="tabpanel" aria-labelledby="acquisition-datetime-tab">
                    <form id="detetime-form">
                        @include('contract.pre.document.partials.forms.date-time',['acquisition' => $acquisition])
                        <hr>
                        @include('contract.pre.document.partials.forms.briefing',['acquisition' => $acquisition])
                        <hr>
                        @include('contract.pre.document.partials.forms.visit',['acquisition' => $acquisition])
                    </form>
                    @include('contract.pre.document.partials.forms.submit', [
                        'hashslug'=> $approval->hashslug,
                        'route_name' => 'api.contract.acquisition.date-time.update',
                        'form' => 'detetime-form',
                    ])
                </div>
                <div class="tab-pane fade" id="document-financial" role="tabpanel" aria-labelledby="acquisition-financial-tab">
                    <form id="financial-form">
                        @include('contract.pre.document.partials.forms.price',['acquisition' => $acquisition])
                    </form>
                    @include('contract.pre.document.partials.forms.submit', [
                        'hashslug'=> $approval->hashslug,
                        'route_name' => 'api.contract.acquisition.price.update',
                        'form' => 'financial-form',
                    ])
                </div>
                <div class="tab-pane fade" id="document-location" role="tabpanel" aria-labelledby="acquisition-location-tab">
                    <form id="location-form">
                        @include('contract.pre.document.partials.forms.location',['acquisition' => $acquisition])
                    </form>
                    @include('contract.pre.document.partials.forms.submit', [
                        'hashslug'=> $approval->hashslug,
                        'route_name' => 'api.contract.acquisition.location.update',
                        'form' => 'location-form',
                    ])
                </div>
                
                <div class="tab-pane fade" id="document-experience" role="tabpanel" aria-labelledby="acquisition-experience-tab">
                    <form id="experience-form">
                        @include('contract.pre.document.partials.forms.experience',['acquisition' => $acquisition])
                    </form>
                    @include('contract.pre.document.partials.forms.submit', [
                        'hashslug'=> $approval->hashslug,
                        'route_name' => 'api.contract.acquisition.experience.update',
                        'form' => 'experience-form',
                    ])
                </div>
                
                <div class="tab-pane fade" id="document-quantity" role="tabpanel" aria-labelledby="acquisition-quantity-tab">
                    <!--<form id="quantity-form">-->
                        <!--<input type="text" name="acquisition_id" id="acquisition_id" value="{{ $acquisition->id }}" hidden/>-->
                        @include('contract.pre.document.partials.forms.quantity',['acquisition' => $acquisition])
                    <!--</form>-->
                    <div class="btn-group float-right">
                        @include('contract.pre.document.partials.forms.submit', [
                            'hashslug'=> $approval->hashslug,
                        ])
                        @role('penyedia')
                            @if(!empty($acquisition) && $acquisition->user_id == user()->id)
                                {{-- @if(empty($review) || empty($review->status) || ($review->status == 'CN' && ((!empty($cetaknotisbpub) && $cetaknotisbpub->status == null) || (!empty($cetaknotisuu) && $cetaknotisuu->status == null) )) ) --}}
                                    <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                    <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $acquisition->id }}" hidden>

                                    <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                                    <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                                    <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                                    <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                                    <input type="text" id="type" name="type" value="Acquisitions" hidden>

                                    <button type="button" id="send" class="btn btn-default float-middle border-default">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                {{-- @endif --}}
                            @endif
                        @endrole

                    </div>
                </div>

                @component('components.tab.content', ['id' => 'document-quantity-page'])
                    @slot('content')
                        @include('contract.pre.document.partials.forms.quantity_page', ['acquisition' => $acquisition])
                    @endslot
                @endcomponent

                @component('components.tab.content', ['id' => 'document-reviews'])
                    @slot('content')

                        <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                            @if((!empty($review)) && (user()->id == $review->created_by))

                                {{-- penyedia --}}

                                @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                    Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s7log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s7log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                                                    Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl6 as $s6log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s6log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif --}}

                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                    Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl4 as $s4log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                    Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl3 as $s3log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif --}}

                                @if(!empty($semakan3zero) && !empty($semakan3zero->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                    Ulasan Semakan Oleh {{$semakan3zero->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif
                        
                        </div>

                    @endslot
                @endcomponent

                <div class="tab-pane fade" id="document-log-reviews" role="tabpanel" aria-labelledby="document-log-reviews-tab">
                    <form id="log-form">
                        <div class="row justify-content-center w-100">
                            <div class="col-12 w-100">
                                @include('contract.pre.box.partials.scripts')
                                @component('components.card')
                                    @slot('card_body')
                                        @component('components.datatable', 
                                            [
                                                'table_id' => 'contract-pre-box',
                                                'route_name' => 'api.datatable.contract.review-log',
                                                'param' => 'acquisition_id=' . $acquisition->id,
                                                'columns' => [
                                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                            ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                    ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                    ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                ],
                                                'headers' => [
                                                    __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                ],
                                                'actions' => minify('')
                                            ]
                                        )
                                        @endcomponent
                                    @endslot
                                @endcomponent
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            {{-- @component('components.tab.button')
            @endcomponent --}}
        @endslot
    @endcomponent
</div>
@endsection