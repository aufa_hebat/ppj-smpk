<html>
    <head>
        <style type="text/css">
            /** Define the margins of your page **/
            @page {
                margin: 70px 70px 60px 100px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 150px;
                color: black;
                text-align: right;
            }

            body{
                font-size: 12px;   
                font-family: "Arial, Helvetica, sans-serif";
                counter-reset:page -1;        
            } 

            footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: center;
                line-height: 35px;	
            }

            /*footer .pagenum:before {
                counter-increment: page;
                content: counter(page);
            }*/

            .pagenum:before {
                content: counter(page);                
            }
                    
            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }

            section {
                counter-reset:page;
            } 

            section .pagenum:after {                
                content: counter(page);
                counter-increment: page;
            }

            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
        </style>
    </head>
    <body>
        <center>
            <table  style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
                <tr>
                    <td style="width: 25%"></td>
                    <td style="width: 5%"></td>
                    <td style="width: 70%; text-align: right; font-weight: bold; text-decoration: underline;">Lampiran 'A'</td>
                </tr>
                <tr>
                    <td colspan="3"><br></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center; font-weight: bold; text-transform: uppercase;" class="title">Surat Akuan Penerimaan</td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td>Rujukan Tuan</td>
                    <td> : </td>
                    <td>{{ (!empty($acq))? $acq->reference:null }}</td>
                </tr>
                <tr>
                    <td>Rujukan Kami</td>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Tarikh</td>
                    <td> : </td>
                    <td>{{ \Carbon::intlFormat(now()) }}</td>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <td colspan="3" style="font-weight: bold;">
                        Bahagian Perolehan dan Ukur Bahan<br/>
                        Perbadanan Putrajaya<br/>
                    </td>
                </tr>
                <tr>
<!--                    <td colspan="3" style="line-height: 1.6">
                        Jabatan Kewangan<br/>
                        Aras 1,Blok C,Kompleks Perbadanan Putrajaya,<br/>
                        24, Persiaran Perdana, Presint 3,<br/>
                        62675 Putrajaya<br/>
                        (No. Faks : 03-8887 5020)
                    </td>-->
                    <td colspan="3" style="line-height: 1.6">
                        Jabatan Kewangan<br/>
                        Aras 1,Blok C,Kompleks Perbadanan Putrajaya,<br/>
                        24, Persiaran Perdana, Presint 3,<br/>
                        62675 Putrajaya<br/>
                        (Emel : ahmad.faizal@ppj.gov.my)
                    </td>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <td colspan="3">Tuan</td>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <td colspan="3" style="font-weight: bold; text-align: justify; text-transform: uppercase; line-height: 1.6">
                        {{ (!empty($acq))? $acq->title: null }}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="font-weight: bold; text-transform: uppercase;">
                        (NO. {{ (!empty($acq))? $acq->category->name: null }} : {{ (!empty($acq))? $acq->reference: null }})
                    </td>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <td colspan="3" style="font-weight: bold;"> - Addenda/Pindaan No. {{ (!empty($addenda))? $addenda->no:null }} Ke Atas Dokumen {{ (!empty($acq))? $acq->category->name:null }}</td>
                </tr>
                <tr>
                    <td colspan="3"><b><hr class="style1"></b></td>
                </tr>
            </table>
            <table  style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr>
                    <td style="line-height: 1.6">Ini adalah untuk mengesahkan penerimaan addenda/pindaan di atas, di mana isi kandungannya telah diambil kira dalam penyediaan dan pengiraan {{ (!empty($acq))? $acq->category->name:null }} harga kami.</td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td>Sekian, terima kasih</td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td>Yang benar,</td>
                </tr>
                <tr><td><br><br></td></tr>
                <tr>
                    <td>..................................................................</td>
                </tr>
            </table>
            <table  style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr><td><br></td></tr>
                <tr>
                    <td width="30%">Nama</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td width="30%">No. Kad Pengenalan</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td width="30%">Jawatan</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td width="30%">Tarikh</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td width="30%">Cop Syarikat</td>
                    <td>:</td>
                </tr>
            </table>
        </center>
    </body>
</html>