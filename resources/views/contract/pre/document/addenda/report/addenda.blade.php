<html>
    <head>
        <style type="text/css">
            /** Define the margins of your page **/
            @page {
                margin: 70px 100px 70px 100px;
            }
           
            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 150px;
                color: black;
                text-align: right;
            }
        
            body{
                font-size: 12px;   
                font-family: "Arial, Helvetica, sans-serif";
                counter-reset:page -1;        
            } 
        
            footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: center;
                line-height: 35px;	
            }
    
            /*footer .pagenum:before {
                counter-increment: page;
                content: counter(page);
            }*/
    
            .pagenum:before {
                content: counter(page);                
            }
                    
            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
        
            section {
                counter-reset:page;
            } 
        
            section .pagenum:after {                
                content: counter(page);
                counter-increment: page;
            }
        
            div.breakNow { page-break-inside:avoid; page-break-after:always; }
    
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
    
        </style>  
    </head>
    <body>
        <!--<center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>-->
        <center>
            <table  style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
<!--                <tr class="title">
                    <td colspan="3" ><b>PERBADANAN PUTRAJAYA</b><br>
                        Kompleks Perbadanan Putrajaya,<br>
                        24, Persiaran Perdana,<br>
                        Presint 3,<br>
                        62675 Putrajaya<br>
                        MALAYSIA
                    </td>
                </tr>-->
                <tr><td colspan="3"><br/><br/></td></tr>
                <tr class="content">
                    <td width="10%"><br><br><br><br><br></td>
                    {{--  <td width="15%"><br><br><br><br><br>Ruj. Tuan </td>  --}}
                    {{--  <td width="5%"><br><br><br><br><br> : </td>  --}}
                    <td width="80%" colspan="2"><br><br><br><br><br>{{ (!empty($acq))? $acq->reference:null }}</td>
                </tr>
                <tr><td colspan="3"><br/><br/><br/></td></tr>
                {{--  <tr class="content">
                    <td>Ruj. Kami</td>
                    <td> : </td>
                    <td></td>
                </tr>  --}}
                {{--  <tr><td colspan="3"><br/></td></tr>  --}}
                <tr class="content">
                    {{--  <td>Tarikh </td>  --}}
                    {{--  <td> : </td>  --}}
                    <td></td>
                    <td colspan="2" style="valign:top">{{ \Carbon::intlFormat(now()) }}</td>
                </tr>  
                <tr><td colspan="3"><br/></td></tr>      
            </table>
            <table  style="width: 100%;border-collapse: collapse;padding: 7px 7px;" align="center" class="content">
                <tr>
                    <td><b>Kepada yang Berkenaan</b></td>
                </tr>
                <tr><td><br/></td></tr>
                <tr>
                    <td><b>Tuan/Puan</b></td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td style="text-align: justify; font-weight: bold;">{{ (!empty($acq))? $acq->title:null }}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold; text-transform: uppercase;">(NO. {{ (!empty($acq))? $acq->category->name:null }} : {{ (!empty($acq))? $acq->reference:null }})</td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td style="font-weight: bold;">Per: Addenda/Pindaan No. {{ (!empty($addenda))? $addenda->no:null }} Ke Atas Dokumen {{ (!empty($acq))? $acq->category->name:null }}</td>
                </tr>
                <tr>
                    <td><b><hr class="style1"></b></td>
                </tr>
                <tr>
                    <td>Dengan hormatnya perkara di atas dirujuk.</td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td style="text-align: justify;">2. &nbsp;&nbsp;&nbsp;Bersama-sama ini kami sertakan satu (1) naskah Addenda/Pindaan No. {{ (!empty($addenda))? $addenda->no:null }} ke atas 
                    Dokumen <span style="text-transform: capitalize;">{{ (!empty($acq))? $acq->category->name:null }}</span>  tersebut dan ianya akan menjadi sebahagian 
                    daripada kontrak.  Addenda No. {{ (!empty($addenda))? $addenda->no:null }} ini mengandungi perkara-perkara berikut untuk tindakan pihak tuan selanjutnya:</td>
                </tr>
                <tr><td><br></td></tr>
            </table>
            <table  style="width: 80%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr>
                    <td style="width: 10%;" valign="top">i.</td>
                    <td style="width: 90%;" align="justify">
                        Arahan addenda/pindaan No. {{ (!empty($addenda))? $addenda->no:null }} kepada Dokumen <span style="text-transform: capitalize;">{{ (!empty($acq))? $acq->category->name:null }}</span> 
                        seperti berikut:
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <center>
                            <table border="1" style="width:100%; border-collapse: collapse; padding: 6px 6px;">
                                <tr>
                                    <td style="font-weight: bold;"><center>BILANGAN</center></td>
                                    <td style="font-weight: bold;"><center>PERKARA</center></td>
                                    <td style="font-weight: bold;"><center>LAMPIRAN</center></td>
                                </tr>
                                @if(!empty($perkara))
                                @foreach($perkara as $bil => $row)
                                    <tr>
                                        <td><center>{{ $bil+1}}</center></td>
                                        <td><center>{{ $row->description}}</center></td>
                                        <td><center>{{ $row->attachment}}</center></td>
                                    </tr>
                                @endforeach
                                @endif
                            </table>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;" valign="top">ii.</td>
                    <td style="width: 90%;" align="justify">
                        Surat pengesahan penerimaan oleh Petender / Penyebut Harga seperti <b>Lampiran A</b> - 
                        Sila kembalikan ke alamat yang dinyatakan dalam lampiran tersebut sebagai pengesahan penerimaan. 
                        Kegagalan tuan mengembalikan surat pengesahan penerimaan ini mungkin akan mengakibatkan 
                        {{ (!empty($acq))? $acq->category->name:null }} tuan ditolak.</td>
                </tr> 
                <tr><td colspan="2"><br></td></tr>       
            </table>
            <table  style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
                <tr>
                    <td style="text-align: justify;">
                        3. &nbsp;&nbsp;&nbsp;Tuan diminta hadir untuk mengambil dokumen Addenda/Pindaan No. {{ (!empty($addenda))? $addenda->no:null }} ke atas 
                        Dokumen <span style="text-transform: capitalize;">{{ (!empty($acq))? $acq->category->name:null }}</span>  di Kaunter Bahagian Perolehan 
                        dan Ukur Bahan, Aras 1, Blok C, Kompleks Perbadanan Putrajaya. Waktu urusan jam 8:30 pagi hingga 4:30 petang. 
                        Sila ambil perhatian tarikh tutup {{ (!empty($acq))? $acq->category->name:null }} 
                        @if(!empty($acq) && !empty($acq->extended_closed_sale_at)) 
                            dilanjutkan iaitu pada <span style="font-weight:bold;">
                                {{ (!empty($acq) && !empty($acq->extended_closed_sale_at))? \Carbon::intlFormat($acq->extended_closed_sale_at) :'' }}
                            </span>
                        @else
                            dikekalkan iaitu pada <span style="font-weight:bold;">
                                {{ (!empty($acq) && !empty($acq->closed_at))? \Carbon::intlFormat($acq->closed_at) :'' }}
                            </span>
                        @endif
                        pada waktu dan tempat yang sama seperti diiklankan.</td>
                </tr>
                <!--<tr><td><br></td></tr>-->
                
            </table>
        <div >
            <table  style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
                <tr>
                    <td>Sekian, terima kasih<br></td>
                </tr>
                <tr>
                    <td colspan="2">Saya yang menjalankan amanah,</td>
                </tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td colspan="2" style="text-transform: uppercase;" class="font-weight-bold"> 
                        @if(!empty($np_dept))
                        {{ '('. (!empty($np_dept->honourary)) ? $np_dept->honourary:null}}&nbsp;{{(!empty($np_dept->name)) ? $np_dept->name.')':null }} 
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2">b.p. Presiden,</td>
                </tr>
                <tr>
                    <td colspan="2">Perbadanan Putrajaya</td>
                </tr>
                <!--<tr><td><br></td></tr>-->
                <tr>
                    <td colspan="2"><b>Sk:</b></td>
                </tr>
                <!--<tr><td><br></td></tr>-->
                <tr>
                    <td colspan="2">Folder Jualan</td>
                </tr>
                <!--<tr><td><br></td></tr>-->
                <tr>
                    <br><td colspan="2">Fail Perolehan</td>
                </tr>
            </table>
        </div>
        </center>
    </body>
</html>