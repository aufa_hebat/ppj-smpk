@extends('layouts.admin')
@push('scripts')
@include('components.forms.assets.datetimepicker')
    <script>
        $(".addmore").on('click',function(){
            var $tr = $("#tableAddenda").find('tr[id^="klon"]:last');
            if($tr.prop("id") === undefined){
                $tr = $("table.d-none");
            }
            var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
            var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
            var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none'); 
              $klon.find('#perkara_'+currnum).attr('id','perkara_'+num).attr('name','baru['+num+'][perkara]').val('');
              $klon.find('#attachment_'+currnum).attr('id','attachment_'+num).attr('name','baru['+num+'][attachment]').val('');
            $('.addmore').parents('table').append($klon);
        });
        $('.buang').click(function () {
            $(this).parents('tr').detach();
        });
        jQuery(document).ready(function($) {
            $('#closed_at').val('{{ (!empty($acq->closed_at))? $acq->closed_at->format("d/m/Y"):null }}');
            $('#extended_closed_sale_at').val("{{ ($acq->extended_closed_sale_at)? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$acq->extended_closed_sale_at )->format('d/m/Y'):'' }}");
        });
    </script>
@endpush
@section('content')
    <div class="row justify-content-center">  
        <div class="col">
            @component('components.card')
                @slot('card_body')
                <div class="row">
                    <div class="col">
                        <div class="btn-group float-right">
                            <a href="{{ route('addenda',['hashslug' => $hashslug]) }}" 
                                target="_blank" 
                                class="btn btn-success border-success">
                                @icon('fe fe-printer') {{ __(' Addenda') }}
                            </a>
                            <a href="{{ route('addenda_attach',['hashslug' => $hashslug]) }}" 
                                target="_blank" 
                                class="btn btn-success border-success">
                                @icon('fe fe-printer') {{ __(' Lampiran A') }}
                            </a>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" method="POST" action="{{route('doc.addenda.store')}}" files="true" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="text" name="acqID" id="acqID" value="{{$acq->id ?? ''}}" hidden>
                <input type="text" name="hashslug" id="hashslug" value="{{$hashslug}}" hidden>
                <div class="row">
                    <div class="col-md-6">
                    @include('components.forms.input', [
                        'input_label' => __('Tarikh Tutup Perolehan'),
                        'id' => 'closed_at',
                        'name' => 'closed_at',
                    ])
                    </div>
                    <div class="col-md-6">
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Tutup Lanjutan (jika berkenaan)'),
                        'id' => 'extended_closed_sale_at',
                        'name' => 'extended_closed_sale_at',
                        'config' => [
                            'format' => config('datetime.display.date'),
                        ]
                    ])
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="float-right">
                        <button type="button" id="addRowAddenda" href="#" class="btn btn-primary addmore">
                            @icon('fe fe-plus')
                            {{ __('Tambah Addenda') }}
                        </button>
                    </div>
                    <div class="form-group">
                            <div id="tableAddenda" class="table table-responsive">
                                <table class="table table-sm table-transparent">
                                    <tr>
                                        <th width="10%" ><b>No. Addenda</b></th>
                                        <th width="55%"><b>Perkara</b></th>
                                        <th width="30%"><b>Lampiran</b></th>
                                        <th width="5%"><b><span class="addmore">HAPUS</span></b></th>
                                    </tr>
                                    @if(!empty($addendas))
                                    @foreach($addendas as $aden)
                                    @foreach($aden->items as $per)
                                    <tr>
                                        <td><input type="text" id="exist_addendaID" name="exist[{{$per->id}}][addendaID]" value="{{$aden->id}}" hidden>{{$aden->no}}</td>
                                        <td><input type="text" id="exist_perkaraID" name="exist[{{$per->id}}][perkaraID]" value="{{$per->id}}" hidden>{{$per->description}}</td>
                                        <td><input type="text" id="exist_attachmentID" name="exist[{{$per->id}}][attachmentID]" value="{{$per->id}}" hidden>{{$per->attachment}}</td>
                                        <td>
                                            <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endforeach
                                    @endif
                                    <tr id="klon0" class="d-none">
                                        <td></td>
                                        <td><input type="text" id="perkara_0" name="baru[0][perkara]" class="form-control" style="width:100%;"></td>
                                        <td><input type="text" id="attachment_0" name="baru[0][attachment]" class="form-control" style="width:100%;"></td>
                                        <td>
                                            <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                        </td>
                                    </tr>
                               </table>
                            </div>
                    </div>
                    <div class="btn-group float-right">
                        <a href="{{ route('acquisition.document.index', ['hashslug'=>$approval->hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                        @if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
                        <button type="submit" class="btn btn-primary">
                            @icon('fe fe-save') {{ __('Simpan') }}
                        </button>
                        @endif
                    </div>
                </div>
                </form>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection