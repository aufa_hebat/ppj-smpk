@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.dropzone')
    <!-- including the plugin -->
    <style type="text/css">
        input.currency {
            text-align: right;
            padding-right: 15px;
        }

        .input-group .form-control {
            float: none;
        }
        .input-group .input-buttons {
            position: relative;
            z-index: 3;
        }

        body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) { overflow-y: visible !important; }

    </style>
    <script src="{{asset('js/hideShowPassword.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/previous_next.js') }}"></script>
    <script>
        // Example 1:
        // - Password hidden by default
        // - Inner toggle shown
        $('#est_cost_project').hidePassword(true);
        

        // Example 2:
        // - Password shown by default
        // - Toggle shown on 'focus' of element
        // - Custom toggle class
        $('#password-2').showPassword('focus', {
            toggle: { className: 'my-toggle' }
        });

        // Example 3:
        // - When checkbox changes, toggle password
        //   visibility based on its 'checked' property
        $('#show-password').change(function(){
            $('#password-3').hideShowPassword($(this).prop('checked'));
        });

    </script>
    <script>
        var description_editor;

        jQuery(document).ready(function($) {

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#acquisition-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh



            $('.money').mask('000,000,000.00', {reverse: true});
            $("#kelulusan-perolehan-documents").dropzone(
                { 
                    url: route('api.contract.acquisition.approval.upload-documents', {hashslug:'{{ $approval->hashslug }}'}),
                    headers: {
                        'X-CSRFToken': $('meta[name="token"]').attr('content'),
                        'Accept': $('meta[name="api-header-accept"]').attr('content')
                    }
                }
            );

            @if($approval->allocation_resource_id == '3')
                $("#LainLain2").show();
            @else
                $("#LainLain2").hide();
            @endif

            $("#allocation_resource_id").change(function(){
                if (this.value == "3" ) {
                    $("#LainLain2").show();
                }
                else {
                    $("#LainLain2").hide();
                }
            });
            // Maklumat Perolehan
            $('#title').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$approval->title) !!}');

            $('#reference').val('{{ $approval->reference }}');
            $('#file_reference').val('{{ $approval->file_reference }}');
            $('#year').val('{{ $approval->year }}');
            $('#acquisition_type_id').val('{{ $approval->acquisition_type_id }}');
            $('#period_type_id').val('{{ $approval->period_type_id }}');
            $('#est_cost_project').val('{{ money()->toCommon($approval->est_cost_project ?? "0",2 ) }}');
            $('#user_name').val('{{ $approval->user->name }}');
            $('#verified_by').val('{{ $approval->user->supervisor->name }}');
            $('#verified_by_id').val('{{ $approval->user->supervisor->id }}');
            $('#created_at').val("{{ $approval->created_at->format('d-M-Y') }}");
            $('#period_length').val('{{ $approval->period_length ?? 1 }}');
            $('#location_id').val({{ $approval->locations->pluck('location_id') }});
            $('#location_id').trigger('change');

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .then( editor => {
                    description_editor = editor;
                    editor.setData("{!! str_replace('"', '\"', $approval->description) !!}");
                } )
                .catch( error => {
                    console.error( error );
                } );
            $('.select2').select2();
            $('#location_id').trigger('change');

            @if($approval->acquisition_type_id == '1' || $approval->acquisition_type_id == '3')
                $('#cidbShow').hide();
                $('#mofShow').show();
                $('#show_cidb').hide();
                $('#show_mof').show();
                $('#show_cidb_mof').val('1');
            @else
                $('#cidbShow').show();
                $('#mofShow').hide();
                $('#show_cidb').show();
                $('#show_mof').hide();
                $('#show_cidb_mof').val('2');
            @endif
            // Kelayakan
            @if($approval->cidb)
                $('#cidb').prop('checked', true);
            @endif

            @if($approval->ssm)
                $('#ssm1').prop('checked', true);
                $('#ssm2').prop('checked', true);
            @endif

            @if($approval->pp)
                $('#pp').prop('checked', true);
            @endif

            @if($approval->bpku_bumiputera)
                $('#bpku_bumiputera').prop('checked', true);
            @endif

            @if($approval->spkk)
                $('#spkk').prop('checked', true);
            @endif

            @if($approval->mof)
                $('#mof').prop('checked', true);
            @endif

            @if($approval->bumiputera)
                $('#bumiputera').prop('checked', true);
            @endif

            @if($approval->acquisition_type_id == '1' || $approval->acquisition_type_id == '3')
                @foreach($approval->mofQualifications as $khus)
                    if('{{$khus->status}}' == '1'){
                        $("#mof-code-status-{{ $khus->code->id }}-1").attr('checked',true);
                    }else if('{{$khus->status}}' == '0'){
                        $("#mof-code-status-{{ $khus->code->id }}-0").attr('checked',true);
                    }
                    @if($khus->code->type == "khusus")
                        $('.khusus-{{ $khus->code->category }}').show();

                    @endif
                @endforeach             
            @else
                @foreach($approval->cidbQualifications as $khus)
                    if('{{$khus->status}}' == '1'){
                        $("#cidb-code-status-{{ $khus->code->id }}-1").attr('checked',true);
                    }else if('{{$khus->status}}' == '0'){
                        $("#cidb-code-status-{{ $khus->code->id }}-0").attr('checked',true);
                    }
                    @if($khus->code->type == "khusus")
                        $('.khusus-{{ $khus->code->category }}').show();

                    @endif
                @endforeach
            @endif

            // Load MOF and CIDB Qualification
            @foreach($approval->mofQualifications as $qualification)
                $('#mof-code-checkbox-{{ $qualification->mof_code_id }}').prop('checked', true);
                $('#mof-code-status-{{ $qualification->mof_code_id }}-1').prop('disabled', false);
                $('#mof-code-status-{{ $qualification->mof_code_id }}-0').prop('disabled', false);
                // $('#cidb-code-status-{{ $qualification->cidb_code_id }}').val('{{ $qualification->status }}', true);
                // $('#cidb-code-status-{{ $qualification->cidb_code_id }}').prop('checked', true);
                $('input[name="codes\\[{{ $qualification->mof_code_id }}\\]]\\[value={{ $qualification->status }}\\]').prop('checked', true);
            @endforeach 
            @foreach($approval->cidbQualifications as $qualification)
                $('#cidb-code-checkbox-{{ $qualification->cidb_code_id }}').prop('checked', true);
                $('#cidb-code-status-{{ $qualification->cidb_code_id }}-1').prop('disabled', false);
                $('#cidb-code-status-{{ $qualification->cidb_code_id }}-0').prop('disabled', false);
                // $('#cidb-code-status-{{ $qualification->cidb_code_id }}').val('{{ $qualification->status }}', true);
                // $('#cidb-code-status-{{ $qualification->cidb_code_id }}').prop('checked', true);
                $('input[name="codes\\[{{ $qualification->cidb_code_id }}\\]]\\[value={{ $qualification->status }}\\]').prop('checked', true);
            @endforeach              


            // Kewangan
            $('#allocation_resource_id').val('{{ $approval->allocation_resource_id }}').change();
            $('#est_cost_project').val('{{ money()->toCommon($approval->est_cost_project) ?? '0.00'}}');
            $('#loyalty_code').val('{{ $approval->loyalty_code ?? ' ' }}');
            $('#other').val('{{ $approval->other }}');


            // Kelulusan
            // $('#verified_by').val({{ $approval->verified_by }});
            $('#finance_officer_id').val('{{ $approval->finance_officer_id }}').change();
            $('#authority_id').val('{{ $approval->authority_id }}');

            $('#financial_officer_id').trigger('change');
            $('#authority_id').trigger('change');

            @if(!empty($approval->prepared_at))
                $('#prepared_at').val('{{ $approval->prepared_at->format("d/m/Y") }}');
            @endif

            @if(!empty($approval->verified_at))
                $('#verified_at').val('{{ $approval->verified_at->format("d/m/Y") }}');
            @endif

            @if(!empty($approval->approved_at))
                $('#approved_at').val('{{ $approval->approved_at->format("d/m/Y") }}');
            @endif
                    // $('#datetimepicker-prepared_at').on('click',function () {
                    //     alert('test');
                    // });


            $('input[type="radio"]').trigger('change');

            $(document).on('change', '#acquisition_type_id', function(event) {
                event.preventDefault();
                if(this.value == '1' || this.value == '3'){
                    $('#cidbShow').hide();
                    $('#mofShow').show();
                    $('#show_cidb').hide();
                    $('#show_mof').show();
                    $('#show_cidb_mof').val('1');
                    $('#mof').prop('checked',true);
                    $('#bumiputera').prop('checked',true);
                    $('#ssm1').prop('checked',true);
                    $('#cidb').prop('checked',false);
                    $('#bpku_bumiputera').prop('checked',false);
                    $('#spkk').prop('checked',false);
                    $('#ssm2').prop('checked',false);
                }else{
                    $('#cidbShow').show();
                    $('#mofShow').hide();
                    $('#show_cidb').show();
                    $('#show_mof').hide();
                    $('#show_cidb_mof').val('2');
                    $('#cidb').prop('checked',true);
                    $('#bpku_bumiputera').prop('checked',true);
                    $('#spkk').prop('checked',true);
                    $('#ssm2').prop('checked',true);
                    $('#mof').prop('checked',false);
                    $('#bumiputera').prop('checked',false);
                    $('#ssm1').prop('checked',false);
                }
            });
           
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $approval->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                $('#description').val(description_editor.getData());
                var data = $('#' + form_id).serialize();
                
                axios.put(route(route_name, id), data).then(response => {
                    if(response.data.type == 'E'){
                        swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'error')
                        .then((result) => {
                            $(window).scrollTop(0);
                         });
                    }else{
                        swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'success')
                        .then((result) => {
                            $(window).scrollTop(0);
                         });
                    }
                    

                });
                
            });

            $(document).on('click', '.send-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $approval->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                $('#description').val(description_editor.getData());
                var data = $('#' + form_id).serialize();

                swal({
                    title: '{!! __('Kelulusan Perolehan') !!}',
                    text: '{!! __('Adakah anda pasti mahu menghantar rekod ini?') !!}',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.put(route(route_name, id), data).then(response => {
                            swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'success')
                            .then((result) => {
                                $(window).scrollTop(0);
                            });
                        });
                    }
                });
            });

            $(document).on('click', '.destroy-budget-code-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                    axios.delete(route('api.contract.acquisition.approval.budget-codes.destroy', id))
                        .then(response => {
                            $('#contract-pre-budget-code').DataTable().ajax.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.destroy-approval-document-action-btn', function(event) {
                event.preventDefault();
                var doc = $(this).data('hashslug');
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                    axios.delete(route('api.contract.acquisition.approval.destroy-documents', {
                        id : '{{ $approval->hashslug }}',
                        doc : doc
                    }))
                        .then(response => {
                            $('#kelulusan-perolehan-documents-datatable').DataTable().ajax.reload();
                        })
                  }
                });
            });

        });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" href="{{asset('css/example.wink.css')}}">
@endpush

@section('content')
    @component('components.forms.hidden-form', 
        [
            'id' => 'destroy-budge-code-form',
            'action' => route('api.contract.acquisition.approval.budget-codes.destroy', 'dummy')
        ])
        @slot('inputs')
            @method('DELETE')
        @endslot
    @endcomponent
	<div class="row" ref="msgContainer" id="tab_button">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="acquisition-tab-content" role="tablist">
                <li class="list-group-item">
                    <a id="acquisition-info-tab" class="list-group-item-action active" data-toggle="tab" href="#acquisition-info" role="tab" aria-controls="acquisition-info" aria-selected="false">
                        @icon('fe fe-search')&nbsp;{{ __('Perinci') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a id="acquisition-info-tab" class="list-group-item-action" data-toggle="tab" href="#acquisition-qualification" role="tab" aria-controls="acquisition-qualification" aria-selected="false">
                        @icon('fe fe-list')&nbsp;{{ __('Kelayakan') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a id="acquisition-info-tab" class="list-group-item-action" data-toggle="tab" href="#acquisition-financial" role="tab" aria-controls="acquisition-financial" aria-selected="false">
                        @icon('fe fe-dollar-sign')&nbsp;{{ __('Kewangan') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a id="acquisition-info-tab" class="list-group-item-action" data-toggle="tab" href="#acquisition-approval" role="tab" aria-controls="acquisition-approval" aria-selected="false">
                        @icon('fe fe-check-square')&nbsp;{{ __('Kelulusan') }}
                    </a>
                </li>
            </ul>
        </div>
        @component('components.card', ['card_classes' => 'col-10'])
            @slot('card_body')
                @component('components.tab.container', ['id' => 'acquisition'])
                    @slot('tabs')
                        @component('components.tab.content', ['id' => 'acquisition-info', 'active' => true])
                            @slot('content')
                                <h4>Maklumat Perolehan</h4>
                                <form id="detail-form">
                                    @include('contract.pre.partials.forms.create')
                                </form>
                                @include('contract.pre.partials.forms.submit', [
                                    'route_name' => 'api.contract.acquisition.approval.details.update',
                                    'form' => 'detail-form'
                                ])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'acquisition-qualification'])
                            @slot('content')
                                <h4>Maklumat Kelayakan</h4>
                                <form id="qualification-form">
                                    @include('contract.pre.partials.forms.qualification', ['hashslug' => $approval->hashslug])
                                </form>
                                @include('contract.pre.partials.forms.submit', [
                                    'route_name' => 'api.contract.acquisition.approval.qualifications.update',
                                    'form' => 'qualification-form'
                                ])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'acquisition-financial'])
                            @slot('content')
                                <h4>Maklumat Kewangan</h4>
                                @include('contract.pre.partials.forms.financial', ['hashslug' => $approval->hashslug])
                            @endslot
                        @endcomponent
                        
                        @component('components.tab.content', ['id' => 'acquisition-approval'])
                            @slot('content')
                                <h4>Maklumat Kelulusan</h4>
                                @include('contract.pre.partials.forms.approval')
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                {{-- <div class="tab-content" id="acquisition-tab-content">
                    <div class="tab-pane fade show active" id="acquisition-info" role="tabpanel" aria-labelledby="acquisition-info-tab">
                        <h4>Maklumat Perolehan</h4>
                        <form id="detail-form">
                            @include('contract.pre.partials.forms.create')
                        </form>
                        @include('contract.pre.partials.forms.submit', [
                            'route_name' => 'api.contract.acquisition.approval.details.update',
                            'form' => 'detail-form'
                        ])
                        <br><br><a class="btn btn-info float-right" href="#" id="next_tab1">@icon('fe fe-skip-forward') Seterusnya</a>
                    </div>
                    <div class="tab-pane fade" id="acquisition-qualification" role="tabpanel" aria-labelledby="acquisition-qualification-tab">
                        <h4>Maklumat Kelayakan</h4>
                        <form id="qualification-form">
                            @include('contract.pre.partials.forms.qualification', ['hashslug' => $approval->hashslug])
                        </form>
                        @include('contract.pre.partials.forms.submit', [
                            'route_name' => 'api.contract.acquisition.approval.qualifications.update',
                            'form' => 'qualification-form'
                        ])

                        <br><br><a class="btn btn-info pull-right" href="#" id="next_tab2">@icon('fe fe-skip-forward') Seterusnya</a>
                    </div>

                    <div class="tab-pane fade" id="acquisition-financial" role="tabpanel" aria-labelledby="acquisition-financial-tab">
                        <h4>Maklumat Kewangan</h4>
                        @include('contract.pre.partials.forms.financial', ['hashslug' => $approval->hashslug])
                    </div>
                    <div class="tab-pane fade" id="acquisition-approval" role="tabpanel" aria-labelledby="acquisition-approval-tab">
                        <h4>Maklumat Kelulusan</h4>
                        @include('contract.pre.partials.forms.approval')

                    </div>
                </div> --}}
                {{-- @component('components.tab.button')
                @endcomponent --}}
            @endslot
		@endcomponent
	</div>
@endsection