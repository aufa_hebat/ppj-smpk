@extends('layouts.acquisition.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.pre.document.partials.scripts')
            @component('components.forms.hidden-form', 
                [
                    'id' => 'destroy-record-form',
                    'action' => route('acquisition.document.destroy', 'dummy')
                ])
                @slot('inputs')
                    @method('DELETE')
                @endslot
            @endcomponent
            @component('components.card')
                
                @slot('card_body')
                
                    @if($tag!=null)
                
                    @component('components.datatable', 
                        [
                            'table_id' => 'perolehan',
                            'route_name' => 'api.datatable.contract.acquisition',
                            'param' => ['tag' => $tag],
                            'columns' => [
                                    ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                    ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                    ['data' => 'department', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                  
                                    ['data' => 'updated_at', 'title' => __('Dikemaskini pada'), 'defaultContent' => '-'],                        
                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                    
                            ],
                            'headers' => [
                                    __('No. Perolehan'), __('Tajuk'), __('Jabatan'), __('table.updated_at'), __('table.action')
                            ],
                            'actions' => minify(view('contract.pre.document.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                    
                    
                    @else
                     @component('components.datatable', 
                        [
                            'table_id' => 'perolehan',
                            'route_name' => 'api.datatable.contract.acquisition',
                            'param' => ['tag' => $tag],
                            'columns' => [
                                    ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                    ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                    ['data' => 'department', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                    ['data' => 'updated_at', 'title' => __('Dikemaskini pada'), 'defaultContent' => '-'],                        
                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                    
                            ],
                            'headers' => [
                                    __('No. Perolehan'), __('Tajuk'), __('Jabatan'),  __('Status'), __('table.updated_at'), __('table.action')
                            ],
                            'actions' => minify(view('contract.pre.document.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                    
                    @endif
                    
                    
                    
                @endslot
            @endcomponent
        </div>
    </div>
@endsection