<div class="text-center">
    <div class="item-action dropdown">
        <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
              <i class="fe fe-more-vertical"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
              style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
          	{{ $prepend_action or '' }}
      			<a class="dropdown-item review-action-btn' + data.{{ $primary_key or 'hashslug' }} + '">
      				@icon('fe fe-file') {{ __('BPUB') }}
      			</a>
      			<a class="dropdown-item law-review-action-btn' + data.{{ $primary_key or 'hashslug' }} + '">
      				@icon('fe fe-file') {{ __('Undang-Undang') }}
      			</a>
        </div>
    </div>
</div>