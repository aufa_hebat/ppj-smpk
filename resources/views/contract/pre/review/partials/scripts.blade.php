@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		/* Actions */
			$(document).on('click', '.review-btn', function(event) {
				/* Handle Method Spoofing */
				$("[name='_method']").val('POST');
				/* Handle primary key */
				$("[name='id']").val(null);
				/* Enable disabled inputs defined */
				$.each(disabled, function(index, val) {
					 $("[name='" + val + "']").prop('readonly', false);
				});
				$('#review-modal').modal('show');
			});
			$(document).on('click', '.law-review-btn', function(event) {
				/* Handle Method Spoofing */
				$("[name='_method']").val('POST');
				/* Handle primary key */
				$("[name='id']").val(null);
				/* Enable disabled inputs defined */
				$.each(disabled, function(index, val) {
					 $("[name='" + val + "']").prop('readonly', false);
				});
				$('#law-review-modal').modal('show');
			});

			@foreach($review as $rev)
				$(document).on('click', '.review-action-btn{{$rev->acquisition_id}}', function(event) {
					event.preventDefault();
					var id = {{$rev->acquisition_id}};
					$("#review-modal" + id).modal('show');
				});
			@endforeach
			@foreach($lawreview as $rev)
				$(document).on('click', '.law-review-action-btn{{$rev->acquisition_id}}', function(event) {
					event.preventDefault();
					var id = {{$rev->acquisition_id}};
					$("#law-review-modal" + id).modal('show');
				});
			@endforeach
	});
</script>
@endpush