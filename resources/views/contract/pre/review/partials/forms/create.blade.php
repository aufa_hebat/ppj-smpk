@include('components.forms.hidden', [
	'id' => 'id',
	'name' => 'id',
	'value' => ''
])

@include('components.forms.hidden', [
	'id' => 'acquisition_id',
	'name' => 'acquisition_id',
	'value' => $acquisition->id
])
@if(!empty($review))
<input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
<input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

<input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
@endif

<input type="text" id="type" name="type" value="Acquisitions" hidden>
<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
		    'input_label' => __('Nama Pegawai Semakan'),
		    'id' => '',
		    'name' => '',
		    'value' => user()->name,
		    'readonly' => true
		])
	</div>
	<div class="col-3">
		@if(!empty($review))
	    	@include('components.forms.input', [
			    'input_label' => __('Tarikh Terima'),
			    'id' => 'requested_at',
			    'name' => 'requested_at',
			])
		@endif
	</div>
	<div class="col-3">
		@if(!empty($review))
			{{-- @if(!empty($review->status)) --}}
	    	@include('components.forms.input', [
			    'input_label' => __('Tarikh Hantar'),
			    'id' => 'approved_ats',
			    'name' => 'approved_at',
			])
			{{-- @endif --}}
		@endif
	</div>
	@include('components.forms.hidden', [
	    'id' => 'requested_by',
	    'name' => 'requested_by',
	    'value' => user()->id
	])
	@include('components.forms.hidden', [
	    'id' => 'approved_by',
	    'name' => 'approved_by',
	    'value' => user()->supervisor->id
	])
</div>

@include('components.forms.textarea', [
    'input_label' => __('Ulasan'),
    'id' => 'remarks',
    'name' => 'remarks'
])