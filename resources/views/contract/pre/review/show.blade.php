@extends('layouts.admin')

@section('content')
@include('components.forms.assets.datetimepicker')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#document-details" role="tab" aria-controls="document-details" aria-selected="false">
                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Semakan') }}
                </a>
            </li>
            
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-briefing" role="tab" aria-controls="document-briefing" aria-selected="false">
                    @icon('fe fe-file-text')&nbsp;{{ __('Ulasan Semakan') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                         @component('components.tab.content', ['id' => 'document-details', 'active' => true])
                            @slot('content')

                            	<form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">

	                            	@include('contract.pre.review.partials.forms.create')

									<div class="btn-group float-right">
				                        <a href="{{ route('acquisition.acquisition.index') }}" 
			                                class="btn btn-default border-primary">
			                                {{ __('Kembali') }}
				                        </a>
				                        <a href="{{ route('acquisition.acquisition.index') }}" 
			                                class="btn btn-danger btn-default border-primary">
			                                @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
				                        </a>
		                                <button type="submit" class="btn btn-primary btn-group float-right">
		                                    @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
		                                </button>
				                        <a href="{{ route('acquisition.acquisition.index') }}" 
			                                class="btn btn-info btn-default border-primary">
			                                @icon('fe fe-send')&nbsp; {{ __('Teratur') }}
				                        </a>
				                    </div>

				                </form>
                            @endslot
                        @endcomponent
                        
                        @component('components.tab.content', ['id' => 'document-briefing'])
                            @slot('content')
                                ....
			                    <div class="btn-group float-right">
			                        <a href="{{ route('acquisition.acquisition.index') }}" 
			                                class="btn btn-default border-primary">
			                                {{ __('Kembali') }}
			                        </a>
			                    </div>
                            @endslot
                        @endcomponent

                    @endslot
                @endcomponent
                
            @endslot
        @endcomponent
    </div>
</div>
@endsection