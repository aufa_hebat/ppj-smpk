@extends('layouts.admin')

@push('scripts')
	<script type="text/javascript">
        jQuery(document).ready(function($) {  

        	@foreach($review as $rev)
        		$('#send{{$rev->acquisition_id}}').click(function(){
	                var route_name = 'acquisition.review.store';
	                var data = {
	                    sentStatus : $('#sentStatus').val(),
	                    acquisition_id : $('#acquisition_id{{$rev->acquisition_id}}').val(),
	                    requested_by : $('#requested_by{{$rev->acquisition_id}}').val(),
	                    progress : $('#progress{{$rev->acquisition_id}}').val(),
	                    task_by : $('#task_by{{$rev->acquisition_id}}').val(),
	                    law_task_by : $('#law_task_by{{$rev->acquisition_id}}').val(),
	                    approved_by : $('#task_by{{$rev->acquisition_id}}').val(),
	                    created_by : $('#created_by{{$rev->acquisition_id}}').val(),
	                    approved_at : $('#approved_at{{$rev->acquisition_id}}').val(),
	                    type : $('#type{{$rev->acquisition_id}}').val(),
	                    status : $('#status{{$rev->acquisition_id}}').val(),
	                    department : $('#department{{$rev->acquisition_id}}').val()
	                };
	                swal({
	                    title: '{!! __('Hantar') !!}',
	                    text: '{!! __('Adakah anda pasti?') !!}',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: '{!! __('Ya') !!}',
	                    cancelButtonText: '{!! __('Batal') !!}'
	                }).then((result) => {
	                    if (result.value) {
	                        axios.post(route(route_name), data).then(response => {
	                                                   
	                            // if(response.data == 'success'){

	                                $('#send').hide();
	                                redirect(route('home'));
	                                //
	                                //go to
	                            // }else{
	                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
	                            // }
	                                                                
	                         });
	                    }
	                });
	            });
        	@endforeach
            

            $(document).on('click', '#law_send', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                console.log(form_id);
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name, id) , data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#law_send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                        });
                    }
                });
                
            });
        });
    </script>
@endpush

@section('content')

@if(user()->current_role_login == 'administrator')

	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.review.partials.scripts')
			<font style="color: red">*</font> "-" bermaksud belum diserah tugas lagi.
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-review',
							'route_name' => 'api.datatable.contract.acquisition.reviews',
							'columns' => [
								['data' => 'no_perolehan', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'tajuk_perolehan', 'title' => __('Tajuk Perolehan'), 'defaultContent' => '-'],
								['data' => 'assign_to', 'title' => __('Serah Tugas (BPUB)'), 'defaultContent' => '-'],
								['data' => 'law_assign_to', 'title' => __('Serah Tugas (JU)'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Perolehan'),__('Tajuk Perolehan'), __('Serah Tugas (BPUB)'), __('Serah Tugas (JU)'), __('Tindakan')
							],
							'actions' => minify(view('contract.pre.review.partials.action')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>

	@foreach($lawreview as $rev)
		<div class="modal fade" id="law-review-modal{{$rev->acquisition_id}}" role="dialog" aria-labelledby="modalTambahJKPenilaiModalCenterTitle" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="modalTambahJKPenilaiModalLongTitle">Penyerahan Tugasan</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
		          <span aria-hidden="true"></span>
		        </button>
		      </div>
		      <div class="modal-body">
    			<form id="law-review-form{{$rev->acquisition_id}}" files = "true" enctype="multipart/form-data" method="POST">
				    @csrf
			    	@method('PUT')

		      		<input type="text" id="acquisition_id" name="acquisition_id" value="{{ $rev->acquisition_id }}" hidden>

			        <div class="row">
			            <div class="col-md-12 col-lg-12 col-xl-12">
			            	<div class="row">
			            		<div class="col-4">

			            			<div class="form-group row">
									    <label for="" class="col col-form-label">
									        Tarikh Terima
									    </label>

									    <div class="col">
									        <input readonly="true" type="text" class="form-control" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" autofocus>
									    </div>
									</div>
			            			
			            		</div>
			            		<div class="col-4">

					                <div class="form-group row">
									    <label for="" class="col col-form-label">
									        Tarikh Hantar
									    </label>

									    <div class="col">
									        <input readonly="true" type="text" class="form-control" id="requested_at" name="requested_at" value="{{ date('d/m/Y h:i A', strtotime($rev->approved_at)) }}" autofocus>
									    </div>
									</div>
			            			
			            		</div>
			            		<div class="col-4">

			            			<div class="form-group row">
									    <label for="" class="col col-form-label">
									        Nama Urusetia
									    </label>

									    <div class="col">
									        <input readonly="true" type="text" class="form-control" id="" name="" value="{{ user()->name }}" autofocus>
									        <input readonly="true" type="hidden" class="form-control" id="requested_by" name="requested_by" value="{{ user()->id }}" autofocus>
									    </div>
									</div>
			            			
			            		</div>
			            	</div>
			            	<div class="row">
			            		<div class="col-2">
			                        <input type="text" id="type" name="type" value="Acquisitions" hidden>
			            		</div>
			            		<div class="col-8">
			            			<div class="form-group row">
			            				@if($rev->progress == '0')
										    <label for="" class="col col-form-label">
										        Serah Tugas Kepada
										    </label>
					            			<select class="form-control input-lg" id="law_task_by" name="law_task_by" style="width: 100%;">
					                            <option value=""></option>
					                            @foreach($lawuser as $users)
					                                <option value="{{$users->id}}">{{$users->name}}</option>
					                            @endforeach
					                        </select>
				                        @else
				                        	<label for="" class="col col-form-label">
										        Serah Tugas Kepada
										    </label>
										    <input type="text" name="" value="{{$rev->lawtask->name ?? ''}}" class="form-control" readonly>
				                        @endif

				                    </div>
			            		</div>
			            		<div class="col-2">
			            			
			            		</div>
			            	</div>
			            	@if($rev->progress == '0')
				            	<div class="btn-group float-right">
							        <button type="submit" class="btn btn-default float-middle border-default " id="law_send"
									    data-route="acquisition.lawreview.update"
									    data-form="law-review-form{{$rev->acquisition_id}}"
									    data-id="{{$rev->acquisition->id}}">
							            @icon('fe fe-send') {{ __('Teratur') }}
							        </button>
					            </div>
				            @endif
			            </div>
			        </div>
			    </form>
		      </div>
		    </div>
		  </div>
		</div>

	@push('scripts')
	    <script>
	    	jQuery(document).ready(function($) {
	    		$('#law_task_by').val('{!! $rev->law_task_by !!}');
	    	});
	    </script>
	@endpush

	@endforeach

	@foreach($review as $rev)
			<div class="modal fade" id="review-modal{{$rev->acquisition_id}}" role="dialog" aria-labelledby="modalTambahJKPenilaiModalCenterTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="modalTambahJKPenilaiModalLongTitle">Penyerahan Tugasan</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
			          <span aria-hidden="true"></span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
			      		@csrf

			      		 <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

			      		 <input type="text" id="acquisition_id{{$rev->acquisition_id}}" name="acquisition_id" value="{{ $rev->acquisition_id }}" hidden>

				        <div class="row">
				            <div class="col-md-12 col-lg-12 col-xl-12">
				            	<div class="row">
				            		<div class="col-4">

				            			<div class="form-group row">
										    <label for="" class="col col-form-label">
										        Tarikh Terima
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="approved_at{{$rev->acquisition_id}}" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            		<div class="col-4">

						                <div class="form-group row">
										    <label for="" class="col col-form-label">
										        Tarikh Hantar
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="requested_at{{$rev->acquisition_id}}" name="requested_at" value="{{ date('d/m/Y h:i A', strtotime($rev->approved_at)) }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            		<div class="col-4">

				            			<div class="form-group row">
										    <label for="" class="col col-form-label">
										        Nama Urusetia
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="" name="" value="{{ user()->name }}" autofocus>
										        <input readonly="true" type="hidden" class="form-control" id="requested_by{{$rev->acquisition_id}}" name="requested_by" value="{{ user()->id }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            	</div>
				            	<div class="row">
				            		<div class="col-2">
				                        <input type="text" id="type{{$rev->acquisition_id}}" name="type" value="Acquisitions" hidden>
				            		</div>
				            		<div class="col-8">
				            			<div class="form-group row">

				            				@if($rev->progress == '0')
											    <label for="" class="col col-form-label">
											        Serah Tugas Kepada
											    </label>
						            			<select class="form-control input-lg" id="task_by{{$rev->acquisition_id}}" name="task_by" style="width: 100%;">
						                            <option value=""></option>
						                            @foreach($user as $users)
						                                <option value="{{$users->user_id}}">{{$users->user_name}}</option>
						                            @endforeach
						                        </select>
					                        @else
					                        	<label for="" class="col col-form-label">
											        Serah Tugas Kepada
											    </label>
											    <input type="text" name="" value="{{$rev->task->name ?? ''}}" class="form-control" readonly>
					                        @endif

					                    </div>
				            		</div>
				            		<div class="col-2">
				            			
				            		</div>
				            	</div>
				            	@if($rev->progress == '0')
					            	<div class="btn-group float-right">
						            	<button type="button" id="send{{$rev->acquisition_id}}" class="btn btn-default float-middle border-default savSent">
						                    @icon('fe fe-send') {{ __('Teratur') }}
						                </button>
						            </div>
					            @endif
				            </div>
				        </div>
				    </form>
			      </div>
			    </div>
			  </div>
			</div>

		@push('scripts')
		    <script>
		    	jQuery(document).ready(function($) {
		    		$('#task_by').val('{!! $rev->task_by !!}');
		    	});
		    </script>
		@endpush

		@endforeach

@else
	@if(user()->department_id == '3')
		<div class="row justify-content-center">
			<div class="col">
				@include('contract.pre.review.partials.scripts')
				@component('components.card')
					@slot('card_body')
						@component('components.datatable', 
							[
								'table_id' => 'contract-pre-review',
								'route_name' => 'api.datatable.contract.acquisition.lawreview',
								'columns' => [
									['data' => 'no_perolehan', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
									['data' => 'tajuk_perolehan', 'title' => __('Tajuk Perolehan'), 'defaultContent' => '-'],
									['data' => 'assign_to', 'title' => __('Serah Tugas Kepada'), 'defaultContent' => '-'],
									['data' => 'progress', 'title' => __('Status'), 'defaultContent' => '-'],
									['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
								],
								'headers' => [
									__('No. Perolehan'),__('Tajuk Perolehan'), __('Serah Tugas Kepada'), __('Status'), __('Tindakan')
								],
								'actions' => minify(view('contract.pre.review.partials.actions')->render())
							]
						)
						@endcomponent
					@endslot
				@endcomponent
			</div>
		</div>

		@foreach($lawreview as $rev)
			<div class="modal fade" id="review-modal{{$rev->acquisition_id}}" role="dialog" aria-labelledby="modalTambahJKPenilaiModalCenterTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="modalTambahJKPenilaiModalLongTitle">Penyerahan Tugasan</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
			          <span aria-hidden="true"></span>
			        </button>
			      </div>
			      <div class="modal-body">
	    			<form id="law-review-form{{$rev->acquisition_id}}" files = "true" enctype="multipart/form-data" method="POST">
					    @csrf
				    	@method('PUT')

			      		<input type="text" id="acquisition_id" name="acquisition_id" value="{{ $rev->acquisition_id }}" hidden>

				        <div class="row">
				            <div class="col-md-12 col-lg-12 col-xl-12">
				            	<div class="row">
				            		<div class="col-4">

				            			<div class="form-group row">
										    <label for="" class="col col-form-label">
										        Tarikh Terima
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            		<div class="col-4">

						                <div class="form-group row">
										    <label for="" class="col col-form-label">
										        Tarikh Hantar
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="requested_at" name="requested_at" value="{{ date('d/m/Y h:i A', strtotime($rev->approved_at)) }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            		<div class="col-4">

				            			<div class="form-group row">
										    <label for="" class="col col-form-label">
										        Nama Urusetia
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="" name="" value="{{ user()->name }}" autofocus>
										        <input readonly="true" type="hidden" class="form-control" id="requested_by" name="requested_by" value="{{ user()->id }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            	</div>
				            	<div class="row">
				            		<div class="col-2">
				                        <input type="text" id="type" name="type" value="Acquisitions" hidden>
				            		</div>
				            		<div class="col-8">
				            			<div class="form-group row">
				            				@if($rev->progress == '0')
											    <label for="" class="col col-form-label">
											        Serah Tugas Kepada
											    </label>
						            			<select class="form-control input-lg" id="law_task_by" name="law_task_by" style="width: 100%;">
						                            <option value=""></option>
						                            @foreach($lawuser as $users)
						                                <option value="{{$users->id}}">{{$users->name}}</option>
						                            @endforeach
						                        </select>
					                        @else
					                        	<label for="" class="col col-form-label">
											        Serah Tugas Kepada
											    </label>
											    <input type="text" name="" value="{{$rev->lawtask->name ?? ''}}" class="form-control" readonly>
					                        @endif

					                    </div>
				            		</div>
				            		<div class="col-2">
				            			
				            		</div>
				            	</div>
				            	@if($rev->progress == '0')
					            	<div class="btn-group float-right">
								        <button type="submit" class="btn btn-default float-middle border-default " id="law_send"
										    data-route="acquisition.lawreview.update"
										    data-form="law-review-form{{$rev->acquisition_id}}"
										    data-id="{{$rev->acquisition->id}}">
								            @icon('fe fe-send') {{ __('Teratur') }}
								        </button>
						            </div>
					            @endif
				            </div>
				        </div>
				    </form>
			      </div>
			    </div>
			  </div>
			</div>

		@push('scripts')
		    <script>
		    	jQuery(document).ready(function($) {
		    		$('#law_task_by').val('{!! $rev->law_task_by !!}');
		    	});
		    </script>
		@endpush

		@endforeach
	@else
		<div class="row justify-content-center">
			<div class="col">
				@include('contract.pre.review.partials.scripts')
				@component('components.card')
					@slot('card_body')
						@component('components.datatable', 
							[
								'table_id' => 'contract-pre-review',
								'route_name' => 'api.datatable.contract.acquisition.review',
								'columns' => [
									['data' => 'no_perolehan', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
									['data' => 'tajuk_perolehan', 'title' => __('Tajuk Perolehan'), 'defaultContent' => '-'],
									['data' => 'assign_to', 'title' => __('Serah Tugas Kepada'), 'defaultContent' => '-'],
									['data' => 'progress', 'title' => __('Status'), 'defaultContent' => '-'],
									['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
								],
								'headers' => [
									__('No. Perolehan'),__('Tajuk Perolehan'), __('Serah Tugas Kepada'), __('Status'), __('Tindakan')
								],
								'actions' => minify(view('contract.pre.review.partials.actions')->render())
							]
						)
						@endcomponent
					@endslot
				@endcomponent
			</div>
		</div>

		@foreach($review as $rev)
			<div class="modal fade" id="review-modal{{$rev->acquisition_id}}" role="dialog" aria-labelledby="modalTambahJKPenilaiModalCenterTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="modalTambahJKPenilaiModalLongTitle">Penyerahan Tugasan</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
			          <span aria-hidden="true"></span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
			      		@csrf

			      		 <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

			      		 <input type="text" id="acquisition_id{{$rev->acquisition_id}}" name="acquisition_id" value="{{ $rev->acquisition_id }}" hidden>

				        <div class="row">
				            <div class="col-md-12 col-lg-12 col-xl-12">
				            	<div class="row">
				            		<div class="col-4">

				            			<div class="form-group row">
										    <label for="" class="col col-form-label">
										        Tarikh Terima
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="approved_at{{$rev->acquisition_id}}" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            		<div class="col-4">

						                <div class="form-group row">
										    <label for="" class="col col-form-label">
										        Tarikh Hantar
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="requested_at{{$rev->acquisition_id}}" name="requested_at" value="{{ date('d/m/Y h:i A', strtotime($rev->approved_at)) }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            		<div class="col-4">

				            			<div class="form-group row">
										    <label for="" class="col col-form-label">
										        Nama Urusetia
										    </label>

										    <div class="col">
										        <input readonly="true" type="text" class="form-control" id="" name="" value="{{ user()->name }}" autofocus>
										        <input readonly="true" type="hidden" class="form-control" id="requested_by{{$rev->acquisition_id}}" name="requested_by" value="{{ user()->id }}" autofocus>
										    </div>
										</div>
				            			
				            		</div>
				            	</div>
				            	<div class="row">
				            		<div class="col-2">
				                        <input type="text" id="type{{$rev->acquisition_id}}" name="type" value="Acquisitions" hidden>
				            		</div>
				            		<div class="col-8">
				            			<div class="form-group row">

				            				@if($rev->progress == '0')
											    <label for="" class="col col-form-label">
											        Serah Tugas Kepada
											    </label>
						            			<select class="form-control input-lg" id="task_by{{$rev->acquisition_id}}" name="task_by" style="width: 100%;">
						                            <option value=""></option>
						                            @foreach($user as $users)
						                                <option value="{{$users->user_id}}">{{$users->user_name}}</option>
						                            @endforeach
						                        </select>
					                        @else
					                        	<label for="" class="col col-form-label">
											        Serah Tugas Kepada
											    </label>
											    <input type="text" name="" value="{{$rev->task->name ?? ''}}" class="form-control" readonly>
					                        @endif

					                    </div>
				            		</div>
				            		<div class="col-2">
				            			
				            		</div>
				            	</div>
				            	@if($rev->progress == '0')
					            	<div class="btn-group float-right">
						            	<button type="button" id="send{{$rev->acquisition_id}}" class="btn btn-default float-middle border-default savSent">
						                    @icon('fe fe-send') {{ __('Teratur') }}
						                </button>
						            </div>
					            @endif
				            </div>
				        </div>
				    </form>
			      </div>
			    </div>
			  </div>
			</div>

		@push('scripts')
		    <script>
		    	jQuery(document).ready(function($) {
		    		$('#task_by').val('{!! $rev->task_by !!}');
		    	});
		    </script>
		@endpush

		@endforeach
	@endif
@endif

@endsection