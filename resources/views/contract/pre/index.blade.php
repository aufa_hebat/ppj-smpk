@extends('layouts.admin')

@push('scripts')
	@include('components.charts.utils')
@endpush

@section('content')
	{{--  @include('contract.pre.partials.dashboard')  --}}
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.partials.scripts')
			@component('components.forms.hidden-form', 
				[
					'id' => 'destroy-record-form',
					'action' => route('contract.pre.destroy', 'dummy')
				])
				@slot('inputs')
					@method('DELETE')
				@endslot
			@endcomponent
			@component('components.card')
				@if(user()->current_role_login == 'penyedia')
					@slot('card_title')
					
						<a href="{{ route('contract.pre.create') }}" class="btn btn-primary float-right">
							@icon('fe fe-plus') {{ __('Kelulusan Perolehan Baru') }}
						</a>
					
					@endslot
				@endif
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-management',
							'route_name' => 'api.datatable.contract.pre',
                                                        'param' => 'dept_id=' . user()->department_id,
							'columns' => [
								['data' => 'reference', 'title' => __('No. Rujukan'), 'defaultContent' => '-'],
								['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
								['data' => 'department', 'title' => __('Jabatan'), 'defaultContent' => '-'],
								['data' => 'updated_at', 'title' => __('Dikemaskini Pada'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Rujukan'), __('Tajuk'), __('table.created_at'), __('table.updated_at'), __('table.action')
							],
							'actions' => minify(view('contract.pre.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection