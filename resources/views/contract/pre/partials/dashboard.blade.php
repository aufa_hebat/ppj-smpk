@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
           @foreach(chart()->acquisitions() as $key => $chart)
                createChart(@json($chart), '{{ snake_case($key) }}');
           @endforeach
        });
    </script>
@endpush

<div class="row">
    @forelse(dashboard()->acquisitions() as $item)
        <div class="col-3">
            @include('components.cards.figure', $item)
        </div>
    @empty
        <div class="col">
            @include('components.cards.empty-info', ['header' => 'Maklumat Perolehan'])
        </div>
    @endforelse
</div>

<div class="row">
    @forelse(chart()->acquisitions() as $key => $chart)
        @if(!empty($chart))
        <div class="col-6">
            @include('components.charts.box', ['chart_id' => snake_case($key)])
        </div>
        @endif
    @empty
        <div class="col">
            @include('components.cards.empty-info', ['header' => 'Carta Perolehan'])
        </div>
    @endforelse
</div>