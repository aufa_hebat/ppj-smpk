<a class="dropdown-item destroy-approval-document-action-btn" style="cursor: pointer;"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
	<i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
</a>
<!-- <a target="_blank" class="dropdown-item download-approval-document-action-btn file"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '"
>
	<i class="fas fa-download text-danger"></i> {{ __('Muat Turun') }}
</a> -->
