<div class="text-center">
	<div class="item-action dropdown">
	  	<a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
	  		<i class="fe fe-more-vertical"></i>
	  	</a>
	  	<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
	  		style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
	  		{{ $prepend_action or '' }}
			<a class="dropdown-item edit-budget_code-action-btn" style="cursor: pointer;"
    			data-toggle="modal" data-target="#edit-kod-bajet-modal"
				data-{{ 'edit_hashslug' }}="' + data.{{ 'hashslug' }} + '" 
    			data-{{ 'fund_id' }}="' + data.{{ 'fund_id' }} + '"
    			data-{{ 'functional_area_id' }}="' + data.{{ 'functional_area_id' }} + '"
				data-{{ 'funded_programme_id' }}="' + data.{{ 'funded_programme_id' }} + '"
				data-{{ 'gl_account_id' }}="' + data.{{ 'gl_account_id' }} + '"
				data-{{ 'business_area_id' }}="' + data.{{ 'business_area_id' }} + '"
				data-{{ 'cost_center_id' }}="' + data.{{ 'cost_center_id' }} + '"
    			data-{{ 'fund' }}="' + data.{{ 'fund' }} + '"
    			data-{{ 'functional_area' }}="' + data.{{ 'functional_area' }} + '"
				data-{{ 'funded_programme' }}="' + data.{{ 'funded_programme' }} + '"
				data-{{ 'gl_account' }}="' + data.{{ 'gl_account' }} + '"
				data-{{ 'business_area' }}="' + data.{{ 'business_area' }} + '"
				data-{{ 'cost_center' }}="' + data.{{ 'cost_center' }} + '"	
    		>
				<i class="fe fe-edit text-success"></i> {{ __('Kemaskini') }}
			</a>
			<a class="dropdown-item destroy-budget-code-action-btn"
				data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
				<i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
			</a>
		</div>
	</div>
</div>
