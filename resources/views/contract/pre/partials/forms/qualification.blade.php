<div class="row">
	<div class="col">
		<div class="form-group">
			<div class="form-label">Syarat Pendaftaran</div>
                            <input type="text" id="show_cidb_mof" name="show_cidb_mof" hidden>
                        <div class="custom-controls-stacked" id="mofShow">
				@include('components.forms.checkbox', [
					'id' => 'mof', 'name' => 'mof', 'value' => '1',
					'label' => 'MOF'
				])
                                <div class="custom-controls-stacked" style="margin-left: 5%"> 
                                    @include('components.forms.checkbox', [
                                            'id' => 'bumiputera', 'name' => 'bumiputera', 'value' => '1',
                                            'label' => 'Bumiputera'
                                    ])
                                </div>
				@include('components.forms.checkbox', [
					'id' => 'ssm1', 'name' => 'ssm1', 'value' => '1',
					'label' => 'Suruhanjaya Syarikat Malaysia (SSM)'
				])

			</div>
			<div class="custom-controls-stacked" id="cidbShow">
				@include('components.forms.checkbox', [
					'id' => 'cidb', 'name' => 'cidb', 'value' => '1',
					'label' => 'Lembaga Pembangunan Industri Pembinaan (CIDB)'
				])
                                <div class="custom-controls-stacked" style="margin-left: 5%"> 
                                    @include('components.forms.checkbox', [
                                            'id' => 'bpku_bumiputera', 'name' => 'bpku_bumiputera', 'value' => '1',
                                            'label' => 'Bahagian Pembangunan Kontraktor dan Usahawan (BPKU)'
                                    ])

                                    @include('components.forms.checkbox', [
                                            'id' => 'spkk', 'name' => 'spkk', 'value' => '1',
                                            'label' => 'Sijil Perolehan Kerja Kerajaan (SPKK)'
                                    ])
                                </div>
                                @include('components.forms.checkbox', [
					'id' => 'ssm2', 'name' => 'ssm2', 'value' => '1',
					'label' => 'Suruhanjaya Syarikat Malaysia (SSM)'
				])
			</div>
		</div>
	</div>
</div>
<hr>
<div id="show_cidb">
	@include('contract.pre.partials.forms.qualification-cidb')
</div>
<div id="show_mof">
	@include('contract.pre.partials.forms.qualification-mof')
</div>