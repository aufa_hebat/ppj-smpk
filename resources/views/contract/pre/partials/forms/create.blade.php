<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col">
                @include('components.forms.textarea', [
                    'input_label' => __('Tajuk'),
                    'id' => 'title',
                    'name' => 'title',
                    'style' => 'height:100px; text-transform: uppercase;',
                    'required'  => true,
                ]) 
            </div>
        </div>

        <div class="row">
            <div class="col">
                @include('components.forms.select', [
                    'input_label' => __('Jenis Perolehan'),
                    'options' => $acq_types,
                    'id' => 'acquisition_type_id',
                    'name' => 'acquisition_type_id',
                    'required'  => true,
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                @include('components.forms.input', [
                    'input_label' => __('No Kertas Pertimbangan'),
                    'id' => 'reference',
                    'name' => 'reference',
                    'readonly' => true,
                    'placeholder' => __('No yang dijana oleh sistem')
                ])
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                @include('components.forms.input', [
                    'input_label' => __('No Rujukan Fail'),
                    'id' => 'file_reference',
                    'name' => 'file_reference',
                ])
            </div>
        </div>
            
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                @include('components.forms.input', [
                    'input_label' => __('Tahun Perolehan'),
                    'type'          => 'text',
                    'id'            => 'year',
                    'name'          => 'year',
                    'min'           => 2000,
                    'maxlength'     => 4,
                    'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9]/.test(String.fromCharCode(event.charCode))'
                ])
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                @include('components.forms.input', [
                    'input_label'   => __('Tempoh Projek'),
                    'type'          => 'number',
                    'id'            => 'period_length',
                    'name'          => 'period_length',
                    'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9]/.test(String.fromCharCode(event.charCode))',
                    'required'  => true,
                ])
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                @include('components.forms.select', [
                    'input_label' => __('Jenis Tempoh'),
                    'options' => period_types()->pluck('name', 'id'),
                    'id' => 'period_type_id',
                    'name' => 'period_type_id',
                    'required'  => true,
                ])
            </div>
        </div>
        <div class="row">
            <div class="col">
                @include('components.forms.select-multiple', [
                    'input_label' => __('Lokasi'),
                    'options' => locations()->pluck('name', 'id'),
                    'id' => 'location_id',
                    'name' => 'location_id',
                    'required'  => true,
                ])
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="row">
            <div class="col">
                @include('components.forms.textarea', [
                    'input_label' => __('Skop'),
                    'id' => 'description',
                    'name' => 'description',
                    'input_container_class' => 'col quill-container',
                    'style' => 'height:480px',
                ]) 
            </div>
        </div>
    </div>
</div>