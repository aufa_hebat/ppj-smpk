<div class="selectgroup w-25 code-container-{{ $cidb_code->id }}">
  <label class="selectgroup-item">
    <input type="radio" id="cidb-code-status-{{ $cidb_code->id }}-1" name="codes[{{ $cidb_code->id }}]" value="1" class="selectgroup-input cidb-status" disabled>
    <span class="selectgroup-button">{{ __('DAN') }}</span>
  </label>
  <label class="selectgroup-item">
    <input type="radio" id="cidb-code-status-{{ $cidb_code->id }}-0" name="codes[{{ $cidb_code->id }}]" value="0" class="selectgroup-input cidb-status" disabled>
    <span class="selectgroup-button">{{ __('ATAU') }}</span>
  </label>
</div>