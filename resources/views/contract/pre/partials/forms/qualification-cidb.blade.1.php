@push('scripts')
	<script>
		jQuery(document).ready(function($) {
			$(document).on('change', '.khusus-checkbox', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
				var checked = this.checked;
				$.each($('.code-container-' + id).find('input'), function(index, val) {
					$(val).prop('disabled', !checked)
					$(val).prop('checked', false);
				});
			});	

			$('.category').hide();
            @foreach($kategori as $bil => $row2)	
				$('.category-{{ $row2->code }}').hide();
            @endforeach
						
			// console.log($kategori);
            @foreach($approval->cidbQualifications as $category)	
				if('{{$category->code->type}}' == 'category') {
					$('.category-{{ $category->code->code }}').show();
				}
            @endforeach		
		
			$('.khusus').hide();
            @foreach($approval->cidbQualifications as $khus)
                if('{{$khus->status}}' == '1'){
                    $("#cidb-code-status-{{ $khus->code->id }}-1").attr('checked',true);
                }else if('{{$khus->status}}' == '0'){
                    $("#cidb-code-status-{{ $khus->code->id }}-0").attr('checked',true);
                }
                @if($khus->code->type == "khusus")
                    $('.khusus-{{ $khus->code->category }}').show();
                @endif
            @endforeach

			$(document).on('change', '.grade-checkbox', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
				var checked = this.checked;
				$.each($('.code-container-' + id).find('input'), function(index, val) {
					$(val).prop('disabled', !checked)
					$(val).prop('checked', false);
				});
			});

			$(document).on('change', '.category-checkbox', function(event) {			

				event.preventDefault();
				var id = $(this).data('id');
				var category = $(this).data('category');
				var checked = this.checked;

				if(checked) {
					$('.khusus-' + category).show();
					$('.category-' + category).show();
				} else {
					$('.khusus-' + category).hide();
					$('.category-' + category).hide();
				}
				$.each($('.code-container-' + id).find('input'), function(index, val) {
					$(val).prop('disabled', !checked)
					$(val).prop('checked', false);
				});
			});

		});
	</script>
@endpush

<h5>Kelayakan Bidang CIDB</h5>

<div class="row">
	<div class="col-4">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>&nbsp;</th>
					<th>{{ __('Gred') }}</th>
					<th class="text-center">{{ __('Status Pilihan') }}</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach(cidb_codes('grade') as $grade)
					<tr>
						<td width="5%" class="text-center">
							@include('contract.pre.partials.forms.cidb-checkbox', [
								'cidb_code' => $grade,
								'class' => 'grade-checkbox',
								'data' => [
									'id' => $grade->id,
								],
							])
						</td>
						<td>{{ $grade->name }}</td>
						<td width="25%">
							@include('contract.pre.partials.forms.cidb-status', ['cidb_code' => $grade])
						</td>
					</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
	<div class="col-8">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>&nbsp;</th>
					<th>{{ __('Kategori') }}</th>
					<th class="text-center">{{ __('Status Pilihan') }}</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach(cidb_codes('category') as $category)
					<tr>
						<td width="5%" class="text-center">
							@include('contract.pre.partials.forms.cidb-checkbox', [
								'cidb_code' => $category,
								'class' => 'category-checkbox',
								'data' => [
									'id' => $category->id,
									'category' => $category->code,
								],
							])
						</td>
						<td>{{ $category->name }}</td>
						<td width="25%">
							@include('contract.pre.partials.forms.cidb-status', ['cidb_code' => $category])
						</td>
					</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
</div>




<div class="row">
	<div class="col">
		<div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
			@foreach(cidb_codes('category') as $category)	
			<div class=" category-{{ $category->code }}">
			<div class="mb-0 card card-primary">
				<div class="card-header" role="tab" id="{{ $category->code }}">
				<h4 class="card-title">
					<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne{{ $category->code }}" aria-expanded="false" aria-controls="collapseOne2">
						{{ $category->name }}
					</a>
				</h4>
				</div>
				<div id="collapseOne{{ $category->code }}" class="card-collapse collapse" role="tabpanel" aria-labelledby=" {{ $category->code }}">
          			<div class="card-body">				
					  	@component('components.table')
					  		@slot('thead')
								<tr>
									<th>&nbsp;</th>
							  		<th>{{ __('Pengkhususan') }}</th>
							  		<th class="text-center">{{ __('Status Pilihan') }}</th>
						  		</tr>
					  		@endslot
					  		@slot('tbody')
						  		@foreach(cidb_codes('khusus') as $khusus)
									@if($khusus->category == $category->code)
										<tr class="khusus khusus-{{ $khusus->category }}">
											<td width="5%" class="text-center">
												@include('contract.pre.partials.forms.cidb-checkbox', [
													'cidb_code' => $khusus,
													'class' => 'khusus-checkbox',
													'data' => [
														'id' => $khusus->id,
													],
												])
											</td>
											<td>{{$khusus->name}}</td>
											<td width="25%">
												@include('contract.pre.partials.forms.cidb-status', ['cidb_code' => $khusus])
											</td>							
										</tr>
									@endif
								@endforeach
							@endslot
						@endcomponent
					</div>
				</div>
			</div>
			</div>
			@endforeach
		</div>
	</div>
</div>

<div class="row">
	<div class="col">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>&nbsp;</th>
					<!-- <th>{{ __('Pengkhususan') }}</th>
					<th class="text-center">{{ __('Status Pilihan') }}</th> -->
				</tr>
			@endslot
			@slot('tbody')

			<tr>
				<td>
				@foreach(cidb_codes('category') as $category)
					<div class="category-{{ $category->code }}">
						<div>
							{{ $category->name }}							
						</div>

						<div>
						@component('components.table')
						@slot('thead')
							<tr>
								<th>&nbsp;</th>
								<th>{{ __('Pengkhususan') }}</th>
								<th class="text-center">{{ __('Status Pilihan') }}</th>
							</tr>
						@endslot
						@slot('tbody')

						@foreach(cidb_codes('khusus') as $khusus)
						@if($khusus->category == $category->code)
						<tr class="khusus khusus-{{ $khusus->category }}">
							<td width="5%" class="text-center">
								@include('contract.pre.partials.forms.cidb-checkbox', [
									'cidb_code' => $khusus,
									'class' => 'khusus-checkbox',
									'data' => [
										'id' => $khusus->id,
									],
								])
							</td>
							<td>{{$khusus->name}}</td>
							<td width="25%">
								@include('contract.pre.partials.forms.cidb-status', ['cidb_code' => $khusus])
							</td>							
						</tr>
						@endif
						@endforeach

						@endslot
						@endcomponent
						</div>
					</div>
				@endforeach
				</td>
			</tr>


				<!-- @foreach(cidb_codes('khusus') as $khusus)
					<tr class="khusus khusus-{{ $khusus->category }}">
						<td width="5%" class="text-center">
							@include('contract.pre.partials.forms.cidb-checkbox', [
								'cidb_code' => $khusus,
								'class' => 'khusus-checkbox',
								'data' => [
									'id' => $khusus->id,
								],
							])
						</td>
						<td>{{ $khusus->name }}</td>
						<td width="25%">
							@include('contract.pre.partials.forms.cidb-status', ['cidb_code' => $khusus])
						</td>
					</tr>
				@endforeach -->
			@endslot
		@endcomponent
	</div>
</div>