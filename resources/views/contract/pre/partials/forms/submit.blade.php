<div class="btn-group float-right">
	{{ html()->a(route('contract.pre.index'), __($button_name ?? 'Kembali'))->class('btn btn-outline-default border-primary') }}

	@isset($action)
		<button type="submit" class="btn btn-primary float-right send-action-btn" 
			data-route="{{ $route_name }}"
			data-form="{{ $form }}">
			@icon('fe fe-save') {{ __($action) }}
		</button>
	@else
		<button type="submit" class="btn btn-primary float-right submit-action-btn" 
			data-route="{{ $route_name }}"
			data-form="{{ $form }}">
			@icon('fe fe-save') {{ __('Simpan') }}
		</button>
	@endisset
</div>
