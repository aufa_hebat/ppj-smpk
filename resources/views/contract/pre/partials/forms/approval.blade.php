@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($approval) && ($approval->documents->count() > 0))
                @foreach($approval->documents as $doc)
                    t.row.add( [
                        '<div class="form-group">\n' +
                        '   <div class="col input-group">\n' +
                        '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                        '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                        '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                        '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
						'       <label class="input-group-text" for="uploadFile'+ counter +'"><a href="/download/{{$doc->document_path}}/{{ $doc->document_name }}" target="_blank"><i class="fas fa-download"></i></a></label>\n' +
                        '   </div>\n' +
                        '</div>',
                        '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                    ] ).draw( false );

                    counter++;
                @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow').click();
            @endif
           

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });


            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            });

            $(document).on('click', '#acq-aprv-submit', function(event) {
                event.preventDefault();                

                var id = '{{ $approval->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                axios.post(route(route_name, id), data).then( (response) => {
                    swal('{!! __('Dokumen Kelulusan Perolehan') !!}', response.data.message, 'success');
                    location.reload();
                    
                }).catch((error)=>{
                    console.log(error.response);
                });
            });

        });
    </script>
@endpush


<div class="row">
    <div class="col-12">
        <form id="upload-approval-form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            @method('PUT')

            <div class="row">
				<div class="col-6">
					@include('components.forms.input', [
						'input_label' => __('Nama Penyedia'),
						'id' => 'user_name',
						'name' => 'user_name',
						'readonly' => true,
					])
				</div>
				<div class="col-6">
					@include('components.forms.datetimepicker', [
						'input_label' 	=> __('Tarikh Disediakan'),
						'id' 			=> 'prepared_at',
                        'name' 			=> 'prepared_at',
                        'required'      => true,
						'config' 		=> [
											'format' => config('datetime.display.date'),
						]
					])
				</div>	
			</div>
			<div class="row">
				<div class="col-6">
					@include('components.forms.input', [
						'input_label' => __('Nama Penyemak'),
						'id' => 'verified_by',
						'name' => '',
						'readonly' => true,
					])
				</div>
				<div class="col-6">
					@include('components.forms.datetimepicker', [
						'input_label' 	=> __('Tarikh Disemak'),
						'id' 			=> 'verified_at',
                        'name' 			=> 'verified_at',
                        'required'      => true,
						'config' 		=> [
											'format' => config('datetime.display.date'),
						]
					])
				</div>	
			</div>
			<div class="row">
				<div class="col-6">
					@include('components.forms.select', [
						'input_label' => __('Nama Pegawai Pengulas Jab. Kewangan'),
						'id' => 'finance_officer_id',
						'name' => 'finance_officer_id',
                        'options' => finance_users()->pluck('name', 'id'),
                        'required'  => true,
					])
				</div>
				<div class="col-6">
					@include('components.forms.hidden', [
						'value' => '',
						'id' => 'user_id',
						'name' => 'user_id',
					])
					@include('components.forms.hidden', [
						'id' => 'verified_by_id',
						'name' => 'verified_by',
						'value' => ''
					])	
				</div>	
			</div>
			<div class="row">
				<div class="col-6">
					@include('components.forms.select', [
						'input_label' => __('Kelulusan'),
						'id' => 'authority_id',
                        'name' => 'authority_id',
                        'required'  => true,
						'options' => authorities()->whereIn('code', ['P', 'MOF', 'KWP'])->pluck('name', 'id'),
					])
				</div>
				<div class="col-6">
					@include('components.forms.datetimepicker', [
						'input_label' 	=> __('Tarikh Kelulusan'),
						'id' 			=> 'approved_at',
                        'name' 			=> 'approved_at',
                        'required'      => true,
						'config' 		=> [
											'format' => config('datetime.display.date'),
						]
					])	
				</div>	
			</div>

            <div id="uploads">
                <hr>
                <h4>Muat Naik Dokumen</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="float-right">
                            <button type="button" id="addRow" href="#" class="btn btn-primary">
                                @icon('fe fe-plus')
                                {{ __('Tambah Dokumen') }}
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table id="tblUpload" class="table table-sm table-transparent">
                                <thead>
                                <tr>
                                    <th>Muat Naik Dokumen</th>
                                    <th width="70px">Hapus</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <small class="float-right"><b>
                            Format : .pdf, .doc, .docx, .txt, .xls, .xlsx, .jpeg, .png, .jpg &nbsp;&nbsp;&nbsp; <br/>
                            Maksimum Jumlah Saiz Lampiran : 250MB<br/>
                            Maksimum Jumlah Lampiran : 20 Fail</b></small>
                    </div>
                </div>

            </div>
        </form>

        @if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
            <div class="btn-group float-right" id="submit">
                <button type="submit" class="btn btn-primary float-right " id="acq-aprv-submit"
                    data-route="api.contract.acquisition.approval.update"
                    data-form="upload-approval-form">
                    @icon('fe fe-save') {{ __('Simpan') }}
                </button>
            </div>
        @endif
    </div>
</div>