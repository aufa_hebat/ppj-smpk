<form id="financial-form">
	<div class="row">
		<div class="col-4">
			@include('components.forms.select', [
			    'input_label' => __('Sumber Peruntukan'),
			    'options' => allocation_resources()->pluck('name', 'id'),
			    'id' => 'allocation_resource_id',
				'name' => 'allocation_resource_id',
				'required'	=> 'true',
			])

			<div id="LainLain2">
				@include('components.forms.input', [
				    'input_label' => __('Nyatakan'),
				    'id' => 'other',
					'name' => 'other',					
			    ])
			</div>
		</div>
		<div class="col-4">
			@include('components.forms.input', [
			    'input_label' => __('Kod Setia'),
			    'id' => 'loyalty_code',
			    'name' => 'loyalty_code',
			])
		</div>
		<div class="col-4">
			<!--Anggaran Kos Projek-->
            <div class="form-group" valign="middle">
                <label for="datePicker" class="control-label">
					Anggaran Kos Projek (RM)
					<span style="color:red;"> * </span>
				</label>
				<div class="input-group" valign="bottom">
					<input class="form-control money" id="est_cost_project" name="est_cost_project" type="password"
						onkeypress="return (event.charCode === 0 )||  /[0-9,.]/.test(String.fromCharCode(event.charCode));" 
						style="text-align:right;">
				</div>                
            </div>
		</div>
	</div>
</form>
<div class="row">
	<div class="col">
		@include('contract.pre.partials.forms.submit', [
		    'route_name' => 'api.contract.acquisition.approval.finance.update',
		    'form' => 'financial-form'
		])
	</div>
</div>
<hr>
<h4>Kod Bajet
<!-- @include('components.modals.button', [
	'modal_btn_classes' => 'float-right',
	'label' => __('Kod Bajet Baru'),
	'icon' => 'fe fe-plus',
	'id' => 'kod-bajet-modal'
]) -->

<button class="btn btn-primary float-right kod-bajet" id="kod-bajet"
	@include('components.tooltip', ['tooltip' => __('Kod Bajet Baru')])
  	data-toggle="modal" data-target="#kod-bajet-modal"
  	<i class="fe fe-plus"></i>
  	Kod Bajet Baru
</button>

</h4>
<hr>
<div class="row">
	<div class="col">
		@component('components.datatable', 
			[
				'table_id' => 'contract-pre-budget-code',
				'param' => 'hashslug=' . $hashslug,
				'route_name' => 'api.datatable.contract.budget-code',
				'columns' => [
					['data' => 'business_area', 'title' => __('Business Area'), 'defaultContent' => '-'],
					['data' => 'cost_center', 'title' => __('Cost Center'), 'defaultContent' => '-'],
					['data' => 'functional_area', 'title' => __('Functional Area'), 'defaultContent' => '-'],
					['data' => 'fund', 'title' => __('Fund'), 'defaultContent' => '-'],
					['data' => 'funded_programme', 'title' => __('Funded Programme'), 'defaultContent' => '-'],
					['data' => 'gl_account', 'title' => __('GL Account'), 'defaultContent' => '-'],
					['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
				],
				'headers' => [
					__('Business Area'), __('Cost Center'), __('Functional Area'), __('Fund'), __('Funded Programme'), __('GL Account'), __('table.action')
				],
				'actions' => minify(view('contract.pre.partials.actions-budget-code', ['hashslug' => $hashslug])->render())
			]
		)
		@endcomponent
	</div>
</div>

@component('components.modals.base', [
    'id' => 'kod-bajet-modal',
    'tooltip' => __('Tambah Kod Bajet'),
    'modal_title' => __('Tambah Kod Bajet'),
    ])
    @slot('modal_body')
    <form id="budget-code-form-store">
    	<input type="hidden" name="hashslug" id="hashslug" value="{{ $hashslug }}">
		@include('contract.pre.partials.forms.budget-code')
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.kod-bajet', function(event) {
					$('#select2-business_area-container').html("");
					$('#select2-cost_center-container').html("");
					$('#select2-fund-container').html("");
					$('#select2-functional_area-container').html("");
					$('#select2-funded_programme-container').html("");
					$('#select2-gl_account-container').html("");
	            });

				$(document).on('click', '.submit-budget-code-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#budget-code-form-store').serialize();
	                var route_name = $(this).data('route');

					// clear content
					$("#kod-bajet-modal .business_area").empty();

	                axios.post(route(route_name), data).then(response => {
						if(response.data.type == 'E'){
	                    	swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'error');
						}else{
							swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'success');
						}
						
						$('#kod-bajet-modal').modal('hide');

						$('#contract-pre-budget-code').DataTable().ajax.reload();
	                });
					// location.reload();
					
	            });

			});
		</script>
	@endpush
	@endslot
	
	@if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
		@slot('modal_footer')
			<button type="submit" class="btn btn-primary float-right submit-budget-code-action-btn" 
				data-route="api.contract.acquisition.approval.budget-codes.store"
				data-form="budget-code-form-store">
				@icon('fe fe-save') {{ __('Simpan') }}
			</button>
		@endslot
	@endif
@endcomponent

@component('components.modals.base', [
    'id' => 'edit-kod-bajet-modal',
    'tooltip' => __('Kemaskini Kod Bajet'),
    'modal_title' => __('Kemaskini Kod Bajet'),
    ])
    @slot('modal_body')
    <form id="budget-code-form-update">
		@include('components.forms.hidden', [
            'name' => 'hashslug',
			'id' => 'edit_hashslug',
			'value'	=> '',
        ])
		@include('contract.pre.partials.forms.edit-budget-code')
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.edit-budget_code-action-btn', function(event) {
					event.preventDefault();
					var hashslug = $(this).data('edit_hashslug');
        			var fund = $(this).data('fund');
        			var functional_area = $(this).data('functional_area');
					var funded_programme = $(this).data('funded_programme');
					var gl_account = $(this).data('gl_account');
					var business_area = $(this).data('business_area');
					var cost_center = $(this).data('cost_center');
        			var fund_id = $(this).data('fund_id');
        			var functional_area_id = $(this).data('functional_area_id');
					var funded_programme_id = $(this).data('funded_programme_id');
					var gl_account_id = $(this).data('gl_account_id');
					var business_area_id = $(this).data('business_area_id');
					var cost_center_id = $(this).data('cost_center_id');					

					$('#select2-business_area_id-container').html(business_area);
					$('#select2-cost_center_id-container').html(cost_center);
					$('#select2-fund_id-container').html(fund);
					$('#select2-functional_area_id-container').html(functional_area);
					$('#select2-funded_programme_id-container').html(funded_programme);
					$('#select2-gl_account_id-container').html(gl_account);

        			$('#edit_hashslug').val(hashslug);
        			$('#fund_id').val(fund_id);
        			$('#functional_area_id').val(functional_area_id);
					$('#funded_programme_id').val(funded_programme_id);
					$('#gl_account_id').val(gl_account_id);
					$('#business_area_id').val(business_area_id);
					$('#cost_center_id').val(cost_center_id);
				});
				

				$(document).on('click', '.update-budget-code-action-btn', function(event) {
	                event.preventDefault();
					var id = $('#edit_hashslug').val();
	                var data = $('#budget-code-form-update').serialize();
	                var route_name = $(this).data('route');

					console.log(data);
					console.log(id);

	                axios.put(route(route_name, id), data).then(response => {
	                    if(response.data.type == 'E'){
	                    	swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'error');
						}else{
							swal('{!! __('Kelulusan Perolehan') !!}', response.data.message, 'success');
						}
					
						$('#edit-kod-bajet-modal').modal('hide');

						$('#contract-pre-budget-code').DataTable().ajax.reload();
	                });

					
	            });

			});
		</script>
	@endpush
	@endslot
	
	@if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
		@slot('modal_footer')
			<button type="submit" class="btn btn-primary float-right update-budget-code-action-btn" 
				data-route="api.contract.acquisition.approval.budget-codes.update"
				data-form="budget-code-form-store">
				@icon('fe fe-save') {{ __('Simpan') }}
			</button>
		@endslot
	@endif
@endcomponent