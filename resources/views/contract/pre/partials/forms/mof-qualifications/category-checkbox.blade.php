@include('components.forms.checkbox', [
	'id' => 'mof-code-checkbox-' . $mof_code->id,
	'name' => 'mof_category_code[' . $mof_code->id . ']',
	'class' => 'cidb-code-checkbox ' . $class,
	'data' => $data,
	'label' => '',
	'value' => $mof_code->id
])