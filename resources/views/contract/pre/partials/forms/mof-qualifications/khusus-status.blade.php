<div class="selectgroup w-25 code-container-{{ $mof_code->id }}">
  <label class="selectgroup-item">
    <input type="radio" id="mof-code-status-{{ $mof_code->id }}-1" name="mof_khusus_status[{{ $mof_code->id }}]" value="1" class="selectgroup-input cidb-status" disabled>
    <span class="selectgroup-button">{{ __('DAN') }}</span>
  </label>
  <label class="selectgroup-item">
    <input type="radio" id="mof-code-status-{{ $mof_code->id }}-0" name="mof_khusus_status[{{ $mof_code->id }}]" value="0" class="selectgroup-input cidb-status" disabled>
    <span class="selectgroup-button">{{ __('ATAU') }}</span>
  </label>
</div>