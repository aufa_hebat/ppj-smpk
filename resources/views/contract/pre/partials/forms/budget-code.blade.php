<div class="row">
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Business Area'),
            'options' => budget_code('Business Area')->pluck('name', 'id'),
            'id' => 'business_area',
            'name' => 'business_area',
            'required' => true,
        ])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Cost Centre'),
            'options' => budget_code('Cost Centre - Fun Centre')->pluck('name', 'id'),
            'id' => 'cost_center',
            'name' => 'cost_center',
            'required' => true,
        ])
	</div>
</div>
<div class="row">
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Fund'),
            'options' => budget_code('Fund')->pluck('name', 'id'),
            'id' => 'fund',
            'name' => 'fund',
            'required' => true,
        ])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Functional Area'),
            'options' => budget_code('Functional Area')->pluck('name', 'id'),
            'id' => 'functional_area',
            'name' => 'functional_area',
            'required' => true,
        ])
	</div>
</div>
<div class="row">
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Funded Programme'),
            'options' => budget_code('Funded Programme')->pluck('name', 'id'),
            'id' => 'funded_programme',
            'name' => 'funded_programme',
            'required' => true,
        ])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('GL Account'),
            'options' => budget_code('GL Account')->pluck('name', 'id'),
            'id' => 'gl_account',
            'name' => 'gl_account',
            'required' => true,
        ])
	</div>
</div>