@push('scripts')
	<script>
		jQuery(document).ready(function($) {
			$(document).on('change', '.khusus-mof-checkbox', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
				var checked = this.checked;
				$.each($('.code-container-' + id).find('input'), function(index, val) {
					$(val).prop('disabled', !checked)
					$(val).prop('checked', false);
				});
			});	

			$('.category-mof').hide();
			$('.khusus-mof').hide();
            @foreach($kategori_mof as $bil => $row2)	
				$('.category-mof-{{ $row2->code }}').hide();
            @endforeach
						
            @foreach($approval->mofQualifications as $category)
				if('{{$category->code->type}}' == 'category') {
					$('.category-mof-{{ $category->code->code }}').show();
					$('.khusus-mof-{{ $category->code->code }}').show();
				}
            @endforeach		
		
			
            @foreach($approval->mofQualifications as $khus)
                if('{{$khus->status}}' == '1'){
                    $("#mof-code-status-{{ $khus->code->id }}-1").attr('checked',true);
                }else if('{{$khus->status}}' == '0'){
                    $("#mof-code-status-{{ $khus->code->id }}-0").attr('checked',true);
                }
                @if($khus->code->type == "khusus")
                    $('.khusus-mof-{{ $khus->code->category }}').show();
                @endif
            @endforeach

			$(document).on('change', '.category-mof-checkbox', function(event) {			

				event.preventDefault();
				var id = $(this).data('id');
				var category = $(this).data('category');
				var checked = this.checked;

				if(checked) {
					$('.khusus-mof-' + category).show();
					$('.category-mof-' + category).show();
				} else {
					$('.khusus-mof-' + category).hide();
					$('.category-mof-' + category).hide();
				}
				$.each($('.code-container-' + id).find('input'), function(index, val) {
					$(val).prop('disabled', !checked)
					$(val).prop('checked', false);
				});
			});

		});
	</script>
@endpush

<h5>Kelayakan Bidang MOF</h5>

<div class="row">
	<div class="col-12">
		@component('components.table')
			@slot('thead')
				<tr>
					<th>&nbsp;</th>
					<th>{{ __('Kategori') }}</th>
					<th class="text-center">{{ __('Status Pilihan') }}</th>
				</tr>
			@endslot
			@slot('tbody')
				@foreach(mof_codes('category') as $category)
					<tr>
						<td width="5%" class="text-center">
							@include('contract.pre.partials.forms.mof-qualifications.category-checkbox', [
								'mof_code' => $category,
								'class' => 'category-mof-checkbox',
								'data' => [
									'id' => $category->id,
									'category' => $category->code,
								],
							])
						</td>
						<td>{{ $category->name }}</td>
						<td width="25%">
							@include('contract.pre.partials.forms.mof-qualifications.category-status', ['mof_code' => $category])
						</td>
					</tr>
				@endforeach
			@endslot
		@endcomponent
	</div>
</div>


<div class="row">
	<div class="col">
		<div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
			@foreach(mof_codes('category') as $category)	
			<div class=" category-mof-{{ $category->code }}">
			<div class="mb-0 card card-primary">
				<div class="card-header" role="tab" id="{{ $category->code }}">
				<h4 class="card-title">
					<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne{{ $category->code }}" aria-expanded="false" aria-controls="collapseOne2">
						{{ $category->name }}
					</a>
				</h4>
				</div>
				<div id="collapseOne{{ $category->code }}" class="card-collapse collapse" role="tabpanel" aria-labelledby=" {{ $category->code }}">
          			<div class="card-body">				
					  	@component('components.table')
					  		@slot('thead')
								<tr>
									<th>&nbsp;</th>
							  		<th>{{ __('Pengkhususan') }}</th>
							  		<th class="text-center">{{ __('Status Pilihan') }}</th>
						  		</tr>
					  		@endslot
					  		@slot('tbody')
						  		@foreach(mof_codes('khusus') as $khusus)
									@if($khusus->category == $category->code)
										<tr class="khusus khusus-mof-{{ $khusus->category }}">
											<td width="5%" class="text-center">
												@include('contract.pre.partials.forms.mof-qualifications.khusus-checkbox', [
													'mof_code' => $khusus,
													'class' => 'khusus-mof-checkbox',
													'data' => [
														'id' => $khusus->id,
													],
												])
											</td>
											<td>{{$khusus->name}}</td>
											<td width="25%">
												@include('contract.pre.partials.forms.mof-qualifications.khusus-status', ['mof_code' => $khusus])
											</td>							
										</tr>
									@endif
								@endforeach
							@endslot
						@endcomponent
					</div>
				</div>
			</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
