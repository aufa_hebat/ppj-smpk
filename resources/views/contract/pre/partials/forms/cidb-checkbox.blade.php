@include('components.forms.checkbox', [
	'id' => 'cidb-code-checkbox-' . $cidb_code->id,
	'name' => 'cidb_code[' . $cidb_code->id . ']',
	'class' => 'cidb-code-checkbox ' . $class,
	'data' => $data,
	'label' => '',
	'value' => $cidb_code->id
])