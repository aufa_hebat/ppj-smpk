@component('components.action')
	@slot('append_action')
		<a class="dropdown-item document-action-btn" style="cursor: pointer;"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '" 
		>
			@icon('fe fe-file') {{ __('Dokumen') }}
		</a>
	@endslot
@endcomponent

