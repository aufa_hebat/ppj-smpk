<h4>Kelulusan</h4>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Nama Penyedia'),
            'id' => 'prepare_name',
            'name' => 'prepare_name',
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Tarikh Disediakan'),
            'id' => 'prepared_date',
            'name' => 'prepared_date',
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Nama Penyemak'),
            'id' => 'verifier_name',
            'name' => 'verifier_name',
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Tarikh Disemak'),
            'id' => 'verify_date',
            'name' => 'verify_date',
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Nama Pegawai Pengulas Jab. Kewangan'),
            'id' => 'finance_officer',
            'name' => 'finance_officer',
        ])
    </div>

</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Kelulusan'),
            'id' => 'authority_id',
            'name' => 'authority_id',
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Tarikh Kelulusan'),
            'id' => 'approve_date',
            'name' => 'approve_date',
        ])
    </div>    
</div>

<hr>
<div class="row">
    <div class="col" width="100%">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>NAMA FAIL</th>
                </tr>
                </thead>
                <tbody>
                    @if(!empty($approval->documents))
                        @foreach($approval->documents as $doc)
                        <tr>
                            <td>
                                <a href="/download/{{$doc->document_path}}/{{ $doc->document_name }}" target="_blank">{{ $doc->document_name }}<i class="fas fa-download"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    
                </tbody>
            </table>
        </div>
    </div>
</div>