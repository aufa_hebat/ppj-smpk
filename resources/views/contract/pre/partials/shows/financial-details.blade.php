<h4>Maklumat Kewangan</h4>
<br>
<div class="row">
    <div class="col-4">
        <div class="text-muted">Sumber Peruntukan</div>
        <div>{!! optional($approval->allocation_resource)->name !!}@if(!empty($approval->other)) : {!! $approval->other !!}@endif</div>
    </div>
    <div class="col-4">
        <div class="text-muted">Kod Setia</div>
        <p>{!! $approval->loyalty_code !!}</p>
    </div>
    <div class="col-4">
        <!--Anggaran Kos Projek-->
        <div class="form-group">
            <div class="text-muted">Anggaran Kos Projek (RM)</div>
            <div class="input-group" valign="bottom">
                
                <input class="form-control" id="est_cost_project" name="est_cost_project" type="password"
                   onkeypress="return (event.charCode === 0 )||  /[0-9,.]/.test(String.fromCharCode(event.charCode));" 
                   style="text-align:right;" readonly>                
            </div>
            
        </div>
    </div>
</div>

<hr>
<div class="row">
    <div class="col">
        @component('components.datatable', 
            [
                'table_id' => 'contract-pre-budget-code',
                'param' => 'hashslug=' . $hashslug,
                'route_name' => 'api.datatable.contract.budget-code',
                'columns' => [
                    ['data' => 'business_area', 'title' => __('Business Area'), 'defaultContent' => '-'],
                    ['data' => 'cost_center', 'title' => __('Cost Center'), 'defaultContent' => '-'],
                    ['data' => 'functional_area', 'title' => __('Functional Area'), 'defaultContent' => '-'],
                    ['data' => 'fund', 'title' => __('Fund'), 'defaultContent' => '-'],
                    ['data' => 'funded_programme', 'title' => __('Funded Programme'), 'defaultContent' => '-'],
                    ['data' => 'gl_account', 'title' => __('GL Account'), 'defaultContent' => '-'],
                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                ],
                'headers' => [
                    __('Business Area'), __('Cost Center'), __('Functional Area'), __('Fund'), __('Funded Programme'), __('GL Account'), __('')
                ],
                'actions' => minify('')
            ]
        )
        @endcomponent
    </div>
</div>
