<h4>Maklumat Kelayakan</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Syarat Pendaftaran</div>
        <div @if($approval->cidb != '1') style="display:none" @endif>
                <i class="fe fe-check-square" style="color: green;"></i>&nbsp;
            Lembaga Pembangunan Industri Pembinaan (CIDB)
        </div>
        <div @if($approval->bpku_bumiputera != '1') style="display:none" @else style="margin-left: 5%" @endif >
                <i class="fe fe-check-square" style="color: green;"></i>&nbsp;
            Bahagian Pembangunan Kontraktor dan Usahawan (BPKU)
        </div>
        <div @if($approval->spkk != '1') style="display:none" @else style="margin-left: 5%" @endif>
               <i class="fe fe-check-square" style="color: green;"></i>&nbsp;
            Sijil Perolehan Kerja Kerajaan (SPKK)
        </div>
        <div @if($approval->mof != '1') style="display:none" @endif>
                <i class="fe fe-check-square" style="color: green;"></i>&nbsp;
            MOF
        </div>
        <div @if($approval->bumiputera != '1') style="display:none" @else style="margin-left: 5%" @endif>
                <i class="fe fe-check-square" style="color: green;"></i>&nbsp;
            Bumiputera
        </div>
        <div @if($approval->ssm != '1') style="display:none" @endif>
               <i class="fe fe-check-square" style="color: green;"></i>&nbsp;
            Suruhanjaya Syarikat Malaysia (SSM)
        </div>
    </div>
</div>
<br>

@if($approval->acquisition_type_id == '1' || $approval->acquisition_type_id == '3')
    @include('contract.pre.partials.shows.qualification-mof-details')
@else
    @include('contract.pre.partials.shows.qualification-cidb-details')
@endif
