<h4>Maklumat Perolehan</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Tajuk</div>
        <p>{!! $approval->file_reference . ' - ' . $approval->title !!}</p>
        <div class="row">
            <div class="col-3">
                <div class="text-muted">Jenis Perolehan</div>
                <p>{!! optional($approval['type'])->name !!}</p>
            </div>
            <div class="col-3">
                <div class="text-muted">Tahun Perolehan</div>
                <p>{!! $approval->year !!}</p>
            </div>
            <div class="col-3">
                <div class="text-muted">No. Kertas Pertimbangan</div>
                <p>{!! $approval->reference or '-' !!}</p>
            </div>
            <div class="col-3">
                <div class="text-muted">No. Rujukan Fail</div>
                <p>{!! $approval->file_reference or '-' !!}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="text-muted">Tempoh Projek</div>
                <p>{!! $approval->period_length or '-' !!} {!! optional($approval->period_type)->name !!}</p>
            </div>
            <div class="col-3">
                <div class="text-muted">Lokasi</div>
                <p>{!! $approval->locations_to_string or '-' !!}</p>
            </div>
            <div class="col-6">
                <div class="text-muted">Skop</div>
                <p>{!! $approval->description !!}</p>
            </div>
        </div>
    </div>
</div>