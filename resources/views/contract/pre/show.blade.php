@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{asset('css/example.wink.css')}}">
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
    <script src="{{asset('js/hideShowPassword.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>

        // Example 1:
        // - Password hidden by default
        // - Inner toggle shown
        $('#est_cost_project').hidePassword(true);
        

        // Example 2:
        // - Password shown by default
        // - Toggle shown on 'focus' of element
        // - Custom toggle class
        $('#password-2').showPassword('focus', {
            toggle: { className: 'my-toggle' }
        });

        // Example 3:
        // - When checkbox changes, toggle password
        //   visibility based on its 'checked' property
        $('#show-password').change(function(){
            $('#password-3').hideShowPassword($(this).prop('checked'));
        });

    </script>
    <script>
    	jQuery(document).ready(function($) {
            /*$('#approval_title').val('{!! $approval->file_reference . ' ' . $approval->title !!}').attr('readonly',true);*/
            $('#approval_title').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$approval->file_reference . ' ' . $approval->title) !!}').attr('readonly',true);
    		$('#approval_reference').val('{!! $approval->reference !!}').attr('readonly',true);
    		$('#approval_description').val('{!! $approval->description !!}').attr('readonly',true);
    		$('#acquisition_type').val('{!! $approval['type']->name !!}').attr('readonly',true);
    		$('#period_length').val('{!! $approval->period_length or '-' . ' ' . $approval['period_type']->name !!}').attr('readonly',true);

            $('#est_cost_project').val('{{ money()->toCommon($approval->est_cost_project ?? "0",2 ) }}');
    		
    		@if($approval->cidb == true)
				$('#cidb').val('Lembaga Pembangunan Industri Pembinaan (CIDB)').attr('readonly',true);
			@endif
			@if($approval->ssm == true)
				$('#ssm').val('Suruhanjaya Syarikat Malaysia (SSM)').attr('readonly',true);
			@endif
			@if($approval->pp == true)
				$('#pp').val('Bumiputera').attr('readonly',true);
			@endif
			@if($approval->bpku_bumiputera == true)
				$('#bpku_bumiputera').val('Bahagian Pembangunan Kontraktor dan Usahawan (BPKU)').attr('readonly',true);
			@endif
			@if($approval->spkk == true)
				$('#spkk').val('Sijil Perolehan Kerja Kerajaan (SPKK)').attr('readonly',true);
			@endif

            @if(!empty($approval->prepared_at))
                $('#prepared_date').val('{{ $approval->prepared_at->format("d/m/Y") }}').attr('readonly',true);
            @endif

            @if(!empty($approval->verified_at))
                $('#verify_date').val('{{ $approval->verified_at->format("d/m/Y") }}').attr('readonly',true);
            @endif

            @if(!empty($approval->approved_at))
                $('#approve_date').val('{{ $approval->approved_at->format("d/m/Y") }}').attr('readonly',true);
            @endif

			$('#prepare_name').val('{!! $approval->user->name !!}').attr('readonly',true);
			$('#verifier_name').val('{!! $approval->user->supervisor->name or "-" . ' ' . $approval->title !!}').attr('readonly',true);
			$('#finance_officer').val('{!! $approval->financeOfficer->name or "-" !!}').attr('readonly',true);
            $('#authority_id').val('{!! $approval->authority->name or "-" !!}').attr('readonly',true);
            $('#dataTable').DataTable();
    	});
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#acquisition-details" role="tab" aria-controls="acquisition-details" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Perolehan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#qualification-details" role="tab" aria-controls="qualification-details" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Kelayakan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#financial-details" role="tab" aria-controls="financial-details" aria-selected="false">
                    @icon('fe fe-dollar-sign')&nbsp;{{ __('Kewangan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#approval-details" role="tab" aria-controls="approval-details" aria-selected="false">
                    @icon('fe fe-thumbs-up')&nbsp;{{ __('Kelulusan') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'acquisition-details', 'active' => true])
                            @slot('content')
                                @include('contract.pre.partials.shows.acquisition-details', ['approval' => $approval])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'qualification-details'])
                            @slot('content')
                                @include('contract.pre.partials.shows.qualification-details', ['approval' => $approval])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'financial-details'])
                            @slot('content')
                                @include('contract.pre.partials.shows.financial-details', ['hashslug' => $approval->hashslug])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'approval-details'])
                            @slot('content')
                                @include('contract.pre.partials.shows.approval-details', ['hashslug' => $approval->hashslug])
                            @endslot
                        @endcomponent

                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('contract.pre.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection








