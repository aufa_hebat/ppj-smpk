@extends('layouts.admin')

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.evaluation.partials.scripts')
			@component('components.card')
				@slot('card_body')
					@component('components.datatable',
						[
							'table_id' => 'contract-pre-evaluation',
							'route_name' => 'api.datatable.contract.evaluation',
							'columns' => [
								['data' => 'no_perolehan', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'tajuk', 'title' => __('Tajuk'), 'defaultContent' => '-'],
								['data' => 'syarikat_terpilih', 'title' => __('Syarikat Terpilih'), 'defaultContent' => '-'],
								['data' => 'harga_bersih', 'title' => __('Harga Bersih'), 'defaultContent' => '-'],
								['data' => 'tarikh_perolehan', 'title' => __('Tarikh Tutup Perolehan'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Perolehan'), __('Tajuk'), __('Syarikat Terpilih'), __('Harga Bersih'), __('Tarikh Tutup Perolehan'), __('Tindakan')
							],
							'actions' => minify(view('contract.pre.evaluation.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection