@extends('layouts.admin')

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@component('components.card')
				@slot('card_body')
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="form-group">
								<div class="text-muted">Tajuk Perolehan</div>
								<div>{{ $acquisition->title }}</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<div class="text-muted">No. {{$acquisition->category->name}}</div>
								<div>{!! $acquisition->reference !!}</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<div class="text-muted">{{ __('Bil Mesyuarat') }}</div>
								<div>@if(!empty($acquisition->evaluation)) {!! $acquisition->evaluation->meeting_no !!} @endif</div>
							</div>
							<div class="form-group">
								<div class="text-muted">{{ __('Syarikat Terpilih') }}</div>
								<div>@if(!empty($acquisition->appointed[0])) {!! $acquisition->appointed[0]->company->company_name !!} @endif</div>
							</div>
							<div class="form-group">
								<div class="text-muted">{{ __('Keputusan Mesyuarat Yang telah Di Muat Naik') }}</div>
								<div>
									@if(!empty($acquisition->evaluation) && !empty($acquisition->evaluation->document))
										<a href="/download/{{ $acquisition->evaluation->document->document_path }}/{{ $acquisition->evaluation->document->document_name }}" target="_blank">{{ $acquisition->evaluation->document->document_name }}</a>
									 @endif
								</div>
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
                                                             <!-- Tarikh mesyuarat ikut jenis perolehan, if tender ('Tarikh Mesyuarat Tender') else ('Tarikh Mesyuarat
                                                             SH')
                                                             -->
                                                             @if($acquisition->category->id == 3 || $acquisition->category->id == 4)
                                                             <div class="text-muted">{{ __('Tarikh Mesyuarat Tender') }}</div>
                                                             @else
                                                             <div class="text-muted">{{ __('Tarikh Mesyuarat SH') }}</div>
                                                             @endif
								
								<div>
									@if(!empty($acquisition->evaluation->meeting_date))
									{!! \Carbon\Carbon::createFromFormat('Y-m-d',$acquisition->evaluation->meeting_date)->format('d/m/Y') !!}
									@endif
								</div>
							</div>
							<div class="form-group">
								<div class="text-muted">{{ __('Harga Bersih') }}</div>
								<div>
									@if(!empty($acquisition->appointed[0]))
										{!! money()->toHuman($acquisition->appointed[0]->offered_price) !!}
									@endif
								</div>
							</div>
						</div>
					</div>
				@endslot

				@slot('card_footer')
					<div class="float-right btn-group">
						<a href="{{ route('evaluation.index') }}"
						   class="btn btn-default border-primary">
							{{ __('Kembali') }}
						</a>
					</div>
				@endslot
			@endcomponent
		</div>
	</div>
@endsection