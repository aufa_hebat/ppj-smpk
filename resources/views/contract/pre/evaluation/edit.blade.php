@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {
            @if($acquisition->evaluation->meeting_date)
                $('#meeting_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$acquisition->evaluation->meeting_date)->format('d/m/Y')  }}');
            @endif

            @if( $acquisition->evaluation->document )
                    $('#subFile').val('{{ $acquisition->evaluation->document->document_name }}')
            @endif

            $(".confirm").on('click', function(e){
               $("#confirmation").val(1);
            });

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            $('#meeting_no').val('{{$acquisition->evaluation->meeting_no}}');

            @if(($acquisition->appointed->count()> 0) && !empty($acquisition->appointed[0]->offered_price) )
                $('#offered_price').val('{{ money()->toHuman($acquisition->appointed[0]->offered_price) }}');
                $('#h_offered_price').val('{{ $acquisition->appointed[0]->offered_price }}');
            @else
                if($('#pilihSyarikat').val() == ''){
                    $('#offered_price').val(null);
                    $('#h_offered_price').val(null);
                }
            @endif


            $('#pilihSyarikat').change(function() {
                @foreach($acquisition->boxes as $box)
                    if(this.value == '{{$box->sale->company_id}}'){
                        @if($box->amount)
                            $('#offered_price').val('{{ money()->toHuman($box->amount) }}');
                            $('#h_offered_price').val('{{ $box->amount }}');
                        @endif

                    }
                    else if(this.value == ""){
                        $('#offered_price').val('');
                        $('#h_offered_price').val('');
                    }
                @endforeach
            });
            
            
             $(document).on('click', '#selesai_submit_button', function(event) {
               // if (confirm("Click OK to continue?")){
                    // $('form#delete').submit();
               //}             
                    swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti untuk selesaikan keputusan ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                       $('form#box-form').submit();
                    }
                });
                            
            });
            
            
        });

    </script>
@endpush

@section('content')
	<form id="box-form"  method="POST" action="{{ route('evaluation.update',$acquisition->id) }}" files = "true" enctype="multipart/form-data">
		@csrf
		@method('PUT')
		<div class="row">
            @component('components.card', ['card_classes' => 'col-12'])
                @slot('card_body')
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="text-muted">Tajuk Perolehan</div>
                                <div>{{ $acquisition->title }}</div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <div class="text-muted">No. {{$acquisition->category->name}}</div>
                                <div>{!! $acquisition->reference !!}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.input', [
                                'input_label' => __('Bil Mesyuarat'),
                                'id' => 'meeting_no',
                                'name' => 'meeting_no',
                            ])
                            <div class="form-group row">
                                <label for="SyarikatTerpilih"
                                       class="col col-form-label">
                                    Syarikat Terpilih
                                </label>

                                <div class="col">
                                    <select id="pilihSyarikat" class="select2 form-control" name="company_id">
                                        <option value="">Sila Pilih</option>
                                        @foreach($companies as $company)
                                            <option
                                                    value="{{$company->id}}"
                                                    @if($acquisition->appointed->count() > 0)
                                                        {{$company->id == $acquisition->appointed[0]->company_id ? 'selected' : ''}}
                                                    @endif

                                            >{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('company_id'))
                                        <span class="text-danger">{{ $errors->first('company_id') }}</span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group row">
                                <label for="Fail_Keputusan_Mesyuarat"
                                       class="col col-form-label">
                                    Muat Naik Keputusan Mesyuarat
                                </label>

                                <div class="col input-group">
                                    <input id="subFile" type="text"  class="form-control" readonly>
                                    <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                                    <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                    @if(!empty($acquisition->evaluation->document))
                                            <label class="input-group-text" for="downloadFile">
                                                <a href="/download/{{ $acquisition->evaluation->document->document_path }}/{{ $acquisition->evaluation->document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                            </label>

                                    @endif                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            @include('components.forms.datetimepicker', [
                                'input_label' => __('Tarikh Mesyuarat J/K Sebut Harga'),
                                'id' => 'meeting_date',
                                'name' => 'meeting_date',
                                'config' => ['format' => config('datetime.display.date')],
                                'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))'
                            ])
                            @include('components.forms.input', [
                                'input_label' => __('Harga Bersih'),
                                'id' => 'offered_price',
                                'name' => 'offered_price',
                                'readonly' => true,
                            ])
                            @include('components.forms.hidden', [
                                'id' => 'h_offered_price',
                                'name' => 'h_offered_price',
                                'value' => '',
                            ])
                        </div>
                    </div>

                @endslot
        
                @slot('card_footer')
                    <div class="float-right btn-group">
                        <a href="{{ route('evaluation.index') }}"
                           class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                        </a>
                        <button type="submit" class="btn btn-primary">
                            @icon('fe fe-save') {{ __('Simpan') }}
                        </button>
                        @if(!empty($acquisition->evaluation->meeting_date) && !empty($acquisition->evaluation->document))
                            <input id="confirmation" name="confirmation" type="hidden"/>
                            <button type="button" id="selesai_submit_button" type="submit" class="btn btn-success float-middle border-default confirm">
                                @icon('fe fe-check') {{ __('Selesai') }}
                            </button>
                        @endif
                    </div>
                @endslot
			@endcomponent
		</div>
	</form>
@endsection