<div class="WordSection1" id="customers{{$acquisition->id}}" style="background-color: white;">
    <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>
    <p class=MsoNormal align=center style='margin-left:7.1pt;text-align:center; font-size: medium'><u><strong>JADUAL {{strtoupper($acquisition->category->name)}}</strong></u></p>

    <p class=MsoNormal align=center style='margin-left:7.1pt;text-align:center'><strong>{{$acquisition->title}}</strong></p>
    <p class=MsoNormal align=center style='margin-left:7.1pt;text-align:center'><strong> No. {{$acquisition->category->name}} : {{strtoupper($acquisition->reference)}} </strong></p>
    <p class=MsoNormal align=center style='margin-left:7.1pt;text-align:center'>&nbsp;</p>

    <table>
        <tr>
            <td>
                Tarikh Tutup {{$acquisition->category->name}}
            </td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td>

                <strong>
                    @php
                        $fmt = new IntlDateFormatter(config('app.locale'),IntlDateFormatter::GREGORIAN,IntlDateFormatter::NONE);
                        echo $fmt->format(\Carbon\Carbon::parse($acquisition->closed_at,config('app.timezone')));
                    @endphp
                </strong>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Kelas/Tajuk/Tajuk Kecil Yang Ditetapkan
            </td>
            <td valign="top">:</td>
            <td></td>
            <td></td>
            <td></td>
            <td valign="top">
                @if($acquisition->approval->cidb == '0')
                    <strong>Berdaftar dengan Kementerian Kewangan</strong>
                @elseif ($acquisition->approval->cidb == '1')
                    <strong>Berdaftar dengan  Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB)</strong>
                @endif

                @if(!empty($cidb))
                        <strong>, {{ $cidb }}</strong>
                @endif

                <strong> @if ($acquisition->approval->bpku_bumiputera == '1') serta {{"Bumiputera"}}@endif </strong>
            </td>
        </tr>
        <!--
        <tr>
            <td valign="top">
                Anggaran Jabatan Bedasarkan BQ
            </td>
            <td valign="top">:</td>
            <td></td>
            <td></td>
            <td></td>
            <td valign="top">
                {!! money()->toHuman($acquisition->bqElements->pluck('amount')->sum()) !!}
            </td>
        </tr>   -->     
        <tr>
            <td valign="top">
                Anggaran Jabatan
            </td>
            <td valign="top">:</td>
            <td></td>
            <td></td>
            <td></td>
            <td valign="top">
                {!! money()->toHuman($acquisition->new_department_estimation) !!}
            </td>
        </tr>        
    </table><br>

    <table align="center" class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%" style='border-collapse:collapse;border:none'>
        <thead>
            <tr>
                <th valign=top style='border:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt'>KOD
                </th>
                <th valign=top style='border:solid windowtext 1.0pt; border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>NAMA SYARIKAT
                </th>
                <th valign=top style='border:solid windowtext 1.0pt; border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    @if($acquisition->approval->cidb == '0') KEMENTERIAN KEWANGAN
                    @elseif($acquisition->approval->cidb == '1') CIDB
                    @endif
                </th>
                <th valign=top style='border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>TARAF
                </th>
                <th valign=top style='border:solid windowtext 1.0pt; border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>HARGA BERSIH (RM)
                </th>
                <th valign=top style='border:solid windowtext 1.0pt; border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>TEMPOH SIAP
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($acquisition->boxes->sortBy('no') as $bil => $row)
                {{--@if($row->jualan->id_Perolehan == $perolehan->id_Perolehan)--}}
                    <tr>
                        <td valign=top style='border:solid windowtext 1.0pt; border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>{{$row->no}}/{{$acquisition->boxes->count()}}
                        </td>
                        <td valign=top style='text-transform:uppercase; border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt'>{{$row->sale->company->company_name}}
                        </td>
                        <td valign=top style='border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                            @if(($acquisition->approval->cidb == '1' && !empty($row->sale->company->cidb_no)) || ($acquisition->approval->cidb == '0' && !empty($row->sale->company->mof_no)))
                                BERDAFTAR
                            @else
                                TIDAK BERDAFTAR
                            @endif
                        </td>
                        <td valign=top style='border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                            @if(($acquisition->approval->cidb == '1' && !empty($row->sale->company->bpku_no)) || ($acquisition->approval->cidb == '0' && !empty($row->sale->company->bumiputra_no)))
                                BUMIPUTERA
                            @else
                                BUKAN BUMIPUTERA
                            @endif
                        </td>
                        <td valign=top style='border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                            @if($row->is_stated == '2')
                            LAIN-LAIN
                            @elseif(!empty($row->amount))
                            {{money()->toHuman($row->amount)}}
                            @else
                            TIDAK DINYATAKAN
                            @endif

                        </td>
                        <td valign=top style='border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                            @if(!empty($row->period))  {{$row->period}} @endif
                            @if(!empty($row->period_type->name))  {{$row->period_type->name}}@endif
                        </td>
                    </tr>
                {{--@endif--}}
            @endforeach
        </tbody>
    </table>



    <div>
        <table border="0" style="width: 100%">
            <tr>
                <td>
                    <table border="0" style="width: 100%">
                        <tr>
                            <td colspan="2">
                                <center>
                                    <p class=MsoNormal align="center" style='margin-left:14.2pt'>Ulasan : {{ $acquisition->box_review }}</p>
                                    <p class=MsoNormal align="center" style='margin-left:14.2pt'>Sebanyak <b style="text-transform: uppercase">
                                        <?php
                                            $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                                            echo $f->format($acquisition->boxes->count());
                                        ?> 
                                        ({{$acquisition->boxes->count()}})</b>
                                        tawaran
                                        {{$acquisition->category->name}}
                                        telah diterima dan dibuka pada
                                        <b style="text-decoration: underline">
                                            @php
                                                $fmt = new IntlDateFormatter(config('app.locale'),IntlDateFormatter::GREGORIAN,IntlDateFormatter::NONE);
                                                echo $fmt->format(\Carbon\Carbon::parse($acquisition->closed_at,config('app.timezone')));
                                            @endphp
                                        </b> 
                                        jam <b style="text-decoration: underline">12.01</b> tengahari / petang seperti jadual
                                        {{$acquisition->category->name}} di atas.
                                    </p>
                    
                                    <p class=MsoNormal style='margin-left:14.2pt'>&nbsp;</p>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td><center>......................................</center></td>
                            <td><center>......................................</center></td>
                        </tr>
                        @if($acquisition->committeeApprovals->count() > 0)
                        <tr>
                            <td><center>@if( !empty($acquisition->committeeApprovals[0])) {{ $acquisition->committeeApprovals[0]->name }} @endif</center></td>
                            <td><center>@if( !empty($acquisition->committeeApprovals[1])) {{ $acquisition->committeeApprovals[1]->name }} @endif</center></td>
                        </tr>
                        <tr>
                            <td><center>@if( !empty($acquisition->committeeApprovals[0]->position)) {{ $acquisition->committeeApprovals[0]->position->name }} @endif</center></td>
                            <td><center>@if( !empty($acquisition->committeeApprovals[1]->position)) {{ $acquisition->committeeApprovals[1]->position->name }} @endif</center></td>
                        </tr>
                        <tr>
                            <td><center>@if( !empty($acquisition->committeeApprovals[0])) {{ $acquisition->committeeApprovals[0]->role }} @endif</center></td>
                            <td><center>@if( !empty($acquisition->committeeApprovals[1])) {{ $acquisition->committeeApprovals[1]->role }} @endif</center></td>
                        </tr>
                        @endif                        
                    </table>
                </td>
            </tr>
        </table>
    </div>          
</div>

