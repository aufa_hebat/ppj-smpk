@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {
            $('.money').mask('000,000,000.00', {reverse: true});

            $('#tblSyarikat').DataTable({ "ordering": false, "paging": false, "info": false });
            var countCompany = '{{ $boxes->count() }}';
            $('.countCompany').html(countCompany);

            @if(!empty($bjh_doc))
                @foreach($bjh_doc as $doc)
                    $('#subFile').val('{{ $doc->document_name }}')
                @endforeach
            @endif

            $(".confirm").on('click', function(e){
               $("#confirmation").val(1);
            });

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });
            

            @foreach($sales as $index => $sale)
                $('#is_stated{{ $sale->id }}').change(function (){
                    if(this.value == 0){
                        $('#amount{{ $sale->id }}').attr('disabled', true);
                        $('#amount{{ $sale->id }}').val(null);
                    }
                    else{
                        $('#amount{{ $sale->id }}').attr('disabled', false);
                    }

                });

                $('#cbBox{{ $sale->id }}').change(function (){
                    $('#no{{ $sale->id }}').attr('disabled', !this.checked);
                    $('#period{{ $sale->id }}').attr('disabled', !this.checked);
                    $('#period_type_id{{ $sale->id }}').attr('disabled', !this.checked);
                    $('#is_stated{{ $sale->id }}').attr('disabled', !this.checked);
                    $('#amount{{ $sale->id }}').attr('disabled', !this.checked);

                    if(!this.checked){
                        $('#no{{ $sale->id }}').val(null);
                        $('#period{{ $sale->id }}').val(null);
                        $('#period_type_id{{ $sale->id }}').val('1');
                        $('#is_stated{{ $sale->id }}').val('1');
                        $('#amount{{ $sale->id }}').val(null);

                        countCompany--;
                    }
                    else{ countCompany++; }

                    $('.countCompany').html(countCompany);

                });

                if(!$('#cbBox{{ $sale->id }}').is(":checked")){
                    $('#no{{ $sale->id }}').attr('disabled', true);
                    $('#period{{ $sale->id }}').attr('disabled', true);
                    $('#period_type_id{{ $sale->id }}').attr('disabled', true);
                    $('#is_stated{{ $sale->id }}').attr('disabled', true);
                    $('#amount{{ $sale->id }}').attr('disabled', true);
                }

                if($('#is_stated{{ $sale->id }}').val() == "0"){
                    $('#amount{{ $sale->id }}').attr('disabled', true);
                }

            @endforeach

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->id }}';                
                var route_name = 'box.update';
                var form_id = 'box-form';
                var form = document.forms[form_id];
                var data = new FormData(form);

                var checkBoxNo = '-1';
                // Display the key/value pairs
                for (var pair of data.entries()) {
                    //console.log(pair[0]+ ', ' + pair[1]); 
                    //din checking value period, set as mandatory
                    //alert(pair[0]+ ', ' + pair[1]);
                    
                   
                    if(pair[0] == 'cbBox[]'){
                       // alert(pair[0]+ ', ' + pair[1]);
                       checkBoxNo = pair[1];
                    }
                   
                    var periodId = 'period'.concat(checkBoxNo); 
                    //alert(periodId);
                    
                    //alert(periodId);
                    //if ( pair[0].indexOf(periodId) >= 0){
                       // alert(pair[1]);
                   // }
                   var x = pair[0];
                   
                   // alert(periodId+ ', ' +x);
                   //alert(pair[0]+ ', ' + pair[1]);
                    if(x == periodId){
                        //alert(pair[1]);
                        if(pair[1] == ''){
                             swal('Buka Peti', 'Ruangan tempoh mestilah dipenuhkan.', 'error');
                        return;
                        }
                       
                    }
                    
                    
                }
                
               

                axios.post(route(route_name, id), data).then(response => {
                    swal('Buka Peti', 'Rekod telah berjaya dikemaskini.', 'success');
                    location.reload();
                });
                
            });
            
            
            $(document).on('click', '#selesai_submit_button', function(event) {
               // if (confirm("Click OK to continue?")){
                    // $('form#delete').submit();
               //}             
                    swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti untuk menutup buka peti ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                       $('form#box-form').submit();
                    }
                });
                            
            });

        });
    </script>
@endpush

@section('content')
	<form id="box-form"  method="POST" action="{{ route('box.update',$acquisition->id) }}" files="true" enctype="multipart/form-data">
		@csrf
		@method('PUT')
		<div class="row">
            @component('components.card', ['card_classes' => 'col-12'])
                @slot('card_body')
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <div class="text-muted">Tajuk Perolehan</div>
                                    <div>{{ $acquisition->title }}</div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <div class="text-muted">No. {{$acquisition->category->name}}</div>
                                    <div>{!! $acquisition->reference !!}</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <div class="text-muted">Anggaran Jabatan Bedasarkan BQ</div>
                                    <div>{{ money()->toHuman($acquisition->bqElements->pluck('amount')->sum()) }}</div>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <div class="text-muted"></div>
                                    <div>@include('components.forms.input', [
					    'input_label' => __('Anggaran Jabatan'),
					    'id' => 'new_department_estimation',
					    'name' => 'new_department_estimation',
                                            'value' => money()->toHuman($acquisition->new_department_estimation),
                                            'onkeypress' => 'return (event.charCode === 0 )||  /[0-9,.]/.test(String.fromCharCode(event.charCode));'
					])</div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="Keputusan_Borang_Jadual_Harga" class="col col-form-label">
                                        Keputusan Borang Jadual Harga
                                    </label>

                                    <div class="col input-group">
                                        <input id="subFile" type="text"  class="form-control" readonly>
                                        <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                                        <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                        @if(!empty($bjh_doc))      
                                            @foreach($bjh_doc as $doc)   
                                                <label class="input-group-text" for="downloadFile">
                                                    <a href="/download/{{ $doc->document_path }}/{{ $doc->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                                </label>
                                            @endforeach                                       
                                        @endif                                        
                                    </div>
                                </div>
                                
                               

                                   
                                          @include('components.forms.textarea', [
					    'input_label' => __('Ulasan'),
					    'id' => 'box_review',
					    'name' => 'box_review',
                                            'value' => $acquisition->box_review,
					])
                                  
                            </div>
                        </div>
                        <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table card-table table-vcenter text-nowrap" id="tblSyarikat">
                                    <thead>
                                    <tr>
                                        <th class="w-1">Pilih</th>
                                        <th >Nama Syarikat</th>
                                        <th width="5%">No Penyebut Harga / Petender</th>
                                        <th width="5%">Tempoh</th>
                                        <th width="5%">Jenis Tempoh</th>
                                        <th width="20%">Status Harga</th>
                                        <th width="15%">Harga Bersih (RM)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <input type="hidden" name="cbBox[]" id="hCbBox">
                                    @foreach($sales as $index => $sale)
                                        <tr>
                                            <td>
                                                <label class="custom-switch">

                                                    <input
                                                            id="cbBox{{ $sale->id }}"
                                                            type="checkbox"
                                                            name="cbBox[]"
                                                            class="custom-switch-input"
                                                            value="{{ $sale->id }}"
                                                            @foreach($boxes as $box)
                                                            @if((old('cbBox.'.$sale->id,$box->sale_id) == $sale->id))
                                                            checked
                                                            @endif
                                                            @endforeach
                                                            @if( old('cbBox.' . $sale->id) )
                                                            checked
                                                            @endif
                                                    >
                                                    <span class="custom-switch-indicator"></span>
                                                </label>
                                            </td>
                                            <td style="text-transform:uppercase;">@if(!empty($sale->company)){{ $sale->company->company_name }} @endif</td>
                                            <td style="padding: unset">
                                                <table>
                                                    <tr>
                                                        <td style="padding-right: 0">
                                                            <input
                                                                    id="no{{ $sale->id }}"
                                                                    type="text"
                                                                    class="form-control"
                                                                    name="no{{ $sale->id }}"
                                                                    @foreach($boxes as $box)
                                                                    @if($box->sale_id == $sale->id)
                                                                    value="{{ old('no' . $sale->id,$box->no) }}"
                                                                    @endif
                                                                    @endforeach
                                                                    @if( old('no'.$sale->id))
                                                                    value="{{ old('no' . $sale->id) }}"
                                                                    @endif
                                                            >
                                                        </td>
                                                        <td>/</td>
                                                        <td style="padding-left: 0"><span class="countCompany"></span></td>
                                                    </tr>
                                                </table>


                                            </td>
                                            <td>
                                                <input
                                                        id="period{{ $sale->id }}"
                                                        type="text"
                                                        class="form-control"
                                                        name="period{{ $sale->id }}"
                                                        @foreach($boxes as $box)
                                                        @if($box->sale_id == $sale->id)
                                                        value="{{ old('period'. $sale->id, $box->period) }}"
                                                        @endif
                                                        @endforeach
                                                        @if( old('period'.$sale->id))
                                                        value="{{ old('period' . $sale->id) }}"
                                                        @endif
                                                >
                                            </td>
                                            <td>
                                                <select class="form-control custom-select" name="period_type_id{{ $sale->id }}" id="period_type_id{{ $sale->id }}">
                                                    @foreach(period_types()->pluck('name', 'id') as $key => $value)
                                                        <option
                                                                value="{{ $key }}"
                                                                @foreach($boxes as $box)
                                                                @if(($box->period_type_id == $key) && ($sale->id == $box->sale_id))
                                                                selected
                                                                @endif
                                                                @endforeach
                                                                @if( old('period_type_id'.$sale->id))
                                                                selected
                                                                @endif
                                                        >{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control custom-select" name="is_stated{{ $sale->id }}" id="is_stated{{ $sale->id }}">
                                                    @foreach($listHargaBersih as $hargaBersih)
                                                        <option
                                                                value="{{ $hargaBersih->code_short }}"
                                                                @foreach($boxes as $box)
                                                                @if(($box->is_stated == $hargaBersih->code_short) && ($sale->id == $box->sale_id))
                                                                selected
                                                                @endif
                                                                @endforeach
                                                                @if( old('is_stated'.$sale->id))
                                                                selected
                                                                @endif
                                                        >{{ $hargaBersih->description }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input
                                                        id="amount{{ $sale->id }}"
                                                        type="text"
                                                        class="form-control money"
                                                        name="amount{{ $sale->id }}"
                                                        @foreach($boxes as $box)
                                                        @if($box->sale_id == $sale->id && !empty($box->amount))
                                                        value="{{ old('amount'. $sale->id, money()->toCommon($box->amount)) }}"
                                                        @endif
                                                        @endforeach
                                                        @if( old('amount'.$sale->id))
                                                        value="{{ old('amount' . $sale->id) }}"
                                                        @endif
                                                >
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                @endslot
        
                @slot('card_footer')
                    <div class="float-right btn-group">
                        
                        @if($acquisition->boxes->count() > 0)
                            <a target="_blank" href="{{ route('jadualPerolehan', ['hashslug'=>$acquisition->hashslug] ) }}"
                               class="btn btn-success">
                                @icon('fe fe-printer') {{ __('Cetak') }}
                            </a>
                        @endif
                        
                        <a href="{{ route('box.index') }}"
                           class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                        </a>
                        
                        
                        
                        <button type="submit" class="btn btn-primary  submit-action-btn">
                            @icon('fe fe-save') {{ __('Simpan') }}
                        </button>
                        
                        
                        
                        @if($acquisition->boxes->count() > 0  && !empty($bjh_doc) && count($bjh_doc) > 0)
                            <input id="confirmation" name="confirmation" type="hidden"/>
                            <button type="button" id="selesai_submit_button" class="btn btn-success float-middle border-default confirm">
                                @icon('fe fe-check') {{ __('Selesai') }}
                            </button>
                        @endif
                    </div>
                @endslot
			@endcomponent
		</div>
	</form>
@endsection