@extends('layouts.admin')

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.box.partials.scripts')
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-box',
							'route_name' => 'api.datatable.contract.box',
							'columns' => [
								['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
								['data' => 'closed_at', 'title' => __('Tarikh Tutup Perolehan'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Perolehan'), __('Tajuk'), __('Tarikh Tutup'), __('Tindakan')
							],
							'actions' => minify(view('contract.pre.box.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection