<h4>Perolehan</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Tajuk Perolehan</div>
        <p>{!! $acquisition->title !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">No. Perolehan</div>
        <p>{!! $acquisition->reference !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Anggaran Jabatan</div>
        <p>{!! money()->toHuman($acquisition->bqElements->pluck('amount')->sum()) !!}</p>
    </div>
</div>
<!-- ulasan -->
<div class="row">
    <div class="col-6">
        <div class="text-muted">Ulasan</div>
        <p>{!! $acquisition->box_review !!}</p>
    </div>
    
</div>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="row">
            @foreach($bjh_doc as $doc)
                @if(!($doc->document_name) == NULL)
                    <div class="col-5">
                        <label>Keputusan Borang Jadual Harga</label>
                            <br>
                            <a href="/download/{{ $doc->document_path }}/{{ $doc->document_name }}" target="_blank">{{$doc->document_name}}</a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>