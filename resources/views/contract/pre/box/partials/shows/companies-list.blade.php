<h4>Senarai Syarikat</h4>
<div class="row">
    <div class="col-3">
        <div class="text-muted">Nama Syarikat</div>
    </div>
    <div class="col-2">
        <div class="text-muted">No. Penyebut Harga</div>
    </div>
    <div class="col-2">
        <div class="text-muted">Tempoh</div>
    </div>
    <div class="col-2">
        <div class="text-muted">Amaun Dinyatakan</div>
    </div>
</div>
@foreach($acquisition->boxes as $box)
    <div class="row">
        <div class="col-3">
            <div>{{ $box->sale->company->company_name }}</div>
        </div>
        <div class="col-2">
            <div style="text-transform:uppercase;">{{ $box->no }} / {{ $acquisition->boxes->count() }}</div>
        </div>
        <div class="col-2">
            <div>{{ $box->period.' '.$box->period_type->name }}</div>
        </div>
        <div class="col-2">
            <div>@if(!empty($box->amount)){{ money()->toHuman($box->amount) }} @else {{ __('TIDAK DINYATAKAN') }} @endif</div>
        </div>
    </div>
@endforeach

