@extends('layouts.admin')
@push('scripts')
    <script type="text/javascript">
        @foreach($bjh_doc as $bjh)
            var url = "/uploads/{{$bjh->document_name}}";

            $('.newwindow{{$bjh->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach
    </script>
    <script>
    	jQuery(document).ready(function($) {
    		
    	});
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#acquisition" role="tab" aria-controls="acquisition" aria-selected="false">
                    @icon('fe fe-layers')&nbsp;{{ __('Perolehan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#companies-list" role="tab" aria-controls="companies-list" aria-selected="false">
                    @icon('fe fe-layers')&nbsp;{{ __('Senarai Syarikat') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                         @component('components.tab.content', ['id' => 'acquisition', 'active' => true])
                            @slot('content')
                                @include('contract.pre.box.partials.shows.acquisition', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent
                    
                     	@component('components.tab.content', ['id' => 'companies-list'])
                            @slot('content')
                                @include('contract.pre.box.partials.shows.companies-list', ['acquisition' => $acquisition])
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('box.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                        @if($acquisition->boxes->count() > 0)
                            <a target="_blank" href="{{ route('jadualPerolehan', ['hashslug'=>$acquisition->hashslug] ) }}"
                               class="btn btn-success">
                                @icon('fe fe-printer') {{ __('Cetak') }}
                            </a>
                        @endif
                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection








