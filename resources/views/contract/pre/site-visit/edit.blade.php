@extends('layouts.admin')

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
	<script>
            
        jQuery(document).ready(function($) {
            $('#dataTable').DataTable();
            $('#staff_id').val({{ $acquisition->briefings[1]->visit_staff_id ?? '0' }}).trigger('change');

            $(document).on('click', '.buang', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('api.contract.site-visit.delete', id))
                        .then(response => {
                            swal('Lawatan Tapak', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });
            //upload doc
            @if( $acquisition->doc_sitevisit )
                $('#subFile').val('{{ $acquisition->doc_sitevisit->document_name }}');
            @endif
            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->hashslug }}';
                var route_name = 'api.contract.upload-site';
                var form_id = 'sitevisit_form';
                var form = document.forms[form_id];
                var data = new FormData(form);

                axios.post(route(route_name, id), data).then(response => {
                    swal('Lawatan Tapak', response.data.message, 'success');
                     location.reload();
                });
            });
            //upload doc
        });
        $('.visit').on('click', function(){
                var id = $(this).val();
                console.log('hai');
                var val = 0;
                if($(this).is(":checked")) {
                    $('.site_visit').val('1');
                }else{
                    $('.site_visit').val('2');
                }
                var route_name = 'api.contract.site-visit.update';
                var form_id = 'site_visit_form';
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('Lawatan Tapak', response.data.message, 'success');
                    location.reload();
                });
            });
	</script>
@endpush

@php
    $acqCategory = "";

    if($acquisition->category->id == 1 || $acquisition->category->id == 2) {
        $acqCategory = "Sebut Harga";
    }elseif($acquisition->category->id == 3 || $acquisition->category->id == 4) {
        $acqCategory = "Tender";
    }
@endphp

@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        
        <span class="font-weight-bold">Tajuk : </span>{{ $acquisition->title }}
        <br>
        <span class="font-weight-bold" >No. {{$acqCategory}} : </span>{!! $acquisition->reference !!}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh & Masa Lawatan Tapak : </span>{{ (!empty($acquisition->visit_date_time))? $acquisition->visit_date_time->format(config('datetime.datetimepicker.date_time')):null }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempat Lawatan Tapak : </span>{!! $acquisition->visit_location !!}
        <br>
        <span class="font-weight-bold">Syarat Kelayakan : </span>
            @if(!empty($acquisition) && !empty($acquisition->approval) && ($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3))
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->isNotEmpty())
                    @php $cnt1 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->mofQualifications as $cnt1=>$cetak5)                    
                        @if($cetak5->code->type == 'category')
                        @php $cnt1 = $cnt1 + 1; @endphp
                            {{$cetak5->code->name}}
                            @if($cetak5->status == 1)
                                <b>DAN</b>
                            @elseif($cetak5->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt2 = 0; @endphp
                    @foreach($acquisition->approval->mofQualifications as $cnt2=>$cetak6)
                        @if($cetak6->code->type == 'khusus')
                        @php $cnt2 = $cnt2 + 1; @endphp
                            {{$cetak6->code->code}}
                            @if($cetak6->status == 1)
                                <b>DAN</b>
                            @elseif($cetak6->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                        
                        @endif
                    @endforeach   
                    <br />
                @endif
            @else
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->isNotEmpty())
                    @php $cnt3 = 0; @endphp
                    Gred : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt3=>$cetak)
                        @if($cetak->code->type == 'grade')
                        @php $cnt3 = $cnt3 + 1; @endphp
                            {{$cetak->code->code}} 
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif                    
                    @endforeach 
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    @php $cnt4 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt4=>$cetak)                    
                        @if($cetak->code->type == 'category')
                        @php $cnt4 = $cnt4 + 1; @endphp
                            {{$cetak->code->name}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt5 = 0; @endphp
                    @foreach($acquisition->approval->cidbQualifications as $cnt5=>$cetak)
                        @if($cetak->code->type == 'khusus')
                        @php $cnt5 = $cnt5 + 1; @endphp
                            {{$cetak->code->code}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                      
                        @endif
                    @endforeach                                 
                    <br />
                @endif
            @endif
    @endslot
@endcomponent
	
<div class="row justify-content-center">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @component('components.card')
                        @slot('card_body')
                        <form id="site_visit_form">
                            <input type="hidden" name="acquisition_hashslug" value="{{ $acquisition->hashslug }}">
                            @include('contract.pre.site-visit.partials.forms.edit')
                        </form>
                        <br>
                        <hr>
                        <form id="sitevisit_form" files = "true" enctype="multipart/form-data" method="POST">
                        @csrf
                            <div class="col-12">
                                <label for="Invois" class="col col-form-label"> Muat Naik Lampiran </label>

                                <div class="col input-group">
                                    <input id="subFile" type="text"  class="form-control" readonly>
                                    <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                                    <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if(!empty($acquisition->doc_sitevisit))
                                    <label class="input-group-text" for="downloadFile">
                                        <a href="/download/{{ $acquisition->doc_sitevisit->document_path }}/{{ $acquisition->doc_sitevisit->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                    </label>
                                    @if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
                                    <label class="input-group-text" for="deleteFile">
                                        <input type="input" id="deleteFile" class="buang" value="{{ $acquisition->doc_sitevisit->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                    @endif
                                @endif
                                </div>
                                @if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer')
                                    <button type="submit" class="btn btn-primary float-right submit-action-btn" >
                                        Muat Naik
                                    </button>
                                @endif
                            </div>
                        </form>
                        @endslot
                        @slot('card_footer')
                            <div class="btn-group float-right">
                                <a href="{{ route('site-visit.index') }}"
                                   class="btn btn-default border-primary">
                                        {{ __('Kembali') }}
                                </a>
                            </div>
                        @endslot
                @endcomponent
        </div>
</div>
	
@endsection