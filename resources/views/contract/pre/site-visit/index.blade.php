@extends('layouts.admin')

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.site-visit.partials.scripts')
			@component('components.forms.hidden-form', 
				[
					'id' => 'destroy-record-form',
					'action' => route('site-visit.destroy', 'dummy')
				])
				@slot('inputs')
					@method('DELETE')
				@endslot
			@endcomponent
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-management',
							'route_name' => 'api.datatable.contract.site-visit',
							'columns' => [
								['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'title', 'title' => __('Tajuk Perolehan'), 'defaultContent' => '-'],
								['data' => 'visit_at', 'title' => __('Tarikh Lawatan Tapak'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Rujukan'), __('Tajuk Perolehan'), __('Tarikh Lawatan Tapak'), __('table.action')
							],
							'actions' => minify(view('contract.pre.site-visit.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection