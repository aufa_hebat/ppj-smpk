@extends('layouts.admin')

@push('styles')
	<style>

	</style>
@endpush

@push('scripts')
	<script>
        jQuery(document).ready(function($) {
            // define assignment to inputs
        });
	</script>
@endpush

@section('content')
	<form method="POST" action="{{ route('site-visit.store') }}">
		@csrf
		<div class="row justify-content-center">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
				@component('components.card')
					@slot('card_body')
						<input type="hidden" name="acquisition_id" value="{{ $acquisition->id }}">
						@include('contract.pre.site-visit.partials.forms.create')
					@endslot
					@slot('card_footer')
						<div class="btn-group float-right">
							<a href="{{ route('site-visit.index') }}"
							   class="btn btn-default border-primary">
								{{ __('Kembali') }}
							</a>
							<button type="submit" class="btn btn-primary">
								@icon('fe fe-save') {{ __('Seterusnya') }}
							</button>
						</div>
					@endslot
				@endcomponent
			</div>
		</div>
	</form>
@endsection