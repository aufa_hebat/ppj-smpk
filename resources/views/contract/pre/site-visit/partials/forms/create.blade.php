<div class="row">
    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <div class="text-muted">Tajuk Perolehan</div>
            <div>{{ $acquisition->title }}</div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <div class="text-muted">No. Sebut Harga</div>
            <div>{!! $acquisition->reference !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <div class="text-muted">Tarikh & Masa Lawatan Tapak</div>
            <div>{{ $acquisition->visit_date_time->format(config('datetime.datetimepicker.date_time')) }}</div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6">
        <div class="form-group">
            <div class="text-muted">Tempat Lawatan Tapak</div>
            <div>{!! $acquisition->visit_location !!}</div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        Senarai Syarikat
        @foreach($acquisition->briefings as $company)
            @include('components.forms.checkbox', [
                'label' => $company->company->company_name,
                'name'  => 'company_id[]',
                'value' => $company->company->id,
                'id'    => 'company_id',
            ])
        @endforeach
    </div>
</div>

