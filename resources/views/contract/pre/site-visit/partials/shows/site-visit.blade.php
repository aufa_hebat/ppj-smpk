<div class="row">
    <div class="col-12">
        <h4>Syarikat Yang Hadir Lawatan Tapak</h4>
        <div class="table-responsive table-bordered">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th width="5%">Bil.</th>
                    <th width="20%">Nama Syarikat</th>
                    <th width="20%">Alamat</th>
                    <th width="15%">Nama</th>
                    <th width="10%">No Kad Pengenalan</th>
                    <th width="5%">Wakil/Pemilik</th>
                    <th width="20%">Pegawai Bertugas</th>
                    <th width="5%">Kehadiran</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($acquisition->briefings) && $acquisition->briefings->count() > 0)
                @php $n = 0; @endphp
                    @foreach($acquisition->briefings as $visit)
                        @if($visit->attendant_status != 3)
                            @php $n++; @endphp
                            <tr>
                                <td>{{ $n }}</td>
                                <td>{{ $visit->company->company_name }}</td>
                                <td>
                                    {{ (!empty($visit->company) && !empty($visit->company->addresses)) ? $visit->company->addresses[0]->primary:',' }}
                                    {{ (!empty($visit->company) && !empty($visit->company->addresses)) ? $visit->company->addresses[0]->secondary:',' }} 
                                    {{ (!empty($visit->company) && !empty($visit->company->addresses)) ? $visit->company->addresses[0]->postcode:',' }} 
                                    {{ (!empty($visit->company) && !empty($visit->company->addresses)) ? $visit->company->addresses[0]->state:'' }}
                                </td>
                                <td>{{ ($visit->attendant_status == '1')? $visit->owner->name:$visit->agent->name }}</td>
                                <td>{{ ($visit->attendant_status == '1')? $visit->owner->ic_number:$visit->agent->ic_number }}</td>
                                <td>{{ ($visit->attendant_status == 1)? "Pemilik":"Wakil" }}</td>
                                <td>{{ (!empty($visit->visit_staff))? $visit->visit_staff->name:null }}</td>
                                <td>@if($visit->site_visit == '1') 
                                        Hadir
                                    @else 
                                        Tidak Hadir
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
