@extends('layouts.admin')
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
    	jQuery(document).ready(function($) {
            $('#dataTable').DataTable();
    	});
    </script>
@endpush
@php
    $acqCategory = "";

    if($acquisition->category->id == 1 || $acquisition->category->id == 2) {
        $acqCategory = "Sebut Harga";
    }elseif($acquisition->category->id == 3 || $acquisition->category->id == 4) {
        $acqCategory = "Tender";
    }
@endphp
@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        
        <span class="font-weight-bold">Tajuk : </span>{{ $acquisition->title }}
        <br>
        <span class="font-weight-bold" >No. {{$acqCategory}} : </span>{!! $acquisition->reference !!}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh & Masa Lawatan Tapak : </span>{{ (!empty($acquisition->visit_date_time))? $acquisition->visit_date_time->format(config('datetime.datetimepicker.date_time')):null }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempat Lawatan Tapak : </span>{!! $acquisition->visit_location !!}
        <br>
        <span class="font-weight-bold">Syarat Kelayakan : </span>
            @if(!empty($acquisition) && !empty($acquisition->approval) && ($acquisition->approval->acquisition_type_id == 1 || $acquisition->approval->acquisition_type_id == 3))
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->mofQualifications) && $acquisition->approval->mofQualifications->isNotEmpty())
                    @php $cnt1 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->mofQualifications as $cnt1=>$cetak5)                    
                        @if($cetak5->code->type == 'category')
                        @php $cnt1 = $cnt1 + 1; @endphp
                            {{$cetak5->code->name}}
                            @if($cetak5->status == 1)
                                <b>DAN</b>
                            @elseif($cetak5->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt2 = 0; @endphp
                    @foreach($acquisition->approval->mofQualifications as $cnt2=>$cetak6)
                        @if($cetak6->code->type == 'khusus')
                        @php $cnt2 = $cnt2 + 1; @endphp
                            {{$cetak6->code->code}}
                            @if($cetak6->status == 1)
                                <b>DAN</b>
                            @elseif($cetak6->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                        
                        @endif
                    @endforeach   
                    <br />
                @endif
            @else
                @if(!empty($acquisition) && !empty($acquisition->approval) && !empty($acquisition->approval->cidbQualifications) && $acquisition->approval->cidbQualifications->isNotEmpty())
                    @php $cnt3 = 0; @endphp
                    Gred : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt3=>$cetak)
                        @if($cetak->code->type == 'grade')
                        @php $cnt3 = $cnt3 + 1; @endphp
                            {{$cetak->code->code}} 
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif                    
                    @endforeach 
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    @php $cnt4 = 0; @endphp
                    Kategori : 
                    @foreach($acquisition->approval->cidbQualifications as $cnt4=>$cetak)                    
                        @if($cetak->code->type == 'category')
                        @php $cnt4 = $cnt4 + 1; @endphp
                            {{$cetak->code->name}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif
                        @endif
                    @endforeach 
                    <br />
                    Pengkhususan : 
                    @php $cnt5 = 0; @endphp
                    @foreach($acquisition->approval->cidbQualifications as $cnt5=>$cetak)
                        @if($cetak->code->type == 'khusus')
                        @php $cnt5 = $cnt5 + 1; @endphp
                            {{$cetak->code->code}}
                            @if($cetak->status == 1)
                                <b>DAN</b>
                            @elseif($cetak->status == 0)
                                <b>ATAU</b>
                            @else
                            <b>;&nbsp;</b>
                            @endif                      
                        @endif
                    @endforeach                                 
                    <br />
                @endif
            @endif
    @endslot
@endcomponent
<div class="row">
    <div class="col-12">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                         @component('components.tab.content', ['id' => 'site-visit', 'active' => true])
                            @slot('content')
                                @include('contract.pre.site-visit.partials.shows.site-visit', ['acquisition' => $acquisition])
                                @if(!empty($acquisition->doc_sitevisit))
                                <hr>
                                <div class="col-12">
                                    Lampiran : 
                                        <a href="/download/{{ $acquisition->doc_sitevisit->document_path }}/{{ $acquisition->doc_sitevisit->document_name }}" target="_blank">{{ $acquisition->doc_sitevisit->document_name }}</a>
                                </div>
                                @endif
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('site-visit.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection








