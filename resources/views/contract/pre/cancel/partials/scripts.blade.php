@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) { 
		$(document).on('click', '.cancel-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('acquisition.cancel.show', id));
		});

	});
</script>
@endpush