<div class="btn-group float-right">
    
	{{ html()->a(route('acquisition.document.index',['hashslug'=>$hashslug]), __('Kembali'))->class('btn btn-default border-primary') }}
    @isset($route_name)
	<button type="submit" class="btn btn-primary float-middle save-action-btn" 
		data-route="{{ $route_name }}"
		data-form="{{ $form }}">
	    @icon('fe fe-save') {{ __('Simpan') }}
	</button>
    @endisset
    @isset($route_name)
    <button class="btn btn-success float-middle border-default submit-action-btn" 
		data-route="{{ $route_name }}"
		data-form="{{ $form }}">
	    @icon('fe fe-send') {{ __('Selesai') }}
	</button>
    @endisset
	
</div>