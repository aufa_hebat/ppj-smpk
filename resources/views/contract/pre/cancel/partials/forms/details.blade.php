<h5>Maklumat Pembatalan Perolehan</h5>
<div class="row">
    <div class="col-3">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Pembatalan'),
            'id' => 'cancelled_at',
            'name' => 'cancelled_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
    <div class="col-9">
    @include('components.forms.hidden', [
            'id' => 'status_id',
            'name' => 'status_id',
            'value' => ''
        ])   
    </div>
</div>
<div class="row">
    <div class="col-12">
        @include('components.forms.textarea', [
            'input_label' => __('Sebab Pembatalan'),
            'id' => 'cancelled_reason',
            'name' => 'cancelled_reason',
        ])
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group row">
            <label for="Fail_Keputusan_Mesyuarat"
                class="col col-form-label">
                Muat Naik Dokumen
            </label>
    
            <div class="col input-group">
                <input id="subFile" type="text"  class="form-control" readonly>
                <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                @if(!empty($acquisition) && !empty($acquisition->doc_cancel_acquisition))
                    {{--  @foreach($acquisition->doc_cancel_acquisition as $document)  --}}
                        <label class="input-group-text" for="downloadFile">
                            <a href="/download/{{$acquisition->doc_cancel_acquisition->document_path}}/{{ $acquisition->doc_cancel_acquisition->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                        </label>
                    {{--  @endforeach  --}}
                @endif
            </div>                        
        </div>
    </div>
</div>