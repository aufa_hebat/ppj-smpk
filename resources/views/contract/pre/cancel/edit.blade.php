@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.textarea')
    <script>
        jQuery(document).ready(function($) {
            $('#acquisition_approval_title').val(
                "{{(!empty($acquisition->approval->reference)) ? $acquisition->approval->reference:''}} : {{$acquisition->title}}");
            $('#acquisition_approval_id').val('{{ $acquisition->acquisition_approval_id }}');
            $('#acquisition_category_id').val('{{ $acquisition->acquisition_category_id }}').prop('disabled',true);
            $('#status_id').val('{{ $acquisition->status_id }}');
            $('#reference').val('{{ $acquisition->reference }}').prop('readonly',true);

            $('#cancelled_reason').val('{{ $acquisition->cancelled_reason }}');
            @if(!empty($acquisition->cancelled_at))
            $('#cancelled_at').val('{{ $acquisition->cancelled_at->format("d/m/Y") }}');
            @endif

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });



            @if(!empty($acquisition) && !empty($acquisition->doc_cancel_acquisition))
                $('#subFile').val('{{ $acquisition->doc_cancel_acquisition->document_name }}');                
            @endif

            $(document).on('click', '.save-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var form = document.forms[form_id];
                var data = new FormData(form);
                //var data = $('#' + form_id).serialize();
                
                //axios.put(route(route_name, id), data).then(response => {
                axios.post(route(route_name, id), data).then(response => {
                    swal('{!! __('Maklumat Perolehan') !!}', response.data.message, 'success');
                    location.reload();
                });
            });

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $acquisition->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var form = document.forms[form_id];
                var data = new FormData(form);
                data.append('status_id', '2');
                //$('#status_id').val('2');
                
                //var data = $('#' + form_id).serialize();
                
                axios.post(route(route_name, id), data).then(response => {                    
                    swal('{!! __('Maklumat Perolehan') !!}', response.data.message, 'success');
                    redirect(route('acquisition.cancel.show', id));
                });
            });
        });
    </script>
@endpush

@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk: </span>{{ $acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Perolehan: </span>{{ $acquisition->reference }}
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-12 bg-transparent">
            @component('components.card')
                @slot('card_body')
                <form id="cancel-form" method="post" files="true" enctype="multipart/form-data">
                    @include('contract.pre.cancel.partials.forms.details', ['acquisition' => $acquisition])
                </form>
                @include('contract.pre.cancel.partials.forms.submit', [
                    'hashslug'=> $acquisition->approval->hashslug,
                    'route_name' => 'api.contract.acquisition.update-acq-cancel',
                    'form' => 'cancel-form',
                ])                
                @endslot
            @endcomponent
        </div>
    </div>
@endsection