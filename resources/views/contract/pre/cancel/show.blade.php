@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.textarea')
    <script>
        jQuery(document).ready(function($) {
            $('#acquisition_approval_title').val(
                "{{(!empty($acquisition->approval->reference)) ? $acquisition->approval->reference:''}} : {{$acquisition->title}}");
            $('#acquisition_approval_id').val('{{ $acquisition->acquisition_approval_id }}');
            $('#acquisition_category_id').val('{{ $acquisition->acquisition_category_id }}').prop('disabled',true);
            $('#status_id').val('{{ $acquisition->status_id }}');
            $('#reference').val('{{ $acquisition->reference }}').prop('readonly',true);

            $('#cancelled_reason').val('{{ $acquisition->cancelled_reason }}');
            @if(!empty($acquisition->cancelled_at))
            $('#cancelled_at').val('{{ $acquisition->cancelled_at->format("d/m/Y") }}');
            @endif

            @if(!empty($acquisition) && !empty($acquisition->doc_cancel_acquisition))
                $('#subFile').val('{{ $acquisition->doc_cancel_acquisition->document_name }}');                
            @endif
        });
    </script>
@endpush

@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk: </span>{{ $acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Perolehan: </span>{{ $acquisition->reference }}
        @endslot
    @endcomponent

    <div class="row">
        <div class="col-12 bg-transparent">
            @component('components.card')
                @slot('card_body')
                    @include('contract.pre.cancel.partials.shows.details', ['acquisition' => $acquisition])            
                @endslot
                @slot('card_footer')
                <div class="btn-group float-right">
                    <a href="{{ route('acquisition.cancel.index') }}" 
                            class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                    </a>

                </div>
            @endslot
            @endcomponent
        </div>
    </div>
@endsection