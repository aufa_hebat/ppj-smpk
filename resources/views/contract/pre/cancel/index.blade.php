@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.cancel.partials.scripts')
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-cancel',
							'route_name' => 'api.datatable.contract.cancel',
							'columns' => [
                                ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                ['data' => 'created_at', 'title' => __('table.created_at'), 'defaultContent' => '-'],
                                ['data' => 'updated_at', 'title' => __('table.updated_at'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Rujukan'), __('Tajuk'),  __('table.created_at'), __('table.updated_at'), __('table.action')
                            ],
							'actions' => minify(view('contract.pre.cancel.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection