@extends('layouts.admin')

@push('scripts')
	@include('components.forms.assets.textarea')
	@include('components.forms.assets.select2')
	<script>
		var description_editor;

		jQuery(document).ready(function($) {
				$('#location_id').select2();
				ClassicEditor
						.create( document.querySelector( '#description' ) )
						.then( editor => {
								description_editor = editor;
						} )
						.catch( error => {
								console.error( error );
						} );

				$('#year').keypress(function(){
					return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57;
				})


				$(document).on('click', '.submit-action-btn', function(event) {

					event.preventDefault();
					var route_name = $(this).data('route');
					var form_id = $(this).data('form');
					$('#description').val(description_editor.getData());
					var data = $('#' + form_id).serialize();
			
					axios.post(route(route_name), data).then(response => {
						swal({
							title: '{!! __('Kelulusan Perolehan') !!}',
							text: response.data.message,
							timer: 3000,
							type: 'success',
							onOpen: () => { swal.showLoading() }
						}).then((result) => {
							if (result.dismiss === swal.DismissReason.timer) {
								redirect(response.data.data.redirect);
							}
						});
					});
				});
		});
	</script>
@endpush

@section('content')
	{{--  <form method="POST" action="{{ route('contract.pre.store') }}" id="create-form">  --}}
	<form method="POST" action="" id="create-form">
		@csrf
		<div class="row">
			<div class="col">
				@component('components.card')
					@slot('card_body')
						@include('contract.pre.partials.forms.create')
					@endslot
					@slot('card_footer')
						@include('contract.pre.partials.forms.submit', [
														'hashslug' => '-', 
														'route_name' => 'api.contract.acquisition.approval.details.store',
														'form' => 'create-form',
                                                                                                                'button_name' => 'Batal'
												])
					@endslot
				@endcomponent
			</div>
		</div>
	</form>
@endsection