@extends('layouts.admin')
@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        function calc_bq(val){//for bq auto count amount based on rate
            var rate = document.getElementById('adjust_rate'+val).value ;
            var qty = document.getElementById('adjust_qty'+val).value ;
                    
            rate = rate.replace(/[,]/g, "");
            rate = rate.split(' ').join('');
            qty = qty.replace(/[,]/g, "");
            qty = qty.split(' ').join('');
            var $result = rate * qty;
            document.getElementById('adjust_amt'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
        }
    	jQuery(document).ready(function($) {
            $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            
            @if(!$isDepositAllowed)
                console.log("{{ $depositReason }}");
            @endif
            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });
            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
    	});
    </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
            <br>

            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if( !empty($sst->eots->pluck('eot_approve')->pluck('approved_end_date')->last()) )
                @if($sst->eots->count()>0)
                    <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->eots->pluck('eot_approve')->pluck('approved_end_date')->last())->format('d/m/Y') ?? ''}}&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
            @endif
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
        @endslot
    @endcomponent

    <div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#price-adjustment" role="tab" aria-controls="price-adjustment" aria-selected="false">
                    @icon('fe fe-sliders')&nbsp;{{ __('Pelarasan Harga') }}
                </a>
            </li>       
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#sub-contract" role="tab" aria-controls="sub-contract" aria-selected="false">
                    @icon('fe fe-clipboard')&nbsp;{{ __('Sub Kontraktor') }}
                </a>
            </li>                 
            @if(empty($reviewbon) || ($reviewbon->status == 'S1' || $reviewbon->status == null || ($reviewbon->status == 'CN' && $sst->acquisition->status_task == 'Draft Penyedia')) && $reviewbon->status != 'Selesai')
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#bon-implementation" role="tab" aria-controls="bon-implementation" aria-selected="false" >
                        @icon('fe fe-book-open')&nbsp;{{ __('Bon Pelaksanaan') }}
                    </a>
                </li>
            @else
            @endif
            @if(!empty($reviewbon) && (user()->id == $reviewbon->created_by))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews-bon" role="tab" aria-controls="document-reviews-bon" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan Bon Pelaksanaan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews-bon" role="tab" aria-controls="document-log-reviews-bon" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan Bon Pelaksanaan') }}
                        </a>
                    </li>
                @endif
            @endif
            @if(empty($reviewinsurans) || ($reviewinsurans->status == 'S1' || $reviewinsurans->status == null) && $reviewinsurans->status != 'Selesai')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Insuran') }}
                    </a>
                </li>
            @else
            @endif
            @if(!empty($reviewinsurans) && (user()->id == $reviewinsurans->created_by))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews-insurance" role="tab" aria-controls="document-reviews-insurance" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan Insuran') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews-insurance" role="tab" aria-controls="document-log-reviews-insurance" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan Insuran') }}
                        </a>
                    </li>
                @endif
            @endif

           <!-- @if($isDepositAllowed)-->
                @if(empty($reviewwang) || ($reviewwang->status == 'S1' || $reviewwang->status == null) && $reviewwang->status != 'Selesai')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#cash-deposit" role="tab" aria-controls="cash-deposit" aria-selected="false">
                            @icon('fe fe-dollar-sign')&nbsp;{{ __('Wang Pendahuluan') }}
                        </a>
                    </li>
                @else
                @endif
                @if(!empty($reviewwang) && (user()->id == $reviewwang->created_by))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-reviews-deposit" role="tab" aria-controls="document-reviews-deposit" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan Wang Pendahuluan') }}
                        </a>
                    </li>

                    @if(user()->current_role_login == 'administrator')
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews-deposit" role="tab" aria-controls="document-log-reviews-deposit" aria-selected="false">
                                @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan Wang Pendahuluan') }}
                            </a>
                        </li>
                    @endif
                @endif
            <!--@endif-->
            @if($isDocumentAllowed && $sst->status == '3')
                @if(empty($reviewdoc) || ($reviewdoc->status == 'S1' || $reviewdoc->status == null || ($reviewdoc->status == 'CN' && $sst->acquisition->status_task == 'Draft Penyedia')) && $reviewdoc->status != 'Selesai')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-contract" role="tab" aria-controls="document-contract" aria-selected="false">
                            @icon('fe fe-clipboard')&nbsp;{{ __('Dokumen Kontrak') }}
                        </a>
                    </li>
                @else
                @endif
                @if(!empty($reviewdoc) && (user()->id == $reviewdoc->created_by))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-reviews-documents" role="tab" aria-controls="document-reviews-documents" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan Dokumen Kontrak') }}
                        </a>
                    </li>

                    @if(user()->current_role_login == 'administrator')
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews-documents" role="tab" aria-controls="document-log-reviews-documents" aria-selected="false">
                                @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan Dokumen Kontrak') }}
                            </a>
                        </li>
                    @endif
                @endif
            @endif
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document-tab-content'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'bon-implementation','active' => true])
                            @slot('content')
                                @include('contract.post.partials.indexes.bon-implementation')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-reviews-bon'])
                            @slot('content')

                                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                                    @if((!empty($reviewbon)) && (user()->id == $reviewbon->created_by))

                                        {{-- penyedia --}}

                                        @if(!empty($cetaknotisuulogbon) && !empty($cetaknotisuulogbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuubon" aria-expanded="false" aria-controls="cnuubon">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuubon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7bon as $s7logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan7logbon) && !empty($semakan7logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7bon" aria-expanded="false" aria-controls="s7bon">
                                                            Ulasan Semakan Oleh {{$semakan7logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6bon as $s6logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($cetaknotisbpubsbon) && !empty($cetaknotisbpubsbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubbon" aria-expanded="false" aria-controls="cnbpubbon">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubbon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5bon as $s5logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan5logbon) && !empty($semakan5logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5bon" aria-expanded="false" aria-controls="s5bon">
                                                            Ulasan Semakan Oleh {{$semakan5logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4bon as $s4logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        {{-- @if(!empty($semakan4logbon) && !empty($semakan4logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4bon" aria-expanded="false" aria-controls="s4bon">
                                                            Ulasan Semakan Oleh {{$semakan4logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3bon as $s3logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($semakan3logbon) && !empty($semakan3logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3bon" aria-expanded="false" aria-controls="s3bon">
                                                            Ulasan Semakan Oleh {{$semakan3logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2bon as $s2logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logbon) && !empty($semakan2logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2bon" aria-expanded="false" aria-controls="s2bon">
                                                            Ulasan Semakan Oleh {{$semakan2logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1bon as $s1logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                
                                </div>

                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'insurance'])
                            @slot('content')
                                @include('contract.post.partials.indexes.insurance')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-reviews-insurance'])
                            @slot('content')

                                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
                                    
                                    @if((!empty($reviewinsurans)) && (user()->id == $reviewinsurans->created_by))

                                        {{-- penyedia --}}

                                        @if(!empty($cetaknotisuuloginsurans) && !empty($cetaknotisuuloginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuins" aria-expanded="false" aria-controls="cnuuins">
                                                            Ulasan Semakan Oleh {{$cetaknotisuuloginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuuins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7insurans as $s7loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan7loginsurans) && !empty($semakan7loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7ins" aria-expanded="false" aria-controls="s7ins">
                                                            Ulasan Semakan Oleh {{$semakan7loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6insurans as $s6loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($cetaknotisbpubsinsurans) && !empty($cetaknotisbpubsinsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubins" aria-expanded="false" aria-controls="cnbpubins">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsinsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5insurans as $s5loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan5loginsurans) && !empty($semakan5loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5ins" aria-expanded="false" aria-controls="s5ins">
                                                            Ulasan Semakan Oleh {{$semakan5loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4insurans as $s4loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        {{-- @if(!empty($semakan4loginsurans) && !empty($semakan4loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4ins" aria-expanded="false" aria-controls="s4ins">
                                                            Ulasan Semakan Oleh {{$semakan4loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3insurans as $s3loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($semakan3loginsurans) && !empty($semakan3loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3ins" aria-expanded="false" aria-controls="s3ins">
                                                            Ulasan Semakan Oleh {{$semakan3loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2insurans as $s2loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2loginsurans) && !empty($semakan2loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2ins" aria-expanded="false" aria-controls="s2ins">
                                                            Ulasan Semakan Oleh {{$semakan2loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1insurans as $s1loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                
                                </div>

                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'price-adjustment','BQElemen' => $BQElemen, 'BQItem'=> $BQItem, 'BQSub' => $BQSub ])
                            @slot('content')
                            <input type="text" name="sst_id" id="sst_id" value="{{ $sst->id }}" hidden/>
                                @include('contract.post.partials.indexes.price-adjustment')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'cash-deposit'])
                            @slot('content')
                                @include('contract.post.partials.indexes.cash-deposit')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-reviews-deposit'])
                            @slot('content')

                                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
                                    
                                    @if((!empty($reviewwang)) && (user()->id == $reviewwang->created_by))

                                        {{-- penyedia --}}

                                        @if(!empty($cetaknotisuulogwang) && !empty($cetaknotisuulogwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuwang" aria-expanded="false" aria-controls="cnuuwang">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuuwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7wang as $s7logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan7logwang) && !empty($semakan7logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7wang" aria-expanded="false" aria-controls="s7wang">
                                                            Ulasan Semakan Oleh {{$semakan7logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6wang as $s6logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($cetaknotisbpubswang) && !empty($cetaknotisbpubswang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubwang" aria-expanded="false" aria-controls="cnbpubwang">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubswang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5wang as $s5logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan5logwang) && !empty($semakan5logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5wang" aria-expanded="false" aria-controls="s5wang">
                                                            Ulasan Semakan Oleh {{$semakan5logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4wang as $s4logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        {{-- @if(!empty($semakan4logwang) && !empty($semakan4logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4wang" aria-expanded="false" aria-controls="s4wang">
                                                            Ulasan Semakan Oleh {{$semakan4logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3wang as $s3logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($semakan3logwang) && !empty($semakan3logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3wang" aria-expanded="false" aria-controls="s3wang">
                                                            Ulasan Semakan Oleh {{$semakan3logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2wang as $s2logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logwang) && !empty($semakan2logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2wang" aria-expanded="false" aria-controls="s2wang">
                                                            Ulasan Semakan Oleh {{$semakan2logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1wang as $s1logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                
                                </div>

                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-contract'])
                            @slot('content')
                                @include('contract.post.partials.indexes.document-contract')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-reviews-documents'])
                            @slot('content')

                                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
                                    
                                    @if((!empty($reviewdoc)) && (user()->id == $reviewdoc->created_by))

                                        {{-- penyedia --}}

                                        @if(!empty($cetaknotisuulogdoc) && !empty($cetaknotisuulogdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuudoc" aria-expanded="false" aria-controls="cnuudoc">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuudoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7doc as $s7logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan7logdoc) && !empty($semakan7logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7doc" aria-expanded="false" aria-controls="s7doc">
                                                            Ulasan Semakan Oleh {{$semakan7logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6doc as $s6logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($cetaknotisbpubsdoc) && !empty($cetaknotisbpubsdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubdoc" aria-expanded="false" aria-controls="cnbpubdoc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubdoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5doc as $s5logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan5logdoc) && !empty($semakan5logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5doc" aria-expanded="false" aria-controls="s5doc">
                                                            Ulasan Semakan Oleh {{$semakan5logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4doc as $s4logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        {{-- @if(!empty($semakan4logdoc) && !empty($semakan4logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4doc" aria-expanded="false" aria-controls="s4doc">
                                                            Ulasan Semakan Oleh {{$semakan4logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3doc as $s3logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyedia</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar oleh Penyemak</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($semakan3logdoc) && !empty($semakan3logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3doc" aria-expanded="false" aria-controls="s3doc">
                                                            Ulasan Semakan Oleh {{$semakan3logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2doc as $s2logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logdoc) && !empty($semakan2logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2doc" aria-expanded="false" aria-controls="s2doc">
                                                            Ulasan Semakan Oleh {{$semakan2logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1doc as $s1logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                
                                </div>

                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'sub-contract'])
                            @slot('content')
                                @include('contract.post.partials.indexes.sub-contract')
                            @endslot
                        @endcomponent

                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>
</div>
@endsection