@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {
            $('#file_ref_no').val('{{ $warning->sst->acquisition->approval->file_reference }}');
            $('#warning_letter_no').val('{{ $warning->warning_letter_no}}');
            $('#site_meeting_no').val('{{ $warning->site_meeting_no}}');
            $('#clause').val('{{ $warning->clause}}');
            $('#sub_clause').val('{{ $warning->sub_clause}}');
            $('#scheduled_completion').val('{{ $warning->scheduled_completion}}');
            $('#actual_completion').val('{{ $warning->actual_completion}}');

            @if(!empty($warning->warning_letter_at))
                $('#warning_letter_at').val('{{ $warning->warning_letter_at ? \Carbon\Carbon::createFromFormat('Y-m-d',$warning->warning_letter_at)->format('d/m/Y'):null }}');
            @endif

            @if(!empty($warning->site_meeting_at))
                $('#site_meeting_at').val('{{ $warning->site_meeting_at ? \Carbon\Carbon::createFromFormat('Y-m-d',$warning->site_meeting_at)->format('d/m/Y'):null }}');
            @endif    

            // button for review
            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    warning_id : $('#warning_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    warning_letter_no : $('#warning_letter_no').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    warning_id : $('#warning_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    warning_letter_no : $('#warning_letter_no').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    warning_id : $('#warning_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    warning_letter_no : $('#warning_letter_no').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @endif                   
        });
    </script>
@endpush

@section('content')
    @if($warning->sst->termination_type_id == 1)
    <div class="row">
        <div class="col">
            <div class="btn-group float-right">
                <a href="{{ route('warning',['hashslug' => $warning->hashslug]) }}" 
                    target="_blank" 
                    class="btn btn-success border-success">
                    @icon('fe fe-printer') {{ __(' Cetak') }}
                </a>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <!-- <form method="POST" action="{{ route('termination.warning.update', $warning->hashslug ) }}"> -->
                <!-- @csrf 
                @method("put")
                <input type="hidden" name="hashslug" id="hashslug" value="{{ $warning->hashslug }}"/>
                <input type="hidden" name="sst_id" id="sst_id" value="{{ $warning->sst->id }}"/> -->
                @component('components.card')
                    @slot('card_body')
                        @include('contract.post.termination.warning.partials.shows.details')
                        {{--  <br/><br/>  --}}
                        @if(user()->id == $warning->sst->user_id && $warning->sst->termination_type_id == 1 && $warning->status == 3)
                            @include('contract.post.termination.warning.partials.shows.upload')
                        @endif
                        @if(!empty($review))
                            {{-- ulasan semakan --}}
                            @if(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)
                                
                                @if(!empty($semakan3log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif
                            <br>

                            {{-- semakan --}}
                            @if(user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'Penamatan' && $review->document_contract_type == 'Amaran')
                                <div class="card row col-12"><br>
                                    <h3>Semakan</h3>
                                    <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                        @csrf

                                        @include('components.forms.hidden', [
                                            'id' => 'id',
                                            'name' => 'id',
                                            'value' => ''
                                        ])

                                        @if(!empty($warning))
                                            @include('components.forms.hidden', [
                                                'id' => 'acquisition_id',
                                                'name' => 'acquisition_id',
                                                'value' => $warning->sst->acquisition_id
                                            ])

                                            <input type="text" id="warning_id" name="warning_id" value="@if(!empty($warning)){{ $warning->id }}@endif" hidden>
                                        @endif

                                        @if(!empty($review))
                                        <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
                                        <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                        <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
                                        @endif

                                        @if(!empty($review_previous))
                                            <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                            <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                            <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                            <input type="text" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                            <input type="text" id="type" name="type" value="Penamatan" hidden>
                                            <input type="text" id="document_contract_type" name="document_contract_type" value="Amaran" hidden>
                                        @endif

                                        <input type="text" id="warning_letter_no" name="warning_letter_no" value="{{$warning->warning_letter_no}}" hidden>

                                        <div class="row">
                                            <div class="col-6">
                                                @include('components.forms.input', [
                                                    'input_label' => __('Nama Pegawai Semakan'),
                                                    'id' => '',
                                                    'name' => '',
                                                    'value' => user()->name,
                                                    'readonly' => true
                                                ])
                                            </div>
                                                @include('components.forms.hidden', [
                                                    'id' => 'requested_by',
                                                    'name' => 'requested_by',
                                                    'value' => user()->id
                                                ])
                                                @include('components.forms.hidden', [
                                                    'id' => 'approved_by',
                                                    'name' => 'approved_by',
                                                    'value' => user()->supervisor->id
                                                ])
                                            <div class="col-3">
                                                @if(!empty($review))
                                                    @if(!empty($review->status))
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Terima'),
                                                            'id' => 'requested_at',
                                                            'name' => 'requested_at',
                                                        ])
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="col-3">
                                                @if(!empty($review))
                                                    @if(!empty($review->status))
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Hantar'),
                                                            'id' => 'approved_ats',
                                                            'name' => 'approved_at',
                                                        ])
                                                    @endif
                                                @endif
                                            </div>
                                        </div>

                                        @if(!empty($review) && $review->created_by != user()->id)
                                            @include('components.forms.textarea', [
                                                'input_label' => __('Ulasan'),
                                                'id' => 'remarks',
                                                'name' => 'remarks'
                                            ])
                                        @else

                                        @endif
                                        
                                        <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                        <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                                        <div class="btn-group float-right">
                                            @if(!empty($review) && $review->created_by != user()->id)
                                                <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                                    @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                                </button>
                                                <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                                    @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                                </button>
                                            @endif
                                            <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                                @icon('fe fe-send') {{ __('Teratur') }}
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            @endif

                            {{-- log semakan --}}

                            @if(user()->current_role_login == 'administrator')
                                @if(!empty($warning))
                                    <div class="tab-pane fade show " id="document-contract-review-log" role="tabpanel" aria-labelledby="document-contract-review-log-tab">
                                        <form id="log-form">
                                            <div class="row justify-content-center w-100">
                                                <div class="col-12 w-100">
                                                    @include('contract.pre.box.partials.scripts')
                                                    @component('components.card')
                                                        @slot('card_body')
                                                            @component('components.datatable', 
                                                                [
                                                                    'table_id' => 'contract-pre-box',
                                                                    'route_name' => 'api.datatable.contract.review-log-warning',
                                                                    'param' => 'acquisition_id=' . $warning->sst->acquisition_id,
                                                                    'columns' => [
                                                                        ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                        ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                        ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                        ['data' => 'jenis', 'title' => __('Jenis'), 'defaultContent' => '-'],
                                                                        ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                        ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                        ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                        ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                    ],
                                                                    'headers' => [
                                                                        __('Bil'),__('Penyemak'), __('Ulasan Semakan'), __('Jenis'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                                    ],
                                                                    'actions' => minify('')
                                                                ]
                                                            )
                                                            @endcomponent
                                                        @endslot
                                                    @endcomponent
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            @endif
                        @endif
                        {{-- @slot('card_footer')
                            <div class="btn-group float-right">
                                <a href="{{ route('termination.warning.index', ['hashslug'=>$warning->sst->hashslug]) }}" 
                                    class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>
                                <a href="{{ route('termination.termination.edit', ['hashslug'=>$warning->sst->hashslug]) }}" 
                                    class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>     
                            </div>
                        @endslot --}}
                    @endslot

                    @slot('card_footer')
                        <div class="btn-group float-right">
                            <a href="{{ route('termination.termination.edit', ['hashslug'=>$warning->sst->hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                            </a>
                        </div>
                    </div>
                    @endslot
                @endcomponent
            <!-- </form> -->
        </div>
    </div>
@endsection