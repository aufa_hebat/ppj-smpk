@extends('layouts.admin')
@push('scripts')
@include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {

            $('#file_ref_no').val('{{ $sst->acquisition->approval->file_reference }}');
            $('#warning_letter_no').val('{{ $warning->count() + 1}}');
        });
    </script>
@endpush
@section('content')
    @include('contract.post.termination.warning.partials.scripts')
    <div class="row justify-content-center">  
        <div class="col">
            @component('components.pages.title-sub')
                @slot('title_sub_content')
                    <span class="font-weight-bold">Tajuk : </span>
                        {{ $sst->acquisition->title }}<br/>
                    <span class="font-weight-bold">No. Kontrak : </span>
                        {{ $sst->acquisition->reference }}
                    <span class="font-weight-bold">Nama Kontraktor : </span>
                        {{ $sst->company->company_name }}
                    <span class="font-weight-bold">Nilai Kontrak : </span>
                        RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}
                        <br/>
                    <span class="font-weight-bold">Tarikh Mula Kerja : </span>
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}
                    <span class="font-weight-bold">Tarikh Siap Kerja : </span>
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}
                    <span class="font-weight-bold">Tempoh : </span>
                        {{ $sst->period." ".$sst->period_type->name }}

                @endslot
            @endcomponent



            @component('components.card')
                @slot('card_title')
                    @if($warning->count() < 3)
					<a href="{{ route('termination.warning.create', ['hashslug' => $sst->hashslug]) }}" class="btn btn-primary float-right">
						@icon('fe fe-plus') {{ __('Surat Amaran Baru') }}
					</a>
                    @endif
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'warning',
                            'param' => 'hashslug=' . $hashslug,
                            'route_name' => 'api.datatable.contract.warning',
                            'columns' => [
                                    ['data' => 'warning_letter_no', 'title' => __('Bil Surat Amaran'), 'defaultContent' => '-'],
                                    ['data' => 'warning_letter_at', 'title' => __('Tarikh Surat Amaran'), 'defaultContent' => '-'],
                                    ['data' => 'site_meeting_at', 'title' => __('Tarikh Mesyuarat Tapak'), 'defaultContent' => '-'],
                                    ['data' => 'scheduled_completion', 'title' => __('% Siap Kerja'), 'defaultContent' => '-'],
                                    ['data' => 'actual_completion', 'title' => __('% Kemajuan Kerja'), 'defaultContent' => '-'],
                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                     __('Bil Surat Amaran'), __('Tarikh Surat Amaran'), __('Tarikh Mesyuarat Tapak'), __('% Siap Kerja'), __('% Kemajuan Kerja'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.termination.warning.partials.actions')->render())
                        ]
                    )
                    @endcomponent

 
                @endslot
            @endcomponent
        </div>
    </div>
@endsection