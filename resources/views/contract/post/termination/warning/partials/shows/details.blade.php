@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            @if(!empty($warning) && !empty($warning->documents))
                @foreach($warning->documents as $document)
                    $('#subFile').val('{{ $document->document_name }}');
                @endforeach
            @endif
        });
    </script>
@endpush

@if($warning->sst->termination_type_id == 1)
    <div class="row">
        <div class="col-6">
		    @include('components.forms.input', [
			    'input_label' => 'No Rujukan Fail',
			    'name' => 'file_ref_no',
                'id' => 'file_ref_no',
                'readonly' => true
		    ]) 

        </div>
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Surat Amaran'),
                'id' => 'warning_letter_at',
                'name' => 'warning_letter_at',
                'config' => ['format' => config('datetime.display.date')],
                'readonly' => true
            ])
        </div>
        <div class="col-6">
		    @include('components.forms.input', [
			    'input_label' => 'Bil. Surat Amaran',
			    'name' => 'warning_letter_no',
                'id' => 'warning_letter_no',
                'readonly' => true
		    ])            
        </div>     
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
			    'input_label' => '% Siap Kerja (sepatutnya)',
			    'name' => 'scheduled_completion',
                'id' => 'scheduled_completion',
                'readonly' => true
		    ])       
        </div>
        <div class="col-6">
            @include('components.forms.input', [
			    'input_label' => '% Kemajuan Kerja Siap',
			    'name' => 'actual_completion',
                'id' => 'actual_completion',
                'readonly' => true
		    ])       
        </div>        
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
			    'input_label' => 'Klausa',
			    'name' => 'clause',
                'id' => 'clause',
                'readonly' => true
		    ]) 
        </div>
        <div class="col-6">
            @include('components.forms.input', [
			    'input_label' => 'Sub Klausa',
			    'name' => 'sub_clause',
                'id' => 'sub_clause',
                'readonly' => true
		    ]) 
        </div>        
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Mesyuarat Tapak'),
                'id' => 'site_meeting_at',
                'name' => 'site_meeting_at',
                'config' => ['format' => config('datetime.display.date')],
                'readonly' => true
            ])
        </div>
        <div class="col-6">
		    @include('components.forms.input', [
			    'input_label' => 'Bil. Mesyuarat Tapak',
			    'name' => 'site_meeting_no',
                'id' => 'site_meeting_no',
                'readonly' => true
		    ])            
        </div> 
    </div>
@else
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => 'No Rujukan Fail',
                'name' => 'file_ref_no',
                'id' => 'file_ref_no',
                'readonly' => true
            ])         
        </div>
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => 'Bil. Surat Amaran',
                'name' => 'warning_letter_no',
                'id' => 'warning_letter_no',
                'readonly' => true
            ])            
        </div>     
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group row">
                <label for="Fail_Keputusan_Mesyuarat"
                    class="col col-form-label">
                    Muat Naik Surat Amaran
                </label>
                        
                <div class="col input-group">
                    <input id="subFile" type="text"  class="form-control" readonly>
                    <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                    <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                    @if(!empty($warning) && !empty($warning->documents))
                        @foreach($warning->documents as $document)
                            <label class="input-group-text" for="downloadFile">
                                <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                            </label>
                        @endforeach
                    @endif
                </div>                        
            </div>
        </div>
    </div>
@endif