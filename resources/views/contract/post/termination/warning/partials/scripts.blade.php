@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {

		$(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.warning.show', id));
		});

		$(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.warning.edit', id));
		});

		$(document).on('click', '.destroy-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: '{!! __('Amaran') !!}',
			  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: '{!! __('Ya') !!}',
			  cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  if (result.value) {
			  	$('#destroy-record-form').attr('action', route('termination.warning.destroy', id))
				$('#destroy-record-form').submit();
			  }
			});
		});
	});
</script>
@endpush