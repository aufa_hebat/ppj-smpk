@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    warning_id : $('#warning_id').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    warning_letter_no : $('#warning_letter_no').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#file_ref_no').val('{{ $warning->sst->acquisition->approval->file_reference }}');
            $('#warning_letter_no').val('{{ $warning->warning_letter_no}}');
            $('#site_meeting_no').val('{{ $warning->site_meeting_no}}');
            $('#clause').val('{{ $warning->clause}}');
            $('#sub_clause').val('{{ $warning->sub_clause}}');
            $('#scheduled_completion').val('{{ $warning->scheduled_completion}}');
            $('#actual_completion').val('{{ $warning->actual_completion}}');

            @if(!empty($warning->warning_letter_at))
                $('#warning_letter_at').val('{{ $warning->warning_letter_at ? \Carbon\Carbon::createFromFormat('Y-m-d',$warning->warning_letter_at)->format('d/m/Y'):null }}');
            @endif

            @if(!empty($warning->site_meeting_at))
                $('#site_meeting_at').val('{{ $warning->site_meeting_at ? \Carbon\Carbon::createFromFormat('Y-m-d',$warning->site_meeting_at)->format('d/m/Y'):null }}');
            @endif                        
        });
    </script>
@endpush

@section('content')

<div class="row">
    <div class="col-12">
        <form method="POST" action="{{ route('termination.warning.update', $warning->hashslug ) }}">
            @csrf 
            @method("put")
            <input type="hidden" name="hashslug" id="hashslug" value="{{ $warning->hashslug }}"/>
            <input type="hidden" name="sst_id" id="sst_id" value="{{ $warning->sst->id }}"/>
            @component('components.card')
                @slot('card_body')
                    @include('contract.post.termination.warning.partials.forms.details', ['sst' =>$warning->sst, 'warning' => $warning])

                    <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                        @if((!empty($review)) && (user()->id == $review->created_by))
                        
                            {{-- penyedia --}}

                            @if(!empty($semakan3log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl2 as $s2log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan2log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl1 as $s1log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endif
                    
                    </div>
                    
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('termination.termination.edit', ['hashslug'=>$warning->sst->hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                        
                        <button type="submit" class="btn btn-primary" id="btnSubmit" name="btnSubmit" value="Simpan">
                                @icon('fe fe-save') {{ __('Simpan') }}
                        </button>
                        
                        @if($warning->sst->termination_type_id == 2)
                            <button type="submit" class="btn btn-success"  id="btnSubmit" name="btnSubmit" value="Selesai">
                                @icon('fe fe-save') {{ __('Selesai') }}
                            </button>
                        @endif
        </form>

                        @role('penyedia')
                            @if(!empty($warning) && $warning->sst->user_id == user()->id && $warning->sst->termination_type_id == 1)
                                @if(empty($review) || empty($review->status))
                                    <form method="POST" action="{{ route('acquisition.review.store') }}">
                                        @csrf
                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                        <input type="text" id="acquisition_id" name="acquisition_id" value="@if(!empty($warning)){{ $warning->sst->acquisition_id }}@endif" hidden>
                                        <input type="text" id="warning_id" name="warning_id" value="@if(!empty($warning)){{ $warning->id }}@endif" hidden>

                                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                                        <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                                        @if(!empty($review_previous))
                                            <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                            <input type="text" id="type" name="type" value="Penamatan" hidden>
                                            <input type="text" id="document_contract_type" name="document_contract_type" value="Amaran" hidden>
                                        @endif
                                        <input type="text" id="warning_letter_no" name="warning_letter_no" value="{{$warning->warning_letter_no}}" hidden>
                                        
                                        <button type="button" id="send" class="btn btn-default float-middle border-default">
                                            @icon('fe fe-send') {{ __('Teratur') }}
                                        </button>
                                    </form>
                                @endif
                            @endif
                        @endrole
                    </div>

                    @role('administrator')
                        @if(!empty($warning))
                            <div class="tab-pane fade show " id="sst-review-log" role="tabpanel" aria-labelledby="sst-review-log-tab">
                                <form id="log-form">
                                    <div class="row justify-content-center w-100">
                                        <div class="col-12 w-100">
                                            @include('contract.pre.box.partials.scripts')
                                            @component('components.card')
                                                @slot('card_body')
                                                    @component('components.datatable', 
                                                        [
                                                            'table_id' => 'contract-pre-box',
                                                            'route_name' => 'api.datatable.contract.review-log-warning',
                                                            'param' => 'acquisition_id=' . $warning->sst->acquisition_id,
                                                            'columns' => [
                                                                ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                            ],
                                                            'headers' => [
                                                                __('Bil'),__('Penyemak'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'),  __('Status'), __('')
                                                            ],
                                                            'actions' => minify('')
                                                        ]
                                                    )
                                                    @endcomponent
                                                @endslot
                                            @endcomponent
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    @endrole

                @endslot
                @endslot
            @endcomponent
    </div>
</div>

@endsection