<html>
    <head>

        <style type="text/css">
            @page {
                margin: 70px 70px 70px 100px;
            }

            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }        

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
        </style>
    </head>
    <body>
        <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>

        <center>
            <table  style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr>
                    <td width="15%">Ruj. Tuan</td>
                    <td width="5%"> : </td>
                    <td width="80%"></td>
                </tr>
                <tr>
                    <td>Ruj. Kami</td>
                    <td> : </td>
                    <td>{{ $warning->sst->acquisition->reference }}</td>
                </tr>
                <tr>
                    <td>Tarikh</td>
                    <td> : </td>
                    <td>{{ \Carbon::intlFormat($warning->warning_letter_at) }}</td>
                </tr> 
                <tr>
                    <td colspan="3"><br/></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                            <tr>
                                <td colspan="2"><span style="text-transform: uppercase; font-weight: bold">{{ $appointed->company->company_name}}, </span></td>
                            </tr>
                            @if(!empty($appointed->company->addresses[0]))
                            <tr>
                                <td>{{ $appointed->company->addresses[0]->primary }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>{{ $appointed->company->addresses[0]->secondary }}</td>
                                <td></td>
                            </tr>   
                            <tr>
                                <td width="70%">{{ $appointed->company->addresses[0]->postcode }}, {{ $appointed->company->addresses[0]->state }}</td>
                                <td width="30%">No. Tel :  
                                    @if(!empty($appointed->company->phones[0]))
                                        {{ $appointed->company->phones[0]->phone_number }}
                                    @endif
                                </td>
                            </tr>                                      
                            @endif
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><br/></td>
                </tr>
                <tr>
                    <td colspan="3">Tuan, </td>
                </tr>
                <tr>
                    <td colspan="3"><br/></td>
                </tr>        
                <tr>
                    <td colspan="3">
                        <span style="text-transform: uppercase; font-weight: bold;">
                            @if($warning->warning_letter_no == '3')
                                Surat Peringatan Notis Penamatan
                            @else
                                Amaran Kemungkiran Kontraktor 
                            @endif
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <span style="font-style:italic">@if($warning->warning_letter_no == '1') - Peringatan Pertama :  @endif</span>
                        <span style="font-style:italic">@if($warning->warning_letter_no == '2') - Peringatan Kedua :  @endif</span>
                        <span style="font-style:italic">@if($warning->warning_letter_no == '3') - Peringatan Terakhir :  @endif</span>
                        <span style="font-style:italic">@if($warning->warning_letter_no == '4') - Peringatan Keempat :  @endif</span>
                        <span style="font-style:italic">@if($warning->warning_letter_no == '5') - Peringatan Kelima :  @endif</span>
                        <span style="font-style:italic">@if($warning->warning_letter_no == '6') - Peringatan Keenam :  @endif</span>
                        <span style="font-style:italic">{{ $warning->clause }}</span>
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><br/></td>
                </tr>

                <tr>
                    <td colspan="3">
                        <table style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                            <tr>
                                <td width="30%" valign="top">Nombor Kontrak</td>
                                <td width="3%" valign="top"> : </td>
                                <td width="67%" valign="top">{{ $warning->sst->contract_no }}</td>
                            </tr>
                            <tr>
                                <td valign="top">Tajuk Kontrak</td>
                                <td valign="top"> : </td>
                                <td style="text-align: justify" valign="top">{{ $warning->sst->acquisition->title }}</td>
                            </tr>
                            <tr>
                                <td valign="top">Jumlah Harga Kontrak</td>
                                <td valign="top"> : </td>
                                <td valign="top">{!! money()->toHuman($appointed->offered_price) !!}</td>
                            </tr>  
                            <tr>
                                <td valign="top">Tarikh Milik Tapak Bina</td>
                                <td valign="top"> : </td>
                                <td valign="top">{{ isset($warning->sst->start_working_date) ? \Carbon::intlFormat($warning->sst->start_working_date)  : "-" }}</td>
                            </tr> 
                            <tr>
                                <td>Tarikh Siap Kerja</td>
                                <td> : </td>
                                <td>{{ isset($warning->sst->end_working_date) ? \Carbon::intlFormat($warning->sst->end_working_date) : "-" }}</td>
                            </tr>  
                            <tr><td colspan="3"><br /><br /></td></tr>
                            <tr>
                                <td colspan="3"  style="text-align: justify">
                                    Dukacita diperhatikan bahawa tuan didapati tidak menjalankan kerja dengan bersungguh-sungguh dan kerja 
                                    tuan dijangka tidak dapat disiapkan pada Tarikh Siap Kerja yang telah ditentukan pada 
                                    <span style="font-weight: bold;">{{ isset($warning->sst->end_working_date) ?  \Carbon::intlFormat($warning->sst->end_working_date) : "-" }}</span>.
                                    Kerja tuan sepatutnya sudah <span style="font-weight: bold;">{{ isset($warning->scheduled_completion) ? $warning->scheduled_completion : "0" }} %</span> 
                                    siap tetapi kemajuan yang dicapai setakat ini adalah hanya 
                                    <span style="font-weight: bold;">{{ isset($warning->actual_completion) ? $warning->actual_completion : "0" }} %</span>.
                                    
                                    @if($warning->warning_letter_no > 1)
                                    Kegagalan tuan itu masih berulang sungguhpun tuan telah diberi amaran di Mesyuarat Tapak No. 
                                    <span style="font-weight: bold;">{{ $warning->site_meeting_no }} </span> pada 
                                    <span style="font-weight: bold;">{{ isset($warning->site_meeting_at) ? \Carbon\Carbon::CreateFromFormat('Y-m-d', $warning->site_meeting_at)->format('d/m/Y') : "-" }}</span>.
                                    @endif
                                </td>
                            </tr>  
                            <tr><td colspan="3"><br /></td></tr>
                            <tr>
                                <td colspan="3" style="text-align: justify">
                                    Tuan adalah dinasihatkan untuk meneruskan kerja serta menjalankan kerja dengan bersungguh-sungguh agar projek ini dapat disiapkan 
                                    dalam tempoh yang ditetapkan.
                                </td>
                            </tr> 
                            <tr><td colspan="3"><br /></td></tr>
                            <tr>
                                <td colspan="3" style="text-align: justify">
                                    Sila tuan berikan sebab-sebab yang munasabah mengapa perjalanan kerja telah tergantung serta masalah-masalah yang dihadapi dan 
                                    tindakan yang akan diambil untuk mengatasi masalah-masalah tersebut.
                                </td>
                            </tr>  
                            <tr><td colspan="3"><br /></td></tr>
                            <tr>
                                <td colspan="3" style="text-align: justify">
                                    Sekiranya tuan gagal mematuhi perkara di atas, Pejabat ini akan mengambil tindakan sewajarnya selaras dengan syarat-syarat kontrak.
                                </td>
                            </tr>                                                                                              
                        </table>
                    </td>
                </tr>
                <tr><td><br></td></tr>
                <tr>
                    <td colspan="3">Sekian terima kasih<br></td>
                </tr>
                <tr><td><br><br /></td></tr>
                <tr>
                    <td colspan="3">Saya yang menurut perintah,</td>
                </tr>
                <tr><td colspan="3"><br /><br/></td></tr>
                <tr>
                    <td colspan="3">
                        ....................................................
                        <br/>

                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-transform: uppercase;" class="font-weight-bold"> {{(!empty($np_dept)) ? '('.$np_dept->name.')': '' }} </td>
                </tr>
                <tr>
                    <td colspan="3">{{(!empty($np_dept)) ? $np_dept->position->name : '' }} {{(!empty($np_dept)) ? $np_dept->department->name : ''}}</td>
                </tr>    
                <tr><td colspan="3"><br /></td></tr>    
                <tr>
                    <td colspan="2"><b>Sk:</b></td>
                </tr>
                <tr>
                    <td colspan="3">{{(!empty($np_dept)) ? 'Jabatan ' . $np_dept->department->name : '' }}</td>
                </tr>
                <tr>
                    <td colspan="3">Jabatan Kewangan</td>
                </tr>        
            </table>
            <!-- <table  style="width: 80%;border-collapse: collapse;padding: 4 4;" align="center">
                <tr>

                </tr>
                <tr>
                    <td style="width: 10%"><b>PERBADANAN PUTRAJAYA</b><br>
                        Kompleks Perbadanan Putrajaya,<br>
                        24, Persiaran Perdana,<br>
                        Presint 3,<br>
                        62675 Putrajaya<br>
                        MALAYSIA</td>
                    <td style="width: 10%"></td>
                </tr>

            </table> -->
        </center>
    </body>
</html>