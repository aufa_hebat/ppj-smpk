@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {
            $('#file_ref_no').val('{{ $sst->acquisition->approval->file_reference }}');
            $('#warning_letter_no').val('{{ $warning->count() + 1}}');
        });
    </script>
@endpush

@section('content')

<div class="row">
    <div class="col-12">
        <form method="POST" action="{{ route('termination.warning.store') }}" files = "true" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="hashslug" id="hashslug" value="{{ $hashslug }}"/>
            <input type="hidden" name="sst_id" 
                id="sst_id" 
                value="{{ $sst->id }}"/>
            <input type="hidden" name="status" id="status" value="1"/>
            @component('components.card')
                @slot('card_body')
                    @include('contract.post.termination.warning.partials.forms.details',['sst' => $sst, 'warning' =>$warning])
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('termination.warning.index', ['hashslug'=>$hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Batal') }}
                        </a>
                        <button type="submit" class="btn btn-primary">
                                @icon('fe fe-save') {{ __('Simpan') }}
                        </button>
                    </div>
                @endslot
                @endslot
            @endcomponent
        </form>
    </div>
</div>


@endsection