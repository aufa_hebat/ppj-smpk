@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')

    <script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
            // $('#terminate-tab-content a').click(function (e) {
            //     e.preventDefault();
            //     $(this).tab('show');
            // });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#terminate-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh

            // start semakan 
            // button for review
            $('#send_notice').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus_notice1').val(),
                    acquisition_id : $('#acquisition_id_notice1').val(),
                    requested_by : $('#requested_by_notice1').val(),
                    progress : $('#progress_notice1').val(),
                    task_by : $('#task_by_notice1').val(),
                    law_task_by : $('#law_task_by_notice1').val(),
                    approved_by : $('#approved_by_notice1').val(),
                    created_by : $('#created_by_notice1').val(),
                    approved_at : $('#approved_at_notice1').val(),
                    type : $('#type_notice1').val(),
                    document_contract_type : $('#document_contract_type_notice1').val(),
                    status : $('#status_notice1').val(),
                    department : $('#department_notice1').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send_notice').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savDraf_notice').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus_notice').val(),
                    acquisition_id : $('#acquisition_id_notice').val(),
                    requested_by : $('#requested_by_notice').val(),
                    progress : $('#progress_notice').val(),
                    task_by : $('#task_by_notice').val(),
                    law_task_by : $('#law_task_by_notice').val(),
                    approved_by : $('#approved_by_notice').val(),
                    created_by : $('#created_by_notice').val(),
                    approved_at : $('#approved_at_notice').val(),
                    type : $('#type_notice').val(),
                    document_contract_type : $('#document_contract_type_notice').val(),
                    status : $('#status_notice').val(),
                    department : $('#department_notice').val(),
                    remarks : $('#remarks_notice').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent_notice').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus_notice').val(),
                    acquisition_id : $('#acquisition_id_notice').val(),
                    requested_by : $('#requested_by_notice').val(),
                    progress : $('#progress_notice').val(),
                    task_by : $('#task_by_notice').val(),
                    law_task_by : $('#law_task_by_notice').val(),
                    approved_by : $('#approved_by_notice').val(),
                    created_by : $('#created_by_notice').val(),
                    approved_at : $('#approved_at_notice').val(),
                    type : $('#type_notice').val(),
                    document_contract_type : $('#document_contract_type_notice').val(),
                    status : $('#status_notice').val(),
                    department : $('#department_notice').val(),
                    remarks : $('#remarks_notice').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf_notice').hide();
                                $('#savSent_notice').hide();
                                $('#rejSent_notice').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent_notice').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus_notice').val(),
                    acquisition_id : $('#acquisition_id_notice').val(),
                    requested_by : $('#requested_by_notice').val(),
                    progress : $('#progress_notice').val(),
                    task_by : $('#task_by_notice').val(),
                    law_task_by : $('#law_task_by_notice').val(),
                    approved_by : $('#approved_by_notice').val(),
                    created_by : $('#created_by_notice').val(),
                    approved_at : $('#approved_at_notice').val(),
                    type : $('#type_notice').val(),
                    document_contract_type : $('#document_contract_type_notice').val(),
                    status : $('#status_notice').val(),
                    department : $('#department_notice').val(),
                    remarks : $('#remarks_notice').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf_notice').hide();
                                $('#savSent_notice').hide();
                                $('#rejSent_notice').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#send_reason').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus_reason1').val(),
                    acquisition_id : $('#acquisition_id_reason1').val(),
                    requested_by : $('#requested_by_reason1').val(),
                    progress : $('#progress_reason1').val(),
                    task_by : $('#task_by_reason1').val(),
                    law_task_by : $('#law_task_by_reason1').val(),
                    approved_by : $('#approved_by_reason1').val(),
                    created_by : $('#created_by_reason1').val(),
                    approved_at : $('#approved_at_reason1').val(),
                    type : $('#type_reason1').val(),
                    document_contract_type : $('#document_contract_type_reason1').val(),
                    status : $('#status_reason1').val(),
                    department : $('#department_reason1').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send_reason').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savDraf_reason').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus_reason').val(),
                    acquisition_id : $('#acquisition_id_reason').val(),
                    requested_by : $('#requested_by_reason').val(),
                    progress : $('#progress_reason').val(),
                    task_by : $('#task_by_reason').val(),
                    law_task_by : $('#law_task_by_reason').val(),
                    approved_by : $('#approved_by_reason').val(),
                    created_by : $('#created_by_reason').val(),
                    approved_at : $('#approved_at_reason').val(),
                    type : $('#type_reason').val(),
                    document_contract_type : $('#document_contract_type_reason').val(),
                    status : $('#status_reason').val(),
                    department : $('#department_reason').val(),
                    remarks : $('#remarks_reason').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent_reason').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus_reason').val(),
                    acquisition_id : $('#acquisition_id_reason').val(),
                    requested_by : $('#requested_by_reason').val(),
                    progress : $('#progress_reason').val(),
                    task_by : $('#task_by_reason').val(),
                    law_task_by : $('#law_task_by_reason').val(),
                    approved_by : $('#approved_by_reason').val(),
                    created_by : $('#created_by_reason').val(),
                    approved_at : $('#approved_at_reason').val(),
                    type : $('#type_reason').val(),
                    document_contract_type : $('#document_contract_type_reason').val(),
                    status : $('#status_reason').val(),
                    department : $('#department_reason').val(),
                    remarks : $('#remarks_reason').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf_reason').hide();
                                $('#savSent_reason').hide();
                                $('#rejSent_reason').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent_reason').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus_reason').val(),
                    acquisition_id : $('#acquisition_id_reason').val(),
                    requested_by : $('#requested_by_reason').val(),
                    progress : $('#progress_reason').val(),
                    task_by : $('#task_by_reason').val(),
                    law_task_by : $('#law_task_by_reason').val(),
                    approved_by : $('#approved_by_reason').val(),
                    created_by : $('#created_by_reason').val(),
                    approved_at : $('#approved_at_reason').val(),
                    type : $('#type_reason').val(),
                    document_contract_type : $('#document_contract_type_reason').val(),
                    status : $('#status_reason').val(),
                    department : $('#department_reason').val(),
                    remarks : $('#remarks_reason').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf_reason').hide();
                                $('#savSent_reason').hide();
                                $('#rejSent_reason').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(!empty($review_notice->approved_at))
            $('#requested_at_notice').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review_notice->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats_notice').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1_notice) && $s1_notice = $review_notice)
                @if($s1_notice->reviews_status == 'Deraf')
                    $('#remarks_notice').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1_notice->remarks) !!}');
                @endif
            @elseif(!empty($s2_notice) && $s2_notice = $review_notice)
                @if($s2_notice->reviews_status == 'Deraf')
                    $('#remarks_notice').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2_notice->remarks) !!}');
                @endif
            @endif

            @if(!empty($review_reason->approved_at))
            $('#requested_at_reason').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review_reason->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats_reason').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1_reason) && $s1_reason = $review_reason)
                @if($s1_reason->reviews_status == 'Deraf')
                    $('#remarks_reason').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1_reason->remarks) !!}');
                @endif
            @elseif(!empty($s2_reason) && $s2_reason = $review_reason)
                @if($s2_reason->reviews_status == 'Deraf')
                    $('#remarks_reason').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2_reason->remarks) !!}');
                @endif
            @endif
            // end semakan

            $('#file_ref_no').val('{{ $sst->acquisition->approval->file_reference }}');
            $('#terminate_file_ref_no').val('{{ $sst->acquisition->approval->file_reference }}');

            /*@if(!empty($sst->termination_at))
                $('#termination_at').val('{{ $sst->termination_at->format("d/m/Y") }}');
                $('#others_termination_at').val('{{ $sst->termination_at->format("d/m/Y") }}');
            @endif  */

            @if(!empty($reasonNotice))

                var notice_at, notice_at_mmt, minDate;

                @if(!empty($reasonNotice->notice_at))
                    $('#notice_at').val('{{ $reasonNotice->notice_at->format("d/m/Y") }}');

                    notice_at = "{{ $reasonNotice->notice_at->format("d/m/Y") }}";
                    notice_at_mmt = moment(notice_at,'DD/MM/YYYY'); 
                    minDate = moment(notice_at_mmt).add(14, 'days');

                    $('#datetimepicker-terminate_notice_at').datetimepicker({'minDate': minDate, format:'DD/MM/YYYY'}); 
                @endif                
                
                $("#scheduled_completion").val('{{ $reasonNotice->scheduled_completion }}');
                $("#actual_completion").val('{{ $reasonNotice->actual_completion }}');
                $("#clause").val('{{ $reasonNotice->clause }}');
                $("#sub_clause").val('{{ $reasonNotice->sub_clause }}');
            @endif

            @if(!empty($terminateNotice))
                @if(!empty($terminateNotice->notice_at))
                    $('#terminate_notice_at').val('{{ $terminateNotice->notice_at->format("d/m/Y") }}');

                        var terminate_notice_at, terminate_notice_at_mmt, minDate;
                        
                        terminate_notice_at = "{{ $terminateNotice->notice_at->format("d/m/Y") }}";
                        terminate_notice_at_mmt = moment(terminate_notice_at,'DD/MM/YYYY'); 
                        minDate = moment(terminate_notice_at_mmt).add(30, 'days');

                        $('#datetimepicker-termination_at').datetimepicker({'minDate': minDate, format:'DD/MM/YYYY'}); 

                @endif

                @if(!empty($terminateNotice->termination_at))
                    $('#termination_at').val('{{ $terminateNotice->termination_at->format("d/m/Y") }}');
                @endif
                
                $("#terminate_scheduled_completion").val('{{ $terminateNotice->scheduled_completion }}');
                $("#terminate_actual_completion").val('{{ $terminateNotice->actual_completion }}');
                $("#terminate_clause").val('{{ $terminateNotice->clause }}');
                $("#terminate_sub_clause").val('{{ $terminateNotice->sub_clause }}');
            @endif  


            @if(!empty($othersTerminateNotice))
                @if(!empty($othersTerminateNotice->termination_at))
                    $('#others_termination_at').val('{{ $othersTerminateNotice->termination_at->format("d/m/Y") }}');
                @endif
                $("#description").val('{{ $othersTerminateNotice->description }}');
            @endif

            $("#datetimepicker-terminate_notice_at").on("change.datetimepicker", function (selected) {
                if(selected.date == undefined){

                }else{
                    document.getElementById('termination_at').value = "";
                    var minDate = moment((selected.date)).add(30, 'days');
                    $('#datetimepicker-termination_at').datetimepicker('minDate', minDate);
                }

            });            
                 

            if($('#termination_type_id').val() == 1){
                $('#others').hide(); 
                
                @if($sst->acquisition->approval->acquisition_type_id == 1)
                    $('#warning').show();                      
                    $('#reason-letter').hide();
                    $('#terminate-letter').hide(); 

                    
                    @if($warning->count() > 0)
                        $('#reason-letter').show();
                        @if(!empty($reasonNotice) && $reasonNotice->status == 3))
                            $('#terminate-letter').show();
                        @endif
                    @endif

                @elseif($sst->acquisition->approval->acquisition_type_id == 2)
                    $('#warning').show();                      
                    $('#reason-letter').hide();
                    $('#terminate-letter').hide(); 

                    @if($warning->count() > 1)
                        $('#reason-letter').show();
                        @if(!empty($reasonNotice) && $reasonNotice->status == 3))
                            $('#terminate-letter').show();
                        @endif
                    @endif
                @elseif($sst->acquisition->approval->acquisition_type_id == 3 || 
                $sst->acquisition->approval->acquisition_type_id == 4)
                    $('#warning').hide();                      
                    $('#reason-letter').show();
                    @if(!empty($reasonNotice) && $reasonNotice->status == 3))
                        $('#terminate-letter').show(); 
                    @endif
                @endif

            }else if($('#termination_type_id').val() == 2){

                $("#type_suspension").val('{{ $sst->type_suspension }}');
                $('#jenis_notis_gantung').show();
                
                $('#reason-letter').hide();
                $('#terminate-letter').hide();
                $('#others').hide();

                if($('#type_suspension').val() == 2){
                    $('#warning').show();
                }else{
                    $('#warning').hide();
                }

                @if($warning->count() > 1)
                    $('#terminate-letter').show();
                @endif
                

            }else if($('#termination_type_id').val() == 3){
                $('#warning').hide();
                $('#reason-letter').show();
                $('#terminate-letter').hide();
                @if(!empty($reasonNotice) && $reasonNotice->status == 3))
                    $('#terminate-letter').show();
                @endif
                $('#others').hide();

            }else if( $('#termination_type_id').val() == 4){
                $('#warning').hide();
                $('#reason-letter').hide();
                $('#terminate-letter').show();
                $('#others').hide();
            }else if($('#termination_type_id').val() == 5){
                $('#warning').hide();
                $('#reason-letter').hide();
                $('#terminate-letter').hide();
                $('#others').show();
            }else{
                $('#warning').hide();
                $('#reason-letter').hide();
                $('#terminate-letter').hide();
                $('#others').hide();
            }

            $('#termination_type_id').change(function () {
                if($('#termination_type_id').val() == 1){
                    $('#warning').show();
                    $('#others').hide();

                    @if($warning->count() > 2)
                        $('#reason-letter').show();
                        $('#terminate-letter').show();
                    @else
                        $('#reason-letter').hide();
                        $('#terminate-letter').hide();
                    @endif    
                }else if($('#termination_type_id').val() == 2 || $('#termination_type_id').val() == 3 || $('#termination_type_id').val() == 4){
                    $('#warning').hide();
                    $('#reason-letter').show();
                    $('#terminate-letter').show();                                    
                    $('#others').hide();

                    //special case for penamatan bersama (termination_type = 2)
                    if($('#termination_type_id').val() == 2){

                    }

                }else if($('#termination_type_id').val() == 5){
                    $('#warning').hide();
                    $('#reason-letter').hide();
                    $('#terminate-letter').hide();                                    
                    $('#others').show();
                }else{
                    $('#warning').hide();
                    $('#reason-letter').hide();
                    $('#terminate-letter').hide();                    
                    $('#others').hide();
                }
            });
        });
    </script>


@endpush
@section('content')
@include('contract.post.termination.partials.scripts')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="terminate-tab-content" role="tablist">
            <li class="list-group-item" id="details">
                <a class="list-group-item-action active" data-toggle="tab" href="#contract-details" role="tab" aria-controls="contract-details" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Maklumat Kontrak') }}
                </a>
            </li>
            <li class="list-group-item" id="warning">
                <a class="list-group-item-action" data-toggle="tab" href="#warning-letter" role="tab" aria-controls="warning-letter" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Surat Amaran') }}
                </a>
            </li>
            <li class="list-group-item" id="reason-letter">
                <a class="list-group-item-action" data-toggle="tab" href="#reason" role="tab" aria-controls="reason" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Notis Tujuan Penamatan') }}
                </a>
            </li> 
            @if(!empty($review_reason))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews-reason" role="tab" aria-controls="document-reviews-reason" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan Tujuan Penamatan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews-reason" role="tab" aria-controls="document-log-reviews-reason" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan Tujuan Penamatan') }}
                        </a>
                    </li>
                @endif
            @endif    
            <li class="list-group-item" id="terminate-letter">
                <a class="list-group-item-action" data-toggle="tab" href="#terminate" role="tab" aria-controls="terminate" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Notis Penamatan') }}
                </a>
            </li>  
            @if(!empty($review_notice))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews-notice" role="tab" aria-controls="document-reviews-notice" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan Notis Penamatan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews-notice" role="tab" aria-controls="document-log-reviews-notice" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan Notis Penamatan') }}
                        </a>
                    </li>
                @endif
            @endif   
            <li class="list-group-item" id="others">
                <a class="list-group-item-action" data-toggle="tab" href="#others-details" role="tab" aria-controls="others-details" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Lain-lain') }}
                </a>
            </li>                                
        </ul>
    </div>
    <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'terminate'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'contract-details', 'active' => true])
	                            @slot('content')
	                                @include('contract.post.termination.partials.forms.contract-details', ['sst' => $sst])
	                            @endslot
	                        @endcomponent
	                        @component('components.tab.content', ['id' => 'warning-letter'])
	                            @slot('content')
                                    @include('contract.post.termination.partials.forms.warning-letter', ['warning' => $warning])
                                    {{--  @include('contract.post.termination.warning.index', ['hashslug' => $sst->hashslug])  --}}
	                            @endslot
	                        @endcomponent  
	                        @component('components.tab.content', ['id' => 'reason'])
	                            @slot('content')
	                                @include('contract.post.termination.partials.forms.reason', ['warning' => $warning])
	                            @endslot
	                        @endcomponent 
                            @component('components.tab.content', ['id' => 'document-reviews-reason'])
                                @slot('content')

                                    <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                                        @if((!empty($review_reason)))

                                            {{-- penyedia --}}
                                            @if(!empty($semakan3log_reason))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3reason" aria-expanded="false" aria-controls="s3reason">
                                                                Ulasan Semakan Oleh {{$semakan3log_reason->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3reason" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2_reason as $s2log_reason)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log_reason->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log_reason->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log_reason->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log_reason->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log_reason))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2reason" aria-expanded="false" aria-controls="s2reason">
                                                                Ulasan Semakan Oleh {{$semakan2log_reason->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s2reason" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1_reason as $s1log_reason)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log_reason->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log_reason->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log_reason->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log_reason->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        @endif
                                    
                                    </div>

                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'document-log-reviews-reason'])
                                @slot('content') 
                                    
                                    {{-- log semakan --}}
                                    @role('administrator')
                                        @if(!empty($reasonNotice))
                                            <div class="tab-pane fade show " id="document-contract-review-log" role="tabpanel" aria-labelledby="document-contract-review-log-tab">
                                                <form id="log-form">
                                                    <div class="row justify-content-center w-100">
                                                        <div class="col-12 w-100">
                                                            @include('contract.pre.box.partials.scripts')
                                                            @component('components.card')
                                                                @slot('card_body')
                                                                    @component('components.datatable', 
                                                                        [
                                                                            'table_id' => 'contract-pre-box',
                                                                            'route_name' => 'api.datatable.contract.review-log-reason',
                                                                            'param' => 'acquisition_id=' . $reasonNotice->sst->acquisition_id,
                                                                            'columns' => [
                                                                                ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                                ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                                ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                                                ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                                ['data' => 'jenis', 'title' => __('Jenis'), 'defaultContent' => '-'],
                                                                                ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                                ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                                ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                            ],
                                                                            'headers' => [
                                                                                __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Jenis'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'),  __('Status'), __('')
                                                                            ],
                                                                            'actions' => minify('')
                                                                        ]
                                                                    )
                                                                    @endcomponent
                                                                @endslot
                                                            @endcomponent
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                    @endrole
                                @endslot
                            @endcomponent
	                        @component('components.tab.content', ['id' => 'terminate'])
	                            @slot('content')
	                                @include('contract.post.termination.partials.forms.termination', ['warning' => $warning])
	                            @endslot
	                        @endcomponent  
                            @component('components.tab.content', ['id' => 'document-reviews-notice'])
                                @slot('content')

                                    <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                                        @if((!empty($review_notice)))

                                            {{-- penyedia --}}
                                            @if(!empty($semakan3log_notice))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3notice" aria-expanded="false" aria-controls="s3notice">
                                                                Ulasan Semakan Oleh {{$semakan3log_notice->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3notice" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2_notice as $s2log_notice)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log_notice->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log_notice->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log_notice->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log_notice->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log_notice))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2notice" aria-expanded="false" aria-controls="s2notice">
                                                                Ulasan Semakan Oleh {{$semakan2log_notice->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s2notice" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1_notice as $s1log_notice)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log_notice->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log_notice->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log_notice->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log_notice->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        @endif
                                    
                                    </div>

                                @endslot
                            @endcomponent  
                            @component('components.tab.content', ['id' => 'document-log-reviews-notice'])
                                @slot('content') 

                                    {{-- log semakan --}}
                                    @role('administrator')
                                        @if(!empty($terminateNotice))
                                            <div class="tab-pane fade show " id="document-contract-review-log" role="tabpanel" aria-labelledby="document-contract-review-log-tab">
                                                <form id="log-form">
                                                    <div class="row justify-content-center w-100">
                                                        <div class="col-12 w-100">
                                                            @include('contract.pre.box.partials.scripts')
                                                            @component('components.card')
                                                                @slot('card_body')
                                                                    @component('components.datatable', 
                                                                        [
                                                                            'table_id' => 'contract-pre-box',
                                                                            'route_name' => 'api.datatable.contract.review-log-notice',
                                                                            'param' => 'acquisition_id=' . $terminateNotice->sst->acquisition_id,
                                                                            'columns' => [
                                                                                ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                                ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                                ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                                                ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                                ['data' => 'jenis', 'title' => __('Jenis'), 'defaultContent' => '-'],
                                                                                ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                                ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                                ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                            ],
                                                                            'headers' => [
                                                                                __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Jenis'),__('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'),__('Status'), __('')
                                                                            ],
                                                                            'actions' => minify('')
                                                                        ]
                                                                    )
                                                                    @endcomponent
                                                                @endslot
                                                            @endcomponent
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                    @endrole
                                @endslot
                            @endcomponent    
                            @component('components.tab.content', ['id' => 'others-details'])
                                @slot('content')
                                    @include('contract.post.termination.partials.forms.others', ['warning' => $warning])
                                @endslot
                            @endcomponent                                                                               
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
    </div>
</div>
@endsection