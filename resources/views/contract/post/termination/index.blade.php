@extends('layouts.admin')

@push('style')
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.textarea')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.termination.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'contract-post-termination',
                            'route_name' => 'api.datatable.contract.termination',
                            'columns' => [
                                ['data' => 'no_kontrak', 'title' => __('No. Rujukan'), 'defaultContent' => '-'],
                                ['data' => 'tajuk', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                ['data' => 'syarikat_terpilih', 'title' => __('Kontraktor'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Rujukan'), __('Tajuk'),__('Kontraktor'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.termination.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div
@endsection
