<html>
    <head>

        <style type="text/css">
            @page {
                margin: 70px 70px 70px 100px;
            }

            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }        

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
        </style>
    </head>
    <body>
        <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>

        <center>
            <table  style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr>
                    <td width="20%">Rujukan Tuan</td>
                    <td width="5%"> : </td>
                    <td width="75%"></td>
                </tr>
                <tr>
                    <td>Rujukan Kami</td>
                    <td> : </td>
                    <td>{{ $termination->sst->acquisition->reference }}</td>
                </tr>
                <tr>
                    <td>Tarikh</td>
                    <td> : </td>
                    <td>{{ \Carbon::intlFormat($termination->notice_at) }}</td>
                </tr> 
                <tr>
                    <td colspan="3"><br/></td>
                </tr>
            </table>

            <table style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr>
                    <td colspan="2"><span style="text-transform: uppercase; font-weight: bold">{{ $appointed->company->company_name}}, </span></td>
                </tr>
                @if(!empty($appointed->company->addresses[0]))
                    <tr>
                        <td>{{ $appointed->company->addresses[0]->primary }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>{{ $appointed->company->addresses[0]->secondary }}</td>
                        <td></td>
                    </tr>   
                    <tr>
                        <td width="70%">{{ $appointed->company->addresses[0]->postcode }}, {{ $appointed->company->addresses[0]->state }}</td>
                        <td width="30%">No. Tel :  
                            @if(!empty($appointed->company->phones[0]))
                                {{ $appointed->company->phones[0]->phone_number }}
                            @endif
                        </td>
                    </tr>                                      
                @endif
                <tr>
                    <td colspan="2"><br/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span style="text-transform: uppercase; font-weight: bold;">Notis untuk tujuan penamatan pengambilan kerja kontraktor </span>
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br/></td>
                </tr>                
            </table>
            <table style="width: 100%;border-collapse: collapse;padding: 4 4;" align="center" class="content">
                <tr>
                    <td width="30%" valign="top">Nombor Kontrak</td>
                    <td width="3%" valign="top"> : </td>
                    <td width="67%" valign="top">{{ $termination->sst->contract_no }}</td>
                </tr>
                <tr>
                    <td valign="top">Tajuk Kontrak</td>
                    <td valign="top"> : </td>
                    <td valign="top" style="text-align: justify; text-transform: uppercase;">{{ $termination->sst->acquisition->title }}</td>
                </tr>
                <tr>
                    <td>Jumlah Harga Kontrak</td>
                    <td> : </td>
                    <td>{!! money()->toHuman($appointed->offered_price) !!}</td>
                </tr>  
                <tr>
                    <td>Tarikh Milik Tapak Bina</td>
                    <td> : </td>{{ isset($termination->sst->start_working_date) ? \Carbon::intlFormat($termination->sst->start_working_date)  : "-" }}
                    <td>{{ isset($termination->sst->start_working_date) ? \Carbon::intlFormat($termination->sst->start_working_date)  : "-" }}</td>
                </tr> 
                <tr>
                    <td>Tarikh Siap Kerja</td>
                    <td> : </td>
                    <td>{{ isset($termination->sst->end_working_date) ? \Carbon::intlFormat($termination->sst->end_working_date)  : "-" }}</td>
                </tr>  
                <tr><td colspan="3"><br /><br /></td></tr>
                <tr>
                    <td colspan="3"  style="text-align: justify">
                        Dukacita dimaklumkan bahawa tuan didapati tidak menjalankan kerja dengan bersungguh-sungguh dan kerja 
                        tuan dijangka tidak dapat disiapkan pada Tarikh Siap Kerja yang telah ditentukan pada 
                        <span style="font-weight: bold;">{{ isset($termination->sst->end_working_date) ? \Carbon::intlFormat($termination->sst->end_working_date)  : "-" }}</span>.
                        Kerja tuan sepatutnya sudah <span style="font-weight: bold;">{{ isset($termination->scheduled_completion) ? $termination->scheduled_completion : "0" }} %</span> 
                        siap tetapi kemajuan yang dicapai setakat ini adalah hanya 
                        <span style="font-weight: bold;">{{ isset($termination->actual_completion) ? $termination->actual_completion : "0" }} </span>.
                                    
                        Kegagalan tuan itu masih berulang sungguhpun tuan telah diberi amaran di Mesyuarat Tapak No. 
                        <span style="font-weight: bold;">  </span> pada 
                        <span style="font-weight: bold;"></span>.
                    </td>
                </tr>  
                <tr><td colspan="3"><br /></td></tr>
                <tr>
                    <td colspan="3" style="text-align: justify">
                        2.  Perlakuan tuan sedemikian sangatlah mendukacitakan.  Perlakuan tuan bukan sahaja akan melewatkan penyiapan kerja ini tetapi juga akan
                        mengakibatkan kesusahan kepada orang ramai.
                    </td>
                </tr> 
                <tr><td colspan="3"><br /></td></tr>
                <tr>
                    <td colspan="3" style="text-align: justify">
                        3.  Tuan adalah dikehendaki memulakan kerja tuan semula dan menjalankan kerja tuan dengan lebih cergas lagi.  Sekiranya tuan gagal mematuhi 
                        kehendak perenggan ini dalam tempoh 14 hari selepas penerimaan notis ini atau di dalam tempoh yang munasabah,
                        pengambilan kerja tuan di bawah kontrak ini akan ditamatkan sejajar dengan <span style="font-weight: bold;">{{ isset($termination->clause) ? $termination->clause : "0" }} </span>
                        syarat-syarat kontrak.
                    </td>
                </tr>  
                <tr><td colspan="3"><br /></td></tr>
                <tr>
                    <td colspan="3" style="text-align: justify">
                        4.  Tuan adalah diingatkan iaitu sekiranya pengambilan kerja tuan ditamatkan, kerja akan disiapkan sejajar dengan 
                        <span style="font-weight: bold;">{{ isset($termination->sub_clause) ? $termination->sub_clause : "0" }} </span> syarat-syarat kontrak dan segala 
                        perbelanjaan berlebihan yang akan timbul hendaklah ditanggung sepenuhnya oleh tuan.
                    </td>
                </tr> 
                <tr><td colspan="3"><br /></td></tr> 
                <tr>
                    <td colspan="3">Sekian terima kasih<br></td>
                </tr>
                <tr><td colspan="3"><br /><br /></td></tr>  
                <tr>
                    <td colspan="3">Saya yang menurut perintah,</td>
                </tr>
                <tr><td colspan="3"><br /><br/><br/><br/></td></tr>
                <tr>
                    <td colspan="3">
                        ....................................................
                        <br/>

                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-transform: uppercase;" class="font-weight-bold"> {{(!empty($np_dept)) ? '('.$np_dept->name.')': '' }} </td>
                </tr>
                <tr>
                    <td colspan="3">{{(!empty($np_dept)) ? $np_dept->position->name : '' }} Jabatan Pelaksana</td>
                </tr>    
                <tr><td colspan="3"><br /><br/><br/><br/></td></tr>    
                <tr>
                    <td colspan="2"><b>Sk:</b></td>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <td colspan="3">{{(!empty($np_dept)) ? 'Jabatan ' . $np_dept->department->name : '' }}</td>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                <tr>
                    <br><td colspan="3">Jabatan Kewangan</td></br>
                </tr>        
            </table>
        </center>
    </body>
</html>