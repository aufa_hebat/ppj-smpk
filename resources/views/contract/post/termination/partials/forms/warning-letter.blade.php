@if(user()->current_role_login == 'penyedia')   
    @if(($warning->count() < 2 && $sst->termination_type_id == 2) || 
        ($warning->count() < 1 && $sst->termination_type_id == 1 && $sst->acquisition->approval->acquisition_type_id == 1) || 
        ($sst->termination_type_id == 1 && $sst->acquisition->approval->acquisition_type_id == 2))
		<a href="{{ route('termination.warning.create', ['hashslug' => $sst->hashslug]) }}" class="btn btn-primary float-right">
			@icon('fe fe-plus') {{ __('Surat Amaran Baru') }}
		</a>
    @endif
@endif

<h5>Senarai Surat Amaran Yang Telah Dihantar</h5>
@if($sst->termination_type_id == 1)

<table class="table">
    <tr>
        <th style="width: 5%;">Bil. </th>
        <th style="width: 10%;">Tarikh Surat Amaran </th>
        <th style="width: 5%;">BIl. Mesyuarat Tapak </th>
        <th style="width: 10%;">Tarikh Mesyuarat Tapak. </th>
        <th style="width: 10%;">% Siap Kerja (sepatutnya) </th>
        <th style="width: 10%;">% Kemajuan Kerja </th>
        <th style="width: 10%;">Klausa</th>
        <th style="width: 10%;">Sub Klausa </th>
        <th style="width: 10%;">Tindakan </th>
    </tr>
    @foreach($warning as $index => $warn)
        <tr>
            <td>{{ $warn->warning_letter_no }}</td>
            <td>
                {{ isset($warn->warning_letter_at) ? \Carbon\Carbon::CreateFromFormat('Y-m-d', $warn->warning_letter_at)->format('d/m/Y') : "-" }}
            </td>
            <td>{{ $warn->site_meeting_no }}</td>
            <td>
                {{ isset($warn->site_meeting_at) ? \Carbon\Carbon::CreateFromFormat('Y-m-d', $warn->site_meeting_at)->format('d/m/Y') : "-" }}
            </td>                                            
            <td>{{ $warn->scheduled_completion }}</td>
            <td>{{ $warn->actual_completion }}</td>
            <td>{{ $warn->clause }}</td>
            <td>{{ $warn->sub_clause }}</td>
            <td>
                <div class="text-center">
                    <div class="item-action dropdown">
                        <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                            <i class="fe fe-more-vertical"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                            style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
                            {{ $prepend_action or '' }}
                            <a class="dropdown-item warn-show-action-btn" style="cursor: pointer;"
                                data-{{ $primary_key or 'hashslug' }}="{{ $warn->hashslug }}">
                                <i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
                            </a>
                            @if(user()->current_role_login == 'penyedia' && $warn->status == 1)
                                <a class="dropdown-item warn-edit-action-btn" style="cursor: pointer;"
                                    data-{{ $primary_key or 'hashslug' }}="{{ $warn->hashslug }}">
                                    <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                </a>

                                {{ $append_action or '' }}
                                <div class="dropdown-divider " ></div>	
                                {{ $prepend_footer_action or '' }}
                                <a class="dropdown-item warn-destroy-action-btn"  style="cursor: pointer;"
                                    data-{{ $primary_key or 'hashslug' }}="{{ $warn->hashslug }}">
                                    <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
                                </a>		
                                {{ $append_footer_action or '' }} 

                            @endif
                                                       
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
</table>

@else

    <table class="table">
        <tr>
            <th style="width: 10%;">Bil. Surat Amaran </th>
            <th style="width: 10%;">Tarikh Dikemaskini </th>
            <th style="width: 10%;">Tindakan </th>
        </tr>

            @foreach($warning as $index2 => $warn)
                <tr>
                    <td>{{ $warn->warning_letter_no }}</td>
                    <td>
                        {{ $warn->updated_at->format('d/m/Y ') }}
                    </td>                
                    <td>
                        <div class="text-center">
                            <div class="item-action dropdown">
                                <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                                    <i class="fe fe-more-vertical"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                                    style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    {{ $prepend_action or '' }}
                                    <a class="dropdown-item warn-show-action-btn" style="cursor: pointer;"
                                        data-{{ $primary_key or 'hashslug' }}="{{ $warn->hashslug }}">
                                        <i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
                                    </a>
                                    @if(user()->current_role_login == 'penyedia' && $warn->status == 1)
                                        <a class="dropdown-item warn-edit-action-btn" style="cursor: pointer;"
                                            data-{{ $primary_key or 'hashslug' }}="{{ $warn->hashslug }}">
                                            <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                        </a>

                                        {{ $append_action or '' }}
                                        <div class="dropdown-divider " ></div>	
                                        {{ $prepend_footer_action or '' }}
                                        <a class="dropdown-item warn-destroy-action-btn"  style="cursor: pointer;"
                                            data-{{ $primary_key or 'hashslug' }}="{{ $warn->hashslug }}">
                                            <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
                                        </a>		
                                        {{ $append_footer_action or '' }}  

                                    @endif
                          
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
    </table>
@endif

