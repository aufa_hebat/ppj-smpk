@push('scripts')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            $('#uploadFileTermination').change(function(){
                $('#subFileTermination').val($(this).val().split('\\').pop());
            });

            @if(!empty($terminateNotice) && !empty($terminateNotice->documents))
                @foreach($terminateNotice->documents as $document)
                    $('#subFileTermination').val('{{ $document->document_name }}');
                @endforeach
            @endif

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $terminateNotice->hashslug }}';
                var route_name = 'termination.upload-termination';
                var form_id = 'terminate_letter_form';
                var form = document.forms[form_id];
                var data = new FormData(form);

                // Display the key/value pairs
                for (var pair of data.entries()) {
                    console.log(pair[0]+ ', ' + pair[1]); 
                }

                axios.post(route(route_name, id), data).then(response => {
                    swal('', response.data.message, 'success');
                    location.reload();
                });
            });
        });
    </script>
@endpush

<h4>Muat Naik Notis Penamatan</h4>
<div class="row">
    <div class="col-12">
        <form id="terminate_letter_form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            
            <div class="form-group row">
                <label for="Fail_Keputusan_Mesyuarat"
                   class="col col-form-label">
                    Muat Naik Notis Penamatan
                </label>

                <div class="col input-group">
                    <input id="subFileTermination" type="text"  class="form-control" readonly>
                    <label class="input-group-text" for="uploadFileTermination"><i class="fe fe-upload" ></i></label>
                    <input type="file" class="form-control" id="uploadFileTermination" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                    @if(!empty($terminateNotice) && !empty($terminateNotice->documents))
                        @foreach($terminateNotice->documents as $document)
                            <label class="input-group-text" for="downloadFile">
                                <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                            </label>
                        @endforeach
				    @endif
                </div>
                
            </div>

            <div class="row">
                <div class="col-12" style="text-align:right;">
                    <a href="{{ route('termination.termination.edit', ['hashslug'=>$terminateNotice->sst->hashslug]) }}" 
                            class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                    </a>                    
                    <button type="submit" class="btn btn-primary submit-action-btn">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>