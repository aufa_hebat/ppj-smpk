@push('scripts')
<script>
    jQuery(document).ready(function($) {
        $('#uploadFileNotis').change(function(){
            $('#subFileNotis').val($(this).val().split('\\').pop());
        });

        @if(!empty($terminateNotice) && !empty($terminateNotice->documents))
            @foreach($terminateNotice->documents as $document)
                $('#subFileNotis').val('{{ $document->document_name }}');
            @endforeach
        @endif
    });
</script>
@endpush

<h5>Notis Penamatan Kontrak</h5>
@if(!empty($terminateNotice) && $sst->termination_type_id == 1)
<div class="row">
    <div class="col">
        <div class="btn-group float-right">
            <a href="{{ route('terminate_notice',['hashslug' => $terminateNotice->hashslug]) }}" 
                target="_blank" 
                class="btn btn-success border-success">
                @icon('fe fe-printer') {{ __(' Cetak') }}
            </a>
        </div>
    </div>
</div>
@endif

<form method="POST" action="{{ route('termination.noticeTerminate') }}" files = "true" enctype="multipart/form-data">
    @csrf
        @include('components.forms.hidden', [
			    'name' => 'sst_hashslug',
                'id' => 'sst_hashslug',
                'value' =>  $sst->hashslug 
            ]) 
            
    @if($sst->termination_type_id == 1)

        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'No Rujukan Fail',
                    'name' => 'terminate_file_ref_no',
                    'id' => 'terminate_file_ref_no',
                    'readonly' => true
                ]) 
            </div>
            <div class="col-6"></div>
        </div>
        <div class="row">
            <div class="col-6">
                @if(!empty($terminateNotice))
                    @if(user()->id == $terminateNotice->sst->user_id)
                        @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Notis'),
                            'id' => 'terminate_notice_at',
                            'name' => 'terminate_notice_at',
                            'config' => ['format' => config('datetime.display.date')],
                        ])
                    @endif
                @else
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Notis'),
                        'id' => 'terminate_notice_at',
                        'name' => 'terminate_notice_at',
                        'config' => ['format' => config('datetime.display.date')],
                        {{-- 'readonly' => true --}}
                    ])
                @endif
            </div>   
            <div class="col-6">
                @if(!empty($terminateNotice))
                    @if(user()->id == $terminateNotice->sst->user_id)
                        @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Penamatan Kontrak'),
                            'id' => 'termination_at',
                            'name' => 'termination_at',
                            'config' => ['format' => config('datetime.display.date')],
                        ])
                    @endif
                @else
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Penamatan Kontrak'),
                        'id' => 'termination_at',
                        'name' => 'termination_at',
                        'config' => ['format' => config('datetime.display.date')],
                        {{-- 'readonly' => true --}}
                    ])
                @endif
            </div>            
        </div>
        <div class="row">
            <div class="col-6">
                @if(!empty($terminateNotice))
                    @if(user()->id == $terminateNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => '% Siap Kerja (sepatutnya)',
                            'name' => 'terminate_scheduled_completion',
                            'id' => 'terminate_scheduled_completion',
                        ])   
                    @endif
                @else
                    @include('components.forms.input', [
                        'input_label' => '% Siap Kerja (sepatutnya)',
                        'name' => 'terminate_scheduled_completion',
                        'id' => 'terminate_scheduled_completion',
                        {{-- 'readonly' => true --}}
                    ]) 
                @endif    
            </div>
            <div class="col-6">
                @if(!empty($terminateNotice))
                    @if(user()->id == $terminateNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => '% Kemajuan Kerja Siap',
                            'name' => 'terminate_actual_completion',
                            'id' => 'terminate_actual_completion',
                        ])   
                    @endif 
                @else
                    @include('components.forms.input', [
                        'input_label' => '% Kemajuan Kerja Siap',
                        'name' => 'terminate_actual_completion',
                        'id' => 'terminate_actual_completion',
                        {{-- 'readonly' => true --}}
                    ]) 
                @endif      
            </div>        
        </div>
        <div class="row">
            <div class="col-6">
                @if(!empty($terminateNotice))            
                    @if(user()->id == $terminateNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => 'Klausa',
                            'name' => 'terminate_clause',
                            'id' => 'terminate_clause',
                        ]) 
                    @endif
                @else
                    @include('components.forms.input', [
                        'input_label' => 'Klausa',
                        'name' => 'terminate_clause',
                        'id' => 'terminate_clause',
                        {{-- 'readonly' => true --}}
                    ])
                @endif   
            </div>
            <div class="col-6">
                @if(!empty($terminateNotice))            
                    @if(user()->id == $terminateNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => 'Sub Klausa',
                            'name' => 'terminate_sub_clause',
                            'id' => 'terminate_sub_clause',
                        ]) 
                    @endif
                @else
                    @include('components.forms.input', [
                        'input_label' => 'Sub Klausa',
                        'name' => 'terminate_sub_clause',
                        'id' => 'terminate_sub_clause',
                        {{-- 'readonly' => true --}}
                    ]) 
                @endif   
            </div>        
        </div>
    @elseif($sst->termination_type_id == 2 || $sst->termination_type_id == 3 || $sst->termination_type_id == 4)

        <div class="row">
            <div class="col-4">
                @include('components.forms.datetimepicker', [
                    'input_label' => __('Tarikh Penamatan Kontrak'),
                    'id' => 'termination_at',
                    'name' => 'termination_at',
                    'config' => ['format' => config('datetime.display.date')],
                ])
            </div>
            <div class="col-8">
                <div class="form-group row">
                    <label for="Fail_Keputusan_Mesyuarat"
                        class="col col-form-label">
                        Muat Naik Surat Penamatan
                    </label>
                    
                    <div class="col input-group">
                        <input id="subFileNotis" type="text"  class="form-control" readonly>
                        <label class="input-group-text" for="uploadFileNotis"><i class="fe fe-upload" ></i></label>
                        <input type="file" class="form-control" id="uploadFileNotis" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                        @if(!empty($terminateNotice) && !empty($terminateNotice->documents))
                            @foreach($terminateNotice->documents as $document)
                                <label class="input-group-text" for="downloadFile">
                                    <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                </label>
                            @endforeach
                        @endif
                    </div>                        
                </div>
            </div>
        </div>


    @endif

    <div class="btn-group float-right">
        @if(empty($terminateNotice) || $terminateNotice->status != 3)
            <a href="{{ route('contract.post.index') }}" 
                class="btn btn-default border-primary">
                {{ __('Kembali') }}
            </a>
            @if(empty($terminateNotice) || (!empty($terminateNotice) && ($terminateNotice->status != 3)))
                <input type="submit" class="btn btn-primary save-action-btn" name="btnSubmit" id="btnSubmit" value="Simpan" />
                @if($sst->termination_type_id == 2 || $sst->termination_type_id == 3 || $sst->termination_type_id == 4)
                    <input type="submit" class="btn btn-success submit-action-btn" name="btnSubmit" id="btnSubmit" value="Selesai" />
                @endif
            @endif
        @endif
</form>
    @role('penyedia')
        @if(!empty($terminateNotice) && $terminateNotice->sst->user_id == user()->id && $terminateNotice->sst->termination_type_id == 1 && $terminateNotice->status != 3)
            @if(empty($review_notice) || empty($review_notice->status))
                <form method="POST" action="{{ route('acquisition.review.store') }}">
                    @csrf
                    <input id="sentStatus_notice1" name="sentStatus" type="hidden" value="1" />
                    <input type="text" id="acquisition_id_notice1" name="acquisition_id" value="@if(!empty($terminateNotice)){{ $terminateNotice->sst->acquisition_id }}@endif" hidden>

                    <input type="text" id="requested_by_notice1" name="requested_by" value="{{ user()->id }}" hidden>

                    <input type="text" id="approved_by_notice1" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                    <input type="text" id="created_by_notice1" name="created_by" value="{{user()->id}}" hidden>

                    <input type="text" id="approved_at_notice1" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                    @if(!empty($review_previous_notice))
                        <input type="hidden" id="task_by_notice1" name="task_by" value="{{$review_previous_notice->task_by}}">
                        <input type="hidden" id="law_task_by_notice1" name="law_task_by" value="{{$review_previous_notice->law_task_by}}">
                        <input type="text" id="type_notice1" name="type" value="Penamatan" hidden>
                        <input type="text" id="document_contract_type_notice1" name="document_contract_type" value="Notis" hidden>
                    @endif
                    
                    <button type="button" id="send_notice" class="btn btn-default float-middle border-default">
                        @icon('fe fe-send') {{ __('Teratur') }}
                    </button>
                </form>
            @endif
        @endif
    @endrole
    </div>

    <br>
    @if(!empty($terminateNotice) && $sst->termination_type_id == 1)
        @if($terminateNotice->status == 3)
            @include('contract.post.termination.partials.forms.upload-termination')
        @endif
    @endif

    @if(!empty($review_notice))

        {{-- semakan --}}
        @if(user()->id == $review_notice->approved_by && user()->id != $review_notice->created_by && $review_notice->type == 'Penamatan' && $review_notice->document_contract_type == 'Notis')
            <div class="card row col-12"><br>
                <h3>Ulasan Pegawai</h3>
                <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                    @csrf

                    @include('components.forms.hidden', [
                        'id' => 'id',
                        'name' => 'id',
                        'value' => ''
                    ])

                    @if(!empty($terminateNotice))
                        @include('components.forms.hidden', [
                            'id' => 'acquisition_id_notice',
                            'name' => 'acquisition_id',
                            'value' => $terminateNotice->sst->acquisition_id
                        ])
                    @endif

                    @if(!empty($review_notice))
                    <input type="hidden" id="task_by_notice" name="task_by" value="{{$review_notice->task_by}}">
                    <input type="hidden" id="law_task_by_notice" name="law_task_by" value="{{$review_notice->law_task_by}}">

                    <input type="hidden" id="created_by_notice" name="created_by" value="{{$review_notice->created_by}}">
                    @endif

                    @if(!empty($review_previous_notice))
                        <input type="text" id="created_by_notice" name="created_by" value="{{$review_previous_notice->created_by}}" hidden>
                        <input type="text" id="approved_at_notice" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                        <input type="text" id="task_by_notice" name="task_by" value="{{$review_previous_notice->task_by}}" hidden>
                        <input type="text" id="law_task_by_notice" name="law_task_by" value="{{$review_previous_notice->law_task_by}}" hidden>
                        <input type="text" id="type_notice" name="type" value="Penamatan" hidden>
                        <input type="text" id="document_contract_type_notice" name="document_contract_type" value="Notis" hidden>
                    @endif

                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.input', [
                                'input_label' => __('Nama Pegawai Semakan'),
                                'id' => '',
                                'name' => '',
                                'value' => user()->name,
                                'readonly' => true
                            ])
                        </div>
                            @include('components.forms.hidden', [
                                'id' => 'requested_by_notice',
                                'name' => 'requested_by',
                                'value' => user()->id
                            ])
                            @include('components.forms.hidden', [
                                'id' => 'approved_by_notice',
                                'name' => 'approved_by',
                                'value' => user()->supervisor->id
                            ])
                        <div class="col-3">
                            @if(!empty($review_notice))
                                @if(!empty($review_notice->status))
                                    @include('components.forms.input', [
                                        'input_label' => __('Tarikh Terima'),
                                        'id' => 'requested_at_notice',
                                        'name' => 'requested_at',
                                    ])
                                @endif
                            @endif
                        </div>
                        <div class="col-3">
                            @if(!empty($review_notice))
                                @if(!empty($review_notice->status))
                                    @include('components.forms.input', [
                                        'input_label' => __('Tarikh Hantar'),
                                        'id' => 'approved_ats_notice',
                                        'name' => 'approved_at',
                                    ])
                                @endif
                            @endif
                        </div>
                    </div>

                    @if(!empty($review_notice) && $review_notice->created_by != user()->id)
                        @include('components.forms.textarea', [
                            'input_label' => __('Ulasan'),
                            'id' => 'remarks_notice',
                            'name' => 'remarks'
                        ])
                    @else

                    @endif
                    
                    <input id="rejectStatus_notice" name="rejectStatus" type="hidden" value="1" />
                    <input id="saveStatus_notice" name="saveStatus" type="hidden" value="1" />
                    <input id="sentStatus_notice" name="sentStatus" type="hidden" value="1" />

                    <div class="btn-group float-right">
                        @if(!empty($review_notice) && $review_notice->created_by != user()->id)
                            <button type="button" id="rejSent_notice" class="btn btn-danger btn-default border-primary ">
                                @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                            </button>
                            <button type="button" id="savDraf_notice" class="btn btn-primary btn-group float-right ">
                                @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                            </button>
                        @endif
                        <button type="button" id="savSent_notice" class="btn btn-default float-middle border-default ">
                            @icon('fe fe-send') {{ __('Teratur') }}
                        </button>
                    </div>

                </form>
            </div>
        @endif
    @endif
