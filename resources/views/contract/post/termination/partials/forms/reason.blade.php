@push('scripts')
<script>
    jQuery(document).ready(function($) {
        $('#uploadFileReason').change(function(){
            $('#subFileReason').val($(this).val().split('\\').pop());
        });

        @if(!empty($reasonNotice) && !empty($reasonNotice->documents))
            @foreach($reasonNotice->documents as $document)
                $('#subFileReason').val('{{ $document->document_name }}');
            @endforeach
        @endif

    });
</script>
@endpush

<h5>Notis Tujuan Penamatan Kontrak</h5>
@if(!empty($reasonNotice) && $sst->termination_type_id == 1)
<div class="row">
    <div class="col">
        <div class="btn-group float-right">
            <a href="{{ route('terminate_reason',['hashslug' => $reasonNotice->hashslug]) }}" 
                target="_blank" 
                class="btn btn-success border-success">
                @icon('fe fe-printer') {{ __(' Cetak') }}
            </a>
        </div>
    </div>
</div>
@endif

<form method="POST" action="{{ route('termination.reasonTerminate') }}" files = "true" enctype="multipart/form-data">
    @csrf
        @include('components.forms.hidden', [
			    'name' => 'sst_hashslug',
                'id' => 'sst_hashslug',
                'value' =>  $sst->hashslug 
            ]) 
    @if($sst->termination_type_id == 1)
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'No Rujukan Fail',
                    'name' => 'file_ref_no',
                    'id' => 'file_ref_no',
                    'readonly' => true
                ]) 
            </div>
            <div class="col-6">
                @if(!empty($reasonNotice))
                    @if(user()->id == $reasonNotice->sst->user_id)
                        @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Notis'),
                            'id' => 'notice_at',
                            'name' => 'notice_at',
                            'config' => ['format' => config('datetime.display.date')],
                        ])
                    @endif
                @else
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Notis'),
                        'id' => 'notice_at',
                        'name' => 'notice_at',
                        'config' => ['format' => config('datetime.display.date')],
                        {{-- 'readonly' => true --}}
                    ])
                @endif
            </div>     
        </div>
        <div class="row">
            <div class="col-6">
                @if(!empty($reasonNotice))
                    @if(user()->id == $reasonNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => '% Siap Kerja (sepatutnya)',
                            'name' => 'scheduled_completion',
                            'id' => 'scheduled_completion',
                        ])  
                    @endif
                @else
                    @include('components.forms.input', [
                        'input_label' => '% Siap Kerja (sepatutnya)',
                        'name' => 'scheduled_completion',
                        'id' => 'scheduled_completion',
                        {{-- 'readonly' => true --}}
                    ]) 
                @endif     
            </div>
            <div class="col-6">
                @if(!empty($reasonNotice))
                    @if(user()->id == $reasonNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => '% Kemajuan Kerja Siap',
                            'name' => 'actual_completion',
                            'id' => 'actual_completion',
                        ])
                    @endif 
                @else
                    @include('components.forms.input', [
                        'input_label' => '% Kemajuan Kerja Siap',
                        'name' => 'actual_completion',
                        'id' => 'actual_completion',
                        {{-- 'readonly' => true --}}
                    ])  
                @endif    
            </div>        
        </div>
        <div class="row">
            <div class="col-6">
                @if(!empty($reasonNotice))
                    @if(user()->id == $reasonNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => 'Klausa',
                            'name' => 'clause',
                            'id' => 'clause',
                        ]) 
                    @endif
                @else 
                    @include('components.forms.input', [
                        'input_label' => 'Klausa',
                        'name' => 'clause',
                        'id' => 'clause',
                        {{-- 'readonly' => true --}}
                    ]) 
                @endif  
            </div>  
            <div class="col-6">
                @if(!empty($reasonNotice))
                    @if(user()->id == $reasonNotice->sst->user_id)
                        @include('components.forms.input', [
                            'input_label' => 'Sub Klausa',
                            'name' => 'sub_clause',
                            'id' => 'sub_clause',
                        ]) 
                    @endif
            @else
                    @include('components.forms.input', [
                        'input_label' => 'Sub Klausa',
                        'name' => 'sub_clause',
                        'id' => 'sub_clause',
                        {{-- 'readonly' => true --}}
                    ]) 
            @endif 
            </div>        
        </div>

    @elseif($sst->termination_type_id == 3)
        <div class="row">
            <div class="col-4">
                @include('components.forms.datetimepicker', [
                    'input_label' => __('Tarikh Notis'),
                    'id' => 'notice_at',
                    'name' => 'notice_at',
                    'config' => ['format' => config('datetime.display.date')],
                ])
            </div>
            <div class="col-8">
                <div class="form-group row">
                    <label for="Fail_Keputusan_Mesyuarat"
                        class="col col-form-label">
                        Muat Naik Surat Penamatan
                    </label>
                    
                    <div class="col input-group">
                        <input id="subFileReason" type="text"  class="form-control" readonly>
                        <label class="input-group-text" for="uploadFileReason"><i class="fe fe-upload" ></i></label>
                        <input type="file" class="form-control" id="uploadFileReason" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                        @if(!empty($reasonNotice) && !empty($reasonNotice->documents))
                            @foreach($reasonNotice->documents as $document)
                                <label class="input-group-text" for="downloadFile">
                                    <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                </label>
                            @endforeach
                        @endif
                    </div>                        
                </div>
            </div>
        </div>
    @endif
        
    <div class="btn-group float-right">
        @if(empty($reasonNotice) ||$reasonNotice->status != 3)
            <a href="{{ route('contract.post.index') }}" 
                class="btn btn-default border-primary">
                {{ __('Kembali') }}
            </a>

            @if(empty($reasonNotice) || (!empty($reasonNotice) && ($reasonNotice->status != 3)))
                <input type="submit" class="btn btn-primary save-action-btn" name="btnSubmit" id="btnSubmit" value="Simpan" />
                @if($sst->termination_type_id == 2 || $sst->termination_type_id == 3 || $sst->termination_type_id == 4)
                    <input type="submit" class="btn btn-success submit-action-btn" name="btnSubmit" id="btnSubmit" value="Selesai" />
                @endif
            @endif
        @endif
    
</form>
<br/>
    @role('penyedia')
        @if(!empty($reasonNotice) && $reasonNotice->sst->user_id == user()->id && $reasonNotice->sst->termination_type_id == 1 && $reasonNotice->status != 3)
            @if(empty($review_reason) || empty($review_reason->status))
                <form method="POST" action="{{ route('acquisition.review.store') }}">
                    @csrf
                    <input id="sentStatus_reason1" name="sentStatus" type="hidden" value="1" />
                    <input type="text" id="acquisition_id_reason1" name="acquisition_id" value="@if(!empty($reasonNotice)){{ $reasonNotice->sst->acquisition_id }}@endif" hidden>

                    <input type="text" id="requested_by_reason1" name="requested_by" value="{{ user()->id }}" hidden>

                    <input type="text" id="approved_by_reason1" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                    <input type="text" id="created_by_reason1" name="created_by" value="{{user()->id}}" hidden>

                    <input type="text" id="approved_at_reason1" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                    @if(!empty($review_previous_reason))
                        <input type="hidden" id="task_by_reason1" name="task_by" value="{{$review_previous_reason->task_by}}">
                        <input type="hidden" id="law_task_by_reason1" name="law_task_by" value="{{$review_previous_reason->law_task_by}}">
                        <input type="text" id="type_reason1" name="type" value="Penamatan" hidden>
                        <input type="text" id="document_contract_type_reason1" name="document_contract_type" value="Tujuan" hidden>
                    @endif
                    
                    <button type="button" id="send_reason" class="btn btn-default float-middle border-default">
                        @icon('fe fe-send') {{ __('Teratur') }}
                    </button>
                </form>
            @endif
        @endif
    @endrole
    </div>

<br>
@if(!empty($reasonNotice) && $sst->termination_type_id == 1)
    @if($reasonNotice->status == 3)
        @include('contract.post.termination.partials.forms.upload-reason')
    @endif
@endif


    @if(!empty($review_reason) && $sst->termination_type_id == 1)

        {{-- semakan --}}
        @if(user()->id == $review_reason->approved_by && user()->id != $review_reason->created_by && $review_reason->type == 'Penamatan' && $review_reason->document_contract_type == 'Tujuan')
            <div class="card row col-12"><br>
                <h3>Ulasan Pegawai</h3>
                <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                    @csrf

                    @include('components.forms.hidden', [
                        'id' => 'id',
                        'name' => 'id',
                        'value' => ''
                    ])

                    @if(!empty($reasonNotice))
                        @include('components.forms.hidden', [
                            'id' => 'acquisition_id_reason',
                            'name' => 'acquisition_id',
                            'value' => $reasonNotice->sst->acquisition_id
                        ])
                    @endif

                    @if(!empty($review_reason))
                    <input type="hidden" id="task_by_reason" name="task_by" value="{{$review_reason->task_by}}">
                    <input type="hidden" id="law_task_by_reason" name="law_task_by" value="{{$review_reason->law_task_by}}">

                    <input type="hidden" id="created_by_reason" name="created_by" value="{{$review_reason->created_by}}">
                    @endif

                    @if(!empty($review_previous_reason))
                        <input type="text" id="created_by_reason" name="created_by" value="{{$review_previous_reason->created_by}}" hidden>
                        <input type="text" id="approved_at_reason" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                        <input type="text" id="task_by_reason" name="task_by" value="{{$review_previous_reason->task_by}}" hidden>
                        <input type="text" id="law_task_by_reason" name="law_task_by" value="{{$review_previous_reason->law_task_by}}" hidden>
                        <input type="text" id="type_reason" name="type" value="Penamatan" hidden>
                        <input type="text" id="document_contract_type_reason" name="document_contract_type" value="Tujuan" hidden>
                    @endif

                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.input', [
                                'input_label' => __('Nama Pegawai Semakan'),
                                'id' => '',
                                'name' => '',
                                'value' => user()->name,
                                'readonly' => true
                            ])
                        </div>
                            @include('components.forms.hidden', [
                                'id' => 'requested_by_reason',
                                'name' => 'requested_by',
                                'value' => user()->id
                            ])
                            @include('components.forms.hidden', [
                                'id' => 'approved_by_reason',
                                'name' => 'approved_by',
                                'value' => user()->supervisor->id
                            ])
                        <div class="col-3">
                            @if(!empty($review_reason))
                                @if(!empty($review_reason->status))
                                    @include('components.forms.input', [
                                        'input_label' => __('Tarikh Terima'),
                                        'id' => 'requested_at_reason',
                                        'name' => 'requested_at',
                                    ])
                                @endif
                            @endif
                        </div>
                        <div class="col-3">
                            @if(!empty($review_reason))
                                @if(!empty($review_reason->status))
                                    @include('components.forms.input', [
                                        'input_label' => __('Tarikh Hantar'),
                                        'id' => 'approved_ats_reason',
                                        'name' => 'approved_at',
                                    ])
                                @endif
                            @endif
                        </div>
                    </div>

                    @if(!empty($review_reason) && $review_reason->created_by != user()->id)
                        @include('components.forms.textarea', [
                            'input_label' => __('Ulasan'),
                            'id' => 'remarks_reason',
                            'name' => 'remarks'
                        ])
                    @else

                    @endif
                    
                    <input id="rejectStatus_reason" name="rejectStatus" type="hidden" value="1" />
                    <input id="saveStatus_reason" name="saveStatus" type="hidden" value="1" />
                    <input id="sentStatus_reason" name="sentStatus" type="hidden" value="1" />

                    <div class="btn-group float-right">
                        @if(!empty($review_reason) && $review_reason->created_by != user()->id)
                            <button type="button" id="rejSent_reason" class="btn btn-danger btn-default border-primary ">
                                @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                            </button>
                            <button type="button" id="savDraf_reason" class="btn btn-primary btn-group float-right ">
                                @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                            </button>
                        @endif
                        <button type="button" id="savSent_reason" class="btn btn-default float-middle border-default ">
                            @icon('fe fe-send') {{ __('Teratur') }}
                        </button>
                    </div>

                </form>
            </div>
        @endif
    @endif
