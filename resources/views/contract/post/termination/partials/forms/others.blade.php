@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            @if(!empty($othersTerminateNotice) && !empty($othersTerminateNotice->documents))
                @foreach($othersTerminateNotice->documents as $document)
                    $('#subFile').val('{{ $document->document_name }}');
                @endforeach
            @endif
        });
    </script>
@endpush

<h5>Penamatan Kontrak Kerana Sebab Lain-lain</h5>
<form method="POST" action="{{ route('termination.others') }}" files = "true" enctype="multipart/form-data">
    @csrf
    @include('components.forms.hidden', [
		'name' => 'sst_hashslug',
        'id' => 'sst_hashslug',
        'value' =>  $sst->hashslug 
    ]) 
    <div class="row">
        <div class="col-4">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Penamatan Kontrak'),
                'id' => 'others_termination_at',
                'name' => 'others_termination_at',
                'config' => ['format' => config('datetime.display.date')],
            ])
        </div>
        <div class="col-8">
            <div class="form-group row">
                <label for="Fail_Keputusan_Mesyuarat"
                    class="col col-form-label">
                    Muat Naik Surat Penamatan
                </label>
        
                <div class="col input-group">
                    <input id="subFile" type="text"  class="form-control" readonly>
                    <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                    <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                    @if(!empty($othersTerminateNotice) && !empty($othersTerminateNotice->documents))
                        @foreach($othersTerminateNotice->documents as $document)
                            <label class="input-group-text" for="downloadFile">
                                <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                            </label>
                        @endforeach
                    @endif
                </div>                        
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">            
            @include('components.forms.textarea', [
                'input_label' => 'Keterangan',
                'name' => 'description',
                'id' => 'description',
            ])               
        </div>
    </div>
    <div class="btn-group float-right">
        <a href="{{ route('contract.post.index') }}" 
            class="btn btn-default border-primary">
            {{ __('Kembali') }}
        </a>
        
        @if(empty($othersTerminateNotice) || (!empty($othersTerminateNotice) && ($othersTerminateNotice->status != 3)))
            <input type="submit" class="btn btn-primary save-action-btn" name="btnSubmit" id="btnSubmit" value="Simpan" />
            @if($sst->termination_type_id == 2 || $sst->termination_type_id == 3 || $sst->termination_type_id == 4 || $sst->termination_type_id == 5)
                <input type="submit" class="btn btn-success submit-action-btn" name="btnSubmit" id="btnSubmit" value="Selesai" />
            @endif
        @endif
    </div>
</form>