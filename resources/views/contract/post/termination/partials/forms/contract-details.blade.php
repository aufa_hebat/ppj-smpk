@push('scripts')
    @include('components.forms.assets.select2')
    <script>
        jQuery(document).ready(function($) {

            $("#termination_type_id").val('{{ $sst->termination_type_id }}');
            $('#jenis_notis_gantung').hide();

            if($('#termination_type_id').val() == 2){
                $("#type_suspension").val('{{ $sst->type_suspension }}');
                $('#jenis_notis_gantung').show();
            }

            $('#termination_type_id').change(function () {
                if($('#termination_type_id').val() == 2){
                    $('#jenis_notis_gantung').show();
                }else{
                    $('#jenis_notis_gantung').hide();
                }
            });
        });
    </script>
@endpush

<h5>Maklumat Projek</h5>
<form method="POST" action="{{ route('termination.termination.update', $sst->hashslug ) }}">
    @csrf
    @method('PUT')
    <table class="table">
        <tr>
            <td class="font-weight-bold">Tajuk</td>
            <td> : </td>
            <td colspan="7">{{ $sst->acquisition->title }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold">No. Kontrak</td>
            <td> : </td>
            <td>{{ $sst->acquisition->reference }}</td>
            <td class="font-weight-bold">Nilai Kontrak</td>
            <td> : </td>
            <td>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}</td>  
            <td colspan="3"></td>      
        </tr>
        <tr>
            <td class="font-weight-bold">Nama Kontraktor</td>
            <td> : </td>
            <td>{{ $sst->company->company_name }}</td>
            <td class="font-weight-bold">No. Pendaftaran Kontraktor</td>
            <td> : </td>
            <td>{{ $sst->company->ssm_no }}</td>  
            <td colspan="3"></td>
        </tr> 
        <tr>
            <td class="font-weight-bold">Alamat Kontraktor</td>
            <td> : </td>
            <td colspan="6">
                @if(!empty($sst->company->addresses[0]))
                    {{ $sst->company->addresses[0]->primary }} <br/>
                    {{ $sst->company->addresses[0]->secondary }} <br/>
                    {{ $sst->company->addresses[0]->postcode }}, {{ $sst->company->addresses[0]->state }} 
                @endif        
            </td>  
        </tr> 
        <tr>
            <td class="font-weight-bold">Tarikh Mula Kerja</td>
            <td> : </td>
            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}</td>
            <td class="font-weight-bold">Tarikh Siap Kerja</td>
            <td> : </td>
            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}</td>  
            <td class="font-weight-bold">Tempoh</td>
            <td> : </td>
            <td>{{ $sst->period." ".$sst->period_type->name }}</td> 
        </tr>  
        <tr>
            <td class="font-weight-bold">Sebab Penamatan Kontrak</td>
            <td> : </td>
            <td colspan="6">
                <select id="termination_type_id" name="termination_type_id" class="form-control w-100">
                    <option value="">{{ __('Sila Pilih') }}</option>
                    @foreach(termination_types() as $index => $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
            </td>
        </tr>

        <tr id="jenis_notis_gantung">
            <td class="font-weight-bold">Jenis Penggantungan</td>
            <td> : </td>
            <td colspan="6">
                @include('components.forms.select', [
                    'input_label' => '',
                    'id' => 'type_suspension',
                    'name' => 'type_suspension',
                    'options' => ['1' => 'Keseluruhan', '2' => 'Sebahagian'],                    
                ])

            </td>
        </tr>
    </table>

        <div class="btn-group float-right">
            <a href="{{ route('contract.post.index') }}" 
                class="btn btn-default border-primary">
                {{ __('Kembali') }}
            </a>
            @if(empty($sst->termination_at))
                <button type="submit" class="btn btn-primary">
                    @icon('fe fe-save') {{ __('Simpan') }}
                </button>
            @endif
        </div>

</form>
