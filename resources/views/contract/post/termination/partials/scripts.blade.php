@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {

        $(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.termination.edit', id));
		});
        $(document).on('click', '.warning-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.warning.index', {hashslug:id}));
		}); 

		$(document).on('click', '.warn-show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.warning.show', id));
		});

		$(document).on('click', '.warn-edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.warning.edit', id));
		});

		$(document).on('click', '.warn-destroy-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: '{!! __('Amaran') !!}',
			  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: '{!! __('Ya') !!}',
			  cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  if (result.value) {
			  	$('#destroy-record-form').attr('action', route('termination.warning.destroy', id))
				$('#destroy-record-form').submit();
			  }
			});
		});    

    });
	
</script>
@endpush
