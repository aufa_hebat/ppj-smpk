@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-ppk-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.variation-order.ppk_show', id));
		});
		$(document).on('click', '.edit-ppk-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
            redirect(route('contract.post.variation-order.ppk_edit', id));
		});
        $(document).on('click', '.print-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			window.open(route('vo_ppk', {hashslug:id}), '_blank');
		});	
		
		$(document).on('click', '.destroy-ppk-action-btn', function(event) {
			event.preventDefault();
			/* Act on the event */
			var id = $(this).data('hashslug');
			swal({
			  	title: '{!! __('Amaran') !!}',
			  	text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  	type: 'warning',
			  	showCancelButton: true,
			  	confirmButtonText: '{!! __('Ya') !!}',
			  	cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  	if (result.value) {
					axios.delete(
						route('api.contract.variation-order.destroy', id), 
						{'_method' : 'DELETE'})
						.then(response => {
							$('#contract-post-variation-order-ppk').DataTable().ajax.reload();
							swal('{{ __('Padam Permohonan Perubahan Kerja') }}', response.data.message, 'success');
						}).catch(error => console.error(error));
			  	}
			});
		});

		$(document).on('click', '.amend-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
            redirect(route('contract.post.variation-order-result.ppk_amend', id));
		}); 
		$(document).on('click', '.show-amend-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.variation-order-result.ppk_amend_show', id));
		});		       
	});
</script>
@endpush