@push('scripts')
<script>
    jQuery(document).ready(function($) {
        $(document).on('click', '.vo-details-action', function(event) {
            event.preventDefault();
            var data = $('#vo-details').serialize();
            console.log(data);

            axios.post(route('api.contract.variation-order.store'), data)
                .then(function(response) {
                    if(response.data.type == 'E'){
                        swal('Permohonan Perubahan Kerja', response.data.message, 'error');
                    }else{
                        
                        redirect(route('contract.post.variation-order.ppk_edit', response.data.hashslug));
                    }
                    
                });
        });
    });
</script>
@endpush

<div class="row">
<div class="col">
    <p>Bil. Permohonan Perubahan Kerja:&nbsp;<span class="badge badge-primary"></span></p>
</div>
</div>

<h4>Jenis Permohonan Perubahan Kerja </h4>
<div class="row">
<div class="col">
    <form id="vo-details"  method="POST">
        <input type="hidden" name="id" value="{{ $id }}">
        @foreach($types as $type )
            @include('components.forms.checkbox', [
                'name' => 'type[' . kebab_case($type->id) . ']',
                'id' => 'type-' . kebab_case($type->name),
                'label' => $type->name,
                'value' => $type->id,
            ])
        @endforeach
    </form>

    <div class="float-right btn btn-primary vo-details-action">
        Simpan
    </div>
    <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
        class="float-right btn btn-default border-primary">
        {{ __('Kembali') }}
    </a>    
</div>
</div>