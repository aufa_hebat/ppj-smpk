@push('scripts')
@include('components.forms.assets.datetimepicker')
@include('components.forms.assets.select2')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
<script>
    jQuery(document).ready(function($) {

        $('#uploadFile_ppk').change(function(){
            $('#subFile_ppk').val($(this).val().split('\\').pop());
        });

        @if(!empty($vo) && !empty($vo->doc_ppk))
            $('#subFile_ppk').val('{{ $vo->doc_ppk->document_name }}');
        @endif

        $(document).on('click', '.save-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $vo->id ?? ''}}';
            var route_name = 'api.contract.upload-ppk';
            var form_id = 'ppk_form';
            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', '');

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }

            axios.post(route(route_name, id), data).then(response => {
                swal('', response.data.message, 'success');
                location.reload();
            });
        });

        $(document).on('click', '.submit-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $vo->id ?? ''}}';
            var route_name = 'api.contract.upload-ppk';
            var form_id = 'ppk_form';
            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', 'submit');

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }

            axios.post(route(route_name, id), data).then(response => {
                swal('', 'Rekod telah berjaya dihantar ke urusetia vo', 'success');
                location.reload();
            });
        });
    });
</script>
@endpush

<h4>Muat Naik Dokumen PPK</h4>
<div class="row">
<div class="col-12">
    <form id="ppk_form" files = "true" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="form-group row">
            <label for="Fail_Keputusan_Mesyuarat"
               class="col col-form-label">
                Muat Naik Borang Pengesahan PPK
            </label>

            <div class="col input-group">
                <input id="subFile_ppk" type="text"  class="form-control" readonly>
                <label class="input-group-text" for="uploadFile_ppk"><i class="fe fe-upload" ></i></label>
                <input type="file" class="form-control" id="uploadFile_ppk" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                @if(!empty($vo) && !empty($vo->doc_ppk))
                    <label class="input-group-text" for="downloadFile">
                        <a href="/download/{{$vo->doc_ppk->document_path}}/{{ $vo->doc_ppk->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                    </label>
                @endif
            </div>
            
        </div>

        <div class="row">
            <div class="col-12" style="text-align:right;">
                <div class="btn-group float-right">
                    <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                        class="btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>
                    <button type="submit" class="btn btn-primary save-action-btn">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                    <button type="submit" class="btn btn-success float-middle border-default submit-action-btn">
                            @icon('fe fe-send') {{ __('Selesai') }}
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
</div>