@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
           /*$('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });*/

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });
            
            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh

            // button for review
            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ppk_id : $('#ppk_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ppk_id : $('#ppk_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ppk_id : $('#ppk_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });
            

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif

            // IF TYPES DATA EXIST
            @if(count($vo->types) > 0)
                @foreach($vo->types as $type)
                    @if($type->variation_order_type_id == 1)
                        $('#type-aktifkan-wang-peruntukkan-sementara').prop('checked', true);
                        $('#active_wps_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 2)
                        $('#type-perubahan-kerja').prop('checked', true);
                        $('#vo_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 3)
                        $('#type-wang-peruntukkan-sementara-dan-kos-prima').prop('checked', true);
                        $('#wps_and_prime_cost_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 4)
                        $('#type-pengiraan-semula-kuantiti-sementara\\(-provisional-quantity\\)').prop('checked', true);
                        $('#recalculate_provisional_quantity_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 5)
                        $('#type-kesilapan-keterangan-kerja-dan-kuantiti').prop('checked', true);
                        $('#wrong_info_sub_menu').show();
                    @endif
                @endforeach
            @endif

            // IF ACTIVE WPS DATA EXIST
            @if($vo->active_wps_status)
                @foreach($vo->actives as $active)
                    $('#select-active-wps{{$active->bq_item_id}}').prop('checked', true);
                @endforeach
            @endif

            // UPLOADS
            @if(((!empty($vo->documents)) && $vo->documents->count() > 0) || ((!empty($vo->doc_ppk)) && $vo->doc_ppk->count() > 0))
                var vo_t = $('#vo_tblUpload').DataTable({
                        searching: false,
                        ordering: false,
                        paging: false,
                        info:false
                    });
        
                var vo_counter = 1;
                @foreach($vo->documents as $doc)
                    vo_t.row.add( [
                        vo_counter,
                            '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                        ] ).draw( false );
        
                    vo_counter++;
                @endforeach
                @if($vo->doc_ppk)
                    vo_t.row.add( [
                        vo_counter,
                            '<a href="/download/{{ $vo->doc_ppk->document_path .'/'.$vo->doc_ppk->document_name }}" target="_blank"> {{ $vo->doc_ppk->document_name }}</a>'
                        ] ).draw( false );
    
                    vo_counter++;
                @endif
                @if($vo->doc_ppk_result)
                    vo_t.row.add( [
                        vo_counter,
                            '<a href="/download/{{ $vo->doc_ppk_result->document_path .'/'.$vo->doc_ppk_result->document_name }}" target="_blank"> {{ $vo->doc_ppk_result->document_name }}</a>'
                        ] ).draw( false );

                    vo_counter++;
                @endif                
            @endif
            // UPLOADS END

        });

        $(document).on('click', '.print-action-btn', function(event) {
            event.preventDefault();

            var id = '{{ $vo->hashslug }}';
            swal({
              title: 'PERINGATAN',
              text: 'Sila Cetak Menggunakan Kertas berwarna (ikut jabatan pelaksana ( JB : BIRU  ) ( JL : HIJAU ) ( JK : KUNING ) ( JP : MERAH  ) ( JR  : OREN ) (JW : PUTIH))',
              type: 'info'
            }).then((result) => {
                window.open(route('vo_ppk', {hashslug:id}), '_blank');
            });
        });

    </script>
@endpush
@section('content')
    {{--  @include('contract.post.variation-order.partials.title')  --}}
    @include('components.pages.title')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="vo-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#ppk-details" role="tab" aria-controls="ppk-details" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Jenis VO') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-doc" role="tab" aria-controls="ppk-doc" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Dokumen') }}
                    </a>
                </li>

                @if(!empty($review))
                    
                    @if(user()->id == $review->created_by)
                        @role('penyedia')
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                                    @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                                </a>
                            </li>
                        @endrole
                    @else
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                                @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                            </a>
                        </li>

                        @if(user()->current_role_login == 'administrator')
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews" role="tab" aria-controls="document-log-reviews" aria-selected="false">
                                    @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan') }}
                                </a>
                            </li>
                        @endif
                    @endif
                    @if((user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'VO') 
                        || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S4') && $review->status != 'Selesai') 
                        || (!empty($semakan4) && user()->id == $semakan4->approved_by && user()->id != $review->created_by && (empty($semakan5) || $reviewlog->status != 'S5') && $review->status != 'Selesai') 
                        || (!empty($semakan5) && user()->id == $semakan5->approved_by && user()->id != $review->created_by && (empty($cetaknotisbpub) || $reviewlog->status != 'CNBPUB') && $review->status != 'Selesai'))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-review" role="tab" aria-controls="document-review" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                            </a>
                        </li>
                    @endif
                @endif
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')   
                    @component('components.tab.container', ['id' => 'document'])    
                        @slot('tabs') 
                            @component('components.tab.content', ['id' => 'ppk-details', 'active' => true])    
                                @slot('content')        
                                    <div class="row">
                                        <div class="col">
                                            <p>Bil. Permohonan Perubahan Kerja:&nbsp;<span class="badge badge-primary">{{ $vo->no }}</span></p>
                                        </div>
                                    </div>                  
                                    <div class="row">
                                        <div class="col-8">
                                            <h4>Jenis Permohonan Perubahan Kerja</h4>
                                        </div>
                                        <div class="col-4">
                                            <div class="btn-group float-right">
                                                {{--  <a href="{{ route('vo_ppk',['hashslug' => $vo->hashslug]) }}" 
                                                    target="_blank" 
                                                    class="btn btn-success border-success">
                                                    @icon('fe fe-printer') {{ __(' Cetak') }}
                                                </a>  --}}
                                                <a target="_blank" href="#"
                                                    class="btn btn-success print-action-btn">
                                                    @icon('fe fe-printer') {{ __('Cetak') }}
                                                </a>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            @foreach($types as $type )
                                                <div class="form-inline">
                                                    <span>{{ $type->name }}</span>
                                                    &nbsp;&nbsp;&nbsp;
                                                    @foreach($vo->types as $vt)
                                                        @if($type->id == $vt->variation_order_type_id)
                                                            @if($vt->variation_order_type_id == 1)
                                                                <a href="{{ route('contract.post.variation-order.show-active-wps',['hashslug' => $vt->hashslug]) }}">
                                                                    @icon('fe fe-eye')
                                                                </a>
                                                            @elseif($vt->variation_order_type_id == 2)
                                                                <a href="{{ route('contract.post.variation-order.show-pk',['hashslug' => $vt->hashslug]) }}">
                                                                    @icon('fe fe-eye')
                                                                </a>
                                                            @elseif($vt->variation_order_type_id == 3)
                                                                <a href="{{ route('contract.post.variation-order.show-wpsSelect',['hashslug' => $vo->hashslug]) }}">
                                                                    @icon('fe fe-eye')
                                                                </a>
                                                            @elseif($vt->variation_order_type_id == 4)
                                                                <a href="{{ route('contract.post.variation-order.show-ps',['hashslug' => $vt->hashslug]) }}">
                                                                    @icon('fe fe-eye')
                                                                </a>
                                                            @elseif($vt->variation_order_type_id == 5)
                                                                <a href="{{ route('contract.post.variation-order.show-kkk',['hashslug' => $vt->hashslug]) }}">
                                                                    @icon('fe fe-eye')
                                                                </a>
                                                            @elseif($vt->variation_order_type_id == 6)
                                                                <a href="{{ route('contract.post.variation-order.show-cancel-wps',['hashslug' => $vt->hashslug]) }}">
                                                                    @icon('fe fe-eye')
                                                                </a>                                                
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                                class="float-right btn btn-default border-primary">
                                                {{ __('Kembali') }}
                                            </a>
                                        </div>
                                    </div>
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'ppk-doc'])
                                @slot('content')  
                                    <div class="row">
                                        <div class="col">
                                            @include('contract.post.variation-order.partials.forms.show.uploads', ['vo' => $vo])
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            @role('penyedia')
                                                @if($vo->status == 9)
                                                     @if(!empty($review) && $review->status == 'Selesai') 
                                                        @include('contract.post.variation-order.partials.forms.show.upload', ['vo' => $vo])
                                                        <br><br>
                                                     @endif 
                                                @else
                                                    <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                                        class="float-right btn btn-default border-primary">
                                                        {{ __('Kembali') }}
                                                    </a>                                                
                                                @endif
                                            @endrole
                                            @role('pengesah||penyemak')
                                                <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                                    class="float-right btn btn-default border-primary">
                                                    {{ __('Kembali') }}
                                                </a>
                                            @endrole
                                        </div>
                                    </div>  
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'document-reviews'])
                                @slot('content') 
                                    <div class="row">
                                        <div class="col">

                                            @if(!empty($review))
                                                @if((user()->current_role_login == 'penyedia'))

                                                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl5 as $s5log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl4 as $s4log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s4log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl3 as $s3log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s3log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                                        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif --}}
                                        
                                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl2 as $s2log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl1 as $s1log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s1log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @elseif((user()->current_role_login == 'pengesah' && user()->executor_department_id == 25))

                                                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl5 as $s5log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl4 as $s4log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s4log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl3 as $s3log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s3log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                                        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                        
                                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl2 as $s2log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl1 as $s1log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s1log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @else
                                                    {{-- pegawai bpub --}}
                                                    @if(user()->department->id == '9')

                                                        @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                            <div class="mb-0 card card-primary">
                                                                <div class="card-header" role="tab" id="headingOne2">
                                                                    <h4 class="card-title">
                                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                            Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                    <div class="card-body">
                                                                        @foreach($sl5 as $s5log)
                                                                            <div class="row">
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Status</div>
                                                                                    <p>{!! $s5log->reviews_status !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="text-muted">Ulasan</div>
                                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif

                                                        @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                            <div class="mb-0 card card-primary">
                                                                <div class="card-header" role="tab" id="headingOne2">
                                                                    <h4 class="card-title">
                                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                            Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                    <div class="card-body">
                                                                        @foreach($sl4 as $s4log)
                                                                            <div class="row">
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Status</div>
                                                                                    <p>{!! $s4log->reviews_status !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="text-muted">Ulasan</div>
                                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif

                                                        @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                            <div class="mb-0 card card-primary">
                                                                <div class="card-header" role="tab" id="headingOne2">
                                                                    <h4 class="card-title">
                                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                            Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                    <div class="card-body">
                                                                        @foreach($sl3 as $s3log)
                                                                            <div class="row">
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Status</div>
                                                                                    <p>{!! $s3log->reviews_status !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="text-muted">Ulasan</div>
                                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                        @endforeach                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                        
                                                        @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                            @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                                                <div class="mb-0 card card-primary">
                                                                    <div class="card-header" role="tab" id="headingOne2">
                                                                        <h4 class="card-title">
                                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                        <div class="card-body">
                                                                            @foreach($sl2 as $s2log)
                                                                                <div class="row">
                                                                                    <div class="col-4">
                                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="text-muted">Status</div>
                                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-12">
                                                                                        <div class="text-muted">Ulasan</div>
                                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endif

                                                    @endif

                                                    {{-- pegawai pelaksana --}}
                                                    @if(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)
                                        
                                                        @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                            <div class="mb-0 card card-primary">
                                                                <div class="card-header" role="tab" id="headingOne2">
                                                                    <h4 class="card-title">
                                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                    <div class="card-body">
                                                                        @foreach($sl2 as $s2log)
                                                                            <div class="row">
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Status</div>
                                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="text-muted">Ulasan</div>
                                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif

                                                        @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                            <div class="mb-0 card card-primary">
                                                                <div class="card-header" role="tab" id="headingOne2">
                                                                    <h4 class="card-title">
                                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                    <div class="card-body">
                                                                        @foreach($sl1 as $s1log)
                                                                            <div class="row">
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <div class="text-muted">Status</div>
                                                                                    <p>{!! $s1log->reviews_status !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="text-muted">Ulasan</div>
                                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif

                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>  
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'document-review'])
                                @slot('content') 
                                    <div class="row">
                                        <div class="col">
                                            @if(!empty($review))
                                
                                                {{-- semakan --}}
                                                @if(user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'VO' && $review->document_contract_type == 'PPK')
                                                    <h3>Semakan</h3>
                                                    <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                                        @csrf

                                                        @include('components.forms.hidden', [
                                                            'id' => 'id',
                                                            'name' => 'id',
                                                            'value' => ''
                                                        ])

                                                        @if(!empty($vo))
                                                            @include('components.forms.hidden', [
                                                                'id' => 'acquisition_id',
                                                                'name' => 'acquisition_id',
                                                                'value' => $vo->sst->acquisition_id
                                                            ])
                                                        @endif

                                                        @if(!empty($review))
                                                            <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
                                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">
                                                            <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
                                                        @endif

                                                        @if(!empty($review_previous))
                                                            <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                                            <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                                            <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                                            <input type="text" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                                            <input type="text" id="type" name="type" value="VO" hidden>
                                                            <input type="text" id="document_contract_type" name="document_contract_type" value="PPK" hidden>
                                                        @endif
                                                        <input type="text" id="ppk_id" name="ppk_id" value="{{$vo->id}}" hidden>
                                        
                                                        <div class="row">
                                                            <div class="col-6">
                                                                @include('components.forms.input', [
                                                                    'input_label' => __('Nama Pegawai Semakan'),
                                                                    'id' => '',
                                                                    'name' => '',
                                                                    'value' => user()->name,
                                                                    'readonly' => true
                                                                ])
                                                            </div>
                                                            @include('components.forms.hidden', [
                                                                'id' => 'requested_by',
                                                                'name' => 'requested_by',
                                                                'value' => user()->id
                                                            ])
                                                            @include('components.forms.hidden', [
                                                                'id' => 'approved_by',
                                                                'name' => 'approved_by',
                                                                'value' => user()->supervisor->id
                                                            ])
                                                            <div class="col-3">
                                                                @if(!empty($review))
                                                                    @if(!empty($review->status))
                                                                        @include('components.forms.input', [
                                                                            'input_label' => __('Tarikh Terima'),
                                                                            'id' => 'requested_at',
                                                                            'name' => 'requested_at',
                                                                        ])
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <div class="col-3">
                                                                @if(!empty($review))
                                                                    @if(!empty($review->status))
                                                                        @include('components.forms.input', [
                                                                            'input_label' => __('Tarikh Hantar'),
                                                                            'id' => 'approved_ats',
                                                                            'name' => 'approved_at',
                                                                        ])
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>

                                                        @if(!empty($review) && $review->created_by != user()->id)
                                                            @include('components.forms.textarea', [
                                                                'input_label' => __('Ulasan'),
                                                                'id' => 'remarks',
                                                                'name' => 'remarks'
                                                            ])
                                                        @else
                                                            {{--  TODO  --}}
                                                        @endif
                                        
                                                        <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                                        <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                                                        <div class="btn-group float-right">
                                                            <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                                                class="float-right btn btn-default border-primary">
                                                                {{ __('Kembali') }}
                                                            </a>
                                                            @if(!empty($review) && $review->created_by != user()->id)
                                                                <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                                                    @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                                                </button>
                                                                <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                                                    @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                                                </button>
                                                            @endif
                                                            <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                                                @icon('fe fe-send') {{ __('Teratur') }}
                                                            </button>
                                                        </div>
                                                    </form>
                                                @endif

                                            @endif
                                        </div>
                                    </div>  
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'document-log-reviews'])
                                @slot('content') 
                                    <div class="row">
                                        <div class="col">
                                            @if(!empty($review))

                                                {{-- log semakan --}}

                                                @if(user()->current_role_login == 'administrator')
                                                    @if(!empty($vo))
                                                        <div class="tab-pane fade show " id="document-contract-review-log" role="tabpanel" aria-labelledby="document-contract-review-log-tab">
                                                            <form id="log-form">
                                                                <div class="row justify-content-center w-100">
                                                                    <div class="col-12 w-100">
                                                                        @include('contract.pre.box.partials.scripts')
                                                                        @component('components.card')
                                                                            @slot('card_body')
                                                                                @component('components.datatable', 
                                                                                    [
                                                                                        'table_id' => 'contract-pre-box',
                                                                                        'route_name' => 'api.datatable.contract.review-log-vo-ppk',
                                                                                        'param' => 'ppk_id=' . $vo->id,
                                                                                        'columns' => [
                                                                                            ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                                            ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                                            ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                                                            ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                                            ['data' => 'jenis', 'title' => __('Jenis'), 'defaultContent' => '-'],
                                                                                            ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                                            ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                                            ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                                            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                                        ],
                                                                                        'headers' => [
                                                                                            __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Jenis'),__('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                                                        ],
                                                                                        'actions' => minify('')
                                                                                    ])
                                                                                @endcomponent
                                                                            @endslot
                                                                        @endcomponent
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>  
                                @endslot
                            @endcomponent
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection