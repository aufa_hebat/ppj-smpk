@extends('layouts.admin')

@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    
                    @component('components.tab.container', ['id' => 'variation-order'])
                        @slot('tabs')
                         	@component('components.tab.content', ['id' => 'details', 'active' => true])
                                @slot('content')

                                    <h4>Senarai WPS Aktif</h4>
                                    <div class="row">
                                    <div class="col">
                                        <form id="vo-details">
                                            <input type="hidden" name="id" value="{{ $id }}">
                                            
                                            @foreach($sst->variationOrders as $vos)
                                                @foreach($vos->actives->where('status', 1) as $active )
                                                    @php $statusWps = true; @endphp
                                                    {{--  @foreach($cancels as $cancel)
                                                        @if($cancel->item->id == $active->item->id)
                                                            @php $statusWps = false; @endphp
                                                        @endif  
                                                    @endforeach  --}}

                                                    @if($statusWps)
                                                        <div class="form-inline">
                                                            <label>{{ $active->item->item }}</label>
                                                            &nbsp;&nbsp;&nbsp;

                                                            <a href="{{ route('contract.post.variation-order.show-wps',['vo' => $active->hashslug, 'id' => $vo->id]) }}">
                                                                @icon('fe fe-eye')
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </form>
                                        
                                        <a href="{{ route('contract.post.variation-order.ppk_show', $vo->hashslug) }}" 
                                            class="float-right btn btn-default border-primary">
                                            {{ __('Kembali') }}
                                        </a>     
                                    </div>
                                    </div>
                                @endslot
                            @endcomponent
                            
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection