@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush


@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')            

                    <h4>Wang Peruntukkan Sementara Tidak Digunakan</h4>
                    <hr>
                    
                    <div id="table" class="table-editable table-bordered border-0">
         
                        <table class="table1 table-no-border" width="100%" cellspacing="0">
                            <thead>
                                <th width="5%" style="border:none" >Bil.</th>
                                <th width="70%" style="border:none" >Item</th>
                                <th width="25%" style="border:none">Jumlah (RM)</th>
                            </thead>
                            <tbody>
                                @php
                                    $jumlah = 0;
                                @endphp
                                @foreach($vo->cancels as $key => $cancel)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $cancel->item->item }}</td>
                                        <td style="text-align:right">{{ money()->toCommon($cancel->amount ?? "0", 2) }}</td>
                                    </tr>
                                    @php
                                        $jumlah = $jumlah + $cancel->amount;
                                    @endphp
                                @endforeach
                                <tr>
                                    <td colspan="2" style="font-weight:bold; text-align:center; text-transform:uppercase;">Jumlah</td>
                                    <td style="text-align:right;">{{ money()->toCommon($jumlah ?? "0", 2) }}</td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br/><br/>
                    <a href="{{ route('contract.post.variation-order.ppk_show', $vo->hashslug) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection