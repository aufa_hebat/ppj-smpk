@push('scripts')

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            
            /*!
            * bootstrap-treetable - jQuery plugin for bootstrapview treetable
            *
            * Copyright (c) 2007-2015 songhlc
            *
            * Licensed under the MIT license:
            *   http://www.opensource.org/licenses/mit-license.php
            *
            * Project home:
            *   http://github.com/songhlc
            *
            * Version:  1.0.0
            *
            */
            (function($){
            $.fn.bstreetable = function(options){
                $window = window;
                var element = this;
                var $container;
                var settings = {
                    container:window,
                    data:[],
                    extfield:[],//{title:"column name",key:"",type:"input"}
                    nodeaddEnable:true,
                    maxlevel:3,
                    nodeaddCallback:function(data,callback){},
                    noderemoveCallback:function(data,callback){},
                    nodeupdateCallback:function(data,callback){},
                    customalert:function(msg){
                        alert(msg);
                    },
                    customconfirm:function(msg){
                        return confirm(msg);
                    },
                    text:{
                        NodeDeleteText:"Anda Pasti Ingin Memadam Rekod Ini?"
                    }
                };
                var TREENODECACHE = "treenode";
                var language ={};
                language.addchild = "";
                // language.addchild = "Add A Child Node";
                if(options) {           
                    $.extend(settings, options);
                }
                /* Cache container as jQuery as object. */
                $container = (settings.container === undefined ||
                                settings.container === window) ? $window : $(settings.container);
                /*render data*/
                var dom_addFirstLevel = $("<div class='tt-operation m-b-sm' style='text-align:right'></div>").append($("<button class='btn btn-primary btn-sm j-addClass'><i class='fa fa-plus'></i>&nbsp;Tambah Elemen</button>"));
                var dom_table = $("<div class='tt-body'></div>");
                var dom_header = $("<div class='tt-header'></div>");
                
                /*renderHeader*/
                renderHeader(dom_header);
                element.html('').append(dom_addFirstLevel).append(dom_header);
                var treeData = {};
                
                /*render firstlevel tree*/
                for(var i=0;i<settings.data.length;i++){
                    var row = settings.data[i];
                    
                    //render first level row while row.pid equals 0 or null or undefined
                    if(!row.pid){
                        generateTreeNode(dom_table,row,1);
                        treeData[row.id] = row;
                    }
                    
                }

                element.append(dom_table);
                /*delegate click event*/
                element.delegate(".j-expend","click",function(event){
                    //alert(event.target.classList[0]);
                    //if(event.target.classList[0]=="svg-inline--fa"){
                        
                        var treenode = treeData[$(this).attr('data-id')];
                        toggleicon($(this));
                        if($(this).parent().attr('data-loaded')){
                            toggleExpendStatus($(this),treenode);        		
                        }
                        else{	    	
                            loadNode($(this),treenode);
                        }
                    //}        	        
                });
                element.delegate(".j-addClass","click",function(){
                    var curElement = $(".tt-body");
                    var row = {id:"",name:"",pid:0};
                    var curLevel = 1;
                    generateTreeNode(curElement,row,curLevel,true);
                });
                /*delegate remove event*/
                element.delegate(".j-remove","click",function(event){
                    //TODO:需要判断是否存在子节点，存在则不允许删除
                    var parentDom = $(this).parents(".class-level-ul");
                    var isRemoveAble = false;
                    if(parentDom.attr("data-loaded")=="true"){
                        if(parentDom.parent().find(".class-level").length>0){
                            settings.customalert("Can not be deleted!");
                            return;
                        }
                        else{
                            isRemoveAble = true;
                        }
                    }
                    else{
                        //如果是新增加的节点则设置成可删除否则需要展开再删除
                        if(parentDom.attr("data-id")){
                            var existChild = false;
                            for(var i=0;i<settings.data.length;i++){
                                if(settings.data[i].pid==parentDom.attr("data-id")){
                                    existChild = true;
                                    break;
                                }
                            }
                            if(existChild){
                                settings.customalert("Can not be deleted!");
                                return;
                            }
                            else{
                                isRemoveAble = true;
                            }
                        }
                        else{
                            isRemoveAble = true;
                        }
                    }
                    if(isRemoveAble){
                        var that = $(this);
                        //删除确认
                        if(settings.customconfirm(settings.text.NodeDeleteText)){
                            /*trigger remove callback*/
                            var curLevel = $(this).parents(".class-level-ul").attr("data-level")-0+1; 
                            settings.noderemoveCallback(that.parents(".class-level-ul").attr("data-id"), curLevel-1, function(){
                                that.parents(".class-level-ul").parent().remove();
                            });
                        }
                    }
                });
                /*delegate addchild event*/
                element.delegate(".j-addChild","click",function(){
                    var curElement = $(this).closest(".class-level");
                    var requiredInput = curElement.find(".form-control*[required]");
                    var hasError = false;
                    requiredInput.each(function(){
                        if($(this).val()==""){
                            $(this).parent().addClass("has-error");
                            hasError = true;                    
                        }
                    });
                    if(!hasError){
                        var pid = curElement.find(".j-expend").attr("data-id");
                        var curLevel = $(this).parents(".class-level-ul").attr("data-level")-0+1; 
                        var row = {id:"",name:"",pid:pid};
                        generateTreeNode(curElement,row,curLevel);   
                    }
                                
                });
                element.delegate(".j-searchClass","click", function(){
                    var curElement = $(this).closest(".class-level");
                    var pid = curElement.find(".j-expend").attr("data-id");
                    redirect(route('contract.post.variation-order.searchBQ', pid));
                });                
                element.delegate(".j-calculate","keyup", function(){
                    var curElement = $(this);
                    var curLevel = $(this).parents(".class-level-ul").attr("data-level")-0+1; 

                    console.log($(this).parents(".class-level-ul").children("li").children("input")[5].value);
                    

                    var data = {};

                    data.id = curElement.parent().parent().attr("data-id");
                    var parentUl = curElement.closest(".class-level-ul");
                    data.pid = parentUl.attr("data-pid");
                    data.description = parentUl.attr("data-description");
                    data.pdescription = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-description");
                    data.unit = parentUl.attr("data-unit");
                    data.punit = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-unit");            
                    data.rate = parentUl.attr("data-rate");
                    data.prate = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-rate");
                    data.qtyOM = parentUl.attr("data-qtyOM");
                    data.pqtyOM = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-qtyOM");
                    data.qtyAD = parentUl.attr("data-qtyAD");
                    data.pqtyAD = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-qtyAD");
                    data.ttlOM = parentUl.attr("data-ttlOM");
                    data.pttlOM = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-ttlOM");
                    data.ttlAD = parentUl.attr("data-ttlAD");
                    data.pttlAD = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-ttlAD");
                    data.net = parentUl.attr("data-net");
                    data.pnet = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-net");
                    data.typeBQ = parentUl.attr("data-typeBQ");
                    data.ptypeBQ = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-typeBQ");
                    data.level = curLevel-1;
                    parentUl.find(".form-control").each(function(){

                        data[$(this).attr("name")]=$(this).val();                
                    });

                    
                    //alert( curElement.find("name=ttlAD data-id="+data.id).val());
                    //alert(data.ttlAD);
                    data.ttlOM = data.qtyOM * data.rate;
                    data.ttlAD = data.qtyAD * data.rate;
                    data.net = data.ttlAD - data.ttlOM;

                    $(this).parents(".class-level-ul").children("li").children("input")[4].value = parseFloat(data.ttlOM).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    $(this).parents(".class-level-ul").children("li").children("input")[5].value = parseFloat(data.ttlAD).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    $(this).parents(".class-level-ul").children("li").children("input")[6].value = parseFloat(data.net).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    
                });                 
                /*焦点事件*/
                element.delegate(".form-control","focus",function(){
                    //在blur事件里如果输入内容为空会添加has-error样式
                    $(this).parent().removeClass("has-error");
                });
                /*delegate lose focus event*/
                // element.delegate(".j-save","click",function(){
                element.delegate(".form-control","blur",function(){
                    var curElement = $(this);
                    var curLevel = $(this).parents(".class-level-ul").attr("data-level")-0+1; 
                    var data = {};

                    data.id = curElement.parent().parent().attr("data-id");
                    var parentUl = curElement.closest(".class-level-ul");
                    data.pid = parentUl.attr("data-pid");
                    data.description = parentUl.attr("data-description");
                    data.pdescription = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-description");
                    data.unit = parentUl.attr("data-unit");
                    data.punit = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-unit");            
                    data.rate = parentUl.attr("data-rate");
                    data.prate = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-rate");
                    data.qtyOM = parentUl.attr("data-qtyOM");
                    data.pqtyOM = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-qtyOM");
                    data.qtyAD = parentUl.attr("data-qtyAD");
                    data.pqtyAD = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-qtyAD");
                    data.ttlOM = parentUl.attr("data-ttlOM");
                    data.pttlOM = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-ttlOM");
                    data.ttlAD = parentUl.attr("data-ttlAD");
                    data.pttlAD = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-ttlAD");
                    data.net = parentUl.attr("data-net");
                    data.pnet = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-net");
                    data.typeBQ = parentUl.attr("data-typeBQ");
                    data.ptypeBQ = curElement.parents(".class-level-"+(parentUl.attr("data-level")-1)).children("ul").attr("data-typeBQ");
                    data.level = curLevel-1;
                    parentUl.find(".form-control").each(function(){
                        data[$(this).attr("name")]=$(this).val();                
                    });
                    if(!data.id&&!curElement.attr("data-oldval")){
                        console.log("add node");                
                        settings.nodeaddCallback(data,function(_data){
                            if(_data){
                                curElement.parent().attr("data-id",_data.id);
                                curElement.parent().parent().attr("data-id",_data.id);
                                curElement.parent().parent().attr("data-description",_data.description);
                                curElement.parent().parent().attr("data-unit",_data.unit);
                                curElement.parent().parent().attr("data-rate",_data.rate);
                                curElement.parent().parent().attr("data-qtyOM",_data.qtyOM);
                                curElement.parent().parent().attr("data-qtyAD",_data.qtyAD);
                                curElement.parent().parent().attr("data-ttlOM",_data.ttlOM);
                                curElement.parent().parent().attr("data-ttlAD",_data.ttlAD);
                                curElement.parent().parent().attr("data-net",_data.net);
                                curElement.parent().parent().attr("data-typeBQ",_data.typeBQ);
                                curElement.attr("data-oldval",curElement.val());
                            }
                        });                            
                    }
                    else if(curElement.attr("data-oldval")!=curElement.val()){
                        console.log("update node");   
                        settings.nodeupdateCallback(data,function(){
                            curElement.attr("data-oldval",curElement.val());
                        });
                        
                    }
                });

                /*渲染表头*/
                function renderHeader(_dom_header){
                    var dom_row = $('<div></div>');
                    dom_row.append($("<span class='maintitle'></span>").text(settings.maintitle));
                    dom_row.append($("<span></span>"));        	
                    //render extfield
                    for(var j=0;j<settings.extfield.length;j++){
                        var column = settings.extfield[j];    			
                        $("<span></span>").css("min-width",column.width).text(column.title).appendTo(dom_row);
                    }
                    dom_row.append($("<span class='textalign-center'>Tindakan</span>")); 
                    _dom_header.append(dom_row);
                }
                //动态生成扩展字段
                function generateColumn(row,extfield){
                    var generatedCol;
                    switch(extfield.type){
                        case "textarea":generatedCol=$("<textarea cols='30' rows='1' class='form-control input-sm' placeholder='Keterangan'/>").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "input":generatedCol=$("<input type='text' size='27' class='form-control input-sm' />").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "inputUnit":generatedCol=$("<input type='text' size='1' class='form-control input-sm' placeholder='Unit'/>").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "inputQOM":generatedCol=$("<input type='text' size='2' class='form-control input-sm j-calculate'/>").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "inputQAD":generatedCol=$("<input type='text' size='2' class='form-control input-sm j-calculate'/>").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "inputRate":generatedCol=$("<input type='text' size='3' class='form-control input-sm j-calculate' placeholder='Kadar' />").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "inputTotal":generatedCol=$("<input type='text' size='5' class='form-control input-sm' readonly />").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "inputRef":generatedCol=$("<input type='text' size='1' class='form-control input-sm' />").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "selectType":generatedCol=$("<select class='form-control input-sm' style='height:2rem;'><option value='BQ Rate'>BQ Rate</option><option value='Jkr Rate'>JKR Rate</option><option value='Prorate Rate'>Prorate Rate</option><option value='Propose Rate'>Propose Rate</option></select>").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        case "selectTypeBq":generatedCol=$("<select class='form-control input-sm' style='height:2rem;'><option value='BQ Rate'>BQ Rate</option></select>").val(row[extfield.key]).attr("data-oldval",row[extfield.key]).attr("name",extfield.key);break;
                        default:generatedCol=$("<span></span>").text(row[extfield.key]);break;
                    }
                    return generatedCol;
                }

                function generateDummyColumn(row){
                    var generatedCol;
                    
                    generatedCol=$("<textarea cols='108' rows='1' class='form-control input-sm'/>").val(row["description"]).attr("data-oldval",row["description"]).attr("name","description");
                                        
                    return generatedCol;
                }

                function generateDescriptionColumn(row){
                    var generatedCol;
                    
                    generatedCol=$("<textarea cols='108' rows='1' class='form-control input-sm' placeholder='Sebab-Sebab Perubahan Kerja'/>").val(row["description"]).attr("data-oldval",row["description"]).attr("name","description");
                                        
                    return generatedCol;
                }

                function toggleicon(toggleElement){
                    var _element = toggleElement.find(".svg-inline--fa");
                    if(_element.hasClass("fa-plus")){
                        _element.removeClass("fa-plus").addClass("fa-minus");
                        toggleElement.parent().addClass("selected");
                    }else{
                        _element.removeClass("fa-minus").addClass("fa-plus");
                        toggleElement.parent().removeClass("selected");                        
                    }
                }
                function toggleExpendStatus(curElement){
                    console.log(curElement.parent().parent().find(".class-level"));
                    if(curElement.find(".fa-minus").length>0){
                            curElement.parent().parent().find(".class-level").removeClass("rowhidden");
                    }
                    else{
                        curElement.parent().parent().find(".class-level").addClass("rowhidden");
                        //curElement.parent().parent().find(".class-level").removeClass("rowhidden");
                    }
                    
                }
                function collapseNode(){

                }
                /*展开节点*/
                function expendNode(){

                }
                /*加载子节点*/
                function loadNode(loadElement,parentNode){
                    var curElement = loadElement.parent().parent();
                    var curLevel = loadElement.parent().attr("data-level")-0+1; 
                    //TODO:将已经加载过的数据从list中删除，减少循环次数
                    if(parentNode&&parentNode.id){
                        for(var i=0;i<settings.data.length;i++){
                            var row = settings.data[i];
                            //render first level row while row.pid equals 0 or null or undefined
                            if(row.pid==parentNode.id){
                                generateTreeNode(curElement,row,curLevel);
                                //cache treenode 
                                treeData[row.id] = row;
                            }	        	
                        }                
                    }
                    loadElement.parent().attr('data-loaded',true);                    
                }

                function generateTreeNode(curElement,row,curLevel,isPrepend){
                    var dom_row = $('<div class="class-level class-level-'+curLevel+'"></div>');
                    var dom_ul =$('<ul class="class-level-ul"></ul>');
                    dom_ul.attr("data-pid",row.pid).attr("data-level",curLevel).attr("data-id",row.id);
                    row.description&&dom_ul.attr("data-description",row.description);
                    row.unit&&dom_ul.attr("data-unit",row.unit);
                    row.rate&&dom_ul.attr("data-rate",row.rate);
                    row.qtyOM&&dom_ul.attr("data-qtyOM",row.qtyOM);
                    row.qtyAD&&dom_ul.attr("data-qtyAD",row.qtyAD);
                    row.ttlOM&&dom_ul.attr("data-ttlOM",row.ttlOM);
                    row.ttlAD&&dom_ul.attr("data-ttlAD",row.ttlAD);
                    row.net&&dom_ul.attr("data-net",row.net);
                    row.typeBQ&&dom_ul.attr("data-typeBQ",row.typeBQ);
                    if(curLevel-0>=settings.maxlevel){
                        $('<li class="j-expend"></li>')
                             .append('<label class="p-xs"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
                             //.append($("<input type='text' size='29' class='form-control input-sm' required/>").attr("data-oldval",row['name']).val(row['name']).attr("name","name")).attr('data-id',row.id)
                            .append($("<textarea id='editor' cols='25' rows='1' class='form-control input-sm' required/>").attr("data-oldval",row['name']).val(row['name']).attr("name","name")).attr('data-id',row.id)
                            .appendTo(dom_ul);
                        dom_ul.attr("data-loaded",true);
                    }
                    else{
                        if(curLevel == 1){
                            $('<li class="j-expend"></li>')
                                .append('<label class="fa fa-plus p-xs icon-indent"></label>&nbsp;&nbsp;')
                                //.append($("<input type='text' size='29' class='form-control input-sm' required/>").attr("data-oldval",row['name']).val(row['name']).attr("name","name")).attr('data-id',row.id)
                                .append($("<textarea id='editor' cols='25' rows='1' class='form-control input-sm' placeholder='Huraian Perubahan Kerja' required/>").attr("data-oldval",row['name']).val(row['name']).attr("name","name")).attr('data-id',row.id)
                                .appendTo(dom_ul);
                        }else{
                            $('<li class="j-expend"></li>')
                            .append('<label class="fa fa-plus p-xs icon-indent"></label>&nbsp;&nbsp;')
                            //.append($("<input type='text' size='29' class='form-control input-sm' required/>").attr("data-oldval",row['name']).val(row['name']).attr("name","name")).attr('data-id',row.id)
                            .append($("<textarea id='editor' cols='25' rows='1' class='form-control input-sm' required/>").attr("data-oldval",row['name']).val(row['name']).attr("name","name")).attr('data-id',row.id)
                            .appendTo(dom_ul);
                        }
                    }

                    if(curLevel-0>=settings.maxlevel){
                        for(var j=0;j<settings.extfield.length;j++){
                            var colrender = settings.extfield[j];
                            var coltemplate = generateColumn(row,colrender);
                            $('<li></li>').attr("data-id",row.id).html(coltemplate).appendTo(dom_ul);
                        }
                    }else{
                        if(curLevel == 1){
                            var coltemplate = generateDescriptionColumn(row);
                            $('<li></li>').attr("data-id",row.id).html(coltemplate).appendTo(dom_ul);                            
                        }else{
                            var coltemplate = generateDummyColumn(row);
                            $('<li></li>').attr("data-id",row.id).html(coltemplate).appendTo(dom_ul);
                        }
                    }
                    
                    if(settings.nodeaddEnable){

                        if(curLevel-0>=settings.maxlevel){
                            $("<li></li>")
                            // .append($('<button class="btn btn-outline btn-sm j-searchClass"><i class="fa fa-archive"></i></button>').attr("data-id",row.id))

                            dom_ul
                            .append($('<div class="dropdown"> \n' +
                                    '<button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \n' +
                                      '<i class="fa fa-ellipsis-v"></i> \n' +
                                    '</button> \n' +
                                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> \n' +                                                                       
                                      '<button class="btn btn-outline btn-sm j-remove dropdown-item"><i class="fe fe-trash  text-danger"></i> Hapus </button> \n' +
                                    '</div> \n' +
                                  '</div>').attr("data-id",row.id));


                            //.appendTo(dom_ul);
                        }
                        else{
                            if(curLevel == 1){
                                $("<li></li>")
                                //.append($('<button class="btn btn-outline btn-sm j-addChild"><i class="fa fa-plus"></i>'+language.addchild +'</button>').attr("data-id",row.id))
                                //.append($('<button class="btn btn-outline btn-sm j-searchClass"><i class="fa fa-archive"></i></button>').attr("data-id",row.id))
                                //.appendTo(dom_ul);  
                                dom_ul
                                .append($('<div class="dropdown"> \n' +
                                        '<button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \n' +
                                          '<i class="fa fa-ellipsis-v"></i> \n' +
                                        '</button> \n' +
                                        '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> \n' +                                  
                                          '<button class="btn btn-outline btn-sm j-addChild dropdown-item"><i class="fa fa-plus text-primary"></i> Tambah Item \n' +
                                          '<button class="btn btn-outline btn-sm j-searchClass dropdown-item"><i class="fe fe-file text-info"></i> Senarai Kuantiti </button> \n' +
                                          '<button class="btn btn-outline btn-sm j-remove dropdown-item"><i class="fe fe-trash  text-danger"></i> Hapus </button> \n' +
                                        '</div> \n' +
                                      '</div>').attr("data-id",row.id));
                            }else{
                                $("<li></li>")
                                //.append($('<button class="btn btn-outline btn-sm j-addChild"><i class="fa fa-plus"></i>'+language.addchild +'</button>').attr("data-id",row.id))
                                //.appendTo(dom_ul);  
                                dom_ul
                                .append($('<div class="dropdown"> \n' +
                                        '<button class="btn btn-secondary btn-sm" style="border:0px" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \n' +
                                          '<i class="fa fa-ellipsis-v"></i> \n' +
                                        '</button> \n' +
                                        '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> \n' +                                  
                                          '<button class="btn btn-outline btn-sm j-addChild dropdown-item"><i class="fa fa-plus text-primary"></i> Tambah Item \n' +
                                          '<button class="btn btn-outline btn-sm j-remove dropdown-item"><i class="fe fe-trash  text-danger"></i> Hapus </button> \n' +
                                        '</div> \n' +
                                      '</div>').attr("data-id",row.id));
                            }                   
                        }
                        
                    }       

                    /*dom_ul
                        .append($('<div class="dropdown"> \n' +
                                '<button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \n' +
                                  '<i class="fa fa-ellipsis-v"></i> \n' +
                                '</button> \n' +
                                '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> \n' +                                  
                                  '<button class="btn btn-outline btn-sm j-addChild dropdown-item"><i class="fa fa-plus text-primary"></i> Tambah Item \n' +
                                  '<button class="btn btn-outline btn-sm j-searchClass dropdown-item"><i class="fe fe-file text-info"></i> Senarai Kuantiti </button> \n' +
                                  '<button class="btn btn-outline btn-sm j-remove dropdown-item"><i class="fe fe-trash  text-danger"></i> Hapus </button> \n' +
                                '</div> \n' +
                              '</div>').attr("data-id",row.id));*/
                        //.append($("<li><button class='btn btn-outline btn-sm j-remove'><i class='fa fa-trash'></i></button></li>"))
                        //.append($("<li><button class='btn btn-outline btn-sm j-save'><i class='fa fa-save'></i></button></li>"));
                    dom_row.append(dom_ul);
                    if(isPrepend){
                        curElement.prepend(dom_row);
                    }
                    else{
                        curElement.append(dom_row);
                    }
                    
                }
            }
            })(jQuery)
        });
    </script>
@endpush