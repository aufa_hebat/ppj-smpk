@extends('layouts.admin')

@push('scripts')

@endpush

@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')   
                
                    @if(!empty($elements))
                    <form method="post" action="{{ route('contract.post.variation-order.selectBQ', $id) }}">
                        @csrf
                        @foreach($elements as $bq)    
                            {{--  @if($bq->element != 'PROVISIONAL SUMS' && $bq->element != 'PROVISIONAL SUM' && $bq->element != 'PROV SUM' && $bq->element != 'PROV. SUM' && $bq->element != 'PROV.SUM')                          --}}
                            @php
                                $isProSumFound = common()->checkProSum($bq->element);
                            @endphp
                        
                            @if(!$isProSumFound)

                            <div class="mb-0 card" >
                                <div class="card-header" role="tab" id="heading{{$bq->id}}">
                                    <h4 class="card-title cardElemen">
                                        <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{$bq->id}}" href="#collapse{{$bq->id}}" aria-expanded="true" aria-controls="collapse{{$bq->id}}">
                                            <i class="mr-2 fa fa-folder-open"></i><u>{{$bq->no}}&nbsp;:&nbsp;{{$bq->element}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($bq->amount_adjust ?? 0,2 )}}</u>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$bq->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{$bq->id}}" >
                                    <div class="card-body">
                                        <div class="row">
                                            <div id="semakanBq" class="col-md-12 table-responsive">
                                                
                                                <table class="table1  table-no-border" width="100%" cellspacing="0">
                                                    @foreach($items as $it) 
                                                        @if($bq->no == $it->bq_no_element && $bq->acquisition_id == $it->acquisition_id)                                
                                                            <thead >
                                                                <th style="width:100%;border:none" >
                                                                    <h5>{{$it->bq_no_element}}.{{$it->no}}&nbsp;:&nbsp;{{$it->item}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($it->amount_adjust ?? 0,2 )}}</h5><br>
                                                                </th>
                                                            </thead>
                                                            <tbody class="list">
                                                                <tr style="width:100%;border:none">
                                                                    <td class="ulasanBQs">
                                                                        <table id="ulasan_bq{{$it->id}}" class="table table-bordered" width="100%" cellspacing="0" style="font-size:12px;">
                                                                            <thead>
                                                                                <th style="width:2%">NO ITEM</th>
                                                                                <th style="width:24%">ELEMEN</th>
                                                                                <th style="width:2%">UNIT</th>
                                                                                <th style="width:6%">HARGA SEUNIT</th>
                                                                                <th style="width:6%">KUANTITI ASAL</th>
                                                                                <th style="width:10%">JUMLAH ASAL</th>
                                                                                <th style="width:10%">PILIH</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach($subItems as $key=>$bqs)
                                                                                    @if($bqs->bq_no_element == $bq->no  && $bqs->acquisition_id == $bq->acquisition_id && $bqs->bq_no_item == $it->no)
                                                                                        <tr>
                                                                                            <td>{{$bqs->bq_no_element}}.{{$bqs->bq_no_item}}.{{$bqs->no}}</td>
                                                                                            <td>{{$bqs->item}}</td>
                                                                                            <td>{{$bqs->unit}}</td>
                                                                                            <td>{{money()->toCommon($bqs->rate_per_unit_adjust ?? 0,2 )}}</td>
                                                                                            <td>{{money()->toCommon($bqs->quantity ?? 0,2 )}}</td>
                                                                                            <td>{{money()->toCommon($bqs->amount_adjust ?? 0,2 )}}</td>
                                                                                            <td>
                                                                                                    <input type="checkbox" name="vo[{{$bqs->id}}][id]" value="{{$bqs->id}}"  />
                                                                                                {{--  <a href="{{ route('contract.post.variation-order.selectBQ',['id' => $id, 'bqId' => $bqs->id]) }}" class="btn btn-primary float-right">Pilih</a>  --}}
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                        </tbody>
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach 
                        <div style="text-align:right;">
                                <button class="btn btn-primary" type='submit'>Simpan</button> 
                            </div

                    </form> 
                    @endif             

                    {{--  <table width="100%" style="border-style: solid;border-width: thin;" >
                        @foreach($elements as $el)
                            <tr>
                                <td width="100%">{{ $el->element }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" style="border-style: solid;border-width: thin;" >
                                        @foreach($items as $it)
                                            @if($it->bq_no_element == $el->no)
                                                <tr>
                                                    <td width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $it->item }}</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" style="border-style: solid;border-width: thin;" >
                                                            @foreach($subItems as $sit)
                                                                @if($sit->bq_no_element == $el->no && $sit->bq_no_item == $it->no)
                                                                    <tr>
                                                                        <td width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sit->item }}</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                    </table>  --}}
                @endslot
            @endcomponent
        </div>
    </div>
@endsection