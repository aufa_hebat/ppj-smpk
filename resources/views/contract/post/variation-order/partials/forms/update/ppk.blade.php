@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $('#active_wps_sub_menu').hide();
            $('#type-aktifkan-wang-peruntukkan-sementara').change(function() {
                if($(this).is(":checked")) {
                    $('#active_wps_sub_menu').show();
                }
                else{
                    $('#active_wps_sub_menu').hide();
                }       
            });

            $('#vo_sub_menu').hide();
            $('#type-perubahan-kerja').change(function() {
                if($(this).is(":checked")) {
                    $('#vo_sub_menu').show();
                }
                else{
                    $('#vo_sub_menu').hide();
                }       
            });

            $('#wps_and_prime_cost_sub_menu').hide();
            $('#type-wang-peruntukkan-sementara-dan-kos-prima').change(function() {
                if($(this).is(":checked")) {
                    $('#wps_and_prime_cost_sub_menu').show();
                }
                else{
                    $('#wps_and_prime_cost_sub_menu').hide();
                }       
            });

            $('#recalculate_provisional_quantity_sub_menu').hide();
            $('#type-pengiraan-semula-kuantiti-sementara\\(-provisional-quantity\\)').change(function() {
                if($(this).is(":checked")) {
                    $('#recalculate_provisional_quantity_sub_menu').show();
                }
                else{
                    $('#recalculate_provisional_quantity_sub_menu').hide();
                }       
            });

            $('#wrong_info_sub_menu').hide();
            $('#type-kesilapan-keterangan-kerja-dan-kuantiti').change(function() {
                if($(this).is(":checked")) {
                    $('#wrong_info_sub_menu').show();
                }
                else{
                    $('#wrong_info_sub_menu').hide();
                }       
            });

            $('#batal_wps_sub_menu').hide();
            $('#type-batalkan-wang-peruntukkan-sementara').change(function() {
                if($(this).is(":checked")) {
                    $('#batal_wps_sub_menu').show();
                }
                else{
                    $('#batal_wps_sub_menu').hide();
                }       
            });

            // IF TYPES DATA EXIST
            @if(count($vo->types) > 0)
                @foreach($vo->types as $type)
                    @if($type->variation_order_type_id == 1)
                        $('#type-aktifkan-wang-peruntukkan-sementara').prop('checked', true);
                        $('#active_wps_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 2)
                        $('#type-perubahan-kerja').prop('checked', true);
                        $('#vo_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 3)
                        $('#type-wang-peruntukkan-sementara-dan-kos-prima').prop('checked', true);
                        $('#wps_and_prime_cost_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 4)
                        $('#type-pengiraan-semula-kuantiti-sementara\\(-provisional-quantity\\)').prop('checked', true);
                        $('#recalculate_provisional_quantity_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 5)
                        $('#type-kesilapan-keterangan-kerja-dan-kuantiti').prop('checked', true);
                        $('#wrong_info_sub_menu').show();
                    @endif
                    @if($type->variation_order_type_id == 6)
                    $('#type-wang-peruntukkan-sementara-tidak-digunakan').prop('checked', true);
                    $('#batal_wps_sub_menu').show();
                @endif                    
                @endforeach
            @endif

            // IF ACTIVE WPS DATA EXIST
            @if($vo->active_wps_status)
                @foreach($vo->actives as $active)
                    $('#select-active-wps{{$active->bq_item_id}}').prop('checked', true);
                @endforeach
            @endif

        });
    </script>
@endpush
@section('content')
    {{--  @include('contract.post.variation-order.partials.title')  --}}
    @include('components.pages.title')
    {{--  <div class="row">  --}}
        {{--  <div class="col-12">  --}}
            {{--  @component('components.card')  --}}
                {{--  @slot('card_body')  --}}
                    
                    {{--  @component('components.tab.container', ['id' => 'variation-order'])  --}}
                        {{--  @slot('tabs')  --}}
                         	{{--  @component('components.tab.content', ['id' => 'details', 'active' => true])  --}}
                                {{--  @slot('content')  --}}
                                    @include('contract.post.variation-order.partials.forms.update.details')
                                {{--  @endslot  --}}
                            {{--  @endcomponent  --}}
                        {{--  @endslot  --}}
                    {{--  @endcomponent  --}}
                {{--  @endslot  --}}
            {{--  @endcomponent  --}}
        {{--  </div>  --}}
    {{--  </div>  --}}
@endsection