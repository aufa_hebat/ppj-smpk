@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(count($BQItem) > 0)
                @foreach($BQItem as $item)
                    @if(count($vo->actives) > 0)    
                        @foreach($vo->actives as $active)
                            @if($item->id == $active->bq_item_id)
                                $('#active-{{ $item->id }}').prop('checked', true);
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif

            $(document).on('click', '.submit-active-wps-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $vo->sst->id }}';
                var hashslug = '{{ $vo->hashslug }}';
                var route_name = 'api.contract.variation-order-active-wps.update';
                var form_id = 'active_wps_form';
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('Aktif Wang Peruntukkan Sementara', response.data.message, 'success');
                    redirect(route('contract.post.variation-order.ppk_edit', hashslug));
                });
            });
        });
    </script>
@endpush


@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')            
                    <form id="active_wps_form">
                        <input type="hidden" name="sst_id" id="sst_id" value="{{ $vo->sst_id }}">
                        <input type="hidden" name="vo_id" id="vo_id" value="{{ $vo->id }}">
                        <input type="hidden" name="variation_order_type_id" id="variation_order_type_id" value="3">

                        <h4>Aktif Wang Peruntukkan Sementara</h4>
                        <hr>
                        @if(!empty($BQElemen))
                            <div id="table" class="table-editable table-bordered border-0">
                                {{--  @foreach($BQElemen as $bq)  --}}
                                    {{--  @if($bq->element == 'PROVISIONAL SUMS' || $bq->element == 'PROVISIONAL SUM' || $bq->element == 'PROV SUM' || $bq->element == 'PROV. SUM' || $bq->element == 'PROV.SUM')  --}}
                                        <input type="hidden" name="bq_element_id" id="bq_element_id" value="{{$BQElemen->id}}">
                                        <div class="mb-0 card" >
                                            <div class="card-header" role="tab" id="heading{{$BQElemen->id}}">
                                                <h4 class="card-title cardElemen">
                                                    <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{$BQElemen->id}}" href="#activewpscollapse{{$BQElemen->id}}" aria-expanded="true" aria-controls="activewpscollapse{{$BQElemen->id}}">
                                                        {{$BQElemen->no}}. {{$BQElemen->element}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($BQElemen->amount ?? 0,2 )}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="activewpscollapse{{$BQElemen->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{$BQElemen->id}}" >
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div id="semakanBq" class="col-md-12 table-responsive">
                                                            <table class="table1 table-no-border" width="100%" cellspacing="0">
                                                                <thead>
                                                                    <th style="border:none" >
                                                                        Kerja
                                                                    </th>
                                                                    <th style="border:none">
                                                                        Pilih
                                                                    </th>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($BQItem as $it)
                                                                        @if($BQElemen->no == $it->bq_no_element && $BQElemen->acquisition_id == $it->acquisition_id)
                                                                            <tr>
                                                                                <td style="border:none;">
                                                                                    <h6>{{$it->bq_no_element}}.{{$it->no}} : {{$it->item}} - RM{{money()->toCommon($it->amount ?? 0,2 )}}</h6>
                                                                                </td>
                                                                                <td style="border:none;">
                                                                                    @include('components.forms.checkbox', [
                                                                                        'name' => 'select_active_wps[' . kebab_case($it->id) . ']',
                                                                                        'id' => 'active-'.$it->id,
                                                                                        'label' => '',
                                                                                        'value' => $it->id,
                                                                                    ])
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {{--  @endif  --}}
                                {{--  @endforeach  --}}
                            </div>
                        @endif
                        <br>
                       
                        <div class="btn-group float-right">
                            <button type="submit" class="btn btn-primary submit-active-wps-action-btn">
                                @icon('fe fe-save') {{ __('Aktif') }}
                            </button>
                        </div>
                        <a href="{{ route('contract.post.variation-order.ppk_edit', $vo->hashslug) }}" 
                            class="float-right btn btn-default border-primary">
                            {{ __('Kembali') }}
                        </a>                          
                    </form>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection





{{--  @push('styles')
    <script>
        jQuery(document).ready(function($) {
            $(document).on('click', '.vo-active-wps-action', function(event) {
                event.preventDefault();
                var data = $('#vo-active-wps').serialize();
                console.log(data);
    
                axios.put(route('api.contract.variation-order-active-wps.update', '{{ $id }}'), data)
                    .then(function(response) {
                        swal('', response.data.message, 'success');
                        location.reload();
                    });
            });
        });
    </script>
@endpush

@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')            
                    <h4>Aktif Wang Peruntukan Sementara</h4>
                    <form id="vo-active-wps">
                        <input type="hidden" name="id" value="{{ $id }}">

                        <table class="table table-bordered" width="100%">
                            <tr>
                                <td width="3%">No.</td>
                                <td width="40%">Item</td>
                                <td width="40%">Keterangan</td>
                                <td width="10%">Jumlah</td>
                                <td width="7%"></td>
                            </tr>
                            @foreach($BQSub as $key => $subItem)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $subItem->item }}</td>
                                    <td>{{ $subItem->description }}</td>
                                    <td>{{ money()->toCommon($subItem->amount ?? 0, 2) }}</td>
                                    <td>
                                        @include('components.forms.checkbox', [
                                            'name' => 'type[' . kebab_case($subItem->id) . ']',
                                            'id' => 'type-' . kebab_case($subItem->id),
                                            'label' => '',
                                            'value' => $subItem->id,
                                        ])
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </form>
                    <div class="float-right btn btn-primary vo-active-wps-action">
                        Simpan
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection  --}}