@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ppk_id : $('#ppk_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($vo) && ($vo->documents->count() > 0))
                @foreach($vo->documents as $doc)
                    t.row.add( [
                        '<div class="form-group">\n' +
                        '   <div class="col input-group">\n' +
                        '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                        '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                        '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                        '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                        '       <label class="input-group-text" for="uploadFile'+ counter +'"><a href="/download/{{$doc->document_path}}/{{ $doc->document_name }}" target="_blank"><i class="fas fa-download"></i></a></label>\n' +
                        '   </div>\n' +
                        '</div>',
                        '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                    ] ).draw( false );

                    counter++;
                @endforeach
            @else
            // Automatically add a first row of data
                $('#addRow').click();
            @endif
       

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        t.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            }); 

            $(document).on('click', '#vo-details-action', function(event) {
                event.preventDefault();

                var id = '{{ $id }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                axios.post(route(route_name, id), data).then( (response) => {
                    swal('', response.data.message, 'success');
                    redirect(route('contract.post.variation-order.ppk_edit', response.data.hashslug));                
                }).catch((error)=>{
                    console.log(error.response);
                });
            });

            $(document).on('click', '#vo-documents-action', function(event) {
                event.preventDefault();

                var id = '{{ $id }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                axios.post(route(route_name, id), data).then( (response) => {
                    swal('', response.data.message, 'success');
                    redirect(route('contract.post.variation-order.ppk_edit', response.data.hashslug));                
                }).catch((error)=>{
                    console.log(error.response);
                });
            });
        });

        $(document).on('click', '.print-action-btn', function(event) {
            event.preventDefault();

            var id = '{{ $vo->hashslug }}';
            swal({
              title: 'PERINGATAN',
              text: 'Sila Cetak Menggunakan Kertas berwarna (ikut jabatan pelaksana ( JB : BIRU  ) ( JL : HIJAU ) ( JK : KUNING ) ( JP : MERAH  ) ( JR  : OREN ) (JW : PUTIH))',
              type: 'info'
            }).then((result) => {
                window.open(route('vo_ppk', {hashslug:id}), '_blank');
            });
        });
    </script>
@endpush

<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="vo-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#ppk-details" role="tab" aria-controls="ppk-details" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Jenis VO') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#ppk-doc" role="tab" aria-controls="ppk-doc" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Dokumen') }}
                </a>
            </li>
            @if(!empty($review) && (user()->id == $review->created_by))
            <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('Senarai Ulasan') }}
                </a>
            </li>
            @endif            
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')   
                @component('components.tab.container', ['id' => 'document'])    
                    @slot('tabs')                         
                            @component('components.tab.content', ['id' => 'ppk-details', 'active' => true])    
                                @slot('content')    
                                    <form id="vo-details-form" files = "true" enctype="multipart/form-data" method="POST">
                                        @csrf
                                        @method('PUT')                
                                        
                                        <input type="hidden" name="id" value="{{ $id }}">                                
                                        
                                        <div class="row">
                                            <div class="col">
                                                <p>Bil. Permohonan Perubahan Kerja:&nbsp;<span class="badge badge-primary">{{ $vo->no }}</span></p>
                                            </div>
                                        </div>                                    
                                    
                                        <div class="row">
                                            <div class="col-8">
                                                <h4>Jenis Permohonan Perubahan Kerja</h4>
                                            </div>
                                            <div class="col-4">
                                                <div class="btn-group float-right">
                                                    {{--  <a href="{{ route('vo_ppk',['hashslug' => $vo->hashslug]) }}" 
                                                        target="_blank" 
                                                        class="btn btn-success border-success">
                                                        @icon('fe fe-printer') {{ __(' Cetak') }}
                                                    </a>  --}}
                                                    <a target="_blank" href="#"
                                                        class="btn btn-success print-action-btn">
                                                        @icon('fe fe-printer') {{ __('Cetak') }}
                                                    </a>   
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col">                                   
                                                @foreach($types as $type )
                                                    <div class="form-inline">
                                                        @include('components.forms.checkbox', [
                                                            'name' => 'type[' . kebab_case($type->id) . ']',
                                                            'id' => 'type-' . kebab_case($type->name),
                                                            'label' => $type->name,
                                                            'value' => $type->id,
                                                        ])&nbsp;&nbsp;&nbsp;
                                                        
                                                        @foreach($vo->types as $vt)
                                                            @if($type->id == $vt->variation_order_type_id)
                                                                @if($vt->variation_order_type_id == 1)
                                                                    <a href="{{ route('contract.post.variation-order.active-wps',['hashslug' => $vt->hashslug]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>
                                                                @elseif($vt->variation_order_type_id == 2)
                                                                    <a href="{{ route('contract.post.variation-order.pk',['hashslug' => $vt->hashslug]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>
                                                                @elseif($vt->variation_order_type_id == 3)
                                                                    <a href="{{ route('contract.post.variation-order.wpsSelect',['hashslug' => $vt->hashslug]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>
                                                                @elseif($vt->variation_order_type_id == 4)
                                                                    <a href="{{ route('contract.post.variation-order.ps',['hashslug' => $vt->hashslug]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>
                                                                @elseif($vt->variation_order_type_id == 5)
                                                                    <a href="{{ route('contract.post.variation-order.kkk',['hashslug' => $vt->hashslug]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>
                                                                @elseif($vt->variation_order_type_id == 6)
                                                                    <a href="{{ route('contract.post.variation-order.cancel-wps',['hashslug' => $vt->hashslug]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endforeach                                    
                                            </div>
                                        </div>  
                                    </form>
                                    <div  class="btn-group float-right">
                                        <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                            class="float-right btn btn-default border-primary">
                                            {{ __('Kembali') }}
                                        </a> 
                                                    
                                        <button type="submit" class="btn btn-primary float-right " id="vo-details-action"
                                            data-route="api.contract.variation-order.update"
                                            data-form="vo-details-form">
                                            @icon('fe fe-save') {{ __('Kemaskini') }}
                                        </button>

                                        @role('penyedia')
                                            @if(!empty($vo) && $vo->sst->user_id == user()->id)
                                                @if(empty($review) || empty($review->status))
                                                    <form method="POST" action="{{ route('acquisition.review.store') }}">
                                                        @csrf
                                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                                        <input type="text" id="acquisition_id" name="acquisition_id" value="@if(!empty($vo)){{ $vo->sst->acquisition_id }}@endif" hidden>
                                            
                                                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>                                
                                                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>
                                                        <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>
                                            
                                                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                            
                                                        @if(!empty($review_previous))
                                                            <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                                            <input type="text" id="type" name="type" value="VO" hidden>
                                                            <input type="text" id="document_contract_type" name="document_contract_type" value="PPK" hidden>
                                                        @endif
                                            
                                                        <input type="text" id="ppk_id" name="ppk_id" value="{{$vo->id}}" hidden>
                                                                        
                                                        <button type="button" id="send" class="btn btn-default float-middle border-default">
                                                            @icon('fe fe-send') {{ __('Teratur') }}
                                                        </button>
                                                    </form>
                                                @endif
                                            @endif
                                            
                                            <br>
                                        @endrole                                         
                                    </div>
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'ppk-doc'])
                                @slot('content')
                                    <form id="vo-documents-form" files = "true" enctype="multipart/form-data" method="POST">
                                        @csrf
                                        @method('PUT')                
                                    
                                        <input type="hidden" name="id" value="{{ $id }}">  

                                        <div class="row">
                                            <div class="col">
                                                <div id="uploads">
                                                    <h5>Muat Naik Dokumen</h5>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="float-right">
                                                                <button type="button" id="addRow" href="#" class="btn btn-primary">
                                                                    @icon('fe fe-plus')
                                                                    {{ __('Tambah Dokumen') }}
                                                                </button>
                                                            </div>
                                                            <div class="table-responsive">
                                                                <table id="tblUpload" class="table table-sm table-transparent">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Muat Naik Dokumen</th>
                                                                            <th width="70px">Hapus</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div  class="btn-group float-right">
                                        <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                            class="float-right btn btn-default border-primary">
                                            {{ __('Kembali') }}
                                        </a> 
                                                        
                                        <button type="submit" class="btn btn-primary float-right " id="vo-documents-action"
                                            data-route="api.contract.variation-order.update"
                                            data-form="vo-documents-form">
                                            @icon('fe fe-save') {{ __('Simpan') }}
                                        </button>

                                        @role('penyedia')
                                            @if(!empty($vo) && $vo->sst->user_id == user()->id)
                                                @if(empty($review) || empty($review->status))
                                                    <form method="POST" action="{{ route('acquisition.review.store') }}">
                                                        @csrf
                                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                                        <input type="text" id="acquisition_id" name="acquisition_id" value="@if(!empty($vo)){{ $vo->sst->acquisition_id }}@endif" hidden>
                                            
                                                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>                                
                                                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>
                                                        <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>
                                            
                                                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                            
                                                        @if(!empty($review_previous))
                                                            <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                                            <input type="text" id="type" name="type" value="VO" hidden>
                                                            <input type="text" id="document_contract_type" name="document_contract_type" value="PPK" hidden>
                                                        @endif
                                            
                                                        <input type="text" id="ppk_id" name="ppk_id" value="{{$vo->id}}" hidden>
                                                                        
                                                        <button type="button" id="send" class="btn btn-default float-middle border-default">
                                                            @icon('fe fe-send') {{ __('Teratur') }}
                                                        </button>
                                                    </form>
                                                @endif
                                            @endif
                                            
                                            <br>
                                        @endrole  
                                    </div>                                    
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'document-reviews'])
                                @slot('content') 
                                    <div class="row">
                                        <div class="col">
                                            @if(!empty($review) && (user()->id == $review->created_by))                                                
                                                @if((!empty($review)) && (user()->id == $review->created_by))
                                                
                                                    {{-- penyedia --}}                                                
                                                    {{-- @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                        Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl7 as $s7log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s7log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                
                                                    @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                                                                        Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl6 as $s6log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s6log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif --}}
                                                
                                                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl5 as $s5log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                
                                                    {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl4 as $s4log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s4log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                                                                    
                                                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl3 as $s3log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s3log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif --}} 
                                                
                                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl2 as $s2log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                
                                                    @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                        <div class="mb-0 card card-primary">
                                                            <div class="card-header" role="tab" id="headingOne2">
                                                                <h4 class="card-title">
                                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                                <div class="card-body">
                                                                    @foreach($sl1 as $s1log)
                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Terima</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="text-muted">Status</div>
                                                                                <p>{!! $s1log->reviews_status !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <div class="text-muted">Ulasan</div>
                                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                    @endforeach                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif                                               
                                                @endif                                                            
                                            @endif 
                                                            
                                            @if(!empty($vo))
                                                @role('administrator')
                                                    <form id="log-form">
                                                        <div class="row justify-content-center w-100">
                                                            <div class="col-12 w-100">
                                                                @include('contract.pre.box.partials.scripts')
                                                                @component('components.card')
                                                                    @slot('card_body')
                                                                        @component('components.datatable', 
                                                                            [
                                                                                'table_id' => 'contract-pre-box',
                                                                                'route_name' => 'api.datatable.contract.review-log-vo-ppk',
                                                                                'param' => 'ppk_id=' . $vo->id,
                                                                                'columns' => [
                                                                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                                    ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                                    ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                                    ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                                ],
                                                                                'headers' => [
                                                                                    __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'),__('Status'), __('')
                                                                                ],
                                                                                'actions' => minify('')
                                                                            ])
                                                                        @endcomponent
                                                                @endslot
                                                                @endcomponent
                                                            </div>
                                                        </div>
                                                    </form>
                                                @endrole
                                            @endif 
                                        </div>
                                    </div>
                                @endslot
                            @endcomponent
                        

                        {{--  <div class="btn-group float-right">                                  --}}
                            {{--  <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                class="float-right btn btn-default border-primary">
                                {{ __('Kembali') }}
                            </a> 
                                
                            <button type="submit" class="btn btn-primary float-right " id="vo-details-action"
                                data-route="api.contract.variation-order.update"
                                data-form="vo-details-form">
                                @icon('fe fe-save') {{ __('Kemaskini') }}
                            </button>  --}}
                            
                            {{--  <br>  --}}
                                
                            {{--  @role('penyedia')
                                @if(!empty($vo) && $vo->sst->user_id == user()->id)
                                    @if(empty($review) || empty($review->status))
                                        <form method="POST" action="{{ route('acquisition.review.store') }}">
                                            @csrf
                                            <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                            <input type="text" name="acquisition_id" value="@if(!empty($vo)){{ $vo->sst->acquisition_id }}@endif" hidden>
                                
                                            <input type="text" name="requested_by" value="{{ user()->id }}" hidden>                                
                                            <input type="text" name="approved_by" value="{{ user()->supervisor->id }}" hidden>
                                            <input type="text" name="created_by" value="{{user()->id}}" hidden>
                                
                                            <input type="text" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                
                                            @if(!empty($review_previous))
                                                <input type="hidden" name="task_by" value="{{$review_previous->task_by}}">
                                                <input type="hidden" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                                <input type="text" name="type" value="VO" hidden>
                                                <input type="text" name="document_contract_type" value="PPK" hidden>
                                            @endif
                                
                                            <input type="text" name="ppk_id" value="{{$vo->id}}" hidden>
                                                            
                                            <button type="submit" class="btn btn-default float-middle border-default">
                                                @icon('fe fe-send') {{ __('Hantar') }}
                                            </button>
                                        </form>
                                    @endif
                                @endif
                                
                                <br>
                            @endrole                                       --}}
                        {{--  </div>   --}}
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>
</div>



