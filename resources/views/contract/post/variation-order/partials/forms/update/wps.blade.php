@extends('layouts.admin')
@include('contract.post.variation-order.partials.forms.update.js.wps')
@push('scripts')
    {{--  <script type="text/javascript" src="http://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>  --}}
    {{--  <script type="text/javascript" src="{{ asset('js/jquery.edittreetable.js') }}"></script>  --}}
    
    <script>
        jQuery(document).ready(function($) {

            var vo_type_id = '{{ $type_id }}';
            
            var data = [];
            data = [
                @if($PKElemen != null)
                    @foreach($PKElemen as $bqE)  
                        {id:'E'+'{{ $bqE->id }}',name:'{!!str_replace(["\r\n","\r","\n"],"\\n ", $bqE->element)!!}',pid:0,description:'{!!str_replace("\n", " \\n ",  $bqE->description  )!!}',unit:'',rate:"",qtyAD:"",ttlAD:'{{ money()->toCommon($bqE->amount_addition) }}',net:'{{ money()->toCommon($bqE->net_amount) }}',ref:'{{ $bqE->bq_reference }}', typeBQ:""},          
                    @endforeach
                @endif
                @if($PKItem != null)                    
                    @foreach($PKItem as $bqI)                                                      
                        {id:'I' + '{{ $bqI->id }}',name:'{!!str_replace(["\r\n","\r","\n"],"\\n ", $bqI->item)!!}',pid:'E' + '{{ $bqI->vo_element_id }}',description:'{!!str_replace("\n", "\\n", $bqI->description) !!}',unit:'',rate:"",qtyAD:"",ttlAD:'{{ money()->toCommon($bqI->amount_addition) }}',net:'{{ money()->toCommon($bqI->net_amount) }}',ref:'{{ $bqI->bq_reference }}',typeBQ:""},
                    @endforeach
                @endif                      
                @if($PKSub != null)
                    @foreach($PKSub as $bq) 
                        {id:'S' + '{{ $bq->id }}',name:'{!!str_replace(["\r\n","\r","\n"],"\\n ", $bq->item)!!}',pid:'I' + '{{ $bq->vo_item_id }}',description:'{!!str_replace("\n", "\\n", $bq->description)!!}',unit:'{{ $bq->unit }}',rate:'{{ money()->toCommon($bq->rate_per_unit) }}',qtyAD:'{{ money()->toCommon($bq->quantity_addition) }}',ttlAD:'{{ money()->toCommon($bq->amount_addition) }}',net:'{{ money()->toCommon($bq->net_amount) }}',ref:'{{ $bq->bq_reference }}',typeBQ:'{{ $bq->status_lock }}'},                            
                    @endforeach          
                @endif
            ];
        
            $("#bs-treeetable").bstreetable({
                data:data,
                maintitle:"Elemen/Item",
                
                nodeaddCallback:function(data,callback){
                    //alert(JSON.stringify(data));

                    //do your things then callback
                    if(data.pid == 0){
                        var route_name = 'api.contract.create-vo-element'; 
                    }else{
                        if(data.level == 2){
                            var route_name = 'api.contract.create-vo-item';
                        }else if(data.level == 3){
                            var route_name = 'api.contract.create-vo-subitem';                        
                        }
                    }

                    var id = '{{ $type_id ?? ''}}';
                    data.activeId = '{{ $active->hashslug ?? ''}}';
                    console.log(data);
                    axios.post(route(route_name, id), data)
                    .then(response => {
                        if(data.level == 1){
                            callback({id:'E' + response.data.id,name:data.name,innercode:"ttttt",pid:data.pid});
                        }else if(data.level == 2){
                            callback({id:'I' + response.data.id,name:data.name,innercode:"ttttt",pid:data.pid});
                        }else if(data.level == 3){
                            callback({id:'S' + response.data.id,name:data.name,innercode:"ttttt",pid:data.pid});
                        }
                        
                    });

                    
                },
                noderemoveCallback:function(data, level, callback){
                    //alert(JSON.stringify(data));
                               
                    //do your things then callback
                    var route_name = ''; 
                    if(level == 1){
                        route_name = 'api.contract.delete-vo-element';
                    }else if(level == 2){
                        route_name = 'api.contract.delete-vo-item';
                    }else if(level == 3){
                        route_name = 'api.contract.delete-vo-subitem';                        
                    } 

                    var id = data;
                    if(id != ''){
                        axios.get(route(route_name, id)).then(response => {
                            //location.reload();
                        });
                    }


                    callback();
                },
                nodeupdateCallback:function(data,callback){
                    //alert(JSON.stringify(data));
                    //do your things then callback

                    var route_name = ''; 
                    if(data.level == 1){
                        route_name = 'api.contract.create-vo-element';
                    }else if(data.level == 2){
                        route_name = 'api.contract.create-vo-item';
                    }else if(data.level == 3){
                        route_name = 'api.contract.create-vo-subitem';                        
                    }                    

                    var id = '{{ $type_id ?? ''}}';
                    data.activeId = '{{ $active->hashslug ?? ''}}';
                    axios.post(route(route_name, id), data)
                    .then(response => {
                        //location.reload();
                    });

                    callback();
                },
                extfield:[
                    {title:"Keterangan",key:"description",type:"textarea",width:"220px"},
                    {title:"Unit",key:"unit",type:"inputUnit",width:"40px"},
                    {title:"Kadar",key:"rate",type:"inputRate",width:"55px"},
                    //{title:"Q-OM",key:"qtyOM",type:"inputQOM",width:"55px"},
                    {title:"Kuantiti",key:"qtyAD",type:"inputQAD",width:"55px"},
                    //{title:"T-OM",key:"ttlOM",type:"inputTotal",width:"70px"},
                    {title:"Jumlah",key:"ttlAD",type:"inputTotal",width:"70px"},
                    {title:"Jumlah Bersih",key:"net",type:"inputTotal",width:"70px"},
                    {title:"Ruj BQ",key:"ref",type:"inputRef",width:"100px"},
                    {title:"Jenis",key:"typeBQ",type:"selectType",width:"45px"},
                ]
            });
           // $(".j-expend").trigger('click');
            //$(".j-expend").trigger('click');
            //$(".j-expend").trigger('click');
        });
    </script>
 
@endpush

@push('styles')
    {{--  <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">  --}}
    {{--  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">  --}}
    {{--  <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" crossorigin="anonymous">  --}}
    {{--  <link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css" media="screen" title="no title" charset="utf-8"/>  --}}

    <style>
        body { background-color:#fafafa;}
        .treetable{
    
        }
        .treetable .fa{
            cursor: pointer;
            padding-right: 5px;
        }
        .treetable .rowhidden{
            display: none;
            /*display: '';*/
        }
        .treetable .j-addChild{
            /*display: 'none';*/
            display: '';
        }
        .treetable .selected .j-addChild{
            /*display: block;*/
            display: '';
        }
        .treetable .btn-outline{
            background-color: transparent;
        }
        .treetable .form-control{
            width: auto;
            display: inline-block;
            font-size: 13px;  
            padding: 0.3rem 0.3rem; 
            border: 1px solid #000;           
        }
        .treetable .textalign-center{
            text-align: center;
        }
        .treetable .j-expend{
            cursor: pointer;
            width: 26% !important;
            text-align: left !important;
        }
        .treetable .maintitle{
            width: 26% !important;
        }
        .treetable .j-remove{
            /*padding: 8px;*/
            cursor: pointer;
            font-size: 13px;
            /*color:red;*/
        }
        .treetable .tt-header{
            margin-top:10px;
            font-size: 13px;
        }
        .treetable .class-level-2 .class-level-ul .j-expend{
            position: relative;
            left: 22px;
        }
        .treetable .class-level-3 .class-level-ul .j-expend{
            position: relative;
            left: 44px;
        }
        .treetable .class-level-4 .class-level-ul .j-expend{
            position: relative;
            left: 66px;
        }
        .treetable .class-level-1 {
            border-bottom: dashed 1px #eee;
        }
        .treetable .class-level-ul{
            padding: 0;
            margin-bottom: 2px;
        }
        .treetable .class-level-ul li {
            float: left;
            text-align: center;
            vertical-align: middle;
            /*padding: 1px 10px;*/
            min-width: 10px;
            list-style: none;
        }
        .treetable .class-level-ul:after {
            display: block;
            clear: both;
            height: 0;
            content: "\0020";
        }
        .treetable .tt-header div span {
            width: auto;
            line-height: 29px;
            display: inline-block;
            /*min-width: 120px;*/
            text-align: center;
        }
        .treetable .tt-body{
            border: solid 1px #DDD;
            padding-top: 1px;
            background-color:#FFF;            
        }
        .treetable .tt-header div{
            border: solid 1px #DDD;
            border-bottom:none;
            background-color:#FFF;
        }

        .icon-indent{
           vertical-align:0.5em;
        }

        /*.svg-inline--fa {
            vertical-align:0.5em;
        }*/
    </style>   
@endpush

@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')            
                    <h4>WPS & Kos Prima (Tambahan)</h4>
                    <div class="row">
                        <div class="col-12"><h5>{{ $active->item->item }}</h5></div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            Jumlah Peruntukan Asal : {{ money()->toHuman($active->item->amount_adjust ?? "0", 2)}}
                        </div>                        
                        <div class="col-4">
                            Jumlah Peruntukan Terkini : {{ money()->toHuman($active->amount ?? "0", 2)}}
                        </div>
                        {{--  <div class="col-4">
                            Jumlah Telah Digunakan : {{ money()->toHuman($totalUsed ?? "0", 2)}}
                        </div>   
                        <div class="col-4">
                            Baki : {{ money()->toHuman($active->amount - $totalUsed ?? "0", 2)}}
                        </div>                         --}}
                    </div>                    

                    <div class="container" style="margin-top:10px;">
                        <div id="bs-treeetable" class="treetable">
                            Loading ...
                        </div>
                    </div>
                    <br/>
                    <a href="{{ route('contract.post.variation-order.wpsSelect', $type_id) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a> 
                @endslot
            @endcomponent
        </div>
    </div>
@endsection
    

   

