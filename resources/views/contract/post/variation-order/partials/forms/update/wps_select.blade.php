@extends('layouts.admin')

@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    
                    @component('components.tab.container', ['id' => 'variation-order'])
                        @slot('tabs')
                         	@component('components.tab.content', ['id' => 'details', 'active' => true])
                                @slot('content')

                                    <h4>Senarai Wang Peruntukan Sementara (WPS) Aktif</h4>
                                    <div class="row">
                                        <div class="col">
                                            <form id="vo-details">
                                                <input type="hidden" name="id" value="{{ $id }}">
                                                
                                                <table class="table" width="95%">
                                                    <tr>
                                                        <td width="70%">Wang Peruntukan Sementara</td>
                                                        <td width="15%">Jumlah Peruntukan</td>
                                                        <td width="15%">Tindakan</td>
                                                    </tr>

                                                    @foreach($sst->variationOrders as $vos)
                                                        @foreach($vos->actives->where('status', 1) as $active )
                                                            @php $statusWps = true; @endphp
                                                            @foreach($cancels as $cancel)
                                                                @if($cancel->item->id == $active->item->id)
                                                                    @php $statusWps = false; @endphp
                                                                @endif  
                                                            @endforeach

                                                            @if($statusWps)
                                                                <tr>
                                                                    <td>{{ $active->item->item }}</td>
                                                                    <td>
                                                                        {{--  {{ money()->toCommon( $active->item->amount_adjust ?? "0", 2) }}  --}}
                                                                        {{ money()->toCommon( $active->amount ?? "0", 2) }}
                                                                    </td>
                                                                    <td>                                                                        
                                                                        <a href="{{ route('contract.post.variation-order.wps',['vo' => $active->hashslug, 'id' => $vo->id]) }}">
                                                                            @icon('fe fe-edit')
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </table>
                                            </form>
                                        
                                            <a href="{{ route('contract.post.variation-order.ppk_edit', $vo->hashslug) }}" 
                                                class="float-right btn btn-default border-primary">
                                                {{ __('Kembali') }}
                                            </a>     
                                        </div>
                                    </div>
                                @endslot
                            @endcomponent
                            
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection