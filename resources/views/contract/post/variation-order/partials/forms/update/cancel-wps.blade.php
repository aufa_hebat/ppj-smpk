@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(count($BQItem) > 0)
                @foreach($BQItem as $item)
                    @if(count($vo->cancels) > 0)    
                        @foreach($vo->cancels as $cancel)
                            @if($item->id == $cancel->bq_item_id)
                                $('#cancel-{{ $item->id }}').prop('checked', true);
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif

            $(document).on('click', '.submit-cancel-wps-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $vo->sst->id }}';
                var hashslug = '{{ $vo->hashslug }}';
                var route_name = 'api.contract.variation-order-cancel-wps.update';
                var form_id = 'cancel_wps_form';
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('Wang Peruntukkan Sementara Tidak Digunakan', response.data.message, 'success');
                   // redirect(route('contract.post.variation-order.ppk_edit', hashslug));
                });
            });
        });

    </script>
@endpush


@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')            
                    <form id="cancel_wps_form" >
                        <input type="hidden" name="sst_id" id="sst_id" value="{{ $vo->sst_id }}">
                        <input type="hidden" name="vo_id" id="vo_id" value="{{ $vo->id }}">
                        <input type="hidden" name="variation_order_type_id" id="variation_order_type_id" value="3">

                        <h4>Wang Peruntukkan Sementara Tidak Digunakan</h4>
                        <hr>
                        @if(!empty($BQElemen))
                            <div id="table" class="table-editable table-bordered border-0">
                                {{--  @foreach($BQElemen as $bq)  --}}
                                    {{--  @if($bq->element == 'PROVISIONAL SUMS' || $bq->element == 'PROVISIONAL SUM' || $bq->element == 'PROV SUM' || $bq->element == 'PROV. SUM' || $bq->element == 'PROV.SUM')  --}}
                                        <input type="hidden" name="bq_element_id" id="bq_element_id" value="{{$BQElemen->id}}">
                                        <div class="mb-0 card" >
                                            <div class="card-header" role="tab" id="heading{{$BQElemen->id}}">
                                                <h4 class="card-title cardElemen">
                                                    <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{$BQElemen->id}}" href="#cancelwpscollapse{{$BQElemen->id}}" aria-expanded="true" aria-controls="cancelwpscollapse{{$BQElemen->id}}">
                                                        {{$BQElemen->no}}. {{$BQElemen->element}} &nbsp;&nbsp;&nbsp;
                                                        @if(!empty($BQElemen->voActives))
                                                            RM{{money()->toCommon($BQElemen->voActives()->pluck('amount')->sum() ?? 0,2 )}}
                                                        @else
                                                            RM{{money()->toCommon($BQElemen->amount_adjust ?? 0,2 )}}
                                                        @endif
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cancelwpscollapse{{$BQElemen->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{$BQElemen->id}}" >
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div id="semakanBq" class="col-md-12 table-responsive">
                                                            <table class="table1 table-no-border" width="100%" cellspacing="0">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="border:none" width="70%">
                                                                            Kerja
                                                                        </th>
                                                                        <th style="border:none" width="5%">
                                                                            Status
                                                                        </th>
                                                                        <th style="border:none" width="10%">
                                                                            Jumlah Peruntukan
                                                                        </th>
                                                                        <th style="border:none" width="10%">
                                                                            Jumlah Digunakan
                                                                        </th>
                                                                        <th style="border:none" width="10%">
                                                                            Baki
                                                                        </th>
                                                                        <th style="border:none" width="5%">
                                                                            Pilih
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($BQItem as $it)
                                                                        @if($BQElemen->no == $it->bq_no_element && $BQElemen->acquisition_id == $it->acquisition_id)
                                                                            <tr>
                                                                                <td style="border:none;">
                                                                                    @php $currAmount = 0; @endphp

                                                                                    @if($actives->count() > 0 && $actives->where('bq_item_id', $it->id)->where('status', 1) != null)
                                                                                        @php $currAmount = $actives->where('bq_item_id', $it->id)->where('status', 1)->pluck('amount')->sum(); @endphp
                                                                                        <h6>{{$it->bq_no_element}}.{{$it->no}} : {{$it->item}} - RM{{money()->toCommon($currAmount ?? 0,2 )}}</h6>
                                                                                    @else
                                                                                        @php $currAmount = $it->amount_adjust; @endphp
                                                                                        <h6>{{$it->bq_no_element}}.{{$it->no}} : {{$it->item}} - RM{{money()->toCommon($it->amount_adjust ?? 0,2 )}}</h6>
                                                                                    @endif
                                                                                    
                                                                                </td>
                                                                                <td style="border:none;">
                                                                                    @php 
                                                                                        $status = false; 
                                                                                        $balanceAmount = $currAmount;
                                                                                        $totalPending = 0;
                                                                                        $totalUsed = 0;
                                                                                        $totalUsedAll = 0;

                                                                                        foreach($actives as $active){
                                                                                            if($it->id == $active->bq_item_id && $active->vo->status == 2){
                                                                                                $status = true;

                                                                                                foreach($sst->variationOrders as $voL){
                                                                                                    if($voL->status == 2){
                                                                                                        foreach($voL->vo_elements as $voE){
                                                                                                            if($voE->status == 1){
                                                                                                                foreach($voE->items as $voI){
                                                                                                                    $totalUsed = $totalUsed + $voI->subItems()->where('wps_status', 1)->where('vo_active_id', $active->id)->pluck('net_amount')->sum();
                                                                                                                }
                                                                                                                
                                                                                                            }
                                                                                                        }
                                                                                                        
                                                                                                    }else{
                                                                                                        foreach($voL->vo_elements as $voE){
                                                                                                            foreach($voE->items as $voI){
                                                                                                                $totalPending = $totalPending + $voI->subItems()->where('wps_status', 1)->where('vo_active_id', $active->id)->pluck('net_amount')->sum();
                                                                                                            }
                                                                                                        }                                                                                                        
                                                                                                    }                                                                                                    
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        $totalUsedAll = $totalUsed + $totalPending;
                                                                                        $balanceAmount = $balanceAmount - ($totalUsed + $totalPending);
                                                                                    @endphp


                                                                                    <h6>@if($status) Aktif @else Tidak Aktif @endif</h6>
                                                                                </td>
                                                                                <td style="border:none;">
                                                                                    {{ money()->toCommon($currAmount ?? "0", 2) }}
                                                                                </td>
                                                                                <td style="border:none;">
                                                                                    {{money()->toHuman($totalUsedAll ?? 0,2 )}}
                                                                                </td>
                                                                                <td style="border:none;">
                                                                                    {{money()->toHuman($balanceAmount ?? 0,2 )}}
                                                                                    @include('components.forms.hidden', [
                                                                                        'id' => 'balance-'.$it->id,
                                                                                        'name' => 'balance_wps[' . kebab_case($it->id) . ']',
                                                                                        'value' => $balanceAmount,
                                                                                    ])

                                                                                </td>
                                                                                <td style="border:none;">
                                                                                    @if($balanceAmount == 0)
                                                                                        @include('components.forms.checkbox', [
                                                                                            'name' => 'select_cancel_wps[' . kebab_case($it->id) . ']',
                                                                                            'id' => 'cancel-'.$it->id,
                                                                                            'label' => '',
                                                                                            'value' => $it->id,
                                                                                            'disabled' => true,
                                                                                        ])
                                                                                    @else
                                                                                        @include('components.forms.checkbox', [
                                                                                            'name' => 'select_cancel_wps[' . kebab_case($it->id) . ']',
                                                                                            'id' => 'cancel-'.$it->id,
                                                                                            'label' => '',
                                                                                            'value' => $it->id,
                                                                                        ])
                                                                                    @endif
                                                                                    {{--  <input type="checkbox" id="cancel-wps-{{$it->id}}" name="cancelWps[{{$it->id}}]['status']" 
                                                                                        value="{{ $it->id }}" />  --}}
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {{--  @endif  --}}
                                {{--  @endforeach  --}}
                            </div>
                        @endif
                        <br>
                       
                        <div class="btn-group float-right">
                            <button type="submit" class="btn btn-primary submit-cancel-wps-action-btn">
                                @icon('fe fe-save') {{ __('Simpan') }}
                            </button>
                        </div>
                        <a href="{{ route('contract.post.variation-order.ppk_edit', $vo->hashslug) }}" 
                            class="float-right btn btn-default border-primary">
                            {{ __('Kembali') }}
                        </a>                          
                    </form>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection
