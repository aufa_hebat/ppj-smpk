@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

        });
    </script>
@endpush
@section('content')
    {{--  @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
        @endslot
    @endcomponent  --}}
    @include('components.pages.title')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk" role="tab" aria-controls="ppk" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('PPK') }}
                    </a>
                </li>              
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'variation-order'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'ppk', 'active' => true])
                                @slot('content')
                                    <input type="text" name="sst_id" id="sst_id" value="{{ $sst->id }}" hidden/>
                                    @include('contract.post.variation-order.manage.ppk',['sst' => $sst])
                                @endslot
                            @endcomponent                                                   
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection