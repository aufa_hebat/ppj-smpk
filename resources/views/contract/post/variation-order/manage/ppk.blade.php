
<div class="row justify-content-center">
    <div class="col">
            
        @include('contract.post.variation-order.partials.scripts-ppk')
            @component('components.card',['card_classes' => 'col-12'])
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id'   => 'contract-post-variation-order-ppk',
                            'route_name' => 'api.datatable.contract.variation-order-result-ppk',
                            'param'     => 'hashslug=' . $sst->hashslug,
                            'columns' => [
                                ['data' => 'running_no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                ['data' => 'ppk_no', 'title' => __('No PPK'), 'defaultContent' => '-'],
                                ['data' => 'no_kontrak', 'title' => __('No Rujukan'), 'defaultContent' => '-'],
                                ['data' => 'status_detail', 'title' => __('Status'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('Bil'),__('No PPK'),__('No Rujukan'),__('Status'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.variation-order.manage.partials.actions-ppk')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>