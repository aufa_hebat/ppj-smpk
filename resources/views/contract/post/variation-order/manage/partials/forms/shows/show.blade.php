@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            $('#mtg_no').val(' {{ $vo->mtg_no }} ').prop('disabled',true);
            $('#mtg_year').val(' {{ $vo->mtg_year }} ').prop('disabled',true);
            $('#ppk_kod_1').val(' {{ $vo->ppk_kod_1 }} ').prop('disabled',true);
            $('#ppk_kod_2').val(' {{ $vo->ppk_kod_2 }} ').prop('disabled',true);
            $('#ppk_kod_3').val(' {{ $vo->ppk_kod_3 }} ').prop('disabled',true);

            // UPLOADS
            @if(((!empty($vo->documents)) && $vo->documents->count() > 0) || ((!empty($vo->doc_ppk)) && $vo->doc_ppk->count() > 0) || ((!empty($vo->doc_ppk_result)) && $vo->doc_ppk_result->count() > 0))
                var vo_t = $('#vo_tblUpload').DataTable({
                    searching: false,
                    ordering: false,
                    paging: false,
                    info:false
                });
    
                var vo_counter = 1;
                @foreach($vo->documents as $doc)
                    vo_t.row.add( [
                        vo_counter,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );
                
                    vo_counter++;
                @endforeach
                
                @if($vo->doc_ppk)
                    vo_t.row.add( [
                        vo_counter,
                        '<a href="/download/{{ $vo->doc_ppk->document_path .'/'.$vo->doc_ppk->document_name }}" target="_blank"> {{ $vo->doc_ppk->document_name }}</a>'
                    ] ).draw( false );

                    vo_counter++;
                @endif
            
                @if($vo->doc_ppk_result)
                    vo_t.row.add( [
                        vo_counter,
                        '<a href="/download/{{ $vo->doc_ppk_result->document_path .'/'.$vo->doc_ppk_result->document_name }}" target="_blank"> {{ $vo->doc_ppk_result->document_name }}</a>'
                    ] ).draw( false );

                    vo_counter++;
                @endif                
            @endif
            // UPLOADS END

        });
    </script>
@endpush    
@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="vo-tab-content" role="tablist">   
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#ppk-meeting" role="tab" aria-controls="ppk-meeting" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Maklumat') }}
                    </a>
                </li>                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-result" role="tab" aria-controls="ppk-result" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Keputusan') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-ajk" role="tab" aria-controls="ppk-ajk" aria-selected="false">
                        @icon('fe fe-user')&nbsp;{{ __('Jawatankuasa') }}
                    </a>
                </li>                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-doc" role="tab" aria-controls="ppk-doc" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Dokumen') }}
                    </a>
                </li>                 
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'document'])
                        @slot('tabs') 
                            <div>
                                <div class="btn-group float-right">
                                    <a href="{{ route('vo_ppk_result',['hashslug' => $vo->hashslug]) }}" 
                                        target="_blank" 
                                        class="btn btn-success border-success">
                                        @icon('fe fe-printer') {{ __(' Cetak') }}
                                    </a>
                                </div>
                            </div>                        
                            @component('components.tab.content', ['id' => 'ppk-meeting', 'active' => true]) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.shows.ppk_meeting', ['vo' => $vo])
                                @endslot
                            @endcomponent                        
                            @component('components.tab.content', ['id' => 'ppk-result']) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.shows.ppk_result', ['vo' => $vo])
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'ppk-ajk']) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.shows.ppk_approver', ['vo' => $vo])
                                @endslot
                            @endcomponent   
                            @component('components.tab.content', ['id' => 'ppk-doc']) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.shows.documents', ['vo' => $vo])
                                    <br/><br/>
                                    @role('urusetiavo')
                                        @if($vo->status == 1)
                                            @include('contract.post.variation-order.manage.partials.forms.shows.upload', ['vo' => $vo])
                                        @else                                        
                                            <div class="row">
                                                <div class="col-12" style="text-align:right;">
                                                    <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
                                                        class="btn btn-default border-primary">
                                                        {{ __('Kembali') }}
                                                    </a>  
                                                </div>
                                            </div>
                                        @endif
                                    @endrole
                                @endslot
                            @endcomponent                                                      
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection