{{--  @extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $(".addmore").on('click',function(){
                var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
                    
                $klon.find('#appointment_date_'+currnum).attr('id','appointment_date_'+num).attr('name','baru['+num+'][appointment_date]').val('');
                $klon.find('#name_'+currnum).attr('id','name_'+num).attr('name','baru['+num+'][name]').val('');
                $klon.find('#role_'+currnum).attr('id','role_'+num).attr('name','baru['+num+'][role]').val('');
                $('.addmore').parents('table').append($klon);
            });
            
            $('.buang').click(function () {
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        $(this).parents('tr').detach();
                    }
                });
            });

            $(document).on('click', '.submit-approver-ppk-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $vo->id }}';
                var hashslug = '{{ $vo->hashslug }}';
                var route_name = 'api.contract.ppk-approver';
                var form_id = 'approver_ppk_form';
                var data = $('#' + form_id).serialize();
    
                console.log(data);

                axios.post(route(route_name, id), data).then(response => {
                    swal('Jawatankuasa Perubahan Kerja', response.data.message, 'success');
                    redirect(route('contract.post.variation-order-result.ppk_amend', hashslug));
                });
            });
        });
    </script>
@endpush
@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')  --}}
                    <div class="row">
                        <div class="col">
                            <h4>Jawatankuasa Perubahan Kerja (P.K)</h4>
                            <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
                        </div>
                    </div>
                    <form id="approver_ppk_form">
                        <div class="row">
                            <div class="col">
                                <input type="hidden" name="sst_id" id="sst_id" value="{{ $vo->sst_id }}">
                                <input type="hidden" name="vo_id" id="vo_id" value="{{ $vo->id }}">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="table-responsive">
                                            <div id="tableJKPembuka" class="table-editable table-bordered ">
                                                <table class="table">
                                                    <tr>
                                                        <th width="30%"><b>Ahli Jawatankuasa Perubahan Kerja</b></th>
                                                        <th width="10%"><b>Peranan</b></th>
                                                        <th width="5%"><b><span class="fe fe-plus addmore"></span></b></th>
                                                    </tr>
                                                    @if(!empty($officer) && $officer->count() > 0)
                                                        @foreach($officer as $jk)
                                                        <tr>
                                                            <td>
                                                                <input type="hidden" name="exist[{{$jk->id}}][id]" value="{{$jk->id}}">
                                                                <span>{{$jk->user->name}}</span>
                                                            </td>
                                                            <td><span>{{$jk->role}}</span></td>
                                                            <td>
                                                                <b><span class="fe fe-minus buang"></span></b></span>
                                                            </td>
                                                        </tr>
                                                        @endforeach                          
                                                    @endif
                                                    <tr id="klon0" class="d-none">
                                                        <td>
                                                            <select class="form-control input-lg" id="name_0" name="baru[0][name]" style="width: 100%;">
                                                                <option value=""></option>
                                                                @foreach($user as $users)
                                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control input-lg" id="role_0" name="baru[0][role]" style="width: 100%;">
                                                                <option value="ahli">Ahli</option>
                                                                <option value="pengerusi">Pengerusi</option>
                                                                <option value="pengerusi ganti">Pengerusi Ganti</option>
                                                                
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <b><span class="fe fe-minus buang"></b></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-group float-right">
                                    <button type="submit" class="btn btn-primary submit-approver-ppk-action-btn">
                                        @icon('fe fe-save') {{ __('Simpan') }}
                                    </button>
                                </div>
                                <a href="{{ route('contract.post.variation-order-result.ppk_amend', $vo->hashslug) }}" 
                                    class="float-right btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>     
                            </div>
                        </div>
                    </form>
                {{--  @endslot
            @endcomponent
        </div>
    </div>
@endsection  --}}