@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
    <script>
        jQuery(document).ready(function($) {
            // MAKLUMAT MEETING
            $('#mtg_no').val('{{ $vo->mtg_no }}');
            $('#mtg_year').val('{{ $vo->mtg_year }}');
            $('#ppk_kod_1').val('{{ $vo->ppk_kod_1 }}');
            $('#ppk_kod_2').val('{{ $vo->ppk_kod_2 }}');
            $('#ppk_kod_3').val('{{ $vo->ppk_kod_3 }}');
            // MAKLUMAT MEETING END

            // KEPUTUSAN MESYUARAT
            @if(count($vo->actives) > 0)    
                @foreach($vo->actives as $active)
                    @if($active->status == '1')
                        $('#active-{{ $active->id }}').prop('checked', true);
                    @endif
                    $('#ulasan-{{ $active->id }}').val(' {{ str_replace(["\r\n", "\r", "\n"], "\\n", $active->comment) }} '); //str_replace(["\r\n","\r","\n"],"\\n ",$owner->addresses[0]->primary)
                @endforeach
            @endif

            @if(count($vo->cancels) > 0)    
                @foreach($vo->cancels as $cancel)
                    @if($cancel->status == '1')
                        $('#batal-{{ $cancel->id }}').prop('checked', true);
                    @endif
                    $('#ulasanBatal-{{ $cancel->id }}').val(' {{ str_replace(["\r\n", "\r", "\n"], "\\n", $cancel->comment) }} ');
                @endforeach
            @endif

            @if(count($PPKElemen) > 0)
                @foreach($PPKElemen as $elemen)
                    @if($elemen->status == '1')
                        $('#elemen-{{ $elemen->id }}').prop('checked', true);
                    @endif
                    $('#elemenUlasan-{{ $elemen->id }}').val(' {{ str_replace(["\r\n","\r","\n"], "\\n", $elemen->comment) }} ');
                @endforeach
            @endif
            // KEPUTUSAN MESYUARAT END

            // JAWATANKUASA MESYUARAT
            $(".addmore").on('click',function(){
                var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
                    
                $klon.find('#appointment_date_'+currnum).attr('id','appointment_date_'+num).attr('name','baru['+num+'][appointment_date]').val('');
                $klon.find('#name_'+currnum).attr('id','name_'+num).attr('name','baru['+num+'][name]').val('');
                $klon.find('#role_'+currnum).attr('id','role_'+num).attr('name','baru['+num+'][role]').val('');
                $('.addmore').parents('table').append($klon);
            });
            
            $('.buang').click(function () {
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        $(this).parents('tr').detach();
                    }
                });
            });
            // JAWATANKUASA MESYUARAT END


            // UPLOADS
            @if(((!empty($vo->documents)) && $vo->documents->count() > 0) || ((!empty($vo->doc_ppk)) && $vo->doc_ppk->count() > 0)  || ((!empty($vo->doc_ppk_result)) && $vo->doc_ppk_result->count() > 0))
                var vo_t = $('#vo_tblUpload').DataTable({
                    searching: false,
                    ordering: false,
                    paging: false,
                    info:false
                });
    
                var vo_counter = 1;
                @foreach($vo->documents as $doc)
                    vo_t.row.add( [
                        vo_counter,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );
                
                    vo_counter++;
                @endforeach
                
                @if($vo->doc_ppk)
                    vo_t.row.add( [
                        vo_counter,
                        '<a href="/download/{{ $vo->doc_ppk->document_path .'/'.$vo->doc_ppk->document_name }}" target="_blank"> {{ $vo->doc_ppk->document_name }}</a>'
                    ] ).draw( false );

                    vo_counter++;
                @endif
            
                @if($vo->doc_ppk_result)
                    vo_t.row.add( [
                        vo_counter,
                        '<a href="/download/{{ $vo->doc_ppk_result->document_path .'/'.$vo->doc_ppk_result->document_name }}" target="_blank"> {{ $vo->doc_ppk_result->document_name }}</a>'
                    ] ).draw( false );

                    vo_counter++;
                @endif                
            @endif
            // UPLOADS END


            // UPDATE MAKLUMAT MEETING
            $(document).on('click', '#vo-mtg-action', function(event) {
                var id = '{{ $vo->id }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = null;
                            
                $("#vo-mtg-form").validate({
                    rules: {
                        mtg_no: "required",
                        mtg_year: "required"
                    }, 
                    messages: {
                        mtg_no: { required: "* required" },
                        mtg_year: { required: "* required" }
                    },
                    submitHandler: function (e){
                        event.preventDefault();

                        data = $('#' + form_id).serialize();
                        
                        axios.put(route(route_name, id), data).then( (response) => {
                            swal('Perubahan Kerja', response.data.message, 'success');
                            location.reload();
                        }).catch((error)=>{
                            console.log(error.response);
                        });
                    }
                });
            });

            // UPDATE KEPUTUSAN MEETING
            $(document).on('click', '.submit-result-active-wps-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $vo->sst->id }}';
                var hashslug = '{{ $vo->hashslug }}';
                var route_name = 'api.contract.active-wps-result';
                var form_id = 'result_ppk_form';
                var data = $('#' + form_id).serialize();
    
                console.log(data);
                axios.post(route(route_name, id), data).then(response => {
                    swal('Keputusan Permohonan Perubahan Kerja', response.data.message, 'success');
                    redirect(route('contract.post.variation-order-result.ppk_amend', hashslug));
                });
            });

            // UPDATE JAWATANKUASA MEETING
            $(document).on('click', '.submit-approver-ppk-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $vo->id }}';
                var hashslug = '{{ $vo->hashslug }}';
                var route_name = 'api.contract.ppk-approver';
                var form_id = 'approver_ppk_form';
                var data = $('#' + form_id).serialize();
    
                console.log(data);

                axios.post(route(route_name, id), data).then(response => {
                    swal('Jawatankuasa Perubahan Kerja', response.data.message, 'success');
                    redirect(route('contract.post.variation-order-result.ppk_amend', hashslug));
                });
            });

        });
    </script>
@endpush    
@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="vo-tab-content" role="tablist">   
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#ppk-mtg" role="tab" aria-controls="ppk-mtg" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Maklumat Mesyuarat') }}
                    </a>
                </li>                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-result" role="tab" aria-controls="ppk-result" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Keputusan Mesyuarat') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-ajk" role="tab" aria-controls="ppk-ajk" aria-selected="false">
                        @icon('fe fe-user')&nbsp;{{ __('Jawatankuasa Mesyuarat') }}
                    </a>
                </li>                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ppk-doc" role="tab" aria-controls="ppk-doc" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Dokumen') }}
                    </a>
                </li>                 
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'document'])
                        @slot('tabs') 
                            <div>
                                <div class="btn-group float-right">
                                    <a href="{{ route('vo_ppk_result',['hashslug' => $vo->hashslug]) }}" 
                                        target="_blank" 
                                        class="btn btn-success border-success">
                                        @icon('fe fe-printer') {{ __(' Cetak') }}
                                    </a>
                                </div>
                            </div>
                            @component('components.tab.content', ['id' => 'ppk-mtg', 'active' => true]) 
                                @slot('content')      
                                    <form id="vo-mtg-form" method="POST">
                                        @csrf
                                        @method('PUT') 
        
                                        <input type="hidden" name="sst_id" id="sst_id" value="{{ $vo->sst_id }}">
                                        <input type="hidden" name="vo_id" id="vo_id" value="{{ $vo->id }}">
                                        @include('contract.post.variation-order.manage.partials.forms.amend.ppk_meeting', ['vo' => $vo])
                                    </form>
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'ppk-result']) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.amend.ppk_result', ['vo' => $vo])
                                @endslot
                            @endcomponent                            
                            @component('components.tab.content', ['id' => 'ppk-ajk']) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.amend.ppk_approver', ['vo' => $vo])
                                @endslot
                            @endcomponent   
                            @component('components.tab.content', ['id' => 'ppk-doc']) 
                                @slot('content')      
                                    @include('contract.post.variation-order.manage.partials.forms.shows.documents', ['vo' => $vo])
                                    <br/><br/>       
                                    @role('urusetiavo')
                                        @if($vo->status == 1)
                                            @include('contract.post.variation-order.manage.partials.forms.shows.upload', ['vo' => $vo])
                                        @else                                        
                                            <div class="row">
                                                <div class="col-12" style="text-align:right;">
                                                    <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
                                                        class="btn btn-default border-primary">
                                                        {{ __('Kembali') }}
                                                    </a>  
                                                </div>
                                            </div>
                                        @endif
                                    @endrole                                                                          
                                @endslot
                            @endcomponent                                                      
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection