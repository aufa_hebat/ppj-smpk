<div class="row">
    <div class="col">
        <h4>Maklumat Mesyuarat</h4>
        <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col">
                Mesyuarat Permohonan Perubahan Kerja Bil.
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                @include('components.forms.input', [
                    'input_label' => __(''),
                    'id' => 'mtg_no',
                    'name' => 'mtg_no',
                    'placeholder' => __('No')
                ])
            </div>
            <div class="col-3">
                @include('components.forms.input', [
                    'input_label' => __(''),
                    'id' => 'mtg_year',
                    'name' => 'mtg_year',
                    'placeholder' => __('Year')
                ])
            </div>
            <div class="col-7">
            </div>            
        </div>
    </div>      
</div>
<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col">
                Kod Permohonan
            </div>
        </div>        
        <div class="row">
            <div class="col-2">
                @include('components.forms.input', [
                    'input_label' => __(''),
                    'id' => 'ppk_kod_1',
                    'name' => 'ppk_kod_1',
                    'placeholder' => __('Kod 1')
                ])
            </div>
            <div class="col-1">
                /
            </div>
            <div class="col-2">
                @include('components.forms.input', [
                    'input_label' => __(''),
                    'id' => 'ppk_kod_2',
                    'name' => 'ppk_kod_2',
                    'placeholder' => __('Kod 2')
                ])
            </div> 
            <div class="col-3">
                @include('components.forms.input', [
                    'input_label' => __(''),
                    'id' => 'ppk_kod_3',
                    'name' => 'ppk_kod_3',
                    'placeholder' => __('Kod 3')
                ])
            </div>  
            <div class="col-4">
            </div>                     
        </div>
    </div>  
</div>

<div class="btn-group float-right">       
    <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
        class="float-right btn btn-default border-primary">
        {{ __('Kembali') }}
    </a> 
    <button type="submit" class="btn btn-primary float-right" id="vo-mtg-action"
        data-route="api.contract.ppk-meeting"
        data-form="vo-mtg-form">
        @icon('fe fe-save') {{ __('Simpan') }}
    </button>     
</div>
