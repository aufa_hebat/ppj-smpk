@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush
@section('content')
    {{--  @include('contract.post.variation-order.partials.title')  --}}
    @include('components.pages.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    <div class="row">
                        <div class="col">
                            <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <h4>Keputusan Jawatankuasa Perubahan Kerja (P.K)</h4>
                        </div>
                        <div class="col-4">
                            <div class="btn-group float-right">
                                <a href="{{ route('vo_ppk',['hashslug' => $vo->hashslug]) }}" 
                                    target="_blank" 
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer') {{ __(' Cetak') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            Keputusan Permohonan Perubahan Kerja 
                            <a href="{{ route('contract.post.variation-order-result.show-ppk-amend-result',['hashslug' => $vo->hashslug]) }}">
                                @icon('fe fe-edit')
                            </a>
                            <br/>
                            Jawatankuasa Perubahan Kerja
                            <a href="{{ route('contract.post.variation-order-result.show-ppk-amend-approver',['hashslug' => $vo->hashslug]) }}">
                                @icon('fe fe-edit')
                            </a>

                            <br/><br/>
                            @include('contract.post.variation-order.manage.partials.forms.shows.upload', ['vo' => $vo])
                            <br/><br/>    
                        </div>
                    </div>

                @endslot
            @endcomponent
        </div>
    </div>
@endsection