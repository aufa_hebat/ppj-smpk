@push('scripts')
@include('components.forms.assets.datetimepicker')
@include('components.forms.assets.select2')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
<script>
    jQuery(document).ready(function($) {

        $('#uploadFile').change(function(){
            $('#subFile').val($(this).val().split('\\').pop());
        });

        @if(!empty($vo) && !empty($vo->doc_ppk_result))
            $('#subFile').val('{{ $vo->doc_ppk_result->document_name }}');
        @endif

        $(document).on('click', '.save-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $vo->id ?? ''}}';
            var route_name = 'api.contract.upload-ppk-result';
            var form_id = 'ppk_form';
            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', '');

            // Display the key/value pairs
            /*for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }*/

            axios.post(route(route_name, id), data).then(response => {
                swal('', response.data.message, 'success');
                location.reload();
            });
        });

        $(document).on('click', '.submit-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $vo->id ?? ''}}';
            var hashslug = '{{ $vo->hashslug ?? ''}}';
            var route_name = 'api.contract.upload-ppk-result';
            var form_id = 'ppk_form';
            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', 'submit');

            // Display the key/value pairs
            /*for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }*/

            axios.post(route(route_name, id), data).then(response => {
                swal('', response.data.message, 'success');
                redirect(route('contract.post.variation-order-result.ppk_amend_show', hashslug));
            });
        });
    });
</script>
@endpush

<h4>Muat Naik Dokumen Keputusan PPK</h4>
<div class="row">
<div class="col-12">
    <form id="ppk_form" files = "true" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="form-group row">
            <label for="Fail_Keputusan_Mesyuarat"
               class="col col-form-label">
                Muat Naik Dokumen Keputusan PPK
            </label>

            <div class="col input-group">
                <input id="subFile" type="text"  class="form-control" readonly>
                <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                @if(!empty($vo) && !empty($vo->doc_ppk_result))
                    <label class="input-group-text" for="downloadFile">
                        <a href="/download/{{$vo->doc_ppk_result->document_path}}/{{ $vo->doc_ppk_result->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                    </label>
                @endif
            </div>
            
        </div>

        <div class="row">
            <div class="col-12" style="text-align:right;">
                <div class="btn-group float-right">
                    <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
                        class="btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>                 
                    <button type="submit" class="btn btn-primary save-action-btn">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>  
                    <button type="submit" class="btn btn-success float-middle border-default submit-action-btn">
                        @icon('fe fe-send') {{ __('Selesai') }}
                    </button>   
                </div>              
            </div>
        </div>
    </form>
</div>
</div>