{{--  @extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(count($vo->actives) > 0)    
                @foreach($vo->actives as $active)
                    @if($active->status == '1')
                        $('#active-{{ $active->id }}').prop('checked', true);
                    @endif
                    $('#ulasan-{{ $active->id }}').val(' {{ $active->comment }} ');
                @endforeach
            @endif

            @if(count($vo->cancels) > 0)    
                @foreach($vo->cancels as $cancel)
                    @if($cancel->status == '1')
                        $('#batal-{{ $cancel->id }}').prop('checked', true);
                    @endif
                    $('#ulasanBatal-{{ $cancel->id }}').val(' {{ $cancel->comment }} ');
                @endforeach
            @endif

            @if(count($PPKElemen) > 0)
                @foreach($PPKElemen as $elemen)
                    @if($elemen->status == '1')
                        $('#elemen-{{ $elemen->id }}').prop('checked', true);
                    @endif
                    $('#elemenUlasan-{{ $elemen->id }}').val(' {{ $elemen->comment }} ');
                @endforeach
            @endif

        $(document).on('click', '.submit-result-active-wps-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $vo->sst->id }}';
            var hashslug = '{{ $vo->hashslug }}';
            var route_name = 'api.contract.active-wps-result';
            var form_id = 'result_ppk_form';
            var data = $('#' + form_id).serialize();

            console.log(data);
            axios.post(route(route_name, id), data).then(response => {
                swal('Keputusan Permohonan Perubahan Kerja', response.data.message, 'success');
                redirect(route('contract.post.variation-order-result.ppk_amend', hashslug));
            });
        });
        });
    </script>
@endpush
@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')  --}}
                    <div class="row">
                        <div class="col">
                            <h4>Keputusan Mesyuarat</h4>
                            <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
                        </div>
                    </div>
                    <form id="result_ppk_form">
                        <div class="row">
                            <div class="col">
                                <input type="hidden" name="sst_id" id="sst_id" value="{{ $vo->sst_id }}">
                                <input type="hidden" name="vo_id" id="vo_id" value="{{ $vo->id }}">

                                @foreach($types as $type )
                                
                                    <div class="form-inline">
                                        @foreach($vo->types as $vt)
                                            @if($type->id == $vt->variation_order_type_id)
                                                @if($vt->variation_order_type_id == 1)
                                                    <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                    <table class="table table-sm table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <td width="5%">Bil.</td>
                                                                <td width="40%">Item</td>
                                                                <td width="15%">Nilai (RM)</td>
                                                                <td width="7%">Keputusan</td>
                                                                <td width="32%">Ulasan</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($vo->actives as $key => $active)
                                                                <tr>
                                                                    <td>{{ $key + 1 }}</td>
                                                                    <td>{{ $active->item->item }}</td>
                                                                    <td>{{ money()->toCommon($active->item->amount ?? "0", 2) }}</td>
                                                                    <td>
                                                                        @include('components.forms.checkbox', [
                                                                            'name' => 'result[' . kebab_case($active->id) . ']',
                                                                            'id' => 'active-'.$active->id,
                                                                            'label' => '',
                                                                            'value' => $active->id,
                                                                        ])
                                                                    </td>
                                                                    <td>
                                                                        @include('components.forms.textarea', [
                                                                            'input_label' => '',
                                                                            'id' => 'ulasan-'.$active->id,
                                                                            'name' => 'ulasan[' . kebab_case($active->id) .']',
                                                                            'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                        ])
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($vt->variation_order_type_id == 2)
                                                    <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                    <table class="table table-sm table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <td width="5%">Bil.</td>
                                                                <td width="45%">Elemen</td>
                                                                <td width="10%">Omission (RM)</td>
                                                                <td width="10%">Addition(RM)</td>
                                                                <td width="7%">Keputusan</td>
                                                                <td width="23%">Ulasan</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $index = 0;
                                                            @endphp
                                                            @foreach($PPKElemen as $key => $elemen)
                                                                @if($elemen->vo_type_id == $vt->id)

                                                                    <tr>
                                                                        <td>{{ $index = $index + 1 }}</td>
                                                                        <td>{{ $elemen->element }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_omission ?? "0", 2) }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>
                                                                        <td>
                                                                            @include('components.forms.checkbox', [
                                                                                'name' => 'elemen[' . kebab_case($elemen->id) . ']',
                                                                                'id' => 'elemen-'.$elemen->id,
                                                                                'label' => '',
                                                                                'value' => $elemen->id,
                                                                                'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                            ])
                                                                        </td>
                                                                        <td>
                                                                            @include('components.forms.textarea', [
                                                                                'input_label' => '',
                                                                                'id' => 'elemenUlasan-'.$elemen->id,
                                                                                'name' => 'elemenUlasan[' . kebab_case($elemen->id) .']',
                                                                                'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                            ])
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($vt->variation_order_type_id == 3)
                                                    <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                    <table class="table table-sm table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <td width="5%">Bil.</td>
                                                                <td width="45%">Elemen</td>
                                                                <td width="10%">Addition(RM)</td>
                                                                <td width="7%">Keputusan</td>
                                                                <td width="23%">Ulasan</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $index = 0;
                                                            @endphp
                                                            @foreach($PPKElemen as $key => $elemen)
                                                                @if($elemen->vo_type_id == $vt->id)

                                                                    <tr>
                                                                        <td>{{ $index = $index + 1 }}</td>
                                                                        <td>{{ $elemen->element }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>
                                                                        <td>
                                                                            @include('components.forms.checkbox', [
                                                                                'name' => 'elemen[' . kebab_case($elemen->id) . ']',
                                                                                'id' => 'elemen-'.$elemen->id,
                                                                                'label' => '',
                                                                                'value' => $elemen->id,
                                                                            ])
                                                                        </td>
                                                                        <td>
                                                                            @include('components.forms.textarea', [
                                                                                'input_label' => '',
                                                                                'id' => 'elemenUlasan-'.$elemen->id,
                                                                                'name' => 'elemenUlasan[' . kebab_case($elemen->id) .']',
                                                                                'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                            ])
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($vt->variation_order_type_id == 4)
                                                    <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                    <table class="table table-sm table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <td width="5%">Bil.</td>
                                                                <td width="45%">Elemen</td>
                                                                <td width="10%">Omission (RM)</td>
                                                                <td width="10%">Addition(RM)</td>
                                                                <td width="7%">Keputusan</td>
                                                                <td width="23%">Ulasan</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $index = 0;
                                                            @endphp
                                                            @foreach($PPKElemen as $key => $elemen)
                                                                @if($elemen->vo_type_id == $vt->id)

                                                                    <tr>
                                                                        <td>{{ $index = $index + 1 }}</td>
                                                                        <td>{{ $elemen->element }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_omission ?? "0", 2) }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>                                                                        
                                                                        <td>
                                                                            @include('components.forms.checkbox', [
                                                                                'name' => 'elemen[' . kebab_case($elemen->id) . ']',
                                                                                'id' => 'elemen-'.$elemen->id,
                                                                                'label' => '',
                                                                                'value' => $elemen->id,
                                                                            ])
                                                                        </td>
                                                                        <td>
                                                                            @include('components.forms.textarea', [
                                                                                'input_label' => '',
                                                                                'id' => 'elemenUlasan-'.$elemen->id,
                                                                                'name' => 'elemenUlasan[' . kebab_case($elemen->id) .']',
                                                                                'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                            ])
                                                                        </td>                                                                        
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($vt->variation_order_type_id == 5)
                                                    <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                    <table class="table table-sm table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <td width="5%">Bil.</td>
                                                                <td width="45%">Elemen</td>
                                                                <td width="10%">Omission (RM)</td>
                                                                <td width="10%">Addition(RM)</td>
                                                                <td width="7%">Keputusan</td>
                                                                <td width="23%">Ulasan</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $index = 0;
                                                            @endphp
                                                            @foreach($PPKElemen as $key => $elemen)
                                                                @if($elemen->vo_type_id == $vt->id)

                                                                    <tr>
                                                                        <td>{{ $index = $index + 1 }}</td>
                                                                        <td>{{ $elemen->element }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_omission ?? "0", 2) }}</td>
                                                                        <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>                                                                        
                                                                        <td>
                                                                            @include('components.forms.checkbox', [
                                                                                'name' => 'elemen[' . kebab_case($elemen->id) . ']',
                                                                                'id' => 'elemen-'.$elemen->id,
                                                                                'label' => '',
                                                                                'value' => $elemen->id,
                                                                            ])
                                                                        </td>
                                                                        <td>
                                                                            @include('components.forms.textarea', [
                                                                                'input_label' => '',
                                                                                'id' => 'elemenUlasan-'.$elemen->id,
                                                                                'name' => 'elemenUlasan[' . kebab_case($elemen->id) .']',
                                                                                'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                            ])
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($vt->variation_order_type_id == 6)
                                                    <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                    <table class="table table-sm table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <td width="5%">Bil.</td>
                                                                <td width="45%">Item</td>
                                                                <td width="20%">Nilai (RM)</td>
                                                                <td width="7%">Keputusan</td>
                                                                <td width="23%">Ulasan</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($vo->cancels as $key => $cancel)
                                                                <tr>
                                                                    <td>{{ $key + 1 }}</td>
                                                                    <td>{{ $cancel->item->item }}</td>
                                                                    <td>{{ money()->toCommon($cancel->amount ?? "0", 2) }}</td>
                                                                    <td>
                                                                        @include('components.forms.checkbox', [
                                                                            'name' => 'batal[' . kebab_case($cancel->id) . ']',
                                                                            'id' => 'batal-'.$cancel->id,
                                                                            'label' => '',
                                                                            'value' => $cancel->id,
                                                                        ])
                                                                    </td>
                                                                    <td>
                                                                        @include('components.forms.textarea', [
                                                                            'input_label' => '',
                                                                            'id' => 'ulasanBatal-'.$cancel->id,
                                                                            'name' => 'ulasanBatal[' . kebab_case($cancel->id) .']',
                                                                            'style' => 'height:70px; width:300px; border: 1px solid black',
                                                                        ])
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>
                                @endforeach
                                <div class="row">
                                    <div class="col">
                                        <p><span style="font-size:12px;font-style:italic">** Sila tick keputusan yang lulus</span></p>
                                    </div>
                                </div>
                                <div class="btn-group float-right">
                                    <button type="submit" class="float-right btn btn-primary submit-result-active-wps-action-btn">
                                        @icon('fe fe-save') {{ __('Simpan') }}
                                    </button>
                                </div>
                                <a href="{{ route('contract.post.variation-order-result.ppk_amend', $vo->hashslug) }}" 
                                    class="float-right btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>     
                            </div>
                        </div>
                    </form>
                {{--  @endslot
            @endcomponent
        </div>
    </div>
@endsection  --}}