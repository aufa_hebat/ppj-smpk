@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $('#mtg_no').val(' {{ $vo->mtg_no }} ');
            $('#mtg_year').val(' {{ $vo->mtg_year }} ');
            $('#ppk_kod_1').val(' {{ $vo->ppk_kod_1 }} ');
            $('#ppk_kod_2').val(' {{ $vo->ppk_kod_2 }} ');
            $('#ppk_kod_3').val(' {{ $vo->ppk_kod_3 }} ');

            $(document).on('click', '#vo-mtg-action', function(event) {
                event.preventDefault();

                var id = '{{ $vo->id }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                //var form = document.forms[form_id];
                var data = $('#' + form_id).serialize();

                console.log(data);
                axios.put(route(route_name, id), data).then( (response) => {
                    swal('', response.data.message, 'success');
                    location.reload();
                }).catch((error)=>{
                    console.log(error.response);
                });
            });
        });
    </script>
@endpush
@section('content')
    {{--  @include('contract.post.variation-order.partials.title')  --}}
    @include('components.pages.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    <div class="row">
                        <div class="col">
                            <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-8">
                            <h4>Keputusan Jawatankuasa Perubahan Kerja (P.K)</h4>
                        </div>
                        <div class="col-4">
                            <div class="btn-group float-right">
                                <a href="{{ route('vo_ppk',['hashslug' => $vo->hashslug]) }}" 
                                    target="_blank" 
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer') {{ __(' Cetak') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <form id="vo-mtg-form" method="POST">
                                @csrf
                                @method('PUT') 

                                <input type="hidden" name="sst_id" id="sst_id" value="{{ $vo->sst_id }}">
                                <input type="hidden" name="vo_id" id="vo_id" value="{{ $vo->id }}">
                                Keputusan Permohonan Perubahan Kerja 
                                <a href="{{ route('contract.post.variation-order-result.ppk_amend_result',['hashslug' => $vo->hashslug]) }}">
                                    @icon('fe fe-edit')
                                </a>
                                <br/>
                                Jawatankuasa Perubahan Kerja
                                <a href="{{ route('contract.post.variation-order-result.ppk_amend_approver',['hashslug' => $vo->hashslug]) }}">
                                    @icon('fe fe-edit')
                                </a>
                                <br/><br/>
                                @include('contract.post.variation-order.manage.partials.forms.amend.ppk_meeting', ['vo' => $vo])
                            </form>

                            <div class="btn-group float-right"> 
                                <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
                                    class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>                                
                                <button type="submit" class="btn btn-primary" id="vo-mtg-action"
                                    data-route="api.contract.ppk-meeting"
                                    data-form="vo-mtg-form">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                                </button>
                            </div>                                
                        </div>
                    </div>

                @endslot
            @endcomponent
        </div>
    </div>
@endsection