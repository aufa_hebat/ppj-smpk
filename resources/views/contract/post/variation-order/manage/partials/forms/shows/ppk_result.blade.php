{{--  @extends('layouts.admin')
@push('scripts')

@endpush
@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')  --}}
                    <div class="row">
                        <div class="col">
                            <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">                                
                            @foreach($types as $type )                                
                                <div class="form-inline">
                                    @foreach($vo->types as $vt)
                                        @if($type->id == $vt->variation_order_type_id)
                                            @if($vt->variation_order_type_id == 1)
                                                <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">Bil.</td>
                                                            <td width="45%">Item</td>
                                                            <td width="20%">Nilai (RM)</td>
                                                            <td width="7%">Keputusan</td>
                                                            <td width="23%">Ulasan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($vo->actives as $key => $active)
                                                            <tr>
                                                                <td>{{ $key + 1 }}</td>
                                                                <td>{{ $active->item->item }}</td>
                                                                <td>{{ money()->toCommon($active->item->amount ?? "0", 2) }}</td>
                                                                <td>
                                                                    @if($active->status == 1)Lulus @else Tidak Lulus @endif
                                                                </td>
                                                                <td>
                                                                    {{ $active->comment }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @elseif($vt->variation_order_type_id == 2)
                                                <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">Bil.</td>
                                                            <td width="45%">Elemen</td>
                                                            <td width="10%">Omission (RM)</td>
                                                            <td width="10%">Addition(RM)</td>
                                                            <td width="7%">Keputusan</td>
                                                            <td width="23%">Ulasan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $index = 0;
                                                        @endphp
                                                        @foreach($PPKElemen as $key => $elemen)
                                                            @if($elemen->vo_type_id == $vt->id)

                                                                <tr>
                                                                    <td>{{ $index = $index + 1 }}</td>
                                                                    <td>{{ $elemen->element }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_omission ?? "0", 2) }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>
                                                                    <td>@if($elemen->status == 1)Lulus @else Tidak Lulus @endif</td>
                                                                    <td>{{ isset($elemen->comment) ? $elemen->comment : "-" }}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @elseif($vt->variation_order_type_id == 3)
                                                <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">Bil.</td>
                                                            <td width="45%">Elemen</td>
                                                            <td width="10%">Addition(RM)</td>
                                                            <td width="7%">Keputusan</td>
                                                            <td width="23%">Ulasan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $index = 0;
                                                        @endphp
                                                        @foreach($PPKElemen as $key => $elemen)
                                                            @if($elemen->vo_type_id == $vt->id)
                                                                <tr>
                                                                    <td>{{ $index = $index + 1 }}</td>
                                                                    <td>{{ $elemen->element }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>
                                                                    <td>@if($elemen->status == 1)Lulus @else Tidak Lulus @endif</td>
                                                                    <td>{{ isset($elemen->comment) ? $elemen->comment : "-" }}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @elseif($vt->variation_order_type_id == 4)
                                                <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">Bil.</td>
                                                            <td width="45%">Elemen</td>
                                                            <td width="10%">Omission (RM)</td>
                                                            <td width="10%">Addition(RM)</td>
                                                            <td width="7%">Keputusan</td>
                                                            <td width="23%">Ulasan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $index = 0;
                                                        @endphp
                                                        @foreach($PPKElemen as $key => $elemen)
                                                            @if($elemen->vo_type_id == $vt->id)
                                                                <tr>
                                                                    <td>{{ $index = $index + 1 }}</td>
                                                                    <td>{{ $elemen->element }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_omission ?? "0", 2) }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>                                                                        
                                                                    <td>@if($elemen->status == 1)Lulus @else Tidak Lulus @endif</td>
                                                                    <td>{{ isset($elemen->comment) ? $elemen->comment : "-" }}</td>                                                                       
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @elseif($vt->variation_order_type_id == 5)
                                                <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">Bil.</td>
                                                            <td width="45%">Elemen</td>
                                                            <td width="10%">Omission (RM)</td>
                                                            <td width="10%">Addition(RM)</td>
                                                            <td width="7%">Keputusan</td>
                                                            <td width="23%">Ulasan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $index = 0;
                                                        @endphp
                                                        @foreach($PPKElemen as $key => $elemen)
                                                            @if($elemen->vo_type_id == $vt->id)
                                                                <tr>
                                                                    <td>{{ $index = $index + 1 }}</td>
                                                                    <td>{{ $elemen->element }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_omission ?? "0", 2) }}</td>
                                                                    <td>{{ money()->toCommon($elemen->amount_addition ?? "0", 2) }}</td>                                                                        
                                                                    <td>@if($elemen->status == 1)Lulus @else Tidak Lulus @endif</td>
                                                                    <td>{{ isset($elemen->comment) ? $elemen->comment : "-" }}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @elseif($vt->variation_order_type_id == 6)
                                                <h4>{{ $type->name }}</h4>&nbsp;&nbsp;&nbsp;
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">Bil.</td>
                                                            <td width="45%">Item</td>
                                                            <td width="20%">Nilai (RM)</td>
                                                            <td width="7%">Keputusan</td>
                                                            <td width="23%">Ulasan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($vo->cancels as $key => $cancel)
                                                            <tr>
                                                                <td>{{ $key + 1 }}</td>
                                                                <td>{{ $cancel->item->item }}</td>
                                                                <td>{{ money()->toCommon($cancel->amount ?? "0", 2) }}</td>
                                                                <td>
                                                                    @if($cancel->status == 1)Lulus @else Tidak Lulus @endif
                                                                </td>
                                                                <td>
                                                                    {{ $cancel->comment }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>                                                
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach

                            <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
                                class="float-right btn btn-default border-primary">
                                {{ __('Kembali') }}
                            </a>     
                        </div>
                    </div>
                {{--  @endslot
            @endcomponent
        </div>
    </div>
@endsection  --}}