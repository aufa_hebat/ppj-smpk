{{--  @extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush  --}}
{{--  @section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')  --}}
                    <div class="row">
                        <div class="col">
                            <h4>Jawatankuasa Perubahan Kerja (P.K)</h4>
                            <p>Bil. Permohonan Perubahan Kerja (PPK) :&nbsp;<span class="badge badge-primary">{{ $vo->no}}</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="table-responsive">
                                        <div id="tableJKPembuka" class="table-editable table-bordered ">
                                            <table class="table">
                                                <tr>
                                                    <th width="30%"><b>Ahli Jawatankuasa Perubahan Kerja</b></th>
                                                    <th width="10%"><b>Peranan</b></th>
                                                </tr>
                                                @if(!empty($officer))
                                                    @foreach($officer as $jk)
                                                        <tr>
                                                            <td>
                                                                <span>{{$jk->user->name}}</span>
                                                            </td>
                                                            <td>
                                                                <span>{{$jk->role}}</span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a href="{{ route('contract.post.variation-order-result.edit', $sst->hashslug) }}" 
                                class="float-right btn btn-default border-primary">
                                {{ __('Kembali') }}
                            </a>     
                        </div>
                    </div>
                {{--  @endslot
            @endcomponent
        </div>
    </div>
@endsection  --}}