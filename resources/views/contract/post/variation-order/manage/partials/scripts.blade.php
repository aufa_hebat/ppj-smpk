@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.variation-order-result.show', id));
		});
		$(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.variation-order-result.edit', id));
		});
	});
</script>
@endpush