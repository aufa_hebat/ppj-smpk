
<div class="row justify-content-center">
    <div class="col-12">
        @if($ipc_akhir == 0)
            <a href="{{ route('contract.post.variation-order-wps.wps-active',['hashslug' => $sst->hashslug]) }}" 
                class="btn btn-primary float-right">Bayar WPS</a>
        @endif
        @include('contract.post.variation-order.wps.partials.scripts-wps')
        @component('components.card',['card_classes' => 'col-12'])
            @slot('card_body')
                @component('components.datatable', 
                    [
                        'table_id'   => 'contract-post-variation-order-wps',
                        'route_name' => 'api.datatable.contract.variation-order-wps',
                        'param'     => 'hashslug=' . $sst->hashslug,
                        'columns' => [
                            ['data' => 'running_no', 'title' => __('Bil'), 'defaultContent' => '-'],
                            ['data' => 'no_kontrak', 'title' => __('No Rujukan'), 'defaultContent' => '-'],
                            ['data' => 'amount', 'title' => __('Jumlah'), 'defaultContent' => '-'],
                            ['data' => 'status_detail', 'title' => __('Status'), 'defaultContent' => '-'],
                            ['data' => 'updated_at', 'title' => __('Dikemaskini pada'), 'defaultContent' => '-'],                        
                            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                        ],
                        'headers' => [
                            __('Bil'),__('No Rujukan'),__('Jumlah'),__('Status'),__('Dikemaskini Pada'), __('table.action')
                        ],
                        'actions' => minify(view('contract.post.variation-order.wps.partials.actions-wps')->render())
                    ]
                )
                @endcomponent
            @endslot
        @endcomponent
    </div>
</div>