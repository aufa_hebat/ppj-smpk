@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {

		$(document).on('click', '.show-ppjhk-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
            redirect(route('contract.post.variation-order-ppjhk.show', id));
		});

		$(document).on('click', '.edit-ppjhk-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
            redirect(route('contract.post.variation-order-ppjhk.edit', id));
		});
 
        $(document).on('click', '.print-ppjhk-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			window.open(route('vo_ppjhk', {hashslug:id}), '_blank');
		});	

		$(document).on('click', '.destroy-ppjhk-action-btn', function(event) {
			event.preventDefault();
			/* Act on the event */
			var id = $(this).data('hashslug');
			swal({
			  	title: '{!! __('Amaran') !!}',
			  	text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  	type: 'warning',
			  	showCancelButton: true,
			  	confirmButtonText: '{!! __('Ya') !!}',
			  	cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  	if (result.value) {
					axios.delete(
						route('api.contract.variation-order-ppjhk.destroy', id), 
						{'_method' : 'DELETE'})
						.then(response => {
							$('#contract-post-variation-order-ppjhk').DataTable().ajax.reload();
							swal('{{ __('Padam Perakuan Pelarasan Jumlah Harga Kontrak') }}', response.data.message, 'success');
						}).catch(error => console.error(error));
			  	}
			});
		});

	});
</script>
@endpush