@if(!empty($review) && (user()->id == $review->created_by))

    @if((!empty($review)) && (user()->id == $review->created_by))

        {{-- penyedia --}}

        {{-- @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                            Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl7 as $s7log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s7log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach

                    </div>
                </div>
            </div>
        @endif

        @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                            Ulasan Semakan Oleh {{$semakan7log->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl6 as $s6log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s6log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif --}}

        @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                            Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl5 as $s5log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s5log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                            Ulasan Semakan Oleh {{$semakan5log->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl4 as $s4log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s4log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                            Ulasan Semakan Oleh {{$semakan4log->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl3 as $s3log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s3log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif --}}

        @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl2 as $s2log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s2log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
            <div class="mb-0 card card-primary">
                <div class="card-header" role="tab" id="headingOne2">
                    <h4 class="card-title">
                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-body">
                        @foreach($sl1 as $s1log)
                            <div class="row">
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Terima</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Tarikh Hantar</div>
                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                </div>
                                <div class="col-4">
                                    <div class="text-muted">Status</div>
                                    <p>{!! $s1log->reviews_status !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-muted">Ulasan</div>
                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

    @endif
                        
@endif

@role('administrator')
    <form id="log-form">
        <div class="row justify-content-center w-100">
            <div class="col-12 w-100">
                @include('contract.pre.box.partials.scripts')
                @component('components.card')
                    @slot('card_body')
                        @component('components.datatable', 
                            [
                                'table_id' => 'contract-pre-box',
                                'route_name' => 'api.datatable.contract.review-log-vo-ppjhk',
                                'param' => 'ppjhk_id=' . $ppjhk->id,
                                'columns' => [
                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                    ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                    ['data' => 'tarikh', 'title' => __('Tarikh Semakan'), 'defaultContent' => '-'],
                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                ],
                                'headers' => [
                                    __('Bil'),__('Penyemak'),__('Jabatan'),  __('Ulasan Semakan'), __('Tarikh Semakan'), __('Status'), __('')
                                ],
                                'actions' => minify('')
                            ]
                        )
                        @endcomponent
                    @endslot
                @endcomponent
            </div>
        </div>
    </form>
@endrole               