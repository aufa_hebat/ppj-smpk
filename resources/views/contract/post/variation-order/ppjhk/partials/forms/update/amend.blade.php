@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $(document).on('click', '.ppjhk-amend-action', function(event) {
                event.preventDefault();
                var data = $('#ppjhk-details').serialize();
                
                console.log(data);
    
                axios.put(route('api.contract.variation-order-ppjhk.amend', '{{ $element->hashslug }}'), data)
                    .then(function(response) {
                        swal('Perakuan Pelarasan Jumlah Harga Kontrak', response.data.message, 'success');
                        location.reload();
                        //redirect(route('contract.post.variation-order.ppk_edit', response.data.hashslug));
                    });
            });

        });

        function calc_vo(val){//for bq auto count amount based on rate
            var rate = document.getElementById('kadar-'+val).value ;
            var qty = document.getElementById('kuantiti-'+val).value ;
            var jmlhOmsn = document.getElementById('jumOmsn-'+val).value ;
            var qtyOmsn = document.getElementById('qtyOmsn-'+val).value ;
                    
            var ratePpk = document.getElementById('kadarPpk-'+val).value ;
            var qtyAdsnPpk = document.getElementById('qtyAdsnPpk-'+val).value ;

            rate = rate.replace(/[,]/g, "");
            rate = rate.split(' ').join('');
            qty = qty.replace(/[,]/g, "");
            qty = qty.split(' ').join('');
            jmlhOmsn = jmlhOmsn.replace(/[,]/g, "");
            jmlhOmsn = jmlhOmsn.split(' ').join('');
            qtyOmsn = qtyOmsn.replace(/[,]/g, "");
            qtyOmsn = qtyOmsn.split(' ').join('');    
            
            ratePpk = ratePpk.replace(/[,]/g, "");
            ratePpk = ratePpk.split(' ').join('');
            qtyAdsnPpk = qtyAdsnPpk.replace(/[,]/g, "");
            qtyAdsnPpk = qtyAdsnPpk.split(' ').join('');

            if(parseFloat(rate) > parseFloat(ratePpk)){
                alert('Kadar yang dimasukkan melebihi dari yang dipersetujui');
                return;
                //break;
            }

            if(parseFloat(qty) > parseFloat(qtyAdsnPpk)){
                alert('Jumlah yang dimasukkan melebihi dari yang dipersetujui');
                return;
                //break;
            }
            
            var $result = rate * qty;
            var $nett = $result - jmlhOmsn;
            var $totalQty = qty - qtyOmsn;

            document.getElementById('jumlah-'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
            document.getElementById('nett-'+val).value = parseFloat($nett).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
            document.getElementById('totalQty-'+val).value = parseFloat($totalQty).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
        }


        function checkWps(val){
            var checkBox = document.getElementById("wps-"+val);
            
            if (checkBox.checked == true){
                document.getElementById('wps-'+val).value = '1';
            }
        }
        /*
        function checkWps(val){
            var checkBox = document.getElementById("wps-"+val);
            var net = document.getElementById('nett-'+val).value;
            var balance = document.getElementById('totalBalance').value;
            var newBalance = 0;
            
            net = net.replace(/[,]/g, "");
            net = net.split(' ').join('');
            net = parseInt(net);

            balance = balance.replace(/[,]/g, "");
            balance = balance.split(' ').join('');
            balance = parseInt(balance);


            if (checkBox.checked == true){
                if(balance >= net){
                    newBalance = balance - net;
                    document.getElementById('totalBalance').value = parseFloat(newBalance).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    document.getElementById('wps-'+val).value = '1';
                }else{
                    alert('Baki WPS tidak mencukupi');
                    checkBox.checked = false;
                }
                
            }else{
                newBalance = parseInt(balance) + parseInt(net);
                document.getElementById('totalBalance').value = parseFloat(newBalance).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
            }            
        }*/

    </script>
@endpush
@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')

                    <div class="row">
                        <div class="col">
                            <p>Bil. Pelarasan Jumlah Harga Kontrak:&nbsp;<span class="badge badge-primary">{{ $ppjhk->no }}</span></p>
                        </div>
                    </div>

                    <form id="ppjhk-details"  method="POST">
                        @csrf
                        <table width="100%">
                            <tr>
                                <td style="font-size:15px; font-weight:bold; text-decoration:underline;">{{ $element->voElement->element }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="hidden" name="totalAmount" id="totalAmount" value="{{ money()->toCommon($totalAmount ?? '0', 2) }}" />
                                    <input type="hidden" name="totalUsed" id="totalUsed" value="{{ money()->toCommon($totalUsed ?? '0', 2) }}"/>
                                    <input type="hidden" name="totalPending" id="totalPending" value="{{ money()->toCommon($totalPending ?? '0', 2) }}"/>
                                    <input type="hidden" name="totalBalance" id="totalBalance" value="{{ money()->toCommon($totalAmount - $totalUsed - $totalPending ?? '0', 2) }}"/>
                                </td>
                            </tr>
                            <tr>                                
                                <td>
                                    
                                    {{--  <table width="100%" >  --}}
                                        @foreach($element->ppjhkItems as $key => $item)
                                            <div class="mb-0 card" >
                                                <div class="card-header" role="tab" id="heading_{{ $item->id }}">
                                                    <h4 class="card-title cardElemen">
                                                        <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion_{{ $item->id }}" href="#collapse_{{ $item->id }}" aria-expanded="true" aria-controls="collapse_{{ $item->id }}">
                                                            <i class="mr-2 fa fa-folder-open"></i><u>{{ $key+1 }}&nbsp;:&nbsp;{{ $item->voItem->item}} &nbsp;&nbsp;&nbsp;RM{{ money()->toCommon($item->net_amount ?? 0,2 )}}</u>
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="collapse_{{ $item->id }}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading_{{ $item->id }}" >
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div id="semakanBq" class="col-md-12 table-responsive">
                                                                <table width="100%" class="table table-bordered" style="font-size:12px;">
                                                                    <thead>
                                                                        <tr>
                                                                            <td rowspan="2">Bil.</td>
                                                                            <td rowspan="2">Sub Item</td>
                                                                            <td rowspan="2">Kadar</td>
                                                                            <td colspan="2">Kuantiti</td>
                                                                            <td colspan="3">Total (RM)</td>
                                                                            <td colspan="6">Amend</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>BQ</td>
                                                                            <td>Actual</td>
                                                                            <td>BQ</td>
                                                                            <td>Actual</td>
                                                                            <td>Nett</td>
                                                                            <td>Kadar</td>
                                                                            <td>Kuantiti</td>
                                                                            <td>Jumlah</td>
                                                                            <td>Nett</td>
                                                                            <td>Jenis</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach($item->ppjhkSubItems as $keyS => $sub)
                                                                            <tr>
                                                                                <td width="1%">{{ $keyS + 1 }}</td>
                                                                                <td width="32%">{{ $sub->voSubItem->item }}</td>
                                                                                <td width="4%">{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? "0", 2) }}</td>
                                                                                <td width="2%">{{ money()->toCommon($sub->voSubItem->quantity_omission ?? "0", 2) }}</td>
                                                                                <td width="2%">{{ money()->toCommon($sub->voSubItem->quantity_addition ?? "0", 2) }}</td>
                                                                                <td width="6%">{{ money()->toCommon($sub->voSubItem->amount_omission ?? "0", 2) }}</td>
                                                                                <td width="6%">{{ money()->toCommon($sub->voSubItem->amount_addition ?? "0", 2) }}</td>
                                                                                <td width="6%">{{ money()->toCommon($sub->voSubItem->net_amount ?? "0", 2) }}</td>                                                                                
                                                                                <td width="5%">
                                                                                    <input type="hidden" size="5" id="kadarPpk-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kadarPpk']" onkeyup="calc_vo({{ $sub->id }})" value="{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? '0', 2) }}" />
                                                                                    <input type="text" size="5" id="kadar-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kadar']" onkeyup="calc_vo({{ $sub->id }})" value="{{ money()->toCommon($sub->rate_per_unit ?? '0', 2) }}" />
                                                                                </td>
                                                                                <td width="5%">
                                                                                    <input type="hidden" id="qtyOmsn-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['qtyOmsn']" value="{{ money()->toCommon($sub->voSubItem->quantity_omission ?? '0', 2) }}" readonly/>
                                                                                    <input type="hidden" id="qtyAdsnPpk-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['qtyAdsnPpk']" value="{{ money()->toCommon($sub->voSubItem->quantity_addition ?? '0', 2) }}" readonly/>
                                                                                    <input type="text" size="5" id="kuantiti-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kuantiti']" onkeyup="calc_vo({{ $sub->id }})" value="{{ money()->toCommon($sub->quantity_ppjhk ?? '0', 2) }}" />
                                                                                </td>
                                                                                <td width="8%">
                                                                                    <input type="hidden" id="jumOmsn-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['jumOmsn']" value="{{ money()->toCommon($sub->voSubItem->amount_omission ?? '0', 2) }}" readonly/>
                                                                                    <input type="text" size="7" id="jumlah-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['jumlah']" value="{{ money()->toCommon($sub->amount_ppjhk ?? '0', 2) }}" readonly/>
                                                                                </td>
                                                                                <td width="8%">
                                                                                    <input type="hidden" size="7" id="totalQty-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['totalQty']" value="{{ money()->toCommon($sub->total_quantity ?? '0', 2) }}" readonly/>
                                                                                    <input type="text" size="7" id="nett-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['nett']" value="{{ money()->toCommon($sub->net_amount ?? '0', 2) }}" readonly/>
                                                                                </td>
                                                                                <td width="13%">
                                                                                    <select  id="jenis-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['status_lock']" style="width: 100%;">
                                                                                        <option value="BQ Rate" @if($sub->status_lock == 'BQ Rate') selected @endif>BQ Rate</option>
                                                                                        <option value="Jkr Rate" @if($sub->status_lock == 'Jkr Rate') selected @endif>JKR Rate</option>
                                                                                        <option value="Propose Rate" @if($sub->status_lock == 'Propose Rate') selected @endif>Propose Rate</option>
                                                                                        <option value="Prorate Rate" @if($sub->status_lock == 'Prorate Rate') selected @endif>Prorate Rate</option>
                                                                                        <option value="Agreed Rate" @if($sub->status_lock == 'Agreed Rate') selected @endif>Agreed Rate</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            {{--  <tr>
                                                <td style="font-size:13px; font-weight:bold;">{{ $item->voItem->item }}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%"  style="font-size:12px;" border="1">
                                                        <thead>
                                                            <tr>
                                                                <td rowspan="2">Bil.</td>
                                                                <td rowspan="2">Item</td>
                                                                <td rowspan="2">Kadar</td>
                                                                <td colspan="2">Kuantiti</td>
                                                                <td colspan="3">Total (RM)</td>
                                                                <td colspan="4">Amend</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Omission</td>
                                                                <td>Addition</td>
                                                                <td>Omission</td>
                                                                <td>Addition</td>
                                                                <td>Nett</td>
                                                                <td>Kadar</td>
                                                                <td>Kuantiti</td>
                                                                <td>Jumlah</td>
                                                                <td>Nett</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($item->ppjhkSubItems as $keyS => $sub)
                                                                <tr>
                                                                    <td width="3%">{{ $keyS + 1 }}</td>
                                                                    <td width="47%">{{ $sub->voSubItem->item }}</td>
                                                                    <td width="5%">{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? "0", 2) }}</td>
                                                                    <td width="4%">{{ $sub->voSubItem->quantity_omission }}</td>
                                                                    <td width="4%">{{$sub->voSubItem->quantity_addition }}</td>
                                                                    <td width="6%">{{ money()->toCommon($sub->voSubItem->amount_omission ?? "0", 2) }}</td>
                                                                    <td width="6%">{{ money()->toCommon($sub->voSubItem->amount_addition ?? "0", 2) }}</td>
                                                                    <td width="6%">{{ money()->toCommon($sub->voSubItem->net_amount ?? "0", 2) }}</td>
                                                                    <td width="5%">
                                                                        <input type="text" size="5" id="kadar-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kadar']" onkeyup="calc_vo({{ $sub->id }})" value="{{ money()->toCommon($sub->rate_per_unit ?? '0', 2) }}" />
                                                                    </td>
                                                                    <td width="5%">
                                                                        <input type="text" size="5" id="kuantiti-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kuantiti']" onkeyup="calc_vo({{ $sub->id }})" value="{{ $sub->quantity_ppjhk }}" />
                                                                    </td>
                                                                    <td width="8%">
                                                                        <input type="hidden" id="jumOmsn-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['jumOmsn']" value="{{ money()->toCommon($sub->voSubItem->amount_omission ?? '0', 2) }}" readonly/>
                                                                        <input type="text" size="7" id="jumlah-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['jumlah']" value="{{ money()->toCommon($sub->amount_ppjhk ?? '0', 2) }}" readonly/>
                                                                    </td>
                                                                    <td width="8%">
                                                                        <input type="text" size="7" id="nett-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['nett']" value="{{ money()->toCommon($sub->net_amount ?? '0', 2) }}" readonly/>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>    
                                                </td>
                                            </tr>  --}}
                                        @endforeach
                                    {{--  </table>  --}}
                                </td>
                            </tr>
                        </table>
                    </form>
                    
                    <div class="float-right btn btn-primary ppjhk-amend-action">
                        Simpan
                    </div>
                    <a href="{{ route('contract.post.variation-order-ppjhk.edit', $ppjhk->hashslug) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>                    
                @endslot
            @endcomponent
        </div>
    </div>
@endsection