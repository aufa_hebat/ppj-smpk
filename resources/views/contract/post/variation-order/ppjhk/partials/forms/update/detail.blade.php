                    <div class="row">
                        <div class="col-8">
                            <p>Bil. Pelarasan Jumlah Harga Kontrak:&nbsp;<span class="badge badge-primary">{{ $ppjhk->no }}</span></p>
                        </div>
                        <div class="col-4">
                            <div class="btn-group float-right">
                                @if($ppjhk->status != 1)
                                <a href="{{ route('vo_ppjhk',['hashslug' => $ppjhk->hashslug]) }}" 
                                    target="_blank" 
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer') {{ __(' Cetak') }}
                                </a>
                                @endif
                            </div>
                        </div>                        
                    </div>

                    <form id="ppjhk-details"  method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $id }}">

                        @php
                            $totalElementPerType = 0;
                        @endphp

                        @foreach($types as $type)
                            @php $totalElementPerType = 0; @endphp
                            
                            @if($type->id != 1)
                                <h5><u>{{ $type->name }}</u></h5>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <td width="3%">Bil.</td>
                                            <td width="72%">Elemen</td>
                                            <td width="5%">P.P.K</td>
                                            <td width="15%">Jumlah</td>
                                            <td width="5%">Pilihan</td>
                                        </tr>
                                    </thead>
                                    <tbody>                                
                                        @foreach($vos as $vo)
                                            @foreach($vo->types as $vot)
                                                @if($vot->variation_order_type_id == $type->id)
                                                    @if($type->id == 3)
                                                        @foreach($vot->elements as $key => $element)
                                                            @if(empty($element->ppjhk_status) && ($element->status == '1'))                                                               
                                                                @php                                                                                                                            
                                                                    $balanced = 0;
                                                                    foreach($element->items as $item){
                                                                        $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                                    }
                                                                @endphp
                                                                    
                                                                @if($balanced > 0)
                                                                    @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                    <tr>
                                                                        <td>{{ $totalElementPerType }}</td>
                                                                        <td>{{ $element->element }}</td>
                                                                        <td>{{ $vo->no }}</td>
                                                                        <td>
                                                                            @foreach($ppjhk->elements as $ppjhkElement)
                                                                                @if($ppjhkElement->vo_element_id == $element->id)                                                          
                                                                                    @php $netAmount = $ppjhkElement->net_amount; @endphp
                                                                                @endif
                                                                            @endforeach
                                                                            {{ money()->toHuman($balanced ?? "0", 2) }}
                                                                        </td>
                                                                        <td>
                                                                            <div class=" form-inline">
                                                                                @include('components.forms.checkbox', [
                                                                                    'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                    'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                    'label' => '',
                                                                                    'value' => $element->id,
                                                                                    ])
                                                                                @foreach($ppjhk->elements as $ppjhkElement)
                                                                                    @if($ppjhkElement->vo_element_id == $element->id)
                                                                                        <a href="{{ route('contract.post.variation-order-ppjhk.amend',['id' => $element->id]) }}">
                                                                                            @icon('fe fe-edit')
                                                                                        </a>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif

                                                            @else
                                                                @if(!empty($element->ppjhk_id))
                                                                    @if($element->ppjhk_id == $ppjhk->id)
                                                                        @php                                                                                                                            
                                                                            $balanced = 0;
                                                                            foreach($element->items as $item){
                                                                                $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                                            }
                                                                        @endphp
                                                                    
                                                                        @if($balanced > 0)
                                                                            @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                            <tr>
                                                                                <td>{{ $totalElementPerType }}</td>
                                                                                <td>{{ $element->element }}</td>
                                                                                <td>{{ $vo->no }}</td>
                                                                                <td>
                                                                                    @foreach($ppjhk->elements as $ppjhkElement)
                                                                                        @if($ppjhkElement->vo_element_id == $element->id)                                                          
                                                                                            @php $netAmount = $ppjhkElement->net_amount; @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    {{ money()->toHuman($netAmount ?? "0", 2) }}
                                                                                </td>
                                                                                <td>
                                                                                    <div class=" form-inline">
                                                                                        @include('components.forms.checkbox', [
                                                                                            'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                            'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                            'label' => '',
                                                                                            'value' => $element->id,
                                                                                            ])
                                                                                        @foreach($ppjhk->elements as $ppjhkElement)
                                                                                            @if($ppjhkElement->vo_element_id == $element->id)
                                                                                                <a href="{{ route('contract.post.variation-order-ppjhk.amend',['id' => $element->id]) }}">
                                                                                                    @icon('fe fe-edit')
                                                                                                </a>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endif
                                                                @endif    
                                                            @endif
                                                        @endforeach
                                                    @elseif($type->id == 6)
                                                        @foreach($vo->cancels as $cancel)
                                                            @if(empty($cancel->ppjhk_status) && ($cancel->status == '1'))
                                                                @php $totalElementPerType = $totalElementPerType + 1;  @endphp
                                                                <tr>
                                                                    <td>{{ $totalElementPerType }}</td>
                                                                    <td>{{ $cancel->item->item }}</td>
                                                                    <td>{{ $vo->no }}</td>
                                                                    <td> {{ money()->toHuman($cancel->amount ?? "0", 2) }}</td>
                                                                    <td>
                                                                        @include('components.forms.checkbox', [
                                                                            'name' => 'cancel[' . kebab_case($cancel->id) . ']',
                                                                            'id' => 'cancelElement-' . kebab_case($cancel->id),
                                                                            'label' => '',
                                                                            'value' => $cancel->id,
                                                                        ])
                                                                    </td>                                            
                                                                </tr>
                                                            @else
                                                                @if(!empty($cancel->ppjhk_id))
                                                                    @if($cancel->ppjhk_id == $ppjhk->id)
                                                                        @php $totalElementPerType = $totalElementPerType + 1;  @endphp
                                                                        <tr>
                                                                            <td>{{ $totalElementPerType }}</td>
                                                                            <td>{{ $cancel->item->item }}</td>
                                                                            <td>{{ $vo->no }}</td>
                                                                            <td> {{ money()->toHuman($cancel->amount ?? "0", 2) }}</td>
                                                                            <td>
                                                                                @include('components.forms.checkbox', [
                                                                                    'name' => 'cancel[' . kebab_case($cancel->id) . ']',
                                                                                    'id' => 'cancelElement-' . kebab_case($cancel->id),
                                                                                    'label' => '',
                                                                                    'value' => $cancel->id,
                                                                                ])
                                                                            </td>                                            
                                                                        </tr>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endforeach                                                    
                                                    @else
                                                        @foreach($vot->elements as $key => $element)
                                                            @if(empty($element->ppjhk_status) && ($element->status == '1'))
                                                                @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                <tr>
                                                                    <td>{{ $totalElementPerType }}</td>
                                                                    <td>{{ $element->element }}</td>
                                                                    <td>{{ $vo->no }}</td>
                                                                    <td> {{ money()->toHuman($element->net_amount ?? "0", 2) }}</td>
                                                                    <td class=" form-inline">
                                                                        @include('components.forms.checkbox', [
                                                                            'name' => 'element[' . kebab_case($element->id) . ']',
                                                                            'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                            'label' => '',
                                                                            'value' => $element->id,
                                                                        ])
                                                                        @foreach($ppjhk->elements as $ppjhkElement)
                                                                            @if($ppjhkElement->vo_element_id == $element->id)
                                                                                <a href="{{ route('contract.post.variation-order-ppjhk.amend',['id' => $element->id]) }}">
                                                                                    @icon('fe fe-edit')
                                                                                </a>
                                                                            @endif
                                                                        @endforeach
                                                                    </td>                                                                
                                                                </tr>
                                                            @else                                                
                                                                @if(!empty($element->ppjhk_id))
                                                                    @php $netAmount = 0.0; @endphp
                                                                    @if($element->ppjhk_id == $ppjhk->id)
                                                                        @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                        <tr>
                                                                            <td>{{ $totalElementPerType }}</td>
                                                                            <td>{{ $element->element }}</td>
                                                                            <td>{{ $vo->no }}</td>
                                                                            <td>
                                                                                @foreach($ppjhk->elements as $ppjhkElement)
                                                                                    @if($ppjhkElement->vo_element_id == $element->id)                                                          
                                                                                        @php $netAmount = $ppjhkElement->net_amount; @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                {{ money()->toHuman($netAmount ?? "0", 2) }}
                                                                            </td>
                                                                            <td>
                                                                                <div class=" form-inline">
                                                                                    @include('components.forms.checkbox', [
                                                                                        'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                        'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                        'label' => '',
                                                                                        'value' => $element->id,
                                                                                    ])
                                                                                    @foreach($ppjhk->elements as $ppjhkElement)
                                                                                        @if($ppjhkElement->vo_element_id == $element->id)
                                                                                            <a href="{{ route('contract.post.variation-order-ppjhk.amend',['id' => $element->id]) }}">
                                                                                                @icon('fe fe-edit')
                                                                                            </a>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endforeach

                                        @if($totalElementPerType == 0)
                                            <tr>
                                                <td colspan="5">-Tiada-</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            @endif
                        @endforeach

                    </form>
                    
                    
                    <div class="btn-group float-right">
                        <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                            class="float-right btn btn-default border-primary">
                            {{ __('Kembali') }}
                        </a>
                        <div class="float-right btn btn-primary ppjhk-details-action">
                            Simpan
                        </div>   
                        <br>
                        @role('penyedia')
                            @if(!empty($ppjhk) && $ppjhk->sst->user_id == user()->id)
                                @if(empty($review) || empty($review->status))
                                    <form method="POST" action="{{ route('acquisition.review.store') }}">
                                        @csrf
                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                        <input type="text" id="acquisition_id" name="acquisition_id" value="@if(!empty($ppjhk)){{ $ppjhk->sst->acquisition_id }}@endif" hidden>

                                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                                        <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                                        @if(!empty($review_previous))
                                            <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                            <input type="text" id="type" name="type" value="VO" hidden>
                                            <input type="text" id="document_contract_type" name="document_contract_type" value="PPJHK" hidden>
                                        @endif

                                        <input type="text" id="ppjhk_id" name="ppjhk_id" value="{{$ppjhk->id}}" hidden>
                                        
                                        <button type="button" id="send" class="btn btn-default float-middle border-default">
                                            @icon('fe fe-send') {{ __('Teratur') }}
                                        </button>
                                    </form>
                                @endif
                            @endif
                            <br>
                        @endrole  
                    </div>
