@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $(document).on('click', '.ppjhk-details-action', function(event) {
                event.preventDefault();
                var data = $('#ppjhk-details').serialize();
                
                console.log(data);
    
                axios.post(route('api.contract.variation-order-ppjhk.store'), data)
                    .then(function(response) {
                        swal('', response.data.message, 'success');
                        redirect(route('contract.post.variation-order-ppjhk.edit', response.data.hashslug));
                    });
            });
        });
    </script>
@endpush
@section('content')
    {{--  @include('contract.post.variation-order.partials.title')  --}}
    @include('components.pages.title')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Elemen') }}
                    </a>
                </li>               
            </ul>            
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'variation-order-ppjhk'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'detail', 'active' => true])
                                @slot('content')
                                    @include('contract.post.variation-order.ppjhk.partials.forms.create.detail',['sst' => $sst])
                                @endslot
                            @endcomponent
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>


    {{--  <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')

                    <div class="row">
                        <div class="col">
                            <p>Bil. Pelarasan Jumlah Harga Kontrak:&nbsp;<span class="badge badge-primary"></span></p>
                        </div>
                    </div>

                    <form id="ppjhk-details"  method="POST">
                        <input type="hidden" name="id" value="{{ $id }}">

                        @php
                            $totalElementPerType = 0;
                        @endphp

                        @foreach($types as $type)
                            @php $totalElementPerType = 0; @endphp
                            
                            @if($type->id != 1)
                                <h5><u>{{ $type->name }}</u></h5>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <td width="3%">Bil.</td>
                                            <td width="72%">Elemen</td>
                                            <td width="5%">P.P.K</td>
                                            <td width="15%">Jumlah</td>
                                            <td width="5%">Pilihan</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($vos as $vo)
                                    
                                        @foreach($vo->types as $vot)
                                            @if($vot->variation_order_type_id == $type->id)
                                                @if($type->id == 3)
                                                    @foreach($vot->elements as $key => $element)
                                                        @if(empty($element->ppjhk_status) && ($element->status == '1'))   
                                                            @php                                                                                                                         
                                                                $balanced = 0;
                                                                foreach($element->items as $item){
                                                                    $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                                }
                                                            @endphp
                                                                {{ $balanced }}
                                                            @if($balanced > 0)
                                                                @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                <tr>
                                                                    <td>{{ $totalElementPerType }}</td>
                                                                    <td>{{ $element->element }}</td>
                                                                    <td>{{ $vo->no }}</td>
                                                                    <td>
                                                                        {{ money()->toHuman($netAmount ?? "0", 2) }}
                                                                    </td>
                                                                    <td>
                                                                        <div class=" form-inline">
                                                                            @include('components.forms.checkbox', [
                                                                                'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                'label' => '',
                                                                                'value' => $element->id,
                                                                                ])
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach                                                
                                                @else
                                                    @foreach($vot->elements as $key => $element)
                                                        @if(empty($element->ppjhk_status) && ($element->status == '1'))
                                                            @php $totalElementPerType = $totalElementPerType + 1;  @endphp
                                                            <tr>
                                                                <td>{{ $totalElementPerType }}</td>
                                                                <td>{{ $element->element }}</td>
                                                                <td>{{ $vo->no }}</td>
                                                                <td> {{ money()->toHuman($element->net_amount ?? "0", 2) }}</td>
                                                                <td>
                                                                    @include('components.forms.checkbox', [
                                                                        'name' => 'element[' . kebab_case($element->id) . ']',
                                                                        'id' => 'element-' . kebab_case($element->id),
                                                                        'label' => '',
                                                                        'value' => $element->id,
                                                                    ])
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                        
                                    @endforeach
                                    @if($totalElementPerType == 0)
                                        <tr>
                                            <td colspan="5">-Tiada-</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <br/>
                            @endif
                        @endforeach
                    </form>
                    
                    <div class="float-right btn btn-primary ppjhk-details-action">
                        Kemaskini
                    </div>
                    <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>                    
                @endslot
            @endcomponent
        </div>
    </div>  --}}
@endsection