@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ppjhk_id : $('#ppjhk_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(count($ppjhk->elements) > 0)
                @foreach($ppjhk->elements as $element)
                    $('#ppjhkElement-{{ $element->vo_element_id }}').prop('checked', true);
                @endforeach
            @endif

            @if(count($ppjhk->cancels) > 0)
                @foreach($ppjhk->cancels as $cancel)
                    $('#cancelElement-{{ $cancel->id }}').prop('checked', true);
                @endforeach
            @endif

            $(document).on('click', '.ppjhk-details-action', function(event) {
                event.preventDefault();
                var data = $('#ppjhk-details').serialize();
                
                console.log(data);
    
                axios.put(route('api.contract.variation-order-ppjhk.update', '{{ $id }}'), data)
                    .then(function(response) {
                        swal('Perakuan Pelarasan Jumlah Harga Kontrak', response.data.message, 'success');
                        location.reload();
                    });
            });
        });
    </script>
@endpush
@section('content')
    {{--  @include('contract.post.variation-order.partials.title')  --}}
    @include('components.pages.title')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Elemen') }}
                    </a>
                </li>  
                @if(!empty($review) && (user()->id == $review->created_by))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Ulasan Pegawai') }}
                    </a>
                </li>
                @endif                          
            </ul>            
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'variation-order-ppjhk'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'detail', 'active' => true])
                                @slot('content')
                                    @include('contract.post.variation-order.ppjhk.partials.forms.update.detail',['sst' => $sst])
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'document-reviews'])
                            @slot('content')
                                @include('contract.post.variation-order.ppjhk.partials.forms.update.remarks',['sst' => $sst])
                            @endslot
                        @endcomponent                            
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection