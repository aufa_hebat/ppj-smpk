@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $(document).on('click', '.ppjhk-amend-action', function(event) {
                event.preventDefault();
                var data = $('#ppjhk-details').serialize();
                
                console.log(data);
    
                axios.put(route('api.contract.variation-order-ppjhk.amend', '{{ $element->hashslug }}'), data)
                    .then(function(response) {
                        swal('', response.data.message, 'success');
                        //redirect(route('contract.post.variation-order.ppk_edit', response.data.hashslug));
                    });
            });

        });

        function calc_vo(val){//for bq auto count amount based on rate
            var rate = document.getElementById('kadar-'+val).value ;
            var qty = document.getElementById('kuantiti-'+val).value ;
            var jmlhOmsn = document.getElementById('jumOmsn-'+val).value ;
                    
            rate = rate.replace(/[,]/g, "");
            rate = rate.split(' ').join('');
            qty = qty.replace(/[,]/g, "");
            qty = qty.split(' ').join('');
            jmlhOmsn = jmlhOmsn.replace(/[,]/g, "");
            jmlhOmsn = jmlhOmsn.split(' ').join('');
            
            var $result = rate * qty;
            var $nett = $result - jmlhOmsn;

            document.getElementById('jumlah-'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
            document.getElementById('nett-'+val).value = parseFloat($nett).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
        }
    </script>
@endpush
@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')

                    <div class="row">
                        <div class="col">
                            <p>Bil. Pelarasan Jumlah Harga Kontrak:&nbsp;<span class="badge badge-primary">{{ $ppjhk->no }}</span></p>
                        </div>
                    </div>

                    <table width="100%">
                        <tr>
                            <td style="font-size:15px; font-weight:bold; text-decoration:underline;">{{ $element->voElement->element }}</td>
                        </tr>
                        <tr>                                
                            <td>                                    
                                @foreach($element->ppjhkItems as $key => $item)
                                    <div class="mb-0 card" >
                                        <div class="card-header" role="tab" id="heading_{{ $item->id }}">
                                            <h4 class="card-title cardElemen">
                                                <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion_{{ $item->id }}" href="#collapse_{{ $item->id }}" aria-expanded="true" aria-controls="collapse_{{ $item->id }}">
                                                    <i class="mr-2 fa fa-folder-open"></i><u>{{ $item->voItem->no}}&nbsp;:&nbsp;{{ $item->voItem->item}} &nbsp;&nbsp;&nbsp;RM{{ money()->toCommon($item->net_amount ?? 0,2 )}}</u>
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="collapse_{{ $item->id }}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading_{{ $item->id }}" >
                                            <div class="card-body">
                                                <div class="row">
                                                    <div id="semakanBq" class="col-md-12 table-responsive">
                                                        <table width="100%" class="table table-bordered" style="font-size:12px;">
                                                            <thead>
                                                                <tr>
                                                                    <td rowspan="2">Bil.</td>
                                                                    <td rowspan="2">Item</td>
                                                                    <td rowspan="2">Kadar</td>
                                                                    <td colspan="2">Kuantiti</td>
                                                                    <td colspan="3">Total (RM)</td>
                                                                    <td colspan="4">Amend</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Omission</td>
                                                                    <td>Addition</td>
                                                                    <td>Omission</td>
                                                                    <td>Addition</td>
                                                                    <td>Nett</td>
                                                                    <td>Kadar</td>
                                                                    <td>Kuantiti</td>
                                                                    <td>Jumlah</td>
                                                                    <td>Nett</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($item->ppjhkSubItems as $keyS => $sub)
                                                                    <tr>
                                                                        <td width="3%">{{ $keyS + 1 }}</td>
                                                                        <td width="47%">{{ $sub->voSubItem->item }}</td>
                                                                        <td width="5%">{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? "0", 2) }}</td>
                                                                        <td width="4%">{{ money()->toCommon($sub->voSubItem->quantity_omission ?? "0", 2) }}</td>
                                                                        <td width="4%">{{ money()->toCommon($sub->voSubItem->quantity_addition ?? "0", 2) }}</td>
                                                                        <td width="6%">{{ money()->toCommon($sub->voSubItem->amount_omission ?? "0", 2) }}</td>
                                                                        <td width="6%">{{ money()->toCommon($sub->voSubItem->amount_addition ?? "0", 2) }}</td>
                                                                        <td width="6%">{{ money()->toCommon($sub->voSubItem->net_amount ?? "0", 2) }}</td>
                                                                        <td width="5%">
                                                                            <input type="text" size="5" id="kadar-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kadar']" value="{{ money()->toCommon($sub->rate_per_unit ?? '0', 2) }}" readonly/>
                                                                        </td>
                                                                        <td width="5%">
                                                                            <input type="text" size="5" id="kuantiti-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['kuantiti']" value="{{ $sub->quantity_ppjhk }}"  readonly/>
                                                                        </td>
                                                                        <td width="8%">
                                                                            <input type="hidden" id="jumOmsn-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['jumOmsn']" value="{{ money()->toCommon($sub->voSubItem->amount_omission ?? '0', 2) }}" readonly/>
                                                                            <input type="text" size="7" id="jumlah-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['jumlah']" value="{{ money()->toCommon($sub->amount_ppjhk ?? '0', 2) }}" readonly/>
                                                                        </td>
                                                                        <td width="8%">
                                                                            <input type="text" size="7" id="nett-{{$sub->id}}" name="ppjhk[{{$sub->id}}]['nett']" value="{{ money()->toCommon($sub->net_amount ?? '0', 2) }}" readonly/>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>                                                             
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                
                    <a href="{{ route('contract.post.variation-order-ppjhk.show', $ppjhk->hashslug) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>                    
                @endslot
            @endcomponent
        </div>
    </div>
@endsection