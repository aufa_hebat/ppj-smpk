
                    <div class="row">
                        <div class="col-8">
                            <p>Bil. Pelarasan Jumlah Harga Kontrak:&nbsp;<span class="badge badge-primary">{{ $ppjhk->no }}</span></p>
                        </div>
                        <div class="col-4">
                            @if($ppjhk->status != 1)
                                <div class="btn-group float-right">
                                    {{--  <a href="{{ route('vo_ppjhk',['hashslug' => $ppjhk->hashslug]) }}" 
                                        target="_blank" 
                                        class="btn btn-success border-success">
                                        @icon('fe fe-printer') {{ __(' Cetak') }}
                                    </a>  --}}
                                    <a target="_blank" href="#"
                                        class="btn btn-success print-action-btn">
                                        @icon('fe fe-printer') {{ __('Cetak') }}
                                    </a>                                     
                                </div>
                            @endif
                        </div>                        
                    </div>

                    @php
                        $totalElementPerType = 0;
                    @endphp

                    @foreach($types as $type)
                        @php $totalElementPerType = 0; @endphp
                            
                        @if($type->id != 1)
                            <h5><u>{{ $type->name }}</u></h5>
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <td width="3%">Bil.</td>
                                        <td width="72%">Elemen</td>
                                        <td width="5%">P.P.K</td>
                                        <td width="15%">Jumlah</td>
                                        <td width="5%">Pilihan</td>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    @foreach($vos as $vo)                           
                                        @foreach($vo->types as $vot)
                                            @if($vot->variation_order_type_id == $type->id)
                                                @if($type->id == 3)
                                                    @foreach($vot->elements as $key => $element)                                                        
                                                        @if(empty($element->ppjhk_status) && ($element->status == '1'))
                                                    
                                                            @php
                                                                $balanced = 0;
                                                                foreach($element->items as $item){
                                                                    $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                                }
                                                            @endphp
                                                            @if($balanced > 0)
                                                                @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                <tr>
                                                                    <td>{{ $totalElementPerType }}</td>
                                                                    <td>{{ $element->element }}</td>
                                                                    <td>{{ $vo->no }}</td>
                                                                    <td>
                                                                        @foreach($ppjhk->elements as $ppjhkElement)
                                                                            @if($ppjhkElement->vo_element_id == $element->id)                                                          
                                                                                @php $netAmount = $ppjhkElement->net_amount; @endphp
                                                                            @endif
                                                                        @endforeach
                                                                        {{ money()->toHuman($netAmount ?? "0", 2) }}
                                                                    </td>
                                                                    <td>
                                                                        <div class=" form-inline">
                                                                            @include('components.forms.checkbox', [
                                                                                'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                'label' => '',
                                                                                'value' => $element->id,
                                                                                'disabled' => true,
                                                                            ])
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif

                                                        @else
                                                            @if(!empty($element->ppjhk_id))
                                                                @if($element->ppjhk_id == $ppjhk->id)
                                                                    @php
                                                                        $balanced = 0;
                                                                        foreach($element->items as $item){
                                                                            $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                                        }
                                                                    @endphp
                                                                    
                                                                    @if($balanced > 0)
                                                                        @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                        <tr>
                                                                            <td>{{ $totalElementPerType }}</td>
                                                                            <td>{{ $element->element }}</td>
                                                                            <td>{{ $vo->no }}</td>
                                                                            <td>
                                                                                @foreach($ppjhk->elements as $ppjhkElement)
                                                                                    @if($ppjhkElement->vo_element_id == $element->id)                                                          
                                                                                        @php $netAmount = $ppjhkElement->net_amount; @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                {{ money()->toHuman($netAmount ?? "0", 2) }}
                                                                            </td>
                                                                            <td>
                                                                                <div class=" form-inline">
                                                                                    @include('components.forms.checkbox', [
                                                                                        'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                        'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                        'label' => '',
                                                                                        'value' => $element->id,
                                                                                        'disabled' => true,
                                                                                    ])                                                                                    
                                                                                    @foreach($ppjhk->elements as $ppjhkElement)
                                                                                        @if($ppjhkElement->vo_element_id == $element->id)
                                                                                            <a href="{{ route('contract.post.variation-order-ppjhk.show-amend',['id' => $element->id]) }}">
                                                                                                @icon('fe fe-eye')
                                                                                            </a>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @elseif($type->id == 6)
                                                    @foreach($vo->cancels as $cancel)
                                                        @if(!empty($cancel->ppjhk_id))
                                                            @if($cancel->ppjhk_id == $ppjhk->id)
                                                                @php $totalElementPerType = $totalElementPerType + 1;  @endphp
                                                                <tr>
                                                                    <td>{{ $totalElementPerType }}</td>
                                                                    <td>{{ $cancel->item->item }}</td>
                                                                    <td>{{ $vo->no }}</td>
                                                                    <td> {{ money()->toHuman($cancel->amount ?? "0", 2) }}</td>
                                                                    <td>
                                                                        @include('components.forms.checkbox', [
                                                                            'name' => 'cancel[' . kebab_case($cancel->id) . ']',
                                                                            'id' => 'cancelElement-' . kebab_case($cancel->id),
                                                                            'label' => '',
                                                                            'value' => $cancel->id,
                                                                            'disabled' => true,
                                                                        ])
                                                                    </td>                                            
                                                                </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach                                                  
                                                @else
                                                    @foreach($vot->elements as $key => $element)
                                                        @if(empty($element->ppjhk_status) && ($element->status == '1'))
                                                            @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                            <tr>
                                                                <td>{{ $totalElementPerType }}</td>
                                                                <td>{{ $element->element }}</td>
                                                                <td>{{ $vo->no }}</td>
                                                                <td> {{ money()->toHuman($element->net_amount ?? "0", 2) }}</td>
                                                                <td >
                                                                    {{--  @if($element->status == 1)Lulus @else Tidak Lulus @endif  --}}
                                                                    
                                                                    @include('components.forms.checkbox', [
                                                                        'name' => 'element[' . kebab_case($element->id) . ']',
                                                                        'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                        'label' => '',
                                                                        'value' => $element->id,
                                                                        'disabled' => true,
                                                                    ])
                                                                    {{--  @foreach($ppjhk->elements as $ppjhkElement)
                                                                        @if($ppjhkElement->vo_element_id == $element->id)
                                                                            <a href="{{ route('contract.post.variation-order-ppjhk.show-amend',['id' => $element->id]) }}">
                                                                                @icon('fe fe-edit')
                                                                            </a>
                                                                        @endif
                                                                    @endforeach  --}}
                                                                </td>                                                                
                                                            </tr>
                                                        @else                                                    
                                                            @if(!empty($element->ppjhk_id))
                                                                @php $netAmount = 0.0; @endphp
                                                                @if($element->ppjhk_id == $ppjhk->id)
                                                                    @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                                    <tr>
                                                                        <td>{{ $totalElementPerType }}</td>
                                                                        <td>{{ $element->element }}</td>
                                                                        <td>{{ $vo->no }}</td>
                                                                        <td>
                                                                            @foreach($ppjhk->elements as $ppjhkElement)
                                                                                @if($ppjhkElement->vo_element_id == $element->id)                                                          
                                                                                    @php $netAmount = $ppjhkElement->net_amount; @endphp
                                                                                @endif
                                                                            @endforeach
                                                                            {{ money()->toHuman($netAmount ?? "0", 2) }}
                                                                        </td>
                                                                        <td>
                                                                            <div class=" form-inline">
                                                                                @include('components.forms.checkbox', [
                                                                                    'name' => 'element[' . kebab_case($element->id) . ']',
                                                                                    'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                                    'label' => '',
                                                                                    'value' => $element->id,
                                                                                    'disabled' => true,
                                                                                ])
                                                                                @foreach($ppjhk->elements as $ppjhkElement)
                                                                                    @if($ppjhkElement->vo_element_id == $element->id)
                                                                                        <a href="{{ route('contract.post.variation-order-ppjhk.show-amend',['id' => $element->id]) }}">
                                                                                            @icon('fe fe-eye')
                                                                                        </a>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    @endforeach

                                    @if($totalElementPerType == 0)
                                        <tr>
                                            <td colspan="5">-Tiada-</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif
                    @endforeach

                    <br/><br/>
                    @role('penyedia')
                        @if($ppjhk->status == 9)
                            {{--  @if(!empty($review) && $review->status == 'Selesai')
                                @include('contract.post.variation-order.ppjhk.partials.forms.show.upload', ['ppjhk' => $ppjhk])
                            @else
                                <div class="btn-group float-right">
                                    <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                        class="btn btn-default border-primary">
                                        {{ __('Kembali') }}
                                    </a>
                                </div>
                            @endif  --}}
                            <div class="btn-group float-right">
                                <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                    class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>
                            </div>
                        @else
                            {{--  <div class="btn-group float-right">
                                <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                    class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>
                            </div>  --}}
                        @endif
                    @endrole

                    {{--  @if($ppjhk->status == 1)
                        @include('contract.post.variation-order.ppjhk.partials.forms.show.show-upload', ['ppjhk' => $ppjhk])
                    @endif  --}}
                    <br/><br/> 

                    {{--  @if(!empty($review))  --}}
                        {{--  @role('penyedia')  --}}

                            {{--  @if(!empty($cetaknotisuulog))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl7 as $s7log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s7log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan7log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl6 as $s6log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s6log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($cetaknotisbpubs))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl5 as $s5log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan5log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl4 as $s4log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan4log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl3 as $s3log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif
                                
                            @if(!empty($semakan3log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl2 as $s2log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan2log))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl1 as $s1log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif  --}}
                            
                        {{--  @else  --}}
                            {{-- pegawai pelaksana --}}
                            {{--  @if($review->create->department->id == user()->department->id)
                                
                                @if(!empty($semakan3log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif  --}}

                            {{-- pegawai bpub --}}
                            {{--  @if(user()->department->id == '9')

                                @if(!empty($cetaknotisbpubs))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan5log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                    Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl4 as $s4log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan4log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                    Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl3 as $s3log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif  --}}

                            {{-- pegawai undang-undang --}}
                            {{--  @if(user()->department->id == '3')

                                @if(!empty($cetaknotisuulog))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                    Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s7log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s7log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan7log))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                    Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl6 as $s6log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s6log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif  --}}
                        {{--  @endrole  --}}
                        {{--  <br>  --}}
                        
                        {{-- semakan --}}
                        {{--  @if(user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'VO' && $review->document_contract_type == 'PPJHK')  --}}
                            {{--  <div class="card row col-12"><br>
                                <h3>Semakan</h3>
                                <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                    @csrf

                                    @include('components.forms.hidden', [
                                        'id' => 'id',
                                        'name' => 'id',
                                        'value' => ''
                                    ])

                                    @if(!empty($ppjhk))
                                        @include('components.forms.hidden', [
                                            'id' => 'acquisition_id',
                                            'name' => 'acquisition_id',
                                            'value' => $ppjhk->sst->acquisition_id
                                        ])
                                    @endif

                                    @if(!empty($review))
                                    <input type="hidden" name="task_by" value="{{$review->task_by}}">
                                    <input type="hidden" name="law_task_by" value="{{$review->law_task_by}}">

                                    <input type="hidden" name="created_by" value="{{$review->created_by}}">
                                    @endif

                                    @if(!empty($review_previous))
                                        <input type="text" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                        <input type="text" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                        <input type="text" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                        <input type="text" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                        <input type="text" name="type" value="VO" hidden>
                                        <input type="text" name="document_contract_type" value="PPJHK" hidden>
                                    @endif
                                    <input type="text" name="ppjhk_id" value="{{$ppjhk->id}}" hidden>

                                    <div class="row">
                                        <div class="col-6">
                                            @include('components.forms.input', [
                                                'input_label' => __('Nama Pegawai Semakan'),
                                                'id' => '',
                                                'name' => '',
                                                'value' => user()->name,
                                                'readonly' => true
                                            ])
                                        </div>
                                            @include('components.forms.hidden', [
                                                'id' => 'requested_by',
                                                'name' => 'requested_by',
                                                'value' => user()->id
                                            ])
                                            @include('components.forms.hidden', [
                                                'id' => 'approved_by',
                                                'name' => 'approved_by',
                                                'value' => user()->supervisor->id
                                            ])
                                        <div class="col-3">
                                            @if(!empty($review))
                                                @if(!empty($review->status))
                                                    @if($review->status == 'S1')
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Terima'),
                                                            'id' => 'requested_at',
                                                            'name' => 'requested_at',
                                                        ])
                                                    @elseif(($review->status == 'S2') || ($review->status == 'S3') || ($review->status == 'S4') || ($review->status == 'S5') || ($review->status == 'N') || ($review->status == 'S6') || ($review->status == 'S7') || ($review->status == 'CN'))
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Hantar'),
                                                            'id' => 'requested_at',
                                                            'name' => 'requested_at',
                                                        ])  
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-3">
                                            @if(!empty($review))
                                                @if(!empty($review->status))
                                                    @if($review->status == 'S1')
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Terima'),
                                                            'id' => 'approved_at',
                                                            'name' => 'approved_at',
                                                        ])
                                                    @elseif(($review->status == 'S2') || ($review->status == 'S3') || ($review->status == 'S4') || ($review->status == 'S5') || ($review->status == 'N') || ($review->status == 'S6') || ($review->status == 'S7') || ($review->status == 'CN'))
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Terima Oleh Pengesah'),
                                                            'id' => 'approved_at',
                                                            'name' => 'approved_at',
                                                        ])
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    @if(!empty($review) && $review->created_by != user()->id)
                                        @include('components.forms.textarea', [
                                            'input_label' => __('Ulasan'),
                                            'id' => 'remarks',
                                            'name' => 'remarks'
                                        ])
                                    @else

                                    @endif
                                    
                                    <input class="rejectStatus" name="rejectStatus" type="hidden"/>
                                    <input class="saveStatus" name="saveStatus" type="hidden"/>
                                    <input class="sentStatus" name="sentStatus" type="hidden"/>

                                    <div class="btn-group float-right">
                                        <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                            class="float-right btn btn-default border-primary">
                                            {{ __('Kembali') }}
                                        </a>
                                        @if(!empty($review) && $review->created_by != user()->id)
                                            <button type="submit" class="btn btn-danger btn-default border-primary rejSent">
                                                @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                            </button>
                                            <button type="submit" class="btn btn-primary btn-group float-right savDraf">
                                                @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                            </button>
                                        @endif
                                        <button type="submit" class="btn btn-default float-middle border-default savSent">
                                            @icon('fe fe-send') {{ __('Hantar') }}
                                        </button>
                                    </div>

                                </form>
                            </div>  --}}
                        {{--  @else  --}}
                            {{-- <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                class="float-right btn btn-default border-primary">
                                {{ __('Kembali') }}
                            </a> --}}
                        {{--  @endif  --}}


                        {{-- log semakan --}}
                        {{--  @role('administrator')
                            @if(!empty($ppjhk))
                                <div class="tab-pane fade show " id="document-contract-review-log" role="tabpanel" aria-labelledby="document-contract-review-log-tab">
                                    <form id="log-form">
                                        <div class="row justify-content-center w-100">
                                            <div class="col-12 w-100">
                                                @include('contract.pre.box.partials.scripts')
                                                @component('components.card')
                                                    @slot('card_body')
                                                        @component('components.datatable', 
                                                            [
                                                                'table_id' => 'contract-pre-box',
                                                                'route_name' => 'api.datatable.contract.review-log-vo-ppjhk',
                                                                'param' => 'ppjhk_id=' . $ppjhk->id,
                                                                'columns' => [
                                                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                    ['data' => 'jenis', 'title' => __('Jenis'), 'defaultContent' => '-'],
                                                                    ['data' => 'tarikh', 'title' => __('Tarikh Semakan'), 'defaultContent' => '-'],
                                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                ],
                                                                'headers' => [
                                                                    __('Bil'),__('Penyemak'), __('Ulasan Semakan'), __('Jenis'), __('Tarikh Semakan'), __('Status'), __('')
                                                                ],
                                                                'actions' => minify('')
                                                            ]
                                                        )
                                                        @endcomponent
                                                    @endslot
                                                @endcomponent
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        @endrole  --}}
                    {{--  @endif   --}}

                    {{--  <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>                      --}}
