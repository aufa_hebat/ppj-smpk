@push('scripts')
@include('components.forms.assets.datetimepicker')
@include('components.forms.assets.select2')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
<script>
    jQuery(document).ready(function($) {

        $('#uploadFile').change(function(){
            $('#subFile').val($(this).val().split('\\').pop());
        });
        
        @if(!empty($ppjhk) && !empty($ppjhk->doc_ppjhk))
            $('#subFile').val('{{ $ppjhk->doc_ppjhk->document_name }}');
        @endif

        $(document).on('click', '.save-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $ppjhk->id ?? ''}}';
            var route_name = 'api.contract.upload-ppjhk';
            var form_id = 'ppjhk_form';
            var form = document.forms[form_id];
            var data = new FormData(form);

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }

            axios.post(route(route_name, id), data).then(response => {
                swal('', response.data.message, 'success');
                location.reload();
            });
        });

        $(document).on('click', '.submit-action-btn', function(event) {
            event.preventDefault();
            var id = '{{ $ppjhk->id ?? ''}}';
            var route_name = 'api.contract.upload-ppjhk';
            var form_id = 'ppjhk_form';
            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', 'submit');

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }

            axios.post(route(route_name, id), data).then(response => {
                swal('', response.data.message, 'success');
                location.reload();
            });
        });
    });
</script>
@endpush

<h4>Paparan Dokumen PPJHK</h4>
<div class="row">
<div class="col-12">
    <form id="ppjhk_form" files = "true" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="form-group row">
            <div class="col input-group">
                <input id="subFile" type="text"  class="form-control" readonly>
                {{--  <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>  --}}
                {{--  <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">  --}}
                @if(!empty($ppjhk) && !empty($ppjhk->doc_ppjhk))
                    <label class="input-group-text" for="downloadFile">
                        <a href="/download/{{$ppjhk->doc_ppjhk->document_path}}/{{ $ppjhk->doc_ppjhk->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                    </label>
                @endif
            </div>
            
        </div>

        <div class="row">
            <div class="col-12" style="text-align:right;">

                <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                    class="btn btn-default border-primary">
                    {{ __('Kembali') }}
                </a>        
            </div>
        </div>
    </form>
</div>
</div>