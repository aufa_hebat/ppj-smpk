<div class="row">
    <div class="col">
        <p>Bil. Pelarasan Jumlah Harga Kontrak:&nbsp;<span class="badge badge-primary"></span></p>
    </div>
</div>
<form id="ppjhk-details"  method="POST">
    <input type="hidden" name="id" value="{{ $id }}">

    @php
        $totalElementPerType = 0;
    @endphp

    @foreach($types as $type)
        @php $totalElementPerType = 0; @endphp
                            
        @if($type->id != 1)
            <h5><u>{{ $type->name }}</u></h5>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <td width="3%">Bil.</td>
                        <td width="72%">Elemen</td>
                        <td width="5%">P.P.K</td>
                        <td width="15%">Jumlah</td>
                        <td width="5%">Pilihan</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vos as $vo)                                    
                        @foreach($vo->types as $vot)
                            @if($vot->variation_order_type_id == $type->id)
                                @if($type->id == 3)
                                    @foreach($vot->elements as $key => $element)
                                        @if(empty($element->ppjhk_status) && ($element->status == '1'))   
                                            @php                                                                                                                         
                                                $balanced = 0;
                                                foreach($element->items as $item){
                                                    $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                }
                                            @endphp
                                            @if($balanced > 0)
                                                @php $totalElementPerType = $totalElementPerType + 1; @endphp
                                                <tr>
                                                    <td>{{ $totalElementPerType }}</td>
                                                    <td>{{ $element->element }}</td>
                                                    <td>{{ $vo->no }}</td>
                                                    <td>{{ money()->toHuman($balanced ?? "0", 2) }}</td>
                                                    <td>
                                                        <div class=" form-inline">
                                                            @include('components.forms.checkbox', [
                                                                'name' => 'element[' . kebab_case($element->id) . ']',
                                                                'id' => 'ppjhkElement-' . kebab_case($element->id),
                                                                'label' => '',
                                                                'value' => $element->id,
                                                            ])
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach                                                
                                @elseif($type->id == 6)
                                    @foreach($vo->cancels as $cancel)
                                        @if(empty($cancel->ppjhk_status) && ($cancel->status == '1'))
                                            @php $totalElementPerType = $totalElementPerType + 1;  @endphp
                                            <tr>
                                                <td>{{ $totalElementPerType }}</td>
                                                <td>{{ $cancel->item->item }}</td>
                                                <td>{{ $vo->no }}</td>
                                                <td> {{ money()->toHuman($cancel->amount ?? "0", 2) }}</td>
                                                <td>
                                                    @include('components.forms.checkbox', [
                                                        'name' => 'cancel[' . kebab_case($cancel->id) . ']',
                                                        'id' => 'cancelElement-' . kebab_case($cancel->id),
                                                        'label' => '',
                                                        'value' => $cancel->id,
                                                    ])
                                                </td>                                            
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    @foreach($vot->elements as $key => $element)
                                        @if(empty($element->ppjhk_status) && ($element->status == '1'))
                                            @php $totalElementPerType = $totalElementPerType + 1;  @endphp
                                            <tr>
                                                <td>{{ $totalElementPerType }}</td>
                                                <td>{{ $element->element }}</td>
                                                <td>{{ $vo->no }}</td>
                                                <td> {{ money()->toHuman($element->net_amount ?? "0", 2) }}</td>
                                                <td>
                                                    @include('components.forms.checkbox', [
                                                        'name' => 'element[' . kebab_case($element->id) . ']',
                                                        'id' => 'element-' . kebab_case($element->id),
                                                        'label' => '',
                                                        'value' => $element->id,
                                                    ])
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach                                      
                    @endforeach
                    @if($totalElementPerType == 0)
                        <tr>
                            <td colspan="5">-Tiada-</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <br/>
        @endif
    @endforeach
</form>
                    
<div class="float-right btn btn-primary ppjhk-details-action">
    Kemaskini
</div>
<a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
    class="float-right btn btn-default border-primary">
    {{ __('Kembali') }}
</a> 