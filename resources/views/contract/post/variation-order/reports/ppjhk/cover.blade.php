<table width="100%" border="0" style="width: 100%;border-collapse: collapse;padding: 4 4; ">
    <tr>
        <td>
            <center><img style="width: 150px; height: 150px;" src="{{ logo($print) }}"></center>
            <br/><br/><br/><br/>
            <center>
                <span style="text-transform: uppercase; font-size: 36px;font-weight:bold;">
                    Perbadanan Putrajaya
                </span><br/><br/><br/>
                <hr/>
                <span style="text-transform: uppercase; font-size: 32px;font-weight:bold;">
                    Perakuan Pelarasan Jumlah<br/>
                    Harga Kontrak No. {{ $ppjhk->no }} 
                </span>
                <hr/>
            </center>
            <br/><br/><br/>
            <table width="100%">
                <tr>
                    <td width="5%"></td>
                    <td align="center" width="90%" style="text-align: justify; text-transform: uppercase; font-size: 26px; font-weight:bold;">
                        {{ $sst->acquisition->title }}
                    </td>
                    <td width="5%"></td>
                </tr>
                <tr><td colspan="3"><br/><br/><br/></td></tr>
                <tr>
                    <td colspan="3" style="text-align: center; text-transform: uppercase; font-size: 26px;">
                        <span style="font-weight:bold;text-decoration:underline;">KONTRAKTOR</span><br/>
                        <span style="font-weight:bold;">{{ $sst->company->company_name }} </span>
                    </td>
                </tr> 
                <tr><td colspan="3" ><br/><br/></td></tr>               
                <tr>
                    <td  colspan="3" style="text-align: center; text-transform: uppercase; font-size: 26px; ">
                        <span style="font-weight:bold;text-decoration:underline;">No. Kontrak</span><br/>
                        <span style="font-weight:bold;">{{ $sst->acquisition->reference }}</span>
                    </td>
                </tr>
				<tr><td colspan="3" ><br/><br/><br/><br/></td></tr>   
            </table>			
        </td>
    </tr>
</table>