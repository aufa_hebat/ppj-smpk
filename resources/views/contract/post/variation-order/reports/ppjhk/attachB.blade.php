<div class="breakNow"><br></div>
<div style="top: -60px; text-align:right; font-size:10px; text-transform: uppercase; text-decoration: underline;">
    Lampiran B
</div>
<table width="100%" style="border-collapse: collapse;padding: 4 4; font-size: 12px; ">
    <tr>
        <td style="text-align:left; text-transform: uppercase; font-size: 12px; padding:10px 10px;">
            {{ $ppjhk->sst->acquisition->title }}<br/><br/>
            Perakuan Pelarasan Jumlah Harga KOntrak No. {{ $ppjhk->no }}<br/>
        </td>
    </tr>
</table>
<table width="100%" class="bordered content" style="border-collapse: collapse;padding: 4 4;">
    <thead>
        <tr style="text-align:center; font-weight:bold;">
            <th rowspan="2" class="bordered">Bil.</th>
            <th rowspan="2" class="bordered">P.P.K No.</th>
            <th rowspan="2" class="bordered">Elemen</th>
            <th colspan="3" class="bordered">Total Amount</th>
            <th rowspan="2" class="bordered">Nett</th>
        </tr>
        <tr style="text-align:center; font-weight:bold;">
            <th class="bordered">BQ</th>
            <th class="bordered">PPK</th>
            <th class="bordered">PPJHK</th>
        </tr>
    </thead>
    <tbody>
        @php $bil = 0; @endphp
        @foreach($ppjhk->elements as $ppjhkElement)
            @if($ppjhkElement->voElement->vo_type->variation_order_type->id == 3)
                <tr style="text-align:center;">
                    <td class="bordered">{{ $bil = $bil + 1 }}</td>
                    <td class="bordered">{{ $ppjhkElement->voElement->vo->no }}</td>
                    <td class="bordered">{{ $ppjhkElement->voElement->element }}</td>
                    <td class="bordered">
                        @if($ppjhkElement->voElement->amount_omission > 0)
                            {{ money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered">
                        @if($ppjhkElement->voElement->amount_addition > 0)
                            {{ money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2)) }})
                        @endif
                    </td> 
                    <td class="bordered">
                        @if($ppjhkElement->amount_ppjhk > 0)
                            {{ money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered">
                        @if($ppjhkElement->net_amount > 0)
                            {{ money()->toCommon($ppjhkElement->net_amount ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->net_amount ?? "0", 2)) }})
                        @endif
                    </td>
                </tr>
            @endif
            @if($ppjhkElement->voElement->vo_type->variation_order_type->id == 4)
                <tr style="text-align:center;">
                    <td class="bordered">{{ $bil = $bil + 1 }}</td>
                    <td class="bordered">{{ $ppjhkElement->voElement->vo->no }}</td>
                    <td class="bordered">{{ $ppjhkElement->voElement->element }}</td>
                    <td class="bordered">
                        @if($ppjhkElement->voElement->amount_omission > 0)
                            {{ money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered">
                        @if($ppjhkElement->voElement->amount_addition > 0)
                            {{ money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2)) }})
                        @endif
                    </td> 
                    <td class="bordered">
                        @if($ppjhkElement->amount_ppjhk > 0)
                            {{ money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered">
                        @if($ppjhkElement->net_amount > 0)
                            {{ money()->toCommon($ppjhkElement->net_amount ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->net_amount ?? "0", 2)) }})
                        @endif
                    </td>
                </tr>
            @endif
            @if($ppjhkElement->voElement->vo_type->variation_order_type->id == 5)
                <tr style="text-align:center;">
                    <td class="bordered">{{ $bil = $bil + 1 }}</td>
                    <td class="bordered">{{ $ppjhkElement->voElement->vo->no }}</td>
                    <td class="bordered">{{ $ppjhkElement->voElement->element }}</td>
                    <td class="bordered">
                        @if($ppjhkElement->voElement->amount_omission > 0)
                            {{ money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered">
                        @if($ppjhkElement->voElement->amount_addition > 0)
                            {{ money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2)) }})
                        @endif
                    </td> 
                    <td class="bordered">
                        @if($ppjhkElement->amount_ppjhk > 0)
                            {{ money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered">
                        @if($ppjhkElement->net_amount > 0)
                            {{ money()->toCommon($ppjhkElement->net_amount ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->net_amount ?? "0", 2)) }})
                        @endif
                    </td>
                </tr>
            @endif   
        @endforeach
        @foreach($ppjhk->cancels as $cancel)
            @if(!empty($cancel->ppjhk_id))
                @if($cancel->ppjhk_id == $ppjhk->id)
                    <tr style="text-align:center;">
                        <td class="bordered">{{ $bil = $bil + 1 }}</td>
                        <td class="bordered">{{ $cancel->vo->no }}</td>
                        <td class="bordered">{{ $cancel->item->item }}</td>
                        <td class="bordered">0.0</td>
                        <td class="bordered">({{ str_replace("-", "", money()->toCommon($cancel->amount ?? "0", 2)) }})</td>
                        <td class="bordered">({{ str_replace("-", "", money()->toCommon($cancel->amount ?? "0", 2)) }})</td>
                        <td class="bordered">({{ str_replace("-", "", money()->toCommon($cancel->amount ?? "0", 2)) }})</td>
                    </tr>
                @endif
            @endif
        @endforeach
    </tbody>
</table>
@foreach($ppjhk->elements as $ppjhkElement)
    @if($ppjhkElement->voElement->vo_type->variation_order_type->id == 3)
        @php $index = 0; @endphp
        <div class="breakNow"><br></div>
        <table width="108%" style="border-collapse: collapse;padding: 4 4;" class="content">
            <tr>
                <td style="text-align:left; text-transform: uppercase; font-size: 12px; padding:10px 10px;">
                    {{ $ppjhk->sst->acquisition->title }}<br/><br/>
                    Perakuan Pelarasan Jumlah Harga KOntrak No. {{ $ppjhk->no }}<br/>
                </td>
            </tr>
        </table>
        <span style="font-size:12px;">{{ $ppjhkElement->voElement->element }}</span>
        <table width="100%" class="bordered" style="border-collapse: collapse;padding: 4 4; font-size: 10px; ">
            <thead>
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered" rowspan="2">Item</td>
                    <td class="bordered" rowspan="2" width="25%">Description</td>
                    <td class="bordered" rowspan="2">Unit</td>
                    <td class="bordered" colspan="2">Rate (RM)</td>
                    <td class="bordered" colspan="3">Quantity</td>
                    <td class="bordered" colspan="3">Total Amount (RM)</td>
                    <td class="bordered" rowspan="2">Nett (RM)</td>
                    <td class="bordered" rowspan="2">BQ Ref</td>
                </tr>    
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                    <td class="bordered">BQ</td>
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                    <td class="bordered">BQ</td>
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                </tr>
            </thead>
            <tbody>
                @foreach($ppjhkElement->ppjhkItems as $item)
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px; text-transform: uppercase; text-decoration:underline;">{{ $item->voItem->item}}</td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                    </tr>
                    @foreach($item->ppjhkSubItems as $key => $sub)
                        <tr>
                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:center;" valign="top">{{ $index = $index + 1}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->voSubItem->item}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->voSubItem->unit}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->rate_per_unit ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->quantity_omission ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->quantity_addition ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->quantity_ppjhk ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->amount_omission ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->amount_addition ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->amount_ppjhk ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                                @if($sub->net_amount < 0)
                                    ({{ str_replace("-","",money()->toCommon($sub->net_amount ?? "0", 2)) }})
                                @else
                                    {{ money()->toCommon($sub->net_amount ?? "0", 2)}}
                                @endif
                            </td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                                {{ $sub->voSubItem->status_lock }}&nbsp;{{  $sub->voSubItem->bq_reference }}
                            </td>
                        </tr>
                    @endforeach                 
                @endforeach
                <tr>
                    <td class="bordered" style="padding:10px 5px;"></td>
                    <td class="bordered" style="padding:10px 5px;" colspan="7"></td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2) }}</td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2)}}</td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2)}}</td>
                    <td class="bordered" style="padding:10px 5px;">
                        @if($ppjhkElement->net_amount < 0)
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->net_amount ?? "0", 2)) }})
                        @else
                            {{ money()->toCommon($ppjhkElement->net_amount ?? "0", 2)}}
                        @endif
                    </td>
                    <td class="bordered" style="padding:10px 5px;"></td>
                </tr>                   
            </tbody>
        </table>
    @endif
    @if($ppjhkElement->voElement->vo_type->variation_order_type->id == 4)
        @php $index = 0; @endphp
        <div class="breakNow"><br></div>
        <table width="108%" style="border-collapse: collapse;padding: 4 4;" class="content">
            <tr>
                <td style="text-align:left; text-transform: uppercase; padding:10px 10px;">
                    {{ $ppjhk->sst->acquisition->title }}<br/><br/>
                    Perakuan Pelarasan Jumlah Harga KOntrak No. {{ $ppjhk->no }}<br/>
                </td>
            </tr>
        </table>        
        <span style="font-size:12px;">{{ $ppjhkElement->voElement->element }}</span>
        <table width="100%" class="bordered" style="border-collapse: collapse;padding: 4 4; font-size: 10px; ">
            <thead>
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered" rowspan="2">Item</td>
                    <td class="bordered" rowspan="2" width="25%">Description</td>
                    <td class="bordered" rowspan="2">Unit</td>
                    <td class="bordered" colspan="2">Rate (RM)</td>
                    <td class="bordered" colspan="3">Quantity</td>
                    <td class="bordered" colspan="3">Total Amount (RM)</td>
                    <td class="bordered" rowspan="2">Nett (RM)</td>
                    <td class="bordered" rowspan="2">BQ Ref</td>
                </tr>    
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                    <td class="bordered">BQ</td>
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                    <td class="bordered">BQ</td>
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                </tr>
            </thead>
            <tbody>
                @foreach($ppjhkElement->ppjhkItems as $item)
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px; text-transform: uppercase; text-decoration:underline;">{{ $item->voItem->item}}</td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                    </tr>
                    @foreach($item->ppjhkSubItems as $key => $sub)
                        <tr>
                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:center;" valign="top">{{ $index = $index + 1}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->voSubItem->item}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->voSubItem->unit}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->rate_per_unit ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->quantity_omission ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->quantity_addition ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->quantity_ppjhk ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->amount_omission ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->amount_addition ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->amount_ppjhk ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                                @if($sub->net_amount < 0)
                                    ({{ str_replace("-","",money()->toCommon($sub->net_amount ?? "0", 2)) }})
                                @else
                                    {{ money()->toCommon($sub->net_amount ?? "0", 2)}}
                                @endif
                            </td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                                {{ $sub->voSubItem->status_lock }}&nbsp;{{  $sub->voSubItem->bq_reference }}
                            </td>
                        </tr>
                    @endforeach                 
                @endforeach
                <tr>
                    <td class="bordered" style="padding:10px 5px;"></td>
                    <td class="bordered" style="padding:10px 5px;" colspan="7"></td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2) }}</td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2)}}</td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2)}}</td>
                    <td class="bordered" style="padding:10px 5px;">
                        @if($ppjhkElement->net_amount < 0)
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->net_amount ?? "0", 2)) }})
                        @else
                            {{ money()->toCommon($ppjhkElement->net_amount ?? "0", 2)}}
                        @endif
                    </td>
                    <td class="bordered" style="padding:10px 5px;"></td>
                </tr>                   
            </tbody>
        </table>
    @endif  
    @if($ppjhkElement->voElement->vo_type->variation_order_type->id == 5)
        @php $index = 0; @endphp
        <div class="breakNow"><br></div>
        <table width="100%" style="border-collapse: collapse;padding: 4 4;" class="content">
            <tr>
                <td style="text-align:left; text-transform: uppercase; padding:10px 10px;">
                    {{ $ppjhk->sst->acquisition->title }}<br/><br/>
                    Perakuan Pelarasan Jumlah Harga KOntrak No. {{ $ppjhk->no }}<br/>
                </td>
            </tr>
        </table>        
        <span style="font-size:12px;">{{ $ppjhkElement->voElement->element }}</span>
        <table width="108%" class="bordered" style="border-collapse: collapse;padding: 4 4; font-size: 10px; ">
            <thead>
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered" rowspan="2">Item</td>
                    <td class="bordered" rowspan="2" width="25%">Description</td>
                    <td class="bordered" rowspan="2">Unit</td>
                    <td class="bordered" colspan="2">Rate (RM)</td>
                    <td class="bordered" colspan="3">Quantity</td>
                    <td class="bordered" colspan="3">Total Amount (RM)</td>
                    <td class="bordered" rowspan="2">Nett (RM)</td>
                    <td class="bordered" rowspan="2">BQ Ref</td>
                </tr>    
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                    <td class="bordered">BQ</td>
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                    <td class="bordered">BQ</td>
                    <td class="bordered">Actual</td>
                    <td class="bordered">PPJHK</td>
                </tr>
            </thead>
            <tbody>
                @foreach($ppjhkElement->ppjhkItems as $item)
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px; text-transform: uppercase; text-decoration:underline;">{{ $item->voItem->item}}</td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                    </tr>
                    @foreach($item->ppjhkSubItems as $key => $sub)
                        <tr>
                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:center;" valign="top">{{ $index = $index + 1}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->voSubItem->item}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->voSubItem->unit}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->rate_per_unit ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->rate_per_unit ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->quantity_omission ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->quantity_addition ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->quantity_ppjhk ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->amount_omission ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->voSubItem->amount_addition ?? "0", 2)}}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->amount_ppjhk ?? "0", 2) }}</td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                                @if($sub->net_amount < 0)
                                    ({{ str_replace("-","",money()->toCommon($sub->net_amount ?? "0", 2)) }})
                                @else
                                    {{ money()->toCommon($sub->net_amount ?? "0", 2)}}
                                @endif
                            </td>
                            <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                                {{ $sub->voSubItem->status_lock }}&nbsp;{{  $sub->voSubItem->bq_reference }}
                            </td>
                        </tr>
                    @endforeach                 
                @endforeach
                <tr>
                    <td class="bordered" style="padding:10px 5px;"></td>
                    <td class="bordered" style="padding:10px 5px;" colspan="7"></td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->voElement->amount_omission ?? "0", 2) }}</td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->voElement->amount_addition ?? "0", 2)}}</td>
                    <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($ppjhkElement->amount_ppjhk ?? "0", 2)}}</td>
                    <td class="bordered" style="padding:10px 5px;">
                        @if($ppjhkElement->net_amount < 0)
                            ({{ str_replace("-","",money()->toCommon($ppjhkElement->net_amount ?? "0", 2)) }})
                        @else
                            {{ money()->toCommon($ppjhkElement->net_amount ?? "0", 2)}}
                        @endif
                    </td>
                    <td class="bordered" style="padding:10px 5px;"></td>
                </tr>                   
            </tbody>
        </table>
    @endif               
@endforeach