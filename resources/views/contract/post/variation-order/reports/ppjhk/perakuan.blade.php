<center><span style="font-weight:bold; font-size:16px">Pengakuan Persetujuan Kontraktor Ke atas PPJHK No. {{ $ppjhk->no }}</span></center>
<br/><br/>
<span class="content">
    Sila akui persetujuan tuan terhadap Perakuan di atas dengan menurunkan tandatangan tuan di bawah serta 
    disaksikan dengan sempurnanya.
</span>
<br/><br/>
<span class="content">
    *&nbsp;&nbsp; Saya/Kami yang menandatangani di bawah ini mengakui iaitu *saya/kami bersetuju dengan penilaian 
    seperti ditunjuk di atas dan menerima penyelarasan kepada Jumlah Harga Kontrak seperti berikut:
</span>
<br/><br/><br/><br/>
<table width="100%" class="content">
    <tr>
        <td width="50%">
            <table width="100%" class="content">
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px; text-align:center;">Tandatangan Saksi</td></tr>
                <tr><td style="padding: 7px 7px;">Nama Penuh : ---------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">Alamat : ---------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;"><br/><br/><br/></td></tr>
                <tr><td style="padding: 7px 7px;">Tarikh : ------------------------------------</td></tr>
            </table>
        </td>
        <td width="50%">
            <table width="100%" class="content">
                    <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px; text-align:center;">Tandatangan Kontraktor</td></tr>
                <tr><td style="padding: 7px 7px;">Nama Penuh : ---------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">Alamat : ---------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td style="padding: 7px 7px;">---------------------------------------------------------</td></tr>
                <tr><td><br/><br/><br/></td></tr>
                <tr><td style="padding: 7px 7px;">Tarikh : ------------------------------------</td></tr>
            </table>
        </td>        
    </tr>
</table>
