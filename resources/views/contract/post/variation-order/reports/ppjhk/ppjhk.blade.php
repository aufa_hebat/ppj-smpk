<html>
    <head>
        <style type="text/css">
            @page {
                margin: 50px 70px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }

            .bordered-double {
                border-color: #000000;
                border-style: double;
                border-width: 3px;
            }

            title{
                font-size: 16px;
            }

            body {
                font-family: "Arial, Helvetica, sans-serif";   
            }

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
			}
        </style>
    </head>
    <body>

        @php
            $totalNettA = 0;
            $totalNettB = 0;
        @endphp

        @include('contract.post.variation-order.reports.ppjhk.cover', ['ppjhk' => $ppjhk])
        <div class="breakNow"><br></div>
        @include('contract.post.variation-order.reports.ppjhk.intro', ['ppjhk' => $ppjhk])
        <div class="breakNow"><br></div>
        @include('contract.post.variation-order.reports.ppjhk.lampiranA', ['ppjhk' => $ppjhk])
        <div class="breakNow"><br></div>
        @include('contract.post.variation-order.reports.ppjhk.approval', ['ppjhk' => $ppjhk])
        <div class="breakNow"><br></div>
        @include('contract.post.variation-order.reports.ppjhk.perakuan', ['ppjhk' => $ppjhk])
        @include('contract.post.variation-order.reports.ppjhk.attachA', ['ppjhk' => $ppjhk])
        @include('contract.post.variation-order.reports.ppjhk.attachB', ['ppjhk' => $ppjhk])
    </body>
</html>