<table width="100%" border="0" style="border-collapse: collapse; padding: 10 10; " class="content">
    <tr>
        
        <td colspan="2">1.1 &nbsp;&nbsp;&nbsp; Pelarasan disebabkan Perubahan Kerja (Lampiran A)</td>
    </tr>
    <tr><td colspan="2"><br/></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%" class="bordered" style="border-collapse: collapse;padding: 4 4;" class="content">
                <thead>
                    <tr style="text-align:center; font-weight:bold;">
                        <td rowspan="2" width="10%" class="bordered">Bil.P.K</td>
                        <td rowspan="2" width="50%" class="bordered">Huraian</td>
                        <td colspan="2" width="40%" class="bordered">Jumlah (RM)</td>
                    </tr>
                    <tr style="text-align:center">
                        <td width="20%" class="bordered">Tambahan<br/>(RM)</td>
                        <td width="20%" class="bordered">Potongan<br/>(RM)</td>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $total = 0; 
                        $totalAdd = 0;
                        $totalMinus = 0;
                    @endphp
                    @foreach($ppjhk->elements as $element) 
                        @if($element->voElement->vo_type->variation_order_type_id == '2')
                            @php $total = $total + 1; @endphp
                            <tr>
                                <td class="bordered" style="text-align:center; padding: 7px 7px;" valign="bottom">No. {{ $element->voElement->vo->no }}</td>
                                <td class="bordered" style="padding: 7px 7px;" valign="bottom">
                                    @if($total == 1) 
                                        <span style="font-weight:bold; text-decoration:underline;">Perubahan Kerja</span><br/><br/>
                                    @endif

                                    {{ $element->voElement->element }}
                                </td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                    @if($element->net_amount > 0)
                                        {{ money()->toCommon($element->net_amount ?? "0", 2) }}
                                        @php
                                            $totalAdd = $totalAdd + $element->net_amount;
                                            $totalNettA = $totalNettA + $element->net_amount;
                                        @endphp
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                    @if($element->net_amount < 0)
                                        ({{ str_replace("-","",money()->toCommon($element->net_amount ?? "0", 2)) }})
                                        @php
                                            $totalMinus = $totalMinus + $element->net_amount;
                                            $totalNettA = $totalNettA + $element->net_amount;
                                        @endphp
                                    @else
                                        -
                                    @endif
                                </td>                                
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <td class="bordered"></td>
                        <td class="bordered" style="padding: 7px 7px; text-align:center;">Jumlah</td>
                        <td class="bordered" style="padding: 7px 7px; text-align:center;">{{ money()->toCommon($totalAdd ?? "0.00", 2) }}</td>
                        <td class="bordered" style="padding: 7px 7px; text-align:center;">({{ str_replace("-","",money()->toCommon($totalMinus ?? "0.00", 2)) }})</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="bordered" style="padding: 7px 7px; text-align:center;">Jumlah Bersih</td>
                        <td class="bordered" style="padding: 7px 7px; text-align:center;">
                            @if($totalNettA > 0)
                                {{ money()->toCommon($totalNettA ?? "0", 2) }}
                            @endif
                        </td>
                        <td class="bordered" style="padding: 7px 7px; text-align:center;">
                            @if($totalNettA < 0)
                                ({{ str_replace("-", "",money()->toCommon($totalNettA ?? "0", 2)) }})
                            @endif                            
                        </td>
                    </tr>
                </tbody>

            </table>
        </td>
    </tr>
    <tr><td colspan="2"><br/></td></tr>
</table>

@include('contract.post.variation-order.reports.ppjhk.lampiranB', ['ppjhk' => $ppjhk])
