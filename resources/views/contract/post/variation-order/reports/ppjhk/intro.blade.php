<table width="100%" border="0" style="width: 100%; border-collapse: collapse; padding: 10 10; line-height: 1.6 " class="content">
    <tr>
        <td colspan="4">
            <center><img style="width: 120px; height: 120px;" src="{{ logo($print) }}"></center>			
            <br/>
            <center>
                <span style="text-transform: uppercase; font-size: 22px;font-weight:bold;">
                    Perbadanan Putrajaya
                </span><br/><br/><br/>
            </center>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="text-transform: uppercase; font-size: 16px;font-weight:bold;">
            Perakuan Pelarasan Jumlah Harga Kontrak No. {{ $ppjhk->no }}<br/>
        </td>
    </tr>
    <tr><td colspan="4"><br/></td></tr>
    <tr>
        <td width="50%"></td>
        <td width="10%" valign="top">Pejabat</td>
        <td width="3%" valign="top"> : </td>
        <td width="37%">
            Perbadanan Putrajaya<br/>
            Kompleks Perbadanan Putrajaya<br/>
            24, Persiaran Perdana, Presint 3<br/>
            62675 Putrajaya<br/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>Tarikh</td>
        <td> : </td>
        <td></td>
    </tr>
    <tr><td colspan="4"><br/><br/></td></tr>
    <tr>
        <td colspan="4">
            <table>
                <tr>
                    <td valign="top">Kepada : </td>
                    <td>
                        {{ $ppjhk->sst->company->company_name }}<br/>
                        @if(!empty($ppjhk->sst->company->addresses[0]))
                            @if(!empty($ppjhk->sst->company->addresses[0]->primary))
                                {{ $ppjhk->sst->company->addresses[0]->primary }}<br>
                            @endif
                            @if(!empty($ppjhk->sst->company->addresses[0]->secondary))
                                {{ $ppjhk->sst->company->addresses[0]->secondary }}<br>
                            @endif
                            @if(!empty($ppjhk->sst->company->addresses[0]->postcode))
                                {{ $ppjhk->sst->company->addresses[0]->postcode }}, 
                                {{ $ppjhk->sst->company->addresses[0]->state }}   
                            @endif
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="4"><br/></td></tr>
    <tr>
        <td colspan="4">
            <table width="100%">
                <tr>
                    <td width="25%" style="text-transform:uppercase;" valign="top">No. Kontrak</td>
                    <td width="3%" valign="top"> : </td>
                    <td width="72%">
                        <span style="font-weight:bold;">{{ $ppjhk->sst->acquisition->reference }}</span>
                    </td>
                </tr>
                <tr>
                    <td style="text-transform:uppercase;" valign="top">Kontrak Untuk</td>
                    <td valign="top"> : </td>
                    <td>
                        <span style="font-weight:bold;text-decoration:underline;text-transform:uppercase;">
                            {{ $ppjhk->sst->acquisition->title }}
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="4"><br/></td></tr>
    <tr>
        <td colspan="4">
            <table width="100%">
                <tr>
                    <td width="50%" style="text-transform:uppercase;">Jumlah Harga Kontrak Asal</td>
                    <td width="5%"> : </td>
                    <td width="45%">{{ money()->toHuman($appointed->offered_price ?? "0", 2)}}</td>
                </tr>
                @php $total = $appointed->offered_price; @endphp
                @if($ppjhks->count() > 0)
                    @foreach($ppjhks as $ppjhkD)
                        @if($ppjhkD->id != $ppjhk->id)
                            <tr>
                                <td style="text-transform:uppercase;">
                                    @if(($ppjhkD->elements->pluck('net_amount')->sum() + $ppjhkD->cancels->pluck('amount')->sum())  > 0) Tambahan 
                                    @else Potongan @endif
                                    P.P.J.H.K. No. {{ $ppjhkD->no }}
                                </td>
                                <td> : </td>
                                <td>
                                    {{ money()->toHuman($ppjhkD->elements->pluck('net_amount')->sum() + $ppjhkD->cancels->pluck('amount')->sum() ?? "0", 2) }}
                                </td>
                            </tr>
                            @php $total = $total + ($ppjhkD->elements->pluck('net_amount')->sum() + $ppjhkD->cancels->pluck('amount')->sum()); @endphp
                        @endif
                    @endforeach 
                    
                    <tr>
                        <td style="text-transform:uppercase;">Jumlah Harga Kontrak Baru</td>
                        <td> : </td>
                        <td>{{ money()->toHuman($total ?? "0", 2)  }}</td>
                    </tr>
                @endif
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <table width="100%" style="text-align:justify; padding: 10px 10px;">
                <tr>
                    <td width="7%" valign="top">1.</td>
                    <td>
                        Menurut klausa-klausa yang berkenaan dalam syarat-syarat Kontrak, sejumlah wang 
                        <span style="text-transform: capitalize; ">Ringgit Malaysia :
                                
                            @php
                                $netAmount = $ppjhk->elements->pluck('net_amount')->sum() +  $ppjhk->cancels->pluck('amount')->sum();

                                $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                                $amaun = str_replace("-","",money()->toCommon($netAmount ?? "0" , 2));
                                $format = explode('.',$amaun);
                                $formats = substr($amaun,-2);
                                $partringgit = str_replace(',', '', $format[0]);
                                $partsen = $format[1];
                                $partkosongsen = (int)$formats;
                
                                if($partkosongsen == '.00'){
                                    echo '<span style="text-transform: capitalize; text-decoration: underline; text-decoration-style: dotted;">' . $f->format($partringgit) . " Sahaja" . '</span>';
                                }
                                else{
                                    echo '<span style="text-transform: capitalize; text-decoration: underline; text-decoration-style: dotted;">' . $f->format($partringgit) . " Dan " . $f->format($partkosongsen) . " Sen Sahaja" . '</span>';
                                }
                
                            @endphp					
                                
                            ({{ money()->toHuman($netAmount ?? "0" , 2) }}	)</span> 
                            adalah @if($netAmount > 0) ditambah kepada @else dipotong daripada @endif Jumlah Harga Kontrak seperti berikut:
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>