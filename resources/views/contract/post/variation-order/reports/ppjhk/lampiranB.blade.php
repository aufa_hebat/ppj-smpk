<table width="100%" border="0" style="border-collapse: collapse; padding: 10 10; " class="content">
    <tr>
        <td colspan="2">1.2 &nbsp;&nbsp;&nbsp; Pelarasan-pelarasan lain (Perkiraan Kenaikan Harga, Pelarasan Wang Kos Prima, Pengukuran Semula dsb.) (Lampiran B)</td>
        </tr>
        <tr><td colspan="2"><br/></td></tr>
        <tr>
            <td colspan="2">
                <table width="100%" class="bordered" style="border-collapse: collapse;padding: 4 4;" class="content">
                    <thead>
                        <tr style="text-align:center; font-weight:bold;">
                            <td rowspan="2" width="10%" class="bordered">Bil.P.K</td>
                            <td rowspan="2" width="50%" class="bordered">Huraian</td>
                            <td colspan="2" width="40%" class="bordered">Jumlah (RM)</td>
                        </tr>
                        <tr style="text-align:center">
                            <td width="20%" class="bordered">Tambahan<br/>(RM)</td>
                            <td width="20%" class="bordered">Potongan<br/>(RM)</td>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                            $totalPS = 0; 
                            $totalWPS = 0; 
                            $totalKKK = 0; 
                            $totalAdd = 0;
                            $totalMinus = 0;
                        @endphp
                        @foreach($ppjhk->cancels as $cancel)
                            @php $totalWPS = $totalWPS + 1; @endphp
                            <tr>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">No. {{ $cancel->vo->no }}</td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                    @if($totalWPS == 1) 
                                        <span style="font-weight:bold; text-decoration:underline;">Wang Peruntukan Sementara Dan Kos Prima (Provisional Sum)</span><br/><br/>
                                    @endif

                                    {{ $cancel->item->item }}
                                </td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                    @if($cancel->amount > 0)
                                        {{ money()->toCommon($cancel->amount ?? "0", 2) }}
                                        @php
                                            $totalAdd = $totalAdd + $cancel->amount;
                                            $totalNettB = $totalNettB + $cancel->amount;
                                        @endphp
                                    @else
                                        -
                                    @endif
                                </td> 
                                <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                    @if($cancel->amount < 0)
                                        ({{ str_replace("-","",money()->toCommon($cancel->amount ?? "0", 2)) }})
                                        @php
                                            $totalMinus = $totalMinus + $cancel->amount;
                                            $totalNettB = $totalNettB + $cancel->amount;
                                        @endphp
                                    @else
                                        -
                                    @endif
                                </td>                                                                                                
                            </tr>
                        @endforeach
                        @foreach($ppjhk->elements as $element) 
                            @if($element->voElement->vo_type->variation_order_type_id == '3')
                                @php $totalWPS = $totalWPS + 1; @endphp
                                <tr>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">No. {{ $element->voElement->vo->no }}</td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($totalWPS == 1) 
                                            <span style="font-weight:bold; text-decoration:underline;">Wang Peruntukan Sementara Dan Kos Prima (Provisional Sum)</span><br/><br/>
                                        @endif

                                        {{ $element->voElement->element }}
                                    </td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($element->net_amount > 0)
                                            {{ money()->toCommon($element->net_amount ?? "0", 2) }}
                                            @php
                                                $totalAdd = $totalAdd + $element->net_amount;
                                                $totalNettB = $totalNettB + $element->net_amount;
                                            @endphp
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($element->net_amount < 0)
                                            ({{ str_replace("-","",money()->toCommon($element->net_amount ?? "0", 2)) }})
                                            @php
                                                $totalMinus = $totalMinus + $element->net_amount;
                                                $totalNettB = $totalNettB + $element->net_amount;
                                            @endphp
                                        @else
                                            -
                                        @endif
                                    </td>                                
                                </tr>
                            @endif                        
                            @if($element->voElement->vo_type->variation_order_type_id == '4')
                                @php $totalPS = $totalPS + 1; @endphp
                                <tr>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">No. {{ $element->voElement->vo->no }}</td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($totalPS == 1) 
                                            <span style="font-weight:bold; text-decoration:underline;">Pengiraan Semula</span><br/><br/>
                                        @endif
    
                                        {{ $element->voElement->element }}
                                    </td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($element->net_amount > 0)
                                            {{ money()->toCommon($element->net_amount ?? "0", 2) }}
                                            @php
                                                $totalAdd = $totalAdd + $element->net_amount;
                                                $totalNettB = $totalNettB + $element->net_amount;
                                            @endphp
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($element->net_amount < 0)
                                            ({{ str_replace("-","",money()->toCommon($element->net_amount ?? "0", 2)) }})
                                            @php
                                                $totalMinus = $totalMinus + $element->net_amount;
                                                $totalNettB = $totalNettB + $element->net_amount;
                                            @endphp
                                        @else
                                            -
                                        @endif
                                    </td>                                
                                </tr>
                            @endif
                            @if($element->voElement->vo_type->variation_order_type_id == '5')
                                @php $totalKKK = $totalKKK + 1; @endphp
                                <tr>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">No. {{ $element->voElement->vo->no }}</td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($totalKKK == 1) 
                                            <span style="font-weight:bold; text-decoration:underline;">Kesilapan Keterangan Kerja dan Kuantiti</span><br/><br/>
                                        @endif

                                        {{ $element->voElement->element }}
                                    </td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($element->net_amount > 0)
                                            {{ money()->toCommon($element->net_amount ?? "0", 2) }}
                                            @php
                                                $totalAdd = $totalAdd + $element->net_amount;
                                                $totalNettB = $totalNettB + $element->net_amount;
                                            @endphp
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="bordered" style="padding: 7px 7px; text-align:center;" valign="bottom">
                                        @if($element->net_amount < 0)
                                            ({{ str_replace("-","",money()->toCommon($element->net_amount ?? "0", 2)) }})
                                            @php
                                                $totalMinus = $totalMinus + $element->net_amount;
                                                $totalNettB = $totalNettB + $element->net_amount;
                                            @endphp
                                        @else
                                            -
                                        @endif
                                    </td>                                
                                </tr>
                            @endif                            
                        @endforeach
                        <tr>
                            <td class="bordered"></td>
                            <td class="bordered" style="padding: 7px 7px; text-align:center;">Jumlah</td>
                            <td class="bordered" style="padding: 7px 7px; text-align:center;">{{ money()->toCommon($totalAdd ?? "0.00", 2) }}</td>
                            <td class="bordered" style="padding: 7px 7px; text-align:center;">({{ str_replace("-","",money()->toCommon($totalMinus ?? "0.00", 2)) }})</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bordered" style="padding: 7px 7px; text-align:center;">Jumlah Bersih</td>
                            <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                @if($totalNettB > 0)
                                    {{ money()->toCommon($totalNettB ?? "0", 2) }}
                                @endif
                            </td>
                            <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                @if($totalNettB < 0)
                                    ({{ str_replace("-","",money()->toCommon($totalNettB ?? "0", 2)) }})
                                @endif                            
                            </td>
                        </tr>
                    </tbody>
    
                </table>
            </td>
        </tr>
        <tr><td colspan="2"><br/></td></tr>
        <tr>
            <td colspan="2">
                Arahan-arahan Wakil Perbadanan dan butir-butir perkiraan adalah dikembarkan bersama-sama ini.
            </td>
        </tr>
    </table>
    <div class="breakNow"><br></div>
    @include('contract.post.variation-order.reports.ppjhk.lampiranC', ['ppjhk' => $ppjhk])