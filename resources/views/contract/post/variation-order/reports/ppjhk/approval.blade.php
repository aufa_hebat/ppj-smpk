@php
    $netAmount = $ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum();
@endphp

<table width="100%" border="0" style="width: 100%; border-collapse: collapse; padding: 10 10; " class="content">
    <tr>
        <td colspan="2">
            Dengan jumlah @if($netAmount > 0)tambahan @else potongan @endif di atas, Jumlah Harga Baru Kontrak 
            sehingga dan termasuk Perakuan ini adalah seperti berikut:

            <br/><br/><br/>
            <table width="100%" class="content">
                <tr>
                    <td width="60%">Jumlah Harga Kontrak Asal</td>
                    <td width="3%">RM</td>
                    <td width="37%">{{ money()->toCommon($appointed->offered_price ?? "0", 2)}}</td>
                </tr>
                @php $total = $appointed->offered_price; @endphp
                @foreach($ppjhks as $ppjhkD)
                    @if($ppjhkD->id != $ppjhk->id)
                    <tr>
                        <td>
                            @if($ppjhkD->elements->pluck('net_amount')->sum() + $ppjhkD->cancels->pluck('amount')->sum() > 0) Tambahan 
                            @else Potongan @endif
                            
                            P.P.J.H.K. No. {{ $ppjhkD->no }}
                        </td>
                        <td>RM</td>
                        <td> 
                            {{ money()->toCommon($ppjhkD->elements->pluck('net_amount')->sum() + $ppjhkD->cancels->pluck('amount')->sum() ?? "0", 2) }}
                            @php $total = $total + ($ppjhkD->elements->pluck('net_amount')->sum() + $ppjhkD->cancels->pluck('amount')->sum()); @endphp
                        </td>
                    </tr>
                    @endif
                @endforeach
                <tr>
                    <td>
                        @if($ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum() > 0) Tambahan 
                        @else Potongan @endif
                            
                        P.P.J.H.K. No. {{ $ppjhk->no }}
                    </td>
                    <td>RM</td>
                    <td> 
                        {{ money()->toCommon($ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum() ?? "0", 2) }}
                        @php $total = $total + ($ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum()); @endphp
                    </td>
                </tr>                
                <tr>
                    <td>Jumlah Harga Kontrak Baru</td>
                    <td>RM</td>
                    <td>{{ money()->toCommon($total ?? "0.00", 2)  }}</td>
                </tr>
            </table>
        </td>        
    </tr>
    <tr><td colspan="2"><br/><br/><br/></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%" class="content">
                <tr>
                    <td width="5%" valign="top">1</td>
                    <td width="95%" style="text-align:justify;">
                        Sila akui penerimaan Perakuan ini dan mengembalikan semua salinannya (kecuali satu salinan untuk simpanan tuan) 
                        kepada Wakil Perbadanan, setelah ditandatangani sekiranya dipersetujui oleh tuan dalam tempoh dua (2) minggu 
                        dari tarikh penerimaan.  Jika kontraktor gagal berbuat demikian, pejabat ini akan meneruskan dengan tindakan 
                        penyediaan perakuan muktamad.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="2"><br/><br/><br/></td></tr>
    <tr>
        <td colspan="2">
            <table width="50%" class="content">
                <tr><td style="padding:10px 10px">---------------------------------------------------------------</td></tr>
                <tr><td style="padding:5px 5px; text-align:center;">Tandatangan Wakil Perbadanan</td></tr>
                <tr><td style="padding:10px 10px">Nama Penuh : ----------------------------------------------</td></tr>
                <tr><td style="padding:10px 10px">Jawatan : -------------------------------------------------</td></tr>
            </table>
        </td>
    </tr>
</table>