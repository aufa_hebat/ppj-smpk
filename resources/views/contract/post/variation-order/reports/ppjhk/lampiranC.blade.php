<table width="100%" border="0" style="border-collapse: collapse; padding: 10 10; " class="content">
    <tr>
        <td colspan="2">1.3 &nbsp;&nbsp;&nbsp; Pelarasan Jumlah Harga Kontrak</td>
        </tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2">
                    <table width="100%" class="bordered" style="border-collapse: collapse;padding: 4 4;" class="content">
                        <thead>
                            <tr style="text-align:center; font-weight:bold;">
                                <td rowspan="2" width="5%" class="bordered">Bil.</td>
                                <td rowspan="2" width="55%" class="bordered">Huraian</td>
                                <td colspan="2" width="40%" class="bordered">Jumlah (RM)</td>
                            </tr>
                            <tr style="text-align:center">
                                <td width="20%" class="bordered">Tambahan<br/>(RM)</td>
                                <td width="20%" class="bordered">Potongan<br/>(RM)</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">1.</td>
                                <td class="bordered" style="padding: 7px 7px; text-align:left;">Jumlah bersih seperti di perenggan 1.1 di atas</td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                    @if($totalNettA > 0)
                                        {{ money()->toCommon($totalNettA ?? "0", 2) }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                    @if($totalNettA < 0)
                                        ({{ str_replace("-","",money()->toCommon($totalNettA ?? "0", 2)) }})
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">2.</td>
                                <td class="bordered" style="padding: 7px 7px; text-align:left;">Jumlah bersih seperti di perenggan 1.2 di atas</td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                    @if($totalNettB > 0)
                                        {{ money()->toCommon($totalNettB ?? "0", 2) }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                    @if($totalNettB < 0)
                                        ({{ str_replace("-","",money()->toCommon($totalNettB ?? "0", 2)) }})
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            @php
                                $total = $totalNettA + $totalNettB;
                            @endphp
                            <tr>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;"></td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">Jumlah</td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                    @if($total > 0)
                                        {{ money()->toCommon($total ?? "0", 2) }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="bordered" style="padding: 7px 7px; text-align:center;">
                                    @if($total < 0)
                                        ({{ str_replace("-","",money()->toCommon($total ?? "0", 2)) }})
                                    @else
                                        -
                                    @endif
                                </td>                                
                            </tr> 
                        </tbody>
        
                    </table>
                </td>
            </tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2">
                    Arahan-arahan Wakil Perbadanan dan butir-butir perkiraan adalah dikembarkan bersama-sama ini.
                </td>
            </tr>
        </table>