<div class="breakNow"><br></div>
<table width="100%">
    <tr>
        <td style="text-align:left; text-transform: uppercase; font-size: 12px; padding:5px 5px;">
            {{ $vo->sst->acquisition->title }}<br/><br/>
            Permohonan Perubahan Kerja No. {{ $vo->no }}
        </td>
    </tr>
    <tr>
        <td style="text-align:left; font-size: 12px; padding:5px 5px;">
            {{ $el->element}}
        </td>
    </tr>
</table>
@php $index = 0; @endphp
<table border="0" style="width: 98%;border-collapse: collapse;padding: 4 4; font-size:11px;" class="bordered" align="center">
    <thead>
        <tr style="background-color: #d9d9d9; text-align: center;">
            <td class="bordered" rowspan="2" width="3%">Item</td>
            <td class="bordered" rowspan="2" width="15%">Description</td>
            <td class="bordered" rowspan="2" width="5%">Unit</td>
            <td class="bordered" rowspan="2" width="5%">Rate (RM)</td>
            <td class="bordered" colspan="2" width="10%">Quantity</td>
            <td class="bordered" colspan="2" width="15%">Total Amount</td>
            <td class="bordered" rowspan="2" width="5%">Nett (RM)</td>
            <td class="bordered" rowspan="2" width="5%">BQ Reference</td>
        </tr>    
        <tr style="background-color: #d9d9d9; text-align: center;">
            <td class="bordered">BQ</td>
            <td class="bordered">Actual</td>
            <td class="bordered">BQ</td>
            <td class="bordered">Actual</td>
        </tr>
    </thead>
    <tbody>
        @foreach($el->items as $item)
            <tr>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px; text-transform: uppercase; text-decoration:underline;">{{ $item->item}}</td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
            </tr>
            @foreach($item->subItems as $key => $sub)
                <tr>
                    <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:center;" valign="top">{{ $index = $index + 1}}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->item}}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->unit}}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->rate_per_unit ?? "0", 2)}}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->quantity_omission ?? "0", 2) }}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->quantity_addition ?? "0", 2) }}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->amount_omission ?? "0", 2) }}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ money()->toCommon($sub->amount_addition ?? "0", 2)}}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">
                        @if($sub->net_amount < 0)
                            ({{ str_replace("-","",money()->toCommon($sub->net_amount ?? "0", 2)) }})
                        @else
                            {{ money()->toCommon($sub->net_amount ?? "0", 2)}}
                        @endif
                    </td>
                    <td style="border-right: 1px solid #959594; padding:10px 5px;" valign="top">{{ $sub->status_lock }}<br/>&nbsp;{{  $sub->bq_reference }}</td>
                </tr>
            @endforeach
            {{--  <tr>
                <td class="bordered" style="padding:10px 5px;"></td>
                <td class="bordered" style="padding:10px 5px;" colspan="3"></td>
                <td class="bordered" style="padding:10px 5px;" colspan="2"></td>
                <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($item->amount_omission ?? "0", 2) }}</td>
                <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($item->amount_addition ?? "0", 2)}}</td>
                <td class="bordered" style="padding:10px 5px;">
                    @if($item->net_amount < 0)
                        ({{ str_replace("-","",money()->toCommon($item->net_amount ?? "0", 2)) }})
                    @else
                        {{ money()->toCommon($item->net_amount ?? "0", 2)}}
                    @endif
                </td>
                <td class="bordered" style="padding:10px 5px;"></td>
            </tr>  --}}
        @endforeach
        <tr>
            <td class="bordered" style="padding:10px 5px;"></td>
            <td class="bordered" style="padding:10px 5px;" colspan="3"></td>
            <td class="bordered" style="padding:10px 5px;" colspan="2"></td>
            <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($el->amount_omission ?? "0", 2) }}</td>
            <td class="bordered" style="padding:10px 5px;">{{ money()->toCommon($el->amount_addition ?? "0", 2)}}</td>
            <td class="bordered" style="padding:10px 5px;">
                @if($el->net_amount < 0)
                    ({{ str_replace("-","",money()->toCommon($el->net_amount ?? "0", 2)) }})
                @else
                    {{ money()->toCommon($el->net_amount ?? "0", 2)}}
                @endif
            </td>
            <td class="bordered" style="padding:10px 5px;"></td>
        </tr>        
    </tbody>
</table>

