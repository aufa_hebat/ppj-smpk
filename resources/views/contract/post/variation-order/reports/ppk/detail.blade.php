{{--  <style type="text/css">
        thead {
            display: table-header-group;
        }
        tfoot {
            display: table-row-group;
        }
        tr {
            page-break-inside: avoid;
        }
</style>  --}}
@php
    $letter = collect(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']);
    $roman = collect(['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix', 'x']);
@endphp
<table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center" page-break-inside: auto>
    <tr>
        <td style="text-align:center;">
            <span style="text-transform:uppercase; font-size:14px;">
                Perbadanan Putrajaya<br/>Permohonan Perubahan Kerja No. {{ $vo->no }}<br/>
            </span>
            <span style="text-transform:capitalize; font-size:10px; font-style:italic;">
                (Dokumen Bahagian Perolehan &amp; Ukur Bahan ini untuk edaran dalaman sahaja)
            </span>
        </td>
    </tr>
</table>
                     
<div style="font-size:12px; border:1; padding: 5px 5px;">
    BAHAGIAN B : MAKLUMAT MENGENAI PERUBAHAN KERJA YANG DIPOHON 
    <table border="0" style="width: 98%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
        <thead>
            <tr style="text-align:center;background-color: #d9d9d9;">
                <td width="4%" class="bordered" rowspan="2">Bil.</td>
                <td width="30%" class="bordered" rowspan="2">Huraian Perubahan Kerja</td>
                <td width="30%" class="bordered" rowspan="2">Sebab Perubahan Diperlukan</td>
                <td width="36%" class="bordered" colspan="4">Anggaran (RM)*</td>                    
            </tr>
            <tr style="text-align:center;background-color: #d9d9d9;">
                <td width="9%" class="bordered">Tambahan (a)</td>
                <td width="9%" class="bordered">%</td>
                <td width="9%" class="bordered">Kurangan (b)</td>
                <td width="9%" class="bordered">%</td>
            </tr>
        </thead>
        <tbody> 
            @php
                $percentJumlahBersih = 0;
                $jumlahBersihAd = 0;
                $jumlahBersihOm = 0;
            @endphp

            @foreach($types as $keyT => $type)                                     
                <tr>
                    <td style="border-right: 1px solid #959594; border-left: 1px solid #959594; padding:10px 10px;" valign="top">{{ $letter[$keyT] }}</td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;">
                        <span style="text-transform:uppercase;text-decoration:underline;">{{ $type->name }}</span>                                
                    </td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                    <td style="border-right: 1px solid #959594; padding:10px 10px;"></td>
                </tr>
                @php
                    $totalOmission = 0.0;
                    $totalAddition = 0.0;
                    $total = 0.0;
                    $totalElement = 0;
                    $totalNettOmission = 0;
                    $totalNettAddition = 0;
                    $percentNettOmission = 0;
                    $percentNettAddition = 0;
                @endphp

                @foreach($vo->types as $vot)
                    @if($vot->variation_order_type_id == $type->id || ($vot->variation_order_type_id == 1 && $type->id == 3) || ($vot->variation_order_type_id == 6 && $type->id == 3))
                        @if($vot->variation_order_type_id == 1)
                            @foreach($vo->actives as $key=>$active)
                                @php
                                    $totalElement = $totalElement + 1;
                                @endphp
                                <tr>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" valign="top">{{ $roman[$totalElement - 1] }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">
                                        {{ $active->item->item }}
                                        <br/>
                                        ({{ money()->toHuman($active->item->amount ?? "0", 2 ) }})
                                    </td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">
                                        Peruntukan sementara untuk diaktifkan
                                    </td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00%</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00%</td> 
                                </tr>
                            @endforeach
                        @endif

                        @if($vot->variation_order_type_id == 6)
                            @foreach($vo->cancels as $key=>$cancel)


                                @foreach($wpsElements as $wpsElement)
                                    @if($wpsElement->actives->bq_item_id == $cancel->bq_item_id)
                                        @php $totalElement = $totalElement + 1; @endphp
                                        <tr>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" valign="top">{{ $roman[$totalElement - 1] }}</td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">{{ $wpsElement->element }}</td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">
                                                Pelarasan untuk <span style="text-transform:capitalize; font-size:10px; font-style:italic;">{{ $cancel->item->item }}</span>
                                            </td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">
                                                {{ money()->toCommon( $wpsElement->net_amount ?? "0", 2) }}
                                            </td> 
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top"></td> 
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top"></td>
                                        </tr>
                                    @endif
                                @endforeach

                                @php
                                    $totalElement = $totalElement + 1;
                                @endphp

                                <tr>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" valign="top">{{ $roman[$totalElement - 1] }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">{{ $cancel->item->item }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top">Peruntukan tidak digunakan</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">
                                        @php $totalNettOmission = $totalNettOmission +  $cancel->amount; @endphp
                                        ({{ str_replace("-","",money()->toCommon($cancel->amount ?? "0", 2 )) }})                                        
                                    </td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top">0.00</td> 
                                </tr>
                                @php
                                    $totalAddition = $totalAddition + $cancel->amount;
                                    $total = $total + $cancel->amount;                                        
                                @endphp
                            @endforeach
                        @endif

                        @if($vot->variation_order_type_id == 3)
                            @php $totalBalanced = 0; @endphp
                            @foreach($Elemen as $key=>$el)
                                @if($el->vo_type_id == $vot->id)
                                    @php
                                        $totalElement = $totalElement + 1;
                                    @endphp
                                    <tr>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px; text-align: center;" valign="top">{{ $roman[$totalElement - 1] }}</td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top">
                                            <span>{{ $el->element }}</span>
                                            <br/>
                                            @php
                                                $used = 0;
                                                foreach($el->items as $item){
                                                    $used = $used + $item->subItems()->where('wps_status', '1')->pluck('net_amount')->sum();
                                                }
                                            @endphp
                                            {{--  ({{ money()->toHuman($used ?? "0", 2 ) }})  --}}
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"><span>{{ $el->description }}</span></td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                            @php
                                                $balanced = 0;
                                                foreach($el->items as $item){
                                                    $balanced = $balanced + $item->subItems()->where('wps_status', '0')->pluck('net_amount')->sum();
                                                }

                                                $totalBalanced = $totalBalanced + $balanced;
                                            @endphp

                                            {{--  @if($balanced > 0)
                                                <span>{{ money()->toCommon($balanced ?? "0", 2) }}</span>
                                            @endif  --}}

                                            @if($el->amount_addition > $el->amount_omission)
                                                <span>{{ money()->toCommon($el->net_amount ?? "0", 2) }}</span>
                                                @php $totalNettAddition = $totalNettAddition +  $el->net_amount; @endphp
                                            @endif                                            

                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                            @if($el->amount_addition > $el->amount_omission)
                                                <span>{{ $el->percent_addition }}</span>
                                            @endif
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                            @if($el->amount_addition < $el->amount_omission)
                                                <span>({{ str_replace("-", "",money()->toCommon($el->net_amount ?? "0", 2)) }})</span>
                                                @php $totalNettOmission = $totalNettOmission +  $el->net_amount; @endphp
                                            @endif
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top">
                                            @if($el->amount_addition < $el->amount_omission)
                                                <span>({{ str_replace("-", "",$el->percent_addition) }})</span>
                                            @endif    
                                        </td>                                        
                                    </tr>
                                    @php
                                        $totalOmission = $totalOmission + $el->amount_omission;
                                        $totalAddition = $totalAddition + $el->amount_addition;
                                                                                
                                        /*$total = $totalBalanced;*/
                                        $total = $total + $el->net_amount;   
                                    @endphp
                                @endif
                            @endforeach
                        @endif

                        @if($vot->variation_order_type_id == 2 || $vot->variation_order_type_id == 4 || $vot->variation_order_type_id == 5)
                            @foreach($Elemen as $key=>$el)
                                @if($el->vo_type_id == $vot->id)
                                    @php
                                        $totalElement = $totalElement + 1;
                                    @endphp
                                    <tr>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px; text-align: center;" valign="top">{{ $roman[$totalElement - 1] }}</td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"><span>{{ $el->element }}</span></td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;" valign="top"><span>{{ $el->description }}</span></td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                            @if($el->amount_addition > $el->amount_omission)
                                                <span>{{ money()->toCommon($el->net_amount ?? "0", 2) }}</span>
                                                @php $totalNettAddition = $totalNettAddition +  $el->net_amount; @endphp
                                            @endif
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                            @if($el->amount_addition > $el->amount_omission)
                                                <span>{{ $el->percent_addition }}</span>
                                                
                                            @endif
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px;text-align:right;" valign="top">
                                            @if($el->amount_addition < $el->amount_omission)
                                                <span>({{ str_replace("-", "",money()->toCommon($el->net_amount ?? "0", 2)) }})</span>
                                                @php $totalNettOmission = $totalNettOmission +  $el->net_amount; @endphp
                                            @endif
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:7px 5px; text-align:right;" valign="top">
                                            @if($el->amount_addition < $el->amount_omission)
                                                <span>({{ str_replace("-", "",$el->percent_addition) }})</span>
                                            @endif    
                                        </td>                                        
                                    </tr>

                                    @php
                                        $totalOmission = $totalOmission + $el->amount_omission;
                                        $totalAddition = $totalAddition + $el->amount_addition;
                                        $total = $total + $el->net_amount;                                        
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                    @endif
                @endforeach
                @if($totalElement == 0 && ($type->id != 1 || $type->id != 6))
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"><span> - Tiada - </span></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;" valign="top"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;" valign="top"></td>                                        
                    </tr>
                @endif
                <tr>
                    <td colspan="3" class="bordered" style="text-align:left; padding:7px 7px;">Jumlah: Tambahan / Pengurangan</td>
                    <td class="bordered" style="text-align:right;">{{ money()->toCommon($totalNettAddition ?? "0", 2)}}</td>
                    <td class="bordered" style="text-align:right;">
                        @php
                            $percentNettAddition = ($totalNettAddition / $appointed->offered_price) * 100;
                            $percentNettAddition = number_format($percentNettAddition, 2, '.', '');
                            
                            $jumlahBersihAd = $jumlahBersihAd + $totalNettAddition;
                        @endphp
                        {{ $percentNettAddition }}
                    </td>
                    <td class="bordered" style="text-align:right;">
                        @if($totalNettOmission < 0)
                            ({{ str_replace("-", "",money()->toCommon($totalNettOmission ?? "0", 2)) }})
                        @else
                            {{ str_replace("-", "",money()->toCommon($totalNettOmission ?? "0", 2)) }}
                        @endif
                    </td>
                    <td class="bordered" style="text-align:right;">
                        @php
                            $percentNettOmission = ($totalNettOmission / $appointed->offered_price) * 100;
                            $percentNettOmission = number_format($percentNettOmission, 2, '.', '');

                            $jumlahBersihOm = $jumlahBersihOm + $totalNettOmission;
                        @endphp
                        {{ $percentNettOmission }}  
                    </td>                    
                </tr>
                @if($type->id != 2)
                <tr>
                    <td colspan="3" class="bordered" style="text-align:left; padding:10px 10px;">Jumlah Bersih: 
                        @if($totalAddition < $totalOmission) 
                            <span style="text-decoration: line-through;">Tambahan</span> / Pengurangan
                        @elseif($totalAddition > $totalOmission) 
                            Tambahan / <span style="text-decoration: line-through;">Pengurangan</span>
                        @else
                            Tambahan / Pengurangan
                        @endif
                    </td>
                    <td class="bordered" style="text-align:right;">
                        @if($totalAddition > $totalOmission)
                            {{ money()->toCommon($total ?? "0", 2) }}
                        @else                            
                            0.00
                        @endif
                    </td>
                    <td class="bordered" style="text-align:right;">
                        @if($totalAddition > $totalOmission)
                            @php
                                $percentNettAddition = ($total / $appointed->offered_price) * 100;
                                $percentNettAddition = number_format($percentNettAddition, 2, '.', '');
                            @endphp
                            {{ $percentNettAddition }}  
                        @else
                            0.00
                        @endif
                    </td>
                    <td class="bordered" style="text-align:right;">
                        @if($totalAddition < $totalOmission)
                           ({{ str_replace("-", "",money()->toCommon($total ?? "0", 2)) }})
                        @else
                            0.00
                        @endif
                    </td>
                    <td class="bordered"style="text-align:right;">
                        @if($totalAddition < $totalOmission)
                            @php
                                $percentNettOmission = ($total / $appointed->offered_price) * 100;
                                $percentNettOmission = number_format($percentNettOmission, 2, '.', '');
                            @endphp
                            ( {{ str_replace("-", "", $percentNettOmission) }} ) 
                        @else 
                            0.00
                        @endif
                    </td>                        
                </tr>
                @endif

                @if($type->id == 2)
                    <tr>
                        <td colspan="3" class="bordered" style="font-size:11px;padding:7px 7px;">Jumlah terkini Perubahan Kerja (Tambahan Sahaja): Terkumpul PPK No. 1 sehingga PPK No. {{$vo->no}} (terkini) </td>
                        <td colspan="3" class="bordered" style="text-align:center;font-size:13px; font-weight:bold;padding:7px 7px;">{{ money()->toCommon($totalAdditional ?? "0", 2) }}
                        <td class="bordered" style="text-align:center;font-size:13px; font-weight:bold;padding:7px 7px;">{{$percentAdditional}}</td>
                    </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3" class="bordered" style="padding:7px 7px;">
                    Jumlah Keseluruhan Bersih (Item 1+2+3+4) 
                    @if(($jumlahBersihAd + $jumlahBersihOm) > 0)
                        Tambahan
                    @else
                        Pengurangan
                    @endif
                </td>
                <td colspan="3" class="bordered" style="text-align:center;font-size:13px; font-weight:bold;padding:7px 7px;">
                    @if(($jumlahBersihAd + $jumlahBersihOm) > 0)
                        {{ money()->toCommon(($jumlahBersihAd + $jumlahBersihOm) ?? "0", 2) }}
                    @else
                        ({{ str_replace("-","",money()->toCommon(($jumlahBersihAd + $jumlahBersihOm) ?? "0", 2)) }})
                    @endif
                </td> 
                <td class="bordered" style="text-align:center;font-size:13px; font-weight:bold;padding:7px 7px;">
                    @php
                        $percentJumlahBersih = (($jumlahBersihAd + $jumlahBersihOm) / $appointed->offered_price) * 100;
                        $percentJumlahBersih = number_format($percentJumlahBersih, 2, '.', '');                        
                    @endphp

                    {{$percentJumlahBersih}}
                </td>                   
            </tr>
        </tbody>
    </table>
</div>
<div class="nobreak">
    <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
        <tr>
            <td style="font-size:12px;"><br/></td>
        </tr>
        <tr style="text-align:center;background-color: #d9d9d9;">
            <td style="font-size:12px;"><span style="text-transform:uppercase; font-weight:bold;">Pengesahan oleh Jabatan Bertanggungjawab</span></td>
        </tr>
        <tr>
            <td style="font-size:12px;">
                <table border="1" style="width: 98%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
                    <tr>
                        <td width="33%" style="text-align:center;">Disediakan Oleh:</td>
                        <td width="33%" style="text-align:center;">Disemak Oleh:</td>
                        <td width="33%" style="text-align:center;">Disokong Oleh: (Naib Presiden)</td>
                    </tr>
                    <tr>
                        <td>
                            <br/><br/><br/><br/>
                            Nama : {{$vo->user->name}}<br/>
                            Jawatan : {{ $vo->user->position->name }}<br/>
                            Bahagian  : {{ $vo->user->section->name }}<br/>
                            Jabatan : {{ $vo->user->department->name}}<br/>
                            Tarikh : 
                        </td>
                        <td>
                            <br/><br/><br/><br/>
                            Nama : {{ $vo->user->supervisor->name }}<br/>
                            Jawatan : {{ $vo->user->supervisor->position->name }}<br/>
                            Bahagian  : {{ $vo->user->supervisor->bahagian->name }}<br/>
                            Jabatan : {{ $vo->user->supervisor->department->name }}<br/>
                            Tarikh : 
                        </td>
                        <td>
                            <br/><br/><br/><br/>
                            {{--  Nama : {{ (!empty($vo->user && !empty($vo->user->supervisor) && !empty(!empty($vo->user->supervisor->supervisor))))? $vo->user->supervisor->supervisor->name:null }}<br/>
                            Jawatan : {{ (!empty($vo->user) && !empty($vo->user->supervisor) && !empty($vo->user->supervisor->supervisor) && !empty($vo->user->supervisor->supervisor->position))? $vo->user->supervisor->supervisor->position->name:null }}<br/>
                            Bahagian  : {{ (!empty($vo->user) && !empty($vo->user->supervisor) && !empty($vo->user->supervisor->supervisor) && !empty($vo->user->supervisor->supervisor->section))? $vo->user->supervisor->supervisor->section->name:null }}<br/>
                            Jabatan : {{ (!empty($vo->user) && !empty($vo->user->supervisor) && !empty($vo->user->supervisor->supervisor) && !empty($vo->user->supervisor->supervisor->department))? $vo->user->supervisor->supervisor->department->name:null }}<br/>
                            Tarikh :   --}}
                            Nama : {{ $np_dept->name }}<br/>
                            Jawatan : {{ $np_dept->position->name }}<br/><br/>
                            {{--  Bahagian  : {{ $np_dept->section->name }}<br/>  --}}
                            Jabatan : {{ $np_dept->department->name }}<br/>
                            Tarikh : 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>