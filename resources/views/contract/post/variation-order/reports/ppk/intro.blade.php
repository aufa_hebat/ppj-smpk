<table width="100%" border="1" style="width: 100%;border-collapse: collapse;padding: 4 4; border-style:double;" class="bordered-double">
    <tr>
        <td class="bordered-double" height="740">
            <center><img style="width: 150px; height: 150px;" src="{{ logo($print) }}"></center>
            <br/><br/><br/><br/>
            <center>
                <span style="text-transform: uppercase; font-size: 32px;font-weight:bold; font-family: Arial, Helvetica, sans-serif;">
                    Permohonan Perubahan Kerja<br/>
                    (PPK)<br/><br/>
                    Bil.No. {{ $vo->no }}
                </span>
            </center>
            <br/><br/><br/>
            <table width="100%">
                <tr>
                    <td width="10%"></td>
                    <td align="center" width="85%" style="text-align: justify; text-transform: uppercase; font-size: 26px; font-weight:bold; font-family: Arial, Helvetica, sans-serif;">
                        {{ $vo->sst->acquisition->title }}
                    </td>
                    <td width="10%"></td>
                </tr>
                <tr><td colspan="3"><br/><br/><br/></td></tr>
                <tr>
                    <td colspan="3" style="text-align: center; text-transform: uppercase; font-size: 26px; font-family: Arial, Helvetica, sans-serif;">
                        KONTRAKTOR : {{ $vo->sst->company->company_name }}                            
                    </td>
                </tr> 
                <tr><td colspan="3" ><br/><br/></td></tr>               
                <tr>
                    <td  colspan="3" style="text-align: center; text-transform: uppercase; font-size: 26px; font-family: Arial, Helvetica, sans-serif;">
                        No. Kontrak : {{ $vo->sst->acquisition->reference }}
                    </td>
                </tr>
				<tr><td colspan="3" ><br/><br/><br/><br/></td></tr>   
            </table>			
        </td>
    </tr>
</table>