<table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
    <tr>
        <td style="text-align:center;">
            <span style="text-transform:uppercase; font-size:14px;">
                Perbadanan Putrajaya<br/>
                {{ $vo->sst->acquisition->title }}<br/>
                Permohonan Perubahan Kerja No. {{ $vo->no }}<br/>
            </span>
            <span style="text-transform:capitalize; font-size:10px; font-style:italic;">
                (Dokumen ini untuk edaran dalaman sahaja)
            </span>
        </td>
    </tr>
    <tr>
        <td style="font-size:12px;padding: 7 7;">
            BAHAGIAN C : MAKLUMAT PERUBAHAN KERJA YANG TELAH DILULUSKAN          
            
            <table border="0" style="width: 98%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
                <tr style="text-align:center;background-color: #d9d9d9;">
                    <td class="bordered" width="6%">Bil.</td>
                    <td class="bordered" width="40%">Perubahan Kerja Yang Telah Diluluskan</td>
                    <td class="bordered" width="16%">Nilai Diluluskan (RM)</td>
                    <td class="bordered" width="11%">%</td>
                    <td class="bordered" width="16%">Nilai Terkumpul (Tambahan Sahaja)</td>                    
                    <td class="bordered" width="11%">% Terkumpul (Tambahan Sahaja)</td> 
                </tr>
                @php
                    $sst = $vo->sst;
                    $totalElement = 0;
                    $total = 0.0;
                    
                @endphp
                @foreach($sst->variationOrders as $vos)
                    @if($vos->id != $vo->id)
                        @if($vos->status == '2')
                            <tr>
                                <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                    <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">PPk BIL. NO. {{ $vos->no }}</span>
                                </td>
                                <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                            </tr>
                            @foreach($vos->types->sortBy('variation_order_type_id') as $vot)
                                @if($vot->variation_order_type->id != 1)
                                    <tr>
                                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                        <td style="border-right: 1px solid #959594; padding:10px 5px;">
                                            <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">{{ $vot->variation_order_type->name }}</span>
                                        </td>
                                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                        <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>                                                                                                                                
                                    </tr>
                                    @if($vot->variation_order_type->id == 6)
                                        @foreach($vot->vo->cancels as $cancel)
                                            @php
                                                $total = $total + $cancel->amount
                                            @endphp
                                            <tr>
                                                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                                <td style="border-right: 1px solid #959594; padding:10px 5px;"><span>{{ $cancel->item->item }}</span></td>
                                                <td style="border-right: 1px solid #959594; padding:10px 5px;text-align:center;">
                                                    <span>({{ str_replace("-","",money()->toCommon($cancel->amount ?? "0", 2)) }})</span>
                                                </td>
                                                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                                <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>                                                 
                                            </tr>
                                        @endforeach
                                    @else
                                        @foreach($vot->elements as $key=>$el)
                                            @if($el->vo_type_id == $vot->id)
                                                @php
                                                    $total = $total + $el->net_amount
                                                @endphp
                                                <tr>
                                                    <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                                    <td style="border-right: 1px solid #959594; padding:10px 5px;"><span>{{ $el->element }}</span></td>
                                                    <td style="border-right: 1px solid #959594; padding:10px 5px;text-align:center;">
                                                        @if($el->amount_addition >= $el->amount_omission)
                                                            <span>{{ money()->toCommon($el->net_amount ?? "0", 2) }}</span>
                                                        @endif
                                                        @if($el->amount_addition < $el->amount_omission)
                                                            <span>({{ str_replace("-","",money()->toCommon($el->net_amount ?? "0", 2)) }})</span>
                                                        @endif
                                                    </td>
                                                    <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                                    <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                                    <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>                                                                                                                                
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endif
                @endforeach
                <tr style="background-color: #d9d9d9;">
                    <td class="bordered"></td>
                    <td class="bordered" style="text-align:center; padding:10px 10px;">Jumlah</td>
                    <td class="bordered" style="text-align:center;">
                        @if($total > 0)
                            {{ money()->toCommon($total ?? "0", 2) }}
                        @else
                            ({{ str_replace("-","",money()->toCommon($total ?? "0", 2)) }})
                        @endif
                    </td>
                    <td class="bordered"></td>
                    <td class="bordered"></td>
                    <td class="bordered"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>