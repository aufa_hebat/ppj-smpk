
<footer>
    <div class="pagenum-container title-footer">Page <span class="pagenum"></span></div>
</footer>
<div style="text-align:right" class="title-header">
    PPJ-BPUB/JW-PPK/V2/2018<br/>
    MUKA HADAPAN (JB:BIRU)(JL:HIJAU)(JK:KUNING)(JP:MERAH)(JR:OREN)(JW:PUTIH)
</div>
<table width="100%" border="1" style="width: 100%;border-collapse: collapse;padding: 4 4; border-style:double;" class="bordered-double">
    <tr>
        <td height="100">
            <table width="100%" class="content">
                <tr>
                    <td colspan="4" class="title-header" style="font-style: italic; text-align:right;text-decoration:underline;">Untuk diisi oleh Urusetia</td>
                </tr>
                <tr>
                    <td rowspan="2" width="25%" style="text-align:center;">
                        <img style="width: 100px; height: 100px;" src="{{ logo($print) }}">
                    </td>
                    <td width="50%" style="text-align:right;">Mesyuarat Permohonan Perubahan Kerja Bil.</td>
                    <td width="5%"> : </td>
                    <td width="25%">
                        <table width="100%" class="content">
                            <tr>
                                <td class="bordered" width="50%" height="15" style="text-align:center;">{{ isset($vo->mtg_no) ? $vo->mtg_no : " " }}</td>
                                <td class="bordered" width="50%" height="15" style="text-align:center;">{{ isset($vo->mtg_year) ? $vo->mtg_year : " " }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:right;">Kod Permohonan</td>
                    <td> : </td>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="bordered" width="25%" style="text-align:center;">{{ $vo->ppk_kod_1 }}</td>
                                <td class="bordered" width="25%" style="text-align:center;">/</td>
                                <td class="bordered" width="25%" style="text-align:center;">{{ $vo->ppk_kod_2 }}</td>
                                <td class="bordered" width="25%" style="text-align:center;">{{ $vo->ppk_kod_3 }}</td>
                            </tr>
                        </table>                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="190">
            <table width="100%">
                <tr><td></td></tr>
                <tr>
                    <td style="text-align:center; text-transform:uppercase; font-weight:bold;">Perbadanan Putrajaya</td>
                </tr>
                <tr><td><br/></td></tr>
                <tr>
                    <td style="text-align:center; text-transform:uppercase; font-weight:bold;">Jabatan {{ $vo->sst->acquisition->department->name }}</td>
                </tr>
                <tr><td><br/><br/></td></tr>
                <tr>
                    <td style="text-align:center; text-transform:uppercase; font-weight:bold;">Permohonan Perubahan Kerja No. : <span style="border: 1px solid #959594;">&nbsp;&nbsp;{{ $vo->no }}&nbsp;&nbsp;</span></td>
                </tr> 
                <tr><td><br/></td></tr>
                <tr>
                    <td style="text-align:center; font-style:italic; font-size:11px;">(Dokumen Bahagian Perolehan &amp; Ukur Bahan ini untuk edaran dalam sahaja)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="150">
            <table width="100%">
                <tr><td style="text-transform: uppercase; font-size:12px; font-weight:bold; padding: 10 10;">Bahagian A : Butiran Kontrak</td></tr>
                <tr><td style="font-size:12px; padding: 1 10;">Nama Kontraktor : </td></tr>
                <tr><td style="text-transform: uppercase; font-size:12px; font-weight:bold; padding: 1 10 10 10;">{{ $vo->sst->company->company_name }}</td></tr>
                <tr><td style="font-size:12px; padding: 1 10;">Tajuk Projek : </td></tr>
                <tr><td style="text-transform: uppercase; font-size:12px; font-weight:bold; padding: 1 10 10 10;">{{ $vo->sst->acquisition->title }}</td></tr>                
            </table>
        </td>
    </tr>
    <tr>
        <td height="270" valign="top">
            <br/>
            <table width="100%">
                <tr>
                    <td width="40%" style="font-size:11px;" valign="top">
                        <table width="100%"  style="font-size:11px;">
                            <tr>
                                <td width="3%" style="padding: 10px 5px;">1.</td>
                                <td width="44%" style="padding: 10px 10px;">No. Kontrak</td>
                                <td width="3%" style="padding: 10px 10px;"> : </td>
                                <td width="50%" style="padding: 10px 10px;">{{ $vo->sst->contract_no }}</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 5px;">2.</td>
                                <td style="padding: 10px 5px;">No Kontrak Lanjutan</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;"></td>
                            </tr>                            
                            <tr>
                                <td style="padding: 10px 5px;">3.</td>
                                <td style="padding: 10px 5px;">Harga Kontrak</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;">{{ money()->toHuman($appointed->offered_price ?? "0", 2) }}</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 5px;">4.</td>
                                <td style="padding: 10px 5px;">Harga Kontrak Lanjutan</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;"></td>
                            </tr>                            
                            <tr>
                                <td style="padding: 10px 5px;">5.</td>
                                <td style="padding: 10px 5px;">Tarikh Milik Tapak</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;">{{ \Carbon::intlFormat($vo->sst->start_working_date) }}</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 5px;">6.</td>
                                <td style="padding: 10px 5px;">Tarikh Siap Asal</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;">{{ \Carbon::intlFormat($vo->sst->end_working_date) }}</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 5px;">7.</td>
                                <td style="padding: 10px 5px;">Lanjutan Masa (EOT)</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;">
                                    @php $count = 0; @endphp
                                    @foreach($eots as $eot)
                                        @if(!empty($eot->eot_approve))
                                            @if($eot->eot_approve->approval == 1)
                                                EOT NO. {{ $eot->bil }}&nbsp; / &nbsp;({{ \Carbon::intlFormat($eot->eot_approve->approved_end_date) }})<br/>
                                                @php $count ++; @endphp
                                            @endif
                                        @endif
                                    @endforeach
                                    @if($count == 0)
                                        TIADA
                                    @endif
                                </td>
                            </tr>                                                                                                                      
                        </table>
                    </td>
                    <td width="2%">&nbsp;&nbsp;</td>
                    <td width="58%" style="font-size:11px;" valign="top">
                        <table width="100%">
                            <tr>
                                <td style="padding: 10px 5px;">8.</td>
                                <td style="padding: 10px 5px;">Tarikh Siap Sebenar</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;">
                                    {{ isset($vo->sst->cpc) ? \Carbon::intlFormat($vo->sst->cpc->signature_date) : "-"}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 5px;">9.</td>
                                <td style="padding: 10px 5px;">Peratus Siap &amp; Tarikh</td>
                                <td style="padding: 10px 5px;"> : </td>
                                <td style="padding: 10px 5px;">
                                    @php 
                                        $monitoring = '';

                                        if($vo->sst->siteVisits()->latest()->first() != null)
                                        {
                                            $monitoring = $vo->sst->siteVisits()->latest()->first();
                                        }                                        
                                    @endphp

                                    {{!isset($monitoring) ? $monitoring->percentage_completed : '0.00' }}
                                    &nbsp;&nbsp;
                                    {{!isset($monitoring) ? \Carbon::intlFormat($monitoring->visited_date) : ""}}
                                </td>
                            </tr>                               
                            <tr>
                                <td width="3%" valign="top" style="padding: 10px 5px;">10.</td>
                                <td width="50%" valign="top" style="padding: 10px 5px;" style="padding: 10px 10px;">Nilai Permohonan Perubahan Kerja Maksima Peringkat Perbadanan Putrajaya</td>
                                <td width="5%" valign="top" style="padding: 10px 5px;"> : </td>
                                <td width="42%" valign="top" style="padding: 10px 5px;">
                                    @php
                                        $maxValue = 0;
                                        $percentValue = 0;
                                        if($vo->sst->acquisition->acquisition_category_id == 3 || $vo->sst->acquisition->acquisition_category_id == 4){
                                            $minValue = 0.2 * $appointed->offered_price;
                                            $percentValue = "20%";
                                            $limitValue = 100000000;
                                            $limit = "1 Juta";

                                            if($minValue > $limitValue ){
                                                $minValue = $limitValue;
                                            }
                                        }elseif($vo->sst->acquisition->acquisition_category_id == 1 || $vo->sst->acquisition->acquisition_category_id == 2){
                                            $minValue = 0.1 * $appointed->offered_price;
                                            $percentValue = "10%";
                                            $limitValue = 10000000;
                                            $limit = "100k";

                                            if($minValue > $limitValue){
                                                $minValue = $limitValue;
                                            }
                                        }                                        
                                    @endphp
                                    <span style="font-weight:bold;">{{money()->toHuman($minValue ?? "0", 2)}}</span>
                                    <br/><br/>
                                    <span style="font-style:italic">(Had Nilai: {{ $percentValue }} dari Harga Kontrak atau RM {{ $limit }}; Yang Mana Lebih Rendah)</span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td valign="top" style="padding: 10px 5px;">11.</td>
                                <td valign="top" style="padding: 10px 5px;" style="padding: 10px 10px;">
                                    Jumlah Nilai/Peratus yang telah diluluskan (Tambahan Sahaja) sehingga PPK No. <br/>
                                    (&nbsp;{{$listPpk}}&nbsp;)
                                </td>
                                <td valign="top" style="padding: 10px 5px;"> : </td>
                                <td valign="top" style="padding: 10px 5px;">

                                    <span style="font-weight:bold;">{{ money()->toHuman($totalPrevAdditional ?? "0", 2) }}</span>    &nbsp;&nbsp;  {{$percentPrevAdditional}}%  
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="padding: 10px 5px;">12.</td>
                                <td valign="top" style="padding: 10px 5px;" style="padding: 10px 10px;">Jumlah Permohonan Perubahan Kerja ini (Tambahan Sahaja)</td>
                                <td valign="top" style="padding: 10px 5px;"> : </td>
                                <td valign="top" style="padding: 10px 5px;">
                                    <span style="font-weight:bold;">{{ money()->toHuman($totalCurrentAdditional ?? "0", 2) }}</span>    &nbsp;&nbsp;  {{$percentCurrentAdditional}}%  
                                </td>
                            </tr>  
                            <tr>
                                <td valign="top" style="padding: 10px 5px;">13.</td>
                                <td valign="top" style="padding: 10px 5px;" style="padding: 10px 10px;">Nilai Terkumpul Dijangkakan (Tambahan Sahaja)</td>
                                <td valign="top" style="padding: 10px 5px;"> : </td>
                                <td valign="top" style="padding: 10px 5px;">
                                    <span style="font-weight:bold;">{{ money()->toHuman($totalAdditional ?? "0", 2) }}</span>    &nbsp;&nbsp;  {{$percentAdditional}}%  
                                </td>
                            </tr>                                                                                   
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>