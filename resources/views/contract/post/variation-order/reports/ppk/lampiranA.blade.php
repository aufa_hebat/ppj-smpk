{{--  <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center" page-break-inside: auto>
    <tr>
        <td style="text-align:center;">
            <span style="text-transform:uppercase; font-size:14px;">
                Perbadanan Putrajaya<br/>Permohonan Perubahan Kerja No. {{ $vo->no }}<br/>
            </span>
            <span style="text-transform:capitalize; font-size:10px; font-style:italic;">
                (Dokumen Bahagian Perolehan &amp; Ukur Bahan ini untuk edaran dalaman sahaja)
            </span>
        </td>
    </tr>
</table>  --}}

<div style="top: -60px; text-align:right; font-size:10px; text-transform: uppercase; text-decoration: underline;">
    Lampiran A
</div>
<div style="font-size:12px; border:1; padding: 5px 5px;">
    <div style="text-align:center; text-decoration: underline;">LAMPIRAN A: MAKLUMAT PERUBAHAN KERJA YANG TELAH DILULUSKAN</div>
    <table border="0" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
        <thead>
            <tr style="text-align:center;background-color: #d9d9d9;">
                <td class="bordered" width="4%" rowspan="2">PPK No.</td>
                <td class="bordered" width="40%" rowspan="2">Perubahan Kerja Yang Telah Diluluskan</td>
                <td class="bordered" colspan="4">Nilai Diluluskan</td>                    
                <td class="bordered" width="10%" rowspan="2">Nilai Terkumpul (Tambahan Sahaja)</td>
                <td class="bordered" width="10%" rowspan="2">% Terkumpul (Tambahan Sahaja)</td>
            </tr>
            <tr style="text-align:center;background-color: #d9d9d9;">
                <td class="bordered" width="11%">Tambahan</td>
                <td class="bordered" width="7%">%</td>
                <td class="bordered" width="11%">Kurangan</td>
                <td class="bordered" width="7%">%</td>
            </tr>
        </thead>
        <tbody>
            @foreach($types as $keyT => $type) 
                {{--  @if($type->id == '2')
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $keyT = $keyT + 1 }}</td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                            <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold; text-align:center;">{{ $type->name }}</span>
                        </td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                    </tr>
                    @foreach($vo->sst->variationOrders as $vos)
                        @if($vos->id != $vo->id)
                            @if($vos->status == '2')
                                @foreach($vos->types->sortBy('variation_order_type_id') as $vot)
                                    @if($vot->variation_order_type_id == $type->id)
                                        <tr>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                                <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">PPK NO.</span>
                                            </td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                @elseif($type->id == '3')  --}}
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $keyT = $keyT + 1 }}</td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                            <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">{{ $type->name }}</span>
                        </td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                    </tr>
                    @php
                        $totalAddition = 0;
                        $totalOmission = 0;
                    @endphp
                    @foreach($vo->sst->variationOrders as $vos)
                        @if($vos->id != $vo->id)
                            @if($vos->status == '2' && $vos->updated_at < $vo->updated_at)
                                @foreach($vos->types->sortBy('variation_order_type_id') as $vot)
                                    @if($vot->variation_order_type_id == $type->id || 
                                        ($vot->variation_order_type_id == '1' && $type->id == '3') || 
                                        ($vot->variation_order_type_id == '6' && $type->id == '3'))

                                        @php 
                                            $itemIndex = -1;
                                        @endphp

                                        <tr>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                                <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">PPK NO. {{ $vos->no }}</span>
                                            </td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                        </tr>
                                        @if($vot->variation_order_type->id == 1 && $type->id == 3)
                                            @foreach($vot->vo->actives as $active)
                                                @if($active->status == 1)
                                                    @php $totalAddition = $totalAddition + $active->item->amount; @endphp
                                                    <tr>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $roman[ $itemIndex = $itemIndex + 1 ] }}</td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                                            <span style="">{{ $active->item->item }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            <span style="text-align:right">{{ money()->toCommon($active->item->amount ?? "0", 2) }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            <span style="text-align:right">{{ number_format(($active->item->amount / $appointed->offered_price * 100), 2)  }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                                            
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @elseif($vot->variation_order_type->id == 6 && $type->id == 3)
                                            @foreach($vot->vo->cancels as $cancel)
                                                @if($cancel->status == 1)
                                                    @php $totalOmission = $totalOmission + $cancel->amount; @endphp
                                                    <tr>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $roman[ $itemIndex = $itemIndex + 1 ] }}</td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                                            <span style="">{{ $cancel->item->item }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            <span style="text-align:right">{{ str_replace("-","",money()->toCommon($cancel->amount ?? "0", 2)) }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            <span style="text-align:right">{{ number_format(($cancel->amount / $appointed->offered_price * 100), 2)  }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($vot->elements as $key=>$el)
                                                @if($el->vo_type_id == $vot->id && $el->status == 1)
                                                    <tr>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $roman[  $itemIndex = $itemIndex + 1 ] }}</td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                                            <span style="">{{ $el->element }}</span>
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            @if($el->amount_addition >= $el->amount_omission)
                                                                <span>{{ money()->toCommon($el->net_amount ?? "0", 2) }}</span>
                                                                @php $totalAddition = $totalAddition + $el->net_amount; @endphp
                                                            @endif
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            @if($el->amount_addition >= $el->amount_omission)
                                                                <span style="text-align:right">{{ number_format(($el->net_amount / $appointed->offered_price * 100), 2) }}</span>
                                                            @endif
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            @if($el->amount_addition < $el->amount_omission)
                                                                <span>({{ str_replace("-","",money()->toCommon($el->net_amount ?? "0", 2)) }})</span>
                                                                @php $totalOmission = $totalOmission + $el->net_amount; @endphp
                                                            @endif
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:right;">
                                                            @if($el->amount_addition < $el->amount_omission)
                                                                <span style="text-align:right">{{ number_format(($el->net_amount / $appointed->offered_price * 100), 2) }}</span>
                                                            @endif
                                                        </td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach 
                    <tr style="text-align:center;background-color: #d9d9d9;">
                        <td class="bordered"></td>
                        <td class="bordered">
                            <span style="">Jumlah : </span>
                        </td>
                        <td class="bordered" style="text-align:right; padding:5px 5px;">{{ money()->toCommon($totalAddition ?? "0", 2)}}</td>
                        <td class="bordered"></td>
                        <td class="bordered" style="text-align:right; padding:5px 5px;">{{ money()->toCommon($totalOmission ?? "0", 2)}}</td>
                        <td class="bordered"></td>
                        <td class="bordered"></td>
                        <td class="bordered"></td>
                    </tr>                  
                {{--  @elseif($type->id == '4')
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $keyT = $keyT + 1 }}</td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                            <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">{{ $type->name }}</span>
                        </td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                    </tr>
                @elseif($type->id == '5')
                    <tr>
                        <td style="border-right: 1px solid #959594; padding:5px 5px; text-align:center;">{{ $keyT = $keyT + 1 }}</td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;">
                            <span style="text-transform:uppercase;text-decoration:underline;font-weight:bold;">{{ $type->name }}</span>
                        </td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                        <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                    </tr>
                @endif  --}}
            @endforeach
        </tbody>
    </table>
</div>