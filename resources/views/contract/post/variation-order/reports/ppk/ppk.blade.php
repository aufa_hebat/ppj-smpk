<html>
	<head>
		<style>
			.nobreak {
				page-break-inside: avoid;
			}
			
			div.breakNow { page-break-inside:avoid; page-break-after:always; }

			.bordered {
				border-color: #959594;
				border-style: solid;
				border-width: 1px;
			}

			.bordered-double {
				border-color: #000000;
				border-style: double;
				border-width: 3px;
			}

			title{
				font-size: 16px;
			}

			
			header {
				position: fixed;
				top: -60px;
				left: 0px;
				right: 0px;
				height: 150px;

				color: black;
				text-align: right;
			}

			footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: center;
				line-height: 35px;	
				font-size:11px;
            }

			body{
				counter-reset:page -1; 
				font-family: "Arial, Helvetica, sans-serif";       
			}

			.title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
			}

			.title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }

			.content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
			
			.pagenum:before {
                content: counter(page);                
            }
		</style>
	</head>
	<body>
		@php
			$letter = collect(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']);
			$roman = collect(['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix', 'x']);
		@endphp

		@php
			$totalPrevAdditional = 0;
			$percentPrevAdditional = 0;
			$listPpk = "";

			$totalCurrentAdditional = 0;
			$percentCurrentAdditional = 0;

			$totalAdditional = 0;
			$precentAdditional = 0;

			foreach($vo->sst->variationOrders as $vos){
				if($vos->id != $vo->id && $vos->status == '2'){
					foreach($vos->types->sortBy('variation_order_type_id') as $vot){
						if($vot->variation_order_type->id == 2){
							if($listPpk == ""){
								$listPpk = "No." . $vos->no;
							}else{
								$listPpk = $listPpk . ", No." . $vos->no;
							}
							foreach($vot->elements as $key=>$el){
								if($el->vo_type_id == $vot->id){
									if($el->net_amount > 0){
										$totalPrevAdditional = $totalPrevAdditional + $el->net_amount;
									}
								}
							}
						}
					}
				}elseif($vos->id == $vo->id){
					foreach($vos->types->sortBy('variation_order_type_id') as $vot){
						if($vot->variation_order_type->id == 2){
							foreach($vot->elements as $key=>$el){
								if($el->vo_type_id == $vot->id){
									if($el->net_amount > 0){
										$totalCurrentAdditional = $totalCurrentAdditional + $el->net_amount;
									}
								}
							}
						}
					}
				}
			}

			$percentPrevAdditional = ($totalPrevAdditional / $appointed->offered_price) * 100;
			$percentPrevAdditional = number_format($percentPrevAdditional, 2, '.', '');

			$percentCurrentAdditional = ($totalCurrentAdditional / $appointed->offered_price) * 100;
			$percentCurrentAdditional = number_format($percentCurrentAdditional, 2, '.', '');

			$totalAdditional = $totalPrevAdditional + $totalCurrentAdditional;
			$percentAdditional = ($totalAdditional / $appointed->offered_price) * 100;
			$percentAdditional = number_format($percentAdditional, 2, '.', '');
		@endphp

		@include('contract.post.variation-order.reports.ppk.intro', ['vo' => $vo])
		<div class="breakNow"><br></div>
		@include('contract.post.variation-order.reports.ppk.cover', ['vo' => $vo])
		<div class="breakNow"><br></div>
		@include('contract.post.variation-order.reports.ppk.detail', ['vo' => $vo])
		<div class="breakNow"><br></div>
		@include('contract.post.variation-order.reports.ppk.lampiranA', ['vo' => $vo])
		@include('contract.post.variation-order.reports.ppk.lampiranB', ['vo' => $vo])
		{{--  @include('contract.post.variation-order.reports.ppk.prev_vo', ['vo' => $vo])  --}}
		@include('contract.post.variation-order.reports.ppk.summary', ['vo' => $vo])
		
		@role('urusetia')
		<div class="breakNow"><br></div>
		@include('contract.post.variation-order.reports.ppk.result', ['vo' => $vo])
		@endrole
	</body>
</html>