<div class="breakNow"><br></div>
<div style="top: -60px; text-align:right; font-size:10px; text-transform: uppercase; text-decoration: underline;">
    Lampiran B
</div>
<div style="font-size:12px; border:1; padding: 5px 5px;">
    <div style="text-align:center; text-decoration: underline;">Permohonan Untuk Kelulusan Bagi Kerja-Kerja Di Bawah Wang Peruntukan Sementara (WPS)</div>

    <table style="width: 100%;border-collapse: collapse;padding: 4 4;" bordder="0" class="bordered" align="center">
        <thead>
            <tr style="text-align:center;background-color: #d9d9d9;">
                <th class="bordered" width="4%" rowspan="2">Bil.</th>
                <th class="bordered" width="20%" rowspan="2">Huraian Perubahan Kerja</th>
                <th class="bordered" width="20%" rowspan="2" style="padding: 4 4;">Sebab Perlu Diberikan Kepada Kontraktor Utama / NSC / SubKontraktor Domestik / Dikeluarkan dari Kontrak (Pihak Ketiga)</th>
                <th class="bordered" width="10%">Nilai Asal Peruntukan</th>
                <th class="bordered" width="10%">Nilai Semakan Dijangkakan</th>
                <th class="bordered" colspan="2">Nilai Peratus Baru Tambahan / Kurangan Dijangkakan</th>   
            </tr>
            <tr style="text-align:center;background-color: #d9d9d9;">
                <th class="bordered">RM</th>
                <th class="bordered">RM</th>
                <th class="bordered" width="10%">RM</th>
                <th class="bordered" width="10%">%</th>
            </tr>
        </thead>
        <tbody>

            @php $totalElement = 0; @endphp
            @foreach($vo->types as $vot)
                @if($vot->variation_order_type_id == 1)
                    @foreach($vo->actives as $key=>$active)
                        @php $totalElement = $totalElement + 1; @endphp                            
                        <tr>
                            <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" >{{ $roman[$totalElement - 1] }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                    {{ $active->item->item }}
                            </td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                Peruntukan sementara untuk diaktifkan
                            </td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($active->item->amount ?? "0", 2 ) }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($active->item->amount ?? "0", 2 ) }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                        </tr>
                    @endforeach
                @endif

                @if($vot->variation_order_type_id == 3)                        
                    @foreach($vo->sst->variationOrders as $vos)
                        @foreach($vos->actives as $key=>$activeWps)                    
                            @php 
                                $isFound = 0; 
                                $totalUsed = 0;

                                foreach($Elemen as $key=>$el){
                                    if($el->vo_type_id == $vot->id){
                                        if($el->vo_active_id == $activeWps->id){
                                            $isFound = 1;
                                            $totalUsed = $totalUsed + $el->net_amount;
                                        }
                                    }                               
                                }
                            @endphp
                            
                            @if($isFound == 1)
                                @php $totalElement = $totalElement + 1; @endphp  
                                <tr>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" >{{ $roman[$totalElement - 1] }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">{{ $activeWps->item->item }}</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($activeWps->item->amount ?? "0", 2 ) }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($totalUsed ?? "0", 2 ) }}</td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                </tr>
                                
                                {{--  @foreach($Elemen as $key1=>$el)
                                    @if($el->vo_type_id == $vot->id)
                                        @if($el->vo_active_id == $activeWps->id)
                                            <tr>
                                                <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" ></td> 
                                                <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                <td style="border-right: 1px solid #959594; padding:5px 5px;">{{ $el->element }}</td>
                                                <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                                <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($el->net_amount ?? "0", 2 ) }}</td> 
                                                <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                                <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                            </tr>
                                        @endif
                                    @endif                              
                                @endforeach  --}}

                                @foreach($vo->sst->variationOrders as $vosList)
                                    @foreach($vosList->types as $voType)
                                    @if($voType->variation_order_type_id == 3 && $vosList->status == 2 && $vosList->updated_at <= $vo->updated_at)
                                        @foreach($vosList->vo_elements as $vosE)
                                            @if($vosE->wps_status == 1)
                                                <tr>
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" ></td> 
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px;"></td>
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">{{ $vosE->element }}</td>
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">
                                                        @php
                                                            $total = 0;
                                                            foreach($vosE->items as $items){
                                                                $total = $total + $items->subItems()->where('wps_status', 1)->pluck('net_amount')->sum();
                                                            }

                                                        @endphp
                                                        {{ money()->toCommon($total ?? "0", 2) }}

                                                    </td> 
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                                    <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;"></td> 
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    @endforeach
                                @endforeach
                            

                                <tr>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                    <td style="border-right: 1px solid #959594; padding:5px 5px;">&nbsp;</td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                @endif 
                
                @if($vot->variation_order_type_id == 6)
                    @foreach($vo->cancels as $key=>$cancel)
                        @php $totalElement = $totalElement + 1; @endphp                            
                        <tr>
                            <td style="border-right: 1px solid #959594; padding:5px 5px; text-align: center;" >{{ $roman[$totalElement - 1] }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                {{ $cancel->item->item }}
                            </td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;">
                                Peruntukan tidak digunakan
                            </td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($cancel->item->amount ?? "0", 2 ) }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($cancel->amount ?? "0", 2 ) }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">{{ money()->toCommon($cancel->amount ?? "0", 2 ) }}</td> 
                            <td style="border-right: 1px solid #959594; padding:5px 5px;text-align:right;">
                                
                            </td> 
                        </tr>
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>
</div>