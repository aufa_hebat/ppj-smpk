@foreach($types as $keyT => $type) 

    @foreach($vo->types->sortBy('variation_order_type_id') as $vot)
        @if($vot->variation_order_type->id != 1)
            @if($vot->variation_order_type_id == $type->id || ($vot->variation_order_type_id == 6 && $type->id == 3))

                <div class="breakNow"><br></div>
                <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4; border-style:double;" class="bordered-double">
                    <tr>
                        <td height="730" style="font-size:40px; text-transform:uppercase;">
                            <center>{{ $letter[$type->id - 2] }}. &nbsp;&nbsp;&nbsp; {{ $type->name }}</center>
                        </td>
                    </tr>
                </table>

                <div class="breakNow"><br></div>

                <table width="100%">
                    <tr>
                        <td style="text-align:left; text-transform: uppercase; font-size: 12px; padding:10px 10px;">
                            {{ $vo->sst->acquisition->title }}<br/><br/>
                            Permohonan Perubahan Kerja No. {{ $vo->no }}<br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" style="width: 98%;border-collapse: collapse;padding: 4 4; font-size:12px;" class="bordered" align="center">
                                <tr style="text-align:center;background-color: #d9d9d9;">
                                    <td class="bordered" rowspan="2" width="6%">Item</td>
                                    <td class="bordered" rowspan="2" width="40%">Description</td>
                                    <td class="bordered" rowspan="2" width="10%">Page No.</td>
                                    <td class="bordered" colspan="2" width="30%">Total Amount (RM)</td>                    
                                    <td class="bordered" rowspan="2" width="14%">Nett</td>
                                </tr>
                                <tr style="text-align:center;background-color: #d9d9d9;">
                                    <td class="bordered" width="15%">BQ</td>
                                    <td class="bordered" width="15%">Actual</td>
                                </tr>
                                @php                        
                                    $total = 0.0;
                                    $totalOmission = 0;
                                    $totalAddition = 0;
                                    $totalElement = 0;
                                    $index = 0;
                                @endphp

                                @if($vot->variation_order_type_id == 6)
                                    @foreach($vo->cancels as $key=>$cancel)
                                        @php
                                            $totalElement = $totalElement + 1;
                                        @endphp
                                        <tr>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:center;" valign="top">{{ $roman[$totalElement - 1] }}</td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px;"><span>{{ $cancel->item->item }}</span></td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:right;">0.00</td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:right;">
                                                ({{ str_replace("-","",money()->toCommon($cancel->amount ?? "0", 2)) }})
                                            </td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:right;">
                                                ({{ str_replace("-","",money()->toCommon($cancel->amount ?? "0", 2)) }})
                                            </td>
                                        </tr>
                                        @php
                                            $totalAddition = $totalAddition + $cancel->amount;
                                            $total = $total + $cancel->amount;                                        
                                        @endphp
                                    @endforeach
                                @endif


                                @foreach($vot->elements as $key=>$el)
                                    @if($el->vo_type_id == $vot->id)
                                        @php
                                            $totalElement = $totalElement + 1;
                                        @endphp
                                        <tr>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:center;" valign="top">{{ $roman[$totalElement - 1] }}</td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px;"><span>{{ $el->element }}</span></td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px;"></td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:right;">
                                                @if($el->amount_omission > 0)
                                                    {{ money()->toCommon($el->amount_omission ?? "0", 2) }}
                                                @else
                                                    ({{ str_replace("-","",money()->toCommon($el->amount_omission ?? "0", 2)) }})
                                                @endif
                                            </td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:right;">
                                                @if($el->amount_addition > 0)
                                                    {{ money()->toCommon($el->amount_addition ?? "0", 2) }}
                                                @else
                                                    ({{ str_replace("-","",money()->toCommon($el->amount_addition ?? "0", 2)) }})
                                                @endif
                                            </td>
                                            <td style="border-right: 1px solid #959594; padding:10px 5px; text-align:right;">
                                                @if($el->net_amount > 0)
                                                    {{ money()->toCommon($el->net_amount ?? "0", 2) }}
                                                @else
                                                    ({{ str_replace("-","",money()->toCommon($el->net_amount ?? "0", 2)) }})
                                                @endif
                                            </td>
                                        </tr>
                                        @php
                                            $total = $total + $el->net_amount;
                                            $totalOmission = $totalOmission + $el->amount_omission;
                                            $totalAddition = $totalAddition + $el->amount_addition;
                                        @endphp
                                    @endif
                                @endforeach

                                <tr>
                                    <td class="bordered"></td>
                                    <td class="bordered" colspan="2" style="text-align:right; text-transform:uppercase; font-weight: bold; font-size: 12px;"> Summary Variation Of Works  </td>
                                    <td class="bordered" style=" padding:10px 5px; text-align:right;">
                                        @if($totalOmission > 0)
                                            {{ money()->toCommon($totalOmission ?? "0", 2) }}
                                        @else
                                            ({{ str_replace("-","",money()->toCommon($totalOmission ?? "0", 2)) }})
                                        @endif

                                    </td>
                                    <td class="bordered" style=" padding:10px 5px; text-align:right;">
                                        @if($totalAddition > 0)
                                            {{ money()->toCommon($totalAddition ?? "0", 2) }}
                                        @else
                                            ({{ str_replace("-","",money()->toCommon($totalAddition ?? "0", 2)) }})
                                        @endif
                                    </td>
                                    <td class="bordered" style=" padding:10px 5px; text-align:right;">
                                        @if($total > 0)
                                            {{ money()->toCommon($total ?? "0", 2) }}
                                        @else
                                            ({{ str_replace("-","",money()->toCommon($total ?? "0", 2)) }})
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            
                @foreach($vot->elements as $key=>$el)
                    @if($el->vo_type_id == $vot->id)
                        @include('contract.post.variation-order.reports.ppk.vo_details', ['el' => $el])
                    @endif
                @endforeach
            @endif
        @endif
    @endforeach

@endforeach