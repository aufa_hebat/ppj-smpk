<html>
    <head>
        <style type="text/css">

            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }

            .bordered-double {
                border-color: #000000;
                border-style: double;
                border-width: 3px;
            }

            title{
                font-size: 16px;
            }
            
            footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: right;
                line-height: 35px;	
                font-size:11px;
            }
            .pagenum:before {
                content: counter(page);                
            }
        </style>
    </head>
    <body>
        <footer>
            <table width="10%" align="right">
                <tr><td style="text-decoration:overline; text-align:center;" class="bordered">Disahkan</td></tr>
            </table>
        </footer>
        <table border="1" style="width: 100%;border-collapse: collapse;padding: 4 4;" class="bordered" align="center">
            <thead>
            <tr>
                <th>
                    <table width="100%">
                        <tr>
                            <td colspan="2" style="text-align:right; text-decoration:underline; font-size:12px;font-style:italic;">
                                Untuk diisi oleh Urusetia
                            </td>
                        </tr>
                        <tr>
                            <td width="70%" style="font-weight:bold; font-size:13px;">
                                <span style="text-transform: uppercase; ">Perbadanan Putrajaya</span><br/>
                                <span style="text-transform: uppercase; ">Permohonan Perubahan Kerja No. </span><span style="text-decoration:underline;">{{ $vo->no }}</span><br/>
                                <span style="text-transform: uppercase; ">Jabatan : </span><span style="text-decoration:underline;text-transform:capitalize;">{{ $vo->sst->acquisition->department->name }}</span><br/>
                                <span style="text-transform: uppercase; ">Nama Projek : </span><span style="text-decoration:underline;text-transform:capitalize;">{{ $vo->sst->acquisition->title }}</span><br/>
                                <span style="text-transform: uppercase; ">No. Kontrak : </span><span style="text-decoration:underline;">{{ $vo->sst->contract_no }}</span><br/>
                            </td>
                            <td width="30%" class="bordered" style="font-size:13px;">
                                <table width="100%">
                                    <tr>
                                        <td colspan="3">Mesy. PPK Bil. : </td>
                                        <td class="bordered" style="text-align:center;">{{ $vo->mtg_no }}</td>
                                        <td class="bordered" style="text-align:center;">{{ $vo->mtg_year }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">Kod Permohonan</td>
                                    </tr>
                                    <tr>
                                        <td width="5%"><br/></td>
                                        <td class="bordered" width="20%" style="text-align:center;">{{ $vo->ppk_kod_1 }}</td>
                                        <td class="bordered" width="20%" style="text-align:center;">/</td>
                                        <td class="bordered" width="20%" style="text-align:center;">{{ $vo->ppk_kod_2 }}</td>
                                        <td class="bordered" width="35%" style="text-align:center;">{{ $vo->ppk_kod_3 }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center; font-style:italic; font-size:12px; padding: 7px 7px;">
                                Dokuman Bahagian Perolehan &amp; Ukur Bahan ini untuk edaran dalaman sahaja
                            </td>
                        </tr>                
                    </table>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="padding: 7px 7px;">
                    <span style="text-transform: uppercase;font-size:12px;font-weight:bold;">
                        Bahagian C: Keputusan Jawatankuasa Permohonan Perubahan Kerja (p.k)
                    </span>
                    <span style="font-size:12px;">(Untuk diisi oleh Urusetia)</span>
                    <br/>
                    <table width="100%" class="bordered-double" border="1" style="font-size:12px; width: 100%;border-collapse: collapse;padding: 7px 7px; border-style:double;">
                        <tr style="text-align:center; font-weight:bold;padding: 7px 7px;">
                            <td width="3%" style="padding: 7px 7px;">Bil.</td>
                            <td width="57%" style="padding: 7px 7px;">
                                Rujukan Wang Peruntukan Sementara<br/>
                                (Rujuk Bahagian B Item 2.0)
                            </td>
                            <td width="40%">Kerja Diberikan Kepada / Ulasan (Jika Ada)</td>
                        </tr>
                        @foreach($vo->types as $vot)
                            @if($vot->variation_order_type_id == 1)
                                @if($vo->actives->count() > 0)
                                    <tr><td colspan="3">{{ $vot->variation_order_type->name }}</td></tr>
                                @endif
                                @foreach($vo->actives as $key => $active)
                                <tr style="text-align:left">
                                    <td style="padding: 5px 5px; text-align:center;">{{ $key + 1 }}.</td>
                                    <td style="padding: 5px 5px;">
                                        {{ $active->item->item }}
                                        &nbsp;-&nbsp;
                                        <b>@if($active->status == 1)Lulus @else Tidak Lulus @endif</b>
                                    </td>
                                    <td style="padding: 5px 5px;">{{ isset($active->comment) ? $active->comment : "-" }}</td>
                                </tr>
                                @endforeach
                            @elseif($vot->variation_order_type_id == 6)
                                @if($vo->cancels->count() > 0)
                                    <tr><td colspan="3">{{ $vot->variation_order_type->name }}</td></tr>
                                @endif
                                @foreach($vo->cancels as $key => $cancel)
                                <tr style="text-align:left">
                                    <td style="padding: 5px 5px; text-align:center;">{{ $key + 1 }}.</td>
                                    <td style="padding: 5px 5px;">
                                        {{ $cancel->item->item }}
                                        &nbsp;-&nbsp;
                                        <b>@if($cancel->status == 1)Lulus @else Tidak Lulus @endif</b>
                                    </td>
                                    <td style="padding: 5px 5px;">{{ isset($cancel->comment) ? $cancel->comment : "-" }}</td>
                                </tr>
                                @endforeach                    
                            @elseif($vot->variation_order_type_id == 3)
                                <tr><td colspan="3">{{ $vot->variation_order_type->name }}</td></tr>
                                @foreach($vot->elements as $key => $element)
                                <tr style="text-align:left">
                                    <td style="padding: 5px 5px; text-align:center;">{{ $key + 1 }}.</td>
                                    <td style="padding: 5px 5px;">
                                        {{ $element->element }}
                                        &nbsp;-&nbsp;
                                        <b>@if($element->status == 1)Lulus @else Tidak Lulus @endif</b>
                                    </td>
                                    <td style="padding: 5px 5px;">{{ isset($element->comment) ? $element->comment : "-" }}</td>
                                </tr>
                                @endforeach                    
                            @endif
                        @endforeach
                    </table>
                    
                    @if($vo->sst->acquisition->approval->acquisition_type_id == 1 || $vo->sst->acquisition->approval->acquisition_type_id == 2 || $vo->sst->acquisition->approval->acquisition_type_id == 4)
                    <table width="100%" class="bordered-double" border="1" style="font-size:12px; width: 100%;border-collapse: collapse;padding: 7 7; border-style:double;">
                        <tr style="text-align:center; font-weight:bold; ">
                            <td width="3%" style="padding: 7px 7px;">Bil.</td>
                            <td width="57%" style="padding: 7px 7px;">
                                Rujukan Perubahan Kerja Diluluskan / Tidak Diluluskan
                            </td>
                            <td width="40%" style="padding: 7px 7px;">Kerja Diberikan Kepada / Ulasan (Jika Ada)</td>
                        </tr>
                        @foreach($vo->types as $vot)
                            @if($vot->variation_order_type_id == 2)
                                @foreach($vot->elements as $key => $element)
                                    <tr style="text-align:left">
                                        <td style="padding: 5px 5px; text-align:center;">{{ $key + 1 }}.</td>
                                        <td style="padding: 5px 5px;">
                                            {{ $element->element }}
                                            &nbsp;-&nbsp;
                                            <b>@if($element->status == 1)Lulus @else Tidak Lulus @endif</b>
                                        </td>
                                        <td style="padding: 5px 5px;">{{ isset($element->comment) ? $element->comment : "-" }}</td>
                                    </tr>
                                @endforeach                    
                            @endif
                        @endforeach
                    </table>
                    @endif
                    
                    <table width="100%" class="bordered-double" border="1" style="font-size:12px; width: 100%;border-collapse: collapse;padding: 7 7; border-style:double;">
                        <tr style="text-align:center; font-weight:bold; ">
                            <td width="3%" style="padding: 7px 7px;">Bil.</td>
                            <td width="57%" style="padding: 7px 7px;">
                                Rujukan Pengiraan Semula Kuantiti Sementara<br/>
                                (Rujuk Bahagian B Item 3.0)
                            </td>
                            <td width="40%" style="padding: 7px 7px;">Kerja Diberikan Kepada / Ulasan (Jika Ada)</td>
                        </tr>
                        @foreach($vo->types as $vot)
                            @if($vot->variation_order_type_id == 4)
                                @foreach($vot->elements as $key => $element)
                                    <tr style="text-align:left">
                                        <td style="padding: 5px 5px; text-align:center;">{{ $key + 1 }}.</td>
                                        <td style="padding: 5px 5px;">
                                            {{ $element->element }}
                                            &nbsp;-&nbsp;
                                            <b>@if($element->status == 1)Lulus @else Tidak Lulus @endif</b>
                                        </td>
                                        <td style="padding: 5px 5px;">{{ isset($element->comment) ? $element->comment : "-" }}</td>
                                    </tr>
                                @endforeach                    
                            @endif
                        @endforeach
                    </table> 
                    
                    @if($vo->sst->acquisition->approval->acquisition_type_id == 1 || $vo->sst->acquisition->approval->acquisition_type_id == 2 || $vo->sst->acquisition->approval->acquisition_type_id == 4)
                    <table width="100%" class="bordered-double" border="1" style="font-size:12px; width: 100%;border-collapse: collapse;padding: 7 7; border-style:double;">
                        <tr style="text-align:center; font-weight:bold; ">
                            <td width="3%" style="padding: 7px 7px;">Bil.</td>
                            <td width="57%" style="padding: 7px 7px;">
                                Rujukan Kesilapan Keterangan Kerja Dan Kuantiti<br/>
                                (Rujuk Bahagian B Item 4.0)
                            </td>
                            <td width="40%" style="padding: 7px 7px;">Kerja Diberikan Kepada / Ulasan (Jika Ada)</td>
                        </tr>
                        @foreach($vo->types as $vot)
                            @if($vot->variation_order_type_id == 5)
                                @foreach($vot->elements as $key => $element)
                                    <tr style="text-align:left">
                                        <td style="padding: 5px 5px; text-align:center;">{{ $key + 1 }}.</td>
                                        <td style="padding: 5px 5px;">
                                            {{ $element->element }}
                                            &nbsp;-&nbsp;
                                            <b>@if($element->status == 1)Lulus @else Tidak Lulus @endif</b>
                                        </td>
                                        <td style="padding: 5px 5px;">{{ isset($element->comment) ? $element->comment : "-" }}</td>
                                    </tr>
                                @endforeach                    
                            @endif
                        @endforeach
                    </table>
                    @endif
                                
                </td>
            </tr>
            <tr>
                <td>
                    <div style="font-size:12px; padding: 5px 5px; text-decoration:underline; font-weight:bold;">Pengesahan Ahli Jawatankuasa Permohonan Perubahan Kerja</div>
                    <table border="1" style="width: 98%; font-size:11px; " align="center">
                        @php 
                            $total = 0; 
                        @endphp
                        
                        @foreach($vo->approvers as $approver)

                            @if($total == 0) <tr> @endif
                                
                                <td style="text-align:center;" width="23%" valign="top"> 
                                    <table width="100%" border="0" style="border-collapse: collapse;text-align:center;">
                                        <tr>
                                            <td style="text-align:center; text-transform:capitalize;">{{ $approver->role }}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/><br/><br/>
                                                {{ $approver->user->name }}<br/>
                                                {{ isset($approver->position) ? $approver->position->name : "-" }}<br/>
                                                {{ isset($approver->department) ? $approver->department->name : "-"}}<br/>
                                                Tarikh : 
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                        
                            @if($total == 3) 
                                </tr> 
                                @php $total = -1; @endphp
                            @endif

                            @php $total = $total + 1; @endphp
                        @endforeach

                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </body>
</html>