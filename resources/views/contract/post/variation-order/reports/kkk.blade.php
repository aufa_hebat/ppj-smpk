<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 80px 40px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 150px;

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                /* line-height: 35px; */
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 30px; 

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;

				
            }

			footer .pagenum:before {
      			content: counter(page);
			}

			div.breakNow { page-break-inside:avoid; page-break-after:always; }

        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
			<div style="text-align: right; font-size: 10px;">
				<span>SST Sebut Harga Kerja (Syarikat Yang <b>Telah Berdaftar</b> Dengan JKDM) Dan</span><br>
				<span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span>
			</div>
			
			<div style="text-align: left; font-size: 14px;">
				<span>*No.</span>
				
			</div>
			<br/><br/>
        </header>

        <footer>
			<div class="pagenum-container">Page <span class="pagenum"></span></div>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
			<br/><br/>

			<div>
				<b>KESILAPAN KETERANGAN KERJA DAN KUANTITI NO. {!! $vo->no !!}</b>
			</div>
			<br/>
        </main>
    </body>
</html>