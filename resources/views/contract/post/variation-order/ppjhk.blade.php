
<div class="row justify-content-center">
    <div class="col-12">
        @if($ipc_akhir == 0)
            <a href="{{ route('contract.post.variation-order-ppjhk.ppjhk',['hashslug' => $sst->hashslug]) }}" class="btn btn-primary float-right">Tambah PPJHK</a>
        @endif
            @include('contract.post.variation-order.ppjhk.partials.scripts')
            @component('components.card',['card_classes' => 'col-12'])
                @slot('card_body')
                    @component('components.datatable', 
                    [
                        'table_id'   => 'contract-post-variation-order-ppjhk',
                        'route_name' => 'api.datatable.contract.variation-order-ppjhk',
                        'param'     => 'hashslug=' . $sst->hashslug,
                        'columns' => [
                            ['data' => 'running_no', 'title' => __('Bil'), 'defaultContent' => '-'],
                            ['data' => 'ppjhk_no', 'title' => __('No PPJHK'), 'defaultContent' => '-'],
                            ['data' => 'no_kontrak', 'title' => __('No Rujukan'), 'defaultContent' => '-'],
                            ['data' => 'status_detail', 'title' => __('Status'), 'defaultContent' => '-'],
                            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                        ],
                        'headers' => [
                            __('Bil'),__('No PPJHK'),__('No Rujukan'),__('Status'), __('table.action')
                        ],
                        'actions' => minify(view('contract.post.variation-order.ppjhk.partials.actions')->render())
                    ])
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>