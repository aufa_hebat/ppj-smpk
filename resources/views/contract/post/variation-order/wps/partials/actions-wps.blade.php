<div class="text-center">
    <div class="item-action dropdown">
        <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
            <i class="fe fe-more-vertical"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
            style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
            {{ $prepend_action or '' }}
            <a class="dropdown-item show-wps-action-btn" style="cursor: pointer;"
                data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
                <i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
            </a>
            <a class="dropdown-item edit-wps-action-btn" style="cursor: pointer; display: '+data.{{'display'}}+'"
                data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
                <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
            </a>
            
            {{ $append_action or '' }}
            
            {{ $prepend_action or '' }}
            <a class="dropdown-item destroy-wps-action-btn" style="cursor: pointer; display: '+data.{{'display'}}+'"
                data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
                <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
            </a>
            {{ $prepend_action or '' }}


        </div>
    </div>
</div>