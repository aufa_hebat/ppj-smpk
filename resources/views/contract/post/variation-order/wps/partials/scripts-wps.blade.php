@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-wps-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.variation-order-wps.wps-show', id));
		});
		$(document).on('click', '.edit-wps-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
            redirect(route('contract.post.variation-order-wps.wps-edit', id));
		});
		
		$(document).on('click', '.destroy-wps-action-btn', function(event) {
			event.preventDefault();
			/* Act on the event */
			var id = $(this).data('hashslug');
			swal({
			  	title: '{!! __('Amaran') !!}',
			  	text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  	type: 'warning',
			  	showCancelButton: true,
			  	confirmButtonText: '{!! __('Ya') !!}',
			  	cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  	if (result.value) {
					axios.delete(
						route('api.contract.variation-order.destroy', id), 
						{'_method' : 'DELETE'})
						.then(response => {
							$('#contract-post-variation-order-wps').DataTable().ajax.reload();
							swal('{{ __('Padam Permohonan Perubahan Kerja') }}', response.data.message, 'success');
						}).catch(error => console.error(error));
			  	}
			});
		});
	       
	});
</script>
@endpush