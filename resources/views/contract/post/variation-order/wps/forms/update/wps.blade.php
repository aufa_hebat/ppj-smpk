@extends('layouts.admin')
@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#submit-action-btn').click(function(){
                var id = '{{ $vo->id ?? ''}}';
                var hashslug = '{{ $vo->hashslug ?? ''}}';
                var route_name = 'api.contract.submit-wps';
                var data = {
                };

                axios.post(route(route_name, hashslug), data).then(response => {
                    swal('Wang Peruntukan Sementara', response.data.message, 'success');
                    redirect(route('contract.post.variation-order-wps.wps-show', hashslug));
                });
        
            });
        });
    </script>
@endpush

@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    
                    @component('components.tab.container', ['id' => 'variation-order'])
                        @slot('tabs')
                         	@component('components.tab.content', ['id' => 'details', 'active' => true])
                                @slot('content')

                                    <h4>Senarai WPS Aktif</h4>
                                    <div class="row">
                                        <div class="col-12">
                                            <form id="vo-details">
                                                <input type="hidden" name="id" value="{{ $id }}">
                                                <table width="95%" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th width="60%">Wang Peruntukan Sementara (WPS)</th>
                                                            <th width="10%">Jumlah Peruntukan</th>
                                                            <th width="10%">Jumlah Telah Digunakan</th>
                                                            <th width="10%">Jumlah Sedang Digunakan</th>
                                                            <th width="10%">Baki</th>                                                        
                                                            <th width="10%">Tindakan</th>
                                                        </tr>  
                                                    </thead> 
                                                    <tbody>                                             
                                                        @foreach($sst->variationOrders as $vos)
                                                            @foreach($vos->actives->where('status', 1) as $active )
                                                                @php $statusWps = true; @endphp
                                                                @foreach($cancels as $cancel)
                                                                    @if($cancel->item->id == $active->item->id)
                                                                        @php $statusWps = false; @endphp
                                                                    @endif  
                                                                @endforeach

                                                                @if($statusWps)
                                                                    <tr>
                                                                        <td>{{ $active->item->item }}</td>
                                                                        <td>{{ money()->toCommon($active->amount ?? "0", 2) }}</td>
                                                                        <td>                                                                        
                                                                            {{ money()->toCommon( $wpsElement->where('vo_active_id', '=', $active->id)->where('vo_id', '!=', $vo->id)->pluck('net_amount')->sum() ?? "0", 2) }}
                                                                        </td>
                                                                        <td>                                                                        
                                                                            {{ money()->toCommon( $wpsElement->where('vo_active_id', '=', $active->id)->where('vo_id', '=', $vo->id)->pluck('net_amount')->sum() ?? "0", 2) }}
                                                                        </td>
                                                                        <td>
                                                                            {{  money()->toCommon(($active->amount - $wpsElement->where('vo_active_id', '=', $active->id)->pluck('net_amount')->sum()) ?? "0", 2) }}
                                                                        </td>                                                                        
                                                                        <td>
                                                                            <a href="{{ route('contract.post.variation-order-wps.wps-select',['vo' => $active->hashslug, 'id' => $vo->id]) }}">
                                                                                @icon('fe fe-edit')
                                                                            </a>    
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </form>
                                                                                    
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-9"></div>
                                        <div class="col-3">
                                            <button type="button" id="submit-action-btn" class="float-right btn btn-success float-middle border-default">
                                                @icon('fe fe-send') {{ __('Selesai') }}
                                            </button>                                             
                                            <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                                class="float-right btn btn-default border-primary">
                                                {{ __('Kembali') }}
                                            </a>
                                        </div>
                                    </div>
                                @endslot
                            @endcomponent
                            
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection