@extends('layouts.admin')

@section('content')
	@include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    
                    @component('components.tab.container', ['id' => 'variation-order'])
                        @slot('tabs')
                         	@component('components.tab.content', ['id' => 'details', 'active' => true])
                                @slot('content')

                                    <h4>Senarai WPS Aktif</h4>
                                    <div class="row">
                                        <div class="col-12">
                                            <form id="vo-details">
                                                <input type="hidden" name="id" value="{{ $id }}">
                                                @php $wpsIsEmpty = true; @endphp
                                                <table width="95%" class="table">
                                                    <thead>
                                                        <tr>
                                                            <td width="70%">Wang Peruntukan Sementara (WPS)</td>
                                                            <td width="15%">Jumlah Peruntukan</td>
                                                            <td width="15%">Jumlah Telah Digunakan</td>
                                                            <td width="15%">Baki</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($sst->variationOrders->where('status', 2) as $vos)
                                                        @foreach($vos->actives->where('status', 1) as $active )
                                                            @php $statusWps = true; @endphp
                                                            @foreach($cancels as $cancel)
                                                                @if($cancel->item->id == $active->item->id)
                                                                    @php $statusWps = false; @endphp
                                                                @endif  
                                                            @endforeach

                                                            @if($statusWps)
                                                                @php $wpsIsEmpty = false; @endphp
                                                                <tr>
                                                                    <td>
                                                                        {{ $active->item->item }}
                                                                    </td>
                                                                    <td>
                                                                        {{money()->toCommon($active->amount ?? "0" , 2)}}
                                                                    </td>
                                                                    <td>                                                                        
                                                                        {{ money()->toCommon( $wpsElement->where('vo_active_id', '=', $active->id)->pluck('net_amount')->sum() ?? "0", 2) }}
                                                                    </td>
                                                                    <td>
                                                                        {{  money()->toCommon(($active->amount - $wpsElement->where('vo_active_id', '=', $active->id)->pluck('net_amount')->sum()) ?? "0", 2) }}
                                                                    </td>
                                                                    {{--        
                                                                    <a href="{{ route('contract.post.variation-order-wps.select-wps',['vo' => $active->hashslug, 'id' => $vo->id]) }}">
                                                                        @icon('fe fe-edit')
                                                                    </a>  --}}
                                                                </tr>                                                           
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </form>                                            
                                        </div>
                                        <div class="col-1">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-9"></div>
                                        <div class="col-3">
                                            @if(!$wpsIsEmpty)
                                            <a href="{{ route('contract.post.variation-order-wps.wps-create', $sst->hashslug) }}" 
                                                class="float-right btn btn-primary border-primary">
                                                {{ __('Guna WPS') }}
                                            </a>           
                                            @endif                                   
                                            <a href="{{ route('contract.post.variation-order.edit', $sst->hashslug) }}" 
                                                class="float-right btn btn-default border-primary">
                                                {{ __('Kembali') }}
                                            </a>  
                                                                                     
                                        </div>
                                    </div>
                                @endslot
                            @endcomponent
                            
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection