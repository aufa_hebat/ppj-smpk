@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@section('content')
    @include('contract.post.variation-order.partials.title')
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')            
                    <h4>WPS & Kos Prima</h4>
                    <div class="row">
                        <div class="col-12"><h5>{{ $active->item->item }}</h5></div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            Jumlah Peruntukan Asal : {{ money()->toHuman($active->item->amount_adjust ?? "0", 2)}}
                        </div>
                        <div class="col-4">
                            Jumlah Peruntukan Terkini : {{ money()->toHuman($active->amount ?? "0", 2)}}
                        </div>                        
                        <div class="col-3">
                            Jumlah Telah Digunakan : {{ money()->toHuman($totalUsed ?? "0", 2)}}
                        </div>   
                        <div class="col-2">
                            Baki : {{ money()->toHuman($active->amount - $totalUsed ?? "0", 2)}}
                        </div>                       
                    </div> 
                    
                    <div class="row">
                        <div class="col-12">
                    @if(!empty($PKElemen))
                        <div id="table" class="table-editable table-bordered border-0">
                            @php $sumBq = 0; @endphp
    
                            @foreach($PKElemen as $keyE => $elemen)
                                @php $sumBq = $sumBq + $elemen->net_amount; @endphp
    
                                <div class="mb-0 card" >
                                    <div class="card-header" role="tab" id="heading{{$elemen->id}}">
                                        <h4 class="card-title cardElemen">
                                            <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{$elemen->id}}" href="#collapse{{$elemen->id}}" aria-expanded="true" aria-controls="collapse{{$elemen->id}}">
                                                <i class="mr-2 fa fa-folder-open"></i>
                                                <u>{{$keyE + 1 }}&nbsp;:&nbsp;{{$elemen->element}} &nbsp;&nbsp;&nbsp;
                                                    RM{{money()->toCommon($elemen->net_amount ?? 0,2 )}}
                                                </u>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$elemen->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{$elemen->id}}" >
                                        <div class="card-body">
                                            <div class="row">
                                                <div id="semakanBq" class="col-md-12 table-responsive">
                                                    <p>@if(!empty($elemen->description)) <u>{{ $elemen->description }}</u> @endif</p>
                                                    <table class="table1  table-no-border" width="100%" cellspacing="0">
                                                        @foreach($PKItem as $keyI => $it)                            
                                                            @if($elemen->id == $it->vo_element_id)   
                                                                <thead >
                                                                    <th style="width:100%;border:none" >
                                                                        <br/><br/>
                                                                        <h5>{{ $keyE + 1 }}.{{ $keyI + 1 }}&nbsp;:&nbsp;{{$it->item}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($it->net_amount ?? 0,2 )}}</h5>
                                                                        <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                                                    </th>
                                                                </thead>
                                                                <tbody class="list">
                                                                    <tr style="width:100%;border:none">
                                                                        <td class="ulasanBQs">
                                                                            <table id="ulasan_bq{{$it->id}}" class="table table-bordered" width="100%" cellspacing="0">
                                                                                <thead>
                                                                                    <tr style="font-size:13px;color:#495057;">
                                                                                        <td rowspan="2">NO SUB ITEM</td>
                                                                                        <td rowspan="2">SUB ITEM</td>
                                                                                        <td rowspan="2">KETERANGAN</td>
                                                                                        <td rowspan="2">UNIT</td>
                                                                                        <td rowspan="2">HARGA SEUNIT</td>
                                                                                        <td colspan="2">KUANTITI</td>                                                                            
                                                                                        <td colspan="2">AMAUN</td>
                                                                                        <td rowspan="2">NETT</td>
                                                                                    </tr>
                                                                                    <tr style="font-size:13px;color:#495057;">
                                                                                        <td>OMSN</td>
                                                                                        <td>ADD</td>
                                                                                        <td>OMSN</td>
                                                                                        <td>ADD</td>                                                                            
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @foreach($PKSub as $keyS => $bqs)
                                                                                        @if($bqs->vo_item_id == $it->id)
                                                                                            <tr style="font-size:13px;">
                                                                                                <td width="3%">{{ $keyE + 1}}.{{ $keyI + 1 }}.{{ $keyS + 1 }}</td>
                                                                                                <td width="43%">{{$bqs->item}}</td>
                                                                                                <td width="33%">{{$bqs->description}}</td>
                                                                                                <td width="3%">{{$bqs->unit}}</td>
                                                                                                <td width="3%">{{money()->toCommon($bqs->rate_per_unit ?? 0,2 )}}</td>
                                                                                                <td width="3%">{{money()->toCommon($bqs->quantity_omission ?? 0,2) }}</td>
                                                                                                <td width="3%">{{money()->toCommon($bqs->quantity_addition ?? 0,2) }}</td>
                                                                                                <td width="3%">{{money()->toCommon($bqs->amount_omission ?? 0,2 )}}</td>
                                                                                                <td width="3%">{{money()->toCommon($bqs->amount_addition ?? 0,2 )}}</td>
                                                                                                <td width="3%">{{money()->toCommon($bqs->net_amount ?? 0,2 )}}</td>
                                                                                            </tr>
                                                                                            @push('scripts')
                                                                                                <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
                                                                                                <script type="text/javascript">
                                                                                                    jQuery(document).ready(function($) {
                                                                                                        $("#ulasan_bq{{$it->id}}").DataTable();
                                                                                                    });
                                                                                                </script>
                                                                                            @endpush
                                                                                        @endif
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            @endif
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                        
                            @endforeach
                    
                            <div class="mb-0 card" >
                                <div class="card-header">
                                    <h4 class="card-title cardElemen">
                                        <i class="mr-2 fa fa-folder-open"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;Jumlah Keseluruhan : &nbsp;&nbsp;&nbsp;{{money()->toHuman($sumBq ?? 0,2 )}}
                                    </h4>
                                </div>
                            </div>
                        </div>
                    @endif 
                        </div>                   
                    </div>
                    <br/>
                    <a href="{{ route('contract.post.variation-order-wps.wps-show', $vo->hashslug) }}" 
                        class="float-right btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a> 
                @endslot
            @endcomponent
        </div>
    </div>
@endsection
    

   

