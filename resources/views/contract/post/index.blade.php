@extends('layouts.admin')
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
<script type="text/javascript">
    	jQuery(document).ready(function($) {
            $("#table_document").DataTable();
        });
</script>
@endpush
@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.partials.scripts')
            @component('components.card')
                @slot('card_body')
                <div class="table-responsive" >
                    <table id="table_document" class="table table-sm table-transparent table-hover table-responsive" style="min-height:240px">
                        <thead>
                            <th width='10%'>NO KONTRAK</th>
                            <th width='70%'>TAJUK</th>
                            <th width='15%'>SYARIKAT TERPILIH</th>
                            <th width='5%'>TINDAKAN</th>
                        </thead>
                        <tbody>
                        @if(!empty($sst))
                            @foreach($sst as $k => $contract)
                                @if($contract->status == 3)
                                    <tr>
                                        <td>{{ $contract->contract_no }}</td>
                                        <td>{{ $contract->acquisition->title }}</td>
                                        <td>{{ $contract->company->company_name }}</td>
                                        <td>
                                            <div class="text-center">
                                                <div class="item-action dropdown">
                                                    <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                                                    <i class="fe fe-more-vertical"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                                                        style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;"  >
                                                            <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.show',$contract->hashslug) }}" >
                                                                <i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
                                                            </a>
                                                            @role('penyedia||administrator||developer||urusetia')
                                                                <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.edit',$contract->hashslug) }}" >
                                                                    <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                                                </a>
                                                            @endrole
                                                            
                                                            
                                                            @if($contract->acquisition->approval->type->name == 'Perkhidmatan' || $contract->acquisition->approval->type->name == 'Bekalan')
                                                            
                                                                @if(!empty(user()->department_id) && user()->department_id != 3)

                                                                    <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cpc.edit',$contract->hashslug) }}"  >
                                                                        <i class="fe fe-file-text"></i> {{ __('Sijil Siap Kerja') }}
                                                                    </a>
                                                            
                                                                @endif
                                                                
                                                            @else
                                                            
                                                                @if(!empty(user()->department_id) && user()->department_id != 3)

                                                                    @if($contract->acquisition->approval->type->name == 'Penyelenggaraan' || $contract->acquisition->approval->type->name == 'Perkhidmatan' || $contract->acquisition->approval->type->name == 'Bekalan')
                                                                        <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cpc.edit',$contract->hashslug) }}"  >
                                                                            <i class="fe fe-file-text"></i> {{ __('Sijil Siap Kerja') }}
                                                                        </a>
                                                                        <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cnc.edit',$contract->hashslug) }}" >
                                                                            <i class="fe fe-file"></i> {{ __('Sijil Tidak Siap Kerja') }}
                                                                        </a>                                                                        
                                                                    @else
                                                                        <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cpc.edit',$contract->hashslug) }}"  >
                                                                                <i class="fe fe-file-text"></i> {{ __('Sijil Siap Kerja') }}
                                                                            </a>                                                                    
                                                                        <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cmgd.edit',$contract->hashslug) }}" >
                                                                            <i class="fe fe-file-plus"></i> {{ __('Sijil Siap Membaiki Kecacatan') }}
                                                                        </a>
                                                                        <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cnc.edit',$contract->hashslug) }}" >
                                                                            <i class="fe fe-file"></i> {{ __('Sijil Tidak Siap Kerja') }}
                                                                        </a>
                                                                        <a class="dropdown-item" style="cursor: pointer;" href="{{ route('contract.post.cpo.edit',$contract->hashslug) }}" >
                                                                            <i class="fe fe-file-minus"></i> {{ __('Sijil Siap Kerja Separa') }}
                                                                        </a>
                                                                    @endif
                                                                @endif

                                                            @endif
                                                            
                                                            
                                                            <a class="dropdown-item" style="cursor: pointer;" href="{{ route('termination.termination.edit',$contract->hashslug) }}" >
                                                                <i class="fas fa-plus"></i> {{ __('Tamat Kontrak') }}
                                                            </a>			
                                                    </div>
                                                </div>
                                            </div></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                @endslot 
            @endcomponent
        </div>
    </div>
@endsection