<hr>
<h4>Maklumat LAD</h4>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Anggaran Nilai (Siap Sebahagian)'),
            'id' => 'estimation_amount',
            'name' => 'estimation_amount',

        ])
        @include('components.forms.input', [
            'input_label' => __('Nilai semula LAD Sehari'),
            'id' => 'lad_per_day',
            'name' => 'lad_per_day',
            'readonly' => true,
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Nilaian Kerja Tidak Siap'),
            'id' => 'cpo_amount',
            'name' => 'cpo_amount',
            'readonly' => true,
        ])
    </div>
</div>