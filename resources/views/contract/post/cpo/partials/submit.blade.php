<div class="btn-group float-right">
	{{ html()->a(route('contract.post.index'), __('Batal'))->class('btn btn-outline-primary') }}

	<button type="submit" class="btn btn-primary float-right submit-action-btn" id="cpo-submit"
			data-route="{{ $route_name }}"
			data-form="{{ $form }}">
	    @icon('fe fe-save') {{ __('Simpan') }}
	</button>
</div>
