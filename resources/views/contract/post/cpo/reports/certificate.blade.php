<html>
    <head>
        <style type="text/css">
            @page {
                margin: 70px 70px 70px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }    
    
            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
                
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }  
    
            .content{
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
    
            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
        </style>
    </head>
    <body>
        <div>
            <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>
        </div>
        <table width="100%" style="text-align: center" class="title">
            <tr>
                <td>
                    <b>KERAJAAN MALAYSIA</b>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    <b>PERBADANAN PUTRAJAYA</b>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    <b>PERAKUAN PENDUDUKAN SEPARA</b><br>
                    <span style="font-style: italic; ">(CERTIFICATE OF PARTIAL OCCUPATION)</span>
                </td>
            </tr>
        </table>
        <table width="100%" class="content">
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td width="60%" valign="top">Rujukan: {{ $cpo->reference_no }}</td>
                <td width="40%">
                    <b>Pejabat Naib Presiden</b><br>
                    Jabatan Kejuruteraan Dan Penyelenggaraan<br>
                    Perbadanan Putrajaya,<br>
                    Kompleks Pebadanan Putrajaya<br>
                    24, Persiaran Perdana,<br>
                    Presint 3,
                    62675 Putrajaya<br>
                    <br>
                    Tarikh: {{  \Carbon::intlFormat(\Carbon\Carbon::now()) }}
                </td>
            </tr>
        </table>
        <br/>
        <table width="100%" class="content">
            <tr>
                <td colspan="3">
                    <span style="text-transform: uppercase"><b> {{ $sst->company->company_name }}</b></span><br>
                    @if(!empty($sst->company->addresses[0]))
                        @if(!empty($sst->company->addresses[0]->primary))
                            <span>{{ $sst->company->addresses[0]->primary }}</span><br />
                        @endif
                        @if(!empty($sst->company->addresses[0]->secondary))
                            <span>{{ $sst->company->addresses[0]->secondary }}</span><br />
                        @endif
                        @if(!empty($sst->company->addresses[0]->postcode))
                            <span>{{ $sst->company->addresses[0]->postcode }}, </span>
                        @endif
                        @if(!empty($sst->company->addresses[0]->state))
                            <span style=" text-transform: uppercase;">{{ $sst->company->addresses[0]->state }}</span>
                        @endif
                    @endif
                </td>
            </tr>
            <tr><td colspan="3"><br/></td></tr>
            <tr>
                <td width="20%">Berdaftar dengan</td>
                <td width="3%"> : </td>
                <td width="77%"><span style="font-weight: bold; text-transform: uppercase;">{{ $cidb }}</span></td>
            </tr>
            <tr>
                <td>Kontrak No.</td>
                <td> : </td>
                <td>{{ $sst->acquisition->reference }}</td>
            </tr>
            <tr>
                <td valign="top">Kontrak untuk </td>
                <td valign="top"> : </td>
                <td valign="top" style="font-weight: bold; font-decoration: underline; line-height: 1.6;">{{ $sst->acquisition->title }}</td>
            </tr>
             
            <tr>
                <td valign="top" colspan="3">
                    <hr>
                    Perihal Bahagian daripada Kerja yang diduduki : 
                    <span style="text-transform: uppercase; font-weight: bold; font-decoration: underline; line-height: 1.6;">{{ $cpo->part_of_work_title }}</span>
                </td>
            </tr>
        </table>
       
        <table width="100%" class="content">
            <tr>
                <td style="text-align: justify; line-height: 1.6;">
                    Menurut <b>Klausa {{ $cpo->clause }}</b> Syarat-Syarat Kontrak, Perbadanan berhasrat hendak mengambil milik
                    dan menduduki bahagian daripada Kerja seperti yang tersebut dia atas (kemudian dari ini disebut "Bahagian yang berkaitan")
                    sebelum sempurna siap sepenuhnya Kerja itu, dan dengan persetujuan tuan secara bertulis bertarikh
                    <b>{{ \Carbon::intlFormat($cpo->agreement_date) }}</b> mengenai perkara itu, maka adalah dengan ini diperakui bahawa Bahagian yang berkaitan
                    itu telah siap dengan memuaskan hati pada <b>{{  \Carbon::intlFormat($cpo->partial_date) }}</b> dan diambil milik dan
                    diduduki pada <b>{{  \Carbon::intlFormat($cpo->cpo_start_date) }}</b> dan dengan itu Tempoh Tanggungan Kecacatan mengenai Bahagian yang berkaitan itu
                    akan bermula pada <b>{{  \Carbon::intlFormat($cpo->cpo_start_date) }}</b> dan berakhir pada <b>{{  \Carbon::intlFormat($cpo->cpo_end_date) }}</b>.
                    <br><br>
                    2.&nbsp;&nbsp; Nilai yang dianggarkan bagi Bahagian berkaitan yang telah siap dan diambil milik dan diduduki ialah <b>{{  money()->toHuman($cpo->estimation_amount) }}</b>
                    <br><br>
                    3.&nbsp;&nbsp; Setelah Bahagian yang berkaitan itu siap dan diambil milik dan diduduki, Gantirugi Tertentu dan
                    Ditetapkan benyaknya seperti yang dinyatakan dalam Lampiran kepada Syarat-Syarat Kontrak, adalah dengan ini dikurangkan daripada
                    <b>{{  money()->toHuman($sst->document_LAD_amount) }}</b> Setiap hari kepada <b>{{  money()->toHuman($cpo->lad_per_day) }}</b> setiap hari
                    menurut <b>Klausa {{ $cpo->clause }}.1(d)</b> Syarat-Syarat Kontrak.
                </td>
            </tr>
        </table>
        <br><br>
        <table width="100%" class="content">
            <tr>
                <td></td>
                <td width="100px" style="text-align: center">
                    ..............................................................
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center">
                    Pegawai Penguasa/Wakil P.P
                </td>
            </tr>
        </table>
        <br>
        <table width="100%" class="content">
            <tr>
                <td></td>
                <td width="400px" style="text-align: right">
                    Nama Penuh:..........................................................................
                </td>
            </tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td></td>
                <td style="text-align: right">
                    Nama Jawatan:.......................................................................
                </td>
            </tr>
        </table>
    </body>
</html>
