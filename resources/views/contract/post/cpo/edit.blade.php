@extends('layouts.admin')
@include('components.forms.assets.datetimepicker')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {
            
            //$('#cpo_end_date').val('{{ $cpoEndDate->format('d/m/Y') }}');
            //add on change partial_date , Tarikh Siap Sebahagian + Tempoh DLP
            function addDays(date, days) {
               var result = new Date(date);
               result.setDate(result.getDate() + days);
              return result;
            }
            
            var dlpPeriod = {{$sst->defects_liability_period}};
            //alert(dlpPeriod);
              $("#datetimepicker-partial_date").on("change.datetimepicker", function (selected) {
                if(selected.date != undefined){
                    //alert('hi');
                    
                    //var addDate = addDays(selected.date,5);
                    var x = dlpPeriod; //or whatever offset
                    var CurrentDate = new Date(selected.date);
                    CurrentDate.setMonth(CurrentDate.getMonth() + x);
                    //alert(CurrentDate.format("DD/MM/YYYY"));
                     var newDT = moment(CurrentDate).format('DD/MM/YYYY');
                     //alert(newDT);
                   
                    $('#cpo_end_date').val(newDT);
                   
                }
             });
           
            
            
            

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    document_contract_type : $('#document_contract_type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cpo_print').hide();
            $('#assessment_date').val('{{ $sst->siteVisits->count()>0 ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->siteVisits->pluck('visited_at')->last())->format('d/m/Y'):null }}');
            

            @if(!empty($cpo))
                $('#cpo_print').show();

                $('#agreement_date').val('{{ $cpo->agreement_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->agreement_date)->format('d/m/Y'):null }}');
                $('#partial_date').val('{{ $cpo->partial_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->partial_date)->format('d/m/Y'):null }}');
                $('#cpo_start_date').val('{{ $cpo->cpo_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->cpo_start_date)->format('d/m/Y'):null }}');
                $('#cpo_end_date').val('{{ $cpo->cpo_end_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->cpo_end_date)->format('d/m/Y'):null }}');
                $('#estimation_amount').val('{{ $cpo->estimation_amount ? money()->toCommon($cpo->estimation_amount) : '0.00'}}');
                $('#lad_per_day').val('{{ $cpo->lad_per_day ? money()->toHuman($cpo->lad_per_day) : 'RM 0.00'}}');
                $('#cpo_amount').val('{{ $cpo->cpo_amount ? money()->toHuman($cpo->cpo_amount) : 'RM 0.00'}}');
                $('#reference_no').val('{{ $cpo->reference_no}}');
                $('#clause').val('{{ $cpo->clause}}');
                $('#part_of_work_title').val('{{ $cpo->part_of_work_title}}');
            @else
                $('#reference_no').val('{{ $sst->acquisition->approval->file_reference }}');
            @endif

            $('#estimation_amount').keyup(function(val){

                var nilai_kerja_siap = $(this).val();
                var harga_kontrak = '{{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}';
                harga_kontrak = harga_kontrak.replace(/[,]/g, "");
                harga_kontrak = harga_kontrak.split(' ').join('');
                    
                //var jumlah_telah_dibayar = '{{ $sst->ipc->pluck('demand_amount')->sum() }}';
               // alert(jumlah_telah_dibayar);
               //var jumlah_telah_dibayar = '{{ money()->toCommon($sst->ipc->pluck('demand_amount')->sum()?? "0" , 2) }}';
                
                // bayar yang status di SAP paidIpc
                 var jumlah_telah_dibayar = '{{ money()->toCommon($paid_ipc->pluck('demand_amount')->sum()?? "0" , 2) }}';
                
                jumlah_telah_dibayar = jumlah_telah_dibayar.replace(/[,]/g, "");
                jumlah_telah_dibayar = jumlah_telah_dibayar.split(' ').join('');
                
               // alert(jumlah_telah_dibayar);
                var kerja_tidak_siap = harga_kontrak - jumlah_telah_dibayar - nilai_kerja_siap;
                //alert(kerja_tidak_siap);
                
               
                $('#cpo_amount').val('RM '+kerja_tidak_siap.toLocaleString('{{ config('app.locale') }}', {maximumFractionDigits:2 , minimumFractionDigits: 2}));

                //$('#cpo_amount').val('RM '+kerja_tidak_siap);

                var blr='{{ $sst->document_blr ? money()->toCommon($sst->document_blr) : ''}}';
                var lad_sehari = kerja_tidak_siap * (blr/100)/365;
                $('#lad_per_day').val('RM '+lad_sehari.toLocaleString('{{ config('app.locale') }}', {maximumFractionDigits:2 , minimumFractionDigits: 2}));

            });

            /* DOCUMENT UPLOAD */
            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($cpo) && ($cpo->documents->count() > 0))
            @foreach($cpo->documents as $doc)
            t.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow').click();
            @endif

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                }
            });
            });

            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD */

            $(document).on('click', '#cpo-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                // Display the key/value pairs
                for (var pair of data.entries()) {
                    console.log(pair[0]+ '= ' + pair[1]);
                }

                axios.post(route(route_name, id), data ).then( (response) => {
                    swal('{!! __('Sijil Siap Kerja Separa') !!}', response.data.message, 'success');
                    $('#cpo_print').show();
                    location.reload();
                }).catch((error)=>{
                        console.log(error.response);
                });
            });

        });

    </script>
@endpush

@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if($sst->acquisition->approval->acquisition_type_id != 1 && $sst->acquisition->approval->acquisition_type_id != 3)
                <span class="font-weight-bold">Tempoh DLP : </span>{{ $sst->defects_liability_period." Bulan" }}&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if($sst->eots->count()>0)
                <span class="font-weight-bold">Tarikh Siap Lanjutan : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->eots->pluck('extended_end_date')->last())->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
        @endslot
    @endcomponent

    @component('components.card')
        @slot('card_body')
            <h4>Sijil Siap Kerja Separa</h4>
            <div class="row" id="cpo_print">
                <div class="col-12">
                    <div class="btn-group float-right">
                        <a target="_blank" href="{{ route('report.certificate.cpo', ['hashslug'=>$sst->hashslug] ) }}"
                           class="btn btn-success">
                            @icon('fe fe-printer') {{ __('Cetak') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-12">
                    <form id="cpo-form" method="post" enctype="multipart/form-data" files = "true">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-6">
                                @include('components.forms.input', [
                                    'input_label' => __('No Rujukan Fail'),
                                    'id' => 'reference_no',
                                    'name' => 'reference_no',
                                ])
                                @include('components.forms.datetimepicker', [
                                    'input_label' => __('Tarikh CPO di Tandatangani'),
                                    'id' => 'cpo_start_date',
                                    'name' => 'cpo_start_date',
                                    'config' => [
                                        'format' => config('datetime.display.date'),
                                    ],
                                ])
                                @include('components.forms.datetimepicker', [
                                    'input_label' => __('Tarikh Siap Sebahagian'),
                                    'id' => 'partial_date',
                                    'name' => 'partial_date',
                                    'config' => [
                                        'format' => config('datetime.display.date'),
                                    ],
                                ])
                                @include('components.forms.input', [
                                    'input_label' => __('Klausa Kontrak'),
                                    'id' => 'clause',
                                    'name' => 'clause',
                                ])
                            </div>
                            <div class="col-6">
                                @include('components.forms.datetimepicker', [
                                    'input_label' => __('Tarikh Lawatan Tapak / Mesyuarat'),
                                    'id' => 'assessment_date',
                                    'name' => 'assessment_date',
                                    'config' => [
                                        'format' => config('datetime.display.date'),
                                    ],
                                   
                                ])
                                @include('components.forms.datetimepicker', [
                                    'input_label' => __('Tarikh Tamat CPO'),
                                    'id' => 'cpo_end_date',
                                    'name' => 'cpo_end_date',
                                    'config' => [
                                        'format' => config('datetime.display.date'),
                                    ],

                                ])
                                @include('components.forms.datetimepicker', [
                                    'input_label' => __('Tarikh Surat Persetujuan (Siap Sebahagian)'),
                                    'id' => 'agreement_date',
                                    'name' => 'agreement_date',
                                    'config' => [
                                        'format' => config('datetime.display.date'),
                                    ],
                                ])
                                @include('components.forms.input', [
                                    'input_label' => __('Perihal Bahagian daripada Kerja'),
                                    'id' => 'part_of_work_title',
                                    'name' => 'part_of_work_title',
                                ])
                            </div>
                        </div>

                        @include('contract.post.cpo.partials.lad') 
                        @include('contract.post.cpo.partials.uploads')

                    </form>
                    <div class="btn-group float-right">
                        @role('penyedia')
                            @if(!empty($cpo) && $cpo->sst->user_id == user()->id)
                                @if(empty($review) || empty($review->status))
                                    <form method="POST" action="{{ route('acquisition.review.store') }}">
                                        @csrf
                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                        <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $cpo->sst->acquisition_id }}" hidden>

                                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                                        <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                                        @if(!empty($review_previous))
                                            <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                            <input type="text" id="type" name="type" value="Dokumen Kontrak" hidden>
                                            <input type="text" id="document_contract_type" name="document_contract_type" value="CPO" hidden>
                                        @endif
                                        
                                        <button type="button" id="send" class="btn btn-default float-middle border-default">
                                            @icon('fe fe-send') {{ __('Teratur') }}
                                        </button>
                                    </form>
                                @endif
                            @endif
                        @endrole
                    </div>
                    @include('contract.post.cpo.partials.submit', [
                        'route_name' => 'api.contract.acquisition.cpo.update',
                        'form' => 'cpo-form'
                    ])
                </div>


            </div>
        @endslot
    @endcomponent

    @if((!empty($review)) && (user()->id == $review->created_by))
        @component('components.card')
            @slot('card_body')
            <h4>Senarai Ulasan</h4>
                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                    {{-- penyedia --}}

                    {{-- @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                        Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl7 as $s7log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s7log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif --}}

                    {{-- @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                                        Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl6 as $s6log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s6log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif --}}

                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl5 as $s5log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s5log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif

                    {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl4 as $s4log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s4log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif

                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl3 as $s3log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s3log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif --}}

                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl2 as $s2log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s2log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif

                    @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                        <div class="mb-0 card card-primary">
                            <div class="card-header" role="tab" id="headingOne2">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                <div class="card-body">
                                    @foreach($sl1 as $s1log)
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Terima</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Tarikh Hantar</div>
                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-muted">Status</div>
                                                <p>{!! $s1log->reviews_status !!}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-muted">Ulasan</div>
                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endif
                
                </div>
            @endslot
        @endcomponent
    @endif
@endsection