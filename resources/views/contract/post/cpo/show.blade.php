@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(!empty($cpo))
                $('#cpo_reference_no').html('{{ $cpo->reference_no }}');
                $('#cpo_clause').html('{{ $cpo->clause }}');
                $('#cpo_part_of_work_title').html('{{ $cpo->part_of_work_title }}');

                $('#cpo_start_date').html('{{ $cpo->cpo_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->cpo_start_date)->format('d/m/Y') : '' }}');
                $('#cpo_partial_date').html('{{ $cpo->partial_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->partial_date)->format('d/m/Y') : '' }}');
                $('#cpo_assessment_date').html('{{ $cpo->assessment_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->assessment_date)->format('d/m/Y') : '' }}');
                $('#cpo_end_date').html('{{ $cpo->cpo_end_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->cpo_end_date)->format('d/m/Y') : '' }}');
                $('#cpo_agreement_date').html('{{ $cpo->agreement_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cpo->agreement_date)->format('d/m/Y') : '' }}');

                // UPLOADS
                @if($cpo->documents->count() > 0)
                    var cpo_t = $('#cpo_tblUpload').DataTable({
                            searching: false,
                            ordering: false,
                            paging: false,
                            info:false
                        });

                    var cpo_counter = 1;
                    @foreach($cpo->documents as $doc)
                        cpo_t.row.add( [
                            cpo_counter,
                            '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                        ] ).draw( false );

                        cpo_counter++;
                    @endforeach
                @endif
                // UPLOADS END

            @endif
        });

    </script>
@endpush

<h4>Sijil Siap Kerja Separa</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="btn-group float-right">
            <a target="_blank" href="{{ route('report.certificate.cpo', ['hashslug'=>$sst->hashslug] ) }}"
               class="btn btn-success">
                @icon('fe fe-printer') {{ __('Cetak') }}
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('No Rujukan Fail') }}</div>
                    <div id="cpo_reference_no"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh CPO di Tandatangani') }}</div>
                    <div id="cpo_start_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Siap Sebahagian') }}</div>
                    <div id="cpo_partial_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Kontrak') }}</div>
                    <div id="cpo_clause"></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Lawatan Tapak / Mesyuarat') }}</div>
                    <div id="cpo_assessment_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Tamat CPO') }}</div>
                    <div id="cpo_end_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Surat Persetujuan (Siap Sebahagian)') }}</div>
                    <div id="cpo_agreement_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Perihal Bahagian daripada Kerja') }}</div>
                    <div id="cpo_part_of_work_title"></div>
                </div>
            </div>
        </div>

        @include('contract.post.cpo.partials.show.uploads')

        @include('contract.post.partials.shows.return')
    </div>
</div>