@extends('layouts.admin')

@push('style')
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush

@section('content')
@include('components.forms.assets.datetimepicker')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.extension.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'acceptance-letter',
                            'route_name' => 'api.datatable.contract.extension',
                            'columns' => [
                                ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                ['data' => 'acquisition_title', 'title' => __('Perolehan'), 'defaultContent' => '-'],
                                ['data' => 'company_name', 'title' => __('Syarikat Berjaya'), 'defaultContent' => '-'],
                                ['data' => 'created_at', 'title' => __('Menanti Kelulusan'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Perolehan'), __('Perolehan'), __('Syarikat Berjaya'), __('Menanti Kelulusan'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.extension.partials.actions', [])->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection