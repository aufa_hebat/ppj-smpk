@if($appointed_company->acquisition->approval->acquisition_type_id == '2')
<h3><b>Maklumat Perakuan Kelambatan dan Pelanjutan Masa</b></h3>
@else
<h3><b>Maklumat Pelanjutan Tempoh Kontrak</b></h3>
@endif
<div class="row">

    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-1">
            @include('components.forms.input', [
                'input_label' => 'Bil',
                'name' => 'bil',
                'id' => 'bil',
                'readonly' => 'readonly'
            ])
        </div>
    @endif
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => 'Permohonan Tempoh EOT',
            'name' => 'period',
            'id' => 'period'
        ])
    </div>
    
    <div class="col-2">
        @include('components.forms.select', [
            'input_label' => __('Jenis Tempoh'),
            'options' => period_types()->pluck('name', 'id'),
            'id' => 'period_type_id',
            'name' => 'period_type_id',
        ])
    </div>
    <div class="col-2">
        @include('components.forms.input', [
            'input_label' => 'Tarikh Siap Lanjutan',
            'name' => 'extended_end_date',
            'id' => 'extended_end_date',
            'readonly' => 'readonly'
        ])
    </div>
    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-3">
            <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Bahagian</label><br>
            <div class="selectgroup w-25 grade-container">
                <label class="selectgroup-item">
                <input type="radio" id="category1" name="section" value="1" class="selectgroup-input" required>
                    <span class="selectgroup-button">{{ __('Keseluruhan') }}</span>
                </label>
                <label class="selectgroup-item">
                <input type="radio" id="category2" name="section" value="2" class="selectgroup-input">
                    <span class="selectgroup-button">{{ __('Sebahagian') }}</span>
                </label>
            </div>
        </div>
    @endif
</div>
<div class="row">
    <div class="col-4">
        @include('components.forms.hidden', [
            'id' => 'original_end_date',
            'name' => 'original_end_date',
            'value' => '',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => 'readonly'
        ])
    </div>
</div>

{{-- table --}}
<div class="row">
    <div class="col-md-12">
        <div class="float-right">
            <button type="button" id="addRowJKPembuka" href="#" class="btn btn-primary addmore">
                @icon('fe fe-plus')
                {{ __('Tambah Sebab') }}
            </button>
        </div><br>
        <div class="form-group">
            <div id="tableJKPembuka" class="table table-responsive">
                <table class="table table-sm table-transparent">
                    <tr>
                        <th width="45%">Sebab Sebab</th>
                        <th width="15%">Klausa</th>
                        <th width="35%">Tempoh Kelambatan Dan Lanjutan Masa</th>
                        <th width="5%"><b><span class="addmore">HAPUS</span></b></th>
                    </tr>
                    <tr id="klon0" class="d-none">
                        <td>
                            <input type="text" class="form-control" name="baru[0][reason]" id="reason_0" value="">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="baru[0][clauses]" id="clauses_0" value="">
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-4">
                                    <input type="text" class="form-control" name="baru[0][period]" id="period_0" value="">
                                </div>
                                <div class="col-8" style="margin-top: -22.5px">
                                    @include('components.forms.select', [
                                        'input_label' => __(''),
                                        'options' => period_types()->pluck('name', 'id'),
                                        'id' => 'period_type_0',
                                        'name' => 'baru[0][period_type]',
                                    ])
                                </div>
                            </div>
                        </td>
                        <td>
                            <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                        </td>
                    </tr>
               </table>
            </div>
        </div>
    </div>
</div>