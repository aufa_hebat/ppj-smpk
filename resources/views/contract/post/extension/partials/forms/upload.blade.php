<div id="uploads">
    <hr>
    <h4>Muat Naik Dokumen Sokongan</h4>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                @if(!isset($eot->eot_approve->id))
                    <button type="button" id="addRowSokongan" href="#" class="btn btn-primary">
                        @icon('fe fe-plus')
                        {{ __('Tambah Dokumen') }}
                    </button>
                @endif
            </div>
            <div class="table-responsive">
                <table id="tblUploadSokongan" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th>Muat Naik Dokumen Sokongan</th>
                        @if(!isset($eot->eot_approve->id))
                            <th width="70px">Hapus</th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

<br>

<div id="uploads1">
    <hr>
    <h4>Muat Naik Laporan Eksekutif</h4>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                @if(!isset($eot->eot_approve->id))
                    <button type="button" id="addRowLaporan" href="#" class="btn btn-primary">
                        @icon('fe fe-plus')
                        {{ __('Tambah Laporan') }}
                    </button>
                @endif
            </div>
            <div class="table-responsive">
                <table id="tblUploadLaporan" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th>Muat Naik Laporan Eksekutif</th>
                        @if(!isset($eot->eot_approve->id))
                            <th width="70px">Hapus</th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

<br>

<div id="uploads2">
    <hr>
    <h4>Muat Naik Kertas Pertimbangan Presiden (Lanjutan)</h4>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                @if(!isset($eot->eot_approve->id))
                    <button type="button" id="addRowKertas" href="#" class="btn btn-primary">
                        @icon('fe fe-plus')
                        {{ __('Tambah Kertas Pertimbangan') }}
                    </button>
                @endif
            </div>
            <div class="table-responsive">
                <table id="tblUploadKertas" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th>Muat Naik Kertas Pertimbangan Presiden (Lanjutan)</th>
                        @if(!isset($eot->eot_approve->id))
                            <th width="70px">Hapus</th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>