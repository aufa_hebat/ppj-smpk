<div class="text-center">
	<div class="item-action dropdown">
	  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
	  	<i class="fe fe-more-vertical"></i>
	  </a>
	  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
	  	style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
	  	{{ $prepend_action or '' }}
	  	@role('administrator||developer||penyedia||penyemak||pengesah')
	  		{{-- acquisition_type_id=2		     --}}
			<a class="dropdown-item show-action-btn" style="cursor: pointer; display: '+data.{{'display'}}+'"
				data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
				<i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
			</a>
			@role('penyedia')
				<a class="dropdown-item edit-action-btn" style="cursor: pointer; display: '+data.{{'display5'}}+'"
					data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
					<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
				</a>
				{{-- <a class="dropdown-item print-action-btn" target="_blank" style="cursor: pointer; display: '+data.{{'display'}}+'"
					data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
					<i class="fe fe-printer text-success"></i> {{ __('Cetak') }}
				</a> --}}
				<a class="dropdown-item add-action-btn" style="cursor: pointer; display: '+data.{{'display4'}}+'"
					data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
					<i class="fas fa-plus"></i> {{ __('Tambah Pelanjutan Baru') }}
				</a>
			@endrole
			{{-- acquisition_type_id=1/3/4		     --}}  
			<a class="dropdown-item eot-show-action-btn" style="cursor: pointer; display: '+data.{{'display2'}}+'"
				data-{{ $primary_key or 'eot_hashslug' }}="' + data.{{ $primary_key or 'eot_hashslug' }} + '">
				<i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
			</a>
			@role('penyedia')
				<a class="dropdown-item eot-edit-action-btn" style="cursor: pointer; display: '+data.{{'display2'}}+'"
					data-{{ $primary_key or 'eot_hashslug' }}="' + data.{{ $primary_key or 'eot_hashslug' }} + '">
					<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
				</a>	

				<a class="dropdown-item print-action-btn" target="_blank" style="cursor: pointer; display: '+data.{{'display2'}}+'"
					data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
					<i class="fe fe-printer text-success"></i> {{ __('Cetak Sijil') }}
				</a>

				<a class="dropdown-item print-letter-action-btn" target="_blank" style="cursor: pointer; display: '+data.{{'display2'}}+'"
					data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
					<i class="fe fe-printer text-success"></i> {{ __('Cetak Surat') }}
				</a>	    
				<a class="dropdown-item add-action-btn" style="cursor: pointer; display: '+data.{{'display3'}}+'"
					data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
					<i class="fas fa-plus"></i> {{ __('Tambah Pelanjutan Baru') }}
				</a> 
			@endrole
		@endrole
		@role('developer||urusetia||administrator')
			<a class="dropdown-item approve-action-btn" style="cursor: pointer;"
				data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
				<i class="fe fe-check-square"></i> {{ __('Kelulusan') }}
			</a>
		@endrole
		{{ $append_action or '' }}
	  </div>
	</div>
</div>