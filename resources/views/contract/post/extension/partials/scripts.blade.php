@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('extension.extension_show.index', {hashslug:id}));
		});
		$(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('extension.extension_edit.index', {hashslug:id}));
		});
		$(document).on('click', '.eot-edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('eot_hashslug');
			redirect(route('extension.extension_edit.edit', id));
		});
		$(document).on('click', '.eot-show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('eot_hashslug');
			redirect(route('extension.extension_show.show', id));
		});
		$(document).on('click', '.approve-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('extension.extension_approve.index', {hashslug:id}));
		});
		$(document).on('click', '.add-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('extension.extension.create', {hashslug:id}));
		});
		$(document).on('click', '.print-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: 'PERINGATAN',
			  text: 'Sila cetak menggunakan kertas berwarna KUNING',
			  type: 'info'
			}).then((result) => {
				// redirect(route('extension_report', {hashslug:id}));
				window.open(route('extension_report', {hashslug:id}), '_blank');
			});
		});
		$(document).on('click', '.print-letter-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: 'PERINGATAN',
			  text: 'Sila cetak menggunakan kertas berwarna KUNING',
			  type: 'info'
			}).then((result) => {
				window.open(route('extension_letter_report_bekalan_perkhidmatan', {hashslug:id}), '_blank');
			});
		});
	});
</script>
@endpush