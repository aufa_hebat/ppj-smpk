@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
		$(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('extension.extension_approve.edit', id));
		});
		
	});
</script>
@endpush