@if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
	<a class="dropdown-item edit-action-btn" style="cursor: pointer;"
		data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
		<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
	</a>
@endif