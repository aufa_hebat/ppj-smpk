@if($appointed_company->acquisition->approval->acquisition_type_id == '2')
<h3><b>Maklumat Perakuan Kelambatan dan Pelanjutan Masa</b></h3>
@else
<h3><b>Maklumat Pelanjutan Tempoh Kontrak</b></h3>
@endif

<div class="row">
    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-1">
            @include('components.forms.input', [
                'input_label' => 'Bil',
                'name' => 'bil',
                'id' => 'bil',
                'readonly' => 'readonly'
            ])
        </div>
    @endif
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => 'Permohonan Tempoh EOT',
            'name' => 'period',
            'id' => 'period',
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-2">
        @include('components.forms.select', [
            'input_label' => __('Jenis Tempoh'),
            'options' => period_types()->pluck('name', 'id'),
            'id' => 'period_type_id',
            'name' => 'period_type_id',
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-2">
        @include('components.forms.input', [
            'input_label' => 'Tarikh Siap Lanjutan',
            'name' => 'extended_end_date',
            'id' => 'extended_end_date',
            'readonly' => 'readonly'
        ])
    </div>
    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-3">
            <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Bahagian</label><br>
            <div class="selectgroup w-25 grade-container">
                <label class="selectgroup-item">
                <input type="radio" id="category1" name="section" value="1" class="selectgroup-input" disabled="true">
                    <span class="selectgroup-button">{{ __('Keseluruhan') }}</span>
                </label>
                <label class="selectgroup-item">
                <input type="radio" id="category2" name="section" value="2" class="selectgroup-input" disabled="true">
                    <span class="selectgroup-button">{{ __('Sebahagian') }}</span>
                </label>
            </div>
        </div>
    @endif
</div>
<div class="row">
    <div class="col-4">
        @include('components.forms.hidden', [
            'id' => 'original_end_date',
            'name' => 'original_end_date',
            'value' => '',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => 'readonly'
        ])
    </div>
</div>

{{-- table --}}
<div class="row">
    <div class="col-md-12">
        @if(user()->current_role_login == 'administrator')
            <div class="float-right">
                <button type="button" id="addDetail href="#" class="btn btn-primary addmore">
                    @icon('fe fe-plus')
                    {{ __('Tambah Sebab') }}
                </button>
            </div>
        @endif
        <div class="form-group">
                <div id="tableJKPembuka" class="table table-responsive">
                    <table class="table table-sm table-transparent">
                        <tr>
                            <th width="45%">Sebab Sebab</th>
                            <th width="15%">Klausa</th>
                            <th width="35%">Tempoh Kelambatan Dan Lanjutan Masa</th>
                            @if(user()->current_role_login == 'administrator')
                                <th width="5%"><b><span class="addmore">HAPUS</span></b></th>
                            @endif
                        </tr>

                        @foreach($eot->eot_details as $details)
                            <tr>
                                <td>
                                    <input type="hidden" name="exist[{{$details->id}}][id]" id="exist_id" value="{{$details->id}}">
                                    <input type="text" class="form-control" name="exist[{{$details->id}}][reason]" id="exist_reason" value="{{ $details->reasons }}" @if(user()->current_role_login == 'urusetia') readonly="true" @endif>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="exist[{{$details->id}}][clauses]" id="exist_clauses" value="{{ $details->clause }}" @if(user()->current_role_login == 'urusetia') readonly="true" @endif>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="text" class="form-control" name="exist[{{$details->id}}][period]" id="exist_period" value="{{ $details->periods }}" @if(user()->current_role_login == 'urusetia') readonly="true" @endif>
                                        </div>
                                        <div class="col-8">
                                            <select class="select2 form-control" id="exist_period_type" name="exist[{{$details->id}}][period_type]" @if(user()->current_role_login == 'urusetia') disabled="true" @endif>
                                                <option value=""></option>
                                                @foreach(period_types() as $peri)
                                                    <option value="{{$peri->id}}" @if ($peri->id == old('period_type_id', $details->period_type_id))
                                                    selected="selected"
                                                    @endif >{{$peri->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>

                                @if(user()->current_role_login == 'administrator')
                                    <td>
                                        <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        <tr id="klon0" class="d-none">
                            <td><input type="text" id="reason_0" name="baru[0][reason]" class="form-control" style="width:100%;"></td>
                            <td><input type="text" id="clauses_0" name="baru[0][clauses]" class="form-control" style="width:100%;"></td>
                            <td>
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="form-control" name="baru[0][period]" id="period_0" value="">
                                    </div>
                                    <div class="col-8" style="margin-top: -22.5px">
                                        @include('components.forms.select', [
                                            'input_label' => __(''),
                                            'options' => period_types()->pluck('name', 'id'),
                                            'id' => 'period_type_0',
                                            'name' => 'baru[0][period_type]',
                                        ])
                                    </div>
                                </div>
                            </td>

                            @if(user()->current_role_login == 'administrator')
                                <td>
                                    <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                </td>
                            @endif
                        </tr>
                   </table>
                </div>
        </div>
    </div>
</div>

{{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4') --}}
    <h3>Muat Naik Dokumen</h3>
    <div id="uploads_sokongan">
        <hr>
        <h4>Muat Naik Dokumen Sokongan</h4>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="tblUpload_sokongan" class="table table-sm table-transparent">
                        <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th >Nama Dokumen</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div id="uploads_laporan">
        <hr>
        <h4>Muat Naik Laporan Eksekutif</h4>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="tblUpload_laporan" class="table table-sm table-transparent">
                        <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th >Nama Dokumen</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div id="uploads_kertas">
        <hr>
        <h4>Muat Naik Kertas Pertimbangan Presiden (Lanjutan)</h4>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="tblUpload_kertas" class="table table-sm table-transparent">
                        <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th >Nama Dokumen</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
{{-- @endif --}}