<h3><b>Maklumat Kelulusan</b></h3>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => 'Bil Mesyuarat',
            'name' => 'bil_meeting',
            'id' => 'bil_meeting'
        ])
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mesyuarat J/K Sebutharga'),
            'id' => 'eot_approve_meeting_date',
            'name' => 'eot_approve_meeting_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
</div>
<div class="row">
    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => 'Kelulusan Tempoh Perakuan Kelambatan dan Pelanjutan Masa',
                'name' => 'eot_approve_period',
                'id' => 'eot_approve_period'
            ])
        </div>
    @else
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => 'Kelulusan Tempoh Lanjutan Kontrak',
                'name' => 'eot_approve_period',
                'id' => 'eot_approve_period'
            ])
        </div>
    @endif
    <div class="col-6">
        @include('components.forms.select', [
            'input_label' => __('Jenis Tempoh'),
            'options' => period_types()->pluck('name', 'id'),
            'id' => 'eot_approve_period_type_id',
            'name' => 'eot_approve_period_type_id'
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => 'Tarikh Siap Diluluskan',
            'name' => 'approved_end_date',
            'id' => 'approved_end_date',
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-6">
        <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Keputusan</label><br>
        <div class="selectgroup w-25 grade-container">
            <label class="selectgroup-item">
            <input type="radio" id="eot_approve_approval" name="eot_approve_approval" value="2" class="selectgroup-input" >
                <span class="selectgroup-button">{{ __('GAGAL') }}</span>
            </label>
            <label class="selectgroup-item">
            <input type="radio" id="eot_approve_approval" name="eot_approve_approval" value="1" class="selectgroup-input" >
                <span class="selectgroup-button">{{ __('LULUS') }}</span>
            </label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div id="uploads">
            <hr>
            <h4>Muat Naik Keputusan Mesyuarat</h4>
            <div class="row">
                <div class="col-12">
                    <div class="float-right">
                        <button type="button" id="addRow" href="#" class="btn btn-primary">
                            @icon('fe fe-plus')
                            {{ __('Tambah Dokumen') }}
                        </button>
                    </div>
                    <div class="table-responsive">
                        <table id="tblUpload" class="table table-sm table-transparent">
                            <thead>
                            <tr>
                                <th>Muat Naik Dokumen</th>
                                <th width="70px">Hapus</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="btn-group float-right">
    <a href="{{ route('extension.extension_approve.index', ['hashslug' => $appointed_company->hashslug]) }}" 
            class="btn btn-default border-primary">
            {{ __('Kembali') }}
    </a>
    <button type="submit" class="btn btn-primary submit-action-btn">
        @icon('fe fe-save') {{ __('Simpan') }}
    </button>
</div>