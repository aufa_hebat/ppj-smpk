@extends('layouts.admin')

@push('style')
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush

@section('content')
@include('components.forms.assets.datetimepicker')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.extension.approve_list.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'extension_approve',
                            'param' => 'appointed_company_id=' . $appointed_company->id,
                            'route_name' => 'api.datatable.contract.extension_approve',
                            'columns' => [
                                ['data' => 'bil', 'title' => __('Bil'), 'defaultContent' => '-'],
                                ['data' => 'extended_time', 'title' => __('Masa Lanjutan Diminta'), 'defaultContent' => '-'],
                                ['data' => 'extended_time_approved', 'title' => __('Masa Lanjutan Diluluskan'), 'defaultContent' => '-'],
                                ['data' => 'approval', 'title' => __('Status Kelulusan'), 'defaultContent' => '-'],
                                ['data' => 'created_at', 'title' => __('table.created_at'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('Bil'), __('Masa Lanjutan Diminta'), __('Masa Lanjutan Diluluskan'), __('Status Kelulusan'), __('table.created_at'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.extension.approve_list.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection