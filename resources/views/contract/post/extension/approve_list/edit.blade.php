@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    @include('components.forms.assets.datetimepicker')
	<script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
            // $('#document-tab-content a').click(function (e) {
            //     e.preventDefault();
            //     $(this).tab('show');
            // });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh

            // button for review
            $('#savDraf').click(function(){
                var route_name = 'extension.extension_approve.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'extension.extension_approve.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'extension.extension_approve.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($noti) && $noti = $review)
                @if($noti->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$noti->remarks) !!}');
                @endif
            @endif
            
            $('#bil').val('{{ $eot->bil }}');
            $('#period').val('{{ $eot->period }}') ;
            $("#period_type_id").val('{{ $eot->period_type_id }}').attr("disabled", true);
            $("#period_type_ids").attr("disabled", false);
            $('#original_end_date').val('{{ $latest_end_date }}') ;
            $('#extended_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->extended_end_date)->format('d/m/Y') }}') ;
            @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
            $('input:radio[name=section][value={{ $eot->section }}]').prop('checked', true);
            @endif
            $('#eot_id').val('{{ $eot->id }}');
            $('#eot_hashslug').val('{{ $eot->hashslug }}');

            @if(!isset($eot->eot_approve->id))
                $('#eot_approve_id').val('first');
            @else
                $('#eot_approve_id').val('{{ $eot->eot_approve->id }}');
                $('#eot_approve_period').val('{{ $eot->eot_approve->period }}') ;
                $("#eot_approve_period_type_id").val('{{ $eot->eot_approve->period_type_id }}');
                $('#bil_meeting').val('{{ $eot->eot_approve->bil_meeting }}');
                $('#eot_approve_meeting_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->eot_approve->meeting_date)->format('d/m/Y') }}');
                $('#approved_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s',$eot->eot_approve->approved_end_date)->format('d/m/Y') }}');
                $('input:radio[name=eot_approve_approval][value={{ $eot->eot_approve->approval }}]').prop('checked', true);
                
            @endif
            $(".addmore").on('click',function(){
                var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
                  $klon.find('#reason_'+currnum).attr('id','reason_'+num).attr('name','baru['+num+'][reason]').val('');
                  $klon.find('#clauses_'+currnum).attr('id','clauses_'+num).attr('name','baru['+num+'][clauses]').val('');
                  $klon.find('#period_'+currnum).attr('id','period_'+num).attr('name','baru['+num+'][period]').val('');
                  $klon.find('#period_type_'+currnum).attr('id','period_type_'+num).attr('name','baru['+num+'][period_type]').val('');
                $('.addmore').parents('table').append($klon);
                console.log(currnum);
            });
            $('.buang').click(function () {
                $(this).parents('tr').detach();
            });

            @if(isset($eot->eot_approve->document->document_name))
                $('#subFile').val('{{ $eot->eot_approve->document->document_name }}');
            @endif

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });


            /* DOCUMENT UPLOAD */
            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($eot->eot_approve) && ($eot->eot_approve->documents->count() > 0))
            @foreach($eot->eot_approve->documents as $doc)
            t.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow').click();
            @endif

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD */

            $('#eot_approve_period_type_id').change(function() {
                var originalEndDate, approvedEndDate;
                if($("#eot_approve_period").val() != undefined){

                    originalEndDate = moment('{{ $latest_end_date }}', "DD/MM/YYYY");
                    
                    if($("#eot_approve_period_type_id").val() == 1){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'days');
                    }else if($("#eot_approve_period_type_id").val() == 2){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'weeks');
                    }else if($("#eot_approve_period_type_id").val() == 3){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'months');
                    }else if($("#eot_approve_period_type_id").val() == 4){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'years');
                    }
                    
                    $("#approved_end_date").val(approvedEndDate.format("DD/MM/YYYY"));
                }
            });

            $('#eot_approve_period').keyup(function() {
                var originalEndDate, approvedEndDate;
                if($("#eot_approve_period_type_id").val() != undefined){

                    originalEndDate = moment('{{ $latest_end_date }}', "DD/MM/YYYY");
                    
                    if($("#eot_approve_period_type_id").val() == 1){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'days');
                    }else if($("#eot_approve_period_type_id").val() == 2){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'weeks');
                    }else if($("#eot_approve_period_type_id").val() == 3){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'months');
                    }else if($("#eot_approve_period_type_id").val() == 4){
                        approvedEndDate = moment((originalEndDate)).add($( "#eot_approve_period" ).val(), 'years');
                    }

                    $("#approved_end_date").val(approvedEndDate.format("DD/MM/YYYY"));
                }
            });

            $(document).on('click', '.submit-action-btn', function(event) {
                
                event.preventDefault();
            	var id = '{{ $eot->hashslug }}';
                var route_name = 'api.contract.extension_approve.update';
                var form_id = 'extension_approve_form';
                var form = document.forms[form_id];
                var data = new FormData(form);
                axios.post(route(route_name, id), data).then(response => {
                    swal('Maklumat Kelulusan Pelanjutan Tempoh Kontrak', response.data.message, 'success').then((result) => {
                        if (result.value) {
                            window.location.replace('');
                        }
                    });
                });
            });

            @if($eot->support_documents->count() > 0)
                var t_sokongan = $('#tblUpload_sokongan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_sokongan = 1;
                @foreach($eot->support_documents as $doc)
                    t_sokongan.row.add( [
                        counter_sokongan,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_sokongan++;
                @endforeach
            @endif

            @if($eot->executif_documents->count() > 0)
                var t_laporan = $('#tblUpload_laporan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_laporan = 1;
                @foreach($eot->executif_documents as $doc)
                    t_laporan.row.add( [
                        counter_laporan,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_laporan++;
                @endforeach
            @endif

            @if($eot->president_documents->count() > 0)
                var t_kertas = $('#tblUpload_kertas').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_kertas = 1;
                @foreach($eot->president_documents as $doc)
                    t_kertas.row.add( [
                        counter_kertas,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_kertas++;
                @endforeach
            @endif
        });
	</script>
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        @component('components.pages.title-sub')
            @slot('title_sub_content')
                <span class="font-weight-bold">Tajuk Perolehan : </span>{{ $sst->acquisition->title }}
                <br>
                <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nilai Kontrak : </span>{{ money()->toHuman($appointed_company->offered_price) }}
                <br>
                <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ $sst->start_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tempoh : </span>{{ $sst->period.' '.$sst->period_type->name }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ $sst->end_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                @if(!empty($new_eot_date))
                    <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
                @endif
            @endslot
        @endcomponent
    </div>
</div>
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#extension" role="tab" aria-controls="extension" aria-selected="false">
                    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                        @icon('fe fe-file')&nbsp;{{ __('Sijil Kelambatan dan Pelanjutan Masa') }}
                    @else
                        @icon('fe fe-file')&nbsp;{{ __('Pelanjutan Tempoh Kontrak') }}
                    @endif
                </a>
            </li>
            @if(!empty($review))
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#eot-reviews" role="tab" aria-controls="eot-reviews" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#eot-review-log" role="tab" aria-controls="eot-review-log" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif
            @endif
            @if($review->status == 'Selesai' || $review->approved_by == $review->created_by)
            @else
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#eot-review" role="tab" aria-controls="eot-review" aria-selected="false">
                        @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                    </a>
                </li>
            @endif
            {{-- @if($review->status == 'CN' || $review->approved_by == $review->created_by)
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#eot-review" role="tab" aria-controls="eot-review" aria-selected="false">
                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Semakan') }}
                </a>
            </li>
            @endif --}}
                {{-- <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#approve" role="tab" aria-controls="approve" aria-selected="false">
                        @icon('fe fe-check-square')&nbsp;{{ __('Kelulusan') }}
                    </a>
                </li> --}}
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'extension', 'active' => true])
                            @slot('content')
                                <form id="extension_approve_form" files="true" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="eot_approve_id" id="eot_approve_id">
                                    <input type="hidden" name="eot_id" id="eot_id">
                                    <input type="hidden" name="eot_hashslug" id="eot_hashslug">
	                                @include('contract.post.extension.approve_list.partials.forms.extension', [])
                                    @if(empty($review) || $review->status == 'S1' || $review->status == 'S2')
                                    @else
                                        @include('contract.post.extension.approve_list.partials.forms.approve', [])
                                    @endif
                                </form>
                            @endslot
                        @endcomponent

                        <div class="tab-pane fade show" id="eot-reviews" role="tabpanel" aria-labelledby="eot-details-tab">
                            @if(!empty($review))
                                @if( ($appointed_company->acquisition->approval->acquisition_type_id == '2' && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2')) || (($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3'|| $appointed_company->acquisition->approval->acquisition_type_id == '4') && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2' || $appointed_company->acquisition->acquisition_category_id == '3' || $appointed_company->acquisition->acquisition_category_id == '4')) )
                                    @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                        Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sln as $s5log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2 as $s2log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1 as $s1log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}
                                @else
                                    @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                        Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sln as $s5log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5 as $s5log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4 as $s4log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3 as $s3log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2 as $s2log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1 as $s1log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}
                                @endif
                            @endif
                        </div>
                        <div class="tab-pane fade show " id="eot-review-log" role="tabpanel" aria-labelledby="eot-review-log-tab">
                            <form id="log-form">
                                <div class="row justify-content-center w-100">
                                    <div class="col-12 w-100">
                                        @include('contract.pre.box.partials.scripts')
                                        @component('components.card')
                                            @slot('card_body')
                                                @component('components.datatable', 
                                                    [
                                                        'table_id' => 'contract-pre-box',
                                                        'route_name' => 'api.datatable.contract.review-log-eot',
                                                        'param' => 'eot_id=' . $eot->id,
                                                        'columns' => [
                                                            ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                            ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                        ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                            ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                        ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                        ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                        ],
                                                        'headers' => [
                                                            __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                        ],
                                                        'actions' => minify('')
                                                    ]
                                                )
                                                @endcomponent
                                            @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade show" id="eot-review" role="tabpanel" aria-labelledby="eot-review-tab">
                            <form  method="POST" action="{{ route('extension.extension_approve.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @include('components.forms.hidden', [
                                    'id' => 'eot_id',
                                    'name' => 'eot_id',
                                    'value' => $eot->id
                                ])

                                @include('components.forms.hidden', [
                                    'id' => 'acquisition_id',
                                    'name' => 'acquisition_id',
                                    'value' => $appointed_company->acquisition_id
                                ])

                                @if(!empty($review))
                                <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
                                <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                    <input type="text" id="type" name="type" value="EOT" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($review))
                                            @if(!empty($review->status))
                                                @include('components.forms.input', [
                                                    'input_label' => __('Tarikh Terima'),
                                                    'id' => 'requested_at',
                                                    'name' => 'requested_at',
                                                ])
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($review))
                                            @if(!empty($review->status))
                                                @include('components.forms.input', [
                                                    'input_label' => __('Tarikh Hantar'),
                                                    'id' => 'approved_ats',
                                                    'name' => 'approved_at',
                                                ])
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                        
                                @if(!empty($review_previous) && $sst->acquisition->approval->acquisition_type_id == 2)
                                    @include('components.forms.hidden', [
                                        'id' => 'approved_by',
                                        'name' => 'approved_by',
                                        'value' => $review_previous->task_by
                                    ])
                                    <input type="hidden" id="progress" name="progress" value="1">
                                    <input type="hidden" id="status" name="status" value="S3">
                                    <input type="hidden" id="department" name="department" value="BPUB">
                                @else
                                    @include('components.forms.hidden', [
                                        'id' => 'approved_by',
                                        'name' => 'approved_by',
                                        'value' => user()->supervisor->id
                                    ])
                                @endif

                                @if(!empty($review) && $review->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif
                                
                                <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($review) && $review->created_by != user()->id)
                                        <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="savSent" class="btn btn-success float-middle border-default ">
                                        @icon('fe fe-check') {{ __('Selesai') }}
                                    </button>
                                </div>

                            </form>
                        </div>
                        {{-- @component('components.tab.content', ['id' => 'approve'])
                            @slot('content')
                                
                            @endslot
                        @endcomponent --}}
                    @endslot
                @endcomponent
                @slot('card_footer')
                    
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection