@extends('layouts.admin')

@push('scripts')
    @include('components.forms.assets.datetimepicker')
	<script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
            // $('#document-tab-content a').click(function (e) {
            //     e.preventDefault();
            //     $(this).tab('show');
            // });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh
            
            // button for review
            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if($eot->support_documents->count() > 0)
                var t_sokongan = $('#tblUpload_sokongan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_sokongan = 1;
                @foreach($eot->support_documents as $doc)
                    t_sokongan.row.add( [
                        counter_sokongan,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_sokongan++;
                @endforeach
            @endif

            @if($eot->executif_documents->count() > 0)
                var t_laporan = $('#tblUpload_laporan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_laporan = 1;
                @foreach($eot->executif_documents as $doc)
                    t_laporan.row.add( [
                        counter_laporan,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_laporan++;
                @endforeach
            @endif

            @if($eot->president_documents->count() > 0)
                var t_kertas = $('#tblUpload_kertas').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_kertas = 1;
                @foreach($eot->president_documents as $doc)
                    t_kertas.row.add( [
                        counter_kertas,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_kertas++;
                @endforeach
            @endif

            @if(!empty($eot->eot_approve) && ($eot->eot_approve->documents->count() > 0))
                var t_result = $('#tblUpload_result').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_result = 1;
                @foreach($eot->eot_approve->documents as $doc)
                    t_result.row.add( [
                        counter_result,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_result++;
                @endforeach
            @endif

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            @if(!empty($eot) && !empty($eot->documents))
                @foreach($eot->documents as $document)
                    $('#subFile').val('{{ $document->document_name }}');
                @endforeach
            @endif

            @if(!empty($eot->eot_approve))
                $('#bil_meeting').val('{{ $eot->eot_approve->bil_meeting }}');
                $('#eot_approve_id').val('{{ $eot->eot_approve->id }}');
                $('#eot_approve_period').val('{{ $eot->eot_approve->period }}') ;
                $("#eot_approve_period_type_id").val('{{ $eot->eot_approve->period_type_id }}').attr("disabled", true);;
                $('#eot_approve_bil').val('{{ $eot->eot_approve->bil }}');
                $('#eot_approve_meeting_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d  H:i:s', $eot->eot_approve->meeting_date)->format('d/m/Y') }}');
                $('#approved_end_date').val('{{ $latest_end_date }}');
                $('input:radio[name=eot_approve_approval][value={{ $eot->eot_approve->approval }}]').prop('checked', true);

                $('#bil').val('{{ $eot->bil }}') ;
                $('#period').val('{{ $eot->period }}') ;
                $("#period_type_id").val('{{ $eot->period_type_id }}').attr("disabled", true);
                $('#original_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->original_end_date)->format('d/m/Y') }}') ;
                $('#extended_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->extended_end_date)->format('d/m/Y') }}') ;
                @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                    $('input:radio[name=section][value={{ $eot->section }}]').prop('checked', true);
                @endif
                $('#eot_id').val('{{ $eot->id }}');
            @endif

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif
        });
	</script>
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        @component('components.pages.title-sub')
            @slot('title_sub_content')
                <span class="font-weight-bold">Tajuk Perolehan : </span>{{ $sst->acquisition->title }}
                <br>
                <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nilai Kontrak : </span>{{ money()->toHuman($appointed_company->offered_price) }}
                <br>
                <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ $sst->start_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tempoh : </span>{{ $sst->period.' '.$sst->period_type->name }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ $sst->end_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                @if(!empty($new_eot_date))
                    <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
                @endif
            @endslot
        @endcomponent
    </div>
</div>
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#extension" role="tab" aria-controls="extension" aria-selected="false">
					@if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                    	@icon('fe fe-file')&nbsp;{{ __('Sijil Kelambatan dan Pelanjutan Masa') }}
                	@else
                		@icon('fe fe-file')&nbsp;{{ __('Pelanjutan Tempoh Kontrak') }}
                	@endif
                </a>
            </li>

            @if(!empty($review))
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#eot-reviews" role="tab" aria-controls="eot-reviews" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#eot-review-log" role="tab" aria-controls="eot-review-log" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif

                @if(
                    (
                        (user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'EOT') 
                        || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S4') && $review->status != 'Selesai') 
                        || (!empty($semakan4) && user()->id == $semakan4->approved_by && user()->id != $review->created_by && (empty($semakan5) || $reviewlog->status != 'S5') && $review->status != 'Selesai') 
                        || (!empty($semakan5) && user()->id == $semakan5->approved_by && user()->id != $review->created_by && (empty($cetaknotisbpub) || $reviewlog->status != 'CNBPUB') && $review->status != 'Selesai' && (user()->current_role_login != 'pengesah' || user()->department_id != 9 || user()->executor_department_id != 25))
                    )
                   )
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#eot-review" role="tab" aria-controls="eot-review" aria-selected="false">
                            @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                        </a>
                    </li>
                @endif
                 {{-- || ((!empty($semakan6)) && ($semakan6->status == 'S6' && $semakan6->approved_by == NULL) && user()->department->id == '3') --}}

            @endif

            {{-- @if(!empty($eot->bon))
	            <li class="list-group-item">
	                <a class="list-group-item-action" data-toggle="tab" href="#bon" role="tab" aria-controls="bon" aria-selected="false">
	                    @icon('fe fe-award')&nbsp;{{ __('Bon') }}
	                </a>
	            </li>
	        @endif
	        @if(!empty($eot->insurance))
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                    @icon('fe fe-dollar-sign')&nbsp;{{ __('Insurans') }}
                </a>
            </li>
            @endif --}}

            @if(!empty($eot->eot_approve))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#approve" role="tab" aria-controls="approve" aria-selected="false">
                        @icon('fe fe-check-square')&nbsp;{{ __('Kelulusan') }}
                    </a>
                </li>
                @if($eot->eot_approve->approval == 1)
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#bon" role="tab" aria-controls="bon" aria-selected="false">
                            @icon('fe fe-award')&nbsp;{{ __('Bon') }}
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                            @icon('fe fe-dollar-sign')&nbsp;{{ __('Insurans') }}
                        </a>
                    </li>
                @endif
            @endif
            @role('penyedia')
                @if(!empty($review) && $review->status == 'Selesai')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#upload" role="tab" aria-controls="upload" aria-selected="false">
                            @icon('fe fe-upload')&nbsp;{{ __('Keputusan EOT') }}
                        </a>
                    </li>
                @endif
            @endrole
        </ul>
    </div>
    <div class="col-10">
    	<div id="acceptance_letter_form">
	        @component('components.card')
	            @slot('card_body')
	                @component('components.tab.container', ['id' => 'document'])
	                    @slot('tabs')
	                     	@component('components.tab.content', ['id' => 'extension', 'active' => true])
	                            @slot('content')
	                                @include('contract.post.extension.show_list.partials.forms.extension', [])
	                                <div class="btn-group float-right">
				                        <a href="{{ route('extension.extension_show.index', ['hashslug' => $appointed_company->hashslug]) }}" 
				                                class="btn btn-default border-primary">
				                                {{ __('Kembali') }}
				                        </a>
				                        {{-- @role('penyedia')
					                        <a href="{{ route('extension_report',['hashslug' => $eot->hashslug]) }}" 
					                        		target="_blank"
				                                    class="btn btn-success border-success">
				                                    @icon('fe fe-printer'){{ __(' Cetak') }}
				                            </a>
			                            @endrole --}}
				                    </div>
	                            @endslot
	                        @endcomponent

                            @if(!empty($eot_approve))
                                @component('components.tab.content', ['id' => 'approve'])
                                    @slot('content')
                                        @include('contract.post.extension.edit_list.partials.forms.approve', [])
                                    @endslot
                                @endcomponent
    	                        @component('components.tab.content', ['id' => 'bon'])
    	                            @slot('content')
    	                                @include('contract.post.extension.show_list.partials.forms.bon', [])
    	                                <div class="btn-group float-right">
    				                        <a href="{{ route('extension.extension_show.index', ['hashslug' => $appointed_company->hashslug]) }}" 
    				                                class="btn btn-default border-primary">
    				                                {{ __('Kembali') }}
    				                        </a>
    				                        {{-- <a href="{{ route('extension_report',['hashslug' => $eot->hashslug]) }}" 
    			                                    class="btn btn-success border-success">
    			                                    @icon('fe fe-printer'){{ __(' Cetak') }}
    			                            </a> --}}
    				                    </div>
    	                            @endslot
    	                        @endcomponent
    	                        @component('components.tab.content', ['id' => 'insurance'])
    	                            @slot('content')
    	                                @include('contract.post.extension.show_list.partials.forms.insurance', [])
    	                                <div class="btn-group float-right">
    				                        <a href="{{ route('extension.extension_show.index', ['hashslug' => $appointed_company->hashslug]) }}" 
    				                                class="btn btn-default border-primary">
    				                                {{ __('Kembali') }}
    				                        </a>
    				                        {{-- <a href="{{ route('extension_report',['hashslug' => $eot->hashslug]) }}" 
    			                                    class="btn btn-success border-success">
    			                                    @icon('fe fe-printer'){{ __(' Cetak') }}
    			                            </a> --}}
    				                    </div>
    	                            @endslot
    	                        @endcomponent
                            @endif

			                <div class="tab-pane fade show" id="eot-reviews" role="tabpanel" aria-labelledby="eot-details-tab">
			                    
			                	@if(!empty($review))
                                    @if(user()->current_role_login == 'penyedia')
	                                    
                                        @if( ($appointed_company->acquisition->approval->acquisition_type_id == '2' && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2')) || (($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4') && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2' || $appointed_company->acquisition->acquisition_category_id == '3' || $appointed_company->acquisition->acquisition_category_id == '4')) )
                                            @if(!empty($cetaknotisns) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                        
                                        @if( ($appointed_company->acquisition->approval->acquisition_type_id == '2' && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2')) || (($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4') && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2' || $appointed_company->acquisition->acquisition_category_id == '3' || $appointed_company->acquisition->acquisition_category_id == '4')) )
                                            @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}
                                        @else
                                            @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}
                                        @endif
				                	@else
		                                {{-- pegawai bpub --}}
		                                @if(user()->department->id == '9')
                                            @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

		                                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
		                                        <div class="mb-0 card card-primary">
		                                            <div class="card-header" role="tab" id="headingOne2">
		                                                <h4 class="card-title">
		                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
		                                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
		                                                    </a>
		                                                </h4>
		                                            </div>
		                                            <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
		                                                <div class="card-body">
		                                                    @foreach($sl5 as $s5log)
		                                                        <div class="row">
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Status</div>
		                                                                <p>{!! $s5log->reviews_status !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <div class="row">
		                                                            <div class="col-12">
		                                                                <div class="text-muted">Ulasan</div>
		                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <hr>
		                                                    @endforeach

		                                                </div>
		                                            </div>
		                                        </div>
		                                    @endif

		                                    @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
		                                        <div class="mb-0 card card-primary">
		                                            <div class="card-header" role="tab" id="headingOne2">
		                                                <h4 class="card-title">
		                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
		                                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
		                                                    </a>
		                                                </h4>
		                                            </div>
		                                            <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
		                                                <div class="card-body">
		                                                    @foreach($sl4 as $s4log)
		                                                        <div class="row">
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Status</div>
		                                                                <p>{!! $s4log->reviews_status !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <div class="row">
		                                                            <div class="col-12">
		                                                                <div class="text-muted">Ulasan</div>
		                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <hr>
		                                                    @endforeach

		                                                </div>
		                                            </div>
		                                        </div>
		                                    @endif

		                                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
		                                        <div class="mb-0 card card-primary">
		                                            <div class="card-header" role="tab" id="headingOne2">
		                                                <h4 class="card-title">
		                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
		                                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
		                                                    </a>
		                                                </h4>
		                                            </div>
		                                            <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
		                                                <div class="card-body">
		                                                    @foreach($sl3 as $s3log)
		                                                        <div class="row">
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Status</div>
		                                                                <p>{!! $s3log->reviews_status !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <div class="row">
		                                                            <div class="col-12">
		                                                                <div class="text-muted">Ulasan</div>
		                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <hr>
		                                                    @endforeach

		                                                </div>
		                                            </div>
		                                        </div>
		                                    @endif
                                            
                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl2 as $s2log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif

		                                @endif

		                                {{-- pegawai pelaksana --}}
		                                @if(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)
		                                    
		                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
		                                        <div class="mb-0 card card-primary">
		                                            <div class="card-header" role="tab" id="headingOne2">
		                                                <h4 class="card-title">
		                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
		                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
		                                                    </a>
		                                                </h4>
		                                            </div>
		                                            <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
		                                                <div class="card-body">
		                                                    @foreach($sl2 as $s2log)
		                                                        <div class="row">
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Status</div>
		                                                                <p>{!! $s2log->reviews_status !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <div class="row">
		                                                            <div class="col-12">
		                                                                <div class="text-muted">Ulasan</div>
		                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <hr>
		                                                    @endforeach

		                                                </div>
		                                            </div>
		                                        </div>
		                                    @endif

		                                    @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
		                                        <div class="mb-0 card card-primary">
		                                            <div class="card-header" role="tab" id="headingOne2">
		                                                <h4 class="card-title">
		                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
		                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
		                                                    </a>
		                                                </h4>
		                                            </div>
		                                            <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
		                                                <div class="card-body">
		                                                    @foreach($sl1 as $s1log)
		                                                        <div class="row">
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
		                                                            </div>
		                                                            <div class="col-4">
		                                                                <div class="text-muted">Status</div>
		                                                                <p>{!! $s1log->reviews_status !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <div class="row">
		                                                            <div class="col-12">
		                                                                <div class="text-muted">Ulasan</div>
		                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
		                                                            </div>
		                                                        </div>
		                                                        <hr>
		                                                    @endforeach

		                                                </div>
		                                            </div>
		                                        </div>
		                                    @endif

		                                @endif
	                                @endif
	                            @endif

			                </div>
			                <div class="tab-pane fade show " id="eot-review-log" role="tabpanel" aria-labelledby="eot-review-log-tab">
			                    <form id="log-form">
			                        <div class="row justify-content-center w-100">
			                            <div class="col-12 w-100">
			                                @include('contract.pre.box.partials.scripts')
			                                @component('components.card')
			                                    @slot('card_body')
			                                        @component('components.datatable', 
			                                            [
			                                                'table_id' => 'contract-pre-box',
			                                                'route_name' => 'api.datatable.contract.review-log-eot',
			                                                'param' => 'eot_id=' . $eot->id,
			                                                'columns' => [
                                                				['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
			                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
			                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
			                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
			                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
			                                                ],
			                                                'headers' => [
			                                                    __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
			                                                ],
			                                                'actions' => minify('')
			                                            ]
			                                        )
			                                        @endcomponent
			                                    @endslot
			                                @endcomponent
			                            </div>
			                        </div>
			                    </form>
			                </div>
			                <div class="tab-pane fade show" id="eot-review" role="tabpanel" aria-labelledby="eot-review-tab">
			                    <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
			                        @csrf

			                        @include('components.forms.hidden', [
			                            'id' => 'id',
			                            'name' => 'id',
			                            'value' => ''
			                        ])

			                        @include('components.forms.hidden', [
			                            'id' => 'acquisition_id',
			                            'name' => 'acquisition_id',
			                            'value' => $sst->acquisition_id
			                        ])

			                        @include('components.forms.hidden', [
			                            'id' => 'eot_id',
			                            'name' => 'eot_id',
			                            'value' => $eot->id
			                        ])

			                        @if(!empty($review))
			                        <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
			                        <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

			                        <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
			                        @endif

			                        @if(!empty($review_previous))
			                            <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
			                            <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
			                            <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
			                        	<input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
			                            <input type="text" id="type" name="type" value="EOT" hidden>
			                        @endif

			                        <div class="row">
			                            <div class="col-6">
			                                @include('components.forms.input', [
			                                    'input_label' => __('Nama Pegawai Semakan'),
			                                    'id' => '',
			                                    'name' => '',
			                                    'value' => user()->name,
			                                    'readonly' => true
			                                ])
			                            </div>
			                                @include('components.forms.hidden', [
			                                    'id' => 'requested_by',
			                                    'name' => 'requested_by',
			                                    'value' => user()->id
			                                ])
                                        <div class="col-3">
                                            @if(!empty($review))
                                                @if(!empty($review->status))
                                                    @include('components.forms.input', [
                                                        'input_label' => __('Tarikh Terima'),
                                                        'id' => 'requested_at',
                                                        'name' => 'requested_at',
                                                    ])
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-3">
                                            @if(!empty($review))
                                                @if(!empty($review->status))
                                                    @include('components.forms.input', [
                                                        'input_label' => __('Tarikh Hantar'),
                                                        'id' => 'approved_ats',
                                                        'name' => 'approved_at',
                                                    ])
                                                @endif
                                            @endif
                                        </div>
			                        </div>
			                                
			                        {{-- @if(!empty($review_previous) && ($sst->acquisition->acquisition_category_id == 3 || $sst->acquisition->acquisition_category_id == 4))
			                        	@include('components.forms.hidden', [
			                                'id' => 'approved_by',
			                                'name' => 'approved_by',
			                                'value' => $review_previous->task_by
			                            ])
		                                <input type="hidden" name="progress" value="1">
		                                <input type="hidden" name="status" value="S3">
		                                <input type="hidden" name="department" value="BPUB">
			                        @else --}}
			                            @include('components.forms.hidden', [
			                                'id' => 'approved_by',
			                                'name' => 'approved_by',
			                                'value' => user()->supervisor->id
			                            ])
			                        {{-- @endif --}}

			                        @if(!empty($review) && $review->created_by != user()->id)
			                            @include('components.forms.textarea', [
			                                'input_label' => __('Ulasan'),
			                                'id' => 'remarks',
			                                'name' => 'remarks'
			                            ])
			                        @else

			                        @endif
			                        
			                        <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
			                        <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
			                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

			                        <div class="btn-group float-right">
			                            @if(!empty($review) && $review->created_by != user()->id)
			                                <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
			                                    @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
			                                </button>
			                                <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
			                                    @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
			                                </button>
			                            @endif
	                                    <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
	                                        @icon('fe fe-send') {{ __('Teratur') }}
	                                    </button>
			                        </div>

			                    </form>
			                </div>

                            @if(!empty($eot_approve))
                                @component('components.tab.content', ['id' => 'upload'])
                                    @slot('content')
                                        <form id="upload_form" files = "true" enctype="multipart/form-data" method="POST">
                                            @csrf
                                            <h4>Keputusan EOT</h4>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <label for="Fail_Keputusan_Mesyuarat"
                                                           class="col col-form-label">
                                                            Keputusan EOT
                                                        </label>

                                                        <div class="col input-group">
                                                            <input id="subFile" type="text"  class="form-control" readonly>
                                                            @if(!empty($eot) && !empty($eot->documents))
                                                                @foreach($eot->documents as $document)
                                                                    <label class="input-group-text" for="downloadFile">
                                                                        <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                                                    </label>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    @endslot
                                @endcomponent
                            @endif

	                    @endslot
	                @endcomponent
	                @slot('card_footer')
	                    
	                @endslot

	            @endslot
	        @endcomponent
    	</div>
    </div>
</div>
@endsection