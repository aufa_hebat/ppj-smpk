@extends('layouts.admin')

@push('style')
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.extension.show_list.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'acceptance-letter',
                            'route_name' => 'api.datatable.contract.extension_edit',
                            'param' => 'appointed_company_id=' . $appointed_company->id,
                            'columns' => [
                                ['data' => 'bil', 'title' => __('Bil'), 'defaultContent' => '-'],
                                ['data' => 'extended_time_requested', 'title' => __('Lanjutan Masa Diminta'), 'defaultContent' => '-'],
                                ['data' => 'extended_time_approved', 'title' => __('Lanjutan Masa Diluluskan'), 'defaultContent' => '-'],
                                ['data' => 'approval', 'title' => __('Status Kelulusan'), 'defaultContent' => '-'],
                                ['data' => 'created_at', 'title' => __('table.created_at'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('Bil'),__('Lanjutan Masa Diminta'), __('Lanjutan Masa Diluluskan'), __('Status Kelulusan'),  __('table.created_at'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.extension.show_list.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection