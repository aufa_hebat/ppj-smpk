@if($appointed_company != null && $appointed_company->acquisition->approval->acquisition_type_id == '2')
<h3><b>Butiran Perakuan Kelambatan dan Pelanjutan Masa</b></h3>
@else
<h3><b>Butiran Pelanjutan Tempoh Kontrak</b></h3>
@endif
<div class="row">
    <div class="col-3">
        <div class="text-muted">Permohonan Tempoh EOT</div>
        <div>{{ isset($eot->eot_approve->period) ? $eot->eot_approve->period.' '.$eot->eot_approve->period_type->name : $eot->period.' '.$eot->period_type->name }}</div>
    </div>
    <div class="col-3">
        <div class="text-muted">Tarikh Siap Asal</div>
        <div>{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->original_end_date)->format('d/m/Y') }}</div>
    </div>
    <div class="col-3">
        <div class="text-muted">Tarikh Siap Lanjutan</div>
        <div>{{ isset($eot->eot_approve->approved_end_date) ? \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->eot_approve->approved_end_date)->format('d/m/Y') : \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->extended_end_date)->format('d/m/Y') }}</div>
    </div>
    @if($appointed_company != null && $appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-3">
            <div class="text-muted">Bahagian</div>
            @if($eot->section == 1)
                <div>Keseluruhan</div>
            @else
                <div>Sebahagian</div>
            @endif
        </div>
    @endif
</div><br>

{{-- table --}}
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="table-responsive">
                <div id="tableJKPembuka" class="table-editable table-bordered ">
                    <table class="table">
                        <tr>
                            <th width="60%">Sebab Sebab</th>
                            <th width="20%">Klausa</th>
                            <th width="20%">Tempoh Kelambatan Dan Lanjutan Masa</th>
                        </tr>
                    @foreach($eot->eot_details as $details)
                            <tr>
                                <td>
                                    <div>{{ $details->reasons }}</div>
                                </td>
                                <td>
                                    <div>{{ $details->clause }}</div>
                                </td>
                                <td>
                                    <div>{{ $details->periods.' '.$details->period_type->name }}</div>
                                </td>
                            </tr>
                        @endforeach
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4') --}}
    <div id="uploads_sokongan">
        <hr>
        <h4>Dokumen Sokongan</h4>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="tblUpload_sokongan" class="table table-sm table-transparent">
                        <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th >Nama Dokumen</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div id="uploads_laporan">
        <hr>
        <h4>Laporan Eksekutif</h4>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="tblUpload_laporan" class="table table-sm table-transparent">
                        <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th >Nama Dokumen</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div id="uploads_kertas">
        <hr>
        <h4>Kertas Pertimbangan Presiden (Lanjutan)</h4>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="tblUpload_kertas" class="table table-sm table-transparent">
                        <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th >Nama Dokumen</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
{{-- @endif --}}