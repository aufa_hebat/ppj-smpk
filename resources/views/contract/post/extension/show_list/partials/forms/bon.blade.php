<h3><b>Maklumat Bon Pelaksanaan</b></h3>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Status Bon</div>
        @if(!empty($eot->eot_bon) && $eot->eot_bon->bon_old_new == 1)
          <div>Lama</div>
        @else
          <div>Baru</div>
        @endif
    </div>
    <div class="col-6">
      <div class="text-muted">Jenis Bon Pelaksanaan</div>
      <div>@if(!empty($bon) && !empty($bon->type)){{$bon->type->description}}@endif</div>
    </div>
</div><br>
@if(!empty($bon) && $bon->bon_type == 1)
  <div id="jaminanBank">
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Nama Pengeluar Bank</div>
          <div>{{ $bon->bank->name }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Terima Bon Pelaksanaan</div>
          <div>{{ $bon->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->bank_received_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">No Jaminan Bank</div>
          <div>{{ $bon->bank_no }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Nilai Jaminan Bank (RM)</div>
          <div>{{ money()->toHuman($sst->bon_amount) }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Bermula Jaminan Bank</div>
          <div>{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Luput Jaminan Bank</div>
          <div>{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Akhir Mengemukakan Jaminan Bank</div>
          <div>{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Mengemukakan Surat Pengesahan Jaminan Bank</div>
          <div>{{ $bon->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->bank_approval_proposed_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Penerimaan Surat Pengesahan Jaminan Dari Bank</div>
          <div>{{ $bon->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->bank_approval_received_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
  </div>
@elseif(!empty($bon) && $bon->bon_type == 2)
  <div id="jaminanInsuran">
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Nama Pengeluar Insuran</div>
          <div>{{ $bon->insurance->name }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Terima Insuran</div>
          <div>{{ $bon->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->insurance_received_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">No Jaminan Insuran</div>
          <div>{{ $bon->insurance_no }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Nilai Jaminan Insuran (RM)</div>
          <div>{{ money()->toHuman($sst->bon_amount) }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Bermula Jaminan Insuran</div>
          <div>{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Luput Jaminan Insuran</div>
          <div>{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Akhir Mengemukakan Jaminan Insuran</div>
          <div>{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Mengemukakan Surat Pengesahan Jaminan Insuran</div>
          <div>{{ $bon->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->insurance_approval_proposed_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Penerimaan Surat Pengesahan Jaminan Dari Insuran</div>
          <div>{{ $bon->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->insurance_approval_received_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
  </div>
@elseif(!empty($bon) && $bon->bon_type == 3)
  <div id="cekBank">
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Nama Pengeluar Bank</div>
          <div>{{ $bon->cheque_bank->name }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">No Cek</div>
          <div>{{ $bon->cheque_no }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">No Resit Dari Bahagian Pengurusan Kewangan (BPK)</div>
          <div>{{ $bon->cheque_resit_no }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Tarikh Hantar Cek Ke Bahagian Pengurusan Kewangan (BPK)</div>
          <div>{{ $bon->cheque_submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->cheque_submitted_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Terima Resit Dari Bahagian Pengurusan Kewangan</div>
          <div>{{ $bon->cheque_received_date? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->cheque_received_date)->format('d/m/Y'):null }}</div>
        </div>
      </div>
  </div>
@elseif(!empty($bon) && $bon->bon_type == 4)
  <div id="wangJaminan">
      <div class="row">
        <div class="col-6">
          <div class="text-muted">Tarikh Terima Surat Permohonan WJP</div>
          <div>{{ $bon->money_received_at? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$bon->money_received_at)->format('d/m/Y'):null }}</div>
        </div>
        <div class="col-6">
          <div class="text-muted">Nilai Wang Jaminan Pelaksanaan</div>
          <div>{{ money()->toHuman($bon->money_amount) }}</div>
        </div>
      </div>
  </div>
@endif
