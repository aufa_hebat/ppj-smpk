<h3><b>Maklumat Insurans Tanggungan Awam</b></h3>
<div class="row">
    <div class="col-12">
      <div class="text-muted">Status Insurans</div>
        @if(!empty($insurance) && $insurance->insurance_old_new == 1)
          <div>Lama</div>
        @elseif(!empty($insurance) && $insurance->insurance_old_new == 2)
          <div>Baru</div>
        @endif
    </div>
</div><br>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Nama Pengeluar Insuran</div>
        <div>@if(!empty($insurance) && !empty($insurance->public_insurance)){{ $insurance->public_insurance->name }}@endif</div>
    </div>
    <div class="col-6">
      <div class="text-muted">No. Polisi</div>
        <div>@if(!empty($insurance)){{ $insurance->public_policy_no }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Nilai Polisi (RM)</div>
        <div>{{ money()->toHuman($sst->ins_pli_amount) }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Terima Insuran</div>
        <div>@if(!empty($insurance) && !empty($insurance->public_received_date)){{ $insurance->public_received_date->format('d/m/Y') }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Mula Sah Laku Polisi</div>
        <div>{{ $sst->ins_pli_start_date->format('d/m/Y') }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Luput Polisi</div>
        <div>{{ $sst->ins_pli_end_date->format('d/m/Y') }}</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Akhir Mengemukakan Polisi</div>
        <div>{{ $proposed_date->format('d/m/Y') }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Mengemukakan Pengesahan Polisi</div>
        <div>@if(!empty($insurance) && !empty($insurance->public_approval_proposed_date)){{ $insurance->public_approval_proposed_date->format('d/m/Y') }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Penerimaan Pengesahan Polisi</div>
        <div>@if(!empty($insurance) && !empty($insurance->public_approval_received_date)){{ $insurance->public_approval_received_date->format('d/m/Y') }}@endif</div>
    </div>
</div><br><hr>

<h3><b>Maklumat Insurans Untuk Kerja</b></h3>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Nama Pengeluar Insuran</div>
        <div>@if(!empty($insurance) && !empty($insurance->work_insurance)){{ $insurance->work_insurance->name }}@endif</div>
    </div>
    <div class="col-6">
      <div class="text-muted">No. Polisi</div>
        <div>@if(!empty($insurance)){{ $insurance->work_policy_no }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Nilai Polisi (RM)</div>
        <div>{{ money()->toHuman($sst->ins_work_amount) }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Terima Insuran</div>
        <div>@if(!empty($insurance) && !empty($insurance->work_received_date)){{ $insurance->work_received_date->format('d/m/Y') }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Mula Sah Laku Polisi</div>
        <div>{{ $sst->ins_work_start_date->format('d/m/Y') }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Luput Polisi</div>
        <div>{{ $sst->ins_work_end_date->format('d/m/Y') }}</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Akhir Mengemukakan Polisi</div>
        <div>{{ $proposed_date->format('d/m/Y') }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Mengemukakan Pengesahan Polisi</div>
        <div>@if(!empty($insurance) && !empty($insurance->work_approval_proposed_date)){{ $insurance->work_approval_proposed_date->format('d/m/Y') }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Penerimaan Pengesahan Polisi</div>
        <div>@if(!empty($insurance) && !empty($insurance->work_approval_received_date)){{ $insurance->work_approval_received_date->format('d/m/Y') }}@endif</div>
    </div>
</div><br><hr>

<h3><b>Maklumat Insurans Pampasan Kerja</b></h3>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Nama Pengeluar Insuran</div>
        <div>@if(!empty($insurance) && !empty($insurance->compensation_insurance)){{ $insurance->compensation_insurance->name }}@endif</div>
    </div>
    <div class="col-6">
      <div class="text-muted">No. Polisi</div>
        <div>@if(!empty($insurance)){{ $insurance->compensation_policy_no }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Nilai Polisi (RM)</div>
        <div>{{ money()->toHuman($sst->ins_ci_amount) }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Terima Insuran</div>
        <div>@if(!empty($insurance)&& !empty($insurance->compensation_insurance)){{ $insurance->compensation_received_date->format('d/m/Y') }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Mula Sah Laku Polisi</div>
        <div>{{ $sst->ins_ci_start_date->format('d/m/Y') }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Luput Polisi</div>
        <div>{{ $sst->ins_ci_end_date->format('d/m/Y') }}</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Akhir Mengemukakan Polisi</div>
        <div>{{ $proposed_date->format('d/m/Y') }}</div>
    </div>
    <div class="col-6">
      <div class="text-muted">Tarikh Mengemukakan Pengesahan Polisi</div>
        <div>@if(!empty($insurance) && !empty($insurance->compensation_approval_proposed_date)){{ $insurance->compensation_approval_proposed_date->format('d/m/Y') }}@endif</div>
    </div>
</div>
<div class="row">
    <div class="col-6">
      <div class="text-muted">Tarikh Penerimaan Pengesahan Polisi</div>
        <div>@if(!empty($insurance) && !empty($insurance->compensation_approval_received_date)){{ $insurance->compensation_approval_received_date->format('d/m/Y') }}@endif</div>
    </div>
</div><br>
