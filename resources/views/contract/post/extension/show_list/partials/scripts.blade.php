@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('extension.extension_show.show', id));
		});
		$(document).on('click', '.print-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: 'PERINGATAN',
			  text: 'Sila cetak menggunakan kertas berwarna KUNING',
			  type: 'info'
			}).then((result) => {
				// redirect(route('extension_report', {hashslug:id}));
				window.open(route('extension_report_work', {hashslug:id}), '_blank');
			});
		});
		$(document).on('click', '.print-letter-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: 'PERINGATAN',
			  text: 'Sila cetak menggunakan kertas berwarna KUNING',
			  type: 'info'
			}).then((result) => {
				window.open(route('extension_letter_report_work', {hashslug:id}), '_blank');
			});
		});
	});
</script>
@endpush