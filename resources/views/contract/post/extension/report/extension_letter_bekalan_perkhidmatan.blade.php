<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 70px 50px 70px 100px;
            }

            header {
                position: fixed;
                top: -50px;
                left: 0px;
                right: 0px;
                height: 180px;

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                /* line-height: 35px; */
            }

            footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 30px; 

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;
                
                
            }

            footer .pagenum:before {
                content: counter(page);
            }

            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            
            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            
            .title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

        </style>
    </head>
    <body>
        <table  style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
<!--                <tr class="title">
                <td colspan="3" ><b>PERBADANAN PUTRAJAYA</b><br>
                    Kompleks Perbadanan Putrajaya,<br>
                    24, Persiaran Perdana,<br>
                    Presint 3,<br>
                    62675 Putrajaya<br>
                    MALAYSIA
                </td>
            </tr>-->
            <tr><td colspan="3"><br/><br/></td></tr>
            <tr class="content">
                <td width="10%"><br><br><br><br><br></td>
                {{--  <td width="15%"><br><br><br><br><br>Ruj. Tuan </td>  --}}
                {{--  <td width="5%"><br><br><br><br><br> : </td>  --}}
                <td width="80%" colspan="2"><br><br><br><br><br><br>{{$eot->appointed_company->acquisition->reference}}</td>
            </tr>
            <tr><td colspan="3"><br/><br/><br/></td></tr>
            {{--  <tr class="content">
                <td>Ruj. Kami</td>
                <td> : </td>
                <td></td>
            </tr>  --}}
            {{--  <tr><td colspan="3"><br/></td></tr>  --}}
            <tr class="content">
                {{--  <td>Tarikh </td>  --}}
                {{--  <td> : </td>  --}}
                <td></td>
                <td colspan="2" style="valign:top">{{ \Carbon::intlFormat(now()) }}</td>
            </tr>  
            <tr><td colspan="3"><br/></td></tr>      
        </table>
        <br><br>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td>
                    <b>{{$eot->appointed_company->company->company_name}}</b><br>
                    {{ $eot->appointed_company->company->addresses[0]->primary }}<br>
                    {{ $eot->appointed_company->company->addresses[0]->secondary }}<br>
                    {{ $eot->appointed_company->company->addresses[0]->postcode }} 
                    {{ $eot->appointed_company->company->addresses[0]->state }}<br>
                    (u.p : <b>{{$eot->appointed_company->company->owners->first()->name}}</b>)
                </td>
                <td align="right">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Tel : {{ $eot->appointed_company->company->phones[0]->phone_number }}<br>
                    Faks : {{ $eot->appointed_company->company->phones[2]->phone_number }}
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td>
                    Tuan,<br><br>
                    <b>SURAT TAWARAN PELANJUTAN KONTRAK</b><br>
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <th width="14%" valign="top">PROJEK</th>
                <th width="4%" valign="top">:</th>
                <th width="72%" style="text-transform: uppercase;">
                    {{$eot->appointed_company->acquisition->title}} BAGI TEMPOH 
                    @if(!empty($eot->eot_approve)) 
                        @php    
                            $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                            $tempoh = $eot->eot_approve->period;
                            echo $f->format($tempoh);
                        @endphp 
                        {{ '('.$eot->eot_approve->period.') '.$eot->eot_approve->period_type->name }}
                    @endif
                </th>
            </tr>
            <tr>
                <th width="14%" valign="top">NO. KONTRAK</th>
                <th width="4%" valign="top">:</th>
                <th width="72%"><b>{{$eot->appointed_company->acquisition->reference}}</b></th>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td><b><hr class="style1"></b></td>
            </tr>
            <tr>
                <td>Dengan segala hormatnya saya merujuk kepada perkara di atas.</td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    2. &nbsp;&nbsp;&nbsp; Sukacita dimaklumkan bahawa Perbadanan Putrajaya (”Perbadanan”) dengan ini bersetuju menawarkan pelanjutan tempoh kontrak bagi <b style="text-transform: capitalize;">Pelanjutan Tempoh Kontrak {{$eot->appointed_company->acquisition->title}} BAGI TEMPOH 
                    @if(!empty($eot->eot_approve))
                        @php    
                            $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                            $tempoh = $eot->eot_approve->period;
                            echo $f->format($tempoh);
                        @endphp 
                        {{ '('.$eot->eot_approve->period.') '.$eot->eot_approve->period_type->name }}
                    @endif.</b>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    3. &nbsp;&nbsp;&nbsp; Dengan penerimaan tawaran ini, suatu ikatan kontrak telah wujud di antara Perbadanan dengan pihak tuan. Pihak tuan dikehendaki dalam tempoh empat (4) bulan dari tarikh Surat Tawaran Pelanjutan Kontrak ini memeterai suatu perjanjian tambahan yang mengandungi terma-terma kontrak seperti berikut: 
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
        </table>
        <table border="1" style="width: 100%;border-collapse: collapse; margin-left: 23px">
            <tr>
                <th width="5%">Bil</th>
                <th width="30%">Perkara</th>
                <th>Butiran</th>
            </tr>
            <tr>
                <th valign="top">i</th>
                <th valign="top">Harga Kontrak</th>
                <th style="text-transform: capitalize;">
                    RM {{ money()->toCommon($eot->appointed_company->offered_price ?? "0" , 2) }}<br><br>
                    ( Ringgit Malaysia : 
                        @php    
                            $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                            $amaun = money()->toCommon($eot->appointed_company->offered_price ?? "0" , 2);
                            $format = explode('.',$amaun);
                            $formats = substr($amaun,-2);
                            $partringgit = str_replace(',', '', $format[0]);
                            $partsen = $format[1];
                            $partkosongsen = (int)$formats;
        
                            if($partkosongsen == '.00'){
                                echo $f->format($partringgit) . " Sahaja";
                            }
                            else{
                                echo $f->format($partringgit) . " DAN " . $f->format($partkosongsen) . " SEN Sahaja";
                            }
                        @endphp 
                    )
                </th>
            </tr>
            <tr>
                <th valign="top">ii</th>
                <th valign="top">Tempoh Kerja</th>
                <th style="text-transform: capitalize;">
                    @if(!empty($eot->eot_approve))
                        {{ $eot->eot_approve->period.' '.$eot->eot_approve->period_type->name }}
                    @endif
                </th>
            </tr>
            <tr>
                <th valign="top">iii</th>
                <th valign="top">Tarikh Mula & Siap Kerja</th>
                <th style="text-transform: capitalize;">
                    {{ \Carbon::intlFormat($eot->sst->start_working_date) }} hingga {{(!empty($eot->eot_approve->approved_end_date))? \Carbon::intlFormat($eot->eot_approve->approved_end_date) : \Carbon::intlFormat($eot->extended_end_date)}}
                </th>
            </tr>
            <tr>
                <th valign="top">iv</th>
                <th valign="top">Wakil Perbadanan</th>
                <th style="text-transform: capitalize;">Naib Presiden (Kewangan)</th>
            </tr>
            <tr>
                <th valign="top">v</th>
                <th valign="top">Lokasi</th>
                <th style="text-transform: capitalize;">
                    @foreach($eot->appointed_company->acquisition->approval->locations as $location)
                        {{$location->name}}<br>
                    @endforeach
                </th>
            </tr>
        </table><br>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td>
                    4. &nbsp;&nbsp;&nbsp; Terma-terma dan syarat-syarat lain adalah sebagaimana termaktub dalam perjanjian asal. Sehingga perjanjian tambahan tersebut disempurnakan, Surat Tawaran Pelanjutan Kontrak ini dan perjanjian asal bertarikh <b>{{(!empty($eot->sst->document2))? \Carbon::intlFormat($eot->sst->document2->revenue_stamp_date) : null}}</b><sup>1</sup> hendaklah menjadi dokumen kontrak yang mengikat antara tuan dengan Perbadanan secara sah. 
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    5. &nbsp;&nbsp;&nbsp; Sebagai syarat-syarat sebelum memulakan kerja-kerja, tuan adalah dikehendaki menyerahkan kepada Wakil Perbadanan perkara-perkara berikut: 
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse; margin-left: 23px;">
            <tr>
                <th colspan="2">(a) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bon Pelaksanaan</th>
            </tr>
            <tr>
                <td width="1%"></td>
                <td>
                    Tempoh sah laku 
                    @if(!empty($eot->sst->bon))
                        @if($eot->sst->bon->bon_type == 1)
                            Jaminan Bank asal (No. Rujukan : <b>{{ $eot->sst->bon->bank_no }}</b>)
                        @elseif($eot->sst->bon->bon_type == 2)
                            Jaminan Insurans asal (No. Rujukan : <b>{{ $eot->sst->bon->insurance_no }}</b>)
                        @elseif($eot->sst->bon->bon_type == 3)
                            Deraf Bank asal (No. Rujukan : <b>{{ $eot->sst->bon->cheque_no }}</b>)
                        @else

                        @endif
                    @endif
                    <sup>2</sup> hendaklah dilanjutkan dan berkuat kuasa sehingga 12 bulan selepas tamat Tempoh Kerja.
                </td>
            </tr>
        </table><br>
        <table style="width: 100%;border-collapse: collapse; margin-left: 23px">
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <th colspan="3">(a) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Insurans</th>
            </tr>
            <tr>
                <td width="6%"></td>
                <td width="6%" valign="top">&nbsp;(i)</td>
                <td>
                    Tempoh sah laku <b>Polisi Insurans Tanggungan Awam</b> dan <b>Polisi Insurans Pampasan Pekerja</b> hendaklah dilanjutkan dan berkuat kuasa sehingga tamat Tempoh Kerja.
                </td>
            </tr>
            <tr>
                <td width="6%"></td>
                <td width="6%" valign="top">&nbsp;(ii)</td>
                <td>
                    Tempoh sah laku <b>Polisi Insurans Untuk Kerja</b> hendaklah dilanjutkan dan berkuat kuasa sehingga tamat Tempoh Kerja.
                </td>
            </tr>
        </table><br>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td colspan="2">
                    6. &nbsp;&nbsp;&nbsp; <b>SILA AMBIL PERHATIAN</b> bahawa apa-apa kegagalan dalam mematuhi syarat di perenggan 4 dan 5 akan menyebabkan kontrak ini boleh dibatalkan dan selepas daripada itu Perbadanan tidak lagi dengan apa-apa cara jua bertanggungjawab kepada pihak tuan. 
                </td>
            </tr>
            <tr>
                <td width="30%">
                    <br><br><b><hr class="style1"></b>
                </td>
            </tr>
            <tr>
                <td style="font-size: 10px">
                    <sup>1</sup>Tarikh perjanjian asal<br>
                    <sup>2</sup>Nombor rujukan bon pelaksanaan
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td>
                    7. &nbsp;&nbsp;&nbsp; Surat Tawaran Pelanjutan Kontrak ini dihantar kepada pihak tuan dalam dua (2) salinan iaitu <b>ASAL</b> dan <b>PENDUA</b>. Sila kembalikan surat <b>ASAL</b> yang telah ditandatangani oleh pihak tuan dan disaksikan dengan sempurna dalam tempoh 14 hari dari tarikh Surat Tawaran Pelanjutan Kontrak ini dan sila simpan surat <b>PENDUA</b>nya.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    Sekian, terima kasih. 
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <th>
                    “BANDAR RAYA BESTARI, KEHIDUPAN BERKUALITI” <br>   
                    “BERKHIDMAT UNTUK NEGARA”
                </th>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    Saya yang menurut perintah,
                </td>
            </tr>
            <tr>
                <td>
                    <br><br><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    ............................................ <br>
                    <b style="text-transform: uppercase;">{{ (!empty($np_dept)) ? $np_dept->name : '-' }}</b><br/>
                    {{ (!empty($np_dept)) ? $np_dept->position->name : '-' }}<br/>
                    {{ (!empty($np_dept)) ? $np_dept->department->name : '-'}}<br/>
                    Perbadanan Putrajaya
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;">
            <tr>
                <td colspan="2">s.k.</td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">1.</td>
                <td>
                    Setiausaha<br>
                    Perbadanan Putrajaya.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">2.</td>
                <td>
                    Naib Presiden (Kewangan)<br>
                    Perbadanan Putrajaya.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">3.</td>
                <td>
                    Naib Presiden (Audit dan Kualiti Asurans)<br>
                    Perbadanan Putrajaya.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">4.</td>
                <td>
                    Pengarah Bahagian Perolehan & Ukur Bahan<br>
                    Perbadanan Putrajaya.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">5.</td>
                <td>
                    Pengarah<br>
                    Bahagian Perolehan Kerajaan<br>
                    Perbendaharaan Malaysia,<br>
                    Kompleks Kementerian Kewangan,<br>
                    No. 5, Persiaran Perdana,<br>
                    Presint 2, Pusat Pentadbiran Kerajaan Persekutuan,<br>
                    62592 Putrajaya.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">6.</td>
                <td>
                    Pengarah<br>
                    Jabatan Pungutan Hasil<br>
                    Ibu Pejabat Lembaga Hasil Dalam Negeri Malaysia<br>
                    Menara Hasil Aras 15<br>
                    Persiaran Rimba Permai<br>
                    Cyber 8, 63000 Cyberjaya<br>
                    Selangor.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td valign="top" width="5%">7.</td>
                <td>
                    Perkeso<br>
                    No. 15, Jalan Hentian 1A<br>
                    Pusat Hentian Kajang<br>
                    Jalan Reko 43000 Kajang<br>
                    (U/P : Unit Penguatkuasaan)
                </td>
            </tr>
        </table><br>

        <div class="breakNow"></div>

        <table  style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
<!--                <tr class="title">
                <td colspan="3" ><b>PERBADANAN PUTRAJAYA</b><br>
                    Kompleks Perbadanan Putrajaya,<br>
                    24, Persiaran Perdana,<br>
                    Presint 3,<br>
                    62675 Putrajaya<br>
                    MALAYSIA
                </td>
            </tr>-->
            <tr><td colspan="3"><br/><br/></td></tr>
            <tr class="content">
                <td width="10%"><br><br><br><br><br></td>
                {{--  <td width="15%"><br><br><br><br><br>Ruj. Tuan </td>  --}}
                {{--  <td width="5%"><br><br><br><br><br> : </td>  --}}
                <td width="80%" colspan="2"><br><br><br><br><br><br>{{$eot->appointed_company->acquisition->reference}}</td>
            </tr>
            <tr><td colspan="3"><br/><br/><br/></td></tr>
            {{--  <tr class="content">
                <td>Ruj. Kami</td>
                <td> : </td>
                <td></td>
            </tr>  --}}
            {{--  <tr><td colspan="3"><br/></td></tr>  --}}
            <tr class="content">
                {{--  <td>Tarikh </td>  --}}
                {{--  <td> : </td>  --}}
                <td></td>
                <td colspan="2" style="valign:top">{{ \Carbon::intlFormat(now()) }}</td>
            </tr>  
            <tr><td colspan="3"><br/></td></tr>      
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td>
                    Presiden <br>
                    <b>PERBADANAN PUTRAJAYA</b> <br>
                    Kompleks Perbadanan Putrajaya <br>
                    24, Persiaran Perdana, Presint 3 <br>
                    62675 Putrajaya. <br>
                    <b>(u.p.: {{ $np_dept->department->name }})</b>
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td>
                    Tuan,<br><br>
                    <b><u>AKUAN PENERIMAAN</u></b><br><br>
                    <b>SURAT TAWARAN PELANJUTAN KONTRAK</b><br>
                </td>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <th width="14%" valign="top">PROJEK</th>
                <th width="4%" valign="top">:</th>
                <th width="72%" style="text-transform: uppercase;">
                    {{$eot->appointed_company->acquisition->title}} BAGI TEMPOH 
                    @if(!empty($eot->eot_approve))
                        @php    
                            $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                            $tempoh = $eot->eot_approve->period;
                            echo $f->format($tempoh);
                        @endphp 
                        {{ '('.$eot->eot_approve->period.') '.$eot->eot_approve->period_type->name }}
                    @endif
                </th>
            </tr>
            <tr>
                <th width="14%" valign="top">NO. KONTRAK</th>
                <th width="4%" valign="top">:</th>
                <th width="72%"><b>{{$eot->appointed_company->acquisition->reference}}</b></th>
            </tr>
        </table>
        <table style="width: 100%;border-collapse: collapse;padding: 4px 4px;" align="center" class="content">
            <tr>
                <td><b><hr class="style1"></b></td>
            </tr>
            <tr>
                <td>Saya/kami yang bertandatangan di bawah ini mengakui penerimaan Surat Tawaran Lanjutan Kontrak bertarikh ......................................</td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    Bersama ini juga dikembalikan salinan asal Surat Tawaran Lanjutan Kontrak yang telah disempurnakan dan ditandatangani dan surat penduanya telah disimpan oleh saya/kami. 
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td style="width: 50%;"><br><br><br><br><br></td>
                <td style="width: 50%;"><br><br><br><br><br></td>
            </tr>
            <tr>
                <td style="width: 50%;">........................................................................................</td>
                <td style="width: 50%;">........................................................................................</td>
            </tr>
            <tr>
                <td style="width: 50%;">(Tandatangan Kontraktor)</td>
                <td style="width: 50%;">(Tandatangan Saksi)</td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    Nama Penuh &nbsp; : ..............................................................
                </td>
                <td style="width: 50%;">
                    Nama Penuh &nbsp; : ..............................................................
                </td>
            </tr>
            <tr>
                <td>
                    No. K/P &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ..............................................................
                </td>
                <td>
                    No. K/P &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ..............................................................
                </td>
            </tr>
            <tr>
                <td>
                    Atas Sifat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ..............................................................
                </td>
                <td>
                    Pekerjaan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : ..............................................................
                </td>
            </tr>
            <tr>
                <td>
                    Yang diberi kuasa dengan sempurnanya untuk menandatangani untuk dan bagi pihak:
                </td>
                <td>
                    Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : .............................................................. 
                    <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    ..............................................................
                    <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    ..............................................................
                    <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

                </td>
            </tr>
            <tr>
                <td>
                    <br><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    ..............................................................<br>
                    (Meterai / Cap Syarikat)
                </td>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    Tarikh  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ..............................................................
                </td>
                <td>
                    Tarikh  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : ..............................................................
                </td>
            </tr>
        </table>
    </body>
</html>