<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 70px 50px 70px 100px;
            }

            header {
                position: fixed;
                top: -50px;
                left: 0px;
                right: 0px;
                height: 180px;

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                /* line-height: 35px; */
            }

            footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 30px; 

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;
                
                
            }

            footer .pagenum:before {
                content: counter(page);
            }

            div.breakNow { page-break-inside:avoid; page-break-after:always; }

            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            
            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            
            .title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

        </style>
    </head>
    <body>
        {{-- cetak semak eot mula --}}
        <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>
        <table style="width: 100%">
            <tr>
                <th style="width: 100%;margin-top: -1000px">
                    <center>
                        <h4>
                            @if($eot->appointed_company->acquisition->approval->acquisition_type_id == '2')
                                SENARAI SEMAK <br> SIJIL KELAMBATAN DAN LANJUTAN MASA
                            @else
                                SENARAI SEMAK <br> SIJIL PELANJUTAN TEMPOH KONTRAK
                            @endif
                        </h4>
                    </center>
                </th>
            </tr>
        </table>

        <table style="width: 100%">
            <tr>
                <td valign="top" style="width: 25%;">TAJUK KONTRAK</td>
                <td valign="top" style="width: 3%;">:</td>
                <td style="text-transform: uppercase;width: 72%;" valign="top"><b>{{$eot->appointed_company->acquisition->title}}</b></td>
            </tr>
            <tr>
                <td valign="top">NO KONTRAK</td>
                <td valign="top">:</td>
                <td valign="top"><b>{{$eot->appointed_company->acquisition->reference}}</b></td>
            </tr>
            <tr>
                <td valign="top">TARIKH SIAP (ASAL)</td>
                <td valign="top">:</td>
                <td valign="top">{{(!empty($eot->original_end_date))? \Carbon::intlFormat($eot->original_end_date):""}}</td>
            </tr>
            <tr>
                <td valign="top">TARIKH SIAP (LANJUTAN)</td>
                <td valign="top">:</td>
                <td valign="top">{{(!empty($eot->eot_approve->approved_end_date))? \Carbon::intlFormat($eot->eot_approve->approved_end_date) : \Carbon::intlFormat($eot->extended_end_date)}}</td>
            </tr>
        </table>
        <br>
        <table style="width: 100%;border-collapse: collapse;" border="1">
            <tr>
                <th style="width: 7%;" align="center">BIL</th>
                <th style="width: 50%;" align="center">PERKARA</th>
                <th style="width: 15%;" align="center">TANDA ( / )</th>
                <th style="width: 28%;" align="center">CATATAN / DOKUMEN RUJUKAN</th>
            </tr>
            <tr>
                <td valign="top" align="center">1.</td>
                <td valign="top">Permohonan bertulis daripada kontraktor</td>
                <td><br><br></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top" align="center">2.</td>
                <td valign="top">Semak klausa yang terlibat</td>
                <td><br><br></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top" align="center">3.</td>
                <td valign="top">Sijil lanjutan masa (Kertas warna kuning)</td>
                <td><br><br></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top" align="center">4.</td>
                <td valign="top">Laporan eksekutif</td>
                <td><br><br></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top" align="center">5.</td>
                <td valign="top">
                    Semak sebab kelewatan dan dokumen sokongan seperti : 
                    <br>
                    <table>
                        <tr>
                            <td valign="top">A.</td>
                            <td valign="top">Rekod cuaca harian di tapak</td>
                        </tr>
                        <tr>
                            <td valign="top">B.</td>
                            <td valign="top">Data cuaca 10 tahun bagi minggu berkenaan daripada stesen meteorologi berdekatan</td>
                        </tr>
                        <tr>
                            <td valign="top">C.</td>
                            <td valign="top">Perkiraan menunjukkan bahawa hujan dalam minggu tersebut adalah luar biasa dibandingkan</td>
                        </tr>
                        <tr>
                            <td valign="top">D.</td>
                            <td valign="top">Bukti daripada rekod tapak menunjukkan kerja yang tidak dapat dibuat itu sensitif kepada hujan dan cuaca buruk</td>
                        </tr>
                        <tr>
                            <td valign="top">E.</td>
                            <td valign="top">kerja tersebut dibuktikan daripada program kerja yang disahkan sebagai terjadual untuk dilaksanakan dalam minggu berkenaan, bukan kerja yang sepatutnya telah dibuat dalam musim kering tetapi terlewat atas sebab kesilapan kontraktor lalu terpaksa dibuat dalam hujan.</td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td valign="top" align="center">6.</td>
                <td valign="top">Surat / memo bertulis</td>
                <td><br><br></td>
                <td></td>
            </tr>
        </table>
        <br>
        <table style="width: 100%">
            <tr>
                <td style="width: 45%;">Nama dan Tandatangan Penyedia Dokumen Sokongan Dan Cop Jawatan</td>
                <td style="width: 10%;"></td>
                <td style="width: 45%;">Nama dan Tandatangan Pengesah dan Cop Jawatan</td>
            </tr>
            <tr>
                <td><br><br><br><br><br>........................................................................................</td>
                <td><br><br><br><br><br></td>
                <td><br><br><br><br><br>........................................................................................</td>
            </tr>
            <tr>
                <td style="width: 50%;">Tarikh : </td>
                <td></td>
                <td style="width: 50%;">Tarikh : </td>
            </tr>
        </table><br>
        {{-- cetak semak eot akhir --}}

        <div class="breakNow"><br></div>
        {{-- <footer>
            <div class="pagenum-container">BPUB/EOT/{{$eot->appointed_company->acquisition->approval->year}}/{{$eot->bil}}</div>
        </footer><br><br> --}}
        {{-- cetak eot mula --}}
        <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>
        <table style="width: 100%">
            <tr>
                <th style="width: 100%">
                    <center>
                        <h4>PERBADANAN PUTRAJAYA</h4>
                        <h4>
                            @if($eot->acquisition->approval->acquisition_type_id == '2')
                                SIJIL KELAMBATAN DAN LANJUTAN MASA NO {{ $eot->bil }}
                            @else
                                SIJIL PELANJUTAN TEMPOH KONTRAK
                            @endif
                        </h4>
                    </center>
                </th>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td style="width: 10%;" valign="top">Rujukan</td>
                <td style="width: 3%;" valign="top">:</td>
                <td style="width: 87%;">{{$eot->appointed_company->acquisition->approval->file_reference}}</td>
            </tr>
            <tr>
                <td style="text-transform: uppercase;" valign="top"></td>
                <td valign="top"></td>
                <td valign="top" style="text-transform: capitalize;">Jabatan {{-- {{Auth::user()->stdCdJabatanPelaksana->CD_DESC}} --}}</td>
            </tr>
            <tr>
                <td style="text-transform: uppercase;" valign="top"></td>
                <td valign="top"></td>
                <td valign="top">Perbadanan Putrajaya,</td>
            </tr>
            <tr>
                <td style="text-transform: uppercase;" valign="top"></td>
                <td valign="top"></td>
                <td valign="top">Kompleks Perbadanan Putrajaya,</td>
            </tr>
            <tr>
                <td style="text-transform: uppercase;" valign="top"></td>
                <td valign="top"></td>
                <td valign="top">24, Persiaran Perdana, Persint 3,</td>
            </tr>
            <tr>
                <td style="text-transform: uppercase;" valign="top"></td>
                <td valign="top"></td>
                <td valign="top">62675 Putrajaya.</td>
            </tr>
            <tr>
                <td valign="top">Tarikh</td>
                <td valign="top">:</td>
                <td valign="top">{{ $eot->created_at->format('d/m/Y') }}</td>
            </tr>
            <tr>
                <td valign="top">Kepada</td>
                <td valign="top">:</td>
                <td valign="top" style="text-transform: uppercase;">{{ $eot->appointed_company->company->company_name }}<br>
                    {{ $eot->appointed_company->company->addresses[0]->primary }}<br>
                    {{ $eot->appointed_company->company->addresses[0]->secondary }}<br>
                    {{ $eot->appointed_company->company->addresses[0]->postcode }} 
                    {{ $eot->appointed_company->company->addresses[0]->state }}<br>
                    Malaysia<br>
                    Berdaftar dengan PKK CIDB 
                    Gred
                    @if(!empty($eot->appointed_company->company->syarikatCIDBgreds)) 
                        @foreach($eot->appointed_company->company->syarikatCIDBgreds as $gred)
                          {{$gred->grades}} 
                        @endforeach 
                    @endif
                    dan Bumiputera
                </td>
            </tr>
            <tr>
                <td valign="top" width="20%">No @if($eot->acquisition->acquisition_category_id == '1' || $eot->acquisition->acquisition_category_id == '2') Sebut Harga @else Kontrak @endif</td>
                <td valign="top">:</td>
                <td valign="top">{{$eot->appointed_company->acquisition->reference}}</td>
            </tr>
            <tr>
                <td valign="top">Kontrak Untuk </td>
                <td valign="top">:</td>
                <td style="text-transform: uppercase;" valign="top">{{$eot->appointed_company->acquisition->title}}</td>
            </tr>
            @if($eot->acquisition->approval->acquisition_type_id == '2')
            <tr>
                <td valign="top">Bahagian<br><br></td>
                <td valign="top">:</td>
                <td style="text-transform: uppercase;" valign="top">
                    @if($eot->section == 1)
                        Keseluruhan
                    @elseif($eot->section == 2)
                        Sebahagian
                    @endif
                </td>
            </tr>
            @endif
            <tr>
                <td valign="top" colspan="3" style="text-align: justify;">Dengan  ini  saya  memperakui  bahawa  kemajuan dan penyiapan Kerja-Kerja seperti yang tersebut di atas mungkin/ telah terlambat melewati Tarikh Siap Asal seperti yang  dinyatakan dalam Lampiran kepada Syarat-Syarat @if($eot->acquisition->acquisition_category_id == '1' || $eot->acquisition->acquisition_category_id == '2')Sebut Harga @else Kontrak @endif atau melewati Tarikh Lanjut Siap Lanjutan yang telah dibenarkan sebelum ini, iaitu {{(!empty($eot->original_end_date))? \Carbon::intlFormat($eot->original_end_date):""}} dengan sebab-sebab berikut :</td>
            </tr>
        </table>
        <table border="1" style="width: 100%;border-collapse: collapse;">
            <tr>
                <th>Sebab/sebab-sebab</th>
                <th>Klausa</th>
                <th>
                    @if($eot->acquisition->approval->acquisition_type_id == '2')
                        Tempoh Kelambatan Dan Lanjutan Masa
                    @else
                        Tempoh Pelanjutan Kontrak
                    @endif
                </th>
            </tr>
            @foreach($eot->eot_details as $detail)
            <tr>
                <td>{{ $detail->reasons }}</td>
                <td>{{ $detail->clause }}</td>
                <td>{{ $detail->periods.' '.$detail->period_type->name }}</td>
            </tr>
            @endforeach
        </table><br>
        <table style="width: 100%">
            <tr>
                <td colspan="2">2.&nbsp;&nbsp;&nbsp;Menurut Klausa @if(!empty($eot->eot_details->first())){{ $eot->eot_details->first()->clause }}@endif Syarat-Syarat @if($eot->acquisition->acquisition_category_id == '1' || $eot->acquisition->acquisition_category_id == '2')Sebut Harga @else Kontrak @endif, saya dengan ini membenarkan lanjutan masa selama {{ isset($eot->eot_approve->period) ? $eot->eot_approve->period.' '.$eot->eot_approve->period_type->name : $eot->period.' '.$eot->period_type->name  }} untuk menyiapkan kerja-kerja di bawah kontrak ini.</td>
            </tr>
            <tr>
                <td colspan="2">3.&nbsp;&nbsp;&nbsp;Berikutan dengan itu Tarikh Siap yang telah ditetapkan 
                    @if($eot->acquisition->approval->acquisition_type_id == '2')
                    setelah EOT {{ $eot->bil }} 
                    @endif
                    pada {{(!empty($eot->original_end_date))? \Carbon::intlFormat($eot->original_end_date):""}} sekarang ini dilanjutkan kepada <b>{{(!empty($eot->eot_approve->approved_end_date))? \Carbon::intlFormat($eot->eot_approve->approved_end_date) : \Carbon::intlFormat($eot->extended_end_date)}}</b>.</td>
            </tr>
            <tr>
                <td style="width: 50%;"><br><br><br><br><br></td>
                <td style="width: 50%;"><br><br><br><br><br></td>
            </tr>
            <tr>
                <td style="width: 50%;">........................................................................................</td>
                <td style="width: 50%;">........................................................................................</td>
            </tr>
            <tr>
                <td style="width: 50%;">Tandatangan Pegawai Perbadanan Putrajaya</td>
                <td style="width: 50%;">Tandatangan Wakil Perbadanan yang diberi kuasa bertindak untuk dan bagi pihak Perbadanan Putrajaya</td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    Nama : <a style="text-transform: uppercase;">{{ $eot->appointed_company->acquisition->reviews->where('status','S2')->first()->approve->name ?? '' }}</a><br/>
                    Jawatan : <a style="text-transform: uppercase;">{{ $eot->appointed_company->acquisition->reviews->where('status','S2')->first()->approve->position->name ?? '' }}</a><br/>
                    Jabatan : {{ $eot->appointed_company->acquisition->reviews->where('status','S2')->first()->approve->department->name ?? '' }}<br/>
                    Tarikh : 
                </td>
                <td style="width: 50%;">
                    Nama : <a style="text-transform: uppercase;">{{ (!empty($np_dept)) ? $np_dept->name : '-'}}</a><br/>
                    Jawatan : <a style="text-transform: uppercase;">{{ (!empty($np_dept)) ? $np_dept->position->name : '-'}}</a><br/>
                    Jabatan : {{ (!empty($np_dept)) ? $np_dept->department->name : '-' }}<br/>
                    Tarikh : 
                </td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr><td>S.K.</td></tr>
            <tr>
                <td style="width: 2%;">1. </td>
                <td style="width: 98%;">
                    Jabatan Audit dan Kualiti Asurans <br>
                </td>
            </tr>
            <tr>
                <td style="width: 2%;">2. </td>
                <td style="width: 98%;">
                    Insurans <br>
                </td>
            </tr>
        </table>
        {{-- cetak eot akhir --}}
    </body>
</html>