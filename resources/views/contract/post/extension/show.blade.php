@extends('layouts.admin')

@push('style')
  #datetimepicker-get_bon_date{
    z-index:100;
  }
  #datetimepicker-verify_bon_date{
    z-index:100;
  }
  #datetimepicker-approved_date{
    z-index:100;
  }
  #datetimepicker-final_verify_bon_date{
    z-index:100;
  }
  #datetimepicker-get_approved_bon_date{
    z-index:100;
  }

  #datetimepicker-get_bon_date1{
    z-index:100;
  }
  #datetimepicker-verify_bon_date1{
    z-index:100;
  }
  #datetimepicker-approved_date1{
    z-index:100;
  }
  #datetimepicker-final_verify_bon_date1{
    z-index:100;
  }
  #datetimepicker-get_approved_bon_date1{
    z-index:100;
  }

  #datetimepicker-get_insurans_date{
    z-index:100;
  }
  #datetimepicker-verify_insurans_date{
    z-index:100;
  }
  #datetimepicker-approved_date2{
    z-index:100;
  }
  #datetimepicker-final_verify_insurans_date{
    z-index:100;
  }
  #datetimepicker-get_approved_insurans_date{
    z-index:100;
  }

  #datetimepicker-get_insurans_date1{
    z-index:100;
  }
  #datetimepicker-verify_insurans_date1{
    z-index:100;
  }
  #datetimepicker-approved_date3{
    z-index:100;
  }
  #datetimepicker-final_verify_insurans_date1{
    z-index:100;
  }
  #datetimepicker-get_approved_insurans_date1{
    z-index:100;
  }
@endpush

@push('scripts')
  <script src="{{ asset('js/datetimepicker.js') }}"></script>
	<script>
        $(".addmore").on('click',function(){
            var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
            if($tr.prop("id") === undefined){
                $tr = $("table.d-none");
            }
            var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
            var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
            var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
//              $klon.find('#bil_'+currnum).attr('id','bil_'+num).attr('name','baru['+num+'][bil]').val('');
              $klon.find('#appointment_date_'+currnum).attr('id','appointment_date_'+num).attr('name','baru['+num+'][appointment_date]').val('');
              $klon.find('#name_'+currnum).attr('id','name_'+num).attr('name','baru['+num+'][name]').val('');
            $('.addmore').parents('table').append($klon);
        });
        $('.buang').click(function () {
            $(this).parents('tr').detach();
        });
        jQuery(document).ready(function($) {
//            alert("hey");
        });
    </script>
    <script type="text/javascript">
    	function getKategori1(obj) {
            var bonbaru = document.getElementById('bonbaru');
            var bonlama = document.getElementById('bonlama');

            if(obj.value == '1'){
                bonbaru.hidden = false;
                bonlama.hidden = true;
            }
            else if(obj.value == '2'){
                bonbaru.hidden = true;
                bonlama.hidden = false;
            }
        }

        function getKategori2(obj) {
            var insuransbaru = document.getElementById('insuransbaru');
            var insuranslama = document.getElementById('insuranslama');

            if(obj.value == '1'){
                insuransbaru.hidden = false;
                insuranslama.hidden = true;
            }
            else if(obj.value == '2'){
                insuransbaru.hidden = true;
                insuranslama.hidden = false;
            }
        }
    </script>
@endpush

@section('content')
@include('components.forms.assets.datetimepicker')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.extension.partials.scripts')
            @component('components.card')
	            @slot('card_title')
	                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalEOT">
	                    @icon('fe fe-plus') {{ __('Maklumat Pelanjutan Baru') }}
	                </button>
	            @endslot
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'contract-post-extension-management',
                            'route_name' => 'api.datatable.contract.extension',
                            'columns' => [
                                ['data' => 'reference', 'title' => __('No. Rujukan'), 'defaultContent' => '-'],
                                ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                ['data' => 'created_at', 'title' => __('table.created_at'), 'defaultContent' => '-'],
                                ['data' => 'updated_at', 'title' => __('table.updated_at'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Rujukan'), __('Tajuk'), __('table.created_at'), __('table.updated_at'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.extension.partials.actions1')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>

<div class="modal fade" id="modalEOT" role="dialog" aria-labelledby="modalEOTModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalEOTModalLongTitle">Tambah Maklumat Pelanjutan Masa (EOT)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-2">
      			@include('components.forms.input', [
		            'input_label' => 'Bil EOT',
		            'name' => '',
		            'id' => '',
		            'value' => '1',
		            'readonly' => 'readonly'
		        ])
      		</div>
      		<div class="col-4">
      			@include('components.forms.input', [
		            'input_label' => 'Permohonan Tempoh Lanjutan Masa',
		            'name' => '',
		            'id' => ''
		        ])
      		</div>
      		<div class="col-2">
      			@include('components.forms.select', [
                    'input_label' => __('Jenis Tempoh'),
                    'options' => period_types()->pluck('name', 'id'),
                    'id' => 'period_type_id',
                    'name' => 'period_type_id',
                ])
      		</div>
      		<div class="col-4">
      			@include('components.forms.datetimepicker', [
				    'input_label' => __('Tarikh Siap Asal'),
				    'id' => '',
				    'name' => '',
		            'config' => [
		                'format' => config('datetime.display.date'),
		            ]
				])
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-6">
      			@include('components.forms.input', [
		            'input_label' => 'Tarikh Siap Lanjutan',
		            'name' => '',
		            'id' => '',
		            'readonly' => 'readonly'
		        ])
      		</div>
      		<div class="col-6">
                <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Bahagian</label><br>
                <div class="selectgroup w-25 grade-container">
                  	<label class="selectgroup-item">
                	<input type="radio" id="category1" name="category" value="1" class="selectgroup-input">
                        <span class="selectgroup-button">{{ __('Keseluruhan') }}</span>
                  	</label>
                  	<label class="selectgroup-item">
                    <input type="radio" id="category2" name="category" value="2" class="selectgroup-input">
                        <span class="selectgroup-button">{{ __('Sebahagian') }}</span>
                  	</label>
                </div>
            </div>
      	</div>

      	{{-- table --}}

      	<div class="col-md-12">
            <div class="form-group">
                <div class="table-responsive">
                    <div id="tableJKPembuka" class="table-editable table-bordered ">
                        <table class="table">
                            <tr>
                                <th width="20%">Sebab Sebab</th>
                                <th width="20%">Klausa</th>
                                <th width="55%">Sebab Sebab</th>
                                <th width="5%"><b><span class="fe fe-plus addmore"></span></b></th>
                            </tr>
                            <tr class="d-none">
                                <td>
                                    <input type="hidden" name="exist[][id]" value="">
                                </td>
                                <td>
                                    <input type="hidden" name="exist[][id]" value="">
                                </td>
                                <td>
                                    <input type="hidden" name="exist[][id]" value="">
                                </td>
                                <td>
                                    <b><span class="fe fe-minus buang"></span></b></span>
                                </td>
                            </tr>
                            <tr id="klon0" class="d-none">
                                <td>
                                    <input type="text" class="form-control" name="baru[][id]" value="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="baru[][id]" value="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="baru[][id]" value="">
                                </td>
                                <td>
                                    <b><span class="fe fe-minus buang"></b></span>
                                </td>
                            </tr>
                       </table>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">
                    @icon('fe fe-save') {{ __('Simpan') }}
                </button>
            </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalKemaskiniEOT" role="dialog" aria-labelledby="modalKemaskiniEOTModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalKemaskiniEOTModalLongTitle">Kemaskini Maklumat Pelanjutan Masa (EOT)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-2">
      			@include('components.forms.input', [
		            'input_label' => 'Bil EOT',
		            'name' => '',
		            'id' => '',
		            'value' => '1',
		            'readonly' => 'readonly'
		        ])
      		</div>
      		<div class="col-4">
      			@include('components.forms.input', [
		            'input_label' => 'Permohonan Tempoh Lanjutan Masa',
		            'name' => '',
		            'id' => ''
		        ])
      		</div>
      		<div class="col-2">
      			@include('components.forms.select', [
                    'input_label' => __('Jenis Tempoh'),
                    'options' => period_types()->pluck('name', 'id'),
                    'id' => 'period_type_id',
                    'name' => 'period_type_id',
                ])
      		</div>
      		<div class="col-4">
      			@include('components.forms.datetimepicker', [
				    'input_label' => __('Tarikh Siap Asal'),
				    'id' => '',
				    'name' => '',
		            'config' => [
		                'format' => config('datetime.display.date'),
		            ]
				])
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-6">
      			@include('components.forms.input', [
		            'input_label' => 'Tarikh Siap Lanjutan',
		            'name' => '',
		            'id' => '',
		            'readonly' => 'readonly'
		        ])
      		</div>
      		<div class="col-6">
                <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Bahagian</label><br>
                <div class="selectgroup w-25 grade-container">
                  	<label class="selectgroup-item">
                	<input type="radio" id="category1" name="category" value="1" class="selectgroup-input">
                        <span class="selectgroup-button">{{ __('Keseluruhan') }}</span>
                  	</label>
                  	<label class="selectgroup-item">
                    <input type="radio" id="category2" name="category" value="2" class="selectgroup-input">
                        <span class="selectgroup-button">{{ __('Sebahagian') }}</span>
                  	</label>
                </div>
            </div>
      	</div>

      	{{-- table --}}

      	<div class="col-md-12">
            <div class="form-group">
                <div class="table-responsive">
                    <div id="tableJKPembuka" class="table-editable table-bordered ">
                        <table class="table">
                            <tr>
                                <th width="20%">Sebab Sebab</th>
                                <th width="20%">Klausa</th>
                                <th width="55%">Sebab Sebab</th>
                                <th width="5%"><b><span class="fe fe-plus addmore"></span></b></th>
                            </tr>
                            <tr class="d-none">
                                <td>
                                    <input type="hidden" name="exist[][id]" value="">
                                </td>
                                <td>
                                    <input type="hidden" name="exist[][id]" value="">
                                </td>
                                <td>
                                    <input type="hidden" name="exist[][id]" value="">
                                </td>
                                <td>
                                    <b><span class="fe fe-minus buang"></span></b></span>
                                </td>
                            </tr>
                            <tr id="klon0" class="d-none">
                                <td>
                                    <input type="text" class="form-control" name="baru[][id]" value="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="baru[][id]" value="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="baru[][id]" value="">
                                </td>
                                <td>
                                    <b><span class="fe fe-minus buang"></b></span>
                                </td>
                            </tr>
                       </table>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">
                    @icon('fe fe-save') {{ __('Simpan') }}
                </button>
            </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalTambahBnI" role="dialog" aria-labelledby="modalTambahBnIModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahBnIModalLongTitle">Tambah Maklumat Bon & Insurans (Lanjutan)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs nav-justified" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#bon">
                @icon('fe fe-file-text')&nbsp;{{ __('Maklumat Bon') }}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#insurans">
                @icon('fe fe-file-text')&nbsp;{{ __('Maklumat Insurans') }}
              </a>
            </li>
          </ul>

          <!-- Tab panes -->
          @component('components.card', ['card_classes' => 'col-12'])
            @slot('card_body')
              <div class="tab-content" id="certificate-tab-content">
                <div class="tab-pane fade show active" id="bon" role="tabpanel" aria-labelledby="bon-tab">
                  <form id="detail-form">
                    <div class="col-3">
                      <div class="row" >
                        <div class="col-sm-12">
                          <label class="control-label" style="font-size: 15px" >Status Bon</label><br>
                          <div class="selectgroup w-25 grade-container">
                            <label class="selectgroup-item">
                              <input type="radio" id="category1" name="category" value="1" class="selectgroup-input"  onclick="getKategori1(this)">
                              <span class="selectgroup-button">{{ __('Baru') }}</span>
                            </label>
                            <label class="selectgroup-item">
                              <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori1(this)">
                              <span class="selectgroup-button">{{ __('Lama') }}</span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="bonbaru" hidden>
                      <div class="row">
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Terima Bon pelaksanaan'),
                            'id' => 'get_bon_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.input', [
                            'input_label' => 'Nilai Jaminan (RM)',
                            'name' => 'name'
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Mengemukakan Pengesahan Bon Pelaksanaan'),
                            'id' => 'verify_bon_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.file', [
                            'input_label' => 'Muat Naik Surat Bon Pelaksanaan',
                            'name' => 'name'
                          ])
                        </div>
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Sah Laku (Lanjutan)'),
                            'id' => 'approved_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Akhir Mengemukakan Bon Pelaksanaan'),
                            'id' => 'final_verify_bon_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Penerimaan Pengesahan Bon Pelaksanaan'),
                            'id' => 'get_approved_bon_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])
                        </div>
                      </div>
                    </div>
                    <div id="bonlama" hidden>
                      <div class="row">
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Terima Bon pelaksanaan'),
                            'id' => 'get_bon_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.input', [
                            'input_label' => 'Nilai Jaminan (RM)',
                            'name' => 'name'
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Mengemukakan Pengesahan Bon Pelaksanaan'),
                            'id' => 'verify_bon_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.file', [
                            'input_label' => 'Muat Naik Surat Bon Pelaksanaan',
                            'name' => 'name'
                          ])
                        </div>
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Sah Laku (Lanjutan)'),
                            'id' => 'approved_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Akhir Mengemukakan Bon Pelaksanaan'),
                            'id' => 'final_verify_bon_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Penerimaan Pengesahan Bon Pelaksanaan'),
                            'id' => 'get_approved_bon_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="tab-pane fade" id="insurans" role="tabpanel" aria-labelledby="insurans-tab">
                  <form id="approval-form">
                    <div class="col-3">
                      <div class="row" >
                        <div class="col-sm-12">
                          <label class="control-label" style="font-size: 15px" >Status Insurans</label><br>
                          <div class="selectgroup w-25 grade-container">
                            <label class="selectgroup-item">
                              <input type="radio" id="category1" name="category" value="1" class="selectgroup-input"  onclick="getKategori2(this)">
                              <span class="selectgroup-button">{{ __('Baru') }}</span>
                            </label>
                            <label class="selectgroup-item">
                              <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                              <span class="selectgroup-button">{{ __('Lama') }}</span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="insuransbaru" hidden>
                      <div class="row">
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Terima Insurans pelaksanaan'),
                            'id' => 'get_insurans_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.input', [
                            'input_label' => 'Nilai Jaminan (RM)',
                            'name' => 'name'
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Mengemukakan Pengesahan Insurans Pelaksanaan'),
                            'id' => 'verify_insurans_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.file', [
                            'input_label' => 'Muat Naik Surat Insurans Pelaksanaan',
                            'name' => 'name'
                          ])
                        </div>
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Sah Laku (Lanjutan)'),
                            'id' => 'approved_date2',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Akhir Mengemukakan Insurans Pelaksanaan'),
                            'id' => 'final_verify_insurans_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Penerimaan Pengesahan Insurans Pelaksanaan'),
                            'id' => 'get_approved_insurans_date',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])
                        </div>
                      </div>
                    </div>
                    <div id="insuranslama" hidden>
                      <div class="row">
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Terima Insurans pelaksanaan'),
                            'id' => 'get_insurans_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.input', [
                            'input_label' => 'Nilai Jaminan (RM)',
                            'name' => 'name'
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Mengemukakan Pengesahan Insurans Pelaksanaan'),
                            'id' => 'verify_insurans_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.file', [
                            'input_label' => 'Muat Naik Surat Insurans Pelaksanaan',
                            'name' => 'name'
                          ])
                        </div>
                        <div class="col-6">
                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Sah Laku (Lanjutan)'),
                            'id' => 'approved_date3',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Akhir Mengemukakan Insurans Pelaksanaan'),
                            'id' => 'final_verify_insurans_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])

                          @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Penerimaan Pengesahan Insurans Pelaksanaan'),
                            'id' => 'get_approved_insurans_date1',
                            'name' => '',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                          ])
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            @endslot
          @endcomponent
        </div>
      </div>
    </div>
  </div>
</div>
@endsection