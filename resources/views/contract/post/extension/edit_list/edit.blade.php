@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    @include('components.forms.assets.datetimepicker')
	<script>
        jQuery(document).ready(function($) {

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            @if(!empty($eot) && !empty($eot->documents))
                @foreach($eot->documents as $document)
                    $('#subFile').val('{{ $document->document_name }}');
                @endforeach
            @endif

            $(document).on('click', '.upload-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $eot->id ?? ''}}';
                var route_name = 'api.contract.upload-eot';
                var form_id = 'upload_form';
                var form = document.forms[form_id];
                var data = new FormData(form);

                // Display the key/value pairs
                //for (var pair of data.entries()) {
                //    console.log(pair[0]+ ', ' + pair[1]); 
                //}

                axios.post(route(route_name, id), data).then(response => {
                    swal('', response.data.message, 'success');
                    location.reload();
                });
            });

            $(document).on('click', '.buang-upload', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('api.contract.extension.delete', id))
                        .then(response => {
                            swal('Pelanjutan Masa', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    eot_id : $('#eot_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            //tab remain stay after refresh
            // $('#document-tab-content a').click(function (e) {
            //     e.preventDefault();
            //     $(this).tab('show');
            // });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh

            // document Sokongan upload start

            var tus = $('#tblUploadSokongan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counterSokongan = 1;

            $('#addRowSokongan').on( 'click', function () {
                tus.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFileSokongan'+ counterSokongan +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFileSokongan'+ counterSokongan +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileSokongan" id="uploadFileSokongan'+ counterSokongan +'" name="uploadFileSokongan[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterSokongan +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterSokongan +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterSokongan++;
            } );

            @if(!empty($eot) && ($eot->support_documents->count() > 0))
                @foreach($eot->support_documents as $doc_support)
                    tus.row.add( [
                        '<div class="form-group">\n' +
                        '   <div class="col input-group">\n' +
                        '       <input type="text" id="subFileSokongan'+ counterSokongan +'" class="form-control" value="{{ $doc_support->document_name }}" readonly>\n' +
                        '@if(!isset($eot->eot_approve->id))' +
                        '       <input type="hidden" id="hDocumentIdSokongan'+ counterSokongan +'" name="hDocumentIdSokongan[]" value="{{ $doc_support->id }}">\n' +
                        '       <label class="input-group-text" for="uploadFileSokongan'+ counterSokongan +'"><i class="fe fe-upload" ></i></label>\n' +
                        '       <input type="file" class="form-control uploadFileSokongan" id="uploadFileSokongan'+ counterSokongan +'" name="uploadFileSokongan[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterSokongan +'">\n' +
                        '@endif' +
                        '       <label class="input-group-text" for="uploadFileSokongan'+ counterSokongan +'"><a href="/download/{{$doc_support->document_path}}/{{ $doc_support->document_name }}" target="_blank"><i class="fas fa-download"></i></a></label>\n' +
                        '   </div>\n' +
                        '</div>',
                        '<div class="form-group"><button type="button" id="remove'+ counterSokongan +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                    ] ).draw( false );

                    counterSokongan++;
                @endforeach
            @else
            // Automatically add a first row of data
            $('#addRowSokongan').click();
            @endif

            $("#tblUploadSokongan").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    tus.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUploadSokongan").on('change','.uploadFileSokongan',function(){
                var no = $(this).data('counter');
                $('#subFileSokongan' + no).val($(this).val().split('\\').pop());
            });

            // document Sokongan upload end

            // document Laporan upload start

            var tul = $('#tblUploadLaporan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counterLaporan = 1;

            $('#addRowLaporan').on( 'click', function () {
                tul.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFileLaporan'+ counterLaporan +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFileLaporan'+ counterLaporan +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileLaporan" id="uploadFileLaporan'+ counterLaporan +'" name="uploadFileLaporan[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterLaporan +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterLaporan +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterLaporan++;
            } );

            @if(!empty($eot) && ($eot->executif_documents->count() > 0))
                @foreach($eot->executif_documents as $doc_executif)
                    tul.row.add( [
                        '<div class="form-group">\n' +
                        '   <div class="col input-group">\n' +
                        '       <input type="text" id="subFileLaporan'+ counterLaporan +'" class="form-control" value="{{ $doc_executif->document_name }}" readonly>\n' +
                        '@if(!isset($eot->eot_approve->id))' +
                        '       <input type="hidden" id="hDocumentIdLaporan'+ counterLaporan +'" name="hDocumentIdLaporan[]" value="{{ $doc_executif->id }}">\n' +
                        '       <label class="input-group-text" for="uploadFileLaporan'+ counterLaporan +'"><i class="fe fe-upload" ></i></label>\n' +
                        '       <input type="file" class="form-control uploadFileLaporan" id="uploadFileLaporan'+ counterLaporan +'" name="uploadFileLaporan[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterLaporan +'">\n' +
                        '@endif' +
                        '       <label class="input-group-text" for="uploadFileLaporan'+ counterLaporan +'"><a href="/download/{{$doc_executif->document_path}}/{{ $doc_executif->document_name }}" target="_blank"><i class="fas fa-download"></i></a></label>\n' +
                        '   </div>\n' +
                        '</div>',
                        '<div class="form-group"><button type="button" id="remove'+ counterLaporan +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                    ] ).draw( false );

                    counterLaporan++;
                @endforeach
            @else
            // Automatically add a first row of data
            $('#addRowLaporan').click();
            @endif

            $("#tblUploadLaporan").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    tul.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });


            $("#tblUploadLaporan").on('change','.uploadFileLaporan',function(){
                var no = $(this).data('counter');
                $('#subFileLaporan' + no).val($(this).val().split('\\').pop());
            });

            // document Laporan upload end

            // document Kertas upload start

            var tuk = $('#tblUploadKertas').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counterKertas = 1;

            $('#addRowKertas').on( 'click', function () {
                tuk.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFileKertas'+ counterKertas +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFileKertas'+ counterKertas +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileKertas" id="uploadFileKertas'+ counterKertas +'" name="uploadFileKertas[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterKertas +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterKertas +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterKertas++;
            } );

            @if(!empty($eot) && ($eot->president_documents->count() > 0))
                @foreach($eot->president_documents as $doc_president)
                    tuk.row.add( [
                        '<div class="form-group">\n' +
                        '   <div class="col input-group">\n' +
                        '       <input type="text" id="subFileKertas'+ counterKertas +'" class="form-control" value="{{ $doc_president->document_name }}" readonly>\n' +
                        '@if(!isset($eot->eot_approve->id))' +
                        '       <input type="hidden" id="hDocumentIdKertas'+ counterKertas +'" name="hDocumentIdKertas[]" value="{{ $doc_president->id }}">\n' +
                        '       <label class="input-group-text" for="uploadFileKertas'+ counterKertas +'"><i class="fe fe-upload" ></i></label>\n' +
                        '       <input type="file" class="form-control uploadFileKertas" id="uploadFileKertas'+ counterKertas +'" name="uploadFileKertas[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterKertas +'">\n' +
                        '@endif' +
                        '       <label class="input-group-text" for="uploadFileKertas'+ counterKertas +'"><a href="/download/{{$doc_president->document_path}}/{{ $doc_president->document_name }}" target="_blank"><i class="fas fa-download"></i></a></label>\n' +
                        '   </div>\n' +
                        '</div>',
                        '<div class="form-group"><button type="button" id="remove'+ counterKertas +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                    ] ).draw( false );

                    counterKertas++;
                @endforeach
            @else
            // Automatically add a first row of data
            $('#addRowKertas').click();
            @endif

            $("#tblUploadKertas").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    tuk.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUploadKertas").on('change','.uploadFileKertas',function(){
                var no = $(this).data('counter');
                $('#subFileKertas' + no).val($(this).val().split('\\').pop());
            });

            // document Kertas upload end

            @if(!isset($eot->eot_approve->id))
                $('#eot_approve_id').val('first');

                $('#bil').val('{{ $eot->bil }}') ;
                $('#period').val('{{ $eot->period }}').removeAttr('readonly');
                $("#period_type_id").val('{{ $eot->period_type_id }}').attr("disabled", false);
                $('#original_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->original_end_date)->format('d/m/Y') }}') ;
                $('#extended_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->extended_end_date)->format('d/m/Y') }}') ;
                @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                    $('input:radio[name=section][value={{ $eot->section }}]').prop('checked', true);
                @endif
                $('#eot_id').val('{{ $eot->id }}');

                $('#period_type_id').change(function() {
                    var originalEndDate, endDate;
                    if($("#period").val() != undefined){
                        
                        originalEndDate = moment($("#original_end_date").val(), "DD/MM/YYYY");
                        
                        if($("#period_type_id").val() == 1){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'days');
                        }else if($("#period_type_id").val() == 2){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'weeks');
                        }else if($("#period_type_id").val() == 3){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'months');
                        }else if($("#period_type_id").val() == 4){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'years');
                        }

                        $("#extended_end_date").val(endDate.format("DD/MM/YYYY"));
                    }
                });

                $('#period').keyup(function() {
                    var originalEndDate, endDate;
                    if($("#period_type_id").val() != undefined){
                        
                        originalEndDate = moment($("#original_end_date").val(), "DD/MM/YYYY");
                        
                        if($("#period_type_id").val() == 1){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'days');
                        }else if($("#period_type_id").val() == 2){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'weeks');
                        }else if($("#period_type_id").val() == 3){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'months');
                        }else if($("#period_type_id").val() == 4){
                            endDate = moment((originalEndDate)).add($( "#period" ).val(), 'years');
                        }
                        
                        if(endDate != undefined){
                            $("#extended_end_date").val(endDate.format("DD/MM/YYYY"));
                        }
                    }
                });

                $(".addmore").on('click',function(){
                    var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
                    if($tr.prop("id") === undefined){
                        $tr = $("table.d-none");
                    }
                    var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                    var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                    var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
                      $klon.find('#reason_'+currnum).attr('id','reason_'+num).attr('name','baru['+num+'][reason]').val('');
                      $klon.find('#clauses_'+currnum).attr('id','clauses_'+num).attr('name','baru['+num+'][clauses]').val('');
                      $klon.find('#period_'+currnum).attr('id','period_'+num).attr('name','baru['+num+'][period]').val('');
                      $klon.find('#period_type_'+currnum).attr('id','period_type_'+num).attr('name','baru['+num+'][period_type]').val('');
                    $('.addmore').parents('table').append($klon);
                    console.log(currnum);
                });
                $('.buang').click(function () {
                    $(this).parents('tr').detach();
                });

                $(document).on('click', '.update-action-btn', function(event) {
                    event.preventDefault();
                    var id = '{{ $eot->id }}';
                    var route_name = 'api.contract.extension.update';
                    var form_id = 'extension_form';
                    // var data = $('#' + form_id).serialize();
                    var form = document.forms[form_id];
                    var data = new FormData(form);
                    // Display the key/value pairs
                    for (var pair of data.entries()) {
                        console.log(pair[0]+ ', ' + pair[1]); 
                    }
                    axios.post(route(route_name, id), data).then(response => {
                        swal('Pelanjutan Masa', 'Rekod berjaya dikemaskini', 'success');
                        // window.location.replace('/extension/extension');
                    });
                });
            @else
                $('#bil_meeting').val('{{ $eot->eot_approve->bil_meeting }}');
                $('#eot_approve_id').val('{{ $eot->eot_approve->id }}');
                $('#eot_approve_period').val('{{ $eot->eot_approve->period }}') ;
                $("#eot_approve_period_type_id").val('{{ $eot->eot_approve->period_type_id }}').attr("disabled", true);;
                $('#eot_approve_bil').val('{{ $eot->eot_approve->bil }}');
                $('#eot_approve_meeting_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d  H:i:s', $eot->eot_approve->meeting_date)->format('d/m/Y') }}');
                $('#approved_end_date').val('{{ $latest_end_date }}');
                $('input:radio[name=eot_approve_approval][value={{ $eot->eot_approve->approval }}]').prop('checked', true);

                $('#bil').val('{{ $eot->bil }}') ;
                $('#period').val('{{ $eot->period }}') ;
                $("#period_type_id").val('{{ $eot->period_type_id }}').attr("disabled", true);
                $('#original_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->original_end_date)->format('d/m/Y') }}') ;
                $('#extended_end_date').val('{{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $eot->extended_end_date)->format('d/m/Y') }}') ;
                @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                    $('input:radio[name=section][value={{ $eot->section }}]').prop('checked', true);
                @endif
                $('#eot_id').val('{{ $eot->id }}');
            @endif

            /* DOCUMENT UPLOAD */
            var t = $('#tblUpload1').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            @if(!empty($eot->eot_approve) && ($eot->eot_approve->documents->count() > 0))
                var t_result = $('#tblUpload_result').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
                });

                var counter_result = 1;
                @foreach($eot->eot_approve->documents as $doc)
                    t_result.row.add( [
                        counter_result,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter_result++;
                @endforeach
            @endif
            /* END DOCUMENT UPLOAD */
        });
	</script>
    <script type="text/javascript">
        function getKategori1(obj) {
            var bonbaru = document.getElementById('bonbaru');
            var bonlama = document.getElementById('bonlama');

            if(obj.value == '1'){
                bonbaru.hidden = false;
                bonlama.hidden = true;
            }
            else if(obj.value == '2'){
                bonbaru.hidden = true;
                bonlama.hidden = false;
            }
        }

        function getKategori2(obj) {
            var insuransbaru = document.getElementById('insuransbaru');
            var insuranslama = document.getElementById('insuranslama');

            if(obj.value == '1'){
                insuransbaru.hidden = false;
                insuranslama.hidden = true;
            }
            else if(obj.value == '2'){
                insuransbaru.hidden = true;
                insuranslama.hidden = false;
            }
        }
    </script>
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        @component('components.pages.title-sub')
            @slot('title_sub_content')
                <span class="font-weight-bold">Tajuk Perolehan : </span>{{ $sst->acquisition->title }}
                <br>
                <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nilai Kontrak : </span>{{ money()->toHuman($appointed_company->offered_price) }}
                <br>
                <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ $sst->start_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tempoh : </span>{{ $sst->period.' '.$sst->period_type->name }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ $sst->end_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                @if(!empty($new_eot_date))
                    <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
                @endif
            @endslot
        @endcomponent
    </div>
</div>
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#extension" role="tab" aria-controls="extension" aria-selected="false">
                    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                        @icon('fe fe-file')&nbsp;{{ __('Sijil Kelambatan dan Pelanjutan Masa') }}
                    @else
                        @icon('fe fe-file')&nbsp;{{ __('Pelanjutan Tempoh Kontrak') }}
                    @endif
                </a>
            </li>
            {{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#extension-upload" role="tab" aria-controls="extension-upload" aria-selected="false">
                        @icon('fe fe-upload')&nbsp;{{ __('Muat Naik') }}
                    </a>
                </li>
            @endif --}}
            @if(!empty($review) && (user()->id == $review->created_by))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews" role="tab" aria-controls="document-log-reviews" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif
            @endif
            @if(!empty($eot_approve))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#approve" role="tab" aria-controls="approve" aria-selected="false">
                        @icon('fe fe-check-square')&nbsp;{{ __('Kelulusan') }}
                    </a>
                </li>
                @if($eot->eot_approve->approval == 1)
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#bon" role="tab" aria-controls="bon" aria-selected="false">
                            @icon('fe fe-award')&nbsp;{{ __('Bon') }}
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                            @icon('fe fe-dollar-sign')&nbsp;{{ __('Insurans') }}
                        </a>
                    </li>
                @endif
            @endif
            @role('penyedia')
                @if(!empty($review) && $review->status == 'Selesai')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#upload" role="tab" aria-controls="upload" aria-selected="false">
                            @icon('fe fe-upload')&nbsp;{{ __('Muat Naik Keputusan EOT') }}
                        </a>
                    </li>
                @endif
            @endrole
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'extension', 'active' => true])
                            @slot('content')
                                @include('contract.post.extension.edit_list.partials.forms.extension', [])
                                {{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4') --}}
                                    @include('contract.post.extension.partials.forms.upload', [])
                                    <div id="send_review" class="btn-group float-right">
                                        @if(!isset($eot->eot_approve->id))
                                                <a href="{{ route('extension.extension_edit.index', ['hashslug' => $appointed_company->hashslug]) }}" 
                                                        class="btn btn-default border-primary">
                                                        {{ __('Kembali') }}
                                                </a>
                                                <button type="submit" class="btn btn-primary update-action-btn">
                                                    @icon('fe fe-save') {{ __('Simpan') }}
                                                </button>
                                            </form>
                                        @endif
                                        @role('penyedia')
                                            @if(!empty($eot) && $eot->user_id == user()->id)
                                                @if(empty($review) || empty($review->status))
                                                    <form method="POST" action="{{ route('acquisition.review.store') }}">
                                                        @csrf
                                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                                                        <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $eot->sst->acquisition_id }}" hidden>
                                                        <input type="text" id="eot_id" name="eot_id" value="{{ $eot->id }}" hidden>

                                                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                                                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                                                        <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                                                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                                        <input type="hidden" id="progress" name="progress" value="0">
                                                        <input type="hidden" id="status" name="status" value="S1">
                                                        <input type="hidden" id="department" name="department" value="PELAKSANA">

                                                        @if(!empty($review_previous))
                                                            <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                                                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                                            <input type="text" id="type" name="type" value="EOT" hidden>
                                                        @endif
                                                        
                                                        <button type="button" id="send" class="btn btn-default float-middle border-default">
                                                            @icon('fe fe-send') {{ __('Teratur') }}
                                                        </button>
                                                    </form>
                                                @endif
                                            @endif
                                        @endrole
                                    </div>
                                {{-- @endif --}}
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-reviews'])
                            @slot('content')

                                <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                                    @if((!empty($review)) && (user()->id == $review->created_by))

                                        {{-- penyedia --}}
                                        @if( ($appointed_company->acquisition->approval->acquisition_type_id == '2' && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2')) || (($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3' || $appointed_company->acquisition->approval->acquisition_type_id == '4') && ($appointed_company->acquisition->acquisition_category_id == '1' || $appointed_company->acquisition->acquisition_category_id == '2' || $appointed_company->acquisition->acquisition_category_id == '3' || $appointed_company->acquisition->acquisition_category_id == '4')) )
                                            @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            @if(!empty($cetaknotisns) && !empty($cetaknotisns->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOnen" aria-expanded="false" aria-controls="collapseOnen">
                                                                Ulasan Semakan Oleh {{$cetaknotisns->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOnen" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sln as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif
                                
                                </div>

                            @endslot
                        @endcomponent
                        @if(!empty($eot_approve))
                            @component('components.tab.content', ['id' => 'approve'])
                                @slot('content')
                                    @include('contract.post.extension.edit_list.partials.forms.approve', [])
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'bon'])
                                @slot('content')
                                    @include('contract.post.extension.edit_list.partials.forms.bon', [])
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'insurance'])
                                @slot('content')
                                    @include('contract.post.extension.edit_list.partials.forms.insurance', [])
                                @endslot
                            @endcomponent
                        @endif
                        @component('components.tab.content', ['id' => 'upload'])
                            @slot('content')
                                <form id="upload_form" files = "true" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <h4>Muat Naik Keputusan EOT</h4>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <label for="Fail_Keputusan_Mesyuarat"
                                                   class="col col-form-label">
                                                    Muat Naik Keputusan EOT
                                                </label>

                                                <div class="col input-group">
                                                    <input id="subFile" type="text"  class="form-control" readonly>
                                                    <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                                                    <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                                    @if(!empty($eot) && !empty($eot->documents))
                                                        @foreach($eot->documents as $document)
                                                            <label class="input-group-text" for="downloadFile">
                                                                <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                                            </label>

                                                            <label class="input-group-text" for="deleteFile">
                                                                <input type="input" id="deleteFile" class="buang-upload" value="{{ $document->hashslug }}" hidden>
                                                                <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                                            </label>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                
                                            </div>
                                            <div class="btn-group float-right">   
                                                <a href="{{ route('extension.extension_edit.index', ['hashslug' => $appointed_company->hashslug]) }}" 
                                                    class="btn btn-default border-primary">
                                                    {{ __('Kembali') }}
                                                </a>
                                                @role('penyedia')
                                                    @if(!empty($review) && $review->status == 'Selesai')
                                                        <button type="submit" class="btn btn-primary upload-action-btn">
                                                            @icon('fe fe-save') {{ __('Simpan') }}
                                                        </button>
                                                    @endif
                                                @endrole
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>
</div>
@endsection