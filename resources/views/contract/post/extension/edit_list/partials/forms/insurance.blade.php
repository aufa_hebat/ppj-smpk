@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $('#publicLiability').show();
            $('#work').hide();
            $('#compensation').hide();

            // onload fields
            @if(!empty($insurance))
                @if($insurance->insurance_old_new == 1)
                    @if(!empty($sst->insurance))
                        $("#insurance_old_new").val('{{ $insurance->insurance_old_new }}');
                        $('#public_insurance_name').val('{{ $sst->insurance->public_insurance_name }}');
                        $('#public_policy_no').val('{{ $sst->insurance->public_policy_no }}');
                        $('#public_received_date').val('{{ $sst->insurance->public_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_received_date)->format('d/m/Y'):null }}');
                        $('#public_approval_proposed_date').val('{{ $sst->insurance->public_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_approval_proposed_date)->format('d/m/Y'):null }}');
                        $('#public_approval_received_date').val('{{ $sst->insurance->public_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_approval_received_date)->format('d/m/Y'):null }}');

                        $('#work_insurance_name').val('{{ $sst->insurance->work_insurance_name }}');
                        $('#work_policy_no').val('{{ $sst->insurance->work_policy_no }}');
                        $('#work_received_date').val('{{ $sst->insurance->work_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_received_date)->format('d/m/Y'):null }}');
                        $('#work_approval_proposed_date').val('{{ $sst->insurance->work_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_approval_proposed_date)->format('d/m/Y'):null }}');
                        $('#work_approval_received_date').val('{{ $sst->insurance->work_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_approval_received_date)->format('d/m/Y'):null }}');

                        $('#compensation_insurance_name').val('{{ $sst->insurance->compensation_insurance_name }}');
                        $('#compensation_policy_no').val('{{ $sst->insurance->compensation_policy_no }}');
                        $('#compensation_received_date').val('{{ $sst->insurance->compensation_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_received_date)->format('d/m/Y'):null }}');
                        $('#compensation_approval_proposed_date').val('{{ $sst->insurance->compensation_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_approval_proposed_date)->format('d/m/Y'):null }}');
                        $('#compensation_approval_received_date').val('{{ $sst->insurance->compensation_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_approval_received_date)->format('d/m/Y'):null }}');
                    @endif
                @elseif($insurance->insurance_old_new == 2)
                    $("#insurance_old_new").val('{{ $insurance->insurance_old_new }}');
                    $('#public_insurance_name').val('{{ $insurance->public_insurance_name }}');
                    $('#public_policy_no').val('{{ $insurance->public_policy_no }}');
                    $('#public_received_date').val('{{ $insurance->public_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->public_received_date)->format('d/m/Y'):null }}');
                    $('#public_approval_proposed_date').val('{{ $insurance->public_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->public_approval_proposed_date)->format('d/m/Y'):null }}');
                    $('#public_approval_received_date').val('{{ $insurance->public_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->public_approval_received_date)->format('d/m/Y'):null }}');

                    $('#work_insurance_name').val('{{ $insurance->work_insurance_name }}');
                    $('#work_policy_no').val('{{ $insurance->work_policy_no }}');
                    $('#work_received_date').val('{{ $insurance->work_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->work_received_date)->format('d/m/Y'):null }}');
                    $('#work_approval_proposed_date').val('{{ $insurance->work_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->work_approval_proposed_date)->format('d/m/Y'):null }}');
                    $('#work_approval_received_date').val('{{ $insurance->work_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->work_approval_received_date)->format('d/m/Y'):null }}');

                    $('#compensation_insurance_name').val('{{ $insurance->compensation_insurance_name }}');
                    $('#compensation_policy_no').val('{{ $insurance->compensation_policy_no }}');
                    $('#compensation_received_date').val('{{ $insurance->compensation_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->compensation_received_date)->format('d/m/Y'):null }}');
                    $('#compensation_approval_proposed_date').val('{{ $insurance->compensation_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->compensation_approval_proposed_date)->format('d/m/Y'):null }}');
                    $('#compensation_approval_received_date').val('{{ $insurance->compensation_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$insurance->compensation_approval_received_date)->format('d/m/Y'):null }}');
                @endif
            @endif

            @if(!empty($sst))
                $('#public_policy_amount').val('{{ money()->toHuman($sst->ins_pli_amount) }}');
                $('#public_start_date').val('{{ $sst->ins_pli_start_date->format('d/m/Y') }}');
                $('#public_expired_date').val('{{ $sst->insurance->public_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_expired_date)->format('d/m/Y'):null }}');
                // $('#public_expired_date').val('{{ $sst->ins_pli_end_date->format('d/m/Y') }}');
                $('#public_proposed_date').val('{{ $proposed_date->format('d/m/Y') }}');

                $('#work_policy_amount').val('{{ money()->toHuman($sst->ins_work_amount) }}');
                $('#work_start_date').val('{{ $sst->ins_work_start_date->format('d/m/Y') }}');
                $('#work_expired_date').val('{{ $sst->insurance->work_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_expired_date)->format('d/m/Y'):null }}');
                // $('#work_expired_date').val('{{ $sst->ins_work_end_date->format('d/m/Y') }}');
                $('#work_proposed_date').val('{{ $proposed_date->format('d/m/Y') }}');

                $('#compensation_policy_amount').val('{{ money()->toHuman($sst->ins_ci_amount) }}');
                $('#compensation_start_date').val('{{ $sst->ins_ci_start_date->format('d/m/Y') }}');
                $('#compensation_expired_date').val('{{ $sst->insurance->compensation_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_expired_date)->format('d/m/Y'):null }}');
                // $('#compensation_expired_date').val('{{ $sst->ins_ci_end_date->format('d/m/Y') }}');
                $('#compensation_proposed_date').val('{{ $proposed_date->format('d/m/Y') }}');
            @endif

            @if(!empty($sst) && !empty($sst->insurance))
                $('#insurance_old_new').change(function () {
                    // IF OLD INSURANCE IS SELECTED //
                    if(this.value == '1'){
                        $('#public_insurance_name').val('{{ $sst->insurance->public_insurance_name }}');
                        $('#public_policy_no').val('{{ $sst->insurance->public_policy_no }}');
                        $('#public_received_date').val('{{ $sst->insurance->public_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_received_date)->format('d/m/Y'):null }}');
                        $('#public_approval_proposed_date').val('{{ $sst->insurance->public_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_approval_proposed_date)->format('d/m/Y'):null }}');
                        $('#public_approval_received_date').val('{{ $sst->insurance->public_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->public_approval_received_date)->format('d/m/Y'):null }}');

                        $('#work_insurance_name').val('{{ $sst->insurance->work_insurance_name }}');
                        $('#work_policy_no').val('{{ $sst->insurance->work_policy_no }}');
                        $('#work_received_date').val('{{ $sst->insurance->work_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_received_date)->format('d/m/Y'):null }}');
                        $('#work_approval_proposed_date').val('{{ $sst->insurance->work_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_approval_proposed_date)->format('d/m/Y'):null }}');
                        $('#work_approval_received_date').val('{{ $sst->insurance->work_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->work_approval_received_date)->format('d/m/Y'):null }}');

                        $('#compensation_insurance_name').val('{{ $sst->insurance->compensation_insurance_name }}');
                        $('#compensation_policy_no').val('{{ $sst->insurance->compensation_policy_no }}');
                        $('#compensation_received_date').val('{{ $sst->insurance->compensation_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_received_date)->format('d/m/Y'):null }}');
                        $('#compensation_approval_proposed_date').val('{{ $sst->insurance->compensation_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_approval_proposed_date)->format('d/m/Y'):null }}');
                        $('#compensation_approval_received_date').val('{{ $sst->insurance->compensation_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->insurance->compensation_approval_received_date)->format('d/m/Y'):null }}');
                    }
                    // IF NEW INSURANCE IS SELECTED //
                    else if(this.value == '2'){
                        $('#public_insurance_name').val(null);
                        $('#public_policy_no').val(null);
                        $('#public_received_date').val(null);
                        $('#public_approval_proposed_date').val(null);
                        $('#public_approval_received_date').val(null);

                        $('#work_insurance_name').val(null);
                        $('#work_policy_no').val(null);
                        $('#work_received_date').val(null);
                        $('#work_approval_proposed_date').val(null);
                        $('#work_approval_received_date').val(null);

                        $('#compensation_insurance_name').val(null);
                        $('#compensation_policy_no').val(null);
                        $('#compensation_received_date').val(null);
                        $('#compensation_approval_proposed_date').val(null);
                        $('#compensation_approval_received_date').val(null);
                    }
                });
            @endif

            $('input[type=radio][name=insurance_type]').change(function () {

                if(this.value == '1'){
                    $('#publicLiability').show();
                    $('#work').hide();
                    $('#compensation').hide();
                }
                else if (this.value == '2'){
                    $('#publicLiability').hide();
                    $('#work').show();
                    $('#compensation').hide();
                }
                else if (this.value == '3'){
                    $('#publicLiability').hide();
                    $('#work').hide();
                    $('#compensation').show();
                }
            });

            /* DOCUMENT UPLOAD _publicLiability*/
            var t_publicLiability = $('#tblUpload_publicLiability').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_publicLiability = 1;

            $('#addRow_publicLiability').on( 'click', function () {
                t_publicLiability.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile_publicLiability'+ counter_publicLiability +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile_publicLiability'+ counter_publicLiability +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile_publicLiability" id="uploadFile_publicLiability'+ counter_publicLiability +'" name="uploadFile_publicLiability[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_publicLiability +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove_publicLiability'+ counter_publicLiability +'" class="remove_publicLiability btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter_publicLiability++;
            } );

            @if(!empty($insurance) && ($insurance->documents_publicLiability->count() > 0))
            @foreach($insurance->documents_publicLiability as $doc)
            t_publicLiability.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile_publicLiability'+ counter_publicLiability +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId_publicLiability'+ counter_publicLiability +'" name="hDocumentId_publicLiability[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile_publicLiability'+ counter_publicLiability +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile_publicLiability" id="uploadFile_publicLiability'+ counter_publicLiability +'" name="uploadFile_publicLiability[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_publicLiability +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove_publicLiability'+ counter_publicLiability +'" class="remove_publicLiability btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter_publicLiability++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow_publicLiability').click();
            @endif

            $("#tblUpload_publicLiability").on('click','.remove_publicLiability',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        t_publicLiability.row($(this).closest("tr")).remove().draw(false);
                    }
                });

            });

            $("#tblUpload_publicLiability").on('change','.uploadFile_publicLiability',function(){
                var no_publicLiability = $(this).data('counter');
                $('#subFile_publicLiability' + no_publicLiability).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD _publicLiability */

            /* DOCUMENT UPLOAD _work */
            var t_work = $('#tblUpload_work').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_work = 1;

            $('#addRow_work').on( 'click', function () {
                t_work.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile_work'+ counter_work +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile_work'+ counter_work +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile_work" id="uploadFile_work'+ counter_work +'" name="uploadFile_work[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_work +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove_work'+ counter_work +'" class="remove_work btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter_work++;
            } );

            @if(!empty($insurance) && ($insurance->documents_work->count() > 0))
            @foreach($insurance->documents_work as $doc)
            t_work.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile_work'+ counter_work +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId_work'+ counter_work +'" name="hDocumentId_work[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile_work'+ counter_work +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile_work" id="uploadFile_work'+ counter_work +'" name="uploadFile_work[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_work +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove_work'+ counter_work +'" class="remove_work btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter_work++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow_work').click();
            @endif

            $("#tblUpload_work").on('click','.remove_work',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        t_work.row($(this).closest("tr")).remove().draw(false);
                    }
                });

            });

            $("#tblUpload_work").on('change','.uploadFile_work',function(){
                var no_work = $(this).data('counter');
                $('#subFile_work' + no_work).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD _work */

            /* DOCUMENT UPLOAD _compensation*/
            var t_compensation = $('#tblUpload_compensation').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_compensation = 1;

            $('#addRow_compensation').on( 'click', function () {
                t_compensation.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile_compensation'+ counter_compensation +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile_compensation'+ counter_compensation +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile_compensation" id="uploadFile_compensation'+ counter_compensation +'" name="uploadFile_compensation[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_compensation +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove_compensation'+ counter_compensation +'" class="remove_compensation btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter_compensation++;
            } );

            @if(!empty($insurance) && ($insurance->documents_compensation->count() > 0))
            @foreach($insurance->documents_compensation as $doc)
            t_compensation.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile_compensation'+ counter_compensation +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId_compensation'+ counter_compensation +'" name="hDocumentId_compensation[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile_compensation'+ counter_compensation +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile_compensation" id="uploadFile_compensation'+ counter_compensation +'" name="uploadFile_compensation[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_compensation +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove_compensation'+ counter_compensation +'" class="remove_compensation btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter_compensation++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow_compensation').click();
            @endif

            $("#tblUpload_compensation").on('click','.remove_compensation',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        t_compensation.row($(this).closest("tr")).remove().draw(false);
                    }
                });

            });

            $("#tblUpload_compensation").on('change','.uploadFile_compensation',function(){
                var no_compensation = $(this).data('counter');
                $('#subFile_compensation' + no_compensation).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD _compensation */

            $(document).on('click', '#insurance-submit', function(event) {
                event.preventDefault();

                var id = '{{ $eot->id }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);
                console.log(route(route_name, {id:id}));
                axios.post(route(route_name, id), data).then(response => {
                        swal('{!! __('Insuran') !!}', response.data.message, 'success');
                }).catch((error)=>{
                        console.log(error.response.data);
                });
            });
            
        });


    </script>
@endpush
<h4>Insuran</h4>
<br>
<div class="row">
    <div class="col-12">
        <form id="insurance-form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            @method('PUT')
            
            <div class="form-group">
                <label for="insurance_old_new"
                       class="col col-form-label">{{ __('Insurance Lama Atau Baru') }}
                </label>
                <select id="insurance_old_new" name="insurance_old_new" class="form-control w-100">
                    <option value="">{{ __('Sila Pilih') }}</option>
                        <option value="1">Insurance Lama</option>
                        <option value="2">Insurance Baru</option>
                </select><br>
                <div class="selectgroup w-100">
                    <label class="selectgroup-item">
                        <input type="radio" name="insurance_type" value="1" class="selectgroup-input" checked>
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Tanggungan Awam') }}</span>
                    </label>
                    <label class="selectgroup-item">
                        <input type="radio" name="insurance_type" value="2" class="selectgroup-input">
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Untuk Kerja') }}</span>
                    </label>
                    <label class="selectgroup-item">
                        <input type="radio" name="insurance_type" value="3" class="selectgroup-input">
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Pampasan Kerja') }}</span>
                    </label>
                </div>
            </div>
    
            @include('contract.post.extension.edit_list.partials.forms.insurance.publicLiability')
            @include('contract.post.extension.edit_list.partials.forms.insurance.work')
            @include('contract.post.extension.edit_list.partials.forms.insurance.compensation')
        </form>
        @include('contract.post.extension.edit_list.partials.forms.insurance.submit', [
            'route_name' => 'api.contract.extension_insurance.update',
            'form' => 'insurance-form'
        ])
    </div>
</div>