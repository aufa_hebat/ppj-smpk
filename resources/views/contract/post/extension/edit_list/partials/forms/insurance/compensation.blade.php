<div id="compensation">
    <div class="row">
    <div class="col-6">
        @include('components.forms.select', [
            'input_label' => 'Nama Pengeluar Insuran',
            'id' => 'compensation_insurance_name',
            'name' => 'compensation_insurance_name',
            'options' => insurances()->pluck('name','id'),
        ])
        @include('components.forms.input', [
           'input_label' => __('Nilai Polisi (RM)'),
           'id' => 'compensation_policy_amount',
           'name' => 'compensation_policy_amount',
           'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mula Sah Laku Polisi'),
            'id' => 'compensation_start_date',
            'name' => 'compensation_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Akhir Mengemukakan Polisi'),
            'id' => 'compensation_proposed_date',
            'name' => 'compensation_proposed_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Penerimaan Pengesahan Polisi'),
            'id' => 'compensation_approval_received_date',
            'name' => 'compensation_approval_received_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
           'input_label' => __('No. Polisi'),
           'id' => 'compensation_policy_no',
           'name' => 'compensation_policy_no',
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Terima Insuran'),
            'id' => 'compensation_received_date',
            'name' => 'compensation_received_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Luput Polisi'),
            'id' => 'compensation_expired_date',
            'name' => 'compensation_expired_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mengemukakan Pengesahan Polisi'),
            'id' => 'compensation_approval_proposed_date',
            'name' => 'compensation_approval_proposed_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
</div>
    @include('contract.post.partials.indexes.insurance.uploads_compensation')
</div>