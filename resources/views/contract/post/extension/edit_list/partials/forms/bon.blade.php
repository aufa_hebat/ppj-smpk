@push('scripts')
    @include('components.forms.assets.select2')
    <script>
        jQuery(document).ready(function($) {

            $('#div_bon_type').hide();
            $('#jaminanBank').hide();
            $('#jaminanInsuran').hide();
            $('#cekBank').hide();
            $('#wangJaminan').hide();
            $('#submit').hide();

            // IF EOT_BON ALREADY EXIST //
            @if(!empty($bon))
                // IF NEW BON IS CREATED LAST TIME //
                @if( $bon->bon_old_new == 2 )
                    $('#div_bon_type').show();
                    $('#bon_type').prop("disabled", false);

                    @if( $bon->bon_type == 1 )
                    $('#jaminanBank').show();
                    @elseif($bon->bon_type == 2)
                    $('#jaminanInsuran').show();
                    @elseif($bon->bon_type == 3)
                    $('#cekBank').show();
                    @elseif($bon->bon_type == 4)
                    $('#wangJaminan').show();
                    @endif

                    // onload fields
                    $("#bon_type").val('{{ $bon->bon_type }}');
                    $("#bon_old_new").val('{{ $bon->bon_old_new }}');

                    @if($bon->bon_type)
                    $('#submit').show();
                    @endif

                    $('#bank_name').val('{{ $bon->bank_name }}');
                    $('#bank_no').val('{{ $bon->bank_no }}');
                    $('#bank_approval_received_date').val('{{ $bon->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->bank_approval_received_date)->format('d/m/Y'):null }}');
                    $('#bank_received_date').val('{{ $bon->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->bank_received_date)->format('d/m/Y'):null }}');
                    $('#bank_approval_proposed_date').val('{{ $bon->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->bank_approval_proposed_date)->format('d/m/Y'):null }}');

                    $('#insurance_name').val('{{ $bon->insurance_name }}');
                    $('#insurance_no').val('{{ $bon->insurance_no }}');
                    $('#insurance_approval_received_date').val('{{ $bon->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->insurance_approval_received_date)->format('d/m/Y'):null }}');
                    $('#insurance_received_date').val('{{ $bon->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->insurance_received_date)->format('d/m/Y'):null }}');
                    $('#insurance_approval_proposed_date').val('{{ $bon->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->insurance_approval_proposed_date)->format('d/m/Y'):null }}');

                    $('#cheque_name').val('{{ $bon->cheque_name }}');
                    $('#cheque_resit_no').val('{{ $bon->cheque_resit_no }}');
                    $('#cheque_received_date').val('{{ $bon->cheque_received_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->cheque_received_date)->format('d/m/Y'):null }}');
                    $('#cheque_no').val('{{ $bon->cheque_no }}');
                    $('#cheque_submitted_date').val('{{ $bon->cheque_submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->cheque_submitted_date)->format('d/m/Y'):null }}');

                    $('#money_received_at').val('{{ $bon->money_received_at? \Carbon\Carbon::createFromFormat('Y-m-d h:i:s',$bon->money_received_at)->format('d/m/Y'):null }}');
                // IF OLD BON IS REUSED LAST TIME//
                @elseif( $bon->bon_old_new == 1 )
                    $('#div_bon_type').show();
                    $('#bon_type').prop("disabled", true);

                    @if( $sst->bon->bon_type == 1 )
                    $('#jaminanBank').show();
                    @elseif($sst->bon->bon_type == 2)
                    $('#jaminanInsuran').show();
                    @elseif($sst->bon->bon_type == 3)
                    $('#cekBank').hide();
                    @elseif($sst->bon->bon_type == 4)
                    $('#wangJaminan').hide();
                    @endif

                    // onload fields
                    $("#bon_type").val('{{ $sst->bon->bon_type }}');
                    $("#bon_old_new").val('{{ $bon->bon_old_new }}');

                    @if($bon->bon_type)
                    $('#submit').show();
                    @endif

                    $('#bank_name').val('{{ $sst->bon->bank_name }}');
                    $('#bank_no').val('{{ $sst->bon->bank_no }}');
                    $('#bank_approval_received_date').val('{{ $sst->bon->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_approval_received_date)->format('d/m/Y'):null }}');
                    $('#bank_received_date').val('{{ $sst->bon->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_received_date)->format('d/m/Y'):null }}');
                    $('#bank_approval_proposed_date').val('{{ $sst->bon->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_approval_proposed_date)->format('d/m/Y'):null }}');

                    $('#insurance_name').val('{{ $sst->bon->insurance_name }}');
                    $('#insurance_no').val('{{ $sst->bon->insurance_no }}');
                    $('#insurance_approval_received_date').val('{{ $sst->bon->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_approval_received_date)->format('d/m/Y'):null }}');
                    $('#insurance_received_date').val('{{ $sst->bon->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_received_date)->format('d/m/Y'):null }}');
                    $('#insurance_approval_proposed_date').val('{{ $sst->bon->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_approval_proposed_date)->format('d/m/Y'):null }}');

                    // $('#cheque_name').val('{{ $sst->bon->cheque_name }}');
                    // $('#cheque_resit_no').val('{{ $sst->bon->cheque_resit_no }}');
                    // $('#cheque_received_date').val('{{ $sst->bon->cheque_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->cheque_received_date)->format('d/m/Y'):null }}');
                    // $('#cheque_no').val('{{ $sst->bon->cheque_no }}');
                    // $('#cheque_submitted_date').val('{{ $sst->bon->cheque_submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->cheque_submitted_date)->format('d/m/Y'):null }}');

                    // $('#money_received_at').val('{{ $sst->bon->money_received_at? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->money_received_at)->format('d/m/Y'):null }}');
                    // $('#money_amount').val('{{ money()->toHuman($sst->bon->money_amount) }}');
                @endif
            @endif

            @if(!empty($sst))
                $('#bank_amount').val('{{ money()->toHuman($sst->bon_amount) }}');
                $('#bank_start_date').val('{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}');
                $('#bank_expired_date').val('{{ $sst->bon->bank_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_expired_date)->format('d/m/Y'):null }}');
                // $('#bank_expired_date').val('{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}');
                $('#bank_proposed_date').val('{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}');

                $('#insurance_amount').val('{{ money()->toHuman($sst->bon_amount) }}');
                $('#insurance_start_date').val('{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}');
                $('#insurance_expired_date').val('{{ $sst->bon->insurance_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_expired_date)->format('d/m/Y'):null }}');
                // $('#insurance_expired_date').val('{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}');
                $('#insurance_proposed_date').val('{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}');
            @endif

            @if(!empty($sst) && !empty($sst->bon) )
                $('#bon_old_new').change(function () {
                    // IF OLD BON IS SELECTED //
                    if(this.value == '1'){
                        $('#div_bon_type').show();
                        $('#bon_type').prop("disabled", true);

                        @if( $sst->bon->bon_type == 1 )
                            $('#jaminanBank').show();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                        @elseif($sst->bon->bon_type == 2)
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').show();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                        @elseif($sst->bon->bon_type == 3)
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                        @elseif($sst->bon->bon_type == 4)
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                        @endif

                        // onload fields
                        $("#bon_type").val('{{ $sst->bon->bon_type }}');

                        @if($sst->bon->bon_type)
                        $('#submit').show();
                        @endif

                        $('#bank_name').val('{{ $sst->bon->bank_name }}');
                        $('#bank_no').val('{{ $sst->bon->bank_no }}');
                        $('#bank_approval_received_date').val('{{ $sst->bon->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_approval_received_date)->format('d/m/Y'):null }}');
                        $('#bank_received_date').val('{{ $sst->bon->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_received_date)->format('d/m/Y'):null }}');
                        $('#bank_approval_proposed_date').val('{{ $sst->bon->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->bank_approval_proposed_date)->format('d/m/Y'):null }}');

                        $('#insurance_name').val('{{ $sst->bon->insurance_name }}');
                        $('#insurance_no').val('{{ $sst->bon->insurance_no }}');
                        $('#insurance_approval_received_date').val('{{ $sst->bon->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_approval_received_date)->format('d/m/Y'):null }}');
                        $('#insurance_received_date').val('{{ $sst->bon->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_received_date)->format('d/m/Y'):null }}');
                        $('#insurance_approval_proposed_date').val('{{ $sst->bon->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->insurance_approval_proposed_date)->format('d/m/Y'):null }}');

                        // $('#cheque_name').val('{{ $sst->bon->cheque_name }}');
                        // $('#cheque_resit_no').val('{{ $sst->bon->cheque_resit_no }}');
                        // $('#cheque_received_date').val('{{ $sst->bon->cheque_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->cheque_received_date)->format('d/m/Y'):null }}');
                        // $('#cheque_no').val('{{ $sst->bon->cheque_no }}');
                        // $('#cheque_submitted_date').val('{{ $sst->bon->cheque_submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->cheque_submitted_date)->format('d/m/Y'):null }}');

                        // $('#money_received_at').val('{{ $sst->bon->money_received_at? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->bon->money_received_at)->format('d/m/Y'):null }}');
                        // $('#money_amount').val('{{ money()->toHuman($sst->bon->money_amount) }}');
                    }
                    // IF NEW BON IS SELECTED //
                    else if(this.value == '2'){
                        $('#div_bon_type').val(null);
                        $('#bon_type').prop("disabled", false);
                        $('#submit').show();

                        //clear all field
                        $('#bank_name').val(null);
                        $('#bank_no').val(null);
                        $('#bank_approval_proposed_date').val(null);
                        $('#bank_approval_received_date').val(null);
                        $('#bank_received_date').val(null);

                        $('#insurance_name').val(null);
                        $('#insurance_no').val(null);
                        $('#insurance_approval_proposed_date').val(null);
                        $('#insurance_approval_received_date').val(null);
                        $('#insurance_received_date').val(null);

                        $('#cheque_name').val(null);
                        $('#cheque_resit_no').val(null);
                        $('#cheque_received_date').val(null);
                        $('#cheque_no').val(null);
                        $('#cheque_submitted_date').val(null);

                        $('#money_received_at').val(null);
                        $('#money_value').val(null);

                        if($('#div_bon_type').value == '1'){
                            $('#jaminanBank').show();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                        }
                        else if ($('#div_bon_type').value == '2'){
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').show();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                        }
                        else if ($('#div_bon_type').value == '3'){
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').show();
                            $('#wangJaminan').hide();
                        }
                        else if ($('#div_bon_type').value == '4'){
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').hide();
                            $('#wangJaminan').show();
                        }
                        else{
                            $('#jaminanBank').hide();
                            $('#jaminanInsuran').hide();
                            $('#cekBank').hide();
                            $('#wangJaminan').hide();
                            $('#submit').hide();
                        }
                    }
                });
            @endif

            $('#bon_type').change(function () {
                $('#submit').show();

                //clear all field
                $('#bank_name').val(null);
                $('#bank_no').val(null);
                $('#bank_approval_proposed_date').val(null);
                $('#bank_approval_received_date').val(null);
                $('#bank_received_date').val(null);

                $('#insurance_name').val(null);
                $('#insurance_no').val(null);
                $('#insurance_approval_proposed_date').val(null);
                $('#insurance_approval_received_date').val(null);
                $('#insurance_received_date').val(null);

                $('#cheque_name').val(null);
                $('#cheque_resit_no').val(null);
                $('#cheque_received_date').val(null);
                $('#cheque_no').val(null);
                $('#cheque_submitted_date').val(null);

                $('#money_received_at').val(null);
                $('#money_value').val(null);

                if(this.value == '1'){
                    $('#jaminanBank').show();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').hide();
                    $('#wangJaminan').hide();
                }
                else if (this.value == '2'){
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').show();
                    $('#cekBank').hide();
                    $('#wangJaminan').hide();
                }
                else if (this.value == '3'){
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').show();
                    $('#wangJaminan').hide();
                }
                else if (this.value == '4'){
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').hide();
                    $('#wangJaminan').show();
                }
                else{
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').hide();
                    $('#wangJaminan').hide();
                    $('#submit').hide();
                }
            });

            /* DOCUMENT UPLOAD */
            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($bon) && ($bon->documents->count() > 0))
            @foreach($bon->documents as $doc)
            t.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow').click();
            @endif

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD */

            $(document).on('click', '.submit-bon-btn', function(event) {
                event.preventDefault();
                var id = '{{ $eot->id }}';
                var route_name = 'api.contract.extension_bon.update';
                var form = document.forms['bonForm'];
                var data = new FormData(form);
                data.append('_method', 'PUT'); // ADD THIS LINE
                axios.post(route(route_name, id), data).then(response => {
                    swal('Bon', response.data.message, 'success');
                });
            });

        });


    </script>
@endpush

<h4>Bon Pelaksanaan</h4>
<div class="row">
    <div class="col-12">
        <form id="bonForm" files="true" enctype="multipart/form-data" method="POST">
            <div class="form-group row">
                <label for="bon_old_new"
                       class="col col-form-label">{{ __('Bon Lama Atau Baru') }}
                </label>
                <div class="col">
                    <select id="bon_old_new" name="bon_old_new" class="form-control w-100">
                        <option value="">{{ __('Sila Pilih') }}</option>
                            <option value="1">Bon Lama</option>
                            <option value="2">Bon Baru</option>
                    </select>
                </div>
                
                <div class="col" id="div_bon_type">
                    <label for="bon_type"
                           class="col col-form-label">{{ __('Jenis Bon Pelaksanaan') }}
                    </label>
                    <select id="bon_type" name="bon_type" class="form-control w-100">
                        <option value="">{{ __('Sila Pilih') }}</option>
                        @foreach(bon_types() as $index => $type)
                            <option value="{{ $type->code }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @include('contract.post.extension.edit_list.partials.forms.bon.jaminanBank')
            @include('contract.post.extension.edit_list.partials.forms.bon.jaminanInsuran')
            @include('contract.post.extension.edit_list.partials.forms.bon.cekBank')
            @include('contract.post.extension.edit_list.partials.forms.bon.wangJaminan')
            @include('contract.post.extension.edit_list.partials.forms.bon.uploads')
            @include('contract.post.extension.edit_list.partials.forms.bon.submit', [
                'route_name' => 'api.contract.extension_bon.update',
                'form' => 'bonForm'
            ])
        </form>
    </div>


</div>
