<div id="uploads_work">
    <hr>
    <h4>Dokumen</h4>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                <button type="button" id="addRow_work" href="#" class="btn btn-primary">
                    @icon('fe fe-plus')
                    {{ __('Tambah Dokumen') }}
                </button>
            </div>
            <div class="table-responsive">
                <table id="tblUpload_work" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th>Muat Naik Dokumen</th>
                        <th width="70px">Hapus</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>

</div>