<h3><b>Maklumat Kelulusan</b></h3>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => 'Bil Mesyuarat',
            'name' => 'bil_meeting',
            'id' => 'bil_meeting',
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mesyuarat J/K Sebutharga'),
            'id' => 'eot_approve_meeting_date',
            'name' => 'eot_approve_meeting_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => 'readonly'
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => 'Kelulusan Tempoh Lanjutan Masa',
            'name' => 'eot_approve_period',
            'id' => 'eot_approve_period',
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-6">
        @include('components.forms.select', [
            'input_label' => __('Jenis Tempoh'),
            'options' => period_types()->pluck('name', 'id'),
            'id' => 'eot_approve_period_type_id',
            'name' => 'eot_approve_period_type_id'
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => 'Tarikh Siap Diluluskan',
            'name' => 'approved_end_date',
            'id' => 'approved_end_date',
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-6">
        <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Keputusan</label><br>
        <div class="selectgroup w-25 grade-container">
            <label class="selectgroup-item">
            <input type="radio" id="eot_approve_approval" name="eot_approve_approval" value="2" class="selectgroup-input" disabled="true">
                <span class="selectgroup-button">{{ __('GAGAL') }}</span>
            </label>
            <label class="selectgroup-item">
            <input type="radio" id="eot_approve_approval" name="eot_approve_approval" value="1" class="selectgroup-input" disabled="true">
                <span class="selectgroup-button">{{ __('LULUS') }}</span>
            </label>
        </div>
    </div>
</div>

<div id="uploads_result">
    <hr>
    <h4>Keputusan Mesyuarat</h4>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tblUpload_result" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th width="10px">No</th>
                        <th >Nama Dokumen</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>