<div class="btn-group float-right" id="submit">
	{{ html()->a(route('contract.post.index'), __('Batal'))->class('btn btn-outline-default') }}

	<button type="submit" class="btn btn-primary float-right submit-bon-btn" id="bon-submit"
			data-route="{{ $route_name }}"
			data-form="{{ $form }}">
	    @icon('fe fe-save') {{ __('Simpan') }}
	</button>
</div>
