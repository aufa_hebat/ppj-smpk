@if(!isset($eot->eot_approve->id))
    <form id="extension_form" files = "true" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PUT')
@endif

@if($appointed_company->acquisition->approval->acquisition_type_id == '2')
<h3><b>Maklumat Perakuan Kelambatan dan Pelanjutan Masa</b></h3>
@else
<h3><b>Maklumat Pelanjutan Tempoh Kontrak</b></h3>
@endif

<div class="row">
    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-1">
            @include('components.forms.input', [
                'input_label' => 'Bil',
                'name' => 'bil',
                'id' => 'bil',
                'readonly' => 'readonly'
            ])
        </div>
    @endif
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => 'Permohonan Tempoh EOT',
            'name' => 'period',
            'id' => 'period'
        ])
    </div>

    <div class="col-2">
        @include('components.forms.select', [
            'input_label' => __('Jenis Tempoh'),
            'options' => period_types()->pluck('name', 'id'),
            'id' => 'period_type_id',
            'name' => 'period_type_id',
        ])
    </div>
    <div class="col-2">
        @include('components.forms.input', [
            'input_label' => 'Tarikh Siap Lanjutan',
            'name' => 'extended_end_date',
            'id' => 'extended_end_date',
            'readonly' => 'readonly'
        ])
    </div>
    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
        <div class="col-3">
            <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Bahagian</label><br>
            <div class="selectgroup w-25 grade-container">
                <label class="selectgroup-item">
                <input type="radio" id="category1" name="section" value="1" class="selectgroup-input">
                    <span class="selectgroup-button">{{ __('Keseluruhan') }}</span>
                </label>
                <label class="selectgroup-item">
                <input type="radio" id="category2" name="section" value="2" class="selectgroup-input">
                    <span class="selectgroup-button">{{ __('Sebahagian') }}</span>
                </label>
            </div>
        </div>
    @endif
    
</div>
<div class="row">
    <div class="col-4">
        @include('components.forms.hidden', [
            'id' => 'original_end_date',
            'name' => 'original_end_date',
            'value' => '',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => 'readonly'
        ])
    </div>
</div>

{{-- table --}}
<div class="row">
    <div class="col-md-12">
        @if(!isset($eot->eot_approve->id))
            <div class="float-right">
                <button type="button" id="addDetail href="#" class="btn btn-primary addmore">
                    @icon('fe fe-plus')
                    {{ __('Tambah Sebab') }}
                </button>
            </div>
        @endif
        <div class="form-group">
                <div id="tableJKPembuka" class="table table-responsive">
                    <table class="table table-sm table-transparent">
                        <tr>
                            <th width="45%">Sebab Sebab</th>
                            <th width="15%">Klausa</th>
                            <th width="35%">Tempoh Kelambatan Dan Lanjutan Masa</th>
                            @if(!isset($eot->eot_approve->id))
                                <th width="5%"><b><span class="addmore">HAPUS</span></b></th>
                            @endif
                        </tr>

                        @foreach($eot->eot_details as $details)
                            <tr>
                                <td>
                                    <input type="hidden" name="exist[{{$details->id}}][id]" id="exist_id" value="{{$details->id}}">
                                    <input type="text" class="form-control" name="exist[{{$details->id}}][reason]" id="exist_reason" value="{{ $details->reasons }}" @if(isset($eot->eot_approve->id)) readonly="true" @endif>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="exist[{{$details->id}}][clauses]" id="exist_clauses" value="{{ $details->clause }}" @if(isset($eot->eot_approve->id)) readonly="true" @endif>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="text" class="form-control" name="exist[{{$details->id}}][period]" id="exist_period" value="{{ $details->periods }}" @if(isset($eot->eot_approve->id)) readonly="true" @endif>
                                        </div>
                                        <div class="col-8">
                                            <select class="select2 form-control" id="exist_period_type" name="exist[{{$details->id}}][period_type]">
                                                <option value=""></option>
                                                @foreach(period_types() as $peri)
                                                    <option value="{{$peri->id}}" @if ($peri->id == old('period_type_id', $details->period_type_id))
                                                    selected="selected"
                                                    @endif @if(isset($eot->eot_approve->id)) readonly="true" @endif>{{$peri->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>
                                @if(!isset($eot->eot_approve->id))
                                    <td>
                                        <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        <tr id="klon0" class="d-none">
                            <td><input type="text" id="reason_0" name="baru[0][reason]" class="form-control" style="width:100%;"></td>
                            <td><input type="text" id="clauses_0" name="baru[0][clauses]" class="form-control" style="width:100%;"></td>
                            <td>
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="form-control" name="baru[0][period]" id="period_0" value="">
                                    </div>
                                    <div class="col-8" style="margin-top: -22.5px">
                                        @include('components.forms.select', [
                                            'input_label' => __(''),
                                            'options' => period_types()->pluck('name', 'id'),
                                            'id' => 'period_type_0',
                                            'name' => 'baru[0][period_type]',
                                        ])
                                    </div>
                                </div>
                            </td>
                            @if(!isset($eot->eot_approve->id))
                                <td>
                                    <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                </td>
                            @endif
                        </tr>
                   </table>
                </div>
        </div>
    </div>
</div>
{{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
<div class="btn-group float-right">
    @if(!isset($eot->eot_approve->id))
            <a href="{{ route('extension.extension_edit.index', ['hashslug' => $appointed_company->hashslug]) }}" 
                    class="btn btn-default border-primary">
                    {{ __('Kembali') }}
            </a>
            <button type="submit" class="btn btn-primary update-action-btn">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        </form>
    @endif
    @role('penyedia')
        @if(!empty($eot) && $eot->user_id == user()->id)
            @if(empty($review) || empty($review->status))
                <form method="POST" action="{{ route('acquisition.review.store') }}">
                    @csrf
                    <input id="sentStatus" id="sentStatus" name="sentStatus" type="hidden" value="1" />
                    <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $eot->sst->acquisition_id }}" hidden>
                    <input type="text" id="eot_id" name="eot_id" value="{{ $eot->id }}" hidden>

                    <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                    <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                    <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                    <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                    <input type="hidden" id="progress" name="progress" value="0">
                    <input type="hidden" id="status" name="status" value="S1">
                    <input type="hidden" id="department" name="department" value="PELAKSANA">

                    @if(!empty($review_previous))
                        <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                        <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                        <input type="text" id="type" name="type" value="EOT" hidden>
                    @endif
                    
                    <button type="button" id="send" class="btn btn-default float-middle border-default ">
                        @icon('fe fe-send') {{ __('Teratur') }}
                    </button>
                </form>
            @endif
        @endif
    @endrole
</div>
@endif --}}
