@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    @include('components.forms.assets.datetimepicker')
	<script>
        jQuery(document).ready(function($) {

            // document Sokongan upload start

            var tus = $('#tblUploadSokongan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counterSokongan = 1;

            $('#addRowSokongan').on( 'click', function () {
                tus.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFileSokongan'+ counterSokongan +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFileSokongan'+ counterSokongan +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileSokongan" id="uploadFileSokongan'+ counterSokongan +'" name="uploadFileSokongan[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterSokongan +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterSokongan +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterSokongan++;
            } );

            // Automatically add a first row of data
            $('#addRowSokongan').click();

            $("#tblUploadSokongan").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    tus.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUploadSokongan").on('change','.uploadFileSokongan',function(){
                var no = $(this).data('counter');
                $('#subFileSokongan' + no).val($(this).val().split('\\').pop());
            });

            // document Sokongan upload end

            // document Laporan upload start

            var tul = $('#tblUploadLaporan').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counterLaporan = 1;

            $('#addRowLaporan').on( 'click', function () {
                tul.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFileLaporan'+ counterLaporan +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFileLaporan'+ counterLaporan +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileLaporan" id="uploadFileLaporan'+ counterLaporan +'" name="uploadFileLaporan[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterLaporan +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterLaporan +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterLaporan++;
            } );

            // Automatically add a first row of data
            $('#addRowLaporan').click();

            $("#tblUploadLaporan").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    tul.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });


            $("#tblUploadLaporan").on('change','.uploadFileLaporan',function(){
                var no = $(this).data('counter');
                $('#subFileLaporan' + no).val($(this).val().split('\\').pop());
            });

            // document Laporan upload end

            // document Kertas upload start

            var tuk = $('#tblUploadKertas').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counterKertas = 1;

            $('#addRowKertas').on( 'click', function () {
                tuk.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFileKertas'+ counterKertas +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFileKertas'+ counterKertas +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileKertas" id="uploadFileKertas'+ counterKertas +'" name="uploadFileKertas[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterKertas +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterKertas +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterKertas++;
            } );

            // Automatically add a first row of data
            $('#addRowKertas').click();

            $("#tblUploadKertas").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    tuk.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUploadKertas").on('change','.uploadFileKertas',function(){
                var no = $(this).data('counter');
                $('#subFileKertas' + no).val($(this).val().split('\\').pop());
            });

            // document Kertas upload end

            $(".addmore").on('click',function(){
                var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
                  $klon.find('#reason_'+currnum).attr('id','reason_'+num).attr('name','baru['+num+'][reason]').val('');
                  $klon.find('#clauses_'+currnum).attr('id','clauses_'+num).attr('name','baru['+num+'][clauses]').val('');
                  $klon.find('#period_'+currnum).attr('id','period_'+num).attr('name','baru['+num+'][period]').val('');
                  $klon.find('#period_type_'+currnum).attr('id','period_type_'+num).attr('name','baru['+num+'][period_type]').val('');
                $('.addmore').parents('table').append($klon);
                console.log(currnum);
            });
            $('.buang').click(function () {
                $(this).parents('tr').detach();
            });
            
            $( "#bil" ).val('{{ $bil }}');
            @if(isset($sst->end_working_date))
                $( "#original_end_date" ).val('{{ $latest_end_date }}');
            @endif

            $('#period_type_id').change(function() {
                var originalEndDate, endDate;
                if($("#period").val() != undefined){
                    // alert($("#period").val());
                    
                    originalEndDate = moment($("#original_end_date").val(), "DD/MM/YYYY");
                    
                    if($("#period_type_id").val() == 1){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'days');
                    }else if($("#period_type_id").val() == 2){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'weeks');
                    }else if($("#period_type_id").val() == 3){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'months');
                    }else if($("#period_type_id").val() == 4){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'years');
                    }

                    $("#extended_end_date").val(endDate.format("DD/MM/YYYY"));
                }
            });

            $('#period').keyup(function() {
                var originalEndDate, endDate;
                if($("#period_type_id").val() != undefined){
                    // alert($("#period_type_id").val());
                    originalEndDate = moment($("#original_end_date").val(), "DD/MM/YYYY");
                    
                    if($("#period_type_id").val() == 1){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'days');
                    }else if($("#period_type_id").val() == 2){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'weeks');
                    }else if($("#period_type_id").val() == 3){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'months');
                    }else if($("#period_type_id").val() == 4){
                        endDate = moment((originalEndDate)).add($( "#period" ).val(), 'years');
                    }
                    
                    // alert(endDate);
                    if(endDate != undefined){
                        $("#extended_end_date").val(endDate.format("DD/MM/YYYY"));
                    }
                }
            });

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
            	// var id = '{{ $appointed_company->id }}';
                var id = $(this).data('hashslug');
                var route_name = 'api.contract.extension.store';
                var form_id = 'extension_form';
                // var data = $('#' + form_id).serialize();
                var form = document.forms[form_id];
                var data = new FormData(form);
                axios.post(route(route_name, id), data).then(response => {
                    swal('Pelanjutan Masa',  response.data.message, 'success');
                    // window.location.replace('/extension/extension_edit/'+id);
                    redirect(route('extension.extension_edit.edit', response.data.hashslug));
                });
            });
        });
	</script>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        @component('components.pages.title-sub')
            @slot('title_sub_content')
                <span class="font-weight-bold">Tajuk Perolehan : </span>{{ $sst->acquisition->title }}
                <br>
                <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Nilai Kontrak : </span>{{ money()->toHuman($appointed_company->offered_price) }}
                <br>
                <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ $sst->start_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tempoh : </span>{{ $sst->period.' '.$sst->period_type->name }}&nbsp;&nbsp;&nbsp;
                <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ $sst->end_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
                @if(!empty($new_eot_date))
                    <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
                @endif
            @endslot
        @endcomponent
    </div>
</div>
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#extension" role="tab" aria-controls="extension" aria-selected="false">
                    @if($appointed_company->acquisition->approval->acquisition_type_id == '2')
                        @icon('fe fe-file')&nbsp;{{ __('Perakuan Kelambatan dan Pelanjutan Masa') }}
                    @else
                        @icon('fe fe-file')&nbsp;{{ __('Pelanjutan Tempoh Kontrak') }}
                    @endif
                </a>
            </li>
            {{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3'|| $appointed_company->acquisition->approval->acquisition_type_id == '4')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#extension-upload" role="tab" aria-controls="extension-upload" aria-selected="false">
                        @icon('fe fe-upload')&nbsp;{{ __('Muat Naik') }}
                    </a>
                </li>
            @endif --}}
        </ul>
    </div>
    <div class="col-10">
    	<form id="extension_form" files="true" enctype="multipart/form-data" method="post">
            @csrf
	        @component('components.card')
	            @slot('card_body')
	                @component('components.tab.container', ['id' => 'document'])
	                    @slot('tabs')
	                     	@component('components.tab.content', ['id' => 'extension', 'active' => true])
	                            @slot('content')
	                                @include('contract.post.extension.partials.forms.extension', [])
                                    {{-- @if($appointed_company->acquisition->approval->acquisition_type_id == '1' || $appointed_company->acquisition->approval->acquisition_type_id == '3'|| $appointed_company->acquisition->approval->acquisition_type_id == '4') --}}
                                        @include('contract.post.extension.partials.forms.upload', [])
                                    {{-- @endif --}}
	                            @endslot
	                        @endcomponent
                            {{-- @component('components.tab.content', ['id' => 'extension-upload'])
                                @slot('content')
                                @endslot
                            @endcomponent --}}
	                    @endslot
	                @endcomponent
	                @slot('card_footer')
                        <input type="hidden" name="appointed_company_id" id="appointed_company_id" value="{{ $appointed_company->id }}">
                        <input type="hidden" name="department_id" id="department_id" value="{{ $sst->department_id }}">
	                    <div class="btn-group float-right">
	                        <a href="{{ route('extension.extension.index') }}" 
	                                class="btn btn-default border-primary">
	                                {{ __('Kembali') }}
	                        </a>
	                        <button type="submit" class="btn btn-primary submit-action-btn">
								@icon('fe fe-save') {{ __('Simpan') }}
							</button>
	                    </div>
	                @endslot
	            @endslot
	        @endcomponent
    	</form>
    </div>
</div>
@endsection