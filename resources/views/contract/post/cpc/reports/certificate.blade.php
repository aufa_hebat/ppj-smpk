<html>
    <head>
        <style type="text/css">
            @page {
                margin: 70px 70px 70px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }    

            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
            
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }  

            .content{
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
			}
        </style>
    </head>
    <body>
        <div style="text-align: right;" class="title-header">
            <span>CPC/BPUB/2017</span>
        </div>
        <div>
            <center><img style="width: 70px; height: 70px;" src="{{ logo($print) }}"></center>
        </div>
        <div style="text-align: center; padding: 20px 20px; font-weight: bold; text-transform: uppercase; ">
            <span class="content">Perbadanan Putrajaya</span><br/>
             @if(strpos($appointed->acquisition->approval->type->name, 'Penyelenggaraan') !== false)
             <span class="title">Sijil Siap Kerja Penyelenggaraan</span>
             @elseif($appointed->acquisition->approval->type->name == 'Perkhidmatan')     
            <span class="title">Sijil Siap Perkhidmatan</span>
             @elseif($appointed->acquisition->approval->type->name == 'Bekalan')
             <span class="title">Sijil Siap Pembekalan</span>          
             @else
             <span class="title">Sijil Siap Kerja</span>
             @endif
        </div>

        <div style=" padding: 0px 20px">
            <table width="100%" class="content">
                <tr>
                    <td width="65%">&nbsp;</td>
                    <td width="35%" style="text-align: left;">
                        <span>
                            Perbadanan Putrajaya<br/>
                            Kompleks Perbadanan Putrajaya<br/>
                            24, Persiaran Perdana, Presint 3<br/>
                            62675 Putrajaya<br/>
                            Wilayah Persekutuan Putrajaya<br/><br/>
                            
                            @php
                    $month = date('F', strtotime(\Carbon\Carbon::now()));
                    $bulan = common()->getBulanInMalay($month);
                    $year = date('Y', strtotime(\Carbon\Carbon::now()));
                @endphp

            
                            
                            
                            Tarikh : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $bulan . ' ' . $year }}
                        </span>
                    </td>
                </tr>
                 <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td>
                        <span style="font-weight: bold; text-transform: uppercase;">
                            {{ $sst->company->company_name }}<br/>
                        </span>
                        <span>
                        @if(!empty($sst->company->addresses[0]))
                            @if(!empty($sst->company->addresses[0]->primary))
                                <span>{{ $sst->company->addresses[0]->primary }}</span><br />
                            @endif
                            @if(!empty($sst->company->addresses[0]->secondary))
                                <span>{{ $sst->company->addresses[0]->secondary }}</span><br />
                            @endif
                            @if(!empty($sst->company->addresses[0]->postcode))
                                <span>{{ $sst->company->addresses[0]->postcode }}, </span>
                            @endif
                            @if(!empty($sst->company->addresses[0]->state))
                            <span style="text-transform: uppercase;">{{ $sst->company->addresses[0]->state }}</span>                                    
                            @endif
                        @endif  
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        
        <div style=" padding: 10px 20px">
            <table width="100%" class="content">             
                               <tr>
                    <td width="150px" valign="top">Berdaftar dengan</td>
                    <td width="2px"valign="top"> : </td>
                     @if($appointed->acquisition->approval->type->name == 'Perkhidmatan' || $appointed->acquisition->approval->type->name == 'Bekalan')
                     <td valign="top"> <span style="font-weight: bold; text-transform: uppercase; text-align: justify;">KEM. KEWANGAN MALAYSIA ({{ $mof }})</span></td>
                     @else
                    <td valign="top"> <span style="font-weight: bold; text-transform: uppercase; text-align: justify;">{{ $cidb }}</span></td>
                    @endif
                </tr>
                <tr>
                    <td valign="top">No. Kontrak</td>
                    <td valign="top"> : </td>
                    <td valign="top"><span style="font-weight: bold;">{{ $sst->acquisition->reference }}</span></td>
                </tr>
                <tr>
                    <td valign="top">Kontrak Untuk</td>
                    <td valign="top"> : </td>
                    <td valign="top">  <span style="font-weight: bold; text-transform: uppercase; text-align: justify; line-height: 1.6">
                            {{ $sst->acquisition->title }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><hr/></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: justify;">
                        
                        <!-- Siap kerja penyelengaraan lain sket -->
                         @if($appointed->acquisition->approval->type->name == 'Penyelenggaraan' || $appointed->acquisition->approval->type->name == 'Perkhidmatan' || $appointed->acquisition->approval->type->name == 'Bekalan')
                         <span style="text-align: justify; line-height: 1.6">Menurut <b>Klausa  {{ $cpc->clause_1 }}</b> 
                            
                            Syarat-Syarat 
                            
                            @if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga'))
                                <b>Sebut Harga</b>
                            @else
                                <b>Kontrak</b>
                            @endif
                            
                          
                             
                          
                        maka adalah dengan ini diperakui bahawa segala pembaikian, penggantian, pembekalan 
                        berkaitan dengan kerja-kerja penyelengaraan dalam kontrak yang tersebut di atas,
                        telah disempurnakan pada <span style="font-weight: bold;">{{ $cpc->test_award_date ? locale_date($cpc->test_award_date) : "-"  }} </span>.
                        
                       
                        </span>
                        
                        
                        
                        @else
                        <span style="text-align: justify; line-height: 1.6">Menurut <b>Klausa  {{ $cpc->clause_1 }}</b> 
                            
                            Syarat-Syarat 
                            
                            @if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga'))
                                <b>Sebut Harga</b>
                            @else
                                <b>Kontrak</b>
                            @endif
                            
                          
                        @if($appointed->acquisition->approval->type->name == 'Perkhidmatan' || $appointed->acquisition->approval->type->name == 'Bekalan')       
                          
                        maka adalah dengan ini diperakui bahawa segala perkhidmatan, pengurusan, pembekalan 
                        berkaitan dengan kerja-kerja perkhidmatan dalam kontrak tersebut di atas,
                        telah disempurnakan pada <span style="font-weight: bold;">{{ isset($cpc->dlp_expiry_date) ? locale_date($cpc->dlp_expiry_date) : "-" }} </span>.
                        
                        @else
                        
                         dan tertakluk kepada penyiapan apa-apa kerja yang belum disiapkan dan pembaikan apa-apa kecacatan, ketidaksempurnaan, kekusutan 
                            atau apa-apa kerosakan lain apa jua pun sebagaimana dikehendaki di bawah <b>Klausa {{ $cpc->clause_2 }}</b> Syarat-Syarat
                            
                            @if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga'))
                                <b>Sebut Harga</b>
                            @else
                                <b>Kontrak</b>
                            @endif
                            
                            
                            dan yang mungkin terzahir dalam tempoh Tanggunggan Kecacatan maka
                        adalah dengan ini diperakui bahawa seluruh Kerja-Kerja seperti yang tersebut di atas telah siap dengan 
                        memuaskan hati pada <span style="font-weight: bold;">{{ $cpc->test_award_date ? locale_date($cpc->test_award_date) : "-"  }}</span> dan diambil milik pada 
                        <span style="font-weight: bold;">{{ isset($cpc->dlp_start_date) ? locale_date($cpc->dlp_start_date) : "-" }}</span> dan dengan itu Tempoh Tanggungan kecacatan untuk Kerja-Kerja tersebut
                        bermula pada <span style="font-weight: bold;">{{ isset($cpc->dlp_start_date) ? locale_date($cpc->dlp_start_date) : "-" }} </span>
                        dan akan berakhir pada <span style="font-weight: bold;">{{ isset($cpc->dlp_expiry_date) ? locale_date($cpc->dlp_expiry_date) : "-" }} </span>.
                        </span>
                        
                        @endif
                       
                         
                      @endif  
                    </td>
                    <!-- END -->
                </tr>
            </table>

            <br><br>
        <br><br>
        <br><br>

            <table width="100%" class="content">
                  <tr>
                    <td width="40%">....................................................................<br></td>
                    <td width="10%"></td>
                    <td width="40%">...............................................................<br></td>
                </tr>
                <tr>
                    <td valign="top">
                        Tandatangan Penolong Wakil Perbadanan yang dilantik
                    </td>
                    <td ></td>
                    <td >
                        Tandatangan Pegawai yang diwakilkan bertindak
                        untuk dan bagi pihak
                        <span style="text-transform: uppercase; font-weight: bold;">Perbadanan Putrajaya</span>
                    </td>
                </tr>
                <tr><td colspan="3"><br><br><br><br></td></tr>
                <tr>
                    <td>Tarikh : _______________________</td>
                    <td></td>
                    <td>Tarikh : _______________________</td>
                </tr>
            </table>

        </div>
    </body>
</html>