@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(!empty($cpc))
                $('#cpc_dlp_start_date').html('{{ $cpc->dlp_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cpc->dlp_start_date)->format('d/m/Y') : null }}');
                $('#cpc_dlp_expiry_date').html('{{ $cpc->dlp_expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cpc->dlp_expiry_date)->format('d/m/Y') : null }}');
                $('#cpc_signature_date').html('{{ $cpc->signature_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cpc->signature_date)->format('d/m/Y') : null }}');
                $('#cpc_test_award_date').html('{{ $cpc->test_award_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cpc->test_award_date)->format('d/m/Y') : null }}');
                $('#cpc_clause_1').html('{{ $cpc->clause_1 }}');
                $('#cpc_clause_2').html('{{ $cpc->clause_2 }}');

                // UPLOADS
                    @if($cpc->documents->count() > 0)
                        var cpc_t = $('#cpc_tblUpload').DataTable({
                                searching: false,
                                ordering: false,
                                paging: false,
                                info:false
                            });

                        var cpo_counter = 1;
                        @foreach($cpc->documents as $doc)
                            cpc_t.row.add( [
                                cpo_counter,
                                '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                            ] ).draw( false );

                            cpo_counter++;
                        @endforeach
                    @endif
                // UPLOADS END
            @endif
        });

    </script>
@endpush

<h4>Sijil Siap Kerja</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="btn-group float-right">
            <a target="_blank" href="{{ route('cpcCertificate', ['hashslug'=>$sst->hashslug] ) }}"
               class="btn btn-success">
                @icon('fe fe-printer') {{ __('Cetak') }}
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Mula Membaiki Kecacatan') }}</div>
                    <div id="cpc_dlp_start_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Setuju Terima') }}</div>
                    <div id="cpc_clause_1"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Sijil Di tandatangan') }}</div>
                    <div id="cpc_signature_date"></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Siap Membaiki Kecacatan') }}</div>
                    <div id="cpc_dlp_expiry_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Denda') }}</div>
                    <div id="cpc_clause_2"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Pengujian Dan Pentauliah') }}</div>
                    <div id="cpc_test_award_date"></div>
                </div>
            </div>
        </div>
        @include('contract.post.cpc.partials.shows.uploads')
        @include('contract.post.partials.shows.return')
    </div>


</div>