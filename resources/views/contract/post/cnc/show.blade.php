@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(!empty($cnc))
                $('#cnc_reference_no').html('{{ $cnc->reference_no }}');
                $('#cnc_signature_date').html('{{ $cnc->signature_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cnc->signature_date)->format('d/m/Y') : '' }}');
                $('#cnc_test_award_date').html('{{ $cnc->test_award_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$cnc->test_award_date)->format('d/m/Y') : '' }}');
                $('#cnc_clause_1').html('{{ $cnc->clause_1 }}');
                $('#cnc_clause_2').html('{{ $cnc->clause_2 }}');

                $('#cnc_assessment_date').html('{{ $cnc->assessment_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$cnc->assessment_date)->format('d/m/Y') : '' }}');
                $('#cnc_lad_per_day').html('{{ $cnc->lad_per_day     ? money()->toHuman($cnc->lad_per_day   ) : 'RM 0.00' }}');

                // UPLOADS
                @if($cnc->documents->count() > 0)
                    var cnc_t = $('#cnc_tblUpload').DataTable({
                            searching: false,
                            ordering: false,
                            paging: false,
                            info:false
                        });

                    var cno_counter = 1;
                    @foreach($cnc->documents as $doc)
                        cnc_t.row.add( [
                            cno_counter,
                            '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                        ] ).draw( false );

                        cno_counter++;
                    @endforeach
                @endif
                // UPLOADS END
            @endif
        });

    </script>
@endpush

<h4>Sijil Tidak Siap Kerja</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="btn-group float-right">
            <a target="_blank" href="{{ route('cncCertificate', ['hashslug'=>$sst->hashslug] ) }}"
               class="btn btn-success">
                @icon('fe fe-printer') {{ __('Cetak') }}
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('No Rujukan Fail') }}</div>
                    <div id="cnc_reference_no"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Surat Setuju Terima') }}</div>
                    <div id="cnc_clause_1"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Sijil Di tandatangan') }}</div>
                    <div id="cnc_signature_date"></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Pengujian Dan Pentauliah') }}</div>
                    <div id="cnc_test_award_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Denda (Rujuk Dokumen Kontrak)') }}</div>
                    <div id="cnc_clause_2"></div>
                </div>
            </div>
        </div>

        @include('contract.post.cnc.partials.shows.lad')
        @include('contract.post.cnc.partials.shows.uploads')

        @include('contract.post.partials.shows.return')
    </div>
</div>