<hr>
<h4>Maklumat LAD</h4>
<div class="row">
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Penilaian'),
            'id' => 'assessment_date',
            'name' => 'assessment_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
        {{--@include('components.forms.input', [
            'input_label' => __('Kadar Asas Pinjaman'),
            'id' => 'blr_rate',
            'name' => 'blr_rate',
            'readonly' => true,
            'type'=>'hidden',
        ])--}}
        <input type="hidden" id="blr_rate" name="blr_rate">
        @include('components.forms.input', [
            'input_label' => __('Nilai LAD / Penalti'),
            'id' => 'lad_amount',
            'name' => 'lad_amount',
            'readonly' => true,
        ])
        {{-- <input type="hidden" id="days_passed" name="days_passed"> --}}
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Bil Hari Lewat'),
            'id' => 'days_passed',
            'name' => 'days_passed',
            'readonly' => true,
        ])
        {{-- <input type="hidden" id="days_passed" name="days_passed"> --}}
        @include('components.forms.input', [
            'input_label' => __('LAD Sehari (RM)'),
            'id' => 'lad_per_day',
            'name' => 'lad_per_day',
            'readonly' => true,
        ])
    </div>
</div>