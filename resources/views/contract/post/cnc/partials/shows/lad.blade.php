<hr>
<h4>Maklumat LAD</h4>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Penilaian') }}</div>
            <div id="cnc_assessment_date"></div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('LAD Sehari') }}</div>
            <div id="cnc_lad_per_day"></div>
        </div>
    </div>
</div>