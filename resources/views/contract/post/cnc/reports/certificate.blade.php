<html>
    <head>
        <style type="text/css">
            @page {
                margin: 70px 70px 70px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }    

            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
            
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }  

            .content{
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
        </style>
    </head>
    <body>
        <div>
            <table width="100%" class="content">
                <tr>
                    <td >Rujukan kami : {{ $cnc->reference_no }}</td>
                    <td align='right'>Tarikh : {{ locale_date(\Carbon\Carbon::now()) }}</td>
                </tr>
            </table>
        </div>
        <div>
            <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>
        </div>
        <div style="text-align:center; text-transform: uppercase;">
            <span class="title" style="font-weight: bold;">Sijil Tidak Siap Kerja</span><br>
            <span class="title" style="font-style: italic; ">(Certificate of Non-Completion)
        </div>
        <br/>
        <div style="text-align: left;" class="content">
            <span>{{ $sst->company->company_name}} </span><br />
            @if(!empty($sst->company->addresses[0]))
                @if(!empty($sst->company->addresses[0]->primary))
                    <span>{{ $sst->company->addresses[0]->primary }}</span><br />
                @endif
                @if(!empty($sst->company->addresses[0]->secondary))
                    <span>{{ $sst->company->addresses[0]->secondary }}</span><br />
                @endif
                @if(!empty($sst->company->addresses[0]->postcode))
                    <span>{{ $sst->company->addresses[0]->postcode }}, </span>
                @endif
                @if(!empty($sst->company->addresses[0]->state))
                    <span style="font-weight: bold; text-transform: uppercase;">{{ $sst->company->addresses[0]->state }}</span>                                    
                @endif
            @endif        
        </div>
        <br/>
        <div class="content">
            <span>Tuan,</span><br/><br/>
            @if($sst->acquisition->category->name == 'Sebut Harga' || $sst->acquisition->category->name == 'Sebut Harga B')
             <span style="text-transform: uppercase;">No. Sebut Harga : </span>
            @else
             <span style="text-transform: uppercase;">No. Kontrak : </span>
            @endif
            
           
            <span style="font-weight: bold;">{{ $sst->acquisition->reference }}</span>
            <br/><br/>    
            <div style="text-align: justify;line-height: 1.6;">
                <span style="font-weight: bold;">Kontrak untuk : </span>
                <span style="font-weight: bold; text-transform: uppercase;">{{ $sst->acquisition->title }}</span>
            </div>
            <hr/>
            <div style="text-align: justify; line-height: 1.6;">
                <span style="text-align: justify">
                    Dengan ini diperakukan bahawa tuan telah gagal menyiapkan kerja-kerja yang tersebut di atas pada 
                    <span style="font-weight: bold;">"Tarikh Siap" </span>
                    yang dinyatakan dalam Surat Setuju Terima di bawah <b>Para {{ $cnc->clause_1 }}</b> iaitu pada 
                    <span style="font-weight: bold;"> {{ isset($sst->end_working_date) ? locale_date($sst->end_working_date) : "-" }} </span>
                    @if($sst->eots->count()>0)
                    <span>
                        ataupun dalam Lanjutan Tempoh Masa yang telah dibenarkan di bawah
                        <b>
                        @if($sst->acquisition->approval->acquisition_type_id == config('enums.Kerja'))
                            @if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga'))
                                {{ 'Para 7' }}
                            @else
                                {{ 'Para 43' }}
                            @endif
                        @endif
                        </b>
                        Syarat-Syarat {{ $sst->acquisition->category->name }} iaitu pada <b>{{ locale_date($sst->eots->sortByDesc('id')->first()->extended_end_date) }}</b>
                    </span>
                    @endif
                    dan mengikut pendapat saya kerja-kerja tersebut itu sepatutnya telah disiapkan pada tarikh ini.
                </span>
                <br/><br/>
                <span style="text-align: justify">
                    Menurut <span style="font-weight: bold;">Klausa {{ $cnc->clause_2 }}</span>, tuan adalah dengan ini diberitahu bahawa tuan kenalah
                    membayar atau membenarkan kepada <span style="font-weight: bold; text-transform: uppercase;">Perbadanan Putrajaya </span>
                    sejumlah wang yang dikira atas kadar yang dinyatakan dalam Syarat-Syarat {{ $sst->acquisition->category->name }}, iaitu 
                    <span style="font-weight: bold; text-transform: uppercase;">RM {{money()->toCommon($sst->document_LAD_amount ?? 0, 2)}} </span> setiap hari sebagai Gantirugi yang ditentukan 
                    dan ditetapkan banyaknya sepanjang tempoh kerja-kerja tersebut itu tidak disiapkan sepenuhnya dan saya akan memperakukan
                    supaya potongan sewajarnya dibuat dari apa-apa wang yang kena dibayar atau yang akan kena dibayar kepada tuan di bawah Kontrak ini.
                </span>
            </div>
            <br/><br/><br/><br/><br/><br/>
            <table width="100%" class="content">
                <tr>
                    <td width="40%">------------------------------------------------------------</td>
                    <td width="10%"></td>
                    <td width="40%">------------------------------------------------------------</td>
                </tr>
                <tr>
                    <td valign="top">
                        Tandatangan Penolong Wakil Perbadanan<br/>
                        Yang Dilantik
                    </td>
                    <td width="10%"></td>
                    <td valign="top">
                        Tandatangan pegawai yang diwakilkan<br/>
                        bertindak untuk dan bagi pihak 
                        <span style="text-transform: uppercase;">Perbadanan Putrajaya</span>
                    </td>
                </tr>
                <tr><td colspan="3"><br/><br/><br/></td></tr>
                <tr>
                    <td>Tarikh : ______________________________</td>
                    <td></td>
                    <td>Tarikh : ______________________________</td>
                </tr>
            </table>
        </div>
    </body>
</html>
