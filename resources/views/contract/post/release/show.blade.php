@extends('layouts.admin')
@push('scripts')
<script>
    jQuery(document).ready(function($) {

    });
</script>

@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk Perolehan : </span>{{ $sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nilai Kontrak : </span>{{ money()->toHuman($appointed->offered_price) }}
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ $sst->start_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $sst->period.' '.$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if($sst->acquisition->approval->acquisition_type_id != 1 && $sst->acquisition->approval->acquisition_type_id != 3)
                <span class="font-weight-bold">Tempoh DLP : </span>{{ $sst->defects_liability_period." Bulan" }}&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ $sst->end_working_date->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;
            @if(!empty($new_eot_date))
                <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
            @endif
        @endslot
    @endcomponent

    @component('components.card')
        @slot('card_body')
            <table width="100%">
                <tr>
                    <td><h4>Pelepasan Bon</h4></td>
                    {{-- <td width="10px">
                        @if(!$deposit_has_reviewed)
                        <label class="custom-switch">
                                <input type="checkbox" class="custom-switch-input" id="deposit_cbBox" name="deposit_cbBox" value="0">
                        <span class="custom-switch-indicator"></span>
                        <span class="custom-switch-description">Aktif</span>
                        </label>
                        @endif
                    </td> --}}
                </tr>
            </table><br>
            {{-- <div class="row justify-content-center">
                <div class="col">
                    <h5>Senarai Bon</h5>
                    @component('components.card')
                        @slot('card_body')
                            @component('components.datatable', 
                                [
                                    'table_id' => 'acceptance-letter',
                                    'route_name' => 'api.datatable.contract.release-bon',
                                    'param' => 'sst_id=' . $sst->id,
                                    'columns' => [
                                        ['data' => 'bil', 'title' => __('Bil'), 'defaultContent' => '-'],
                                        ['data' => 'bank_no', 'title' => __('No. Bon'), 'defaultContent' => '-'],
                                        ['data' => 'bank_start_date', 'title' => __('Tarikh Mula'), 'defaultContent' => '-'],
                                        ['data' => 'bank_expired_date', 'title' => __('Tarikh Luput'), 'defaultContent' => '-'],
                                        ['data' => 'money_amount', 'title' => __('Nilai Jaminan'), 'defaultContent' => '-'],
                                        ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                    ],
                                    'headers' => [
                                        __('Bil'),__('No. Bon'), __('Tarikh Mula'), __('Tarikh Luput'), __('Nilai Jaminan'), __('')
                                    ],
                                    'actions' => ''
                                ]
                            )
                            @endcomponent
                        @endslot
                    @endcomponent
                </div>
            </div> --}}
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-sm table-transparent table-hover" id="tblCert" style="width: 100%">
                            <thead>
                                <tr>
                                    <td>BIL</td>
                                    <td>NO. BON</td>
                                    <td>TARIKH MULA</td>
                                    <td>TARIKH LUPUT</td>
                                    <td>NILAI JAMINAN</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bon as $k => $row)
                                <tr>
                                    <td>{{ $k + 1 }}</td>
                                    <td>{{ $row->bank_no }}</td>
                                    <td>{{ Carbon::createFromFormat('Y-m-d', $row->bank_start_date)->format('d/m/Y') }}</td>
                                    <td>{{ Carbon::createFromFormat('Y-m-d', $row->bank_expired_date)->format('d/m/Y') }}</td>
                                    <td>{{ money()->toHuman($row->money_amount) . ' ' }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        @if( $sst->acquisition->approval->acquisition_type_id == 2)
                        
                            {{-- kerja --}}
                            <table class="table table-vcenter text-nowrap" id="tblCert" style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td style="width: 18%">Tarikh CPC</td>
                                        <td style="width: 4%">:</td>
                                        <td style="width: 18%">
                                            @if(!empty($sst->cpc->signature_date))
                                                {!! date('d/m/Y', strtotime($sst->cpc->signature_date)) !!}
                                            @endif
                                        </td>
                                        <td style="width: 18%">Nilai Dilepaskan (RM)</td>
                                        <td style="width: 4%">:</td>
                                        <td style="width: 18%">
                                            @if(!empty($sst->cpc->signature_date))
                                                {{money()->toCommon($sst->bon->money_amount * (50/100) ?? 0,2 )}}
                                            @endif
                                        </td>

                                        {{-- <td style="width: 20%"><button type="button" href="#">Proses Bayaran</button></td> --}}
                                    </tr>
                                    <tr>
                                        <td style="width: 18%">Tarikh CMGD</td>
                                        <td style="width: 4%">:</td>
                                        <td style="width: 18%">
                                            @if(!empty($sst->cmgd->signature_date))
                                                {!! date('d/m/Y', strtotime($sst->cmgd->signature_date)) !!}
                                            @endif
                                        </td>
                                        <td style="width: 18%">Nilai Dilepaskan (RM)</td>
                                        <td style="width: 4%">:</td>
                                        <td style="width: 18%">
                                            @if(!empty($sst->cmgd->signature_date))
                                                {{money()->toCommon($sst->bon->money_amount * (50/100) ?? 0,2 )}}
                                            @endif
                                        </td>

                                        {{-- <td style="width: 20%"><button type="button" href="#">Proses Bayaran</button></td> --}}
                                    </tr>
                                </tbody>
                            </table>

                        @elseif( $sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 3|| $sst->acquisition->approval->acquisition_type_id == 4 )

                            {{-- bekalan / perkhidmatan --}}
                            <input type="text" name="list" value="{{ $sst->bon->money_amount }}" hidden>
                            <input type="hidden" name="sst_id" value="{{ $sst->id }}">
                            <table class="table table-vcenter text-nowrap" id="tblCert">
                                <tbody>
                                    <tr>
                                        <td style="width: 14%">Tarikh CPC</td>
                                        <td style="width: 2%">:</td>
                                        <td style="width: 14%">
                                            @if(!empty($sst->cpc->signature_date))
                                                {!! date('d/m/Y', strtotime($sst->cpc->signature_date)) !!}
                                            @endif
                                        </td>

                                        <td style="width: 14%">Peratus Nilai Dilepaskan (%)</td>
                                        <td style="width: 2%">:</td>
                                        <td style="width: 10%">{!! !empty($sst->release) ? $sst->release->percentage : null!!}</td>

                                        <td style="width: 14%">Nilai Dilepaskan (RM)</td>
                                        <td style="width: 2%">:</td>
                                        <td style="width: 18%">{!! !empty($sst->release) ? money()->toHuman($sst->release->amount) : null!!}</td>
                                        {{-- <td style="width: 10%"><button type="button" href="#">Proses Bayaran</button></td> --}}
                                    </tr>
                                </tbody>
                            </table>

                        @endif
                    </div>
                </div>
            </div>
        @endslot
    @endcomponent
@endsection

