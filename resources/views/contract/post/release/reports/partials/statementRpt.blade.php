<div style="text-align: center; text-transform: uppercase; text-decoration: underline;">
    <span>Penyata Pelaksanaan Harga Kontrak</span>
</div>
<br>
<div class="row">
	<div class="col-12">
		<table width="100%">
			<tr>
				<td width="25%" valign="top">Tajuk Kerja</td>
				<td width="5%" valign="top"> : </td>
				<td width="70%" style="text-weight: bold;">{{ $sst->acquisition->title }}</td>
			</tr>
			<tr>
				<td>No. Kontrak</td>
				<td> : </td>
				<td style="text-weight: bold;">
					{{ $sst->acquisition->reference }}
				</td>
			</tr>
			<tr>
				<td valign="top">Nama &amp; Alamat Kontraktor</td>
				<td valign="top"> : </td>
				<td>
					<span class="text-weight: bold;">{{ $sst->company->company_name }}</span><br>
					@if(!empty($sst->company->addresses[0]))
						@if(!empty($sst->company->addresses[0]->primary))
							{{ $sst->company->addresses[0]->primary }}<br>
						@endif
						@if(!empty($sst->company->addresses[0]->secondary))
							{{ $sst->company->addresses[0]->secondary }}<br>
						@endif
						@if(!empty($sst->company->addresses[0]->postcode))
							{{ $sst->company->addresses[0]->postcode }}, 
							{{ $sst->company->addresses[0]->state }}   
						@endif
					@endif
				</td>
			</tr>
			<tr>
				<td>Harga Asal Kontrak</td>
				<td> : </td>
				<td style="text-weight: bold;">
					{!! money()->toHuman($appointed->offered_price) !!}
				</td>
			</tr>
		</table>
	</div>	
</div>
<br/>
<div class="row">
    <table width="100%" style="border-collapse: collapse;padding: 4 4;">
        <tr>
            <td width="16%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Pelarasan Harga Kontrak No</span>
            </td>
            <td width="50%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Butiran Kerja</span>
            </td>
            <td width="17%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Potongan (RM)</span>
            </td>
            <td width="17%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Tambahan (RM)</span>
            </td>
        </tr>
        @php
            $total_sum_reduced = 0.0;
            $total_sum_additional = 0.0;
            $total_overal = 0.0;
        @endphp

        @foreach($vo as $index => $vod)

            @php
                $sum_reduced = 0.0;
                $sum_additional = 0.0;
                $collection = collect(['']);
                $butiran_kerja = '';

            @endphp

            @php
                foreach($vo_element as $index => $voe) {
                    if($voe->variationOrder->id == $vod->id){
                        if(! $collection->contains($voe->variationOrderType->name)){
                            $collection = $collection->concat([$voe->variationOrderType->name]);
                            
                            if($butiran_kerja == ''){
                                $butiran_kerja = $voe->variationOrderType->name;
                            }else{
                                $butiran_kerja = $butiran_kerja + ',' + $voe->variationOrderType->name;
                            }
                        }

                        $sum_reduced = $sum_reduced + $voe->estimated_reduced_amount; 
                        $sum_additional = $sum_additional + $voe->estimated_additional_amount; 

                        $sum_add_red    =   $sum_additional - $sum_reduced;
                    }
                }

                if($sum_additional > $sum_reduced){
                    $total_sum_additional = $total_sum_additional + $sum_add_red;
                }else{
                    $total_sum_reduced = $total_sum_reduced + $sum_add_red;
                }
                
                
                $total_overal = $total_sum_additional + $total_sum_reduced;
            @endphp

        <tr>
            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;">
                <span style="text-transform: uppercase;">
                    ppjhk No. {{ $vod->no }}
                </span>
            </td>
            <td  style="border-right: 1px solid #959594;">
                <span style="text-transform: uppercase;">
                    {{ $butiran_kerja }}
                </span>            
            </td>
            <td  style="border-right: 1px solid #959594; text-align: right;">
                @if($sum_reduced > $sum_additional)
                    {{ money()->toCommon($sum_add_red ?? "0" , 2) }}
                @endif
            </td>
            <td  style="border-right: 1px solid #959594; text-align: right;">
                @if($sum_reduced < $sum_additional)
                    {{ money()->toCommon($sum_add_red ?? "0" , 2) }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"><br/></td>
            <td  style="border-right: 1px solid #959594;"></td>
            <td  style="border-right: 1px solid #959594;"></td>
            <td  style="border-right: 1px solid #959594;"></td>            
        </tr>
        @endforeach


        <!-- @foreach($vo as $index => $vod)
            <tr>
                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;">
                    <span style="font-weight: bold; text-decoration: underline; text-transform: uppercase;">
                        ppjhk No. {{ $vod->no }}
                    </span>
                </td>
                <td  style="border-right: 1px solid #959594;"></td>
                <td  style="border-right: 1px solid #959594;"></td>
                <td  style="border-right: 1px solid #959594;"></td>
            </tr>
        
            @php
                $sum_reduced = 0.0;
                $sum_additional = 0.0;
                $collection = collect(['test'])
            @endphp

            @foreach($vo_element as $index => $voe) 
                @if($voe->variationOrder->id == $vod->id)

                    @if(! $collection->contains($voe->variationOrderType->name))
                        <tr>    
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"><br/></td>
                            <td style="border-right: 1px solid #959594;"></td>
                            <td style="border-right: 1px solid #959594;"></td>
                            <td style="border-right: 1px solid #959594;"></td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"></td>
                            <td style="border-right: 1px solid #959594;">
                                <span style="text-decoration: underline;">{{ $voe->variationOrderType->name }}</span>
                            </td>
                            <td style="border-right: 1px solid #959594;"></td>
                            <td style="border-right: 1px solid #959594;"></td>
                        </tr>
                        @php
                            $collection = $collection->concat([$voe->variationOrderType->name])
                        @endphp
                    @endif
                    <tr>
                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"></td>
                        <td style="border-right: 1px solid #959594;">
                            <span>{{ $voe->name }}</span>
                        </td>
                        <td style="border-right: 1px solid #959594; text-align: right;">
                            <span>{{ money()->toCommon($voe->estimated_reduced_amount ?? "0" , 2) }}&nbsp;&nbsp;</span>
                        </td>
                        <td style="border-right: 1px solid #959594; text-align: right; ">
                            <span>{{ money()->toCommon($voe->estimated_additional_amount ?? "0" , 2) }}&nbsp;&nbsp;</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"><br/></td>
                        <td style="border-right: 1px solid #959594;"></td>
                        <td style="border-right: 1px solid #959594;"></td>
                        <td style="border-right: 1px solid #959594;"></td>
                    </tr>
                    @php
                        $sum_reduced = $sum_reduced + $voe->estimated_reduced_amount; 
                        $sum_additional = $sum_additional + $voe->estimated_additional_amount; 
                    @endphp
                @endif
            @endforeach

            <tr>
                <td class="bordered"></td>
                <td class="bordered"> SUB-TOTAL</td>
                <td class="bordered" style="text-align: right;">{{ money()->toCommon($sum_reduced ?? "0" , 2) }}&nbsp;&nbsp;</td>
                <td class="bordered" style="text-align: right;">{{ money()->toCommon($sum_additional ?? "0" , 2) }}&nbsp;&nbsp;</td>
            </tr>        

        @endforeach-->
        <tr>
            <td class="bordered"></td>
            <td class="bordered">
                <span style="text-transform: uppercase;">Jumlah</span>
            </td>
            <td class="bordered" style="text-align: right;">
                {{ money()->toCommon($total_sum_reduced ?? "0" , 2) }}
            </td>
            <td class="bordered" style="text-align: right;">
                {{ money()->toCommon($total_sum_additional ?? "0" , 2) }}
            </td>
        </tr>
        <tr>
            <td class="bordered"></td>
            <td class="bordered">
                <span style="text-transform: uppercase;">Jumlah Bersih Tolakan/Tambahan</span>
            </td>
            <td class="bordered" style="text-align: right;">
                @if($total_sum_reduced > $total_sum_additional)
                    {{ money()->toCommon($total_overal ?? "0" , 2) }}
                @endif
            </td>
            <td class="bordered" style="text-align: right;">
                @if($total_sum_reduced < $total_sum_additional)
                    {{ money()->toCommon($total_overal ?? "0" , 2) }}
                @endif            
            </td>
        </tr>         
    </table>
</div>