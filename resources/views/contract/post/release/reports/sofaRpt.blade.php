<style type="text/css">
	div.breakNow { page-break-inside:avoid; page-break-after:always; }

	.bordered {
    	border-color: #959594;
    	border-style: solid;
    	border-width: 1px;
	}

	title{
		font-size: 16px;
	}
</style>

@include('contract.post.sofa.reports.partials.calculation', ['sst' => $sst])

@include('contract.post.sofa.reports.partials.coverRpt', ['sst' => $sst])

<div class="breakNow"><br></div>

<!-- @include('contract.post.sofa.reports.partials.ipcRpt', ['sst' => $sst]) -->

<!-- <div class="breakNow"><br></div> -->


@include('contract.post.sofa.reports.partials.contractRpt', ['sst' => $sst])

<div class="breakNow"><br></div>


@include('contract.post.sofa.reports.partials.akuan1Rpt', ['sst' => $sst])

<div class="breakNow"><br></div>

@include('contract.post.sofa.reports.partials.akuan2Rpt', ['sst' => $sst])

<div class="breakNow"><br></div>

@include('contract.post.sofa.reports.partials.contractorRpt', ['sst' => $sst])

<div class="breakNow"><br></div>

@include('contract.post.sofa.reports.partials.statementRpt', ['sst' => $sst])

