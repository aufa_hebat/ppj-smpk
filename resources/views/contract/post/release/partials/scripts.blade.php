@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {

        $(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.release.edit', id));
		});	

        $(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.release.show', id));
		});				
    });
	
</script>
@endpush
