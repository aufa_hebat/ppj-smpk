@extends('layouts.admin')

@push('style')
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.textarea')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.release.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'contract-post-management',
                            'route_name' => 'api.datatable.contract.post',
                            'columns' => [
                                ['data' => 'no_kontrak', 'title' => __('No. Kontrak'), 'defaultContent' => '-'],
                                ['data' => 'tajuk', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                ['data' => 'syarikat_terpilih', 'title' => __('Syarikat Terpilih'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Kontrak'), __('Tajuk'), __('Syarikat Terpilih'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.release.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div
@endsection
