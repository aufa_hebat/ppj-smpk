<script src="{{ asset('js/jquery.js') }}"></script>
<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
        .sm-content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        .newbr {
            border-left: hidden;
            border-right: hidden;
        }
</style>

<div style="text-align: right;" class="content">
    <i>BPUB/BUTIRAN BAYARAN/IPC</i>
</div>

<div class="title" style="text-align:center;font-weight: bold">
    BUTIRAN BAYARAN
</div>

<div style="text-transform: uppercase;text-align:center;font-weight: bold" class="title">
    {{ (!empty($ipc))? $ipc->sst->acquisition->title:null }}
</div>

<div class="title" style="text-align:center;font-weight: bold">
    No Kontrak : {{ (!empty($ipc))? $ipc->sst->contract_no:null }}
</div>

<div class="title" style="font-weight:bold">
    IPC NO. @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                {{ $ipc->penalty }}
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                {{ $ipc->ipc_no }}
            @elseif($ipc->penalty != null && $ipc->last_ipc == 'y')
                {{ $ipc->penalty."(AKHIR)" }}
            @else
                {{ $ipc->ipc_no."(AKHIR)" }}
            @endif
</div><br>

<div class="table-responsive" class="content">
    <table id="myTable" border="1" style="border-collapse: collapse;width: 100%;" class="content">
        <thead>
            <tr>
                <th rowspan="2" style="background-color: lightgray;text-align:center" valign="top">BIL</th>
                <th rowspan="2" style="background-color: lightgray;text-align:center" valign="top">PERKARA</th>
                <th colspan="4" style="background-color: lightgray;text-align:center" valign="top">KONTRAK</th>
                <th colspan="2" style="background-color: lightgray;text-align:center" valign="top">BAYARAN TERKINI</th>
                <th colspan="2" style="background-color: lightgray;text-align:center" valign="top">BAYARAN TERDAHULU</th>
                <th colspan="2" style="background-color: lightgray;text-align:center" valign="top">BAYARAN SEMASA</th>
            </tr>
            <tr>
                
                <th style="background-color: lightgray;text-align: center" valign="top">UNIT</th>
                <th style="background-color: lightgray;text-align: center" valign="top">KUANTITI</th>
                <th style="background-color: lightgray;text-align: center" valign="top">KADAR (RM)</th>
                <th style="background-color: lightgray;text-align: center" valign="top">JUMLAH HARGA (RM></th>
                <th style="background-color: lightgray;text-align: center" valign="top">KUANTITI ATAU %</th>
                <th style="background-color: lightgray;text-align: center" valign="top">JUMLAH HARGA (RM)</th>
                <th style="background-color: lightgray;text-align: center" valign="top">KUANTITI ATAU %</th>
                <th style="background-color: lightgray;text-align: center" valign="top">JUMLAH HARGA (RM)</th>
                <th style="background-color: lightgray;text-align: center" valign="top">KUANTITI ATAU %</th>
                <th style="background-color: lightgray;text-align: center" valign="top">JUMLAH HARGA (RM)</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($elemen_all) && $elemen_all != null)
                @foreach($elemen_all as $elemen)
                    <tr class="no-bottom-border">
                        <td valign="top" style="text-align:center">{{ $elemen->no }}</td>
                        <td style="padding:4px">{{ (!empty($elemen->element))? $elemen->element:null }}
                            @if(!empty($elemen->description) && $elemen->description != null)
                                <br><br>{{ $elemen->description }}<br>
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @if(!empty($item_all->where('bq_element_id',$elemen->id)->all()))
                        @foreach($item_all->where('bq_element_id',$elemen->id)->all() as $item)
                            <tr class="no-bottom-border">
                                <td valign="top" style="text-align:center">{{ $elemen->no }}.{{ $item->no }}</td>
                                <td style="padding:4px">{{ (!empty($item->item))? $item->item:null }}
                                    @if(!empty($item->description) && $item->description != null)
                                        <br><br>{{ $item->description }}<br>
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @if(!empty($subs_all->where('bq_item_id',$item->id)->all()))
                                @foreach($subs_all->where('bq_item_id',$item->id)->all() as $sub)
                                <tr class="no-top-border">
                                    <td valign="top" style="text-align:center">{{ $elemen->no }}.{{ $item->no }}.{{ $sub->no }}</td>
                                    <td style="padding:4px">
                                        {{ (!empty($sub->item))? $sub->item:null }}
                                        @if(!empty($sub->description) && $sub->description != null)
                                            <br><br>{!! $sub->description !!}
                                        @endif
                                    </td>
                                    <td valign="bottom" style="padding:4px">{{ $sub->unit }}</td>
                                    <td style="text-align:right;padding:4px" valign="bottom">{{ money()->toCommon($sub->quantity ?? 0, 2) }}</td>
                                    <td style="text-align:right;padding:4px" valign="bottom">{{ money()->toCommon($sub->rate_per_unit_adjust ?? 0, 2) }}</td>
                                    <td style="text-align:right;padding:4px" valign="bottom">{{ money()->toCommon($sub->amount_adjust ?? 0, 2)}}</td>
                                    <!-- terkini -->
                                    <td style="text-align:right;padding:4px" valign="bottom">
                                        @if(!empty($ipc_bq) && !empty($ipc_bq->where('bq_subitem_id',$sub->id)))
                                            {{ money()->toCommon($ipc_bq->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('quantity')->sum() ?? 0, 2) }}
                                        @else
                                        0.00
                                        @endif
                                    </td>
                                    <td style="text-align:right;padding:4px" valign="bottom">
                                        @if(!empty($ipc_bq) && !empty($ipc_bq->where('bq_subitem_id',$sub->id)))
                                            {{ money()->toCommon($ipc_bq->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('amount')->sum() ?? 0, 2) }}
                                        @else
                                        0.00
                                        @endif
                                    </td>
                                    <!-- terkini -->
                                    <!-- terdahulu -->
                                    <td style="text-align:right;padding:4px" valign="bottom">
                                        @if(!empty($bq_pre) && !empty($bq_pre->where('bq_subitem_id',$sub->id)))
                                            {{ money()->toCommon($bq_pre->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('quantity')->sum() ?? 0, 2) }}
                                        @else
                                        0.00
                                        @endif
                                    </td>
                                    <td style="text-align:right;padding:4px" valign="bottom">
                                        @if(!empty($bq_pre) && !empty($bq_pre->where('bq_subitem_id',$sub->id)))
                                            {{ money()->toCommon($bq_pre->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('amount')->sum() ?? 0, 2) }}
                                        @else
                                        0.00
                                        @endif
                                    </td>
                                    <!-- terdahulu -->
                                    <!-- semasa -->
                                    <td style="text-align:right;padding:4px" valign="bottom">
                                        @if(!empty($ipc))
                                            @if(!empty($ipc->ipcInvoice))
                                            @php $sum_curr1 = 0; @endphp
                                                @foreach($ipc->ipcInvoice as $pre_curr)
                                                    @if(!empty($pre_curr->ipcBq) && !empty($pre_curr->ipcBq->where('bq_subitem_id',$sub->id)->where('status','N')))
                                                        @php 
                                                            if($sum_curr1 == 0){
                                                                $sum_curr1 = $pre_curr->ipcBq->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('quantity')->sum();
                                                            }else{
                                                                $sum_curr1 = $sum_curr1 + $pre_curr->ipcBq->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('quantity')->sum();
                                                            }
                                                        @endphp
                                                    @endif
                                                @endforeach
                                                {{ money()->toCommon($sum_curr1 ?? 0,2) }}
                                            @else
                                                0.00
                                            @endif
                                        @else
                                            0.00
                                        @endif
                                    </td>
                                    <td style="text-align:right;padding:4px" valign="bottom">
                                        @if(!empty($ipc))
                                            @if(!empty($ipc->ipcInvoice))
                                                @php $sum_curr2 = 0; @endphp
                                                @foreach($ipc->ipcInvoice as $pre_curr)
                                                    @if(!empty($pre_curr->ipcBq) && !empty($pre_curr->ipcBq->where('bq_subitem_id',$sub->id)->where('status','N')))
                                                        @php 
                                                            if($sum_curr2 == 0){
                                                               $sum_curr2 =  $pre_curr->ipcBq->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('amount')->sum();
                                                            }else{
                                                                $sum_curr2 =  $sum_curr2 + $pre_curr->ipcBq->where('bq_subitem_id',$sub->id)->where('status','N')->pluck('amount')->sum();
                                                            }
                                                        @endphp
                                                    @endif
                                                @endforeach
                                                {{ money()->toCommon($sum_curr2 ?? 0,2) }}
                                            @else
                                                0.00
                                            @endif
                                        @else
                                            0.00
                                        @endif
                                    </td>
                                    <!-- semasa -->
                                </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                    <tr>
                        <td></td>
                        <td colspan="6" style="font-weight:bold;padding:4px">Jumlah Di Bawa Ke Ringkasan Bayaran</td>
                        <!-- terkini -->
                        <td style="text-align:right;font-weight:bold;padding:4px">
                            @if(!empty($ipc_bq) && $ipc_bq != null)
                                {{ money()->toCommon($ipc_bq->where('bq_element_id',$elemen->id)->where('status','N')->pluck('amount')->sum() ?? 0, 2) }}
                            @endif
                        </td>
                        <!-- terkini -->
                        <td></td>
                        <!-- terdahulu -->
                        <td style="text-align:right;font-weight:bold;padding:4px">
                            @if(!empty($bq_pre) && $bq_pre != null)
                                {{ money()->toCommon($bq_pre->where('bq_element_id',$elemen->id)->where('status','N')->pluck('amount')->sum() ?? 0, 2) }}
                            @endif
                        </td>
                        <!-- terdahulu -->
                        <td></td>
                        <!-- semasa -->
                        <td style="text-align:right;font-weight:bold;padding:4px">
                            @if(!empty($ipc))
                                @if(!empty($ipc->ipcInvoice))
                                    @php $total_curr = 0; @endphp
                                    @foreach($ipc->ipcInvoice as $pre_curr)
                                        @if(!empty($pre_curr->ipcBq))
                                            @php 
                                                if($total_curr == 0){
                                                    $total_curr = $pre_curr->ipcBq->where('bq_element_id',$elemen->id)->where('status','N')->pluck('amount')->sum();
                                                }else{
                                                    $total_curr = $total_curr + $pre_curr->ipcBq->where('bq_element_id',$elemen->id)->where('status','N')->pluck('amount')->sum();
                                                }
                                            @endphp
                                        @endif
                                    @endforeach
                                    {{ money()->toCommon($total_curr ?? 0,2) }}
                                @else
                                    0.00
                                @endif
                            @else
                                0.00
                            @endif
                        </td>
                        <!-- semasa -->
                    </tr>
                @endforeach
            @endif
            @if(!empty($elemen_v_all) && $elemen_v_all != null)
                @foreach($elemen_v_all as $elmn_v)
                    @if(!empty($elmn_v->element) && $elmn_v->element != null)
                        <tr class="no-bottom-border">
                            <td valign="top" style="text-align:center">{{ $elmn_v->element->no }}</td>
                            <td style="padding:4px">{{ (!empty($elmn_v->element->element))? $elmn_v->element->element:null }}
                                @if(!empty($elmn_v->element->description) && $elmn_v->element->description != null)
                                    <br><br>{{ $elmn_v->element->description }}<br>
                                @endif
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="no-bottom-border">
                            <td valign="top" style="text-align:center">{{ $elmn_v->element->no }}.{{ $elmn_v->item->no }}</td>
                            <td style="padding:4px">{{ (!empty($elmn_v->item->item))? $elmn_v->item->item:null }}
                                @if(!empty($elmn_v->item->description) && $elmn_v->item->description != null)
                                    <br><br>{{ $elmn_v->item->description }}<br>
                                @endif
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @if(!empty($subs_v_all))
                            @foreach($subs_v_all as $sub_v)
                            <tr class="no-top-border">
                                <td valign="top" style="text-align:center"></td>
                                <td style="padding:4px">
                                    {{ (!empty($sub_v->item))? $sub_v->item:null }}
                                    @if(!empty($sub_v->description) && $sub_v->description != null)
                                        <br><br>{!! $sub_v->description !!}
                                    @endif
                                </td>
                                <td valign="bottom" style="padding:4px"></td>
                                <td style="text-align:right;padding:4px" valign="bottom"></td>
                                <td style="text-align:right;padding:4px" valign="bottom"></td>
                                <td style="text-align:right;padding:4px" valign="bottom"></td>
                                <!-- terkini -->
                                <td style="text-align:right;padding:4px" valign="bottom">
                                    @if(!empty($ipc_bq) && !empty($ipc_bq->where('bq_subitem_id',$sub_v->id)->where('status','V')))
                                        {{ money()->toCommon($ipc_bq->where('bq_subitem_id',$sub_v->id)->pluck('quantity')->sum() ?? 0, 2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td style="text-align:right;padding:4px" valign="bottom">
                                    @if(!empty($ipc_bq) && !empty($ipc_bq->where('bq_subitem_id',$sub_v->id)->where('status','V')))
                                        {{ money()->toCommon($ipc_bq->where('bq_subitem_id',$sub_v->id)->where('status','V')->pluck('amount')->sum() ?? 0, 2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <!-- terkini -->
                                <!-- terdahulu -->
                                <td style="text-align:right;padding:4px" valign="bottom">
                                    @if(!empty($bq_pre) && !empty($bq_pre->where('bq_subitem_id',$sub_v->id)->where('status','V')))
                                        {{ money()->toCommon($bq_pre->where('bq_subitem_id',$sub_v->id)->pluck('quantity')->sum() ?? 0, 2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <td style="text-align:right;padding:4px" valign="bottom">
                                    @if(!empty($bq_pre) && !empty($bq_pre->where('bq_subitem_id',$sub_v->id)->where('status','V')))
                                        {{ money()->toCommon($bq_pre->where('bq_subitem_id',$sub_v->id)->where('status','V')->pluck('amount')->sum() ?? 0, 2) }}
                                    @else
                                    0.00
                                    @endif
                                </td>
                                <!-- terdahulu -->
                                <!-- semasa -->
                                <td style="text-align:right;padding:4px" valign="bottom">
                                    @if(!empty($ipc))
                                        @if(!empty($ipc->ipcInvoice))
                                        @php $sum_curr1 = 0; @endphp
                                            @foreach($ipc->ipcInvoice as $pre_curr)
                                                @if(!empty($pre_curr->ipcBq) && !empty($pre_curr->ipcBq->where('bq_subitem_id',$sub_v->id)->where('status','V')))
                                                    @php 
                                                        if($sum_curr1 == 0){
                                                            $sum_curr1 = $pre_curr->ipcBq->where('bq_subitem_id',$sub_v->id)->where('status','V')->pluck('quantity')->sum();
                                                        }else{
                                                            $sum_curr1 = $sum_curr1 + $pre_curr->ipcBq->where('bq_subitem_id',$sub_v->id)->where('status','V')->pluck('quantity')->sum();
                                                        }
                                                    @endphp
                                                @endif
                                            @endforeach
                                            {{ money()->toCommon($sum_curr1 ?? 0,2) }}
                                        @else
                                            0.00
                                        @endif
                                    @else
                                        0.00
                                    @endif
                                </td>
                                <td style="text-align:right;padding:4px" valign="bottom">
                                    @if(!empty($ipc))
                                        @if(!empty($ipc->ipcInvoice))
                                            @php $sum_curr2 = 0; @endphp
                                            @foreach($ipc->ipcInvoice as $pre_curr)
                                                @if(!empty($pre_curr->ipcBq) && !empty($pre_curr->ipcBq->where('bq_subitem_id',$sub_v->id)->where('status','V')))
                                                    @php 
                                                        if($sum_curr2 == 0){
                                                           $sum_curr2 =  $pre_curr->ipcBq->where('bq_subitem_id',$sub_v->id)->where('status','V')->pluck('amount')->sum();
                                                        }else{
                                                            $sum_curr2 =  $sum_curr2 + $pre_curr->ipcBq->where('bq_subitem_id',$sub_v->id)->where('status','V')->pluck('amount')->sum();
                                                        }
                                                    @endphp
                                                @endif
                                            @endforeach
                                            {{ money()->toCommon($sum_curr2 ?? 0,2) }}
                                        @else
                                            0.00
                                        @endif
                                    @else
                                        0.00
                                    @endif
                                </td>
                                <!-- semasa -->
                            </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td></td>
                            <td colspan="6" style="font-weight:bold;padding:4px">Jumlah Di Bawa Ke Ringkasan Bayaran</td>
                            <!-- terkini -->
                            <td style="text-align:right;font-weight:bold;padding:4px">
                                @if(!empty($ipc_bq) && $ipc_bq != null)
                                    {{ money()->toCommon($ipc_bq->where('status','V')->pluck('amount')->sum() ?? 0, 2) }}
                                @endif
                            </td>
                            <!-- terkini -->
                            <td></td>
                            <!-- terdahulu -->
                            <td style="text-align:right;font-weight:bold;padding:4px">
                                @if(!empty($bq_pre) && $bq_pre != null)
                                    {{ money()->toCommon($bq_pre->where('status','V')->pluck('amount')->sum() ?? 0, 2) }}
                                @endif
                            </td>
                            <!-- terdahulu -->
                            <td></td>
                            <!-- semasa -->
                            <td style="text-align:right;font-weight:bold;padding:4px">
                                @if(!empty($ipc))
                                    @if(!empty($ipc->ipcInvoice))
                                        @php $total_curr = 0; @endphp
                                        @foreach($ipc->ipcInvoice as $pre_curr)
                                            @if(!empty($pre_curr->ipcBq))
                                                @php 
                                                    if($total_curr == 0){
                                                        $total_curr = $pre_curr->ipcBq->where('status','V')->pluck('amount')->sum();
                                                    }else{
                                                        $total_curr = $total_curr + $pre_curr->ipcBq->where('status','V')->pluck('amount')->sum();
                                                    }
                                                @endphp
                                            @endif
                                        @endforeach
                                        {{ money()->toCommon($total_curr ?? 0,2) }}
                                    @else
                                        0.00
                                    @endif
                                @else
                                    0.00
                                @endif
                            </td>
                            <!-- semasa -->
                        </tr>
                    @endif
                @endforeach
            @endif
        </tbody>
    </table>
</div><br>
<div >
    <table style="width: 100%" class="content">
        <tr>
            <td >
                DISEDIAKAN OLEH <br><br>
                Nama : {{ (!empty($ipc->user))? $ipc->user->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->position))? $ipc->user->position->name: null }}<br>
                Tarikh :
            </td>
            <td >
                DISEMAK OLEH <br><br>
                Nama : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor))? $ipc->user->supervisor->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->position))? $ipc->user->supervisor->position->name:null }}<br>
                Tarikh :
            </td>
            <td >
                DISAHKAN OLEH <br><br>
                Nama : {{ (!empty($ipc->user && !empty($ipc->user->supervisor) && !empty(!empty($ipc->user->supervisor->supervisor))))? $ipc->user->supervisor->supervisor->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->supervisor) && !empty($ipc->user->supervisor->supervisor->position))? $ipc->user->supervisor->supervisor->position->name:null }}<br>
                Tarikh :
            </td>
        </tr>
    </table>
</div>
