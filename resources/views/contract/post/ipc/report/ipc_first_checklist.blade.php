<div style="text-align: right; font-size: 12px;">
    <i>BPUB/Senarai Semak/IPC No.
            @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                {{ $ipc->penalty }}
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                {{ $ipc->ipc_no }}
            @else
                {{ $ipc->ipc_no."(AKHIR)" }}
            @endif (HIJAU)</i>
</div>
<div>
	<center><b><u>SENARAI SEMAK DOKUMEN BAYARAN INTERIM (IPC) NO.
            @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                {{ $ipc->penalty }}
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                {{ $ipc->ipc_no }}
            @else
                {{ $ipc->ipc_no."(AKHIR)" }}
            @endif
                </u></b></center>
</div>
<div>
	<table style="width:100%">
		<tr>
			<td style="width:25%">NAMA PROJEK</td>
			<td style="width:3%" valign="top">:</td>
			<td valign="top" style="text-transform: uppercase;">{{$ipc->sst->acquisition->title}}</td>
		</tr>
		<tr>
			<td>NO. KONTRAK/SEBUT HARGA</td>
			<td valign="top">:</td>
			<td valign="top">{{$ipc->sst->contract_no}}</td>
		</tr>
		<tr>
			<td>NAMA KONTRAKTOR</td>
			<td valign="top">:</td>
			<td valign="top" style="text-transform: uppercase;">
				@foreach($ipc->ipcInvoice as $ipses)
    				@if(!empty($ipses->company))
    					{{$ipses->company->company_name}} 
    				@endif
    			@endforeach
    		</td>
		</tr>
		<tr>
			<td>JABATAN/BAHAGIAN</td>
			<td valign="top">:</td>
			<td valign="top" style="text-transform: uppercase;">{{$ipc->sst->acquisition->department->name}}</td>
		</tr>
	</table>
	<table style="width:100%">
		<tr>
			<td style="width:25%">TENDER</td>
			<td>:</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
			<td>PILIHAN</td>
			<td>:</td>
			<td>BON PELAKSANAAN</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
		</tr>
		<tr>
			<td style="width:25%">SEBUT HARGA</td>
			<td>:</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td></td>
			<td>:</td>
			<td>WANG JAMINAN PELAKSANAAN</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
	</table>
	<table border="1" style="border-collapse: collapse;width: 100%;">
		<tr>
			<th style="width: 5%">BIL</th>
			<th>PERKARA</th>
			<th style="width: 10%">PENYEDIA</th>
			<th style="width: 10%">BPUB</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Sijil Bayaran Interim (IPC), Ringkasan Bayaran, Status Bayran Interim, Butiran Bayaran **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Surat Tuntutan/Inbois/Inbois Cukai <i>(perlu ada bukti Cop Penerimaan oleh Jabatan)</i> **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Salinan Surat Setuju Terima (LOA) **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>4</td>
			<td>
				Salinan Polisi Insurans **
				<br>a. Contractor's All Risk (Insurance of Work & Third Party Liability)
				<br>b. Workmen Compensation
			</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>5</td>
			<td>
				Salinan Resit Premium Insurans (Sekiranya Polisi masih belum diperolehi) *
				<br>a. Contractor's All Risk (Insurance of Work & Third Party Liability)
				<br>b. Workmen Compensation
			</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td rowspan="2">6</td>
			<td>
				Salinan Bon Pelaksana *
				<br>Salinan resit Premium Bon Pelaksanaan
				<br>[Jika memilih Bon Pelaksana]
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
                <br>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
                <br>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>
				Salinan surat menyatakan pemotongan Wang Jaminan Pelaksanaan (WJP)
				<br>[Jika memilih WJP]
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>7</td>
			<td>Salinan Bill of Quatities (BQ) **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>8</td>
			<td>Pendaftaran Pertubuhan Keselamatan Sosial (PERKESO) **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>9</td>
			<td>Pendaftaran Cukai Barang & Perkhidmatan (SST) **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>10</td>
			<td>Salinan Resit Levi (untuk tender kerja RM500,000 ke atas sahaja) **</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
		<tr>
			<td>11</td>
			<td>Lain-lain : .......................................................................................</td>
			<td >
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
			<td>
				<label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
			</td>
		</tr>
	</table>
</div>
<div>
	<table>
		<tr>
			<td valign="top"><u>Nota : </u></td>
			<td>
				1. Setiap flysheet menggunakan kertas berwarna dan ditagging.
				<br>2. * Sekiranya berkaitan
				<br>3. ** Perlu dipatuhi dan maklumat yang diperlukan
			</td>
		</tr>
	</table>
	<table style="width: 100%;">
		<tr>
			<th colspan="3">Disediakan Oleh</th>
			<th colspan="3">Disemak Oleh (BPUB)</th>
		</tr>
		<tr>
			<td style="width: 15%;">Tandatangan & Cop Jawatan</td>
			<td valign="top">:</td>
			<td></td>
			<td style="width: 15%;">Tandatangan & Cop Jawatan</td>
			<td valign="top">:</td>
			<td></td>
		</tr>
		<tr>
			<td>Tarikh</td>
			<td valign="top">:</td>
			<td></td>
			<td>Tarikh</td>
			<td valign="top">:</td>
			<td></td>
		</tr>
	</table>
</div>