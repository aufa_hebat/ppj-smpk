<style type="text/css">
	 @page {
                margin: 60px 70px 60px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .newbr {
                border-left: hidden;
                border-right: hidden;
            }
</style>
<div class="title" style="text-align:center;font-weight:bold;">
    STATUS BAYARAN INTERIM
</div>

<div class="title" style="text-align:center;font-weight:bold;">
    {{ (!empty($ipc))? $ipc->sst->acquisition->title:null }}
</div>

<div class="title" style="text-align:center;font-weight:bold;">
    No Kontrak : {{ (!empty($ipc))? $ipc->sst->contract_no:null }}
</div><br>

<div class="content">
    <table style="width: 80%" align="center">
        <tr>
            <td style="width: 50%">
                1.	Harga Kontrak       : RM {{ (!empty($appointed))? money()->toCommon($appointed->offered_price ?? 0,2):'0.00' }}<br>
                2.	PPJHK No            : 
                                        @php $total = $appointed->offered_price; @endphp
                                        @if(!empty($ipc) && !empty($ipc->sst->ppjhk_active)) 
                                            @foreach($ipc->sst->ppjhk_active as $ppjhk) 
                                                {{ $ppjhk->no }} 
                                                &nbsp;&nbsp;&nbsp;
                                                @php 
                                                    $getvo = money()->toCommon($ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum() ?? "0", 2);
                                                    if($getvo < 0){
                                                        $getvo = "(RM " . (money()->toCommon((($ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum()) * -1) ?? "0", 2)) . ")";
                                                    }else{
                                                        $getvo = "RM " . $getvo;
                                                    }
                                                @endphp
                                                {{ $getvo }}

                                                @php $total = $total + ($ppjhk->elements->pluck('net_amount')->sum() + $ppjhk->cancels->pluck('amount')->sum()); @endphp
                                            @endforeach 
                                        @endif
                <br>
                3.	Harga Kontrak Baru  : {{ (!empty($ipc)) ? money()->toHuman($ipc->new_contract_amount ?? 0,2):'0.00' }}
                                        {{--  RM {{ (!empty($ipc))? money()->toCommon($ipc->new_contract_amount ?? 0,2):'0.00' }}  --}}
                                        <br>
                4.	Tarikh Penilaian    : {{ (!empty($ipc) && $ipc->evaluation_at !=null)? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->evaluation_at)->format('d/m/Y'):null }}
            </td>
            <td style="width: 50%">
                5.	Tarikh Milik Tapak  : {{ (!empty($ipc->sst->start_working_date))? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->start_working_date)->format('d/m/Y'):null }}<br>
                6.	Tarikh Siap Asal    : {{ (!empty($ipc->sst->end_working_date))? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->end_working_date)->format('d/m/Y'):null }} <br>
                7.	EOT No.             : {{ (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->eots) && $ipc->sst->eots->count() > 1)? $ipc->sst->eots->pluck('bil')->sortByDesc('id')->first():null }}<br>
                8.	Kadar LAD (RM/hari) : {{ (!empty($ipc->sst->document_LAD_amount))? "RM".money()->toCommon($ipc->sst->document_LAD_amount):null }}
            </td>
        </tr>
    </table>
</div>

<div >
    <table border="1" style="border-collapse: collapse;width: 100%;" class="content">
        <tr>
            <th style="background-color: lightgray"></th>
            <th style="background-color: lightgray"></th>
            <th colspan="5" style="background-color: lightgray;text-align: center" valign="top">PENGESAHAN SEMASA (RM)</th>
            <th colspan="5" style="background-color: lightgray;text-align: center" valign="top">PENGESAHAN TERKINI (RM)</th>
            <th colspan="2" style="background-color: lightgray;text-align: center" valign="top">PERATUSAN JUMLAH BAYARAN</th>
            <th style="background-color: lightgray"></th>
        </tr>
        <tr>
            <th style="background-color: lightgray;text-align: center" valign="top">No IPC</th>
            <th style="background-color: lightgray;text-align: center" valign="top">Bulan/Tahun</th>
            <th style="background-color: lightgray;text-align: center" valign="top">Jumlah Kasar (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">SST (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">*Wang Tahanan (WJP) (RM)</th>
            {{--  <th style="background-color: lightgray;text-align: center" valign="top">*Jumlah Perubahan Kerja (RM)</th>  --}}
            <th style="background-color: lightgray;text-align: center" valign="top">*Jumlah LAD (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">Jumlah Bersih (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">Jumlah Kasar (Termasuk Bahan di Tapak) (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">SST (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">*Wang Tahanan (WJP) (RM)</th>
            {{--  <th style="background-color: lightgray;text-align: center" valign="top">*Jumlah Perubahan Kerja (RM)</th>  --}}
            <th style="background-color: lightgray;text-align: center" valign="top">*Jumlah LAD (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">Jumlah Bersih (RM)</th>
            <th style="background-color: lightgray;text-align: center" valign="top">% Semasa </th>
            <th style="background-color: lightgray;text-align: center" valign="top">% Terkini </th>
            <th style="background-color: lightgray;text-align: center" valign="top">Catatan</th>
        </tr>
        @if(!empty($ipc_kumpul) && $ipc_kumpul->count() > 0)
            @foreach($ipc_kumpul->all() as $all)
            <!-- -kiraan pengesahan terkini -->
            @php 
                $kumpul_kasar           = null;
                $kumpul_material        = null;
                $kumpul_material_bayar  = null;
                $kumpul_wjp             = null;
                $kumpul_tahan_wjp       = null;
                $kumpul_vo              = null;
                $kumpul_lad             = null;
                $kumpul_bersih          = null;
               
            if(!empty($ipc_kumpul->where('ipc_no','<=',$all->ipc_no))){
                foreach($ipc_kumpul->where('ipc_no','<=',$all->ipc_no) as $kump){
                    if($kumpul_kasar == null){
                        $kumpul_kasar = $kump->ipcInvoice->pluck('demand_amount')->sum();
                    }else{
                        $kumpul_kasar = $kumpul_kasar + $kump->ipcInvoice->pluck('demand_amount')->sum(); 
                    }
                }
                
                if($kumpul_material == null){
                    $kumpul_material = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('material_on_site_amount')->sum(); 
                }else{
                    $kumpul_material = $kumpul_material + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('material_on_site_amount')->sum(); 
                }
                
                if($kumpul_material_bayar == null){
                    $kumpul_material_bayar = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('material_tahanan_amount')->sum(); 
                }else{
                    $kumpul_material_bayar = $kumpul_material_bayar + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('material_tahanan_amount')->sum(); 
                }
                
                if($kumpul_wjp == null){
                    $kumpul_wjp = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('wjp_amount')->sum(); 
                }else{
                    $kumpul_wjp = $kumpul_wjp + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('wjp_amount')->sum(); 
                }
                
                if($kumpul_tahan_wjp == null){
                    $kumpul_tahan_wjp = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('wjp_lepas_amount')->sum(); 
                }else{
                    $kumpul_tahan_wjp = $kumpul_tahan_wjp + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('wjp_lepas_amount')->sum(); 
                }
                
                if($kumpul_vo == null){
                    $kumpul_vo = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('working_change_amount')->sum(); 
                }else{
                    $kumpul_vo = $kumpul_vo + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('working_change_amount')->sum(); 
                }
                
                if($kumpul_lad == null){
                    $kumpul_lad = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('compensation_amount')->sum(); 
                }else{
                    $kumpul_lad = $kumpul_lad + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('compensation_amount')->sum(); 
                }
                
                if($kumpul_bersih == null){
                    $kumpul_bersih = $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('demand_amount')->sum(); 
                }else{
                    $kumpul_bersih = $kumpul_bersih + $ipc_kumpul->where('ipc_no','<=',$all->ipc_no)->pluck('demand_amount')->sum(); 
                }
            }
            @endphp
            <!-- -kiraan pengesahan terkini -->
                <tr>
                    <td style="text-align: center" valign="top">{{ $all->ipc_no }}</td>
                    <td valign="top" style="padding:2px">@php $bulan = null; @endphp
                        @if(!empty($all->ipcInvoice))
                            @foreach($all->ipcInvoice as $inv_bulan)
                                @if($inv_bulan->bulan_status != null)
                                    @if($bulan == null)
                                        @php $bulan = $inv_bulan->bulan_status; @endphp
                                    @else
                                        @php $bulan = $bulan.' , '.$inv_bulan->bulan_status; @endphp
                                    @endif
                                @else
                                    @php $bulan = ($all->evaluation_at != null)? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$all->evaluation_at)->format('M Y'):null; @endphp
                                @endif
                            @endforeach
                        @else
                            @php $bulan = ($all->evaluation_at != null)? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$all->evaluation_at)->format('M Y'):null; @endphp
                        @endif
                        {{ $bulan }}
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">
                        {{ money()->toCommon(($all->ipcInvoice->pluck('demand_amount')->sum() + $all->material_on_site_amount - $all->material_tahanan_amount + $all->working_change_amount) ?? 0,2) }}
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">0.00</td>
                    <td style="text-align:right;padding:2px" valign="top">({{ money()->toCommon($all->wjp_amount ?? 0,2) }})</td>
                    {{--  <td style="text-align:right;padding:2px" valign="top">{{ ($all->working_change_amount < 0)? "(".money()->toCommon(-$all->working_change_amount ?? 0,2).")":money()->toCommon($all->working_change_amount ?? 0,2) }}</td>  --}}
                    <td style="text-align:right;padding:2px" valign="top">({{ money()->toCommon($all->compensation_amount ?? 0,2) }})</td>
                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->demand_amount < 0)
                            {{ "(".money()->toCommon(-$all->demand_amount ?? 0,2).")" }}
                        @else
                            {{ money()->toCommon($all->demand_amount ?? 0,2) }}
                        @endif
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->ipc_no != 1)
                            {{ money()->toCommon(($kumpul_kasar + $kumpul_material - $kumpul_material_bayar + $all->working_change_amount) ,2) }}
                        @else
                            {{ money()->toCommon(($all->ipcInvoice->pluck('demand_amount')->sum() + $all->material_on_site_amount - $all->material_tahanan_amount + $all->working_change_amount) ?? 0,2) }}
                        @endif
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">0.00</td>
                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->ipc_no != 1)
                            @if(($kumpul_wjp + $kumpul_tahan_wjp) < 0)
                                ({{ money()->toCommon($kumpul_wjp + $kumpul_tahan_wjp ,2) }})
                            @else
                                {{ money()->toCommon(($kumpul_wjp + $kumpul_tahan_wjp) ?? 0 ,2) }}
                            @endif
                            
                        @else
                            @if(($kumpul_wjp + $kumpul_tahan_wjp) < 0)
                                ({{ money()->toCommon($all->wjp_amount + $all->wjp_lepas_amount,2) }})
                            @else
                                {{ money()->toCommon(($all->wjp_amount + $all->wjp_lepas_amount) ?? 0,2) }}
                            @endif
                            
                        @endif
                    </td>
<!--                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->ipc_no != 1)
                            {{ ($kumpul_vo < 0)? '('.money()->toCommon(-$kumpul_vo ,2).')':money()->toCommon($kumpul_vo ,2) }}
                        @else
                            {{ ($all->working_change_amount < 0)? '('.money()->toCommon(-$all->working_change_amount ?? 0,2).')':money()->toCommon($all->working_change_amount ?? 0,2) }}
                        @endif
                    </td>-->
                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->ipc_no != 1)
                            ({{ money()->toCommon($kumpul_lad ,2) }})
                        @else
                            ({{ money()->toCommon($all->compensation_amount ?? 0,2) }})
                        @endif
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->ipc_no != 1)
                            @if($kumpul_bersih < 0)
                                {{ "(".money()->toCommon(-$kumpul_bersih ,2).")" }}
                            @else
                                {{ money()->toCommon($kumpul_bersih ,2) }}
                            @endif
                        @else
                            @if($all->demand_amount < 0)
                                {{ "(".money()->toCommon(-$all->demand_amount ?? 0,2).")" }}
                            @else
                                {{ money()->toCommon($all->demand_amount ?? 0,2) }}
                            @endif
                        @endif
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">
                        @php
                            $satu = money()->toCommon($all->ipcInvoice->pluck('demand_amount')->sum());
                            $dua  = money()->toCommon($all->material_on_site_amount);
                            $tiga = money()->toCommon($total);
                            $empat = ((($satu + $dua) / $tiga ) * 100);
                        @endphp
                        {{ bcdiv($empat, 1, 2) }}
                    </td>
                    <td style="text-align:right;padding:2px" valign="top">
                        @if($all->ipc_no != 1)
                            @php
                                $one = money()->toCommon($kumpul_kasar);
                                $two  = money()->toCommon($kumpul_material);
                                $three = money()->toCommon($total);
                                $four = ((($one + $two) / $three ) * 100);
                            @endphp
                            {{ bcdiv($four, 1, 2) }}
                        @else
                            @php
                                $a = money()->toCommon($all->ipcInvoice->pluck('demand_amount')->sum());
                                $b  = money()->toCommon($all->material_on_site_amount);
                                $c = money()->toCommon($total);
                                $d = ((($a + $b) / $c ) * 100);
                            @endphp
                            {{ bcdiv($d, 1, 2) }}
                        @endif
                    </td>
                    <td></td>
                </tr>
            @endforeach
        @endif
    </table>
</div><br>

<div class="content">
    Nota : * sekiranya berkaitan
    <br><br>
    Kami bertanggungjawab dengan perakuan dan pengesahan kerja/perkhidmatan/bekalan ini telah dinilai dan dilaksanakan dengan sempurna sebagaimana bayaran/tuntutan
</div><br>

<div >
    <table style="width: 100%" class="content">
        <tr>
            <td>
                DISEDIAKAN OLEH <br><br><br><br><br><br>
                Nama : {{ (!empty($ipc->user))? $ipc->user->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->position))? $ipc->user->position->name: null }}<br>
                Tarikh :
            </td>
            <td>
                DISEMAK OLEH <br><br><br><br><br><br>
                Nama : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor))? $ipc->user->supervisor->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->position))? $ipc->user->supervisor->position->name:null }}<br>
                Tarikh :
            </td>
            <td>
                DISAHKAN OLEH <br><br><br><br><br><br>
                Nama : {{ (!empty($ipc->user && !empty($ipc->user->supervisor) && !empty(!empty($ipc->user->supervisor->supervisor))))? $ipc->user->supervisor->supervisor->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->supervisor) && !empty($ipc->user->supervisor->supervisor->position))? $ipc->user->supervisor->supervisor->position->name:null }}<br>
                Tarikh :
            </td>
        </tr>
    </table>
</div>