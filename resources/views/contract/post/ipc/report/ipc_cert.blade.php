<style type="text/css">
	 @page {
                margin: 60px 70px 60px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }
            .title {
                font-size: 11px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .title-norm {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .content {
                font-size: 9px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .content-norm {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
            .newbr {
                border-left: hidden;
                border-right: hidden;
            }
</style>
<body>
    <div style="text-align: right" class="content">
        <i>BPUB/IPC/@if($ipc->penalty != null && $ipc->last_ipc == 'n')
                {{ $ipc->penalty }}
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                {{ $ipc->ipc_no }}
            @elseif($ipc->penalty != null && $ipc->last_ipc == 'y')
                {{ $ipc->penalty."(AKHIR)" }}
            @else
                {{ $ipc->ipc_no."(AKHIR)" }}
            @endif
        </i>
    </div>

    <div class="title" style="text-align:center;font-weight:bold">
        PERBADANAN PUTRAJAYA <br> SIJIL BAYARAN INTERIM
    </div><br>
    <div>
        <table border="1" style="border-collapse: collapse;width: 100%;" class="content">
            <tr>
                <td colspan="3" style="padding:2px">Rujukan Fail / No. Kontrak  : 
                    <b>{{ (!empty($ipc))? $ipc->sst->contract_no: null }}</b>
                </td>
                <td colspan="2" style="width: 30%;padding:2px">Kod Projek : 
                    <b>{{ (! empty($ipc) && ! empty($ipc->ipcInvoice) && ! empty($ipc->ipcInvoice->pluck('financial')->pluck('glAccount')) && ! empty($ipc->ipcInvoice->pluck('financial')->pluck('glAccount')->pluck('code')->first())) ? $ipc->ipcInvoice->pluck('financial')->pluck('glAccount')->pluck('code')->first() : '' }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding:2px">Tarikh Akuan Penerimaan Surat Setuju Terima : 
                    @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->al_contractor_date))
                        <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->al_contractor_date)->format('d M Y') }}</b>
                    @endif
                </td>
                <td colspan="2" style="padding:2px">No Sijil Interim : @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                                        <b>{{ $ipc->penalty }}</b>
                                    @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                                        <b>{{ $ipc->ipc_no }}</b>
                                    @else
                                        <b>{{ $ipc->ipc_no."(AKHIR)" }}</b>
                                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding:2px">Tarikh Kontrak Ditandatangani : 
                    @if(!empty($ipc) && !empty($ipc->sst->document2) && !empty($ipc->sst->document2->revenue_stamp_date))
                        <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$ipc->sst->document2->revenue_stamp_date)->format('d M Y') }}</b>
                    @endif
                </td>
                <td colspan="2" rowspan="2" valign="top" style="padding:2px">No. Invois   : 
                    @if(!empty($ipc) && !empty($ipc->ipcInvoice)) 
                    @php $kire =  $ipc->ipcInvoice->count(); @endphp
                    @foreach($ipc->ipcInvoice as $cnt=>$inv)
                        <b>{{ $inv->invoice_no }}</b>
                        @if($cnt > 0 && $cnt != $kire)
                        <b>,</b>
                        @endif
                    @endforeach 
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding:2px">Tajuk Kerja  : <b>{!! (!empty($ipc))?  $ipc->sst->acquisition->title: null !!}</b></td>
            </tr>
            <tr>
                <td colspan="3" style="padding:2px">Nama Kontraktor : 
                    <b>{{ (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->company))? $ipc->sst->company->company_name: null }} </b>
                </td>
                <td colspan="2" rowspan="2" valign="top" style="padding:2px">
                    Tarikh Invois  : 
                    @if(!empty($ipc)) 
                    @php $kires =  $ipc->ipcInvoice->count(); @endphp
                    @foreach($ipc->ipcInvoice as $cnts=>$inv)
                        <b>{{ (!empty($inv->invoice_at))? date('d.m.Y', strtotime($inv->invoice_at)):null }}</b>
                        @if($cnts > 0 && $cnts != $kires)
                        <b>,</b>
                        @endif
                    @endforeach 
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding:2px">No. Pendaftaran GST : 
                    <b>{{ (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->company))? $ipc->sst->company->gst_registered_no: null }} </b>
                </td>
                
            </tr>
            <tr>
                <td colspan="3" style="padding:2px">No. Pendaftaran SST : </td>
                <td colspan="2" style="padding:2px">Tarikh Penilaian : <b>{{ (!empty($ipc) && !empty($ipc->evaluation_at))? date('d.m.Y', strtotime($ipc->evaluation_at)):null }}</b></td>
            </tr>

        </table>
        <br>
        <table border="1" style="border-collapse: collapse;width: 100%;" class="content">
            <tr>
                <th style="background-color: grey; text-align:center;" width="40%" >BUTIRAN</th>
                <th style="background-color: grey; text-align:center;" width="15%">Jumlah Kontrak</th>
                <th style="background-color: grey; text-align:center;" width="15%">Pengesahan Terkini</th>
                <th style="background-color: grey; text-align:center;" width="15%">Pengesahan Terdahulu</th>
                <th style="background-color: grey; text-align:center;" width="15%">Pengesahan Semasa</th>
            </tr>
            <tr>
                <td style="padding:2px">Harga Tawaran/Nilai Kontrak</td>
                <td style="text-align:right;padding:2px">
                    {{ (!empty($appointed) && !empty($appointed->offered_price))? money()->toCommon($appointed->offered_price,2):'-' }}
                </td>
                <td style="text-align:right;padding:2px">
                    {{ (!empty($kumpul_inv) && $kumpul_inv != null)? money()->toCommon($kumpul_inv,2):'-' }}
                </td>
                <td style="text-align:right;padding:2px">
                    {{ (!empty($pre_inv) && $pre_inv != null)? money()->toCommon($pre_inv,2):'-' }}
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->pluck('demand_amount')->sum() > 0)
                        {{ money()->toCommon($ipc->ipcInvoice->pluck('demand_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="padding:2px">Cukai Jualan dan Cukai Perkhidmatan (SST)</td>
                <td style="text-align:right;padding:2px">-</td>
                <td style="text-align:right;padding:2px">
                    {{ (!empty($kumpul_sst) && $kumpul_sst != null)? money()->toCommon($kumpul_sst,2):'-' }}
                </td>
                <td style="text-align:right;padding:2px">
                    {{ (!empty($pre_sst) && $pre_sst != null)? money()->toCommon($pre_sst,2):'-' }}
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->pluck('sst_amount')->sum() > 0)
                        {{ money()->toCommon($ipc->ipcInvoice->pluck('sst_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="background-color: lightgray;padding:2px">JUMLAH HARGA KONTRAK</td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    @if(!empty($appointed) && $appointed->offered_price > 0)
                        {{ money()->toCommon($appointed->offered_price,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    {{ (!empty($kumpul_total) && $kumpul_total != null)? money()->toCommon($kumpul_total,2):'-' }}
                </td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    {{ (!empty($pre_total) && $pre_total != null)? money()->toCommon($pre_total,2):'-' }}
                </td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    @if(!empty($ipc) && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->pluck('invoice')->sum() > 0)
                        {{ money()->toCommon($ipc->ipcInvoice->pluck('invoice')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">(+) Bahan-bahan dan Barang-Barang Tak Pasang <span style="font-style: italic">(Kontrak kerja sahaja sekiranya berkaitan)</span></td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && ($ipc_kumpul->pluck('material_on_site_amount')->sum() - $ipc_kumpul->pluck('material_tahanan_amount')->sum()) < 0 )
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('material_tahanan_amount')->sum() - $ipc_kumpul->pluck('material_on_site_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_kumpul) && ($ipc_kumpul->pluck('material_on_site_amount')->sum() - $ipc_kumpul->pluck('material_tahanan_amount')->sum()) > 0 )
                        {{ money()->toCommon($ipc_kumpul->pluck('material_on_site_amount')->sum() - $ipc_kumpul->pluck('material_tahanan_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                    
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && ($ipc_pre->pluck('material_on_site_amount')->sum() - $ipc_pre->pluck('material_tahanan_amount')->sum()) < 0 )
                        {{ "(".money()->toCommon($ipc_pre->pluck('material_tahanan_amount')->sum() - $ipc_pre->pluck('material_on_site_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_pre) && ($ipc_pre->pluck('material_on_site_amount')->sum() - $ipc_pre->pluck('material_tahanan_amount')->sum()) > 0 )
                        {{ money()->toCommon($ipc_pre->pluck('material_on_site_amount')->sum() - $ipc_pre->pluck('material_tahanan_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && ($ipc->material_on_site_amount - $ipc->material_tahanan_amount) < 0 )
                        {{ "(".money()->toCommon($ipc->material_tahanan_amount - $ipc->material_on_site_amount,2).")" }}
                    @elseif(!empty($ipc) && ($ipc->material_on_site_amount - $ipc->material_tahanan_amount) > 0 )
                        {{ money()->toCommon($ipc->material_on_site_amount - $ipc->material_tahanan_amount,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="padding:2px">(+/-) Perubahan Kerja</td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() < 0 )
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() > 0 )
                        {{ money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() < 0 )
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() > 0 )
                        {{ money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('working_change_amount')->sum() < 0 )
                        {{ "(".money()->toCommon($ipc_pre->pluck('working_change_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_pre) && $ipc_pre->pluck('working_change_amount')->sum() > 0 )
                        {{ money()->toCommon($ipc_pre->pluck('working_change_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->working_change_amount < 0 )
                        {{ "(".money()->toCommon($ipc->working_change_amount,2).")" }}
                    @elseif(!empty($ipc) && $ipc->working_change_amount > 0 )
                        {{ money()->toCommon($ipc->working_change_amount,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="background-color: lightgray;padding:2px">JUMLAH PERUBAHAN KERJA (+/-)</td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() < 0 )
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() > 0 )
                        {{ money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() < 0 )
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2).")" }}
                    @elseif(!empty($ipc_kumpul) && $ipc_kumpul->pluck('working_change_amount')->sum() > 0 )
                        {{ money()->toCommon($ipc_kumpul->pluck('working_change_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('working_change_amount')->sum() < 0 )
                        {{ "(".money()->toCommon($ipc_pre->pluck('working_change_amount')->sum() ,2).")" }}
                    @elseif(!empty($ipc_pre) && $ipc_pre->pluck('working_change_amount')->sum() > 0 )
                        {{ money()->toCommon($ipc_pre->pluck('working_change_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: lightgray;text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->working_change_amount < 0 )
                        {{ "(".money()->toCommon($ipc->working_change_amount,2).")" }}
                    @elseif(!empty($ipc) && $ipc->working_change_amount > 0 )
                        {{ money()->toCommon($ipc->working_change_amount,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="background-color: #AEAEAE;font-weight:bold;padding:2px">JUMLAH NILAI KONTRAK BARU</td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if(!empty($appointed) && $appointed->offered_price > 0)
                        {{ money()->toCommon(($appointed->offered_price + $ipc_kumpul->pluck('working_change_amount')->sum()),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if($kumpul_total != null && !empty($ipc_kumpul))
                       {{ money()->toCommon(($kumpul_total + $ipc_kumpul->pluck('working_change_amount')->sum() + $ipc_kumpul->pluck('material_on_site_amount')->sum() - $ipc_kumpul->pluck('material_tahanan_amount')->sum()),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if($pre_total != null && !empty($ipc_pre))
                       {{ money()->toCommon(($pre_total + $ipc_pre->pluck('working_change_amount')->sum() + $ipc_pre->pluck('material_on_site_amount')->sum() - $ipc_pre->pluck('material_tahanan_amount')->sum()),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if(!empty($ipc) && ($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_on_site_amount - $ipc->material_tahanan_amount) < 0 )
                        {{ "(".money()->toCommon(($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_tahanan_amount - $ipc->material_on_site_amount) ,2).")" }}
                    @elseif(!empty($ipc) && ($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_on_site_amount - $ipc->material_tahanan_amount) > 0 )
                        {{ money()->toCommon(($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_on_site_amount - $ipc->material_tahanan_amount) ,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="padding:2px">Cukai Barang dan Cukai Perkhidmatan (6% GST) <i>*sehingga 31 Mei 2018</i></td>
                <td style="text-align:right;padding:2px">-</td>
                <td style="text-align:right;padding:2px">-</td>
                <td style="text-align:right;padding:2px">-</td>
                <td style="text-align:right;padding:2px">-</td>
            </tr>
            <tr>
                <td style="background-color: #AEAEAE;font-weight:bold;padding:2px">JUMLAH KASAR SELEPAS SST</td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                        @if(!empty($appointed) && $appointed->offered_price > 0)
                            {{ money()->toCommon(($appointed->offered_price + $ipc_kumpul->pluck('working_change_amount')->sum()) ?? 0,2) }}
                        @else
                            -
                        @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if($kumpul_total != null && !empty($ipc_kumpul))
                       {{ money()->toCommon(($kumpul_total + $ipc_kumpul->pluck('working_change_amount')->sum() + $ipc_kumpul->pluck('material_on_site_amount')->sum() - $ipc_kumpul->pluck('material_tahanan_amount')->sum()),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if($pre_total != null && !empty($ipc_pre))
                       {{ money()->toCommon(($pre_total + $ipc_pre->pluck('working_change_amount')->sum() + $ipc_pre->pluck('material_on_site_amount')->sum() - $ipc_pre->pluck('material_tahanan_amount')->sum()),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if(!empty($ipc) && ($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_on_site_amount - $ipc->material_tahanan_amount) < 0 )
                        {{ "(".money()->toCommon(($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_tahanan_amount - $ipc->material_on_site_amount) ,2).")" }}
                    @elseif(!empty($ipc) && ($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_on_site_amount - $ipc->material_tahanan_amount) > 0 )
                        {{ money()->toCommon(($ipc->ipcInvoice->pluck('invoice')->sum() + $ipc->working_change_amount + $ipc->material_on_site_amount - $ipc->material_tahanan_amount),2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">(+) Bayaran Pendahuluan : 
                    @if(!empty($ipc->sst->deposit) && !empty($ipc->sst->deposit->qualified_amount))
                        {{ money()->toCommon($ipc->sst->deposit->qualified_amount,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('advance_amount')->sum() > 0)
                        {{ money()->toCommon($ipc_kumpul->pluck('advance_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('advance_amount')->sum() > 0)
                        {{ money()->toCommon($ipc_pre->pluck('advance_amount')->sum(),2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->advance_amount > 0)
                        {{ money()->toCommon($ipc->advance_amount ,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">(+) Wang Jaminan Pelaksanaan Dilepaskan (Kontrak Kerja sahaja)</td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('wjp_lepas_amount')->sum() > 0)
                        {{ money()->toCommon($ipc_kumpul->pluck('wjp_lepas_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('wjp_lepas_amount')->sum() > 0)
                        {{ money()->toCommon($ipc_pre->pluck('wjp_lepas_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->wjp_lepas_amount > 0)
                        {{ money()->toCommon($ipc->wjp_lepas_amount,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">(-)  Bayaran Balik Pendahuluan</td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('give_advance_amount')->sum())
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('give_advance_amount')->sum() ,2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('give_advance_amount')->sum())
                        {{ "(".money()->toCommon($ipc_pre->pluck('give_advance_amount')->sum() ,2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->give_advance_amount)
                        {{ "(".money()->toCommon($ipc->give_advance_amount,2).")" }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">(-)  *Jumlah Wang Jaminan Pelaksanaan (WJP) :
                    @if(! empty($ipc->sst->bon) && 4 == $ipc->sst->bon->bon_type && $ipc->sst->bon->money_amount > 0)
                        {{ "(".money()->toCommon($ipc->sst->bon->money_amount, 2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('wjp_amount')->sum() > 0)
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('wjp_amount')->sum() ,2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('wjp_amount')->sum() > 0)
                        {{ "(".money()->toCommon($ipc_pre->pluck('wjp_amount')->sum() ,2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->wjp_amount > 0)
                        {{ "(".money()->toCommon($ipc->wjp_amount ,2).")" }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">*Jumlah & No. Bon Pelaksanaan : 
                @if(! empty($ipc->sst->bon) && (1 == $ipc->sst->bon->bon_type || 2 == $ipc->sst->bon->bon_type) && $ipc->sst->bon->bank_amount > 0)
                    RM {{ money()->toCommon($ipc->sst->bon->bank_amount,2) }}&nbsp;&nbsp;&nbsp;({{ $ipc->sst->bon->bank_no ?? $ipc->sst->bon->insurance_no }})
                @else
                    -
                @endif
                </td>
                <td style="background-color: grey;" colspan="3" rowspan="2"></td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">*sila pilih salah satu dan diisi</td>
            </tr>
            <tr>
                <td style="padding:2px">(-) Ganti Rugi Tertentu Dan Ditetapkan (Penalti/LAD/NCR)
                    {{ (!empty($ipc_kumpul) && $ipc_kumpul->pluck('lad_day')->sum() > 0)? $ipc_kumpul->pluck('lad_day')->sum()." hari":null }}
                </td>
                <td></td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_kumpul) && $ipc_kumpul->pluck('compensation_amount')->sum() > 0)
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('compensation_amount')->sum(),2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc_pre) && $ipc_pre->pluck('compensation_amount')->sum() > 0)
                        {{ "(".money()->toCommon($ipc_pre->pluck('compensation_amount')->sum(),2).")" }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:right;padding:2px">
                    @if(!empty($ipc) && $ipc->compensation_amount > 0)
                        {{ "(".money()->toCommon($ipc->compensation_amount ,2).")" }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="background-color: #AEAEAE;font-weight:bold;padding:2px">JUMLAH BERSIH KENA BAYAR</td>
                <td style="background-color: #AEAEAE;"></td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px" >
                    @if(!empty($ipc_kumpul) && !empty($ipc_kumpul->pluck('demand_amount')) && $ipc_kumpul->pluck('demand_amount')->sum() < 0)
                        {{ "(".money()->toCommon($ipc_kumpul->pluck('demand_amount')->sum() ,2).")" }}
                    @elseif(!empty($ipc_kumpul) && !empty($ipc_kumpul->pluck('demand_amount')) && $ipc_kumpul->pluck('demand_amount')->sum() > 0)
                        {{ money()->toCommon($ipc_kumpul->pluck('demand_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if(!empty($ipc_pre) && !empty($ipc_pre->pluck('demand_amount')) && $ipc_pre->pluck('demand_amount')->sum() < 0)
                        {{ "(".money()->toCommon($ipc_pre->pluck('demand_amount')->sum() ,2).")" }}
                    @elseif(!empty($ipc_pre) && !empty($ipc_pre->pluck('demand_amount')) && $ipc_pre->pluck('demand_amount')->sum() > 0)
                        {{ money()->toCommon($ipc_pre->pluck('demand_amount')->sum() ,2) }}
                    @else
                        -
                    @endif
                </td>
                <td style="background-color: #AEAEAE;text-align:right;font-weight:bold;padding:2px">
                    @if(!empty($ipc) && $ipc->demand_amount < 0)
                        {{ "(".money()->toCommon($ipc->demand_amount,2).")" }}
                    @elseif(!empty($ipc) && $ipc->demand_amount > 0)
                        {{ money()->toCommon($ipc->demand_amount ,2) }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold;padding:2px">
                   (Ringgit Malaysia : Dalam Perkataan) : 
                </td>
                <td colspan="4" style="font-weight:bold;padding:2px">
                    @if(!empty($ipc))
                    
                    <label class=MsoNormal style='text-transform: capitalize '>
                    @php
                        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                        $amt = ($ipc->demand_amount < 0)? -($ipc->demand_amount):$ipc->demand_amount;
                        $amaun = money()->toCommon($amt ?? "0" , 2);
                        $format = explode('.',$amaun);
                        $formats = substr($amaun,-2);
                        $partringgit = str_replace(',', '', $format[0]);
                        $partsen = $format[1];
                        $partkosongsen = (int)$formats;

                        if($partkosongsen == '.00'){
                            echo $f->format($partringgit) . " Sahaja";
                        }
                        else{
                            echo $f->format($partringgit) . " Dan Sen " . $f->format($partkosongsen) . " Sahaja";
                        }

                    @endphp	
                    </label>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="5" style="background-color: grey;padding:2px">Pengesahan oleh Pengurusan Projek (JIKA PERLU)</td>
            </tr>
            <tr>
                <td style="padding:2px">
                    Disediakan <br><br><br><br>
                    Nama : <br>
                    Jawatan : <br>
                    Tarikh : <br>
                </td>
                <td colspan="2" style="padding:2px">
                    Disemak <br><br><br><br>
                    Nama :  <br>
                    Jawatan :  <br>
                    Tarikh : <br>
                </td>
                <td colspan="2" style="padding:2px">
                    Diakui <br><br><br><br>
                    Nama :  <br>
                    Jawatan :  <br>
                    Tarikh : <br>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="background-color: grey;padding:2px">Pengesahan oleh Jabatan Bertanggungjawab </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:2px">
                    <b>Disedia, </b><br><br><br><br>
                    Nama : <b>{{ (!empty($ipc->user))? $ipc->user->name:null }}</b><br>
                    Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->position))? $ipc->user->position->name: null }}<br>
                    Tarikh : <br>
                </td>
                <td colspan="3" style="padding:2px">
                    <b>Disemak dan disahkan, </b><br><br><br><br>
                    Nama : <b>{{ (!empty($ipc->user && !empty($ipc->user->supervisor) && !empty(!empty($ipc->user->supervisor->supervisor))))? $ipc->user->supervisor->supervisor->name:null }}</b> <br>
                    Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->supervisor) && !empty($ipc->user->supervisor->supervisor->position))? $ipc->user->supervisor->supervisor->position->name:null }} <br>
                    Tarikh : <br>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="padding:2px">
                    <b>Diperaku dan disahkan kerja/perkhidmatan/bekalan telah sempurna dilaksanakan seperti tuntutan yang dikemukakan untuk tujuan pembayaran</b></center><br><br><br><br>
                    Nama : <b>
                        @if(!empty($np_dept))
                        {{  (!empty($np_dept->honourary))? $np_dept->honourary:null}}&nbsp;{{(!empty($np_dept->name))? $np_dept->name:null }}
                        @endif
                    </b> <br>
                    Jawatan : {{ (!empty($np_dept) && !empty($np_dept->position) && !empty($np_dept->position->name))? $np_dept->position->name:null }} {{ (!empty($np_dept->department) && !empty($np_dept->department->name))? $np_dept->department->name:null }}<br>
                    Tarikh :
                </td>
            </tr>
            <tr>
                <td colspan="5" style="padding:2px">
                    <b>Diakui tuntutan ini telah disemak dan diperakui untuk tujuan pembayaran</b></center><br><br><br><br>
                    Nama : <b>{{ (!empty($pengesah))? $pengesah->name:null }}</b><br>
                    Jawatan : {{ (!empty($pengesah) && !empty($pengesah->position))? $pengesah->position->name:null }} <br>
                    Tarikh :
                </td>
            </tr>
        </table>
    </div>
</body>