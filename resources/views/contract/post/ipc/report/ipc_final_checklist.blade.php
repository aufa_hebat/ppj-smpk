<div style="text-align: right; font-size: 12px;">
    <i>BPUB/Senarai Semak/IPC Akhir (Biru)</i>
</div>
<div>
    <center><b><u>SENARAI SEMAKAN PENYEDIAAN PERAKUAN MUKTAMAD / IPC AKHIR</u></b></center>
</div>
<div>
    <table style="width:100%">
        <tr>
            <td style="width:25%">NAMA PROJEK</td>
            <td style="width:3%" valign="top">:</td>
            <td valign="top" style="text-transform: uppercase;">{{$ipc->sst->acquisition->title}}</td>
        </tr>
        <tr>
            <td>NO. KONTRAK/SEBUT HARGA</td>
            <td valign="top">:</td>
            <td valign="top">{{$ipc->sst->contract_no}}</td>
        </tr>
        <tr>
            <td>NAMA KONTRAKTOR</td>
            <td valign="top">:</td>
            <td valign="top" style="text-transform: uppercase;">
                @foreach($ipc->ipcInvoice as $ipses)
                    @if(!empty($ipses->company))
                        {{$ipses->company->company_name}} 
                    @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td>JABATAN/BAHAGIAN</td>
            <td valign="top">:</td>
            <td valign="top" style="text-transform: uppercase;">{{$ipc->sst->acquisition->department->name}}</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;width: 100%;">
        <tr>
            <th style="width: 5%">BIL</th>
            <th>PERKARA</th>
            <th style="width: 10%">PENYEDIA</th>
            <th style="width: 10%">BPUB</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Butiran Maklumat Kontrak **</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Perakuan Akaun Dan Bayaran Muktamad ** Berwarna Biru</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>
                Sijil Bayaran Interim (IPC Akhir) **
                <br>Semakan : 
                <label class="custom-control custom-checkbox">
                    <span class="custom-control-label">WJP telah dibayar </span>
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                </label>
                <label class="custom-control custom-checkbox">
                    <span class="custom-control-label">belum dibayar </span>
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                </label>
                Bon Pelaksanaan
            </td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>4</td>
            <td>Ringkasan Bayaran, Status Bayran Interim, Butiran Bayaran **</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>5</td>
            <td>Surat Tuntutan/Inbois/Inbois Cukai <i>(perlu ada bukti Cop Penerimaan oleh Jabatan)</i> **</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
            <tr>
                <td>6</td>
                <td>
                    Perakuan Pelarasan Jumlah Harga Kontrak (PPJHK) <i>Berwarna Hijau</i>
                    <br>**PPJHK No.............
                    <br>**PPJHK No.............
                    <br>*Jika Berkaitan
                </td>
                <td >
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                        <span class="custom-control-label"></span>
                    </label>
                </td>
                <td>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                        <span class="custom-control-label"></span>
                    </label>
                </td>
            </tr>
        <tr>
            <td>7</td>
            <td>
                Salinan Sijil Perakuan Kerja (CPC)
                <br>Salinan Perakuan Siap Kerja-Kerja Penyelenggaraan (CCMW)
            </td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>8</td>
            <td>Salinan Lanjutan Masa (EOT) No. ....... Hingga ........ *Jika Berkaitan</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>9</td>
            <td>Salinan Sijil Tidak Siap Kerja / CNC / LAD *Jika Berkaitan</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>10</td>
            <td>Salinan Sijil Siap Membaiki Kecacatan (CMGD) *Jika Berkaitan</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>11</td>
            <td>Surat Pengecualian LAD *Jika Berkaitan</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>12</td>
            <td>
                Salinan Sijil-Sijil IPC terdahulu yang telah dibayar**
                <br>Jumlah kesemua IPC Bil .................... hingga .....................
            </td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
        <tr>
            <td>13</td>
            <td>Akuan Statuori (Akuan Bersumpah) **</td>
            <td >
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" disabled>
                    <span class="custom-control-label"></span>
                </label>
            </td>
        </tr>
    </table>
</div>
<div>
    <table>
        <tr>
            <td valign="top"><u>Nota : </u></td>
            <td>
                1. Setiap flysheet menggunakan kertas berwarna dan ditagging.
                <br>2. * Sekiranya berkaitan
                <br>3. ** Perlu dipatuhi dan maklumat yang diperlukan
            </td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <th colspan="3">Disediakan Oleh</th>
            <th colspan="3">Disemak Oleh (BPUB)</th>
        </tr>
        <tr>
            <td style="width: 15%;">Tandatangan & Cop Jawatan</td>
            <td valign="top">:</td>
            <td></td>
            <td style="width: 15%;">Tandatangan & Cop Jawatan</td>
            <td valign="top">:</td>
            <td></td>
        </tr>
        <tr>
            <td>Tarikh</td>
            <td valign="top">:</td>
            <td></td>
            <td>Tarikh</td>
            <td valign="top">:</td>
            <td></td>
        </tr>
    </table>
</div>