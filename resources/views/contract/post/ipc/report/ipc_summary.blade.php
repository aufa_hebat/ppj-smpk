<script src="{{ asset('js/jquery.js') }}"></script>
<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
        .newbr {
            border-left: hidden;
            border-right: hidden;
        }
</style>

<div style="text-align: right" class="content">
    <i>BPUB/RINGKASAN BAYARAN/IPC</i>
</div>

<div class="title" style="text-align:center;font-weight:bold">
    RINGKASAN BAYARAN
</div>

<div style="text-transform: uppercase;text-align:center;font-weight:bold" class="title">
    {{ (!empty($ipc))? $ipc->sst->acquisition->title:null }}
</div>

<div class="title" style="text-align:center;font-weight:bold"> 
    No Kontrak : {{ (!empty($ipc))? $ipc->sst->contract_no:null }}
</div>

<div class="title" style="font-weight:bold">
    IPC NO. @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                {{ $ipc->penalty }}
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                {{ $ipc->ipc_no }}
            @elseif($ipc->penalty != null && $ipc->last_ipc == 'y')
                {{ $ipc->penalty."(AKHIR)" }}
            @else
                {{ $ipc->ipc_no."(AKHIR)" }}
            @endif
</div><br>

<div class="table-responsive" class="content">
    <table id="myTable" border="1" style="border-collapse: collapse;width: 100%;" class="content">
        <thead>
            <tr>
                <th style="background-color: lightgray;text-align:center" width="5%" valign="top">SEKSYEN</th>
                <th style="background-color: lightgray;text-align:center" width="20%" valign="top">PERKARA</th>
                <th style="background-color: lightgray;text-align:center" width="15%" valign="top">JUMLAH KONTRAK (RM)</th>
                <th style="background-color: lightgray;text-align:center" width="15%" valign="top">JUMLAH BAYARAN TERKINI (RM)</th>
                <th style="background-color: lightgray;text-align:center" width="15%" valign="top">JUMLAH BAYARAN TERDAHULU (RM)</th>
                <th style="background-color: lightgray;text-align:center" width="15%" valign="top">JUMLAH BAYARAN SEMASA / DISYORKAN (RM)</th>
                <th style="background-color: lightgray;text-align:center" width="15%" valign="top">CATATAN</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($elemen) && $elemen->count() > 0)
            @foreach($elemen as $elmn)
            <tr>
                <td style="text-align:center" valign="top">{{ $elmn->no }}.0.0</td>
                <td style="padding:4px">{{ $elmn->element }}</td>
                <td style="text-align:right;padding:4px">
                    @if( !empty($elmn) && $elmn->amount_adjust > 0)
                        {{ money()->toCommon($elmn->amount_adjust , 2) }}
                    @else
                        -
                    @endif
                </td>
                <!-- terkini -->
                <td style="text-align:right;padding:4px">
                    @if(!empty($ipc_bq) && $ipc_bq != null)
                        @if($ipc_bq->where('status','N') && $elmn->id != $prov_sum && $ipc_bq->where('bq_element_id', $elmn->id)->pluck('amount')->sum() > 0)
                            {{ money()->toCommon($ipc_bq->where('bq_element_id', $elmn->id)->where('status','N')->pluck('amount')->sum(), 2) }}
                        @elseif($ipc_bq->where('status','V') && $elmn->id == $prov_sum && $ipc_bq->where('status', 'V')->pluck('amount')->sum() > 0)
                            {{ money()->toCommon($ipc_bq->where('status', 'V')->pluck('amount')->sum(), 2) }}
                        @else
                            -
                        @endif
                    @else
                        -
                    @endif
                </td>
                <!-- terkini -->
                <!-- terdahulu -->
                <td style="text-align:right;padding:4px">
                    @if(!empty($bq_pre) && $bq_pre != null)
                        @if($bq_pre->where('status','N') && $elmn->id != $prov_sum && $bq_pre->where('bq_element_id', $elmn->id)->pluck('amount')->sum() > 0)
                            {{ money()->toCommon($bq_pre->where('bq_element_id', $elmn->id)->where('status','N')->pluck('amount')->sum(), 2) }}
                        @elseif($bq_pre->where('status','V') && $elmn->id == $prov_sum && $bq_pre->where('status', 'V')->pluck('amount')->sum() > 0)
                            {{ money()->toCommon($bq_pre->where('status', 'V')->pluck('amount')->sum(), 2) }}
                        @else
                            -
                        @endif
                    @else
                        -
                    @endif
                </td>
                <!-- terdahulu -->
                <!-- semasa -->
                <td style="text-align:right;padding:4px">
                    @if(!empty($ipc) && !empty($ipc->ipcInvoice) && !empty($ipc->ipcInvoice->pluck('ipcBq')) && !empty($ipc->ipcInvoice->pluck('ipcBq')->where('bq_element_id',$elmn->id)))
                        @php $sum_amount = 0; @endphp
                        @foreach($ipc->ipcInvoice as $inv)
                            @if($prov_sum != $elmn->id && !empty($inv->ipcBq->where('bq_element_id',$elmn->id)))
                            @php 
                                if($sum_amount == 0){
                                    $sum_amount = $inv->ipcBq->where('bq_element_id',$elmn->id)->where('status','N')->pluck('amount')->sum();
                                }else{
                                    $sum_amount = $sum_amount + $inv->ipcBq->where('bq_element_id',$elmn->id)->where('status','N')->pluck('amount')->sum();
                                }
                            @endphp
                            @elseif(!empty($prov_sum) && $prov_sum != null && $prov_sum == $elmn->id && !empty($inv->ipcBq->where('status','V')))
                                @php 
                                if($sum_amount == 0){
                                    $sum_amount = $inv->ipcBq->where('status','V')->pluck('amount')->sum();
                                }else{
                                    $sum_amount = $sum_amount + $inv->ipcBq->where('status','V')->pluck('amount')->sum();
                                }
                                @endphp
                            @endif
                        @endforeach
                        @if($sum_amount > 0)
                            {{ money()->toCommon($sum_amount , 2) }}
                        @else
                            -
                        @endif
                    @else
                    -
                    @endif
                </td>
                <!-- semasa -->
                <td></td>
            </tr>
            @endforeach
            @endif
            <tr>
                <td colspan="2" style="background-color: lightgray;font-weight:bold;padding:4px">JUMLAH KESELURUHAN</td>
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                        @if(!empty($elemen->pluck('amount_adjust')) && $elemen->pluck('amount_adjust')->sum() > 0)
                            {{money()->toCommon($elemen->pluck('amount_adjust')->sum() ,2)}}
                        @else
                            -
                        @endif
                    
                </td>
                <!-- terkini -->
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                        @if(!empty($ipc_bq) && $ipc_bq != null && $ipc_bq->pluck('amount')->sum() > 0)
                            {{ money()->toCommon($ipc_bq->pluck('amount')->sum() , 2) }}
                        @else
                            -
                        @endif
                    
                </td>
                <!-- terkini -->
                <!-- terdahulu -->
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                    @if(!empty($bq_pre) && $bq_pre != null && $bq_pre->pluck('amount')->sum() > 0)
                        {{ money()->toCommon($bq_pre->pluck('amount')->sum() , 2) }}
                    @else
                        -
                    @endif
                    
                </td>
                <!-- terdahulu -->
                <!-- semasa -->
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                        @if(!empty($ipc) && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->pluck('demand_amount')->sum() > 0)
                            {{ money()->toCommon($ipc->ipcInvoice->pluck('demand_amount')->sum() , 2) }}
                        @else
                            -
                        @endif
                    
                </td>
                <!-- semasa -->
                <td style="background-color: lightgray"></td>
            </tr>
            @php 
                $tot_sum_vo = 0; $tot_vo_amount = 0; $tot_vo_pre_amount = 0;
            @endphp
            @if(!empty($ipc->working_change_amount) && !empty($elemen_vo))
                @foreach($elemen_vo as $vos)
                @php $sum_vo = 0; @endphp
                    @if($vos->ppjhk->status == 1)
                    <tr>
                        <td style="text-align:center;font-weight:bold">PPJHK No.{{ $vos->ppjhk->no }}</td>
                        <td style="font-weight:bold;padding:4px">{{ $vos->voElement->element }}</td>
                        <td style="text-align:right;padding:4px;">
                            @php 
                                if($sum_vo == 0){
                                    $sum_vo = $vos->net_amount;
                                }else{
                                    $sum_vo = $sum_vo + $vos->net_amount;
                                }
                            @endphp
                            @if($sum_vo > 0)
                                {{ money()->toCommon($sum_vo,2) }}
                            @elseif($sum_vo < 0)
                                {{ "(" . money()->toCommon(-$sum_vo,2) .")" }}
                            @else
                                -
                            @endif
                            {{-- {{ money()->toCommon($vos->net_amount ?? 0,2) }} --}}
                        </td>
                        <!-- terkini -->
                        <td style="text-align:right;padding:4px">
                            @if(!empty($ipc_kumpul) && !empty($ipc_kumpul->pluck('ipcVo')) && !empty($ipc_kumpul->pluck('ipcVo')->where('ppjhk_element_id',$vos->id)))
                                @php $vo_amount = 0; @endphp
                                @foreach($ipc_kumpul as $vo_kump)
                                    @if(!empty($vo_kump->ipcVo->where('ppjhk_element_id',$vos->id)))
                                        @if($vo_amount == 0)
                                            @php $vo_amount = $vo_kump->ipcVo->where('ppjhk_element_id',$vos->id)->pluck('amount')->sum(); @endphp 
                                        @else
                                            @php $vo_amount = $vo_amount + $vo_kump->ipcVo->where('ppjhk_element_id',$vos->id)->pluck('amount')->sum(); @endphp 
                                        @endif
                                    @endif
                                @endforeach
                                @if($vo_amount > 0)
                                    {{ money()->toCommon($vo_amount,2) }}
                                @elseif($vo_amount < 0)
                                    {{ "(" . money()->toCommon(-$vo_amount,2) . ")" }}
                                @else
                                    -
                                @endif
                                
                            @else
                            -
                            @endif
                        </td>
                        <!-- terkini -->
                        <!-- terdahulu -->
                        <td style="text-align:right;padding:4px">
                            @if(!empty($ipc_pre) && !empty($ipc_pre->pluck('ipcVo')) && !empty($ipc_pre->pluck('ipcVo')->where('ppjhk_element_id',$vos->id)))
                                @php $vo_pre_amount = 0; @endphp
                                @foreach($ipc_pre as $vo_pre)
                                    @if(!empty($vo_pre->ipcVo->where('ppjhk_element_id',$vos->id)))
                                        @if($vo_pre_amount == 0)
                                            @php $vo_pre_amount = $vo_pre->ipcVo->where('ppjhk_element_id',$vos->id)->pluck('amount')->sum(); @endphp 
                                        @else
                                            @php $vo_pre_amount = $vo_pre_amount + $vo_pre->ipcVo->where('ppjhk_element_id',$vos->id)->pluck('amount')->sum(); @endphp 
                                        @endif
                                    @endif
                                @endforeach
                                @if($vo_pre_amount > 0)
                                    {{ money()->toCommon($vo_pre_amount,2) }}
                                @elseif($vo_pre_amount < 0)
                                    {{ "(" . money()->toCommon(-$vo_pre_amount,2) . ")" }}
                                @else
                                    -
                                @endif
                                
                            @else
                            -
                            @endif
                        </td>
                        <!-- terdahulu -->
                        <!-- semasa -->
                        <td style="text-align:right;padding:4px">
                            @if(!empty($ipc) && !empty($ipc->ipcVo) && $ipc->ipcVo->count() > 0 && !empty($ipc->ipcVo->where('ppjhk_element_id',$vos->id)))
                                @if($ipc->ipcVo->where('ppjhk_element_id',$vos->id)->sum('amount') > 0)
                                    {{ money()->toCommon($ipc->ipcVo->where('ppjhk_element_id',$vos->id)->sum('amount'), 2) }}
                                @elseif($ipc->ipcVo->where('ppjhk_element_id',$vos->id)->sum('amount') < 0)
                                    {{ "(" . money()->toCommon(-$ipc->ipcVo->where('ppjhk_element_id',$vos->id)->sum('amount'), 2) . ")" }}
                                @endif
                            @else
                            -
                            @endif
                        </td>
                        <!-- semasa -->
                        <td></td>
                    </tr>
                    @php 
                        if($tot_sum_vo == 0){
                            $tot_sum_vo = $sum_vo;
                        }else{
                            $tot_sum_vo = $tot_sum_vo + $sum_vo;
                        }
                        if($tot_vo_amount == 0){
                            $tot_vo_amount = $vo_amount;
                        }else{
                            $tot_vo_amount = $tot_vo_amount + $vo_amount;
                        }
                        if($tot_vo_pre_amount == 0){
                            $tot_vo_pre_amount = $vo_pre_amount;
                        }else{
                            $tot_vo_pre_amount = $tot_vo_pre_amount + $vo_pre_amount;
                        }
                    @endphp
                    @endif
                @endforeach
            @endif
            <tr>
                <td colspan="2" style="background-color: lightgray;font-weight:bold;padding:4px">JUMLAH KESELURUHAN SELEPAS PELARASAN</td>
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                        @if(($tot_sum_vo ?? 0) + $elemen->pluck('amount_adjust')->sum() > 0)
                            {{ money()->toCommon((($tot_sum_vo ?? 0) + $elemen->pluck('amount_adjust')->sum()) ,2) }}
                        @elseif(($tot_sum_vo ?? 0) + $elemen->pluck('amount_adjust')->sum() < 0)
                            {{ "(" . money()->toCommon(-(($tot_sum_vo ?? 0) + $elemen->pluck('amount_adjust')->sum()) ,2) . ")" }}
                        @else
                            -
                        @endif
                    
                </td>
                <!-- terkini -->
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                        
                        @if(!empty($ipc_bq) && $ipc_bq != null && (($tot_vo_amount ?? 0) + $ipc_bq->pluck('amount')->sum()) > 0)
                            {{ money()->toCommon((($tot_vo_amount ?? 0) + $ipc_bq->pluck('amount')->sum()), 2) }}
                        @elseif(empty($ipc_bq) && !empty($tot_vo_amount) && $tot_vo_amount > 0)
                            {{ money()->toCommon($tot_vo_amount , 2) }}
                        @elseif(!empty($ipc_bq) && $ipc_bq != null && (($tot_vo_amount ?? 0) + $ipc_bq->pluck('amount')->sum()) < 0)
                            {{ "(" . money()->toCommon(-(($tot_vo_amount ?? 0) + $ipc_bq->pluck('amount')->sum()), 2) .")" }}
                        @elseif(empty($ipc_bq) && !empty($tot_vo_amount) && $tot_vo_amount < 0)
                            {{ "(" . money()->toCommon(-$tot_vo_amount , 2) .")" }}
                        @else
                            -
                        @endif
                    
                </td>
                <!-- terkini -->
                <!-- terdahulu -->
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                    @if(!empty($bq_pre) && $bq_pre != null && (($tot_vo_pre_amount ?? 0) + $bq_pre->pluck('amount')->sum()) > 0)
                        {{ money()->toCommon((($tot_vo_pre_amount ?? 0) + $bq_pre->pluck('amount')->sum()), 2) }}
                    @elseif(empty($bq_pre) && !empty($tot_vo_pre_amount) && $tot_vo_pre_amount > 0)
                        {{ money()->toCommon($tot_vo_pre_amount , 2) }}
                    @elseif(!empty($bq_pre) && $bq_pre != null && (($tot_vo_pre_amount ?? 0) + $bq_pre->pluck('amount')->sum()) < 0)
                        {{ "(" . money()->toCommon(-(($tot_vo_pre_amount ?? 0) + $bq_pre->pluck('amount')->sum()), 2) . ")" }}
                    @elseif(empty($bq_pre) && !empty($tot_vo_pre_amount) && $tot_vo_pre_amount < 0)
                        {{ "(" . money()->toCommon(-$tot_vo_pre_amount , 2) . ")" }}
                    @else
                        -
                    @endif
                    
                </td>
                <!-- terdahulu -->
                <!-- semasa -->
                <td style="text-align:right;background-color: lightgray;font-weight:bold;padding:4px">
                    
                        @php 
                            $cur_vo = (!empty($ipc) && !empty($ipc->ipcVo) && !empty($ipc->ipcVo))? $ipc->ipcVo->pluck('amount')->sum():0;
                        @endphp
                        
                        @if(!empty($ipc) && !empty($ipc->ipcInvoice) && ($cur_vo + $ipc->ipcInvoice->pluck('demand_amount')->sum()) > 0)
                            {{ money()->toCommon(($cur_vo + $ipc->ipcInvoice->pluck('demand_amount')->sum()) , 2) }}
                        @elseif(empty($ipc) && empty($ipc->ipcInvoice) && $cur_vo > 0)
                            {{ money()->toCommon($cur_vo , 2) }}
                        @elseif(!empty($ipc) && !empty($ipc->ipcInvoice) && ($cur_vo + $ipc->ipcInvoice->pluck('demand_amount')->sum()) < 0)
                            {{ "(" . money()->toCommon(-($cur_vo + $ipc->ipcInvoice->pluck('demand_amount')->sum()) , 2) . ")" }}
                        @elseif(empty($ipc) && empty($ipc->ipcInvoice) && $cur_vo < 0)
                            {{ "(" . money()->toCommon(-$cur_vo , 2) . ")" }}
                        @else
                            -
                        @endif
                    
                </td>
                <!-- semasa -->
                <td style="background-color: lightgray"></td>
            </tr>
        </tbody>
    </table>
</div><br>

<div class="content">
    Kami bertanggungjawab dengan perakuan dan pengesahan kerja/perkhidmatan/bekalan ini telah dinilai dan dilaksanakan dengan sempurna sebagaimana bayaran/tuntutan
</div><br>

<div class="content">
    <table style="width: 100%">
        <tr>
            <td>
                DISEDIAKAN OLEH <br><br>
                Nama : {{ (!empty($ipc->user))? $ipc->user->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->position))? $ipc->user->position->name: null }}<br>
                Tarikh :
            </td>
            <td>
                DISEMAK OLEH <br><br>
                Nama : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor))? $ipc->user->supervisor->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->position))? $ipc->user->supervisor->position->name:null }}<br>
                Tarikh :
            </td>
            <td>
                DISAHKAN OLEH <br><br>
                Nama : {{ (!empty($ipc->user && !empty($ipc->user->supervisor) && !empty(!empty($ipc->user->supervisor->supervisor))))? $ipc->user->supervisor->supervisor->name:null }}<br>
                Jawatan : {{ (!empty($ipc->user) && !empty($ipc->user->supervisor) && !empty($ipc->user->supervisor->supervisor) && !empty($ipc->user->supervisor->supervisor->position))? $ipc->user->supervisor->supervisor->position->name:null }}<br>
                Tarikh :
            </td>
        </tr>
    </table>
</div>
