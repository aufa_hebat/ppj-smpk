<h5>Senarai Sijil Interim</h5>
<div class="row justify-content-center">
    <div class="col-12" >
        @role('penyedia')
        @if(empty($sst->ipc) || ($date_valid != null && $sst->ipc->count() < 3 && (empty($sst->document2) || !empty($sst->document2) || empty($sst->document2->revenue_stamp_date) || $sst->document2->revenue_stamp_date == null || $sst->document2->revenue_stamp_date != null)) || ($sst->ipc->count() > 2 && !empty($sst->document2) && $sst->document2->revenue_stamp_date != null && !empty($sst->document2->revenue_stamp_date)) )
        <button class="new_ipc btn btn-primary float-right" 
            @if($sst->ipc->where('ipc_no',$sst->ipc->count())->pluck('status')->first() == 1 || $sst->ipc->where('last_ipc','y')->where('deleted_at',null)->count() > 0)  
            disabled = "disabled"
            @endif
            >Tambah Sijil Interim</button>
        @endif
        @endrole
        @component('components.card')
            @slot('card_body')
            <div class="table-responsive">
                <table id="table_owner" class="table table-sm table-transparent table-hover" style="min-height:100px">
                    <thead>
                        <th width='10%'>NO SIJIL INTERIM</th>
                        <th width='10%'>PEMBAYARAN UNTUK</th>
                        <th width='15%'>AMAUN TUNTUTAN (RM)</th>
                        <th width='20%'>DOKUMEN ID</th>
                        <th width='20%'>BAUCER ID</th>
                        <th width='20%'>STATUS BAYARAN</th>
                        <th width='5%'>TINDAKAN</th>
                    </thead>
                    @if(!empty($sst->ipc))
                        @foreach($sst->ipc as $k => $ipc)
                        @php
                        $status_remark = '';
                        
                        @endphp
                        <tbody>
                            <tr>
                                <td>{{ $ipc->ipc_no }}</td>
                                <td>{{ $ipc->short_text }}</td>
                                <td>{{ money()->toCommon($ipc->demand_amount ?? 0,2) }}</td>
                                <td>
                                    @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                                        @foreach($ipc->ipcInvoice as $inv)
                                            {{ $inv->sap_doc_data }}<br>
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                                        @foreach($ipc->ipcInvoice as $inv)
                                            {{ $inv->sap_bank_no }}<br>
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                                        @foreach($ipc->ipcInvoice as $inv)
                                            @if (2 == $inv->status)
                                                Semakan<br>
                                            @elseif (3 == $inv->status)
                                                Sedang diproses di SAP<br>
                                            @elseif (4 == $inv->status)
                                                Penyediaan Baucer<br>
                                            @else
                                                Draf<br>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td><div class="text-center">
                                        <div class="item-action dropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                                              <i class="fe fe-more-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                                              style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                
                                                @if($ipc->status == 1)
                                                @role('penyedia')
                                                <a class="dropdown-item edit-action-btn" href="{{ route('contract.post.ipc.ipc_invoice_edit',['hashslug' => $ipc->hashslug]) }}" data-backdrop="static">
                                                    <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                                </a>
                                                    @if(empty($ipc->history_semakan) || (!empty($ipc->history_semakan) && $ipc->history_semakan->count() < 1))
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item destroy-ipc-action-btn"
                                                            data-hashslug="{{ $ipc->hashslug }}">
                                                            <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
                                                        </a>
                                                    @endif
                                                @endrole
                                                @hasanyrole('developer|administrator')
                                                    <a class="dropdown-item edit-action-btn" href="{{ route('contract.post.ipc.ipc_show',['hashslug' => $ipc->hashslug]) }}" data-backdrop="static">
                                                        <i class="fe fe-edit text-primary"></i> {{ __('Butiran') }}
                                                    </a>
                                                @endrole
                                                @else
                                                <a class="dropdown-item edit-action-btn" href="{{ route('contract.post.ipc.ipc_show',['hashslug' => $ipc->hashslug]) }}" data-backdrop="static">
                                                    <i class="fe fe-edit text-primary"></i> {{ __('Butiran') }}
                                                </a>
                                                @endif
                                          </div>
                                        </div>
                                    </div></td>
                            </tr>
                        </tbody>
                        @endforeach
                    @endif
                </table>
            </div>
            @endslot                
        @endcomponent
    </div>
</div>
