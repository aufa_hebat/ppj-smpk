@extends('layouts.admin')
@push('scripts')
    <script type="text/javascript">

        jQuery(document).ready(function($) {

            $('#sap_send').click(function(){
                var route_name = 'contract.post.sap';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    hashslug : $('#hashslug').val()
                };
                console.log(data);
                axios.post(route(route_name), data).then(response => {
                    swal('Hantar Dokumen Perolehan', response.data.message, 'success').then((result)=>{
                        $('#sap_send').hide();
                    });
                });
            });   

            @if(!empty($ipc_latest) && !empty($ipc_latest->doc_sijil))
                var urlipc_cert = "/uploads/{{$ipc_latest->doc_sijil->document_name}}";

                $('.newwindowipc_cert{{$ipc_latest->doc_sijil->document_id}}').click(function () {

                    var paramsipc_cert = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_cert = window.open(urlipc_cert, 'website', paramsipc_cert);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->doc_summary))
                var urlipc_summary = "/uploads/{{$ipc_latest->doc_summary->document_name}}";

                $('.newwindowipc_summary{{$ipc_latest->doc_summary->document_id}}').click(function () {

                    var paramsipc_summary = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_summary = window.open(urlipc_summary, 'website', paramsipc_summary);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif
            
            @if(!empty($ipc_latest) && !empty($ipc_latest->doc_status))
                var urlipc_status = "/uploads/{{$ipc_latest->doc_status->document_name}}";

                $('.newwindowipc_status{{$ipc_latest->doc_status->document_id}}').click(function () {

                    var paramsipc_status = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_status = window.open(urlipc_status, 'website', paramsipc_status);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif
            
            @if(!empty($ipc_latest) && !empty($ipc_latest->ipcInvoice) && !empty($ipc_latest->ipcInvoice->pluck('document')))
                var urlipc_invoice = "/uploads/{{$ipc_latest->ipcInvoice->pluck('document')->pluck('document_name')->first()}}";

                $('.newwindowipc_invoice{{$ipc_latest->ipcInvoice->pluck('document')->pluck('document_id')->first()}}').click(function () {

                    var paramsipc_invoice = "menubar=no,titlebar=no,invoice=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_invoice = window.open(urlipc_invoice, 'website', paramsipc_invoice);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->sst->document))
                var urlipc_sst = "/uploads/{{$ipc_latest->sst->document->document_name}}";

                $('.newwindowipc_sst{{$ipc_latest->sst->document->document_id}}').click(function () {

                    var paramsipc_sst = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_sst = window.open(urlipc_sst, 'website', paramsipc_sst);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->sst->insurance) && !empty($ipc_latest->sst->insurance->documents_publicLiability))
                @foreach($ipc_latest->sst->insurance->documents_publicLiability as $public)
                    $('#subFile{{$public->id}}').val('{{ $public->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->sst->insurance) && !empty($ipc_latest->sst->insurance->documents_work))
                @foreach($ipc_latest->sst->insurance->documents_work as $work)
                    $('#subFile{{$work->id}}').val('{{ $work->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->sst->insurance) && !empty($ipc_latest->sst->insurance->documents_compensation))
                @foreach($ipc_latest->sst->insurance->documents_compensation as $compensation)
                    $('#subFile{{$compensation->id}}').val('{{ $compensation->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->sst->bon) && !empty($ipc_latest->sst->bon->documents))
                @foreach($ipc_latest->sst->bon->documents as $bon)
                    $('#subFile{{$bon->id}}').val('{{ $bon->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->doc_perkeso))
                var urlipc_perkeso = "/uploads/{{$ipc_latest->doc_perkeso->document_name}}";

                $('.newwindowipc_perkeso{{$ipc_latest->doc_perkeso->document_id}}').click(function () {

                    var paramsipc_perkeso = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_perkeso = window.open(urlipc_perkeso, 'website', paramsipc_perkeso);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->sst) && !empty($ipc_latest->sst->company) && !empty($ipc_latest->sst->company->gst))
                var urlipc_gst = "/uploads/{{$ipc_latest->sst->company->gst->document_name}}";

                $('.newwindowipc_gst{{$ipc_latest->sst->company->gst->document_id}}').click(function () {

                    var paramsipc_gst = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_gst = window.open(urlipc_gst, 'website', paramsipc_gst);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif

            @if(!empty($ipc_latest) && !empty($ipc_latest->doc_levi))
                var urlipc_levi= "/uploads/{{$ipc_latest->doc_levi->document_name}}";

                $('.newwindowipc_levi{{$ipc_latest->doc_levi->document_id}}').click(function () {

                    var paramsipc_levi= "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowipc_levi= window.open(urlipc_levi, 'website', paramsipc_levi);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endif

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_pekeso').change(function(){
                $('#subFile_pekeso').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_cert').change(function(){
                $('#subFile_cert').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_summary').change(function(){
                $('#subFile_summary').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_status').change(function(){
                $('#subFile_summary').val($(this).val().split('\\').pop());
            });


            // button for review
            $(".savDraf").on('click', function(e){
               $("#saveStatus").val(1);
            });
            $(".savSent").on('click', function(){
                $("#sentStatus").val(1);
            });
            $(".rejSent").on('click', function(){
                $("#rejectStatus").val(1);
            });

            $(".s1").on('click', function(e){
               $("#semak1").val(1);
            });
            $(".s2").on('click', function(){
                $("#semak2").val(1);
            });
            $(".s3").on('click', function(){
                $("#semak3").val(1);
            });

            $('#penyemakpp').show();
            $('#pengesahpp').hide();
            $('#penyemakbpub').hide();

            // $('input[type=select][name=semakan]').change(function () {

            //     if(this.value == '1'){
            //         $('#penyemakpp').show();
            //         $('#pengesahpp').hide();
            //         $('#penyemakbpub').hide();
            //     }
            //     else if (this.value == '2'){
            //         $('#penyemakpp').hide();
            //         $('#pengesahpp').show();
            //         $('#penyemakbpub').hide();
            //     }
            //     else if (this.value == '3'){
            //         $('#penyemakpp').hide();
            //         $('#pengesahpp').hide();
            //         $('#penyemakbpub').show();
            //     }
            // });
            if($('#semakan').val() == 1){
                $('#penyemakpp').show();
                $('#pengesahpp').hide();
                $('#penyemakbpub').hide();  
            }else if($('#semakan').val() == 2){
                $('#penyemakpp').hide();
                $('#pengesahpp').show();
                $('#penyemakbpub').hide();                                 
            }else if($('#semakan').val() == 3){
                $('#penyemakpp').hide();
                $('#pengesahpp').hide();
                $('#penyemakbpub').show();                              
            }else{
                $('#penyemakpp').hide();
                $('#pengesahpp').hide();
                $('#penyemakbpub').hide();                   
            }

            $('#semakan').change(function () {
                if($('#semakan').val() == 1){
                    $('#penyemakpp').show();
                    $('#pengesahpp').hide();
                    $('#penyemakbpub').hide();  
                }else if($('#semakan').val() == 2){
                    $('#penyemakpp').hide();
                    $('#pengesahpp').show();
                    $('#penyemakbpub').hide();                                 
                }else if($('#semakan').val() == 3){
                    $('#penyemakpp').hide();
                    $('#pengesahpp').hide();
                    $('#penyemakbpub').show();                              
                }else{
                    $('#penyemakpp').hide();
                    $('#pengesahpp').hide();
                    $('#penyemakbpub').hide();                   
                }
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($s8) && $s8 = $review)
                @if($s8->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s8->remarks) !!}');
                @endif
            @elseif(!empty($s9) && $s9 = $review)
                @if($s9->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s9->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif
        });


        @foreach($levi_doc as $db)
            var url = "/uploads/{{$db->document_name}}";

            $('.newwindow{{$db->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($pekeso_doc as $db_pekeso)
            var urlpekeso = "/uploads/{{$db_pekeso->document_name}}";

            $('.newwindowpekeso{{$db_pekeso->document_id}}').click(function () {

                var paramspekeso = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowpekeso = window.open(urlpekeso, 'website', paramspekeso);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($cert_doc as $db_cert)
            var urlcert = "/uploads/{{$db_cert->document_name}}";

            $('.newwindowcert{{$db_cert->document_id}}').click(function () {

                var paramscert = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowcert = window.open(urlcert, 'website', paramscert);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($summary_doc as $db_sum)
            var urlsum = "/uploads/{{$db_sum->document_name}}";

            $('.newwindowsum{{$db_sum->document_id}}').click(function () {

                var paramssum = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowsum = window.open(urlsum, 'website', paramssum);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($status_doc as $db_status)
            var urlstatus = "/uploads/{{$db_status->document_name}}";

            $('.newwindowstatus{{$db_status->document_id}}').click(function () {

                var paramsstatus = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowstatus = window.open(urlstatus, 'website', paramsstatus);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach
    </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{!! $sst->acquisition->title !!}
        <br>
        <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
        <br>
        <span class="font-weight-bold"> No Sijil Interim: </span> @if($ipc->count() > 0) @foreach($ipc as $ip) {{$ip->ipc_no}}, @endforeach @endif
        @endslot
    @endcomponent
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#ipc-checklist" role="tab" aria-controls="ipc-checklist" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Senarai Semakan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#ipc-details" role="tab" aria-controls="ipc-details" aria-selected="false">
                    @icon('fe fe-folder')&nbsp;{{ __('Ringkasan Pembayaran') }}
                </a>
            </li>
            @if(!empty($review))
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ipc-reviews" role="tab" aria-controls="ipc-reviews" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Ulasan Semakan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#ipc-review-log" role="tab" aria-controls="ipc-review-log" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif

                @if((user()->id == $review->approved_by && $review->type == 'IPC') || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && (empty($semakan4))))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#ipc-review" role="tab" aria-controls="ipc-review" aria-selected="false">
                            @icon('fe fe-edit text-primary')&nbsp;{{ __('Semakan') }}
                        </a>
                    </li>
                @endif
                 {{-- || ((!empty($semakan6)) && ($semakan6->status == 'S6' && $semakan6->approved_by == NULL) && user()->department->id == '3') --}}

            @endif

            @role('penyedia')
                @if(empty($review) && !empty($review_previous) && $review_previous->created_by == user()->id)
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#ipc-review" role="tab" aria-controls="ipc-review" aria-selected="false">
                            @icon('fe fe-edit text-primary')&nbsp;{{ __('Semakan') }}
                        </a>
                    </li>
                @endif
            @endrole
        </ul>
    </div>
    @component('components.card', ['card_classes' => 'col-10'])
        @slot('card_body')
            
            <div class="tab-content" id="ipc-tab-content">
                <div class="tab-pane fade show active " id="ipc-checklist" role="tabpanel" aria-labelledby="ipc-checklist-tab">
                    @include('contract.post.ipc.partials.show.checklist')
                    <div class="btn-group float-right">
                        <a href="{{ route('contract.post.ipc.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                    </div>
                </div>
                <div class="tab-pane fade show " id="ipc-details" role="tabpanel" aria-labelledby="ipc-details-tab">
                    @include('contract.post.ipc.partials.show.details')
                </div>
                <div class="tab-pane fade show" id="ipc-reviews" role="tabpanel" aria-labelledby="ipc-details-tab">
                    
                    @role('penyedia')

                        @if((!empty($review)) && (user()->id == $review->created_by))

                            {{-- penyedia --}}
                                
                            @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                Ulasan Semakan Oleh {{$cetaknotisklog->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl7 as $s7log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s7log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl6 as $s6log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s6log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl5 as $s5log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl4 as $s4log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl3 as $s3log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl2 as $s2log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                <div class="mb-0 card card-primary">
                                    <div class="card-header" role="tab" id="headingOne2">
                                        <h4 class="card-title">
                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                        <div class="card-body">
                                            @foreach($sl1 as $s1log)
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Terima</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Tarikh Hantar</div>
                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="text-muted">Status</div>
                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="text-muted">Ulasan</div>
                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endif

                    @else

                        @if(!empty($review))
                            {{-- pegawai pelaksana --}}
                            @if(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)
                                
                                @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif

                            {{-- pegawai bpub --}}
                            @if(user()->department->id == '9')
                                
                                @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                    Ulasan Semakan Oleh {{$cetaknotisklog->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s7log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s7log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                    Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl6 as $s6log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s6log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                    Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl4 as $s4log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                    Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl3 as $s3log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif
                        @endif

                    @endrole

                </div>
                <div class="tab-pane fade show " id="ipc-review-log" role="tabpanel" aria-labelledby="ipc-review-log-tab">
                    <form id="log-form">
                        <div class="row justify-content-center w-100">
                            <div class="col-12 w-100">
                                @include('contract.pre.box.partials.scripts')
                                @component('components.card')
                                    @slot('card_body')
                                        @component('components.datatable', 
                                            [
                                                'table_id' => 'contract-pre-box',
                                                'route_name' => 'api.datatable.contract.review-log-ipc',
                                                'param' => 'acquisition_id=' . $sst->acquisition_id,
                                                'columns' => [
                                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                    ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                    ['data' => 'tarikh', 'title' => __('Tarikh Semakan'), 'defaultContent' => '-'],
                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                ],
                                                'headers' => [
                                                    __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Semakan'), __('Status'), __('')
                                                ],
                                                'actions' => minify('')
                                            ]
                                        )
                                        @endcomponent
                                    @endslot
                                @endcomponent
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade show" id="ipc-review" role="tabpanel" aria-labelledby="ipc-review-tab">
                    <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                        @csrf

                        @include('components.forms.hidden', [
                            'id' => 'id',
                            'name' => 'id',
                            'value' => ''
                        ])

                        @include('components.forms.hidden', [
                            'id' => 'acquisition_id',
                            'name' => 'acquisition_id',
                            'value' => $sst->acquisition_id
                        ])

                        @if(!empty($review))
                        <input type="hidden" name="task_by" value="{{$review->task_by}}">
                        <input type="hidden" name="law_task_by" value="{{$review->law_task_by}}">

                        <input type="hidden" name="created_by" value="{{$review->created_by}}">
                        @endif

                        @if(!empty($review_previous))
                            <input type="text" name="created_by" value="{{$review_previous->created_by}}" hidden>
                            <input type="text" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                            <input type="text" name="task_by" value="{{$review_previous->task_by}}" hidden>
                            <input type="text" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                            <input type="text" name="type" value="IPC" hidden>
                        @endif

                        <div class="row">
                            <div class="col-6">
                                @include('components.forms.input', [
                                    'input_label' => __('Nama Pegawai Semakan'),
                                    'id' => '',
                                    'name' => '',
                                    'value' => user()->name,
                                    'readonly' => true
                                ])
                            </div>
                                @include('components.forms.hidden', [
                                    'id' => 'requested_by',
                                    'name' => 'requested_by',
                                    'value' => user()->id
                                ])
                            <div class="col-3">
                                @if(!empty($review))
                                    @if(!empty($review->status))
                                        @include('components.forms.input', [
                                            'input_label' => __('Tarikh Terima'),
                                            'id' => 'requested_at',
                                            'name' => 'requested_at',
                                        ])
                                    @endif
                                @endif
                            </div>
                            <div class="col-3">
                                @if(!empty($review))
                                    @if(!empty($review->status))
                                        @include('components.forms.input', [
                                            'input_label' => __('Tarikh Hantar'),
                                            'id' => 'approved_at',
                                            'name' => 'approved_at',
                                        ])
                                    @endif
                                @endif
                            </div>
                        </div>
                                
                        @if(!empty($review) && $review->status == 'S5')
                            <div class="form-group row">
                                <label for="" class="col col-form-label">
                                    Serah Tugas Kepada
                                </label>
                                <select class="form-control input-lg" id="approved_by" name="approved_by" style="width: 100%;">
                                    <option value=""></option>
                                    @foreach($user as $users)
                                        <option value="{{$users->id}}">{{$users->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @else
                            @include('components.forms.hidden', [
                                'id' => 'approved_by',
                                'name' => 'approved_by',
                                'value' => user()->supervisor->id
                            ])
                        @endif

                        @if(!empty($review) && $review->created_by != user()->id)
                            @include('components.forms.textarea', [
                                'input_label' => __('Ulasan'),
                                'id' => 'remarks',
                                'name' => 'remarks'
                            ])
                        @else

                        <label>Hantar Kepada : </label>

                        <select id="semakan" name="semakan" class="form-control w-100">
                            <option value="">{{ __('Sila Pilih') }}</option>
                            <option value="1">{{user()->supervisor->name}}</option>
                            <option value="2">{{user()->supervisor->supervisor->name}}</option>
                            @if(!empty($review_previous->task))
                                <option value="3">{{$review_previous->task->name}}</option>
                            @endif
                        </select>

                        {{-- <div class="form-group">
                            <div class="selectgroup w-100">
                                <label class="selectgroup-item">
                                    <input type="radio" name="semakan" value="1" class="selectgroup-input" checked>
                                    <span class="selectgroup-button selectgroup-button-icon">{{user()->supervisor->name}}</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="semakan" value="2" class="selectgroup-input">
                                    <span class="selectgroup-button selectgroup-button-icon">{{user()->supervisor->supervisor->name}}</span>
                                </label>
                                @if(!empty($review_previous->task))
                                    <label class="selectgroup-item">
                                        <input type="radio" name="semakan" value="3" class="selectgroup-input">
                                        <span class="selectgroup-button selectgroup-button-icon">{{$review_previous->task->name}}</span>
                                    </label>
                                @endif
                            </div>
                        </div> --}}

                        @endif

                        <input id="semak1" name="semak1" type="hidden"/>
                        <input id="semak2" name="semak2" type="hidden"/>
                        <input id="semak3" name="semak3" type="hidden"/>


                        @if(!empty($review) && $review->created_by != user()->id)
                        @else
                            <div id="penyemakpp">
                                <button type="submit" class="btn btn-default float-right border-default s1 savSent">
                                    @icon('fe fe-send') {{ __('Teratur') }}
                                </button>
                            </div>

                            <div id="pengesahpp">
                                <button type="submit" class="btn btn-default float-right border-default s2 savSent">
                                    @icon('fe fe-send') {{ __('Teratur') }}
                                </button>
                            </div>

                            <div id="penyemakbpub">
                                <button type="submit" class="btn btn-default float-right border-default s3 savSent">
                                    @icon('fe fe-send') {{ __('Teratur') }}
                                </button>
                            </div>
                        @endif
                        
                        <input id="rejectStatus" name="rejectStatus" type="hidden"/>
                        <input id="saveStatus" name="saveStatus" type="hidden"/>
                        <input id="sentStatus" name="sentStatus" type="hidden"/>

                        <div class="btn-group float-right">
                            @if(!empty($review) && $review->created_by != user()->id)
                                <button type="submit" class="btn btn-danger btn-default border-primary rejSent">
                                    @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                </button>
                                <button type="submit" class="btn btn-primary btn-group float-right savDraf">
                                    @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                </button>
                                @if($review->status == 'S9')
                                    <input type="text" id="hashslug" name="hashslug" value="{{$ipc_latest->hashslug}}" hidden>
                                    <div class="btn-group float-right">
                                        <button type="button" id="sap_send" class="btn btn-success border-success">SAP</button>
                                    </div>
                                @elseif($review->status == 'S8')
                                    <a class="btn btn-success border-success" href="{{route('contract.post.sap_vendor', [$ipc_latest->ipcInvoice->pluck('company')->pluck('ssm_no'),"contract.post.ipc.show",$sst->hashslug])}}">Semak Syarikat</a>
                                    <button type="submit" class="btn btn-default float-middle border-default savSent">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                @else
                                    <button type="submit" class="btn btn-default float-middle border-default savSent">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                @endif
                            @endif
                        </div>

                    </form>
                </div>
            </div>
        @endslot
    @endcomponent
</div>

@endsection