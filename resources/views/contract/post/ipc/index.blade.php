@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.ipc.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'contract-post-ipc',
                            'route_name' => 'api.datatable.contract.ipc',
                            'columns' => [
                                ['data' => 'no_kontrak', 'title' => __('No. Kontrak'), 'defaultContent' => '-'],
                                ['data' => 'tajuk', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                ['data' => 'syarikat_terpilih', 'title' => __('Kontraktor'), 'defaultContent' => '-'],
                                ['data' => 'tarikh_mula', 'title' => __('Tarikh Mula Bekerja'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Rujukan'), __('Tajuk'),__('Kontraktor'), __('Tarikh Mula Bekerja'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.ipc.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection