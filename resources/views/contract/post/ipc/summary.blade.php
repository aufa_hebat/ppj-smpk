@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $("#table_summary").DataTable();
        });
    </script>
@endpush
@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
        <br>
        <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ (!empty($sst->start_working_date))? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ (!empty($sst->end_working_date))? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y'):null }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
    @endslot
@endcomponent
<div class="row">
    @component('components.card', ['card_classes' => 'col-12'])
        @slot('card_body')
            
            <div id="table" class="col-md-12 table-responsive table-bordered">
                <table id="table_summary" class="table1 table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <th>NO. IPC</th>
                        <th>Jumlah Wang Jaminan Pelaksanaan (WJP)</th>
                        <th>Wang Tahanan</th>
                        <th>Wang Tahanan Dilepaskan</th>
                        <th>Baki Wang Jaminan Pelaksanaan</th>
                        <th>Jumlah Wang Pendahuluan</th>
                        <th>Bayaran Pendahuluan</th>
                        <th>Bayaran Balik Pendahuluan</th>
                        <th>Baki Wang Pendahuluan</th>
                    </thead>
                    <tbody>
                        @if(!empty($ipc) && $ipc->count() > 0)
                        @foreach($ipc as $ip)
                        <tr>
                            <td>{{ $ip->ipc_no }}</td>
                            <td>RM{{ (!empty($ip->sst->bon))? money()->toCommon($ip->sst->bon->money_amount ?? 0, 2):'0.00' }}</td>
                            <td>RM{{ money()->toCommon($ip->wjp_amount ?? 0, 2) }}</td>
                            <td>RM{{ money()->toCommon($ip->wjp_lepas_amount ?? 0, 2) }}</td>
                            <td></td>
                            <td>RM{{ (!empty($ip->sst->deposit))? money()->toCommon($ip->sst->deposit->qualified_amount ?? 0, 2):'0.00' }}</td>
                            <td>RM{{ money()->toCommon($ip->advance_amount ?? 0, 2) }}</td>
                            <td>RM{{ money()->toCommon($ip->give_advance_amount ?? 0, 2) }}</td>
                            <td></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    
                </table>
            </div>
            <div class="btn-group float-right">
                <a href="{{ route('contract.post.ipc.index') }}" 
                        class="btn btn-default border-primary">
                        {{ __('Kembali') }}
                </a>
            </div>
            
        @endslot
    @endcomponent
</div>
@endsection