@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/previous_next.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.select2').select2();
            $('#contractor_name').val('{{ $sst->company->company_name }}');
            $('#contractor_ssm').val('{{ $sst->company->ssm_no }}');
            $('#contractor_gst').val('{{ $sst->company->gst_registered_no }}');
            
            $('#contractor_add').val('{{($sst->company->addresses->count() > 0)? str_replace(["\r\n","\r","\n"],"\\n ",$sst->company->addresses[0]->primary):""}} {{($sst->company->addresses->count() > 0)? ", ".str_replace(["\r\n","\r","\n"],"\\n ",$sst->company->addresses[0]->secondary):""}} {{($sst->company->addresses->count() > 0)? ", ".$sst->company->addresses[0]->postcode:""}} {{($sst->company->addresses->count() > 0)? ", ".$sst->company->addresses[0]->state:""}}');
            $('#contract_title').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$sst->acquisition->title) !!}');
            $('#contract_value').val('RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}');
            $('#period_type_id').val('{{ $sst->period}} {{$sst->period_type->name }}');
            
            @if(!empty($sst) && !empty($sst->al_issue_date))
                $('#al_issue_date').val('{{ $sst->al_issue_date->format("d/m/Y") }}');
            @endif
            @if(!empty($sst) && !empty($sst->start_working_date))
                $('#start_working_date').val('{{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$sst->start_working_date)->format("d/m/Y") }}');
            @endif
            @if(!empty($sst) && !empty($sst->end_working_date))
                $('#end_working_date').val('{{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$sst->end_working_date)->format("d/m/Y") }}');
            @endif
            @if(!empty($sst) && !empty($sst->acquisition) && !empty($sst->acquisition->evaluation) && !empty($sst->acquisition->evaluation->meeting_date))
                $('#contract_verify').val('{{ (!empty($sst->acquisition->evaluation->meeting_date))? \Carbon\Carbon::createFromFormat("Y-m-d",$sst->acquisition->evaluation->meeting_date)->format("d/m/Y"):"" }}');
            @endif
            
            @if(!empty($sst) && !empty($sst->document) && !empty($sst->document->revenue_stamp_date))
                $('#vp_sign_date').val('{{ \Carbon\Carbon::createFromFormat("Y-m-d",$sst->document->revenue_stamp_date)->format("d/m/Y") }}');
            @endif
            
            //console knape button tambah sijil xkluar
            @if(!empty($sst->ipc) && $sst->ipc->count() > 0)
                    console.log("there is ipc created under this sst ");
            @endif
            @if($date_valid == null || ($sst->ipc->count() > 2))
                    console.log("ipc already created more than 2 or date al_contractor_date already more than 4month");
            @endif
            @if($sst->ipc->count() > 2 && (empty($sst->document2) || empty($sst->document2->revenue_stamp_date) || $sst->document2->revenue_stamp_date == null))
                    console.log("ipc already created more than 2 and there is no revenue_stamp_date");
            @endif
            //end console knape button tambah sijil xkluar
            
            //tab remain after page reload
            $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //page remain after page reload
            $('.new_ipc').on('click', function(){
                redirect("{{route('contract.post.ipc.ipc_invoice',['hashslug' => $sst->hashslug])}}");
            });
            $(document).on('click', '.destroy-ipc-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('api.contract.ipc.sijil.destroy', id))
                        .then(response => {
                            swal('{!! __('Sijil Bayaran Interim') !!}', response.data.message, 'success');
                            location.reload();
                            //window.location.href = window.location.href;
                            
                        })
                  }
                });
            });
        });
    </script>
@endpush
@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
        <br>
        <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ (!empty($sst->start_working_date))? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ (!empty($sst->end_working_date))? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y'):null }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
    @endslot
@endcomponent
<div class="row" ref="msgContainer" id="tab_button">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#ipc-invoice" role="tab" aria-controls="ipc-invoice" aria-selected="false">
                    @icon('fe fe-file-text')&nbsp;{{ __('Sijil Interim') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#ipc-details" role="tab" aria-controls="ipc-details" aria-selected="false">
                    @icon('fe fe-briefcase')&nbsp;{{ __('Maklumat Kontrak') }}
                </a>
            </li>
        </ul>
    </div>
    @component('components.card', ['card_classes' => 'col-10'])
        @slot('card_body')
            <div class="tab-content" id="ipc-tab-content">
                <div class="tab-pane fade show active" id="ipc-invoice" role="tabpanel" aria-labelledby="ipc-invoice-tab">
                    @include('contract.post.ipc.invoice',['sst' => $sst])
                    @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.index',
                            'form'          => 'perinci-form',
                        ])
                </div>
                <div class="tab-pane fade show" id="ipc-details" role="tabpanel" aria-labelledby="ipc-details-tab">
                    <form id="perinci-form">
                        @include('contract.post.ipc.partials.forms.details',['sst' => $sst])
                    </form>
                    @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.index',
                            'form'          => 'perinci-form',
                        ])
                </div>
            </div>
            {{-- @component('components.tab.button')
            @endcomponent --}}
        @endslot
    @endcomponent
</div>
@endsection