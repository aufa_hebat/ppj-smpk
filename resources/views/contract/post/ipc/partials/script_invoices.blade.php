@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
            $(document).on('click', '.edit-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    redirect(route('contract.post.ipc.ipc_invoice_edit', id));
            });
            $(document).on('click', '.destroy-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			swal({
			  title: '{!! __('Amaran') !!}',
			  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: '{!! __('Ya') !!}',
			  cancelButtonText: '{!! __('Batal') !!}'
			}).then((result) => {
			  if (result.value) {
                                axios.delete(route('api.contract.ipc.sijil.destroy', id))
                                .then(response => {
                                    $('#contract-post-ipc-invoice').DataTable().ajax.reload();
                                    location.reload();
                                })
			  }
			});
		});
	});
</script>
@endpush