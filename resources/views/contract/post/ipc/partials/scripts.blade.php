@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
            $(document).on('click', '.summary-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    redirect(route('contract.post.ipc.summary', id));
            });
            $(document).on('click', '.show-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    redirect(route('contract.post.ipc.show', id));
            });
            $(document).on('click', '.edit-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    redirect(route('contract.post.ipc.edit', id));
            });
	});
</script>
@endpush