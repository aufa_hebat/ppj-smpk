@extends('layouts.app')
@push('scripts')
    @include('components.forms.assets.select2')
    <script type="text/javascript">
        
        jQuery(document).ready(function($) {
        });
    </script>
    @section('content')
        @component('components.card', ['card_classes' => 'col-12'])
            @slot('card_body')
            <form  method="POST" action="{{ route('contract.post.update_vendor') }}" files="true" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <h4>SENARAI SYARIKAT DARI SAP</h4>
                <input type="hidden" id="back_route" name="back_route" value="{{ $back_route }}">
                <input type="hidden" id="back_id" name="back_id" value="{{ $back_id }}">
                <table id="table_ssm" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="10%" ><b>NO SSM</b></th>
                            <th width="10%"><b>NO VENDOR</b></th>
                            <th width="40%"><b>SYARIKAT</b></th>
                            <th width="30%"><b>STATUS</b></th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    @if(!empty($data))
                        @foreach($data as $comp)
                        <tbody>
                            <tr>
                                <td>{{ $comp->ssmNo }}</td>
                                <td>{{ $comp->sapVendorId }}</td>
                                <td>{{ $company->where('ssm_no',$comp->ssmNo)->pluck('company_name')->first() ?? "TIADA REKOD" }}</td>
                                <td>{{ (!empty($comp->sapVendorId) && $comp->sapVendorId != null )? "NO.SSM BERDAFTAR DI SAP":"NO.SSM TIDAK BERDAFTAR DI SAP" }}</td>
                                <td>
                                    <input type="checkbox" class="form-control" id="vendor_id" name="vendor_id[]" value="{{ $comp->ssmNo }},{{ $comp->sapVendorId }}" >
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    @endif
                </table>
                <br>
            </div>
            <br>
            <div class="btn-group float-right">
                {{ html()->a(route($back_route,$back_id), __('Kembali'))->class('btn btn-default border-primary') }}
                <button type="submit" class="btn btn-primary float-right submit-action-btn" id="sub-contract-submit">
                    Simpan
                </button>
            </div>
            </form>
            @endslot
        @endcomponent
    @endsection