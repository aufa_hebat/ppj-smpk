<h5>Amaun Wang Tahanan</h5>
<div class="row">
    <input type="text" id="money_amount" name="money_amount" hidden>
    <div class="col-6" id="wjp_tambah" style="display:none">
    @include('components.forms.input', [
        'input_label' => __('Wang Tahanan (RM)'),
        'id' => 'wjp_amount',
        'name' => 'wjp_amount',
        'readonly' => true,
    ])
    </div>
    <div class="col-12" id="wjp_tambah_formula" style="display:none">
        @if(!empty($show_wjp))
        <span style="font-size:12px;">
            Nota:**<br>{{ $show_wjp }}
        </span>
        @endif
    </div>
    <div class="col-6" id="wjp_tolak" style="display:none">
    @include('components.forms.input', [
        'input_label' => __('Wang Tahanan Dilepaskan (RM)'),
        'id' => 'wjp_lepas_amount',
        'name' => 'wjp_lepas_amount',
        'readonly' => true,
    ])
    </div>
    <div class="col-12" id="wjp_tolak_formula" style="display:none">
        @if(!empty($show_wjp_tolak))
        <span style="font-size:12px;">
            Nota:**<br>{{ $show_wjp_tolak }}
        </span>
        @endif
    </div>
</div>
