<h5>Amaun Wang Pendahuluan</h5>
<div class="row">
    <div class="col-6" id="adv_tambah" style="display:none">
    @include('components.forms.input', [
        'id' => 'advance_amount', 
        'name' => 'advance_amount', 
        'input_label' => 'Bayaran Pendahuluan (RM)',
        'readonly' => true,
    ])
    </div>
    <div class="col-6" id="adv_tolak" style="display:none">
    @include('components.forms.input', [
        'input_label' => __('Bayaran Balik Pendahuluan (RM)'),
        'id' => 'give_advance_amount',
        'name' => 'give_advance_amount',
        'readonly' => true,
    ])
    
    </div>
    
    <div class="col-12" id="adv_tolak_formula" style="display:none">
        @if(!empty($show_adv))
        <span style="font-size:12px;">
            Nota:**<br>
            ((2 * pendahuluan) / (j_kontrak - kos_prima_peruntukan) * (j_inbois - (0.25 * (j_kontrak - kos_prima_peruntukan) ) ) )
            <br>{{ $show_adv }}
        </span>
        @endif
    </div>
</div>
