<h5>Muat Naik</h5>
<div class="form-group row">
    
    <label class="col col-form-label"> Pendaftaran Pertubuhan Keselamatan Sosial (PERKESO) </label>

    <div class="col input-group">
        <input id="subFilePerkeso" type="text"  class="form-control" readonly>
        <label class="input-group-text" for="uploadFilePerkeso"><i class="fe fe-upload" ></i></label>
        <input type="file" class="form-control" id="uploadFilePerkeso" name="documentPerkeso[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
        @if(!empty($ipc->doc_perkeso))
            <label class="input-group-text" for="downloadFile">
                <a href="/download/{{ $ipc->doc_perkeso->document_path }}/{{ $ipc->doc_perkeso->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
            </label>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col col-form-label"> Sijil Bayaran Interim </label>

    <div class="col input-group">
        <input id="subFileSijil" type="text"  class="form-control" readonly>
        <label class="input-group-text" for="uploadFileSijil"><i class="fe fe-upload" ></i></label>
        <input type="file" class="form-control" id="uploadFileSijil" name="documentSijil[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
        @if(!empty($ipc->doc_sijil))
            <label class="input-group-text" for="downloadFile">
                <a href="/download/{{ $ipc->doc_sijil->document_path }}/{{ $ipc->doc_sijil->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
            </label>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col col-form-label">Ringkasan Bayaran</label>

    <div class="col input-group">
        <input id="subFileSummary" type="text"  class="form-control" readonly>
        <label class="input-group-text" for="uploadFileSummary"><i class="fe fe-upload" ></i></label>
        <input type="file" class="form-control" id="uploadFileSummary" name="documentSummary[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
        @if(!empty($ipc->doc_summary))
            <label class="input-group-text" for="downloadFile">
                <a href="/download/{{ $ipc->doc_summary->document_path }}/{{ $ipc->doc_summary->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
            </label>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col col-form-label">Status Bayaran Interim</label>

    <div class="col input-group">
        <input id="subFileStatus" type="text"  class="form-control" readonly>
        <label class="input-group-text" for="uploadFileStatus"><i class="fe fe-upload" ></i></label>
        <input type="file" class="form-control" id="uploadFileStatus" name="documentStatus[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
        @if(!empty($ipc->doc_status))
            <label class="input-group-text" for="downloadFile">
                <a href="/download/{{ $ipc->doc_status->document_path }}/{{ $ipc->doc_status->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
            </label>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col col-form-label">Resit Levi</label>

    <div class="col input-group">
        <input id="subFileLevi" type="text"  class="form-control" readonly>
        <label class="input-group-text" for="uploadFileLevi"><i class="fe fe-upload" ></i></label>
        <input type="file" class="form-control" id="uploadFileLevi" name="documentLevi[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
        @if(!empty($ipc->doc_levi))
            <label class="input-group-text" for="downloadFile">
                <a href="/download/{{ $ipc->doc_levi->document_path }}/{{ $ipc->doc_levi->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
            </label>
        @endif
    </div>
</div>