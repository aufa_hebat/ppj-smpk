<div class="btn-group float-right">
    @isset($route_back)
        @isset($hashslug)
	{{ html()->a(route($route_back,['hashslug'=>$hashslug]), __('Kembali'))->class('btn btn-default border-primary') }}
        @else
        {{ html()->a(route($route_back), __('Kembali'))->class('btn btn-default border-primary') }}
        @endisset
    @endisset
    @isset($route_name)
	<button type="submit" class="btn btn-primary float-middle submit-action-btn" 
		data-route="{{ $route_name }}"
		data-form="{{ $form }}">
	    @icon('fe fe-save') {{ __('Simpan') }}
	</button>
    @endisset
    @isset($btn_send)
        @if($ipc->last_ipc == 'n')
            <button type="submit" class="btn btn-default float-middle border-default hantar-action-btn" 
        		data-route="{{ $btn_send }}"
        		data-form="{{ $form }}">
        	    @icon('fe fe-send') {{ __('Teratur') }}
        	</button>
        @else
            @if(!empty($ipc->doc_sofa) && !empty($ipc->doc_statuori))
                <button type="submit" class="btn btn-default float-middle border-default hantar-action-btn" 
                    data-route="{{ $btn_send }}"
                    data-form="{{ $form }}">
                    @icon('fe fe-send') {{ __('Teratur') }}
                </button>
            @endif
        @endif
    @endisset
</div>