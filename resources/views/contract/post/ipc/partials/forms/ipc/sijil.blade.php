<h5>Sijil Interim</h5>
    <div class="row">
        <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('No Sijil Interim'),
            'id' => 'ipc_no',
            'name' => 'ipc_no',
            'readonly' => true,
        ])
        </div>
        <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Ringkasan Pembayaran (SAP): '),
            'id' => 'short_text',
            'name' => 'short_text',
            'maxlength' => 25,
        ])
        </div>
    </div>
    <div class="row">
        <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Penilaian Tapak'),
            'id' => 'evaluation_at',
            'name' => 'evaluation_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        </div>
        <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Keterangan (SAP): '),
            'id' => 'long_text',
            'name' => 'long_text',
            'maxlength' => 50,
        ])
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('Jumlah Yang Perlu Dibayar (RM)'),
                'id' => 'demand_amount',
                'name' => 'demand_amount',
            ])  
        </div>
        
        <div class="col-6 showlast" style="display:none">
            Interim Terakhir
            <input type="checkbox" id="last_ipc" name="last_ipc" class="form-group"
            @if(!empty($ipc) && $ipc->last_ipc == 'y')
                value="y"
                checked="checked"
            @else
                value="n"
            @endif
            >
        </div>
    </div>
    <div class="row">
        <div class="col-12" id="demand_show" style="display:none">
            @if(!empty($demand_show))
            <span style="font-size:12px;">
                Nota:**<br>{!! $demand_show !!}
            </span>
            @endif
        </div>
    </div>