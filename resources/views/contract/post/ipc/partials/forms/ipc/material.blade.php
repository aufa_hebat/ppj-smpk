<h5>Material on Site</h5>
<div class="col-md-12">
    <div class="form-group">
        <div class="table-responsive">
            <div  class="table-editable ">
                <table id="table_material" class="table table-sm table-transparent">
                    @if($ipc->status == 1)
                    <div class="float-right">
                        <span class="addmaterial btn btn-primary" title="Tambah Material on Site">@icon('fe fe-plus')</span>
                    </div>
                    @endif
                    <tr>
                        <th width="45%">Keterangan</th>
                        <th width="10%">Unit</th>
                        <th width="10%">Kuantiti</th>
                        <th width="15%">Kadar</th>
                        <th width="15%">Amaun (RM)</th>
                        <th width="5%">Hapus</th>
                    </tr>
                    @if(!empty($ipc) && $ipc->ipcMaterial->count() > 0)
                        @foreach($ipc->ipcMaterial as $k=>$mate)
                            <tr >
                                <td>{{ $mate->description }}</td>
                                <td>{{ $mate->unit }}</td>
                                <td>{{ money()->toCommon($mate->quantity ?? 0,2)  }}</td>
                                <td>{{ money()->toCommon($mate->rate ?? 0,2) }}</td>
                                <td>{{ money()->toCommon($mate->amount ?? 0,2) }}</td>
                                <td><input type="text" id="exist_{{ $mate->id }}" name="exist[{{ $mate->id }}][material]" value="{{ $mate->id }}" hidden>
                                    <button class="deletematerial btn btn-danger" title="Hapus Material on Site">@icon('fe fe-trash')</button>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr id="material0" class="d-none">
                        <td><input type="text" id="desc_0" name="material[0][desc]" class="form-control" style="width:100%;"></td>
                        <td><input type="text" id="unit_0" name="material[0][unit]" class="form-control" style="width:100%;"></td>
                        <td><input type="text" id="quantity_0" name="material[0][quantity]" class="form-control " onkeyup="calc($this)" style="width:100%;text-align:right"></td>
                        <td><input type="text" id="rate_0" name="material[0][rate]" class="form-control " onkeyup="calc($this)" style="width:100%;text-align:right"></td>
                        <td><div class="material_amt"><input type="text" id="amount_0" name="material[0][amount]" class="form-control matamt" style="width:100%;text-align:right" readonly></div></td>
                        <td>
                            <button class="deletematerial btn btn-danger" title="Hapus Material on Site">@icon('fe fe-trash')</button>
                        </td>
                    </tr>
               </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6" id="material_tambah" >
    @include('components.forms.input', [
        'input_label' => __('Amaun Bahan-bahan dan Barang-barang Tak Pasang (RM)'),
        'id' => 'material_on_site_amount',
        'name' => 'material_on_site_amount',
        'readonly' => true,
    ])
    </div>
    <div class="col-6" id="material_tolak" style="display:none">
    @include('components.forms.input', [
        'input_label' => __('Penolakan Jumlah Material on Site (RM)'),
        'id' => 'material_tahanan_amount',
        'name' => 'material_tahanan_amount',
        'readonly' => true,
    ])
    </div>
    <div class="col-6">
        <label for="Invois" class="col col-form-label"> Muat Naik Lampiran </label>

        <div class="col input-group">
            <input id="subFileMat" type="text"  class="form-control" readonly>
            <label class="input-group-text" for="uploadFileMat"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFileMat" name="documentMat[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
        @if(!empty($ipc->doc_material))
            <label class="input-group-text" for="downloadFile">
                <a href="/download/{{ $ipc->doc_material->document_path }}/{{ $ipc->doc_material->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
            </label>
            <label class="input-group-text" for="deleteFile{{ $ipc->doc_material->id }}">
                <input type="input" id="deleteFile{{ $ipc->doc_material->id }}" class="buang_attach" value="{{ $ipc->doc_material->hashslug }}" hidden>
                <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
            </label>
        @endif
        </div>
    </div>
</div>