@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @foreach($ipc->ipcInvoice as $bq_inv)
                @if(!empty($bq_inv->ipcBq))
                    $heys = $("#showBq{{ $bq_inv->id }}").DataTable({
                        order: false,
                    });
                @endif
            @endforeach
        });
    </script>
@endpush
<h5>Senarai Inbois</h5>
<div class="row justify-content-center">
    <div class="col-12" >
        <!-- button for add new invoice -->
    
        @if($ipc->status == 1 && 
            (empty($ipc->ipcInvoice) || (!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() < 1 ) || (!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0 && !empty($ipc->ipcInvoice->count() > 0) && !empty($ipc->sst) && !empty($ipc->sst->acquisition) && !empty($ipc->sst->acquisition->approval) && !empty($ipc->sst->acquisition->approval->type) && ($ipc->sst->acquisition->approval->type->id != 2) && $ipc->last_ipc != 'y'))
        )
        <a class="btn btn-primary float-right" data-toggle="modal" onclick="set_name(0);set_name_fund(0,null);set_name_comp(0,null,1);set_n(0);" data-target="#modalTambah" style="color: white;" data-backdrop="static">Tambah Inbois</a>
        @endif
        <!-- button for add new invoice -->
        @component('components.card')
            @slot('card_body')
            <!-- list invoice -->
            <div class="table-responsive">
                <table id="table_inv" class="table table-sm table-transparent table-hover">
                    <thead>
                        <th>NO INBOIS</th>
                        <th>JUMLAH INBOIS (RM) </th>
                        <th>JUMLAH SST (RM) </th>
                        <th>JUMLAH BAYARAN (RM) </th>
                        <th>KONTRAKTOR</th>
                        <th>MAKLUMAT KEWANGAN</th>
                        @if($ipc->status == 1)
                        <th>TINDAKAN</th>
                        @endif
                    </thead>
                    @if(!empty($ipc_invoice))
                        @foreach($ipc_invoice as $invoice)
                        <tbody>
                            <tr>
                                <td>{{ $invoice->invoice_no }}</td>
                                <td>{{ money()->toCommon($invoice->demand_amount ?? "0", 2) }}</td>
                                <td>{{ money()->toCommon($invoice->sst_amount ?? "0", 2) }}</td>
                                <td>{{ money()->toCommon($invoice->invoice ?? "0", 2) }}</td>
                                <td>{{ (!empty($invoice->company))? $invoice->company->company_name:'' }}</td>
                                <td>
                                    @if(!empty($invoice->acquisition_approval_financial_id) && !empty($invoice->financial))
                                        @if(!empty($invoice->financial->businessArea))
                                            <b>BUSS AREA:</b> {{ $invoice->financial->businessArea->name ?? '' }}
                                        @endif
                                        @if(!empty($invoice->financial->costCenter))
                                            <br><b>COST CENTRE:</b> {{ $invoice->financial->costCenter->name ?? '' }}
                                        @endif
                                        @if(!empty($invoice->financial->functionalArea))
                                            <br><b>FUNCTIONAL AREA:</b> {{ $invoice->financial->functionalArea->name ?? '' }}
                                        @endif
                                        @if(!empty($invoice->financial->fund))
                                            <br><b>FUND:</b> {{ $invoice->financial->fund->name ?? '' }}
                                        @endif
                                        @if(!empty($invoice->financial->fundedProgramme))
                                            <br><b>FUNDED PROG:</b> {{ $invoice->financial->fundedProgramme->name ?? '' }}
                                        @endif
                                        @if(!empty($invoice->financial->glAccount))
                                            <br><b>GL ACCOUNT:</b> {{ $invoice->financial->glAccount->name ?? '' }}
                                        @endif
                                    @endif
                                    
                                </td>
                                @if($ipc->status == 1)
                                <td><div class="text-center">
                                        <div class="item-action dropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                                              <i class="fe fe-more-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                                              style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                @if($ipc->status == 1)
                                                    <a class="dropdown-item edit-action-btn" data-toggle="modal" onclick="set_name({{ $invoice->id }});set_name_fund('{{ $invoice->id }}','{{ $invoice->acquisition_approval_financial_id }}');set_name_comp('{{ $invoice->id }}','{{ $invoice->company_id ?? 0 }}','{{ $invoice->status_contractor ?? 1 }}');set_n({{ $invoice->id }});" data-target="#modalEdit{{ $invoice->id }}" data-backdrop="static">
                                                      <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                                    </a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item destroy-inv-action-btn"
                                                        data-hashslug="{{ $invoice->hashslug }}">
                                                        <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
                                                    </a>
                                                @endif
                                          </div>
                                        </div>
                                    </div>
                                </td>
                                @endif
                            </tr>
                        </tbody>
                        @endforeach
                    @endif
                </table>
            </div>
            <!-- list invoice -->
            <!-- list bq for ipc show -->
            @if($ipc->status != 1 && !empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
            <br><br>
            <h5>Senarai Kuantiti Yang Dibayar</h5>
                @foreach($ipc->ipcInvoice as $bq_inv)
                <u>{{ $bq_inv->invoice_no }}</u>
                <div  class="table-responsive">
                    <table id="showBq{{ $bq_inv->id }}" class="table" width="100%" cellspacing="0">
                        <thead>
                            <th>NO</th>
                            <th>SUB ITEM</th>
                            <th>KETERANGAN</th>
                            <th>HARGA SEUNIT</th>
                            <th>KUANTITI</th>
                            <th>JUMLAH</th>
                        </thead>
                        <tbody>
                        @if(!empty($bq_inv->ipcBq))
                            @foreach($bq_inv->ipcBq as $bq_bq)
                                @if($bq_bq->status == 'N')
                                    <tr>
                                        <td>{{ (!empty($bq_bq->ipcBqElement))? $bq_bq->ipcBqElement->no:null }}.{{ (!empty($bq_bq->ipcBqItem))? $bq_bq->ipcBqItem->no:null }}.{{ (!empty($bq_bq->ipcBqSub))? $bq_bq->ipcBqSub->no:null }}</td>
                                        <td>{{ (!empty($bq_bq->ipcBqSub))? $bq_bq->ipcBqSub->item:null }}</td>
                                        <td>{{ (!empty($bq_bq->ipcBqSub))? $bq_bq->ipcBqSub->description:null }}</td>
                                        <td>{{ (!empty($bq_bq->ipcBqSub))? money()->toCommon($bq_bq->ipcBqSub->rate_per_unit ?? 0,2):null }}</td>
                                        <td>{{ money()->toCommon($bq_bq->quantity ?? 0,2) }}</td>
                                        <td>{{ money()->toCommon($bq_bq->amount ?? 0,2) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td></td>
                                        <td>{{ (!empty($bq_bq->ipcBqWpsSub))? $bq_bq->ipcBqWpsSub->item:null }}</td>
                                        <td>{{ (!empty($bq_bq->ipcBqWpsSub))? $bq_bq->ipcBqWpsSub->description:null }}</td>
                                        <td>{{ (!empty($bq_bq->ipcBqWpsSub))? money()->toCommon($bq_bq->ipcBqWpsSub->rate_per_unit ?? 0,2):null }}</td>
                                        <td>{{ money()->toCommon($bq_bq->quantity ?? 0,2) }}</td>
                                        <td>{{ money()->toCommon($bq_bq->amount ?? 0,2) }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div><br>
                @endforeach
            @endif
            <!-- list bq for ipc show -->
            @endslot                
        @endcomponent
    </div>
</div>
<!-- check status draf bace modal add and edit -->
@if($ipc->status == 1)
<!-- modal for add-->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel">
  <div class="modal-dialog modal-lg" role="document" style="width:100%;height:100%">
    <div class="modal-content" >
        <div class="modal-header">
            <h4 class="modal-title">Inbois</h4>
            <button type="button" class="close" onclick="unset_name(0);unset_name_fund(0,null);unset_name_comp(0,null,1);" data-dismiss="modal"></button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <div class="selectgroup w-100" >
                    <label class="selectgroup-item" style="background-color: lightgray">
                        <input type="radio" name="inbois_type" id="inbois_type0" value="1" class="selectgroup-input" checked>
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Inbois') }}</span>
                    </label>
                    <label class="selectgroup-item" style="background-color: lightgray">
                        <input type="radio" name="inbois_type" id="inbois_type0" value="2" class="selectgroup-input">
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Senarai Kuantiti') }}</span>
                    </label>
                    <label class="selectgroup-item" style="background-color: lightgray">
                        <input type="radio" name="inbois_type" id="inbois_type0" value="3" class="selectgroup-input">
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Kontraktor ') }}</span>
                    </label>
                    <label class="selectgroup-item" style="background-color: lightgray">
                        <input type="radio" name="inbois_type" id="inbois_type0" value="4" class="selectgroup-input">
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Kewangan') }}</span>
                    </label>
                    
                </div>
                <input type="text" id="invoice_edit0" value="n" hidden>
                <input type="text" id="invoice_id0"  value="null" hidden>
                @include('contract.post.ipc.partials.forms.invoice.invois_info',['n'=>'0'])
                @include('contract.post.ipc.partials.forms.invoice.quantity',['n'=>'0'])
                @include('contract.post.ipc.partials.forms.invoice.subkontrak',['n'=>'0'])
                @include('contract.post.ipc.partials.forms.invoice.finance',['n'=>'0'])
                
            </div>
        </div>
    </div>
  </div>
</div>
<!-- modal for add-->
<!-- modal for edit-->
@if(!empty($ipc_invoice))
    @foreach($ipc_invoice as $invs)
    <div class="modal fade" id="modalEdit{{ $invs->id }}" tabindex="-1"  role="dialog" aria-labelledby="modalEditLabel{{ $invs->id }}">
        <div class="modal-dialog modal-lg" role="document{{ $invs->id }}" style="width:100%;height:100%">
            <div class="modal-content" >
                <div class="modal-header">
                    <h4 class="modal-title">Inbois</h4>
                    <button type="button" class="close" onclick="unset_name({{ $invs->id }});unset_name_fund('{{ $invs->id }}','{{ $invs->acquisition_approval_financial_id }}');unset_name_comp('{{ $invs->id }}','{{ $invs->company_id ?? 0 }}','{{ $invs->status_contractor ?? 0 }}')" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="selectgroup w-100" >
                            <label class="selectgroup-item" style="background-color: lightgray">
                                <input type="radio" name="inbois_type{{ $invs->id }}" id="inbois_type{{ $invs->id }}" value="1" class="selectgroup-input" checked>
                                <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Inbois') }}</span>
                            </label>
                            <label class="selectgroup-item" style="background-color: lightgray">
                                <input type="radio" name="inbois_type{{ $invs->id }}" id="inbois_type{{ $invs->id }}" value="2" class="selectgroup-input">
                                <span class="selectgroup-button selectgroup-button-icon">{{ __('Senarai Kuantiti') }}</span>
                            </label>
                            <label class="selectgroup-item" style="background-color: lightgray">
                                <input type="radio" name="inbois_type{{ $invs->id }}" id="inbois_type{{ $invs->id }}" value="3" class="selectgroup-input">
                                <span class="selectgroup-button selectgroup-button-icon">{{ __('Kontraktor ') }}</span>
                            </label>
                            <label class="selectgroup-item" style="background-color: lightgray">
                                <input type="radio" name="inbois_type{{ $invs->id }}" id="inbois_type{{ $invs->id }}" value="4" class="selectgroup-input">
                                <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Kewangan') }}</span>
                            </label>

                        </div>
                        <input type="text" id="invoice_edit{{ $invs->id }}" value="y" hidden>
                        <input type="text" id="invoice_id{{ $invs->id }}" value="{{ $invs->hashslug }}" hidden>
                        @include('contract.post.ipc.partials.forms.invoice.invois_info',['n'=>$invs->id])
                        @include('contract.post.ipc.partials.forms.invoice.quantity',['n'=>$invs->id])
                        @include('contract.post.ipc.partials.forms.invoice.subkontrak',['n'=>$invs->id])
                        @include('contract.post.ipc.partials.forms.invoice.finance',['n'=>$invs->id])
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            
            $('#invois_info{{ $invs->id }}').show();
            $('#quantity{{ $invs->id }}').hide();
            $('#subkontrak{{ $invs->id }}').hide();
            $('#finance{{ $invs->id }}').hide();
            
            $("#invoice_no{{ $invs->id }}").val(' {{ $invs->invoice_no }} ');
            $("#invoice{{ $invs->id }}").val(' {{ money()->toCommon($invs->invoice ?? "0", 2) }} ');
            $("#sst_amount{{ $invs->id }}").val(' {{ money()->toCommon($invs->sst_amount ?? "0", 2) }} ');
            $("#ref_text{{ $invs->id }}").val(' {{ $invs->ref_text }} ');
            $("#bulan_status{{ $invs->id }}").val(' {{ $invs->bulan_status }} ');
            $("#demand_amount{{ $invs->id }}").val(' {{ money()->toCommon($invs->demand_amount ?? "0", 2) }} ');
            
            @if(!empty($invs->invoice_at))
                $("#invoice_at{{ $invs->id }}").val(' {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$invs->invoice_at)->format("d/m/Y") }} ');
            @endif
            
            @if(!empty($invs->invoice_receive_at))
                $("#invoice_receive_at{{ $invs->id }}").val(' {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$invs->invoice_receive_at)->format("d/m/Y") }} ');
            @endif
            
            @if( $invs->document )
                    $('#subFile{{ $invs->id }}').val('{{ $invs->document->document_name }}')
            @endif
            
            $('#uploadFile{{ $invs->id }}').change(function(){
                $('#subFile{{ $invs->id }}').val($(this).val().split('\\').pop());
            });
            
            // auto count total invoice on credit change
            $('input[type=radio][id=inbois_type{{ $invs->id }}]').change(function () {
                if(this.value == '1'){
                    $('#invois_info{{ $invs->id }}').show();
                    $('#quantity{{ $invs->id }}').hide();
                    $('#subkontrak{{ $invs->id }}').hide();
                    $('#finance{{ $invs->id }}').hide();
                }
                else if (this.value == '2'){
                    $('#invois_info{{ $invs->id }}').hide();
                    $('#quantity{{ $invs->id }}').show();
                    $('#subkontrak{{ $invs->id }}').hide();
                    $('#finance{{ $invs->id }}').hide();
                }
                else if (this.value == '3'){
                    $('#invois_info{{ $invs->id }}').hide();
                    $('#quantity{{ $invs->id }}').hide();
                    $('#subkontrak{{ $invs->id }}').show();
                    $('#finance{{ $invs->id }}').hide();
                }
                else{
                    $('#invois_info{{ $invs->id }}').hide();
                    $('#quantity{{ $invs->id }}').hide();
                    $('#subkontrak{{ $invs->id }}').hide();
                    $('#finance{{ $invs->id }}').show();
                }
            });
            $(".addamt{{ $invs->id }}").attr('readonly',true);
            $('#invoice_no{{ $invs->id }}').on('change', function(){
                $('#ref_text{{ $invs->id }}').val(this.value);
            });
        });
    </script>
    @endpush
    @endforeach
@endif
<!-- modal for edit-->
@endif
<!-- check status draf bace modal add and edit -->