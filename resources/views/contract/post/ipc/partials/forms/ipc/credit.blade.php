<h5>Nota Kredit</h5>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('No Nota Kredit'),
            'id' => 'credit_no',
            'name' => 'credit_no',
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Amaun Nota Kredit (RM)'),
            'id' => 'credit',
            'name' => 'credit',
            'input_classes' => 'money',
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Nota Kredit'),
            'id' => 'credit_at',
            'name' => 'credit_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Terima Nota Kredit'),
            'id' => 'credit_receive_at',
            'name' => 'credit_receive_at',
            'config' => [
            'format' => config('datetime.display.date'),
            ]
        ])
    </div>
</div>