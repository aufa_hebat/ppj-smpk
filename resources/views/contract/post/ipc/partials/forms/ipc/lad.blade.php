@php
    $tjk = "Ganti Rugi/ Denda/ Penalti";
    if($ipc->compensation_amount > 0 || $lad_value > 0){
        $tjk = "Ganti Rugi";
    }elseif(!empty($denda) && ($denda == 1 || $denda == 3 || $denda == 4)){
        $tjk = "Denda";
    }
@endphp
<h5>Amaun {{ $tjk }}</h5>
<div class="row">
    <input type="text" id="lad_day" name="lad_day" hidden>
    <input type="text" id="lad_rate" name="lad_rate" hidden>
    <div class="col-6">
    @if(!empty($denda) && ($denda == 2))
        @include('components.forms.input', [
            'input_label' => __('Ganti Rugi tertentu dan ditetapkan (LAD) (RM)'),
            'id' => 'compensation_amount',
            'name' => 'compensation_amount',
            'readonly' => true,
        ])
    @elseif(!empty($denda) && ($denda == 1 || $denda == 3 || $denda == 4))
        @include('components.forms.input', [
            'input_label' => __('Denda (RM)'),
            'id' => 'compensation_amount',
            'name' => 'compensation_amount',
            'input_classes' => 'money'
        ])
    @endif
    
    </div>
    <div class="col-12" id="lad_formula" style="display:none">
        @if(!empty($lad_formula))
        <span style="font-size:12px;">
            Nota:**<br>{{ $lad_formula }}
        </span>
        @endif
    </div>
</div>