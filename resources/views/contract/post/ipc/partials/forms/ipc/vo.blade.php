@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        //auto count vo
        // var batal = 0;
        // var float_batal = 0.00;
        // var jumlah_batal = 0.00;
        // $('.cancel_amt').each(function(){
        //     if($(this).val() != '')
        //     {
        //         batal = $(this).val();
        //         batal = batal.replace(/[,]/g, "");
        //         batal = batal.split(' ').join('');
                
        //         float_batal = parseFloat(batal);
        //         jumlah_batal = jumlah_batal + float_batal;
        //     }
        // });
        // if(jumlah_batal < 0){
        //     jumlah_batal = -(jumlah_batal);
        // }else{
        //     jumlah_batal = jumlah_batal;
        // }
        
        
        $('.voqty').change(function(){
            var vo_ttl = 0;
            var vo_totl = 0.00;
            var vo_total = 0.00;
            $('.voamt').each(function(){

                if($(this).val() != '')
                {
                    vo_ttl = $(this).val();
                    vo_ttl = vo_ttl.replace(/[,]/g, "");
                    vo_ttl = vo_ttl.split(' ').join('');
                    
                    vo_totl = parseFloat(vo_ttl);
                    vo_total = vo_total + vo_totl;
                }
            });
            $('#working_change_amount').val(parseFloat(vo_total).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));

            var _curr_con = 0;
            $curr_con   = '{{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}';
            $curr_con   = $curr_con.replace(/[,]/g, "");
            $curr_con   = $curr_con.split(' ').join('');
            $change_con = $('#working_change_amount').val();
            $change_con = $change_con.replace(/[,]/g, "");
            $change_con = $change_con.split(' ').join('');
            var $vo_batal   = '{{ money()->toCommon($vo_batal ?? "0" , 2) }}';
            $vo_batal   = $vo_batal.replace(/[,]/g, "");
            $vo_batal   = $vo_batal.split(' ').join('');
            var $pre_vo   = '{{ money()->toCommon($pre_vo ?? "0" , 2) }}';
            $pre_vo   = $pre_vo.replace(/[,]/g, "");
            $pre_vo   = $pre_vo.split(' ').join('');
            // var new_contract = parseFloat($curr_con) + parseFloat($change_con) - - jumlah_batal;
            var new_contract = parseFloat($curr_con) + parseFloat($change_con) - $vo_batal + $pre_vo; 
            
            if(new_contract != $curr_con){
                $('#new_contract_amount').val(parseFloat(new_contract).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
            }else{
                $('#new_contract_amount').val('{{ money()->toCommon(0, 2) }}');
            }

            calc_demand_amount();   
        });
        //end auto count vo 
    });
</script>
@endpush
<h5>Senarai Perubahan Kerja</h5>
<br>
@if(!empty($ipc->sst->ppjhk_active) && $ipc->sst->ppjhk_active->count() > 0)
@foreach($ipc->sst->ppjhk_active as $ppjhk)
@if($ppjhk->status == '1')
<h6><u>P.P.J.H.K {{ $ppjhk->no }}</u></h6>
<div id="table" class="table-editable table-bordered border-0">
    
    @if(!empty($ppjhk->elements) && $ppjhk->elements->count() > 0)
    @foreach($ppjhk->elements as $vo_el)
        <div class="mb-0 card" >
            <div class="card-header" role="tab" id="heading{{ $vo_el->id }}">
                <h4 class="card-title cardElemen">
                    <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{ $vo_el->id }}" href="#collapse{{ $vo_el->id }}" aria-expanded="true" aria-controls="collapse{{ $vo_el->id }}">
                        <i class="mr-2 fa fa-folder-open"></i>{{ $vo_el->voElement->element}}
                    </a>
                </h4>
            </div>
            <div id="collapse{{ $vo_el->id }}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $vo_el->id }}" >
                <div class="card-body">
                    <div class="row">
                        <div class="table-responsive">
                            <p>@if(!empty($vo_el->voElement->description)) <u>{{ $vo_el->voElement->description }}</u> @endif</p>
                            <table class="table1  table-no-border" width="100%" cellspacing="0">
                                @if(!empty($voItem) && $voItem->count() > 0)
                                    @foreach($voItem as $vo_it)
                                    @if($vo_el->vo_element_id == $vo_it->vo_element_id && $vo_el->sst_id == $vo_it->sst_id)
                                    <thead>
                                        <th style="width:100%;border:none" >
                                            <h5>&nbsp;{{ $vo_it->voItem->item}} &nbsp;&nbsp;&nbsp;</h5><br>
                                            <p>@if(!empty($vo_it->voItem->description)) {{ $vo_it->voItem->description }} @endif</p>
                                        </th>
                                    </thead>
                                    <tbody>
                                        <tr style="width:100%;border:none">
                                            <td>
                                                <table id="vo{{ $vo_it->id }}" class="table table-bordered" width="100%" cellspacing="0">
                                                    <thead>
                                                        <!--<th style="width:5%">BIL</th>-->
                                                        <th style="width:10%">SUB ITEM</th>
                                                        <th style="width:10%">KETERANGAN</th>
                                                        <th style="width:10%">UNIT</th>
                                                        <th style="width:10%">KUANTITI</th>
                                                        <th style="width:10%">HARGA SEUNIT (RM)</th>
                                                        <th style="width:10%">JUMLAH (RM)</th>
                                                        <th style="width:10%">KUANTITI TERKUMPUL</th>
                                                        <th style="width:10%">KUANTITI TUNTUTAN</th>
                                                        <th style="width:10%">JUMLAH TUNTUTAN (RM)</th>
                                                        <th style="width:5%"></th>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($voSub))
                                                    @foreach($voSub as $vo_sub)
                                                    @if($vo_it->vo_item_id == $vo_sub->vo_item_id  && $vo_sub->sst_id == $vo_it->sst_id)
                                                    <tr>
                                                        {{-- <td>{{ $vo_sub->voSubItem->no }}</td> --}}
                                                        <td><input type="text" id="vo_subid{{ $vo_sub->id }}" name="vo[{{ $vo_sub->id }}][subid]" value="{{ $vo_sub->id }}" hidden>{{ $vo_sub->voSubItem->item }}</td>
                                                        <td>{{ $vo_sub->voSubItem->description }}</td>
                                                        <td>{{ $vo_sub->voSubItem->unit }}</td>
                                                        <td align="right">
                                                            <input type="text"  id="vo_limit{{ $vo_sub->id }}"  class=" voqty" onkeyup="calc_vo({{ $vo_sub->id }})"
                                                            @if(!empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) )
                                                                value="{{ money()->toCommon($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->sum('quantity') ?? 0,2) }}" 
                                                            @else
                                                                value="0.00"
                                                            @endif
                                                            hidden>
                                                            <input type="text" id="vo_qty{{ $vo_sub->id }}" value="{{ money()->toCommon($vo_sub->total_quantity ?? 0,2) }}" hidden>
                                                            {{ money()->toCommon($vo_sub->total_quantity ?? 0,2 )}}
                                                        </td>
                                                        <td align="right"><input type="text" id="vo_rate{{ $vo_sub->id }}" value="{{ money()->toCommon($vo_sub->rate_per_unit ?? 0,2 ) }}" hidden>{{ money()->toCommon($vo_sub->rate_per_unit ?? 0, 2)}}</td>
                                                        <td align="right"><input type="text" id="vo_amt{{ $vo_sub->id }}" value="{{ money()->toCommon($vo_sub->net_amount ?? 0,2 ) }}" hidden>{{ money()->toCommon($vo_sub->net_amount ?? 0, 2)}}</td>
                                                        <td align="right">
                                                            <input type="text" id="vo_paid{{ $vo_sub->id }}" value="{{ money()->toCommon($vo_sub->paid_quantity ?? 0,2) }}" hidden>
                                                            <input type="text" id="vo_baki{{ $vo_sub->id }}" value="{{ money()->toCommon($vo_sub->baki_quantity ?? 0,2) }}" hidden>
                                                            {{ money()->toCommon($vo_sub->paid_quantity ?? 0,2 )}}
                                                        </td>
                                                        <td><input type="text"  id="vo_qty_edit{{ $vo_sub->id }}"  name="vo[{{ $vo_sub->id }}][qty]"  class="money voqty" onkeyup="calc_vo({{ $vo_sub->id }})" 
                                                            @if(!empty($ipc) && $ipc->last_ipc == 'n' && !empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() > 0)
                                                                value="{{ money()->toCommon($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->sum('quantity') ?? 0,2) }}" 
                                                            @elseif(!empty($ipc) && $ipc->last_ipc == 'y' && (empty($ipc->ipcVo) || (!empty($ipc->ipcVo) && (empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) || (!empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() < 1) ))) ))
                                                                value="{{ money()->toCommon($vo_sub->baki_quantity,2) }}"
                                                            @elseif(!empty($ipc) && $ipc->last_ipc == 'y' && (!empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() > 0 ))
                                                                value="{{ money()->toCommon(($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->sum('quantity') + $vo_sub->baki_quantity) ?? 0,2) }}" 
                                                            @endif
                                                            style="width:100px"
                                                            >
                                                        </td>
                                                        <td>
                                                            <input type="text"  id="vo_amt_edit{{ $vo_sub->id }}" name="vo[{{ $vo_sub->id }}][amt]"  class=" voamt" onkeyup="calc_vo({{ $vo_sub->id }})" 
                                                            @if(!empty($ipc) && $ipc->last_ipc == 'n' && !empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() > 0)
                                                                value="{{ money()->toCommon($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->sum('amount') ?? 0,2) }}" 
                                                            @elseif(!empty($ipc) && $ipc->last_ipc == 'y' && (empty($ipc->ipcVo) || (!empty($ipc->ipcVo) && (empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) || (!empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() < 1) ))) ))
                                                                value="{{ (money()->toCommon($vo_sub->baki_quantity) * money()->toCommon($vo_sub->rate_per_unit)) }}"
                                                            @elseif(!empty($ipc) && $ipc->last_ipc == 'y' && (!empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() > 0 ))
                                                                value="{{ (money()->toCommon(($vo_sub->baki_quantity + $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->sum('quantity'))) * money()->toCommon($vo_sub->rate_per_unit)) }}"
                                                            @endif
                                                            style="width:100px"
                                                            readonly="true">
                                                        </td>
                                                        <td>
                                                            @if(!empty($ipc) && !empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() > 0)
                                                            <input type="text" id="ipc_vo_id{{ $vo_sub->id }}" name="vo[{{ $vo_sub->id }}][ipc_vo_id]" value="{{ $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->pluck('id')->first() }}" hidden>
                                                            @endif
                                                            <input type="checkbox" id="vo_check{{ $vo_sub->id }}" name="vo[{{ $vo_sub->id }}][check]"  onClick="tukar_vo({{ $vo_sub->id }})" 
                                                            @if($vo_sub->baki_quantity == 0 && !empty($ipc) && $ipc->last_ipc == 'y' && (empty($ipc->ipcVo) || (!empty($ipc->ipcVo) && empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)))))
                                                                disabled="true"
                                                            @elseif(!empty($ipc) && $ipc->last_ipc == 'n' && !empty($ipc->ipcVo) && !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) && $ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)->count() > 0)
                                                                checked="true"
                                                            @elseif(!empty($ipc) && $ipc->last_ipc == 'y' && (empty($ipc->ipcVo) || (!empty($ipc->ipcVo) && (empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)) || !empty($ipc->ipcVo->where('status','V')->where('ppjhk_sub_item_id',$vo_sub->id)))) ))
                                                                checked="true"
                                                            @endif
                                                            >
                                                        </td>
                                                    </tr>
                                                    @push('scripts')
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function($) {
                                                            tukar_vo({{ $vo_sub->id }});
                                                        });
                                                    </script>
                                                    @endpush
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @push('scripts')
                                    <script type="text/javascript">
                                        jQuery(document).ready(function($) {
                                            $("#vo{{ $vo_it->id }}").DataTable();
                                        });
                                    </script>
                                    @endpush
                                    @endif
                                    @endforeach
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @endif
    @if(!empty($ppjhk->cancels) && !empty($ppjhk->cancels->where('ppjhk_status',1)->where('status',1)) && $ppjhk->cancels->where('ppjhk_status',1)->where('status',1)->count() > 0)
    <hr>
    <h6>Wang Peruntukan Tidak Digunakan</h6>
        <h7><u>{{ $ppjhk->cancels->where('ppjhk_status',1)->where('status',1)->pluck('element')->pluck('element')->first() }}</u></h7>
        
            <div class="row">
                <div class="table-responsive table-bordered">
                    <p>@if(!empty($ppjhk->cancels->where('ppjhk_status',1)->where('status',1)->pluck('element')->pluck('description')->first())) 
                        <u>{{ $ppjhk->cancels->where('ppjhk_status',1)->where('status',1)->pluck('element')->pluck('description')->first() }}</u> @endif</p>
                    <table id="cancel_table" class="table table-bordered" width="100%" cellspacing="0">
                        <thead>
                            <th>PERKARA</th>
                            <th>JUMLAH DIBATALKAN (RM)</th>
                        </thead>
                        <tbody>
                            @foreach($ppjhk->cancels->where('ppjhk_status',1)->where('status',1) as $can)
                            <tr>
                                <td>{{ $can->item->item }}</td>
                                <td>
                                    <input type="text" class="form-control cancel_amt" value="{{ money()->toCommon($can->amount ?? 0, 2) }}" readonly>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        
    @endif
    @if(empty($ppjhk->elements) && empty($voCancel) && ($ppjhk->elements->count() == 0 || $voCancel->count() == 0))
    <br>
    <p>- Tiada Rekod PPJHK</p><br>
    @endif
<hr>
</div>
@endif
@endforeach
@endif
<h5>Amaun Perubahan</h5>
<div class="row">
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('Amaun Perubahan Kerja (RM)'),
        'id' => 'working_change_amount',
        'name' => 'working_change_amount',
        'readonly' => true,
    ])  
    </div>
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('Jumlah Kontrak Baru (RM)'),
        'id' => 'new_contract_amount',
        'name' => 'new_contract_amount',
        'readonly' => true,
    ])  
    </div>
</div>
