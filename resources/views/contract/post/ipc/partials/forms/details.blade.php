<h5>Maklumat Kontraktor</h5>
@include('components.forms.input', [
    'input_label' => __('Nama Kontraktor'),
    'id' => 'contractor_name',
    'name' => 'contractor_name',
    'readonly' => true,
])
<div class="row">
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('No Pendaftaran Kontraktor'),
        'id' => 'contractor_ssm',
        'name' => 'contractor_ssm',
        'readonly' => true,
    ])
    </div>
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('No Pendaftaran GST'),
        'id' => 'contractor_gst',
        'name' => 'contractor_gst',
        'readonly' => true,
    ])
    </div>
</div>
@include('components.forms.textarea', [
    'input_label' => __('Alamat Kontraktor'),
    'id' => 'contractor_add',
    'name' => 'contractor_add',
    'readonly' => true,
])
<hr>
<h5>Maklumat Kontrak</h5>
@include('components.forms.textarea', [
    'input_label' => __('Tajuk'),
    'id' => 'contract_title',
    'name' => 'contract_title',
    'readonly' => true,
])
<div class="row">
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('Nilai Kontrak'),
        'id' => 'contract_value',
        'name' => 'contract_value',
        'readonly' => true,
    ])
    </div>
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('Tarikh Surat Setuju Terima'),
        'id' => 'al_issue_date',
        'name' => 'al_issue_date',
        'readonly' => true,
    ])
    </div>
</div>
<div class="row">
    <div class="col-5">
    @include('components.forms.input', [
        'input_label' => __('Tarikh Mula Kerja'),
        'id' => 'start_working_date',
        'name' => 'start_working_date',
        'readonly' => true,
    ])
    </div>
    <div class="col-2">
    @include('components.forms.input', [
        'input_label' => __('Tempoh'),
        'id' => 'period_type_id',
        'name' => 'period_type_id',
        'readonly' => true,
    ])
    </div>
    <div class="col-5">
    @include('components.forms.input', [
        'input_label' => __('Tarikh Siap Kerja'),
        'id' => 'end_working_date',
        'name' => 'end_working_date',
        'readonly' => true,
    ])
    </div>
</div>
<div class="row">
    <div class="col-6">
    @include('components.forms.input', [
        'input_label' => __('Tarikh Kontrak diTanda Tangani'),
        'id' => 'vp_sign_date',
        'name' => 'vp_sign_date',
        'readonly' => true,
    ])
    </div>
</div>