@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        // // serialize array for bq
        //     @foreach($ipc->sst->acquisition->bqElements as $bq)
        //         @foreach($bq->bqItems as $it)
        //             $hurm = $("#ulasan_bq{{ $n }}_{{ $it->id }}").DataTable({
        //                 order: false,
        //                 paging: false,
        //             });
        //         @endforeach
        //     @endforeach
        // // end serialize array for bq
        $('.addqty{{ $n }}').change(function(){
            var ttl     = 0;
            var totl    = 0.00;
            var total   = 0.00;
            $('.addamt{{ $n }}').each(function(){
                if($(this).val() !== '')
                {
                    ttl  = $(this).val();
                    ttl  = ttl.replace(/[,]/g, "");
                    ttl  = ttl.split(' ').join('');

                    totl  = parseFloat(ttl);
                    total = total + totl;
                }
            });
            $('#demand_amount{{ $n }}').val(parseFloat(total).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
            var sst2 = 0;
                if($('#sst_amount{{ $n }}').val()){
                    sst2 = $('#sst_amount{{ $n }}').val();
                    sst2 = sst2.replace(/[,]/g, "");
                    sst2 = sst2.split(' ').join('');
                }
                
            var final2 = parseFloat(total) + parseFloat(sst2);
            $('#invoice{{ $n }}').val(final2.toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
        });
        $('#sst_amount{{ $n }}').on('change', function(){
            var demand = 0;
            var sst = $(this).val();
            sst = sst.replace(/[,]/g, "");
            sst = sst.split(' ').join('');
            
            if($('#demand_amount{{ $n }}').val()){
                demand = $('#demand_amount{{ $n }}').val();
                demand = demand.replace(/[,]/g, "");
                demand = demand.split(' ').join('');
            }
            var jumlah = parseFloat(demand) + parseFloat(sst);
            $('#invoice{{ $n }}').val(jumlah.toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
        });
        var checkLast = document.getElementById("last_ipc");
        if(checkLast.checked){
            $('.addqty{{ $n }}').attr('readonly',true);
            var $ttl    = 0;
            var $totl   = 0.00;
            var $total  = 0.00;
            $('.addamt{{ $n }}').each(function(){

                if($(this).val() !== '')
                {
                    $ttl   = $(this).val();
                    $ttl   = $ttl.replace(/[,]/g, "");
                    $ttl   = $ttl.split(' ').join('');
                    
                    $totl  = parseFloat($ttl);
                    $total = $total + $totl; 
                }
            });
            
            $('#demand_amount{{ $n }}').val(parseFloat($total).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
            
            var sst1 = 0;
            if($('#sst_amount{{ $n }}').val()){
                sst1 = $('#sst_amount{{ $n }}').val();
                sst1 = sst1.replace(/[,]/g, "");
                sst1 = sst1.split(' ').join('');
            }
            
            var final1 = parseFloat($total) + parseFloat(sst1);
            $('#invoice{{ $n }}').val(final1.toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));

        }
        
    });
</script>
@endpush
<div id="quantity{{ $n }}">
<h5>Senarai Kuantiti</h5>
<br>
<div id="table" class="table-editable table-bordered border-0">

    @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->acquisition) && !empty($ipc->sst->acquisition->bqElements))
    @foreach($ipc->sst->acquisition->bqElements->sortBy('no') as $bq)
    <div class="mb-0 card" >
        <div class="card-header" role="tab" id="heading{{ $n }}_{{ $bq->id }}">
            <h4 class="card-title cardElemen">
                <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{ $n }}_{{ $bq->id }}" href="#collapse{{ $n }}_{{ $bq->id }}" aria-expanded="true" aria-controls="collapse{{ $n }}_{{ $bq->id }}">
                    <i class="mr-2 fa fa-folder-open"></i><u>{{ $bq->no}}&nbsp;:&nbsp;{{ $bq->element}} &nbsp;&nbsp;&nbsp;RM{{ money()->toCommon($bq->amount_adjust ?? 0,2 )}}</u>
                </a>
            </h4>
        </div>
        <div id="collapse{{ $n }}_{{ $bq->id }}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $n }}_{{ $bq->id }}" >
            <div class="card-body">
                <div class="row">
                    <div id="semakanBq{{ $n }}" class="col-md-12 table-responsive">
                        <p>@if(!empty($bq->description)) <u>{{ $bq->description }}</u> @endif</p>
                        <table class="table1  table-no-border" width="100%" cellspacing="0">
                            @if(!empty($bq->bqItems))
                                @foreach($bq->bqItems->sortBy('no') as $it)
                                    @if($prov_id != $it->bq_element_id)
                                    <thead >
                                        <th style="width:100%;border:none" >
                                            <h5>{{ $it->bq_no_element}}.{{ $it->no}}&nbsp;:&nbsp;{{ $it->item}} &nbsp;&nbsp;&nbsp;RM{{ money()->toCommon($it->amount_adjust ?? 0,2 )}}</h5><br>
                                            <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                        </th>
                                    </thead>
                                    <tbody class="list">
                                        <tr style="width:100%;border:none">
                                            <td class="ulasanBQs">
                                                <table id="ulasan_bq{{ $n }}_{{ $it->id }}" class="table table-bordered" width="100%" cellspacing="0">
                                                    <thead>
                                                        <th style="width:5%">NO SUB ITEM</th>
                                                        <th style="width:10%">SUB ITEM</th>
                                                        <th style="width:10%">KETERANGAN</th>
                                                        <th style="width:10%">UNIT</th>
                                                        <th style="width:10%">KUANTITI</th>
                                                        <th style="width:10%">HARGA SEUNIT TERLARAS</th>
                                                        <th style="width:10%">AMAUN TERLARAS</th>
                                                        <th style="width:10%">KUANTITI TERKUMPUL</th>
                                                        <th style="width:10%">KUANTITI SIAP</th>
                                                        <th style="width:10%">AMAUN TUNTUTAN</th>
                                                        <th style="width:5%"></th>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($it->bqSubItems))
                                                        @foreach($it->bqSubItems->sortBy('no') as $key=>$bqs)
                                                            
                                                                <tr>
                                                                    <td>{{ $bqs->bq_no_element}}.{{ $bqs->bq_no_item}}.{{ $bqs->no}}</td>
                                                                    <td>{{ $bqs->item}}</td>
                                                                    <td>{{ $bqs->description}}</td>
                                                                    <td>{{ $bqs->unit}}</td>
                                                                    <td align="right"><input type="text" id="qty_baki{{ $n }}_{{ $bqs->id }}" value="{{ money()->toCommon($bqs->baki_quantity ?? 0,2) }}" hidden>{{ money()->toCommon($bqs->quantity ?? 0,2 )}}</td>
                                                                    <td align="right"><input type="text" id="qty_rate{{ $n }}_{{ $bqs->id }}" value="{{ money()->toCommon($bqs->rate_per_unit_adjust ?? 0,2 ) }}" hidden>{{ money()->toCommon($bqs->rate_per_unit_adjust ?? 0, 2)}}</td>
                                                                    <td align="right">
                                                                        <input type="text" id="status{{ $n }}_{{ $bqs->id }}" value="N" hidden
                                                                            @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity != 0) {{-- last ipc --}}
                                                                                name="adjust[{{ $n }}_{{ $bqs->id}}][status]"
                                                                            @endif
                                                                        >
                                                                        {{ money()->toCommon($bqs->amount_adjust ?? 0,2 )}}
                                                                    </td>
                                                                    <td align="right"><input type="text" id="qty_paid{{ $n }}_{{ $bqs->id }}"  value="{{ money()->toCommon($bqs->paid_quantity ?? 0,2) }}" hidden>{{ money()->toCommon($bqs->paid_quantity ?? 0,2 )}}</td>
                                                                    <td> {{-- value for qty siap --}}
                                                                        <input type="text"  id="limit_current{{ $n }}_{{ $bqs->id }}"  class="money addqty{{ $n }}" onkeyup="calc_bq({{ $bqs->id}},{{ $n }})" 
                                                                        @if(!empty($ipc_bq) && !empty($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)) && $ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('quantity')->first() > '0' )
                                                                            value="{{ money()->toCommon($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('quantity')->first() ?? 0,2) }}" 
                                                                        @else
                                                                            value="0.00"
                                                                        @endif
                                                                        hidden>
                                                                        <input type="text"  id="qty_current{{ $n }}_{{ $bqs->id }}"  class="money addqty{{ $n }}" onkeyup="calc_bq({{ $bqs->id}},{{ $n }})" 
                                                                        @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity != 0) {{-- last ipc --}}
                                                                            value="{{ money()->toCommon($bqs->baki_quantity,2) }}" name="adjust[{{ $n }}_{{ $bqs->id}}][quantity]" 
                                                                        @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)) && $ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('quantity')->first() > '0' )
                                                                            value="{{ money()->toCommon($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('quantity')->first() ?? 0,2) }}" 
                                                                        @endif
                                                                        style="width:100px"
                                                                        >
                                                                    </td>
                                                                    <td> {{-- value for amt tuntutan --}}
                                                                        <input type="text" id="qty_amount{{ $n }}_{{ $bqs->id }}"  class="money addamt{{ $n }}" 
                                                                        @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity != 0) {{-- last ipc --}}
                                                                            value="{{ (money()->toCommon($bqs->baki_quantity) * money()->toCommon($bqs->rate_per_unit_adjust)) }}" name="adjust[{{ $n }}_{{ $bqs->id}}][amount]"
                                                                        @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)) && $ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('amount')->first() > '0')
                                                                            value="{{ money()->toCommon($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('amount')->first() ?? 0,2) }}" 
                                                                        @endif  
                                                                        style="width:100px"
                                                                        >
                                                                    </td>
                                                                    <td> {{-- checkbox --}}
                                                                        <input type="text" id="qty_edit{{ $n }}_{{ $bqs->id }}"
                                                                        @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity != 0)
                                                                            name="adjust[{{ $n }}_{{ $bqs->id}}][edit]"
                                                                        @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)) && $ipc_bq->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('amount')->first() > '0' )
                                                                            value="{{ $ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('id')->first() }}"
                                                                        @endif
                                                                        hidden>
                                                                        <input type="checkbox" id="qty_id{{ $n }}_{{ $bqs->id }}" value="{{ $bqs->id }}" onClick="tukar({{ $bqs->id }},{{ $n }})" 
                                                                        @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity != 0) {{-- last ipc --}}
                                                                            checked="true" name="adjust[{{ $n }}_{{ $bqs->id}}][id]"
                                                                        @elseif($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity == 0)
                                                                            disabled="true"
                                                                        @elseif($n != '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity == 0 && (empty($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)) || $ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->count()  ==  '0'))
                                                                            disabled="true"
                                                                        @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)) && $ipc_bq->where('status','N')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$bqs->id)->pluck('amount')->first() > 0)
                                                                            checked="true"
                                                                        @endif
                                                                        ></td>
                                                                </tr>
                                                                {{-- @push('scripts')
                                                                <script type="text/javascript">
                                                                    jQuery(document).ready(function($) {
//                                                                        tukar({{ $bqs->id }},{{ $n }});
                                                                    });
                                                                </script>
                                                                @endpush --}}
                                                        @endforeach
                                                    @endif         
                                                    {{-- @push('scripts')
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function($) {
                                                            // $("#ulasan_bq{{ $n }}_{{ $it->id }}").DataTable();
                                                        });
                                                    </script>
                                                    @endpush --}}
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @else
                                    <thead >
                                        <th style="width:100%;border:none" >
                                            <h5>{{ $it->bq_no_element}}.{{ $it->no}}&nbsp;:&nbsp;{{ $it->item}} &nbsp;&nbsp;&nbsp;RM{{ money()->toCommon($it->amount_adjust ?? 0,2 )}}</h5><br>
                                            <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                        </th>
                                    </thead>
                                    <tbody class="list">
                                        <tr style="width:100%;border:none">
                                            <td class="ulasanBQs">
                                                <table id="ulasan_bq{{ $n }}_{{ $it->id }}" class="table table-bordered" width="100%" cellspacing="0">
                                                    <thead>
                                                        {{-- <th style="width:5%">NO SUB ITEM</th> --}}
                                                        <th style="width:15%">SUB ITEM</th>
                                                        <th style="width:10%">KETERANGAN</th>
                                                        <th style="width:10%">UNIT</th>
                                                        <th style="width:10%">KUANTITI</th>
                                                        <th style="width:10%">HARGA SEUNIT TERLARAS</th>
                                                        <th style="width:10%">AMAUN TERLARAS</th>
                                                        <th style="width:10%">KUANTITI TERKUMPUL</th>
                                                        <th style="width:10%">KUANTITI SIAP</th>
                                                        <th style="width:10%">AMAUN TUNTUTAN</th>
                                                        <th style="width:5%"></th>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($vo_active))
                                                        @foreach($vo_active as $vo_act)
                                                            @if($vo_act->bq_element_id == $it->bq_element_id && $vo_act->bq_item_id == $it->id && !empty($all_vo_sub) && !empty($all_vo_sub->where('vo_active_id',$vo_act->id)))
                                                                @foreach($all_vo_sub->where('vo_active_id',$vo_act->id)->all() as $sub_vo)
                                                                <tr>
                                                                    {{-- <td>{{ $it->bq_no_element}}.{{ $it->no}}.{{ $sub_vo->no }}</td> --}}
                                                                    <td>{{ $sub_vo->item }}</td>
                                                                    <td>{{ $sub_vo->description }}</td>
                                                                    <td>{{ $sub_vo->unit }}</td>
                                                                    <td align="right"><input type="text" id="qty_baki{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}" value="{{ money()->toCommon($sub_vo->baki_quantity ?? 0,2) }}" hidden>{{ money()->toCommon($sub_vo->quantity_addition ?? 0, 2) }}</td>
                                                                    <td align="right"><input type="text" id="qty_rate{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}" value="{{ money()->toCommon($sub_vo->rate_per_unit ?? 0,2 ) }}" hidden>{{ money()->toCommon($sub_vo->rate_per_unit ?? 0, 2) }}</td>
                                                                    <td align="right">
                                                                        <input type="text" id="status{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}" value="V" hidden
                                                                            @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $bqs->baki_quantity != 0) {{-- last ipc --}}
                                                                                name="adjust[{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id}}][status]"
                                                                            @endif
                                                                        >
                                                                        {{ money()->toCommon($sub_vo->amount_addition ?? 0, 2) }}
                                                                    </td>
                                                                    <td align="right"><input type="text" id="qty_paid{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}"  value="{{ money()->toCommon($sub_vo->paid_quantity ?? 0,2) }}" hidden>{{ money()->toCommon($sub_vo->paid_quantity ?? 0, 2) }}</td>
                                                                    <td>{{-- value for qty siap --}} 
                                                                        <input type="text"  id="limit_current{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}"  class="money addqty{{ $n }}" onkeyup="calc_bq(a{{ $vo_act->id }}{{ $sub_vo->id}},{{ $n }})" 
                                                                        @if(!empty($ipc_bq) && !empty($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)) && $ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('quantity')->first() > '0' )
                                                                            value="{{ money()->toCommon($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('quantity')->first() ?? 0,2) }}" 
                                                                        @else
                                                                            value="0.00"
                                                                        @endif
                                                                         hidden>
                                                                        <input type="text"  id="qty_current{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}"  class="money addqty{{ $n }}" onkeyup="calc_bq('a{{ $vo_act->id }}{{ $sub_vo->id}}',{{ $n }})" 
                                                                            @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $sub_vo->baki_quantity != 0) {{-- last ipc --}}
                                                                                value="{{ money()->toCommon($sub_vo->baki_quantity,2) }}" name="adjust[{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id}}][quantity]" 
                                                                            @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)) && $ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('quantity')->first() > '0' )
                                                                                value="{{ money()->toCommon($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('quantity')->first() ?? 0,2) }}" 
                                                                            @endif
                                                                            style="width:100px"
                                                                            >
                                                                    </td>
                                                                    <td> {{-- value for amt tuntutan --}}   
                                                                        <input type="text" id="qty_amount{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}"  class="money addamt{{ $n }}" 
                                                                            @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $sub_vo->baki_quantity != 0) {{-- last ipc --}}
                                                                                value="{{ (money()->toCommon($sub_vo->baki_quantity) * money()->toCommon($sub_vo->rate_per_unit)) }}"  name="adjust[{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id}}][amount]"
                                                                            @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)) && $ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('amount')->first() > '0')
                                                                                value="{{ money()->toCommon($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('amount')->first() ?? 0,2) }}" 
                                                                            @endif  
                                                                            style="width:100px"
                                                                            >
                                                                    </td>
                                                                    <td>{{-- checkbox --}}
                                                                        <input type="text" id="qty_edit{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}" 
                                                                            @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $sub_vo->baki_quantity != 0){{-- last ipc --}}
                                                                                name="adjust[{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id}}][edit]"
                                                                            @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)) && $ipc_bq->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('amount')->first() > '0' )
                                                                                value="{{ $ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('id')->first() }}"
                                                                            @endif
                                                                        hidden>
                                                                            <input type="checkbox" id="qty_id{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id }}" value="{{ $sub_vo->id }}" onClick="tukar('a{{ $vo_act->id }}{{ $sub_vo->id }}',{{ $n }})" 
                                                                            @if($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $sub_vo->baki_quantity != 0) {{-- last ipc --}}
                                                                                checked="true" name="adjust[{{ $n }}_a{{ $vo_act->id }}{{ $sub_vo->id}}][id]"
                                                                            @elseif($n == '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $sub_vo->baki_quantity == 0)
                                                                                disabled="true"
                                                                            @elseif($n != '0' && !empty($ipc) && $ipc->last_ipc == 'y' && $sub_vo->baki_quantity != 0 && (empty($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)) || $ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->count() == '0'))
                                                                                disabled="true"
                                                                            @elseif(!empty($ipc_bq) && !empty($ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)) && $ipc_bq->where('status','V')->where('ipc_invoice_id',$n)->where('bq_subitem_id',$sub_vo->id)->pluck('amount')->first() > 0)
                                                                                checked="true"
                                                                            @endif
                                                                            >
                                                                    </td>
                                                                </tr>
                                                                {{-- @push('scripts')
                                                                <script type="text/javascript">
                                                                    jQuery(document).ready(function($) {
//                                                                        tukar("a{{ $vo_act->id }}{{ $sub_vo->id }}",{{ $n }});
                                                                    });
                                                                </script>
                                                                @endpush --}}
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                                {{-- @push('scripts')
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function($) {
                                                            // $("#ulasan_bq{{ $n }}_{{ $it->id }}").DataTable();
                                                        });
                                                    </script>
                                                @endpush --}}
                                                </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                    
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div><br>
<div class="row">
    <div class="col-6">
    @include('components.forms.input', [
        'id' => 'demand_amount'.$n,  
        'input_label' => 'Amaun Tuntutan (RM)',
        'readonly' => true,
    ])
    </div>
</div>
</div>