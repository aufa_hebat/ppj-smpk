<div id="subkontrak{{ $n }}">
<h5>Kontraktor</h5>
<input id="subkon_id{{ $n }}" hidden/>
<div class="col-md-12">
    <div class="form-group">
        <div class="table-responsive">
            <div  class="">
                <table id="table_kod" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="15%" ><b>Nama Syarikat</b></th>
                            <th width="15%" ><b>Jenis Kontraktor</b></th>
                            <th width="20%"><b>Harga Maksima (RM)</b></th>
                            <th width="20%"><b>Bayaran Terkumpul (RM)</b></th>
                            <th width="5%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $appointed->company->company_name ?? ''}}</td>
                            <td>Kontraktor Utama</td>
                            <td>
                                @if(!empty($company) && $company->count() > 0)
                                    {{ money()->toCommon(($appointed->offered_price - $company->pluck('amount')->sum()), 2) }}
                                @else
                                    {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}
                                @endif
                            </td>
                            <td>
                                @if(!empty($invoice_all) && !empty($invoice_all->where('company_id',$appointed->company_id)) && $invoice_all->where('company_id',$appointed->company_id)->count() > 0 )
                                    {{ money()->toCommon($invoice_all->where('company_id',$appointed->company_id)->pluck('invoice')->sum(),2) }}
                                @else
                                    0.00
                                @endif
                            </td>
                            <td><input type="radio" id="inv_company_id{{ $n }}_{{ $appointed->company_id }}_1" onclick="set_name_comp({{ $n }},{{ $appointed->company_id }},1)" value="{{ $appointed->company_id }}">
                            </td>
                        </tr>
                    @if(!empty($company) && $company->count() > 0)
                    @foreach($company as $k => $com)
                    
                        <tr>
                            <td>{{ $com->company->company_name }}</td>
                            <td>Sub Kontraktor</td>
                            <td>{{ money()->toCommon($com->amount ?? 0, 2) }}</td>
                            <td>
                                @if(!empty($invoice_all) && !empty($invoice_all->where('company_id',$appointed->company_id)) && $invoice_all->where('company_id',$appointed->company_id)->count() > 0 )
                                    {{ money()->toCommon($invoice_all->where('company_id',$com->company_id)->pluck('invoice')->sum(),2) }}
                                @else
                                    0.00
                                @endif
                            </td>
                            <td><input type="radio" id="inv_company_id{{ $n }}_{{ $com->company_id }}_2" onclick="set_name_comp({{ $n }},{{ $com->company_id }},2)" value="{{ $com->company_id }}">
                            </td>
                        </tr>
                    
                    @endforeach
                    @endif
                    </tbody>
               </table>
            </div>
        </div>
        <input type="text" id="inv_status_contractor" hidden>
    </div>
</div>
</div>