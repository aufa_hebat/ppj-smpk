<div id="invois_info{{ $n }}">
<h5>Maklumat Invois</h5>
<div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('No Inbois'),
                'id' => 'invoice_no'.$n,
                'maxlength' => 16,
            ])
        </div>
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('Amaun Inbois (RM)'),
                'id' => 'invoice'.$n,
                'readonly' => 'true',
            ])
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Inbois'),
                'id' => 'invoice_at'.$n,
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('Amaun SST (RM)(jika berkenaan)'),
                'id' => 'sst_amount'.$n,
                'input_classes' => 'money',
            ])
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Inbois'),
                'id' => 'invoice_receive_at'.$n,
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('Keterangan Inbois (SAP)'),
                'id' => 'ref_text'.$n,
                'maxlength' => 16,
            ])
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <label for="Invois" class="col col-form-label"> Inbois </label>
            <input type="text" id="running{{ $n }}" value="{{ $n }}" hidden>
            <div class="col input-group">
                <input id="subFile{{ $n }}" type="text"  class="form-control" readonly>
                <label class="input-group-text" for="uploadFile{{ $n }}"><i class="fe fe-upload" ></i></label>
                <input type="file" class="form-control" id="uploadFile{{ $n }}" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                @if(null != $n && 0 != $n && !empty($ipc_invoice) && !empty($ipc_invoice->where('id',$n)) && !empty($ipc_invoice->where('id',$n)->pluck('document')->pluck('document_path')) && $ipc_invoice->where('id',$n)->pluck('document')->pluck('document_path')->first() != null)
                    <label class="input-group-text" for="downloadFile">
                        <a href="/download/{{ $ipc_invoice->where('id',$n)->pluck('document')->pluck('document_path')->first() }}/{{ $ipc_invoice->where('id',$n)->pluck('document')->pluck('document_name')->first() }}" target="_blank"><i class="fas fa-download"></i></a>
                    </label>
                    <label class="input-group-text" for="deleteFile{{ $ipc_invoice->where('id',$n)->pluck('document')->pluck('id')->first() }}">
                        <input type="input" id="deleteFile{{ $ipc_invoice->where('id',$n)->pluck('document')->pluck('id')->first() }}" class="buang_attach" value="{{ $ipc_invoice->where('id',$n)->pluck('document')->pluck('hashslug')->first() }}" hidden>
                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                    </label>
                @endif
            </div>
        </div>
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('Bulan Kerja (jika berkenaan)'),
                'id' => 'bulan_status'.$n,
            ])
        </div>
    </div>
</div>
</div>