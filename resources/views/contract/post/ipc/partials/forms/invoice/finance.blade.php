<div id="finance{{ $n }}">
<h5>Maklumat Kewangan</h5>
<div class="col-md-12">
    <div class="form-group">
        <div class="table-responsive">
            <div  class="">
                <table id="table_kod" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="15%" ><b>Bussiness Area</b></th>
                            <th width="20%"><b>Cost Centre</b></th>
                            <th width="15%"><b>Functional Area</b></th>
                            <th width="15%"><b>Fund</b></th>
                            <th width="15%"><b>Funded Programme</b></th>
                            <th width="15%"><b>GL Account</b></th>
                            <th width="5%"></th>
                        </tr>
                    </thead>
                    @if( !empty($ipc) && $ipc->sst->acquisition->approval->financials->count() > 0)
                    @foreach($ipc->sst->acquisition->approval->financials as $k => $finance)
                    <tbody>
                        <tr>
                            <td>{{ $finance->businessArea->name }}</td>
                            <td>{{ $finance->costCenter->name }}</td>
                            <td>{{ $finance->functionalArea->name }}</td>
                            <td>{{ $finance->fund->name }}</td>
                            <td>{{ $finance->fundedProgramme->name }}</td>
                            <td>{{ $finance->glAccount->name }}</td>
                            <td><input type="radio" id="inv_fund_id{{ $n }}_{{ $finance->id }}" onclick="set_name_fund({{ $n }},{{ $finance->id }})" value="{{$finance->id}}">
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                    @endif
               </table>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <span class="pull-right">
        @include('contract.post.ipc.partials.forms.ipc.submit', [
            'hashslug'      => $ipc->sst->hashslug,
            'route_name'    => 'api.contract.ipc.sijil.update',
            'form'          => 'ipc-form',
        ])
    </span>
</div>
</div>
