@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript">
        
        jQuery(document).ready(function($) {
            $('#ipc_no').val('{{ ($sst->ipc->count() > 0) ? $sst->ipc->count() + 1:1  }}').attr('readonly',true);
            $('#demand_amount').attr('readonly',true);
            $('#short_text').val('{{ (!empty($sst) && !empty($sst->contract_no))? $sst->contract_no:"" }}');
            $('#long_text').val('{{ $get_pre_desc }}');
            
            @if((!empty($kerja) && $kerja != null && 
                (!empty($sst) && !empty($sst->cmgd) && !empty($sst->cmgd->signature_date) && $sst->cmgd->signature_date != null) 
                && (!empty($sst) && !empty($sst->cpc) && !empty($sst->cpc->signature_date) && $sst->cpc->signature_date != null)
                ) || (!empty($bekalan) && $bekalan != null && 
                (!empty($sst) && !empty($sst->cpc) && !empty($sst->cpc->signature_date) && $sst->cpc->signature_date != null)))
                $('.showlast').show();
            @endif
            
            $('#last_ipc').on('change', function(){
                if(this.checked == true){
                    $('#last_ipc').val('y');
                }else{
                    $('#last_ipc').val('n');
                }
            });
            
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.post(route(route_name, id), data).then(response => {
                    swal('{!! __('Sijil Bayaran Interim') !!}', response.data.message, 'success');
                    redirect(route('contract.post.ipc.ipc_invoice_edit', response.data.hashslug));
                });
            });
        });
   </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if(!empty($new_eot_date))
                <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
            @endif
            
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#interim-sijil" role="tab" aria-controls="interim-sijil" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Sijil Interim') }}
                    </a>
                </li>
            </ul>
        </div>
        @component('components.card', ['card_classes' => 'col-10'])
            @slot('card_body')
            <div class="tab-content" id="interim-tab-content">
                
                <div class="tab-pane fade show active" id="interim-sijil" role="tabpanel" aria-labelledby="interim-sijil-tab">
                    <form id="sijil-form" >
                        <input type="text" id="sst_id" name="sst_id" value="{{ $sst->id }}" hidden>
                        @include('contract.post.ipc.partials.forms.ipc.sijil')
                    </form>
                    @include('contract.post.ipc.partials.forms.ipc.submit', [
                        'route_back'    => 'contract.post.ipc.edit',
                        'hashslug'      => $sst->hashslug,
                        'route_name'    => 'api.contract.ipc.sijil.store',
                        'form'          => 'sijil-form',
                    ])
                </div>
            </div>
            @endslot
        @endcomponent
    </div> 
@endsection