@extends('layouts.admin')
@push('styles')
        <style>
            
            .modal-content {
                position:relative;
                background-color: #fefefe;
                margin-left: -150px;
                padding: 0;
                border: 1px solid #888;
                width: 140%; /* Full width */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
                -webkit-animation-name: animatetop;
                -webkit-animation-duration: 0.4s;
                animation-name: animatetop;
                animation-duration: 0.4s

            }
        </style>
@endpush
@if(!empty($ipc))
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript">
        
        function tukar(v1,v2){//onchange bq for update quantity once click checkbox
            $("#qty_id"+v2+'_'+v1).attr('disabled',true);
            $("#qty_current"+v2+'_'+v1).attr('disabled',true);
//            calc_bq(v1,v2);
        }
        
        function tukar_vo(valu){//set all disabled
            $("#vo_check"+valu).attr('disabled',true);
            $("#vo_qty_edit"+valu).attr('disabled',true);
            $("#vo_amt_edit"+valu).attr('disabled',true);
        }
        jQuery(document).ready(function($) {

            @if((!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cmgd) && !empty($ipc->sst->cmgd->signature_date) && $ipc->sst->cmgd->signature_date != null)
                || (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->signature_date) && $ipc->sst->cpc->signature_date != null))
                $('.showlast').show();
            @endif

            $(document).on('click', '.vendor_ssm', function(event){
                event.preventDefault();
                @if(empty($ssm) || $ssm == "" || $ssm == null)
                    swal({
                        title: 'PERINGATAN',
                        text:  'Tiada No SSM didaftarkan',
                        type:  'info'
                    }).then((result) => {
                        event.preventDefault();
                    });
                @else
                    redirect('{{route("contract.post.sap_vendor", [$ssm,"contract.post.ipc.ipc_show",$ipc->hashslug])}}');
                @endif
            });
            
            $(document).on('click', '.print-action-btn', function(event) {
                event.preventDefault();

                var id = '{{ $ipc->sst->hashslug }}';
                swal({
                  title: 'PERINGATAN',
                  text: 'Sila cetak menggunakan kertas berwarna BIRU',
                  type: 'info'
                }).then((result) => {
                    window.open(route('sofaRpt', {hashslug:id}), '_blank');
                });
            });

            $("#showBq").DataTable();
            
            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ipc_id : $('#ipc_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ipc_id : $('#ipc_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ipc_id : $('#ipc_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });
            $('#sap_send').click(function(){
                var route_name = 'contract.post.sap';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    hashslug : $('#hashslug').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    ipc_id : $('#ipc_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    remarks : $('#remarks').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val()
                };
                console.log(data);
                swal({
                    title: '{!! __('Hantar SAP') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                if(response.data.err == 'S'){
                                    swal('Dokumen Berjaya Dihantar', response.data.message, 'success').then((result)=>{
                                        $('#savDraf').hide();
                                        $('#sap_send').hide();
                                        $('#rejSent').hide();
                                        redirect(route('home', response.data.hashslug));
                                    });
                                }else{
                                    swal('Dokumen Tidak Berjaya Dihantar', response.data.message, 'warning').then((result)=>{
                                    });
                                }
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            }); 

            //tab remain stay after refresh
            $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh
            
            // button for review
            $(".savDraf").on('click', function(e){
               $("#saveStatus").val(1);
            });
            $(".savSent").on('click', function(){
                $("#sentStatus").val(1);
            });
            $(".rejSent").on('click', function(){
                $("#rejectStatus").val(1);
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($s8) && $s8 = $review)
                @if($s8->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s8->remarks) !!}');
                @endif
            @elseif(!empty($s9) && $s9 = $review)
                @if($s9->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s9->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif
            
            @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                $('#ipc_no').val('{{ $ipc->penalty }}').attr('readonly',true);
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                $('#ipc_no').val('{{ $ipc->ipc_no }}').attr('readonly',true);
            @elseif($ipc->penalty != null && $ipc->last_ipc == 'y')
                $('#ipc_no').val('{{ $ipc->penalty }} (AKHIR)').attr('readonly',true);
            @else
                $('#ipc_no').val('{{ $ipc->ipc_no }}(AKHIR)').attr('readonly',true);
            @endif
            $('#short_text').val('{{ $ipc->short_text }}');
            $('#evaluation_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->evaluation_at)->format('d/m/Y g:i A')  }}');
            $('#long_text').val('{{ $ipc->long_text }}');
            $('#demand_amount').val('{{ money()->toCommon($ipc->demand_amount ?? 0, 2) }}');
            $("#demand_show").show();
            $('#last_ipc').val('{{ $ipc->last_ipc }}');
            $('#wjp_amount').val('{{ money()->toCommon($ipc->wjp_amount ?? 0, 2) }}');
            $('#wjp_lepas_amount').val('{{ money()->toCommon($ipc->wjp_lepas_amount ?? 0, 2) }}');
            $('#credit_no').val('{{ $ipc->credit_no }}');
            $('#credit').val('{{ money()->toCommon($ipc->credit ?? 0, 2) }}');
            $('#credit_at').val('{{ $ipc->credit_at }}');
            $('#credit_receive_at').val('{{ $ipc->credit_receive_at }}');
            $('#compensation_amount').val('{{ money()->toCommon($ipc->compensation_amount ?? 0, 2) }}');
            $('#working_change_amount').val('{{ money()->toCommon($ipc->working_change_amount ?? 0, 2) }}');
            $('#new_contract_amount').val('{{ money()->toCommon($ipc->new_contract_amount ?? 0, 2) }}');
            $('#material_on_site_amount').val('{{ money()->toCommon($ipc->material_on_site_amount ?? 0, 2) }}');
            $('#material_tahanan_amount').val('{{ money()->toCommon($ipc->material_tahanan_amount ?? 0, 2) }}');
            $('#advance_amount').val('{{ money()->toCommon($ipc->advance_amount ?? 0, 2) }}');
            $("#give_advance_amount").val('{{ money()->toCommon($ipc->give_advance_amount ?? 0, 2) }}');
            
            $('#ipc_no').attr('readonly',true);
            $('#short_text').attr('readonly',true);
            $('#evaluation_at').attr('readonly',true);
            $('#long_text').attr('readonly',true);
            $('#demand_amount').attr('readonly',true);
            $('#last_ipc').attr('readonly',true);
            $('#wjp_amount').attr('readonly',true);
            $('#wjp_lepas_amount').attr('readonly',true);
            $('#credit_no').attr('readonly',true);
            $('#credit').attr('readonly',true);
            $('#credit_at').attr('readonly',true);
            $('#credit_receive_at').attr('readonly',true);
            $('#compensation_amount').attr('readonly',true);
            $('#working_change_amount').attr('readonly',true);
            $('#new_contract_amount').attr('readonly',true);
            $('#material_on_site_amount').attr('readonly',true);
            $('#material_tahanan_amount').attr('readonly',true);
            
            @if(!empty($ipc->material_on_site_amount) && '0' != $ipc->material_on_site_amount)
                $("#material_tambah").show();
            @endif
            @if(!empty($ipc->material_tahanan_amount) && '0' != $ipc->material_tahanan_amount)
                $("#material_tolak").show();
            @endif
            @if(!empty($ipc->wjp_amount) && '0' != $ipc->wjp_amount)
                $("#wjp_tambah").show();
                $("#wjp_tambah_formula").show();
            @endif
            @if(!empty($ipc->wjp_lepas_amount) && '0' != $ipc->wjp_lepas_amount)
                $("#wjp_tolak").show();
                $("#wjp_tolak_formula").show();
            @endif
            @if(!empty($ipc->advance_amount) && '0' != $ipc->advance_amount)
                $("#adv_tambah").show();
            @endif
            @if(!empty($ipc->give_advance_amount) && '0' != $ipc->give_advance_amount)
                $("#adv_tolak").show();
                $("#adv_tolak_formula").show();
            @endif
            
//            $('#material_tolak').show();
//            $('#adv_tambah').show();
//            $('#adv_tolak').show();
//            $('#adv_tolak_formula').show();
//            $('#wjp_tambah').show();
//            $('#wjp_tolak').show();
            
            $('#uploadFileMat').attr('disabled',true);
            $('#uploadFilePerkeso').attr('disabled',true);
            $('#uploadFileSijil').attr('disabled',true);
            $('#uploadFileSummary').attr('disabled',true);
            $('#uploadFileStatus').attr('disabled',true);
            $('#uploadFileDetail').attr('disabled',true);
            $('#uploadFileLevi').attr('disabled',true);
            $('#uploadFileSofa').attr('disabled',true);
            $('#uploadFileStatuori').attr('disabled',true);

            @if( $ipc->doc_material )
                    $('#subFileMat').val('{{ $ipc->doc_material->document_name }}');
            @endif
            
            @if(!empty($ipc) && !empty($ipc->doc_sijil))
                $('#subFile{{$ipc->doc_sijil->id}}').val('{{ $ipc->doc_sijil->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->doc_summary))
                $('#subFile{{$ipc->doc_summary->id}}').val('{{ $ipc->doc_summary->document_name }}')
            @endif
            
            @if(!empty($ipc) && !empty($ipc->doc_status))
                $('#subFile{{$ipc->doc_status->id}}').val('{{ $ipc->doc_status->document_name }}')
            @endif
            @if(!empty($ipc) && !empty($ipc->doc_detail))
                $('#subFile{{$ipc->doc_detail->id}}').val('{{ $ipc->doc_detail->document_name }}')
            @endif
            @if(!empty($ipc) && !empty($ipc->doc_sofa))
                $('#subFile{{$ipc->doc_sofa->id}}').val('{{ $ipc->doc_sofa->document_name }}')
            @endif
            @if(!empty($ipc) && !empty($ipc->doc_statuori))
                $('#subFile{{$ipc->doc_statuori->id}}').val('{{ $ipc->doc_statuori->document_name }}')
            @endif
            @if(!empty($ipc) && !empty($ipc->ipcInvoice) && !empty($ipc->ipcInvoice->pluck('document')))
                $('#subFile{{$ipc->ipcInvoice->pluck('document')->pluck('id')->first()}}').val('{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->document))
                $('#subFile{{$ipc->sst->document->id}}').val('{{ $ipc->sst->document->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_publicLiability))
                @foreach($ipc->sst->insurance->documents_publicLiability as $public)
                    $('#subFile{{$public->id}}').val('{{ $public->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->documents))
                @foreach($ipc->sst->cpc->documents as $cpc)
                    $('#subFile{{$cpc->id}}').val('{{ $cpc->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->cnc) && !empty($ipc->sst->cnc->documents))
                @foreach($ipc->sst->cnc->documents as $cnc)
                    $('#subFile{{$cnc->id}}').val('{{ $cnc->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->cmgd) && !empty($ipc->sst->cmgd->documents))
                @foreach($ipc->sst->cmgd->documents as $cmgd)
                    $('#subFile{{$cmgd->id}}').val('{{ $cmgd->document_name }}')
                @endforeach
            @endif
            
            @if(!empty($sst))
                @foreach($sst as $ssts)
                    @if(!empty($ssts->ipc) && !empty($ssts->ipc->pluck('doc_sijil')))
                        @foreach($ssts->ipc->pluck('doc_sijil') as $ipc_all)
                            @if(!empty($ipc_all))
                                $('#subFile{{$ipc_all->id}}').val('{{ $ipc_all->document_name }}')
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_work))
                @foreach($ipc->sst->insurance->documents_work as $work)
                    $('#subFile{{$work->id}}').val('{{ $work->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_compensation))
                @foreach($ipc->sst->insurance->documents_compensation as $compensation)
                    $('#subFile{{$compensation->id}}').val('{{ $compensation->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->bon) && !empty($ipc->sst->bon->documents))
                @foreach($ipc->sst->bon->documents as $bon)
                    $('#subFile{{$bon->id}}').val('{{ $bon->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->doc_perkeso))
                $('#subFile{{$ipc->doc_perkeso->id}}').val('{{ $ipc->doc_perkeso->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->company) && !empty($ipc->sst->company->gst))
                $('#subFile{{$ipc->sst->company->gst->id}}').val('{{ $ipc->sst->company->gst->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->doc_levi))
                $('#subFile{{$ipc->doc_levi->id}}').val('{{ $ipc->doc_levi->document_name }}')
            @endif

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_pekeso').change(function(){
                $('#subFile_pekeso').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_cert').change(function(){
                $('#subFile_cert').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_summary').change(function(){
                $('#subFile_summary').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_status').change(function(){
                $('#subFile_status').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_detail').change(function(){
                $('#subFile_detail').val($(this).val().split('\\').pop());
            });

            @foreach($levi_doc as $db)
                var url = "/uploads/{{$db->document_name}}";

                $('.newwindow{{$db->document_id}}').click(function () {

                    var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindow = window.open(url, 'website', params);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($pekeso_doc as $db_pekeso)
                var urlpekeso = "/uploads/{{$db_pekeso->document_name}}";

                $('.newwindowpekeso{{$db_pekeso->document_id}}').click(function () {

                    var paramspekeso = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowpekeso = window.open(urlpekeso, 'website', paramspekeso);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($cert_doc as $db_cert)
                var urlcert = "/uploads/{{$db_cert->document_name}}";

                $('.newwindowcert{{$db_cert->document_id}}').click(function () {

                    var paramscert = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowcert = window.open(urlcert, 'website', paramscert);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($summary_doc as $db_sum)
                var urlsum = "/uploads/{{$db_sum->document_name}}";

                $('.newwindowsum{{$db_sum->document_id}}').click(function () {

                    var paramssum = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowsum = window.open(urlsum, 'website', paramssum);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($status_doc as $db_status)
                var urlstatus = "/uploads/{{$db_status->document_name}}";

                $('.newwindowstatus{{$db_status->document_id}}').click(function () {

                    var paramsstatus = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowstatus = window.open(urlstatus, 'website', paramsstatus);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach
            @foreach($detail_doc as $db_detail)
                var urlstatus = "/uploads/{{$db_detail->document_name}}";

                $('.newwindowstatus{{$db_detail->document_id}}').click(function () {

                    var paramsstatus = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowstatus = window.open(urlstatus, 'website', paramsstatus);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach
        });
   </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{{ $ipc->sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $ipc->sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $ipc->sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if(!empty($new_contract) && $new_contract != 0)
            <span class="font-weight-bold">Nilai Kontrak Baru: </span>RM {{ money()->toCommon($new_contract ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $ipc->sst->period." ".$ipc->sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if(!empty($new_eot_date))
                <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{$new_eot_date}}&nbsp;&nbsp;&nbsp;
            @endif
            <br>
            <span class="font-weight-bold"> No Sijil Interim: </span>{{ $ipc->ipc_no ?? '' }}
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#ipc-checklist" role="tab" aria-controls="ipc-checklist" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Senarai Semakan') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-sijil" role="tab" aria-controls="interim-sijil" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Sijil Interim') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-invois" role="tab" aria-controls="interim-invois" aria-selected="false">
                        @icon('fe fe-list')&nbsp;{{ __('Inbois') }}
                    </a>
                </li>
                @if(!empty($ipc->ipcMaterial) && $ipc->ipcMaterial->count() > 0)
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-material" role="tab" aria-controls="interim-material" aria-selected="false">
                        @icon('fe fe-folder')&nbsp;{{ __('Material on Site') }}
                    </a>
                </li>
                @endif
                @if(!empty($ipc->sst) && (!empty($ipc->sst->deposit)) && $ipc->sst->deposit->qualified_amount != '0')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-advance" role="tab" aria-controls="interim-advance" aria-selected="false">
                        @icon('fe fe-map-pin')&nbsp;{{ __('Wang Pendahuluan') }}
                    </a>
                </li>
                @endif
                @if($ipc->wjp_amount != '0' || $ipc->wjp_lepas_amount != '0')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-tahanan" role="tab" aria-controls="interim-tahanan" aria-selected="false">
                        @icon('fe fe-corner-up-right')&nbsp;{{ __('Wang Tahanan') }}
                    </a>
                </li>
                @endif
                @if($ipc->compensation_amount != '0')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-lost" role="tab" aria-controls="interim-lost" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Ganti Rugi') }}
                    </a>
                </li>
                @endif
                @if($ipc->working_change_amount != '0')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-vo" role="tab" aria-controls="interim-vo" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Perubahan Kerja') }}
                    </a>
                </li>
                @endif
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-credit" role="tab" aria-controls="interim-credit" aria-selected="false">
                        @icon('fe fe-book-open')&nbsp;{{ __('Nota Kredit') }}
                    </a>
                </li>
                
<!--                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-attach" role="tab" aria-controls="interim-attach" aria-selected="false">
                        @icon('fe fe-calendar')&nbsp;{{ __('Muat Naik') }}
                    </a>
                </li>-->

                @if(!empty($review))
                    
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#ipc-reviews" role="tab" aria-controls="ipc-reviews" aria-selected="false">
                            @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan') }}
                        </a>
                    </li>

                    @if(user()->current_role_login == 'administrator')
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#ipc-review-log" role="tab" aria-controls="ipc-review-log" aria-selected="false">
                                @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                            </a>
                        </li>
                    @endif

                    @if((user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'IPC') 
                    || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S4') && $review->status != 'Selesai') 
                    || (!empty($semakan4) && user()->id == $semakan4->approved_by && user()->id != $review->created_by && (empty($semakan5) || $reviewlog->status != 'S5') && $review->status != 'Selesai') 
                    || (!empty($semakan5) && user()->id == $semakan5->approved_by && user()->id != $review->created_by && (empty($cetaknotisbpub) || $reviewlog->status != 'CNBPUB') && $review->status != 'Selesai') 
                    || (!empty($semakan8) && user()->id == $semakan8->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S9') && $review->status != 'Selesai') 
                    || (!empty($semakan9) && user()->id == $semakan9->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'CNK') && $review->status != 'Selesai'))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#ipc-review" role="tab" aria-controls="ipc-review" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                            </a>
                        </li>
                    @endif

                @endif
                
            </ul>
        </div>
        @component('components.card', ['card_classes' => 'col-10'])
            @slot('card_body')
                <div class="tab-content" id="interim-tab-content">
                    <div class="tab-pane fade show active " id="ipc-checklist" role="tabpanel" aria-labelledby="ipc-checklist-tab">
                        @include('contract.post.ipc.partials.show.checklist')
                        <div class="btn-group float-right">
                            <a href="{{ route('contract.post.ipc.edit',$sst->hashslug) }}" 
                                    class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                            </a>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="interim-sijil" role="tabpanel" aria-labelledby="interim-sijil-tab">
                        <div class="btn-group float-right">
                            <a href="{{ route('ipc_cert',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Sijil Bayaran ') }}
                            </a>
                            <a href="{{ route('ipc_summary',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Ringkasan Bayaran ') }}
                            </a>
                            <a href="{{ route('ipc_status',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Status Bayaran ') }}
                            </a>
                            <a href="{{ route('ipc_butiran',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Butiran Bayaran ') }}
                            </a>
                            @if(!empty($ipc) && $ipc->last_ipc == 'y')
                            {{-- <a href="{{ route('sofaRpt',['hashslug' => $ipc->sst->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Sijil Muktamad ') }}
                            </a> --}}
                                <a target="_blank" href="#" class="btn btn-success print-action-btn">
                                    @icon('fe fe-printer') {{ __('Sijil Muktamad') }}
                                </a>
                            @endif
                        </div><br>
                        @include('contract.post.ipc.partials.forms.ipc.sijil')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade  " id="interim-invois" role="tabpanel" aria-labelledby="interim-invois-tab">
                        @include('contract.post.ipc.partials.forms.ipc.invoice')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-material" role="tabpanel" aria-labelledby="interim-material-tab">
                        @include('contract.post.ipc.partials.forms.ipc.material')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-advance" role="tabpanel" aria-labelledby="interim-advance-tab">
                        @include('contract.post.ipc.partials.forms.ipc.advance')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-tahanan" role="tabpanel" aria-labelledby="interim-tahanan-tab">
                        @include('contract.post.ipc.partials.forms.ipc.wjp')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-lost" role="tabpanel" aria-labelledby="interim-lost-tab">
                        @include('contract.post.ipc.partials.forms.ipc.lad')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-vo" role="tabpanel" aria-labelledby="interim-vo-tab">
                        @include('contract.post.ipc.partials.forms.ipc.vo')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-credit" role="tabpanel" aria-labelledby="interim-credit-tab">
                        @include('contract.post.ipc.partials.forms.ipc.credit')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
<!--                    <div class="tab-pane fade " id="interim-attach" role="tabpanel" aria-labelledby="interim-attach-tab">
                        @include('contract.post.ipc.partials.forms.ipc.attach')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>-->

                    <div class="tab-pane fade show" id="ipc-reviews" role="tabpanel" aria-labelledby="ipc-details-tab">
                        
                        @if(!empty($review))
                            @if((user()->current_role_login == 'penyedia'))
                                {{-- pegawai kewangan --}}
                                @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnkl" aria-expanded="false" aria-controls="cnkl">
                                                    Ulasan Semakan Oleh {{$cetaknotisklog->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnkl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s9log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s9log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s9log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- @if(!empty($semakan8log) && !empty($semakan8log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s8l" aria-expanded="false" aria-controls="s8l">
                                                    Ulasan Semakan Oleh {{$semakan8log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s8l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl6 as $s8log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s8log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s8log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s8log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s8log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif --}}

                                {{-- pegawai bpub --}}
                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubl" aria-expanded="false" aria-controls="cnbpubl">
                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnbpubl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5l" aria-expanded="false" aria-controls="s5l">
                                                    Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s5l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl4 as $s4log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif --}}

                                {{-- @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4l" aria-expanded="false" aria-controls="s4l">
                                                    Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s4l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl3 as $s3log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif --}}
                                {{-- pegawai pelaksana --}}
                                @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3l" aria-expanded="false" aria-controls="s3l">
                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s3l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2l" aria-expanded="false" aria-controls="s2l">
                                                    Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s2l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @elseif((user()->current_role_login == 'pengesah' && user()->department_id == 9))
                                {{-- pegawai kewangan --}}
                                @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnkl" aria-expanded="false" aria-controls="cnkl">
                                                    Ulasan Semakan Oleh {{$cetaknotisklog->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnkl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s9log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s9log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s9log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan8log) && !empty($semakan8log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s8l" aria-expanded="false" aria-controls="s8l">
                                                    Ulasan Semakan Oleh {{$semakan8log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s8l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl6 as $s8log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s8log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s8log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s8log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s8log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- pegawai bpub --}}
                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubl" aria-expanded="false" aria-controls="cnbpubl">
                                                    Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnbpubl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5l" aria-expanded="false" aria-controls="s5l">
                                                    Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s5l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl4 as $s4log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4l" aria-expanded="false" aria-controls="s4l">
                                                    Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s4l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl3 as $s3log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                                {{-- pegawai pelaksana --}}
                                @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3l" aria-expanded="false" aria-controls="s3l">
                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s3l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2l" aria-expanded="false" aria-controls="s2l">
                                                    Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s2l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                {{-- pegawai bpub --}}
                                @if(user()->department->id == '9')
                                    {{-- pegawai kewangan --}}
                                    @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnkl" aria-expanded="false" aria-controls="cnkl">
                                                        Ulasan Semakan Oleh {{$cetaknotisklog->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnkl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7 as $s9log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s9log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s9log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s9log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s9log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan8log) && !empty($semakan8log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s8l" aria-expanded="false" aria-controls="s8l">
                                                        Ulasan Semakan Oleh {{$semakan8log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s8l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6 as $s8log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s8log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s8log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s8log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s8log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubl" aria-expanded="false" aria-controls="cnbpubl">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5 as $s5log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5l" aria-expanded="false" aria-controls="s5l">
                                                        Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4 as $s4log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4l" aria-expanded="false" aria-controls="s4l">
                                                        Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3 as $s3log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    
                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                        @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3l" aria-expanded="false" aria-controls="s3l">
                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2 as $s2log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                @endif

                                {{-- pegawai pelaksana --}}
                                @if(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)
                                    
                                    @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3l" aria-expanded="false" aria-controls="s3l">
                                                        Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2 as $s2log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2l" aria-expanded="false" aria-controls="s2l">
                                                        Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1 as $s1log)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1log->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @endif
                            @endif
                        @endif

                    </div>
                    @if(!empty($ipc) && !empty($ipc->sst))
                        <div class="tab-pane fade show " id="ipc-review-log" role="tabpanel" aria-labelledby="ipc-review-log-tab">
                            <form id="log-form">
                                <div class="row justify-content-center w-100">
                                    <div class="col-12 w-100">
                                        @include('contract.pre.box.partials.scripts')
                                        @component('components.card')
                                            @slot('card_body')
                                                @component('components.datatable', 
                                                    [
                                                        'table_id' => 'contract-pre-box',
                                                        'route_name' => 'api.datatable.contract.review-log-ipc',
                                                        'param' => 'ipc_id=' . $ipc->id,
                                                        'columns' => [
                                                            ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                            ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                            ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                            ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                        ],
                                                        'headers' => [
                                                            __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                        ],
                                                        'actions' => minify('')
                                                    ]
                                                )
                                                @endcomponent
                                            @endslot
                                        @endcomponent
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                    @if(!empty($ipc) && !empty($ipc->sst))
                    <div class="tab-pane fade show" id="ipc-review" role="tabpanel" aria-labelledby="ipc-review-tab">
                        <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                            @csrf

                            @include('components.forms.hidden', [
                                'id' => 'id',
                                'name' => 'id',
                                'value' => ''
                            ])

                            @if(!empty($ipc) && !empty($ipc->sst))
                                @include('components.forms.hidden', [
                                    'id' => 'acquisition_id',
                                    'name' => 'acquisition_id',
                                    'value' => $ipc->sst->acquisition_id
                                ])
                            @endif

                            @if(!empty($review))
                            <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
                            <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                            <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
                            @endif

                            <div class="row">
                                <div class="col-6">
                                    @include('components.forms.input', [
                                        'input_label' => __('Nama Pegawai Semakan'),
                                        'id' => '',
                                        'name' => '',
                                        'value' => user()->name,
                                        'readonly' => true
                                    ])
                                </div>
                                    @include('components.forms.hidden', [
                                        'id' => 'requested_by',
                                        'name' => 'requested_by',
                                        'value' => user()->id
                                    ])
                                    @include('components.forms.hidden', [
                                        'id' => 'approved_by',
                                        'name' => 'approved_by',
                                        'value' => user()->supervisor->id
                                    ])

                                @if(!empty($review_previous))
                                    <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                @endif
                                
                                <input type="text" id="ipc_id" name="ipc_id" value="{{ $ipc->id }}" hidden>
                                <input type="text" id="type" name="type" value="IPC" hidden>

                                <div class="col-3">
                                    @if(!empty($review))
                                        @if(!empty($review->status))
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at',
                                                'name' => 'requested_at',
                                                'readonly' => 'true'
                                            ])
                                        @endif
                                    @endif
                                </div>
                                <div class="col-3">
                                    @if(!empty($review))
                                        @if(!empty($review->status))
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_ats',
                                                'name' => 'approved_at',
                                            ])
                                        @endif
                                    @endif
                                </div>
                            </div>

                                @include('components.forms.hidden', [
                                    'id' => 'approved_by',
                                    'name' => 'approved_by',
                                    'value' => user()->supervisor->id
                                ])

                            @if(!empty($review) && $review->created_by != user()->id)
                                @include('components.forms.textarea', [
                                    'input_label' => __('Ulasan'),
                                    'id' => 'remarks',
                                    'name' => 'remarks'
                                ])
                            @else

                            @endif
                            
                            <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                            <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                            <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                            <div class="btn-group float-right">
                                @if(!empty($review) && $review->created_by != user()->id)
                                    <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                        @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                    </button>
                                    <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                        @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                    </button>
                                    @if($review->status == 'S9')
                                        <input type="text" id="hashslug" name="hashslug" value="{{$ipc->hashslug}}" hidden>
                                        <div class="btn-group float-right">
                                            <button type="button" id="sap_send" class="btn btn-success border-success">SAP</button>
                                        </div>
                                    @elseif($review->status == 'S8')
                                        <a class="btn btn-success border-success vendor_ssm" href="{{route('contract.post.sap_vendor', [$ssm,"contract.post.ipc.ipc_show",$ipc->hashslug])}}">Semak Syarikat</a>
                                        <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                            @icon('fe fe-send') {{ __('Teratur') }}
                                        </button>
                                    @else
                                        <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                            @icon('fe fe-send') {{ __('Teratur') }}
                                        </button>
                                    @endif
                                @endif
                            </div>

                        </form>
                    </div>
                    @endif
                </div>
            @endslot
        @endcomponent
    </div> 
@endsection
@endif