@extends('layouts.admin')
@push('styles')
        <style>
            
            .modal-content {
                position:relative;
                background-color: #fefefe;
                margin-left: -150px;
                padding: 0;
                border: 1px solid #888;
                width: 140%; /* Full width */
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
                -webkit-animation-name: animatetop;
                -webkit-animation-duration: 0.4s;
                animation-name: animatetop;
                animation-duration: 0.4s

            }
        </style>
@endpush
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/previous_next.js') }}"></script>
    <script type="text/javascript">
        
        function calc_bq(val,val2){//for bq auto count amount based on rate
            var allow_max  = 0;
            var allow_min  = 0;
            var $result    = 0;
            var min_qty    = document.getElementById('qty_paid'+val2+'_'+val).value ;
            var max_qty    = document.getElementById('qty_baki'+val2+'_'+val).value ;
            var edit_qty   = document.getElementById('limit_current'+val2+'_'+val).value ;
            
                max_qty    = max_qty.replace(/[,]/g, "");
                max_qty    = max_qty.split(' ').join('');
                min_qty    = min_qty.replace(/[,]/g, "");
                min_qty    = min_qty.split(' ').join('');
                edit_qty   = edit_qty.replace(/[,]/g, "");
                edit_qty   = edit_qty.split(' ').join('');
                
            if(val2 != '0'){
                allow_max  = parseFloat(max_qty) + parseFloat(edit_qty);
                allow_min  = -(parseFloat(min_qty) + parseFloat(edit_qty));
            }else{
                allow_max  = parseFloat(max_qty);
                allow_min  = -(parseFloat(min_qty));
            }
            
            var $a = document.getElementById('qty_current'+val2+'_'+val).value;
            var $b = document.getElementById('qty_rate'+val2+'_'+val).value;
            $a = $a.replace(/[,]/g, "");
            $a = $a.split(' ').join('');
            $b = $b.replace(/[,]/g, "");
            $b = $b.split(' ').join('');
            
            if($a > 0 && $a > allow_max){
                $result = allow_max * $b;
                document.getElementById('qty_current'+val2+'_'+val).value = parseFloat(allow_max).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('qty_amount'+val2+'_'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('qty_current'+val2+'_'+val).style.color = "#ff0000";
                document.getElementById('qty_amount'+val2+'_'+val).style.color = "#ff0000";
            }else if($a < 0 && $a < allow_min){
                var $result = allow_min * $b;
                document.getElementById('qty_current'+val2+'_'+val).value = parseFloat(allow_min).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('qty_amount'+val2+'_'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('qty_current'+val2+'_'+val).style.color = "#ff0000";
                document.getElementById('qty_amount'+val2+'_'+val).style.color = "#ff0000";
            }else{
                var $result = $a * $b;
                document.getElementById('qty_amount'+val2+'_'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('qty_current'+val2+'_'+val).style.color = "";
                document.getElementById('qty_amount'+val2+'_'+val).style.color = "";
            }

            $('.addqty'+val2).trigger('change');
            document.getElementById('qty_amount'+val2+'_'+val).setAttribute('name', 'adjust['+val2+'_'+val+'][amount]');
            document.getElementById('qty_current'+val2+'_'+val).setAttribute('name','adjust['+val2+'_'+val+'][quantity]');
            document.getElementById('qty_edit'+val2+'_'+val).setAttribute('name','adjust['+val2+'_'+val+'][edit]');
            document.getElementById('qty_id'+val2+'_'+val).setAttribute('name','adjust['+val2+'_'+val+'][id]');
            document.getElementById('status'+val2+'_'+val).setAttribute('name', 'adjust['+val2+'_'+val+'][status]');
        }
        
        
        function tukar(val,val2){//onchange bq for update quantity once click checkbox
            var checkBox = document.getElementById("qty_id"+val2+'_'+val);
            var lastipc  = document.getElementById("last_ipc").value;
            
            if($("#qty_id"+val2+'_'+val).is(':disabled')){
                    $("#qty_current"+val2+'_'+val).attr('readonly',true);
            }
            if (checkBox.checked == true){
                $("#qty_current"+val2+'_'+val).attr('readonly',false);
                if((val2 == '0' || val2 == '' || val2 == null) && '{{ $ipc->last_ipc }}' == 'n'){
                    $("#qty_current"+val2+'_'+val).val(0);
                }
                $('.addqty'+val2).trigger('change');
                calc_bq(val,val2);
            } else {
                if(lastipc != null && lastipc == 'y'){
                    $("#qty_id"+val2+'_'+val).prop('checked',true);
                    $("#qty_id"+val2+'_'+val).trigger('change');
                }else{
                    $("#qty_current"+val2+'_'+val).attr('readonly',true);
                    $("#qty_current"+val2+'_'+val).val(null);
                    $("#qty_amount"+val2+'_'+val).val(null);
                    $('.addqty'+val2).trigger('change');
                    calc_bq(val,val2);
                }
            }
        }
        
        function tukar_vo(valu){//onchange bq vo for update quantity once click checkbox
             
            var checkBox = document.getElementById("vo_check"+valu);
            var lastipc  = document.getElementById("last_ipc").value;
            
            if (checkBox.checked == true){
                $("#vo_qty_edit"+valu).attr('readonly',false);
                if($("#vo_qty_edit"+valu).val() == '' || $("#vo_qty_edit"+valu).val() == null){
                    $("#vo_qty_edit"+valu).val(0);
                }
                $('.voqty').trigger('change');
//                calc_vo(valu);
            } else {
                if(lastipc != null && lastipc == 'y'){
                    $("#vo_check"+valu).prop('checked',true);
                    $("#vo_check"+valu).trigger('change');
                }else{
                   
                    $("#vo_qty_edit"+valu).attr('readonly',true);
                    $("#vo_qty_edit"+valu).val(null);
                    $("#vo_amt_edit"+valu).val(null);
                    $('.voqty').trigger('change');
    //                calc_vo(valu);
                }
            }
        }
        
        
        function calc(val){//for material on site
            var $a = document.getElementById('quantity_'+val).value;
            var $b = document.getElementById('rate_'+val).value;
           
            if($a==''){
                $a='0';
            }
            else if($b==''){
                $b='0';
            }

            $a = $a.replace(/[,]/g, "");
            $a = $a.split(' ').join('');
            $b = $b.replace(/[,]/g, "");
            $b = $b.split(' ').join('');

            var $result = $a * $b;
            document.getElementById('amount_'+val).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");

            var ttl_mat   = 0;
            var totl_mat  = 0;
            var total_mat = 0;
            $('.matamt').each(function(){
                if($(this).val() != '')
                {
                    ttl_mat = $(this).val();
                    ttl_mat = ttl_mat.replace(/[,]/g, "");
                    ttl_mat = ttl_mat.split(' ').join('');
                    
                    totl_mat = parseFloat(ttl_mat);
                    total_mat = total_mat + totl_mat;
                }
            
            });


            var tbl_amt = 0.00;
            $('#table_material').find('tr').each(function (rowIndex, r) {
                var cols = [];
                $(this).find('td:nth-child(5)').each(function (colIndex, c) {
                    if(c.textContent != ''){
                        var c_amt = c.textContent;
                        c_amt = c_amt.replace(/[,]/g, "");
                        c_amt = c_amt.split(' ').join('');
                        tbl_amt = tbl_amt + parseFloat(c_amt);
                    }
                    
                });
            });

            $total_all = 0.9 * (tbl_amt + total_mat);
            $('#material_on_site_amount').val(parseFloat($total_all).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
            
            calc_demand_amount();
        }

        function calc_vo(vl){//for vo auto count amount based on rate
            var allow_max  = 0;
            var allow_min  = 0;
            var $result    = 0;
            var min_qty    = document.getElementById('vo_paid'+vl).value ;
            var max_qty    = document.getElementById('vo_baki'+vl).value ;
            var edit_qty   = document.getElementById('vo_limit'+vl).value ;
            
                max_qty    = max_qty.replace(/[,]/g, "");
                max_qty    = max_qty.split(' ').join('');
                min_qty    = min_qty.replace(/[,]/g, "");
                min_qty    = min_qty.split(' ').join('');
                edit_qty   = edit_qty.replace(/[,]/g, "");
                edit_qty   = edit_qty.split(' ').join('');
                
                if(edit_qty != "0.00"){//edit editing ipc
                    
                    if(max_qty != "0.00" && max_qty < 0 && min_qty != "0.00" && min_qty < 0){
                        
                        min_qty = -(min_qty);
                        edit_qty = -(edit_qty);
                        max_qty = -(max_qty);
                        
                        allow_max  = parseFloat(min_qty) - parseFloat(edit_qty);
                        allow_min  = -(parseFloat(max_qty) + parseFloat(edit_qty));
                        
                    }else if(max_qty != "0.00" && max_qty > 0 && min_qty != "0.00" && min_qty > 0){
                        
                        allow_max  = parseFloat(max_qty) + parseFloat(edit_qty);
                        allow_min  = -(parseFloat(min_qty) - parseFloat(edit_qty));
                    }else{
                        allow_max  = parseFloat(edit_qty);
                        allow_min  = parseFloat(min_qty);
                    }
                }else{
                    if(max_qty < 0 || min_qty < 0){
                        allow_max  = -(parseFloat(min_qty));
                        allow_min  = parseFloat(max_qty);
                    }else{
                        allow_max  = parseFloat(max_qty);
                        allow_min  = -(parseFloat(min_qty));
                    } 
                }
                
            var $a = document.getElementById('vo_qty_edit'+vl).value;
            var $b = document.getElementById('vo_rate'+vl).value;
            var $result = 0;
            $a = $a.replace(/[,]/g, "");
            $a = $a.split(' ').join('');
            $b = $b.replace(/[,]/g, "");
            $b = $b.split(' ').join('');
			
            if($a > 0 && $a > allow_max){
                
                $result = allow_max * $b;
                document.getElementById('vo_qty_edit'+vl).value = parseFloat(allow_max).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('vo_qty_edit'+vl).style.color = "#ff0000";
                document.getElementById('vo_amt_edit'+vl).style.color = "#ff0000";
            }else if($a < 0 && $a < allow_min){
                
                $result = allow_min * $b;
                document.getElementById('vo_qty_edit'+vl).value = parseFloat(allow_min).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                document.getElementById('vo_qty_edit'+vl).style.color = "#ff0000";
                document.getElementById('vo_amt_edit'+vl).style.color = "#ff0000";
            }else{
                
                $result = $a * $b;
                document.getElementById('vo_qty_edit'+vl).style.color = "";
                document.getElementById('vo_amt_edit'+vl).style.color = "";
            }
            document.getElementById('vo_amt_edit'+vl).value = parseFloat($result).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
            calc_demand_amount();
        }

        function calc_demand_amount(){//for clean total amount

            var inbois = '{{ money()->toCommon( $ipc->ipcInvoice->where('status_contractor',1)->pluck('demand_amount')->sum() ?? "0",2 ) }}';
            inbois = inbois.replace(/[,]/g, "");
            inbois = inbois.split(' ').join('');
            
            var material_on_site = $('#material_on_site_amount').val();
            material_on_site = material_on_site.replace(/[,]/g, "");
            material_on_site = material_on_site.split(' ').join('');
            
            var material_tahanan = $('#material_tahanan_amount').val();
            material_tahanan = material_tahanan.replace(/[,]/g, "");
            material_tahanan = material_tahanan.split(' ').join('');

            //calculate new wjp
            var total_inv = '{{ ($ipc->ipcInvoice->count() > 0)? money()->toCommon($ipc->ipcInvoice->where('status_contractor',1)->pluck("demand_amount")->sum(), 2):0 }}'
                total_inv = total_inv.replace(/[,]/g, "");
                total_inv = total_inv.split(' ').join('');
            
            if('{{ $get_wjp}}' > 0){
                var new_wjp = 0.1 * (parseFloat(total_inv) + parseFloat(material_on_site));
                $('#wjp_amount').val(parseFloat(new_wjp).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
            }
            
            var wang_tahanan = $('#wjp_amount').val();
            wang_tahanan = wang_tahanan.replace(/[,]/g, "");
            wang_tahanan = wang_tahanan.split(' ').join('');
            
            var wang_tahanan_release = $('#wjp_lepas_amount').val();
            wang_tahanan_release = wang_tahanan_release.replace(/[,]/g, "");
            wang_tahanan_release = wang_tahanan_release.split(' ').join('');
            
            var advance = $('#advance_amount').val();
            advance = advance.replace(/[,]/g, "");
            advance = advance.split(' ').join('');
            
            var advance_tahanan = $('#give_advance_amount').val();
            advance_tahanan = advance_tahanan.replace(/[,]/g, "");
            advance_tahanan = advance_tahanan.split(' ').join('');

            var amaun_kredit = $('#credit').val();
            amaun_kredit = amaun_kredit.replace(/[,]/g, "");
            amaun_kredit = amaun_kredit.split(' ').join('');

            var vo = $('#working_change_amount').val();
            vo = vo.replace(/[,]/g, "");
            vo = vo.split(' ').join('');
            
            var lad = $('#compensation_amount').val();
            if(lad == undefined){
                lad = 0;
            }else{
                lad = lad.replace(/[,]/g, "");
                lad = lad.split(' ').join('');
            }
            var new_dem = parseFloat(inbois) + parseFloat(material_on_site) - parseFloat(material_tahanan) - parseFloat(wang_tahanan) + parseFloat(wang_tahanan_release) + parseFloat(advance) - parseFloat(advance_tahanan) - parseFloat(amaun_kredit) + parseFloat(vo) - parseFloat(lad);
            $('#demand_amount').val(parseFloat(new_dem).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
        }

        $("#table_material").on('click','.deletematerial',function(){
            var mat_amt = $('#material_on_site_amount').val();
            mat_amt = mat_amt.replace(/[,]/g, "");
            mat_amt = mat_amt.split(' ').join('');

            var value_to_remove = $(this).closest("tr").find('td:nth-child(5)').html();
            if(value_to_remove.indexOf('<div') !== -1  ){
                value_to_remove = $(this).closest("tr").find('td:nth-child(5) input').val();
            }

            value_to_remove = value_to_remove.replace(/[,]/g, "");
            value_to_remove = value_to_remove.split(' ').join('');
            value_to_remove = value_to_remove * 0.9;

            var new_mat_amt = mat_amt - value_to_remove;
            $('#material_on_site_amount').val(parseFloat(new_mat_amt).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));

            $(this).parents('tr').detach();

            calc_demand_amount();
        });
        //set name once modal click
        function set_name(v){
            //set name attr for invoice
            $("#invoice_no"+v).attr('name','inv['+v+'][invoice_no]');
            $("#invoice"+v).attr('name','inv['+v+'][invoice]');
            $("#sst_amount"+v).attr('name','inv['+v+'][sst_amount]');
            $("#invoice_at"+v).attr('name','inv['+v+'][invoice_at]');
            $("#invoice_receive_at"+v).attr('name','inv['+v+'][invoice_receive_at]');
            $("#ref_text"+v).attr('name','inv['+v+'][ref_text]');
            $("#bulan_status"+v).attr('name','inv['+v+'][bulan_status]');
            $("#uploadFile"+v).attr('name','document'+v+'[]');
            $("#invoice_edit"+v).attr('name','inv['+v+'][edit]');
            $("#invoice_id"+v).attr('name','inv['+v+'][id]');
            $("#demand_amount"+v).attr('name','inv['+v+'][demand_amount]');
            $("#running"+v).attr('name','inv['+v+'][running]');
            //set name attr for invoice
        }
        function set_name_fund(v,v2){
            $("#inv_fund_id"+v+'_'+v2).attr('name','inv['+v+'][fund_id]');
            $("#inv_fund_id"+v+'_'+v2).attr('checked',true);
        }
        function set_name_comp(v,v2,v3){
            
            $("#inv_company_id"+v+'_'+v2+'_'+v3).attr('name','inv['+v+'][company_id]');
            $("#inv_company_id"+v+'_'+v2+'_'+v3).attr('checked',true);
            $( "input" ).removeAttr( "name:inv["+v+"][status_contractor]" );
            $("#inv_status_contractor").attr('name','inv['+v+'][status_contractor]');
            $("#inv_status_contractor").val(v3);
            $("#subkon_id"+v).val(v2);
            $("#subkon_id"+v).attr('name','subkon_id'+v);
        }
        function set_n(val){
            $("#set_n").val(val);
        }
        //set name once modal click
        //unset name once close
        function unset_name(v){
            //set name attr for invoice
            $("#invoice_no"+v).removeAttr('name');
            $("#invoice"+v).removeAttr('name');
            $("#sst_amount"+v).removeAttr('name');
            $("#invoice_at"+v).removeAttr('name');
            $("#invoice_receive_at"+v).removeAttr('name');
            $("#ref_text"+v).removeAttr('name');
            $("#bulan_status"+v).removeAttr('name');
            $("#uploadFile"+v).removeAttr('name');
            $("#invoice_edit"+v).removeAttr('name');
            $("#invoice_id"+v).removeAttr('name');
            $("#demand_amount"+v).removeAttr('name');
            $("#running"+v).removeAttr('name');
            //set name attr for invoice
        }
        function unset_name_fund(v,v2){
            $("#inv_fund_id"+v+'_'+v2).removeAttr('name');
            $("#inv_fund_id"+v+'_'+v2).removeAttr('required');
        }
        function unset_name_comp(v,v2,v3){
            $("#inv_company_id"+v+'_'+v2+'_'+v3).removeAttr('name');
            $("#inv_company_id"+v+'_'+v2+'_'+v3).removeAttr('required');
            $("#inv_status_contractor").removeAttr('name');
            $("#subkon_id"+v).removeAttr('name');
            $("#subkon_id"+v).val('');
        }
        //unset name once close
        
        jQuery(document).ready(function($) {
//            $('.money').mask('000,000,000.00', {reverse: true});
            $(document).on('click', '.print-action-btn', function(event) {
                event.preventDefault();

                var id = '{{ $ipc->sst->hashslug }}';
                swal({
                  title: 'PERINGATAN',
                  text: 'Sila cetak menggunakan kertas berwarna BIRU',
                  type: 'info'
                }).then((result) => {
                    window.open(route('sofaRpt', {hashslug:id}), '_blank');
                });
            });

            @if((!empty($kerja) && $kerja != null &&
                    (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cmgd) && !empty($ipc->sst->cmgd->signature_date) && $ipc->sst->cmgd->signature_date != null)
                    && (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->signature_date) && $ipc->sst->cpc->signature_date != null)
                ) || (!empty($bekalan) && $bekalan != null && 
                    (!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->signature_date) && $ipc->sst->cpc->signature_date != null)
                ))
                $('.showlast').show();
            @endif
            
            $('#last_ipc').on('change', function(){
                if(this.checked == true){
                    $('#last_ipc').val('y');
                }else{
                    $('#last_ipc').val('n');
                }
            });
            
            //datatable forms.ipc.invoice
            @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                @foreach($ipc->ipcInvoice as $inv)
                $("#showBq{{ $inv->id }}").DataTable();
                @endforeach
                @if(($ipc->ipc_no == '1' && !empty($ipc->doc_perkeso) && $ipc->doc_perkeso->count() > 0) || $ipc->ipc_no != '1')
                    $(".hantar-action-btn").show();
                @else
                    $(".hantar-action-btn").hide();
                @endif
            @else
                $(".hantar-action-btn").hide();
            @endif
            //end datatable forms.ipc.invoice
            //attachment on first page
            @if(!empty($ipc) && !empty($ipc->doc_sijil))
                $('#subFile{{ $ipc->doc_sijil->id }}').val('{{ $ipc->doc_sijil->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->doc_summary))
                $('#subFile{{ $ipc->doc_summary->id }}').val('{{ $ipc->doc_summary->document_name }}')
            @endif
            
            @if(!empty($ipc) && !empty($ipc->doc_status))
                $('#subFile{{ $ipc->doc_status->id }}').val('{{ $ipc->doc_status->document_name }}')
            @endif
            
            @if(!empty($ipc) && !empty($ipc->doc_detail))
                $('#subFile{{ $ipc->doc_detail->id }}').val('{{ $ipc->doc_detail->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->ipcInvoice) && !empty($ipc->ipcInvoice->pluck('document')))
                $('#subFile{{ $ipc->ipcInvoice->pluck('document')->pluck('id')->first() }}').val('{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->document))
                $('#subFile{{ $ipc->sst->document->id }}').val('{{ $ipc->sst->document->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_publicLiability))
                @foreach($ipc->sst->insurance->documents_publicLiability as $public)
                    $('#subFile{{ $public->id }}').val('{{ $public->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->documents))
                @foreach($ipc->sst->cpc->documents as $cpc)
                    $('#subFile{{ $cpc->id }}').val('{{ $cpc->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->cnc) && !empty($ipc->sst->cnc->documents))
                @foreach($ipc->sst->cnc->documents as $cnc)
                    $('#subFile{{ $cnc->id }}').val('{{ $cnc->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->cmgd) && !empty($ipc->sst->cmgd->documents))
                @foreach($ipc->sst->cmgd->documents as $cmgd)
                    $('#subFile{{ $cmgd->id }}').val('{{ $cmgd->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_work))
                @foreach($ipc->sst->insurance->documents_work as $work)
                    $('#subFile{{ $work->id }}').val('{{ $work->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_compensation))
                @foreach($ipc->sst->insurance->documents_compensation as $compensation)
                    $('#subFile{{ $compensation->id }}').val('{{ $compensation->document_name }}')
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->sst->bon) && !empty($ipc->sst->bon->documents))
                @foreach($ipc->sst->bon->documents as $bon)
                    $('#subFile{{ $bon->id }}').val('{{ $bon->document_name }}')
                @endforeach
            @endif

            @if(!empty($sst))
                @foreach($sst as $ssts)
                    @if(!empty($ssts->ipc) && !empty($ssts->ipc->pluck('doc_sijil')))
                        @foreach($ssts->ipc->pluck('doc_sijil') as $ipc_all)
                            @if(!empty($ipc_all))
                                $('#subFile{{ $ipc_all->id }}').val('{{ $ipc_all->document_name }}')
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif

            @if(!empty($ipc) && !empty($ipc->doc_perkeso))
                $('#subFile{{ $ipc->doc_perkeso->id }}').val('{{ $ipc->doc_perkeso->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->company) && !empty($ipc->sst->company->gst))
                $('#subFile{{ $ipc->sst->company->gst->id }}').val('{{ $ipc->sst->company->gst->document_name }}')
            @endif

            @if(!empty($ipc) && !empty($ipc->doc_levi))
                $('#subFile{{ $ipc->doc_levi->id }}').val('{{ $ipc->doc_levi->document_name }}')
            @endif
            
            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_pekeso').change(function(){
                $('#subFile_pekeso').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_cert').change(function(){
                $('#subFile_cert').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_summary').change(function(){
                $('#subFile_summary').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_status').change(function(){
                $('#subFile_status').val($(this).val().split('\\').pop());
            });
            
            $('#uploadFile_detail').change(function(){
                $('#subFile_detail').val($(this).val().split('\\').pop());
            });

            @foreach($levi_doc as $db)
                var url = "/uploads/{{ $db->document_name }}";

                $('.newwindow{{ $db->document_id }}').click(function () {

                    var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindow = window.open(url, 'website', params);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($pekeso_doc as $db_pekeso)
                var urlpekeso = "/uploads/{{ $db_pekeso->document_name }}";

                $('.newwindowpekeso{{ $db_pekeso->document_id }}').click(function () {

                    var paramspekeso = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowpekeso = window.open(urlpekeso, 'website', paramspekeso);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($cert_doc as $db_cert)
                var urlcert = "/uploads/{{ $db_cert->document_name }}";

                $('.newwindowcert{{ $db_cert->document_id }}').click(function () {

                    var paramscert = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowcert = window.open(urlcert, 'website', paramscert);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($summary_doc as $db_sum)
                var urlsum = "/uploads/{{ $db_sum->document_name }}";

                $('.newwindowsum{{ $db_sum->document_id }}').click(function () {

                    var paramssum = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowsum = window.open(urlsum, 'website', paramssum);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($status_doc as $db_status)
                var urlstatus = "/uploads/{{ $db_status->document_name }}";

                $('.newwindowstatus{{ $db_status->document_id }}').click(function () {

                    var paramsstatus = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowstatus = window.open(urlstatus, 'website', paramsstatus);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach
            @foreach($detail_doc as $db_detail)
                var urlstatus = "/uploads/{{ $db_detail->document_name }}";

                $('.newwindowstatus{{ $db_detail->document_id }}').click(function () {

                    var paramsstatus = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowstatus = window.open(urlstatus, 'website', paramsstatus);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach
            //end attachment on first page

            //tab show for add new invoice
            $('#invois_info0').show();
            $('#quantity0').hide();
            $('#subkontrak0').hide();
            $('#finance0').hide();
            
            $(".addamt0").attr('readonly',true);
            //end tab show for add new invoice
            
            @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                $('#ipc_no').val('{{ $ipc->penalty }}').attr('readonly',true);
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                $('#ipc_no').val('{{ $ipc->ipc_no }}').attr('readonly',true);
            @elseif($ipc->penalty != null && $ipc->last_ipc == 'y')
                $('#ipc_no').val('{{ $ipc->penalty }} (AKHIR)').attr('readonly',true);
            @else
                $('#ipc_no').val('{{ $ipc->ipc_no }} (AKHIR)').attr('readonly',true);
            @endif
            
            $('#short_text').val('{{ $ipc->short_text }}');
            $('#long_text').val('{{ $ipc->long_text }}');
            @if($ipc->last_ipc == 'y')
                $('#last_ipc').attr('checked',true);
            @endif
            $('#material_on_site_amount').val('{{ money()->toCommon($ipc->material_on_site_amount ?? "0", 2) }}');
            $('#material_tahanan_amount').val('{{ money()->toCommon($material_tahan ?? "0", 2) }}');
            $('#credit_no').val('{{ $ipc->credit_no ?? "" }}');
            $('#credit').val('{{ money()->toCommon($ipc->credit ?? "0", 2) }}');
            $('#advance_amount').val('{{ money()->toCommon($adv ?? "0", 2) }}');
            $("#give_advance_amount").val('{{ money()->toCommon($given_adv ?? "0", 2) }}');
            $("#wjp_amount").val('{{ money()->toCommon($get_wjp ?? "0", 2) }}');
            $("#wjp_lepas_amount").val('{{ money()->toCommon($wjp_lepas ?? "0", 2) }}');
            @if(!empty($denda) && ($denda == 1 || $denda == 3 || $denda == 4))
                $("#compensation_amount").val('{{ money()->toCommon($ipc->compensation_amount ??  "0", 2) }}');
            @else
                $("#compensation_amount").val('{{ money()->toCommon($lad_value ??  "0", 2) }}');
            @endif
            $("#lad_day").val('{{ $lad_day }}');
            $("#lad_rate").val('{{ money()->toCommon($lad_rate) }}');
			@if(!empty($lad_value))
				$("#lad_formula").show();
			@endif
            if($("#working_change_amount").val() == ''){
                $("#working_change_amount").val('{{ money()->toCommon($ipc->working_change_amount ?? "0", 2) }}');
            }
            if($("#new_contract_amount").val() == ''){
                $("#new_contract_amount").val('{{ money()->toCommon($ipc->new_contract_amount ?? "0", 2) }}');
            }

//            $("#working_change_amount").val('{{ money()->toCommon($ipc->working_change_amount ?? "0", 2) }}');
//            $("#new_contract_amount").val('{{ money()->toCommon($ipc->new_contract_amount ?? "0", 2) }}');
            $('#demand_amount').val('{{ money()->toCommon($demand ?? "0", 2) }}').attr('readonly',true);
            $("#demand_show").show();
            
            @if(!empty($ipc->evaluation_at))
                @if($ipc->penalty == null)
                    $("#evaluation_at").val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->evaluation_at)->format('d/m/Y') }}');
                @else   
                    $("#evaluation_at").val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->evaluation_at)->format('d/m/Y') }}').attr('readonly',true);
                @endif
            @endif
            @if(!empty($ipc->credit_at))
                $("#credit_at").val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->credit_at)->format('d/m/Y') }}');
            @endif
            @if(!empty($ipc->credit_receive_at))
                $("#credit_receive_at").val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->credit_receive_at)->format('d/m/Y') }}');
            @endif
            
            //sijil material on site
            @if( $ipc->doc_material )
                    $('#subFileMat').val('{{ $ipc->doc_material->document_name }}');
            @endif
            $('#uploadFileMat').change(function(){
                $('#subFileMat').val($(this).val().split('\\').pop());
            });
            //sijil material on site
            //sijil levi
            @if( $ipc->doc_levi )
                    $('#subFileLevi').val('{{ $ipc->doc_levi->document_name }}');
            @endif
            $('#uploadFileLevi').change(function(){
                $('#subFileLevi').val($(this).val().split('\\').pop());
            });
            //sijil levi
            //sijil perkeso
            @if( $ipc->doc_perkeso )
                    $('#subFilePerkeso').val('{{ $ipc->doc_perkeso->document_name }}');
            @endif
            $('#uploadFilePerkeso').change(function(){
                $('#subFilePerkeso').val($(this).val().split('\\').pop());
            });
            //sijil perkeso
            //sijil ipc sijil
            @if( $ipc->doc_sijil )
                    $('#subFileSijil').val('{{ $ipc->doc_sijil->document_name }}');
            @endif
            $('#uploadFileSijil').change(function(){
                $('#subFileSijil').val($(this).val().split('\\').pop());
            });
            //sijil ipc sijil
            //sijil ipc summary
            @if( $ipc->doc_summary )
                    $('#subFileSummary').val('{{ $ipc->doc_summary->document_name }}');
            @endif
            $('#uploadFileSummary').change(function(){
                $('#subFileSummary').val($(this).val().split('\\').pop());
            });
            //sijil ipc summary
            //sijil ipc status
            @if( $ipc->doc_status )
                    $('#subFileStatus').val('{{ $ipc->doc_status->document_name }}');
            @endif
            $('#uploadFileStatus').change(function(){
                $('#subFileStatus').val($(this).val().split('\\').pop());
            });
            //sijil ipc status
            
            //sijil ipc detail
            @if( $ipc->doc_detail )
                    $('#subFileDetail').val('{{ $ipc->doc_detail->document_name }}');
            @endif
            $('#uploadFileDetail').change(function(){
                $('#subFileDetail').val($(this).val().split('\\').pop());
            });
            //sijil ipc Detail
            
            //sijil invoice baru
            $('#uploadFile0').change(function(){
                $('#subFile0').val($(this).val().split('\\').pop());
            });
            //sijil invoice baru
            //sofa
            @if( $ipc->doc_sofa )
                    $('#subFileSofa').val('{{ $ipc->doc_sofa->document_name }}');
            @endif
            $('#uploadFileSofa').change(function(){
                $('#subFileSofa').val($(this).val().split('\\').pop());
            });
            //sofa
            //statuori
            @if( $ipc->doc_statuori )
                    $('#subFileStatuori').val('{{ $ipc->doc_statuori->document_name }}');
            @endif
            $('#uploadFileStatuori').change(function(){
                $('#subFileStatuori').val($(this).val().split('\\').pop());
            });
            //statuori
            @if(!empty($ipc->material_on_site_amount) || '0' != $ipc->material_on_site_amount)
                $("#material_tambah").show();
            @endif
            @if(!empty($material_tahan) || '0' != $material_tahan)
                $("#material_tolak").show();
            @endif
            @if(!empty($get_wjp) || '0' != $get_wjp)
                $("#wjp_tambah").show();
                $("#wjp_tambah_formula").show();
            @endif
            @if(!empty($wjp_lepas) || '0' != $wjp_lepas)
                $("#wjp_tolak").show();
                $("#wjp_tolak_formula").show();
            @endif
            @if(!empty($adv) || '0' != $adv)
                $("#adv_tambah").show();
            @endif
            @if(!empty($given_adv) || '0' != $given_adv)
                $("#adv_tolak").show();
                $("#adv_tolak_formula").show();
            @endif
            
            $('#invoice_no0').on('change', function(){
                $('#ref_text0').val(this.value);
            });
            
            //auto calculate material 
            $(".addmaterial").on('click',function(){
                var $tr = $("#table_material").find('tr[id^="material"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'material'+num).removeClass('d-none'); 
                  $klon.find('#desc_'+currnum).attr('id','desc_'+num).attr('name','material['+num+'][desc]').val('');
                  $klon.find('#unit_'+currnum).attr('id','unit_'+num).attr('name','material['+num+'][unit]').val('');
                  $klon.find('#quantity_'+currnum).attr('id','quantity_'+num).attr('name','material['+num+'][quantity]').attr('onkeyup','calc('+num+')').val('');
                  $klon.find('#rate_'+currnum).attr('id','rate_'+num).attr('name','material['+num+'][rate]').attr('onkeyup','calc('+num+')').val('');
                  $klon.find('#amount_'+currnum).attr('id','amount_'+num).attr('name','material['+num+'][amount]').attr('onkeyup','calc('+num+')').val('');
                $tr.parents('table').append($klon);
            });
            //end auto calculate
            // auto count total invoice on credit change
            $('#credit').change(function(){
                
                if($(this).val() != '' && $(this).val() != '0.00')
                {
                    var cre = $(this).val();
                    calc_demand_amount();                    
                }
            });
            
            //ipc send
            $('#semakan').change(function () {
                if($(this).val() == 1){
                    $('#semak1').val(1);
                    $('#semak2').val(null);
                    $('#semak3').val(null);
                    $('#approved_by').val('{{ user()->supervisor->id }}');
                }else if($(this).val() == 2){
                    $('#semak1').val(null);
                    $('#semak2').val(1);
                    $('#semak3').val(null);
                    $('#approved_by').val('{{ user()->supervisor->supervisor->id }}');
                }else if($(this).val() == 3){
                    $('#semak1').val(null);
                    $('#semak2').val(null);
                    $('#semak3').val(1);
                    @if(!empty($review_previous->task))
                    $('#approved_by').val('{{ $review_previous->task->id }}');
                    @endif
                }else{
                    $('#semak1').val(null);
                    $('#semak2').val(null);
                    $('#semak3').val(null);
                    $('#approved_by').val(null);
                }
            });
            //ipc send
            
            //tab remain after page reload
            $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });
            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //page remain after page reload

            
            $(document).on('click', '.destroy-inv-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan rekod INV ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('api.contract.ipc.invoice.destroy', id))
                        .then(response => {
                            swal('{!! __('Sijil Bayaran Interim') !!}', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });
            
            $(document).on('click', '.buang_attach', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('api.contract.ipc.sijil.delete_attach', id))
                        .then(response => {
                            swal('Sijil Bayaran Interim', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });
            
            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $ipc->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var form = document.forms[form_id];
                var data = new FormData(form);
                //serialize array for bq
                var n = $('#set_n').val();
                @foreach($ipc->sst->acquisition->bqElements as $bq)
                    @foreach($bq->bqItems as $it)
                        var table_bq = $("#ulasan_bq"+n+"_{{ $it->id }}").DataTable();
                        var x = table_bq.$('input').serializeArray();
                        $.each(x, function(i, field){
                            data.append(field.name,field.value);
                        });
                    @endforeach
                @endforeach
                //end serialize array for bq
                //serialize array for vo 
                @if(!empty(!empty($ipc->sst->ppjhk_active) && $ipc->sst->ppjhk_active->count() > 0))
                    @foreach($ipc->sst->ppjhk_active as $ppjhk)
                        @foreach($ppjhk->elements as $vo_el)
                            @foreach($voItem as $vo_it)
                                var table_vo = $("#vo{{ $vo_it->id }}").DataTable();
                                var y = table_vo.$('input').serializeArray();
                                $.each(y, function(i, field){
                                    data.append(field.name,field.value);
                                });
                            @endforeach
                        @endforeach
                    @endforeach
                @endif
                //end serialize array for vo
                axios.post(route(route_name, id), data).then(response => {
                    swal('{!! __('Sijil Bayaran Interim') !!}', response.data.message, 'success');
                     location.reload();
                });
            });
            
            $(document).on('click', '.hantar-action-btn', function(event) {
                event.preventDefault();
                @foreach($ipc->ipcInvoice as $inv)
                    @if($inv->status_contractor == '2' && $inv->sst->subContract->where('company_id',$inv->company_id)->pluck('amount')->first() < $ipc->ipcInvoice->where('status_contractor','2')->where('company_id',$inv->company_id)->pluck('invoice')->sum())
                        swal('Sijil Interim', 'Bayaran sub kontraktor melebihi jumlah diperuntukan!', 'warning');
                        return false;
                    @endif
                @endforeach
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    // semak1 : $('#semak1').val(),
                    // semak2 : $('#semak2').val(),
                    // semak3 : $('#semak3').val(),
                    ipc_id : $('#ipc_id').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('.hantar-action-btn').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });
            
            
            // auto count total invoice on credit change
            $('input[type=radio][id=inbois_type0]').change(function () {
                if(this.value == '1'){
                    $('#invois_info0').show();
                    $('#quantity0').hide();
                    $('#subkontrak0').hide();
                    $('#finance0').hide();
                }
                else if (this.value == '2'){
                    $('#invois_info0').hide();
                    $('#quantity0').show();
                    $('#subkontrak0').hide();
                    $('#finance0').hide();
                }
                else if (this.value == '3'){
                    $('#invois_info0').hide();
                    $('#quantity0').hide();
                    $('#subkontrak0').show();
                    $('#finance0').hide();
                }
                else{
                    $('#invois_info0').hide();
                    $('#quantity0').hide();
                    $('#subkontrak0').hide();
                    $('#finance0').show();
                }
            });
        });
   </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{!! $ipc->sst->acquisition->title !!}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $ipc->sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $ipc->sst->company->company_name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if(!empty($new_contract) && $new_contract != 0)
            <span class="font-weight-bold">Nilai Kontrak Baru: </span>RM {{ money()->toCommon($new_contract ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $ipc->sst->period." ".$ipc->sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ipc->sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if(!empty($new_eot_date))
                <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{ $new_eot_date }}&nbsp;&nbsp;&nbsp;
            @endif
            
            <br>
            <span class="font-weight-bold"> No Sijil Interim: </span>
            @if($ipc->penalty != null && $ipc->last_ipc == 'n')
                {{ $ipc->penalty }}
            @elseif($ipc->penalty == null && $ipc->last_ipc == 'n')
                {{ $ipc->ipc_no }}
            @else
                {{ $ipc->ipc_no }} (AKHIR)
            @endif
        @endslot
    @endcomponent
    <div class="row" ref="msgContainer" id="tab_button">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                <li class="list-group-item">
                    <a class="list-group-item-action active" data-toggle="tab" href="#interim-sijil" role="tab" aria-controls="interim-sijil" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Sijil Interim') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-invois" role="tab" aria-controls="interim-invois" aria-selected="false">
                        @icon('fe fe-list')&nbsp;{{ __('Inbois') }}
                    </a>
                </li>
                @if(!empty($ipc->sst) && !empty($ipc->sst->acquisition) && !empty($ipc->sst->acquisition->approval) && !empty($ipc->sst->acquisition->approval->type) && ($ipc->sst->acquisition->approval->type->id == 2 || $ipc->sst->acquisition->approval->type->id == 4) && !empty($get_inv_main))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-material" role="tab" aria-controls="interim-material" aria-selected="false">
                        @icon('fe fe-folder')&nbsp;{{ __('Material on Site') }}
                    </a>
                </li>
                @endif
                @if(!empty($ipc->sst) && (!empty($ipc->sst->deposit)) && $ipc->sst->deposit->qualified_amount != '0' && (!empty($adv) || '0' != $adv || !empty($given_adv) || '0' != $given_adv))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-advance" role="tab" aria-controls="interim-advance" aria-selected="false">
                        @icon('fe fe-map-pin')&nbsp;{{ __('Wang Pendahuluan') }}
                    </a>
                </li>
                @endif
                @if($ipc->wjp_amount != '0' || $ipc->wjp_lepas_amount != '0' || $wjp_lepas != '0' || $get_wjp != '0')
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-tahanan" role="tab" aria-controls="interim-tahanan" aria-selected="false">
                        @icon('fe fe-corner-up-right')&nbsp;{{ __('Wang Tahanan') }}
                    </a>
                </li>
                @endif 
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-lost" role="tab" aria-controls="interim-lost" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Ganti Rugi') }}
                    </a>
                </li>
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-vo" role="tab" aria-controls="interim-vo" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Perubahan Kerja') }}
                    </a>
                </li>
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#interim-credit" role="tab" aria-controls="interim-credit" aria-selected="false">
                        @icon('fe fe-book-open')&nbsp;{{ __('Nota Kredit') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#ipc-checklist" role="tab" aria-controls="ipc-checklist" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Senarai Semakan') }}
                    </a>
                </li>
                {{-- @if($ipc->last_ipc == 'n')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#interim-send" role="tab" aria-controls="interim-sijil" aria-selected="false">
                            @icon('fe fe-edit text-primary')&nbsp;{{ __('Hantar Sijil') }}
                        </a>
                    </li>
                @else
                    @if(!empty($ipc->doc_sofa) && !empty($ipc->doc_statuori))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#interim-send" role="tab" aria-controls="interim-sijil" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Hantar Sijil') }}
                            </a>
                        </li>
                    @endif
                @endif --}}
                @if(!empty($review))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#interim-reviews" role="tab" aria-controls="interim-reviews" aria-selected="false">
                            @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan') }}
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        @component('components.card', ['card_classes' => 'col-10'])
            @slot('card_body')
            <form id="ipc-form" files = "true" enctype="multipart/form-data" method="post">
            @csrf
            @method('PUT')
            <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $ipc->sst->acquisition_id }}" hidden>
            <input type="text" id="sst_id" name="sst_id" value="{{ $ipc->sst_id }}" hidden>
            <input type="text" id="ipc_id" name="ipc_id" value="{{ $ipc->id }}" hidden>
                <div class="tab-content" id="interim-tab-content">
                    <div class="tab-pane fade show active" id="interim-sijil" role="tabpanel" aria-labelledby="interim-sijil-tab">
                        <div class="btn-group float-right">
                            <a href="{{ route('ipc_cert',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Sijil Bayaran ') }}
                            </a>
                            <a href="{{ route('ipc_summary',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Ringkasan Bayaran ') }}
                            </a>
                            <a href="{{ route('ipc_status',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Status Bayaran ') }}
                            </a>
                            <a href="{{ route('ipc_butiran',['hashslug' => $ipc->hashslug]) }}" target="_blank"
                                    class="btn btn-success border-success">
                                    @icon('fe fe-printer'){{ __(' Butiran Bayaran ') }}
                            </a>
                            @if(!empty($ipc) && $ipc->last_ipc == 'y')
                            <a target="_blank" href="#" class="btn btn-success print-action-btn">
                                @icon('fe fe-printer') {{ __('Sijil Muktamad') }}
                            </a>
                            @endif
                        </div><br><br>

                        @include('contract.post.ipc.partials.forms.ipc.sijil')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade  " id="interim-invois" role="tabpanel" aria-labelledby="interim-invois-tab">
                        <input type="text" id="set_n" hidden>
                        @include('contract.post.ipc.partials.forms.ipc.invoice')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-material" role="tabpanel" aria-labelledby="interim-material-tab">
                        @include('contract.post.ipc.partials.forms.ipc.material')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-advance" role="tabpanel" aria-labelledby="interim-advance-tab">
                        @include('contract.post.ipc.partials.forms.ipc.advance')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-tahanan" role="tabpanel" aria-labelledby="interim-tahanan-tab">
                        @include('contract.post.ipc.partials.forms.ipc.wjp')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-lost" role="tabpanel" aria-labelledby="interim-lost-tab">
                        @include('contract.post.ipc.partials.forms.ipc.lad')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-vo" role="tabpanel" aria-labelledby="interim-vo-tab">
                        @include('contract.post.ipc.partials.forms.ipc.vo')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    <div class="tab-pane fade " id="interim-credit" role="tabpanel" aria-labelledby="interim-credit-tab">
                        @include('contract.post.ipc.partials.forms.ipc.credit')
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'form'          => 'ipc-form',
                        ])
                    </div>
                    
                    <div class="tab-pane fade " id="ipc-checklist" role="tabpanel" aria-labelledby="ipc-checklist-tab">
                        @include('contract.post.ipc.partials.show.checklist')
                        {{-- start semakan --}}
                        
                        <input id='id' name ='id' hidden/>
                        @if(!empty($ipc) && !empty($ipc->sst))
                        <input id = "acquisition_id" name = "acquisition_id" value = "{{ $ipc->sst->acquisition_id }}" hidden/>
                        @endif
                        @if(!empty($review))
                        <input type="hidden" name="task_by" value="{{ $review->task_by }}" hidden>
                        <input type="hidden" name="law_task_by" value="{{ $review->law_task_by }}" hidden>
                        <input type="hidden" name="created_by" value="{{ $review->created_by }}" hidden>
                        @endif
                        @if(!empty($review_previous))
                            <input type="text" name="created_by" value="{{ $review_previous->created_by }}" hidden>
                            <input type="text" name="task_by" value="{{ $review_previous->task_by }}" hidden>
                            <input type="text" name="law_task_by" value="{{ $review_previous->law_task_by }}" hidden>
                        @endif
                        <input id="sentStatus" name="sentStatus" value="1" hidden/>
                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>
                        <input type="text" id="progress" name="progress" value="1" hidden>
                        <input type="text" id="created_by" name="created_by" value="{{user()->id }}" hidden>
                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                        <input type="text" id="type" name="type" value="IPC" hidden>
                        <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>
                        <input id="sentStatus" name="sentStatus" value="1" type="hidden"/>
                        {{-- end semakan --}}
                        <br>
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'btn_send'      => 'acquisition.review.store',
                            'form'          => 'ipc-form',
                        ])
                    </div>

                    @if(!empty($review))
                    <div class="tab-pane fade show stop" id="interim-reviews" role="tabpanel" aria-labelledby="ipc-details-tab">
                        
                        @if(!empty($review))

                            @if(user()->current_role_login == 'penyedia')
                                @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnkl" aria-expanded="false" aria-controls="cnkl">
                                                    Ulasan Semakan Oleh {{ $cetaknotisklog->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnkl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s9log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s9log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s9log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubl" aria-expanded="false" aria-controls="cnbpubl">
                                                    Ulasan Semakan Oleh {{ $cetaknotisbpubs->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnbpubl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                                {{-- pegawai pelaksana --}}
                                @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3l" aria-expanded="false" aria-controls="s3l">
                                                    Ulasan Semakan Oleh {{ $semakan3log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s3l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2l" aria-expanded="false" aria-controls="s2l">
                                                    Ulasan Semakan Oleh {{ $semakan2log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s2l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                {{-- pegawai pelaksana --}}
                                @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3l" aria-expanded="false" aria-controls="s3l">
                                                    Ulasan Semakan Oleh {{ $semakan3log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s3l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl2 as $s2log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2l" aria-expanded="false" aria-controls="s2l">
                                                    Ulasan Semakan Oleh {{ $semakan2log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s2l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl1 as $s1log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s1log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- pegawai kewangan --}}
                                @if(!empty($cetaknotisklog) && !empty($cetaknotisklog->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnkl" aria-expanded="false" aria-controls="cnkl">
                                                    Ulasan Semakan Oleh {{ $cetaknotisklog->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnkl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl7 as $s9log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s9log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s9log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s9log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan8log) && !empty($semakan8log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s8l" aria-expanded="false" aria-controls="s8l">
                                                    Ulasan Semakan Oleh {{ $semakan8log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s8l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl6 as $s8log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s8log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s8log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s8log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s8log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                {{-- pegawai bpub --}}
                                @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubl" aria-expanded="false" aria-controls="cnbpubl">
                                                    Ulasan Semakan Oleh {{ $cetaknotisbpubs->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="cnbpubl" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl5 as $s5log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s5log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5l" aria-expanded="false" aria-controls="s5l">
                                                    Ulasan Semakan Oleh {{ $semakan5log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s5l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl4 as $s4log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s4log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                    <div class="mb-0 card card-primary">
                                        <div class="card-header" role="tab" id="headingOne2">
                                            <h4 class="card-title">
                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4l" aria-expanded="false" aria-controls="s4l">
                                                    Ulasan Semakan Oleh {{ $semakan4log->request->name }}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="s4l" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                            <div class="card-body">
                                                @foreach($sl3 as $s3log)
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Terima</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Tarikh Hantar</div>
                                                            <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="text-muted">Status</div>
                                                            <p>{!! $s3log->reviews_status !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="text-muted">Ulasan</div>
                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif

                        @endif
                    </div>
                    @endif

                    {{-- <div class="tab-pane fade" id="interim-send" role="tabpanel" aria-labelledby="interim-send-tab">
                        
                        <input id='id' name ='id' hidden/>
                        <input id = "acquisition_id" name = "acquisition_id" value = "{{ $ipc->sst->acquisition_id }}" hidden/>
                        @if(!empty($review))
                        <input type="hidden" name="task_by" value="{{ $review->task_by }}" hidden>
                        <input type="hidden" name="law_task_by" value="{{ $review->law_task_by }}" hidden>
                        <input type="hidden" name="created_by" value="{{ $review->created_by }}" hidden>
                        @endif
                        @if(!empty($review_previous))
                            <input type="text" name="created_by" value="{{ $review_previous->created_by }}" hidden>
                            <input type="text" name="task_by" value="{{ $review_previous->task_by }}" hidden>
                            <input type="text" name="law_task_by" value="{{ $review_previous->law_task_by }}" hidden>
                        @endif
                        <input id="sentStatus" name="sentStatus" value="1" hidden/>
                        <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>
                        <input type="text" id="progress" name="progress" value="1" hidden>
                        <input type="text" id="created_by" name="created_by" value="{{user()->id }}" hidden>
                        <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                        <input type="text" id="type" name="type" value="IPC" hidden>
                        <input id="approved_by" name="approved_by" hidden/>
                        
                        <label>Hantar Kepada : </label>
                        <select id="semakan" name="semakan" class="form-control w-100">
                            <option value="">{{ __('Sila Pilih') }}</option>
                            <option value="1">{{user()->supervisor->name }}</option>
                            <option value="2">{{user()->supervisor->supervisor->name }}</option>
                            @if(!empty($review_previous->task))
                                <option value="3">{{ $review_previous->task->name }}</option>
                            @endif
                        </select>
                        <input id="semak1" name="semak1" type="hidden"/>
                        <input id="semak2" name="semak2" type="hidden"/>
                        <input id="semak3" name="semak3" type="hidden"/>
                        <input id="sentStatus" name="sentStatus" value="1" type="hidden"/>
                        <br>
                        @include('contract.post.ipc.partials.forms.ipc.submit', [
                            'route_back'    => 'contract.post.ipc.edit',
                            'hashslug'      => $ipc->sst->hashslug,
                            'route_name'    => 'api.contract.ipc.sijil.update',
                            'btn_send'      => 'acquisition.review.store',
                            'form'          => 'ipc-form',
                        ])
                    </div> --}}
                </div>
            </form>

            {{-- @component('components.tab.button')
            @endcomponent --}}
            
            @endslot
        @endcomponent
    </div> 
@endsection