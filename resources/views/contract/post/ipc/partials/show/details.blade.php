<br>
@if($ipc->count() > 0)
<h4>Senarai Sijil Interim</h4>
        <div class="row">
            <div class="col" width="100%">
                <div class="table-responsive table-bordered">
                    <table class="table " id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No Sijil Interim</th>
                            <th>Jumlah Invois (RM)</th>
                            <th>Material on Site (RM)</th>
                            <th>Wang Pendahuluan</th>
                            <th>Bayaran Balik Wang Pendahuluan (RM)</th>
                            <th>Wang Tahanan (RM)</th>
                            <th>Wang Tahanan Dilepaskan (RM)</th>
                            <th>Wang Ganti Rugi (RM)</th>
                            <th>Wang Tututan (RM)</th>
                            <th>Status Bayaran</th>
                        </tr>
                        </thead>
                        @foreach($ipc as $ip)
                        <tbody>
                            <td>{{ $ip->ipc_no }}</td>
                            <td>{{ money()->toCommon($ip->ipcInvoice->pluck('demand_amount')->sum() ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->material_on_site_amount ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->advance_amount ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->give_advance_amount ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->wjp_amount ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->wjp_lepas_amount ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->compensation_amount ?? 0, 2) }}</td>
                            <td>{{ money()->toCommon($ip->demand_amount ?? 0, 2) }}</td>
                            <td>
                                @if($ip->status == 1)
                                    Draf
                                @elseif($ip->status == 3)
                                    Semakan
                                @elseif($ip->status == 3)
                                    Sedang diproses di SAP
                                @elseif($ip->status == 4)
                                    Selesai
                                @endif
                            </td>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
     <br>    
@endif
