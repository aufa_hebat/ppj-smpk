@if(!empty($ipc) && $ipc->ipc_no == '1')
    <h5>SENARAI SEMAK DOKUMEN BAYARAN INTERIM (IPC NO 1)</h5>
    <div class="table-responsive table-bordered">
        <table class="table " id="table_jualan" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th width="10%">BIL</th>
                    <th width="40%">PERKARA</th>
                    <th width="20%">LAMPIRAN</th>
                    @if($ipc->status == 1)
                    <th width="30%">MUAT NAIK</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1.</td>
                    <td>Sijil Bayaran Interim (IPC), Ringkasan Bayaran, Status Bayaran Interim, Butiran Bayaran</td>
                    <td>
                        @if(!empty($ipc->doc_sijil))
                            Lampiran A: <br>
                            <a href="/download/{{ $ipc->doc_sijil->document_path }}/{{ $ipc->doc_sijil->document_name }}" target="_blank">{{ $ipc->doc_sijil->document_name }}</a>
                        @endif
                        <br>
                        @if(!empty($ipc->doc_summary))
                            Lampiran B: <br>
                            <a href="/download/{{ $ipc->doc_summary->document_path }}/{{ $ipc->doc_summary->document_name }}" target="_blank">{{ $ipc->doc_summary->document_name }}</a>
                        @endif
                        <br>
                        @if(!empty($ipc->doc_status))
                            Lampiran C: <br>
                            <a href="/download/{{ $ipc->doc_status->document_path }}/{{ $ipc->doc_status->document_name }}" target="_blank">{{ $ipc->doc_status->document_name }}</a>
                        @endif
                        <br>
                        @if(!empty($ipc->doc_detail))
                            Lampiran D: <br>
                            <a href="/download/{{ $ipc->doc_detail->document_path }}/{{ $ipc->doc_detail->document_name }}" target="_blank">{{ $ipc->doc_detail->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label"> Sijil Bayaran Interim (Lampiran A)</label>
                            <div class="col input-group">
                                <input id="subFileSijil" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileSijil"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileSijil" name="documentSijil[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_sijil) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_sijil->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_sijil->id }}" class="buang_attach" value="{{ $ipc->doc_sijil->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label">Ringkasan Bayaran (Lampiran B)</label>
                            <div class="col input-group">
                                <input id="subFileSummary" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileSummary"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileSummary" name="documentSummary[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_summary) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_summary->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_summary->id }}" class="buang_attach" value="{{ $ipc->doc_summary->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label">Status Bayaran Interim (Lampiran C)</label>
                            <div class="col input-group">
                                <input id="subFileStatus" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileStatus"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileStatus" name="documentStatus[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_status) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_status->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_status->id }}" class="buang_attach" value="{{ $ipc->doc_status->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label">Butiran Bayaran Interim (Lampiran D)</label>
                            <div class="col input-group">
                                <input id="subFileDetail" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileDetail"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileDetail" name="documentDetail[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_detail) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_detail->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_detail->id }}" class="buang_attach" value="{{ $ipc->doc_detail->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Surat Tuntutan/Inbois/Inbois Cukai (perlu ada bukti Cop Penerimaan oleh Jabatan)</td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->ipcInvoice) && !empty($ipc->ipcInvoice->pluck('document')))
                            <a href="/download/{{ $ipc->ipcInvoice->pluck('document')->pluck('document_path')->first() }}/{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}" target="_blank">{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Salinan Surat Setuju Terima (LOA)</td>
                    <td>
                        @if(!empty($ipc->sst->document))
                            <a href="/download/{{ $ipc->sst->document->document_path }}/{{ $ipc->sst->document->document_name }}" target="_blank">{{ $ipc->sst->document->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Salinan Polisi Insurans<br>
                        a. Contractor's All Risk (Insurance of Work & Third Party Liability)<br>
                        b. Workmen Compensation
                    </td>
                    <td>
                        {{-- public --}}
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_publicLiability->first()))
                            Tanggungan Awam : <br>
                            @foreach($ipc->sst->insurance->documents_publicLiability as $public)
                                @if(!empty($public))
                                    <a href="/download/{{ $public->document_path }}/{{ $public->document_name }}" target="_blank">{{ $public->document_name }}</a>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                        {{-- work --}}
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_work->first()))
                            Insurans Kerja : <br>
                            @foreach($ipc->sst->insurance->documents_work as $work)
                                @if(!empty($work))
                                    <a href="/download/{{ $work->document_path }}/{{ $work->document_name }}" target="_blank">{{ $work->document_name }}</a>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                        {{-- compensation --}}
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->insurance) && !empty($ipc->sst->insurance->documents_compensation->first()))
                            Insurans Pampasan Kerja : <br>
                            @foreach($ipc->sst->insurance->documents_compensation as $compensation)
                                @if(!empty($compensation))
                                    <a href="/download/{{ $compensation->document_path }}/{{ $compensation->document_name }}" target="_blank">{{ $compensation->document_name }}</a>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Salinan Resit Premium Insurans (Sekiranya Polisi masih belum diperolehi)<br>
                        a. Contractor's All Risk (Insurance of Work & Third Party Liability)<br> 
                        b. Workmen Compensation
                    </td>
                    <td></td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Salinan Bon Pelaksana<br>
                        Salinan resit Premium Bon Pelaksanaan<hr>
                        Salinan surat menyatakan pemotongan Wang Jaminan Pelaksanaan (WJP)
                    </td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->bon) && !empty($ipc->sst->bon->documents))
                            @foreach($ipc->sst->bon->documents as $bon)
                                @if(!empty($bon))
                                    <a href="/download/{{ $bon->document_path }}/{{ $bon->document_name }}" target="_blank">{{ $bon->document_name }}</a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Salinan Bill of Quatities (BQ)</td>
                    <td>
                        <a href="{{ route('acq_bq',['hashslug' => $ipc->sst->acquisition->hashslug ?? 0]) }}" target="_blank">
                            {{ __(' Senarai Kuantiti') }}
                        </a>
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>8.</td>
                    <td>Pendaftaran Pertubuhan Keselamatan Sosial (PERKESO) </td>
                    <td>
                        @if(!empty($ipc->doc_perkeso))
                            <a href="/download/{{ $ipc->doc_perkeso->document_path }}/{{ $ipc->doc_perkeso->document_name }}" target="_blank">{{ $ipc->doc_perkeso->document_name }}</a>
                        @endif      
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label"> Pendaftaran Pertubuhan Keselamatan Sosial (PERKESO) </label>
                            <div class="col input-group">
                                <input id="subFilePerkeso" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFilePerkeso"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFilePerkeso" name="documentPerkeso[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_perkeso) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_perkeso->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_perkeso->id }}" class="buang_attach" value="{{ $ipc->doc_perkeso->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>9.</td>
                    <td>Pendaftaran Cukai Barang & Perkhidmatan (SST)</td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->company) && !empty($ipc->sst->company->gst))
                            <a href="/download/{{ $ipc->sst->company->gst->document_path }}/{{ $ipc->sst->company->gst->document_name }}" target="_blank">{{ $ipc->sst->company->gst->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>10.</td>
                    <td>Salinan Resit Levi (untuk tender kerja RM500,000 ke atas sahaja)</td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->doc_levi))
                            <a href="/download/{{ $ipc->doc_levi->document_path }}/{{ $ipc->doc_levi->document_name }}" target="_blank">{{ $ipc->doc_levi->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label">Resit Levi</label>
                            <div class="col input-group">
                                <input id="subFileLevi" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileLevi"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileLevi" name="documentLevi[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_levi) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_levi->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_levi->id }}" class="buang_attach" value="{{ $ipc->doc_levi->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </div><br>

    @if( !empty($ipc) && $ipc->last_ipc == 'y')
        <div class="table-responsive table-bordered">
            <table class="table " id="table_jualan" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="10%">BIL</th>
                        <th width="40%">PERKARA</th>
                        <th width="20%">LAMPIRAN</th>
                        @if($ipc->status == 1)
                        <th width="30%">MUAT NAIK</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>11.</td>
                        <td>Butiran Maklumat Kontrak</td>
                        <td>
                            @if(!empty($ipc->doc_detail))
                                Lampiran D:<br>
                                <a href="/download/{{ $ipc->doc_detail->document_path }}/{{ $ipc->doc_detail->document_name }}" target="_blank">{{ $ipc->doc_detail->document_name }}</a>
                            @endif
                        </td>
                        @if($ipc->status == 1)
                        <td>
                            <div class="form-group row">
                                <label class="col col-form-label">Butiran Bayaran Interim (Lampiran D)</label>
                                <div class="col input-group">
                                    <input id="subFileDetail" type="text"  class="form-control" readonly>
                                    <label class="input-group-text" for="uploadFileDetail"><i class="fe fe-upload" ></i></label>
                                    <input type="file" class="form-control" id="uploadFileDetail" name="documentDetail[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_detail) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_detail->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_detail->id }}" class="buang_attach" value="{{ $ipc->doc_detail->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td>Perakuan Akaun Dan Bayaran Muktamad (Berwarna Biru)</td>
                        <td>
                            @if(!empty($ipc->doc_sofa))
                            <a href="/download/{{ $ipc->doc_sofa->document_path }}/{{ $ipc->doc_sofa->document_name }}" target="_blank">{{ $ipc->doc_sofa->document_name }}</a>
                            @endif
                        </td>
                        @if($ipc->status == 1)
                        <td>
                            <div class="form-group row">
                                <label class="col col-form-label">Perakuan Akaun Dan Bayaran Muktamad</label>
                                <div class="col input-group">
                                    <input id="subFileSofa" type="text"  class="form-control" readonly>
                                    <label class="input-group-text" for="uploadFileSofa"><i class="fe fe-upload" ></i></label>
                                    <input type="file" class="form-control" id="uploadFileSofa" name="documentSofa[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                    @if( !empty($ipc) && !empty($ipc->doc_sofa) )
                                        <label class="input-group-text" for="deleteFile{{ $ipc->doc_sofa->id }}">
                                            <input type="input" id="deleteFile{{ $ipc->doc_sofa->id }}" class="buang_attach" value="{{ $ipc->doc_sofa->hashslug }}" hidden>
                                            <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                        </label>
                                    @endif
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td>13.</td>
                        <td>Sijil Bayaran Interim (IPC Akhir)<br>
                            Semakan : WJP (Telah dibayar/belum dibayar) 
                                      Bon Pelaksanaan
                        </td>
                        <td>
                            @if(!empty($ipc->doc_sijil))
                                <a href="/download/{{ $ipc->doc_sijil->document_path }}/{{ $ipc->doc_sijil->document_name }}" target="_blank">{{ $ipc->doc_sijil->document_name }}</a>
                            @endif
                        </td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Perakuan Pelarasan Jumlah Harga Kontrak (PPJHK) Berwarna Hijau 
                            **PPJHK No............. 
                            **PPJHK No............. 
                        </td>
                        <td>{{-- Cetakan VO --}}</td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Salinan Sijil Perakuan Kerja (CPC) 
                            {{-- Salinan Perakuan Siap Kerja-Kerja Penyelenggaraan (CCMW) --}}
                        </td>
                        <td>
                            @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->documents->first()))
                                Tanggungan Awam : <br>
                                @foreach($ipc->sst->cpc->documents as $cpc)
                                    @if(!empty($cpc))
                                        <a href="/download/{{ $cpc->document_path }}/{{ $cpc->document_name }}" target="_blank">{{ $cpc->document_name }}</a>
                                        <br>
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td>Salinan Lanjutan Masa (EOT) No. @if(!empty($ipc->sst->eots->last())){{ $ipc->sst->eots->last()->bil }} ( {{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $ipc->sst->eots->last()->original_end_date)->format('d/m/Y') }} Hingga {{ isset($ipc->sst->eots->last()->eot_approve->approved_end_date) ? \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $ipc->sst->eots->last()->eot_approve->approved_end_date)->format('d/m/Y') : \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $ipc->sst->eots->last()->extended_end_date)->format('d/m/Y') }} )@else ... ........ hingga ........ @endif </td>
                        <td>
                             @if(!empty($ipc->sst->eots->last()))
                                <a href="{{ route('extension_report',['hashslug' => $ipc->sst->eots->last()->hashslug]) }}" target="_blank">
                                    {{ __('Salinan Lanjutan Masa (EOT)') }}
                                </a>
                            @endif
                        </td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td>Salinan Sijil Tidak Siap Kerja / CNC / LAD</td>
                        <td>
                            @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cnc) && !empty($ipc->sst->cnc->documents->first()))
                                Tanggungan Awam : <br>
                                @foreach($ipc->sst->cnc->documents as $cnc)
                                    @if(!empty($cnc))
                                        <a href="/download/{{ $cnc->document_path }}/{{ $cnc->document_name }}" target="_blank">{{ $cnc->document_name }}</a>
                                        <br>
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    <tr>
                        <td>18.</td>
                        <td>Salinan Sijil Siap Membaiki Kecacatan (CMGD) </td>
                        <td>
                            @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cmgd) && !empty($ipc->sst->cmgd->documents->first()))
                                Tanggungan Awam : <br>
                                @foreach($ipc->sst->cmgd->documents as $cmgd)
                                    @if(!empty($cmgd))
                                        <a href="/download/{{ $cmgd->document_path }}/{{ $cmgd->document_name }}" target="_blank">{{ $cmgd->document_name }}</a>
                                        <br>
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    {{-- <tr>
                        <td>11.</td>
                        <td>Surat Pengecualian LAD</td>
                        <td></td>
                    </tr> --}}
                    <tr>
                        <td>19.</td>
                        <td>Salinan Sijil-Sijil IPC terdahulu yang telah dibayar 
                            Jumlah kesemua IPC Bil {{ $total_ipc->pluck('ipc_no')->first() }} hingga {{ $total_ipc->pluck('ipc_no')->sortbydesc('ipc_no')->last() }}
                        </td>
                        <td>
                            @if(!empty($sst))
                                @foreach($sst as $ssts)
                                    @if(!empty($ssts->ipc) && !empty($ssts->ipc->pluck('doc_sijil')))
                                        @foreach($ssts->ipc->pluck('doc_sijil') as $ipc_all)
                                            @if(!empty($ipc_all))
                                                <a href="/download/{{ $ipc_all->document_path }}/{{ $ipc_all->document_name }}" target="_blank">{{ $ipc_all->document_name }}</a>
                                                <br>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        @if($ipc->status == 1)<td></td>@endif
                    </tr>
                    <tr>
                        <td>20.</td>
                        <td>Akuan Statuori (Akuan Bersumpah)</td>
                        <td>
                            @if(!empty($ipc->doc_statuori))
                            <a href="/download/{{ $ipc->doc_statuori->document_path }}/{{ $ipc->doc_statuori->document_name }}" target="_blank">{{ $ipc->doc_statuori->document_name }}</a>
                            @endif
                        </td>
                        @if($ipc->status == 1)
                        <td>
                            <div class="form-group row">
                                <label class="col col-form-label">Akuan Statuori (Akuan Bersumpah)</label>
                                <div class="col input-group">
                                    <input id="subFileStatuori" type="text"  class="form-control" readonly>
                                    <label class="input-group-text" for="uploadFileStatuori"><i class="fe fe-upload" ></i></label>
                                    <input type="file" class="form-control" id="uploadFileStatuori" name="documentStatuori[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                    @if( !empty($ipc) && !empty($ipc->doc_statuori) )
                                        <label class="input-group-text" for="deleteFile{{ $ipc->doc_statuori->id }}">
                                            <input type="input" id="deleteFile{{ $ipc->doc_statuori->id }}" class="buang_attach" value="{{ $ipc->doc_statuori->hashslug }}" hidden>
                                            <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                        </label>
                                    @endif
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                </tbody>           
            </table>
        </div>
    @endif
@elseif( !empty($ipc) && $ipc->last_ipc == 'n' && $ipc->ipc_no != '1')
    <h5>SENARAI SEMAK DOKUMEN BAYARAN INTERIM (IPC NO {{$ipc->ipc_no}})</h5>
    <div class="table-responsive table-bordered">
        <table class="table " id="table_jualan" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th width="10%">BIL</th>
                    <th width="40%">PERKARA</th>
                    <th width="20%">LAMPIRAN</th>
                    @if($ipc->status == 1)
                    <th width="20%">MUAT NAIK</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1.</td>
                    <td>Sijil Bayaran Interim (IPC), Ringkasan Bayaran, Status Bayaran Interim, Butiran Bayaran</td>
                    <td>
                        @if(!empty($ipc->doc_sijil))
                            Lampiran A:<br>
                            <a href="/download/{{ $ipc->doc_sijil->document_path }}/{{ $ipc->doc_sijil->document_name }}" target="_blank">{{ $ipc->doc_sijil->document_name }}</a>
                        @endif
                        <br>
                        @if(!empty($ipc->doc_summary))
                            Lampiran B:<br>
                            <a href="/download/{{ $ipc->doc_summary->document_path }}/{{ $ipc->doc_summary->document_name }}" target="_blank">{{ $ipc->doc_summary->document_name }}</a>
                        @endif
                        <br>
                        @if(!empty($ipc->doc_status))
                            Lampiran C:<br>
                            <a href="/download/{{ $ipc->doc_status->document_path }}/{{ $ipc->doc_status->document_name }}" target="_blank">{{ $ipc->doc_status->document_name }}</a>
                        @endif
                        @if(!empty($ipc->doc_detail))
                            Lampiran D:<br>
                            <a href="/download/{{ $ipc->doc_detail->document_path }}/{{ $ipc->doc_detail->document_name }}" target="_blank">{{ $ipc->doc_detail->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label"> Sijil Bayaran Interim (Lampiran A)</label>
                            <div class="col input-group">
                                <input id="subFileSijil" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileSijil"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileSijil" name="documentSijil[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_sijil) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_sijil->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_sijil->id }}" class="buang_attach" value="{{ $ipc->doc_sijil->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label">Ringkasan Bayaran (Lampiran B)</label>
                            <div class="col input-group">
                                <input id="subFileSummary" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileSummary"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileSummary" name="documentSummary[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_summary) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_summary->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_summary->id }}" class="buang_attach" value="{{ $ipc->doc_summary->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label">Status Bayaran Interim (Lampiran C)</label>
                            <div class="col input-group">
                                <input id="subFileStatus" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileStatus"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileStatus" name="documentStatus[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_status) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_status->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_status->id }}" class="buang_attach" value="{{ $ipc->doc_status->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col col-form-label">Butiran Bayaran Interim (Lampiran D)</label>
                            <div class="col input-group">
                                <input id="subFileDetail" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileDetail"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileDetail" name="documentDetail[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_detail) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_detail->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_detail->id }}" class="buang_attach" value="{{ $ipc->doc_detail->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Salinan Surat Setuju Terima (LOA) & Bil Of Quatities (BQ)</td>
                    <td>
                        @if(!empty($ipc->sst->document))
                            <a href="/download/{{ $ipc->sst->document->document_path }}/{{ $ipc->sst->document->document_name }}" target="_blank">{{ $ipc->sst->document->document_name }}</a>
                        @endif
                        <br>
                        <a href="{{ route('acq_bq',['hashslug' => $ipc->sst->acquisition->hashslug ?? 0]) }}" target="_blank">
                            {{ __(' Senarai Kuantiti') }}
                        </a>
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Salinan Polisi Permohonan Perubahan Kerja (Borang PPK No._____)</td>
                    <td></td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Salinan Perakuan Pelarasan Jumlah Harga Kontrak (PPJHK No.____)</td>
                    <td></td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Surat Tuntutan/Inbois/Inbois Cukai (perlu ada bukti Cop Penerimaan oleh Jabatan)</td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->ipcInvoice) && !empty($ipc->ipcInvoice->pluck('document')))
                            <a href="/download/{{ $ipc->ipcInvoice->pluck('document')->pluck('document_path')->first() }}/{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}" target="_blank">{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
            </tbody>
        </table>
    </div><br>
@elseif( !empty($ipc) && $ipc->last_ipc == 'y')
    <h5>SENARAI SEMAK DOKUMEN BAYARAN INTERIM (IPC NO {{$ipc->ipc_no}})</h5>
    <div class="table-responsive table-bordered">
        <table class="table " id="table_jualan" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th width="10%">BIL</th>
                    <th width="40%">PERKARA</th>
                    <th width="20%">LAMPIRAN</th>
                    @if($ipc->status == 1)
                    <th width="30%">MUAT NAIK</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1.</td>
                    <td>Butiran Maklumat Kontrak</td>
                    <td>
                        @if(!empty($ipc->doc_detail))
                            Lampiran D:<br>
                            <a href="/download/{{ $ipc->doc_detail->document_path }}/{{ $ipc->doc_detail->document_name }}" target="_blank">{{ $ipc->doc_detail->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label">Butiran Bayaran Interim (Lampiran D)</label>
                            <div class="col input-group">
                                <input id="subFileDetail" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileDetail"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileDetail" name="documentDetail[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                            @if( !empty($ipc) && !empty($ipc->doc_detail) )
                                <label class="input-group-text" for="deleteFile{{ $ipc->doc_detail->id }}">
                                    <input type="input" id="deleteFile{{ $ipc->doc_detail->id }}" class="buang_attach" value="{{ $ipc->doc_detail->hashslug }}" hidden>
                                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                </label>
                            @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Perakuan Akaun Dan Bayaran Muktamad (Berwarna Biru)</td>
                    <td>
                        @if(!empty($ipc->doc_sofa))
                        <a href="/download/{{ $ipc->doc_sofa->document_path }}/{{ $ipc->doc_sofa->document_name }}" target="_blank">{{ $ipc->doc_sofa->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label">Perakuan Akaun Dan Bayaran Muktamad</label>
                            <div class="col input-group">
                                <input id="subFileSofa" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileSofa"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileSofa" name="documentSofa[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_sofa) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_sofa->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_sofa->id }}" class="buang_attach" value="{{ $ipc->doc_sofa->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Sijil Bayaran Interim (IPC Akhir)<br>
                        Semakan : WJP (Telah dibayar/belum dibayar) 
                                  Bon Pelaksanaan
                    </td>
                    <td>
                        @if(!empty($ipc->doc_sijil))
                            <a href="/download/{{ $ipc->doc_sijil->document_path }}/{{ $ipc->doc_sijil->document_name }}" target="_blank">{{ $ipc->doc_sijil->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Surat Tuntutan/Inbois/Inbois Cukai (perlu ada bukti Cop Penerimaan oleh Jabatan)</td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->ipcInvoice) && !empty($ipc->ipcInvoice->pluck('document')))
                            <a href="/download/{{ $ipc->ipcInvoice->pluck('document')->pluck('document_path')->first() }}/{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}" target="_blank">{{ $ipc->ipcInvoice->pluck('document')->pluck('document_name')->first() }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Perakuan Pelarasan Jumlah Harga Kontrak (PPJHK) Berwarna Hijau 
                        **PPJHK No............. 
                        **PPJHK No............. 
                    </td>
                    <td>{{-- Cetakan VO --}}</td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Salinan Sijil Perakuan Kerja (CPC) 
                        {{-- Salinan Perakuan Siap Kerja-Kerja Penyelenggaraan (CCMW) --}}
                    </td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cpc) && !empty($ipc->sst->cpc->documents->first()))
                            Tanggungan Awam : <br>
                            @foreach($ipc->sst->cpc->documents as $cpc)
                                @if(!empty($cpc))
                                    <a href="/download/{{ $cpc->document_path }}/{{ $cpc->document_name }}" target="_blank">{{ $cpc->document_name }}</a>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Salinan Lanjutan Masa (EOT) No. @if(!empty($ipc->sst->eots->last())){{ $ipc->sst->eots->last()->bil }} ( {{ \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $ipc->sst->eots->last()->original_end_date)->format('d/m/Y') }} Hingga {{ isset($ipc->sst->eots->last()->eot_approve->approved_end_date) ? \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $ipc->sst->eots->last()->eot_approve->approved_end_date)->format('d/m/Y') : \Carbon\Carbon::CreateFromFormat('Y-m-d H:i:s', $ipc->sst->eots->last()->extended_end_date)->format('d/m/Y') }} )@else ... ........ hingga ........ @endif </td>
                    <td>
                         @if(!empty($ipc->sst->eots->last()))
                            <a href="{{ route('extension_report',['hashslug' => $ipc->sst->eots->last()->hashslug]) }}" target="_blank">
                                {{ __('Salinan Lanjutan Masa (EOT)') }}
                            </a>
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>8.</td>
                    <td>Salinan Sijil Tidak Siap Kerja / CNC / LAD</td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cnc) && !empty($ipc->sst->cnc->documents->first()))
                            Tanggungan Awam : <br>
                            @foreach($ipc->sst->cnc->documents as $cnc)
                                @if(!empty($cnc))
                                    <a href="/download/{{ $cnc->document_path }}/{{ $cnc->document_name }}" target="_blank">{{ $cnc->document_name }}</a>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>9.</td>
                    <td>Salinan Sijil Siap Membaiki Kecacatan (CMGD) </td>
                    <td>
                        @if(!empty($ipc) && !empty($ipc->sst) && !empty($ipc->sst->cmgd) && !empty($ipc->sst->cmgd->documents->first()))
                            Tanggungan Awam : <br>
                            @foreach($ipc->sst->cmgd->documents as $cmgd)
                                @if(!empty($cmgd))
                                    <a href="/download/{{ $cmgd->document_path }}/{{ $cmgd->document_name }}" target="_blank">{{ $cmgd->document_name }}</a>
                                    <br>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                {{-- <tr>
                    <td>11.</td>
                    <td>Surat Pengecualian LAD</td>
                    <td></td>
                </tr> --}}
                <tr>
                    <td>10.</td>
                    <td>Salinan Sijil-Sijil IPC terdahulu yang telah dibayar 
                        Jumlah kesemua IPC Bil {{ $total_ipc->pluck('ipc_no')->first() }} hingga {{ $total_ipc->pluck('ipc_no')->sortbydesc('ipc_no')->last() }}
                    </td>
                    <td>
                        @if(!empty($sst))
                            @foreach($sst as $ssts)
                                @if(!empty($ssts->ipc) && !empty($ssts->ipc->pluck('doc_sijil')))
                                    @foreach($ssts->ipc->pluck('doc_sijil') as $ipc_all)
                                        @if(!empty($ipc_all))
                                            <a href="/download/{{ $ipc_all->document_path }}/{{ $ipc_all->document_name }}" target="_blank">{{ $ipc_all->document_name }}</a>
                                            <br>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </td>
                    @if($ipc->status == 1)<td></td>@endif
                </tr>
                <tr>
                    <td>11.</td>
                    <td>Akuan Statuori (Akuan Bersumpah)</td>
                    <td>
                        @if(!empty($ipc->doc_statuori))
                        <a href="/download/{{ $ipc->doc_statuori->document_path }}/{{ $ipc->doc_statuori->document_name }}" target="_blank">{{ $ipc->doc_statuori->document_name }}</a>
                        @endif
                    </td>
                    @if($ipc->status == 1)
                    <td>
                        <div class="form-group row">
                            <label class="col col-form-label">Akuan Statuori (Akuan Bersumpah)</label>
                            <div class="col input-group">
                                <input id="subFileStatuori" type="text"  class="form-control" readonly>
                                <label class="input-group-text" for="uploadFileStatuori"><i class="fe fe-upload" ></i></label>
                                <input type="file" class="form-control" id="uploadFileStatuori" name="documentStatuori[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                @if( !empty($ipc) && !empty($ipc->doc_statuori) )
                                    <label class="input-group-text" for="deleteFile{{ $ipc->doc_statuori->id }}">
                                        <input type="input" id="deleteFile{{ $ipc->doc_statuori->id }}" class="buang_attach" value="{{ $ipc->doc_statuori->hashslug }}" hidden>
                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                    </label>
                                @endif
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
            </tbody>           
        </table>
    </div>
@endif