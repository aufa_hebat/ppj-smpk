<html>
    <head>
        <style type="text/css">
            @page {
                margin: 70px 70px 70px 100px;
            }
            div.breakNow { page-break-inside:avoid; page-break-after:always; }    

            .bordered {
                border-color: #959594;
                border-style: solid;
                border-width: 1px;
            }
            
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }  

            .content{
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
			}
        </style>
    </head>
    <body>
        <div>
            <center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>
        </div>
        <div style="text-align: center; font-weight: bold; text-transform: uppercase;" class="title">
            <span class="title">Perbadanan Putrajaya</span><br/>
            <span class="title">Sijil Siap Membaiki Kecacatan</span>
        </div>
        <br/>
        <div>
            <table width="100%" class="content">
                <tr>
                    <td width="65%">&nbsp;</td>
                    <td width="35%">
                        <span>
                            Perbadanan Putrajaya<br/>
                            Kompleks Perbadanan Putrajaya<br/>
                            24, Persiaran Perdana, Presint 3<br/>
                            62675 Putrajaya<br/>
                            Wilayah Persekutuan Putrajaya<br/><br/>
                            Tarikh : {{ locale_date(\Carbon\Carbon::now()) }}
                        </span>
                    </td>
                </tr>
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td>
                        <span style="font-weight: bold; text-transform: uppercase;">
                            {{ $sst->company->company_name }}<br/>
                        </span>
                        <span>
                        @if(!empty($sst->company->addresses[0]))
                            @if(!empty($sst->company->addresses[0]->primary))
                                <span>{{ $sst->company->addresses[0]->primary }}</span><br />
                            @endif
                            @if(!empty($sst->company->addresses[0]->secondary))
                                <span>{{ $sst->company->addresses[0]->secondary }}</span><br />
                            @endif
                            @if(!empty($sst->company->addresses[0]->postcode))
                                <span>{{ $sst->company->addresses[0]->postcode }}, </span>
                            @endif
                            @if(!empty($sst->company->addresses[0]->state))
                                <span style="text-transform: uppercase;">{{ $sst->company->addresses[0]->state }}</span>                                    
                            @endif
                        @endif  
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <br/>
        <div>
            <table width="100%" class="content">
                <tr>
                    <td width="150px" valign="top">Berdaftar dengan</td>
                    <td width="2px"valign="top"> : </td>
                    <td valign="top"> <span style="font-weight: bold; text-transform: uppercase; text-align: justify;">{{ $cidb }}</span></td>
                </tr>
                <tr>
                    <td valign="top">No. Kontrak</td>
                    <td valign="top"> : </td>
                    <td valign="top"><span style="font-weight: bold;">{{ $sst->acquisition->reference }}</span></td>
                </tr>
                <tr>
                    <td valign="top">Kontrak Untuk</td>
                    <td valign="top"> : </td>
                    <td valign="top">  <span style="font-weight: bold; text-transform: uppercase; text-align: justify; line-height: 1.6">
                            {{ $sst->acquisition->title }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><hr/></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: justify;">
                        <span style="text-align: justify; line-height: 1.6">Menurut Klausa <b>{{ $cmgd->clause }}</b> 
                        @if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga'))
                                Sebut Harga
                            @else
                                Kontrak
                            @endif
                        ,
                        
                        maka adalah dengan ini diperakui bahawa segala kecacatan,
                        ketidaksempurnaan dan apa jua pun terhadap kerja-kerja tersebut 
                        di atas yang dikehendaki diperbaiki di bawah Syarat-Syarat
                        @if($sst->acquisition->acquisition_category_id == config('enums.Sebut Harga B') || $sst->acquisition->acquisition_category_id == config('enums.Sebut Harga'))
                                Sebut Harga
                            @else
                                Kontrak
                            @endif 
                        telah 
                        sempurna diperbaiki pada 
                        <span style="font-weight: bold; ">{{ isset($cmgd->mgd_date) ? locale_date($cmgd->mgd_date) : "-" }} </span>.
                    </td>
                </tr>
            </table>
        </div>
        <br><br>
        <br><br>
        <br><br>
         
        <div>
            <table width="100%" class="content">
                <tr>
                    <td width="40%">............................................................................<br></td>
                    <td width="10%"></td>
                    <td width="40%">.............................................................................<br></td>
                </tr>
                <tr>
                    <td valign="top">
                        Tandatangan Penolong Wakil Perbadanan yang dilantik
                    </td>
                    <td ></td>
                    <td valign="top">
                        Tandatangan pegawai yang diwakilkan bertindak untuk dan bagi pihak <b>PERBADANAN PUTRAJAYA</b>
                    </td>            
                </tr>

                <tr>

                    <td><br><br><br><br><br>
                        Tarikh:________________________</td>
                    <td></td>
                    <td><br><br><br><br><br>
                        Tarikh:________________________</td>
                </tr>
                
            </table>
        </div>
    </body>
</html>