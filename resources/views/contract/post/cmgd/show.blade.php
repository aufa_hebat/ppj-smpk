@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(!empty($cmgd))
                $('#cmgd_reference_no').html('{{ $cmgd->reference_no }}');
                $('#cmgd_site_visit_date').html('{{ $cmgd->site_visit_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cmgd->site_visit_date)->format('d/m/Y') : null }}');
                $('#cmgd_signature_date').html('{{ $cmgd->signature_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cmgd->signature_date)->format('d/m/Y') : null }}');
                $('#cmgd_mgd_date').html('{{ $cmgd->mgd_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $cmgd->mgd_date)->format('d/m/Y') : null }}');
                $('#cmgd_clause').html('{{ $cmgd->clause }}');

                // UPLOADS
                @if($cmgd->documents->count() > 0)
                    var cmgd_t = $('#cmgd_tblUpload').DataTable({
                            searching: false,
                            ordering: false,
                            paging: false,
                            info:false
                        });

                    var cmgd_counter = 1;
                    @foreach($cmgd->documents as $doc)
                        cmgd_t.row.add( [
                            cmgd_counter,
                            '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                        ] ).draw( false );

                        cmgd_counter++;
                    @endforeach
                @endif
                // UPLOADS END
            @endif
        });

    </script>
@endpush

<h4>Sijil Siap Membaiki Kecacatan</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="btn-group float-right">
            <a target="_blank" href="{{ route('cmgdCertificate', ['hashslug'=>$sst->hashslug] ) }}"
               class="btn btn-success">
                @icon('fe fe-printer') {{ __('Cetak') }}
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('No Rujukan Fail') }}</div>
                    <div id="cmgd_reference_no"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Kontrak') }}</div>
                    <div id="cmgd_clause"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Sijil Di tandatangan') }}</div>
                    <div id="cmgd_signature_date"></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Siap Membaiki Kecacatan') }}</div>
                    <div id="cmgd_mgd_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Lawatan Tapak') }}</div>
                    <div id="cmgd_site_visit_date"></div>
                </div>
            </div>
        </div>

        @include('contract.post.cmgd.partials.shows.uploads')
        @include('contract.post.partials.shows.return')
    </div>


</div>