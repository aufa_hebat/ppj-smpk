@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/previous_next.js') }}"></script>
    @include('components.forms.assets.datetimepicker')
	<script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
            // $('#document-tab-content a').click(function (e) {
            //     e.preventDefault();
            //     $(this).tab('show');
            // });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#datetimepicker-start_working_date').datetimepicker({ format:'DD/MM/YYYY',useCurrent: false });
            $('#datetimepicker-end_working_date').datetimepicker({ format:'DD/MM/YYYY',useCurrent: false });

            @if($appointed->company_id)
                $( "#company_id" ).val('{{ $appointed->company_id }}');
            @endif
            @if($appointed->acquisition_id)
                $( "#acquisition_id" ).val('{{ $appointed->acquisition_id }}');
            @endif
            @if($appointed->acquisition->category->name == 'Sebut Harga B' || $appointed->acquisition->category->name == 'Sebut Harga')
                $("#contract_no").val('{{ $appointed->acquisition->reference }}').prop('readonly', !this.checked);
            @elseif($appointed->acquisition->category->name == 'Tender Terbuka' || $appointed->acquisition->category->name == 'Tender Terhad')
                @if(isset($sst->contract_no))
                    $("#contract_no").val('{{ $sst->contract_no }}');
                @else
                    $("#contract_no").val('{{ $appointed->acquisition->reference }}');
                @endif
            @endif
            @if(isset($sst->company_owner_id))
                $("#company_owner_id").val('{{ $sst->company_owner_id }}').change();
            @endif
            @if(isset($sst->start_working_date) && ($sst->start_working_date != '-0001-11-30 00:00:00'))
                $( "#start_working_date" ).val('{{ $sst->start_working_date->format('d/m/Y')}}');                
            @endif
            @if(isset($sst->end_working_date) && ($sst->end_working_date != '-0001-11-30 00:00:00'))
                $( "#end_working_date" ).val('{{ $sst->end_working_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->letter_date))
                $( "#letter_date" ).val('{{ $sst->letter_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->defects_liability_period))
                $("#defects_liability_period").val('{{ $sst->defects_liability_period }}').change();
            @endif
            @if(isset($sst->report_type_id))
                $("#report_type_id").val('{{ $sst->report_type_id }}').change();
            @endif
            
            @if(isset($sst->document_LAD_amount))

                $('#document_blr').val('{{ money()->toCommon($sst->document_blr) }}');
                $('#document_blr').keyup(function(){
                    var lad = (this.value / 100) * ('{{ money()->toCommon($appointed->offered_price) }}' / 365);
                    
                    @if($appointed->acquisition->category->name == 'Sebut Harga' || $appointed->acquisition->category->name == 'Sebut Harga B')
                        if(lad < 50){
                            $('#document_LAD_amount').val('RM 50.00');
                        }else{
                            $('#document_LAD_amount').val('RM ' + Math.round(lad.toFixed(2), -1) + '.00');
                        }
                    @else
                        $('#document_LAD_amount').val('RM ' + Math.round(lad.toFixed(2), -1) + '.00');
                    @endif
                    
                });                
                $('#document_LAD_amount').val('{{ money()->toHuman($sst->document_LAD_amount) }}');
            @else

                @if($appointed->acquisition->category->name == 'Sebut Harga B' || $appointed->acquisition->category->name == 'Sebut Harga')
                    $('#document_blr').keyup(function(){
                        var lad = (this.value / 100) * ('{{ money()->toCommon($appointed->offered_price) }}' / 365);
                        
                        $('#document_LAD_amount').val('RM ' + Math.round(lad.toFixed(2), -1) + '.00');
                    });
                @else
                    $('#document_blr').keyup(function(){
                        var lad = (this.value / 100) * ('{{ money()->toCommon($appointed->offered_price) }}' / 365);
                    
                        $('#document_LAD_amount').val('RM ' + Math.round(lad.toFixed(2), -1) + '.00');
                    });
                @endif
            @endif
            @if(isset($sst->ins_pli_start_date))
                $("#ins_pli_start_date").val('{{ $sst->ins_pli_start_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->ins_pli_end_date))
                $("#ins_pli_end_date").val('{{ $sst->ins_pli_end_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->ins_pli_amount))
                $("#ins_pli_amount").val('{{ money()->toHuman($sst->ins_pli_amount) }}');
            @endif
            @if(isset($sst->ins_work_start_date))
                $("#ins_work_start_date").val('{{ $sst->ins_work_start_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->ins_work_end_date))
                $("#ins_work_end_date").val('{{ $sst->ins_work_end_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->ins_work_amount))
                $("#ins_work_amount").val('{{ money()->toHuman($sst->ins_work_amount) }}');
            @endif
            @if(isset($sst->ins_ci_start_date))
                $("#ins_ci_start_date").val('{{ $sst->ins_ci_start_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->ins_ci_end_date))
                $("#ins_ci_end_date").val('{{ $sst->ins_ci_end_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->ins_ci_amount))
                $("#ins_ci_amount").val('{{ money()->toHuman($sst->ins_ci_amount) }}');
            @endif
            @if(isset($sst->vp_sign_date))
                $("#vp_sign_date").val('{{ $sst->vp_sign_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->al_issue_date))
                $("#al_issue_date").val('{{ $sst->al_issue_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->al_contractor_date))
                $("#al_contractor_date").val('{{ $sst->al_contractor_date->format('d/m/Y') }}');
            @endif

        	$( "#work_period" ).val('{!! $sale->box->period. ' ' . $sale->box->period_type->name !!}');
            $( "#period" ).val('{!! $sale->box->period !!}');
            $( "#period_type_id" ).val('{!! $sale->box->period_type->id !!}');
            $( "#bon_amount" ).val('{!! money()->toHuman($appointed->bon_amount) !!}');
            $( "#ins_pli_amount" ).val('{!! money()->toHuman($insPliAmount) !!}');
            $( "#ins_work_amount" ).val('{!! money()->toHuman($insWorkAmount) !!}');
            $( "#ins_ci_amount" ).val('{!! money()->toHuman($insCliAmount) !!}');

            @if($appointed->offered_price < 1000000000)
                $("#sl1m_allowance").val('0').prop('readonly', !this.checked);
                $("#sl1m_entity").val('0');
            @endif

            $("#sl1m_allowance").keyup(function(){      //$priceWithoutWps - ini adalah nilai pembina
                var sl1m_entity = {{ money()->toCommon($appointed->offered_price) * 0.01 }};

                sl1m_entity = Math.floor(sl1m_entity / ($("#sl1m_allowance").val() * 12));

                if(sl1m_entity == 'Infinity'){
                    $( "#sl1m_entity" ).val(0);
                }else{
                    $( "#sl1m_entity" ).val(sl1m_entity);
                }

            }); 

            @if(isset($sst->sl1m_allowance) && ($sst->sl1m_allowance > 0))
                $("#sl1m_allowance").val('{{ money()->toCommon($sst->sl1m_allowance) }}');
            @else
                $("#sl1m_allowance").val('0');
            @endif
            @if(isset($sst->sl1m_entity))
                $("#sl1m_entity").val('{{ $sst->sl1m_entity }}');
            @endif
                        

            $("#datetimepicker-start_working_date").on("change.datetimepicker", function (selected) {
                if(selected.date != undefined){

                    var endDate, sstEndDate, insWorkEndDate;
                    if($("#period_type_id").val() == 1){
                        endDate = moment((selected.date)).add($( "#period" ).val(), 'days');
                    }else if($("#period_type_id").val() == 2){
                        endDate = moment((selected.date)).add($( "#period" ).val(), 'weeks');
                    }else if($("#period_type_id").val() == 3){
                        endDate = moment((selected.date)).add($( "#period" ).val(), 'months');
                    }else if($("#period_type_id").val() == 4){
                        endDate = moment((selected.date)).add($( "#period" ).val(), 'years');
                    }
                    

                    if($("#defects_liability_period").val() != 0 || $("#defects_liability_period").val != ""){
                        sstEndDate = moment((endDate)).add($( "#defects_liability_period" ).val(), 'months');
                        // sstEndDate = endDate;
                    }
                    
                    
                    $("#start_working_date").val(selected.date.format("DD/MM/YYYY"));
                    $("#ins_pli_start_date").val(selected.date.format("DD/MM/YYYY"));
                    $("#ins_work_start_date").val(selected.date.format("DD/MM/YYYY"));
                    $("#ins_ci_start_date").val(selected.date.format("DD/MM/YYYY"));

                    //minus one day
                    endDate = moment((endDate)).add( '-1', 'days');
                    sstEndDate = moment((sstEndDate)).add('-1', 'days');

                    
                    $("#end_working_date").val(endDate.format("DD/MM/YYYY"));
                    $("#ins_pli_end_date").val(sstEndDate.format("DD/MM/YYYY"));
                    $("#ins_work_end_date").val(endDate.format("DD/MM/YYYY"));
                    $("#ins_ci_end_date").val(sstEndDate.format("DD/MM/YYYY"));

                }                
            });

            $('#defects_liability_period').change(function() {
                var startDate, endDate, sstEndDate, insWorkEndDate;
                if($("#start_working_date").val() != undefined){
                    
                    startDate = moment($("#start_working_date").val(), "DD/MM/YYYY");
                    
                    if($("#period_type_id").val() == 1){
                        endDate = moment((startDate)).add($( "#period" ).val(), 'days');
                    }else if($("#period_type_id").val() == 2){
                        endDate = moment((startDate)).add($( "#period" ).val(), 'weeks');
                    }else if($("#period_type_id").val() == 3){
                        endDate = moment((startDate)).add($( "#period" ).val(), 'months');
                    }else if($("#period_type_id").val() == 4){
                        endDate = moment((startDate)).add($( "#period" ).val(), 'years');
                    }

                    sstEndDate = moment((endDate)).add($( "#defects_liability_period" ).val(), 'months');
                    // sstEndDate = endDate;

                    //minus 1 day
                    endDate = moment((endDate)).add( '-1', 'days');
                    sstEndDate = moment((sstEndDate)).add('-1', 'days');

                    $("#end_working_date").val(endDate.format("DD/MM/YYYY"));                    
                    $("#ins_pli_end_date").val(sstEndDate.format("DD/MM/YYYY"));
                    $("#ins_work_end_date").val(endDate.format("DD/MM/YYYY"));
                    $("#ins_ci_end_date").val(sstEndDate.format("DD/MM/YYYY"));
                }
            });


        	// Special Place for Abg Nas Start
        	$('#start_working_date').on('dp.change', function(e)
        	{ 
        		//On change Tarikh Mula Kerja, add Tempoh Kerja to generate Tarikh Siap Kerja
        				// Xdapat jumpe func to add date (+ days/months/year)

        		//On change Tarikh Mula Kerja, take its value, paste to ins_pli_start_date, ins_work_start_date, ins_ci_start_date
        				// ins_pli_end_date = Tarikh Mula Kerja + Tempoh Kerja + Tempoh Liabiliti Kecacatan (defects_liability_period)
        				// ins_work_end_date = Tarikh Mula Kerja + Tempoh Kerja
        				// ins_ci_end_date = Tarikh Mula Kerja + Tempoh Kerja + Tempoh Liabiliti Kecacatan (defects_liability_period)

        		//On change Tarikh Mula Kerja, Tarikh Tandatangan Naib Presiden cannot be more than Tarikh Mula Kerja
        		$('#start_working_date').datetimepicker();
		        $('#vice_president_signature_date').datetimepicker({
		            useCurrent: false
		        });
		        $("#start_working_date").on("dp.change", function (e) {
		            $('#vice_president_signature_date').data("DateTimePicker").minDate(e.date);
		        });
		        $("#vice_president_signature_date").on("dp.change", function (e) {
		            $('#start_working_date').data("DateTimePicker").maxDate(e.date);
		        }); 
        	});
        	// Special Place for Abg Nas End


            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                @if(isset($sst->hashslug) && $sst->hashslug != '')
                	var id = '{{ $sst->hashslug }}';
                @else
                	var id = 'first';
                @endif
                var route_name = 'api.contract.acceptance-letter.update';
                var form_id = 'acceptance_letter_form';
                var data = $('#' + form_id).serialize();

                console.log(data);

                axios.put(route(route_name, id), data).then(response => {
                    swal('', response.data.message, 'success');
                    location.reload();
                });
            });
        });
	</script>
@endpush

@section('content')
@component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk: </span>{{ $appointed->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak: </span>{{ $appointed->acquisition->reference }}
        @endslot
    @endcomponent
<div class="row" ref="msgContainer" id="tab_button">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#acquisition-details" role="tab" aria-controls="acquisition-details" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Perolehan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#sst" role="tab" aria-controls="sst" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('SST/LOA') }}
                </a>
            </li>
            @if(!empty($review) && (user()->id == $review->created_by))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#document-reviews" role="tab" aria-controls="document-reviews" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Senarai Ulasan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-log-reviews" role="tab" aria-controls="document-log-reviews" aria-selected="false">
                            @icon('fe fe-book-open')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif
            @endif
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#bon" role="tab" aria-controls="bon" aria-selected="false">
                    @icon('fe fe-dollar-sign')&nbsp;{{ __('Bon') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                    @icon('fe fe-shield')&nbsp;{{ __('Insurans') }}
                </a>
            </li>
            <!-- <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#approval" role="tab" aria-controls="approval" aria-selected="false">
                    @icon('fe fe-thumbs-up')&nbsp;{{ __('Kelulusan') }}
                </a>
            </li> -->
        </ul>
    </div>
    <div class="col-10">
    	<form id="acceptance_letter_form">
    		<input type="hidden" name="company_id" id="company_id">
    		<input type="hidden" name="acquisition_id" id="acquisition_id">
            <input type="hidden" name="period" id="period">
            <input type="hidden" name="period_type_id" id="period_type_id">
	        @component('components.card')
	            @slot('card_body')
	                @component('components.tab.container', ['id' => 'document'])
	                    @slot('tabs')
	                     	@component('components.tab.content', ['id' => 'acquisition-details', 'active' => true])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.forms.acquisition-details', ['appointed' => $appointed])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'sst'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.forms.sst', ['appointed' => $appointed])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'bon'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.forms.bon', ['appointed' => $appointed])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'insurance'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.forms.insurance', ['appointed' => $appointed])
	                            @endslot
	                        @endcomponent
                            
                            {{-- @component('components.tab.button')
                            @endcomponent --}}

	                        @component('components.tab.content', ['id' => 'approval'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.forms.approval', ['appointed' => $appointed])
	                            @endslot
	                        @endcomponent

                            @component('components.tab.content', ['id' => 'document-reviews'])
                                @slot('content')

                                    <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">

                                        @if((!empty($review)) && (user()->id == $review->created_by))

                                            {{-- penyedia --}}

                                            @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl7 as $s7log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s7log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                                                                Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl6 as $s6log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s6log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        @endif
                                    
                                    </div>

                                @endslot
                            @endcomponent

                            <div class="tab-pane fade" id="document-log-reviews" role="tabpanel" aria-labelledby="document-log-reviews-tab">
                                <form id="log-form">
                                    <div class="row justify-content-center w-100">
                                        <div class="col-12 w-100">
                                            @include('contract.pre.box.partials.scripts')
                                            @component('components.card')
                                                @slot('card_body')
                                                    @component('components.datatable', 
                                                        [
                                                            'table_id' => 'contract-pre-box',
                                                            'route_name' => 'api.datatable.contract.review-log-sst',
                                                            'param' => 'acquisition_id=' . $appointed->acquisition_id,
                                                            'columns' => [
                                                                ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                            ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                                ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                            ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                            ],
                                                            'headers' => [
                                                                __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
                                                            ],
                                                            'actions' => minify('')
                                                        ]
                                                    )
                                                    @endcomponent
                                                @endslot
                                            @endcomponent
                                        </div>
                                    </div>
                                </form>
                            </div>

	                    @endslot
	                @endcomponent
	                @slot('card_footer')
	                @endslot
	            @endslot
	        @endcomponent
    	</form>

        <div class="btn-group float-right">
            @if(empty($review) || empty($review->status) || $review->status == 'CN')
                <form method="POST" action="{{ route('acquisition.review.store') }}">
                    @csrf
                    <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                    
                    @if(!empty($appointed))
                    <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $appointed->acquisition_id }}" hidden>
                    @endif

                    <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                    <input type="text" id="progress" name="progress" value="1" hidden>

                    @if(!empty($review_previous))
                    <input type="text" id="task_by" name="task_by" value="{{ $review_previous->task_by }}" hidden>

                    <input type="text" id="law_task_by" name="law_task_by" value="{{ $review_previous->law_task_by }}" hidden>
                    @endif

                    <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                    <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                    <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                    <input type="text" id="type" name="type" value="SST" hidden>
                </form>
            @endif
        </div>
    </div>
</div>
@endsection