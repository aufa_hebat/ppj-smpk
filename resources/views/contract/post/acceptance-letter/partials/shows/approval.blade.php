<h3>Maklumat Kelulusan</h3>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Tandatangan Naib Presiden</div>
            <div>{{ isset($sst->vp_sign_date) ? $sst->vp_sign_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh SST Dikeluarkan</div>
            <div>{{ isset($sst->al_issue_date) ? $sst->al_issue_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Terima SST Dari Kontraktor</div>
            <div>{{ isset($sst->al_contractor_date) ? $sst->al_contractor_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
</div>


