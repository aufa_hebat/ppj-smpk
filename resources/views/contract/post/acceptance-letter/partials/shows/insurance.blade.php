<h3>Insurans Tanggungan Awam</h3>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Insurans</div>
            <div>{{ isset($sst->ins_pli_start_date) ? $sst->ins_pli_start_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Luput Insurans</div>
            <div>{{ isset($sst->ins_pli_end_date) ? $sst->ins_pli_end_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Jumlah Insurans</div>
            <div>{{ isset($sst->ins_pli_amount) ? money()->toHuman($sst->ins_pli_amount) : "-" }}</div>
        </div>
    </div>
</div>
<hr>
<h3>Insurans Kerja</h3>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Insurans</div>
            <div>{{ isset($sst->ins_work_start_date) ? $sst->ins_work_start_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Luput Insurans</div>
            <div>{{ isset($sst->ins_work_end_date) ? $sst->ins_work_end_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Jumlah Insurans</div>
            <div>{{ isset($sst->ins_work_amount) ? money()->toHuman($sst->ins_work_amount) : "-" }}</div>
        </div>
    </div>
</div>
<hr>
<h3>Insurans Pampasan Pekerja</h3>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Insurans</div>
            <div>{{ isset($sst->ins_ci_start_date) ? $sst->ins_ci_start_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Tarikh Luput Insurans</div>
            <div>{{ isset($sst->ins_ci_end_date) ? $sst->ins_ci_end_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <div class="text-muted">Jumlah Insurans</div>
            <div>{{ isset($sst->ins_ci_amount) ? money()->toHuman($sst->ins_ci_amount) : "-" }}</div>
        </div>
    </div>
</div>

