@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            $('#uploadFile').change(function(){
                $('#subFile').val($(this).val().split('\\').pop());
            });

            @if(!empty($sst) && !empty($sst->documents))
                @foreach($sst->documents as $document)
                    $('#subFile').val('{{ $document->document_name }}');
                @endforeach
            @endif


        });
    </script>
@endpush

<h4>Muat Naik Dokumen SST</h4>
<div class="row">
    <div class="col-12">
        <form id="acceptance_letter_form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            
            <div class="form-group row">
                <label for="Fail_Keputusan_Mesyuarat"
                   class="col col-form-label">
                    Muat Naik Kelulusan SST
                </label>

                <div class="col input-group">
                    <input id="subFile" type="text"  class="form-control" readonly>
                    <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                    <input type="file" class="form-control" id="uploadFile" name="document[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                    @if(!empty($sst) && !empty($sst->documents))
                        @foreach($sst->documents as $document)
                            <label class="input-group-text" for="downloadFile">
                                <a href="/download/{{$document->document_path}}/{{ $document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                            </label>
                        @endforeach
				    @endif
                </div>
                
            </div>
            <div class="row">
                <div class="col-4">
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Tandatangan Presiden'),
                        'id' => 'vp_sign_date',
                        'name' => 'vp_sign_date',
                        'config' => ['format' => config('datetime.display.date')],
                    ])
                </div>
                <div class="col-4">
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Surat Setuju Terima Dikeluarkan'),
                        'id' => 'al_issue_date',
                        'name' => 'al_issue_date',
                        'config' => ['format' => config('datetime.display.date')],
                    ])
                </div>
                <div class="col-4">
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Terima SST Dari Kontraktor'),
                        'id' => 'al_contractor_date',
                        'name' => 'al_contractor_date',
                        'config' => ['format' => config('datetime.display.date')],
                    ])
                </div>
            </div>

            <div class="row">
                {{--  <div class="col-12" style="text-align:right;">
                    <button type="submit" class="btn btn-primary submit-action-btn">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>  --}}
            </div>
        </form>
    </div>
</div>