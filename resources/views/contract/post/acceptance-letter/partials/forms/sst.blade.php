<h3>Maklumat SST/LOA</h3>
<div class="row">
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mula Kerja'),
            'id' => 'start_working_date',
            'name' => 'start_working_date',
            'config' => ['format' => config('datetime.display.date')],
            'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))',
            'required'  => true,
        ])
    </div>
    @if($appointed->acquisition->approval->type->id == 2)
    <div class="col-4">
        @if($appointed->acquisition->category->name == 'Sebut Harga B' || $appointed->acquisition->category->name == 'Sebut Harga')
            @include('components.forms.select', [
                'input_label' => __('Tempoh Liabiliti Kecacatan (bulan)'),
                'id' => 'defects_liability_period',
                'name' => 'defects_liability_period',
                'options' => ['6' => '6', '12' => '12', '18' => '18', '24' => '24'],
                'required'  => true,                
            ])
        @else
            @include('components.forms.select', [
                'input_label' => __('Tempoh Liabiliti Kecacatan (bulan)'),
                'id' => 'defects_liability_period',
                'name' => 'defects_liability_period',
                'options' => ['6' => '6', '12' => '12', '18' => '18', '24' => '24'],
                'required'  => true,
            ])
        @endif
    </div>
    @endif
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => __('Tempoh Kerja'),
            'id' => 'work_period',
            'name' => 'work_period',
            'readonly' => true,
        ])
    </div>
   
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Siap Kerja'),
            'id' => 'end_working_date',
            'name' => 'end_working_date',
            'readonly' => true,
            'config' => ['format' => config('datetime.display.date')],
            'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))'
        ])    
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Surat'),
            'id' => 'letter_date',
            'name' => 'letter_date',
            'required'  => true,
            'config' => ['format' => config('datetime.display.date')],
            'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))'            
        ])

    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Elaun PROTEGE'),
            'id' => 'sl1m_allowance',
            'name' => 'sl1m_allowance'
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('PROTEGE (orang)'),
            'id' => 'sl1m_entity',
            'name' => 'sl1m_entity',
            'readonly' => true,
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
             'input_label' => __('Prime Rate (%)'),
             'id' => 'document_blr',
             'name' => 'document_blr',
             'note' => '* Sila hubungi BPUB untuk mendapatkan Prime Rate terkini',
             'required'  => true,
         ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
             'input_label' => __('Kadar LAD Sehari (RM)'),
             'id' => 'document_LAD_amount',
             'name' => 'document_LAD_amount',
             'readonly' => true,
         ])
    </div>
</div>
<div class="btn-group float-right">
    <a href="{{ route('acceptance-letter.index') }}" 
            class="btn btn-default border-primary">
            {{ __('Kembali') }}
    </a>
    <button type="submit" class="btn btn-primary submit-action-btn">
        @icon('fe fe-save') {{ __('Simpan') }}
    </button>
    @role('penyedia')
        @if(!empty($sst) && $sst->user_id == user()->id)
            @if(empty($review) || empty($review->status) || $review->status == 'CN')
                <button type="button" id="send" class="btn btn-default float-middle border-default">
                    @icon('fe fe-send') {{ __('Teratur') }}
                </button>
            @endif
        @endif
    @endrole
</div>