<h3>Maklumat Perolehan</h3>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            @include('components.forms.input', [
                'input_label' => __('No. Kontrak'),
                'id' => 'contract_no',
                'name' => 'contract_no',
                'required'  => true,
            ])
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <div class="text-muted">Tajuk Perolehan</div>
            <div>{!! $appointed->acquisition->title or "-" !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Rujukan Fail</div>
            <div>{{ $appointed->acquisition->approval->file_reference or "-" }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Harga Kontrak</div>
            <div>{!! money()->toHuman($appointed->offered_price) !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Nama Kontraktor</div>
            <div>{{ $appointed->company->company_name or "-" }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div>
                @if($appointed->acquisition->approval->mof == true)
                    @include('components.forms.select', [
                        'input_label' => __('Untuk Perhatian'),
                        'options' => company_owners_in_a_company_with_mof($appointed->company->id)->pluck('name', 'id'),
                        'id' => 'company_owner_id',
                        'name' => 'company_owner_id',
                        'required'  => true,
                    ])
                @else
                    @include('components.forms.select', [
                        'input_label' => __('Untuk Perhatian'),
                        'options' => company_owners_in_a_company_with_cidb($appointed->company->id)->pluck('name', 'id'),
                        'id' => 'company_owner_id',
                        'name' => 'company_owner_id',
                        'required'  => true,
                    ])
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Alamat Kontraktor</div>
            <div style="text-transform: capitalize">
                        {{--  {{$appointed->acquisition->approval->type->name}}  --}}
                @if($appointed->acquisition->approval->type->name == 'Kerja' || $appointed->acquisition->approval->type->name == 'Penyelenggaraan')
                    @if(count($cidb->addresses) > 0)
                        {{ $cidb->addresses[0]->primary .', '. $cidb->addresses[0]->secondary .', '. $cidb->addresses[0]->state .' '. $cidb->addresses[0]->postcode ?? "-" }}
                    @else
                        -
                    @endif
                @elseif($appointed->acquisition->approval->type->name == 'Bekalan' || $appointed->acquisition->approval->type->name == 'Perkhidmatan')
                    @if(count($mof->addresses) > 0)
                        {{ $mof->addresses[0]->primary .', '. $mof->addresses[0]->secondary .', '. $mof->addresses[0]->state .' '. $mof->addresses[0]->postcode ?? "-" }}
                    @else
                        -
                    @endif
                    
                @endif
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Lokasi</div>
            <div>{!! $appointed->acquisition->approval->locations_to_string or "-" !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">No. Telefon</div>
            <div>
                @foreach($appointed->company->phones as $phone)
                    @if($phone->phone_type_id == 3)
                        {{ $phone->phone_number or "-" }}
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">No. Faks</div>
            <div>
                @foreach($appointed->company->phones as $phone)
                    @if($phone->phone_type_id == 4)
                        {{ $phone->phone_number or "-" }}
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            {{--  <div class="text-muted">Jenis Laporan Cetakan</div>  --}}
            <div>
                @include('components.forms.select', [
                    'input_label' => __('Jenis Laporan Cetakan'),
                    'id' => 'report_type_id',
                    'name' => 'report_type_id',
                    'options' => $reportType,
                    'required'  => true,
                ])
            </div>
        </div>
    </div>
</div>
<div class="btn-group float-right">
    <a href="{{ route('acceptance-letter.index') }}" 
            class="btn btn-default border-primary">
            {{ __('Kembali') }}
    </a>
    <button type="submit" class="btn btn-primary submit-action-btn">
        @icon('fe fe-save') {{ __('Simpan') }}
    </button>
</div>

