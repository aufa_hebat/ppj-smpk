<h3>Maklumat Bon</h3>
<div class="row">
    <div class="col-12">
        @include('components.forms.input', [
            'input_label' => __('Jumlah Bon Pelaksanaan'),
            'id' => 'bon_amount',
            'name' => 'bon_amount',
            'readonly' => true,
        ])
    </div>
</div>