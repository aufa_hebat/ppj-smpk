<h3>Insurans Tanggungan Awam</h3>
<div class="row">
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Insurans'),
            'id' => 'ins_pli_start_date',
            'name' => 'ins_pli_start_date',
            'config' => ['format' => config('datetime.display.date')],
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Luput Insurans'),
            'id' => 'ins_pli_end_date',
            'name' => 'ins_pli_end_date',
            'config' => ['format' => config('datetime.display.date')],
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => __('Jumlah Insurans'),
            'id' => 'ins_pli_amount',
            'name' => 'ins_pli_amount',
            'readonly' => true,
        ])
    </div>
</div>
<br><hr><br>
<h3>Insurans Kerja</h3>
<div class="row">
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Insurans'),
            'id' => 'ins_work_start_date',
            'name' => 'ins_work_start_date',
            'config' => ['format' => config('datetime.display.date')],
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Luput Insurans'),
            'id' => 'ins_work_end_date',
            'name' => 'ins_work_end_date',
            'config' => ['format' => config('datetime.display.date')],
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => __('Jumlah Insurans'),
            'id' => 'ins_work_amount',
            'name' => 'ins_work_amount',
            'readonly' => true,
        ])
    </div>
</div>
<br><hr><br>
<h3>Insurans Pampasan Pekerja</h3>
<div class="row">
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Insurans'),
            'id' => 'ins_ci_start_date',
            'name' => 'ins_ci_start_date',
            'config' => ['format' => config('datetime.display.date')],
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Luput Insurans'),
            'id' => 'ins_ci_end_date',
            'name' => 'ins_ci_end_date',
            'config' => ['format' => config('datetime.display.date')],
            'readonly' => 'readonly'
        ])
    </div>
    <div class="col-4">
        @include('components.forms.input', [
            'input_label' => __('Jumlah Insurans'),
            'id' => 'ins_ci_amount',
            'name' => 'ins_ci_amount',
            'readonly' => true,
        ])
    </div>
</div>