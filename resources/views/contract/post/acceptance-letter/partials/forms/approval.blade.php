<h3>Maklumat Kelulusan</h3>
<div class="row">
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Tandatangan Presiden'),
            'id' => 'vp_sign_date',
            'name' => 'vp_sign_date',
            'config' => ['format' => config('datetime.display.date')],
        ])
    </div>
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Surat Setuju Terima Dikeluarkan'),
            'id' => 'al_issue_date',
            'name' => 'al_issue_date',
            'config' => ['format' => config('datetime.display.date')],
        ])
    </div>
    <div class="col-4">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Terima SST Dari Kontraktor'),
            'id' => 'al_contractor_date',
            'name' => 'al_contractor_date',
            'config' => ['format' => config('datetime.display.date')],
        ])
    </div>
</div>
