<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 100px 50px 70px 100px;
            }
    
            header {
                position: fixed;
                top: -70px;
                left: 0px;
                right: 0px;
                height: 250px;
    
                /** Extra personal styles **/
                /* background-color: #03a9f4; */

                color: black;
                text-align: right;

                /* line-height: 35px; */
            }
    
            footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
    
                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;                    
            }
    
            footer .pagenum:before {
                content: counter(page);
            }
    
            div.breakNow { page-break-inside:avoid; page-break-after:always; }
    
            .title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
                
            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
                
            .title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
    
            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }
    
        </style>
    </head>
    <body>
    
        @php
            $acqCategory = "";
            
            if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
                $acqCategory = "sebut harga";
            }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
                $acqCategory = "tender";
            }
        @endphp
        
        <!-- Define header and footer blocks before your content -->
        <header>
            <div style="text-align: right; " class="title-header">
    
                @if($sst->company->gst_registered_no != '')
                    <span>SST Bekalan Dengan Dokumen Kontrak</span><br>
                    <span>(Syarikat Yang <b>Telah Berdaftar</b> dengan JKDM) Dan</span><br/>
                    <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span><br/>
                @else
                    <span>SST Bekalan Dengan Dokumen Kontrak</span><br>
                    <span>(Syarikat Yang <b>Belum Berdaftar</b> dengan JKDM) Dan</span><br/>
                    <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span><br/>
                @endif                           
            </div>
                
            <div style="text-align: left;" class="content">
                <span style="text-transform:capitalize; font-weight:bold;">*No. {{ $acqCategory }}</span> : {{ $sst->acquisition->reference }}
            </div>
            <br/><br/>
        </header>
    
        <footer>
            <div class="pagenum-container title-footer">Page <span class="pagenum"></span></div>
        </footer>
    
        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
            <br/><br/>
    
            @include('contract.post.acceptance-letter.report.partials.bekalan.sub-report.sec_info', ['sst' => $sst])
    
            @if($sst->company->gst_registered_no != '')
                @include('contract.post.acceptance-letter.report.partials.bekalan.sec_daftar_tiada_pelepasan', ['sst' => $sst, 'appointed' => $appointed])
            @else
                @include('contract.post.acceptance-letter.report.partials.bekalan.sec_belum_daftar_tiada_pelepasan', ['sst' => $sst, 'appointed' => $appointed])                
            @endif
    
            <br/>
    
            @include('contract.post.acceptance-letter.report.partials.bekalan.sub-report.sec_sk', ['sst' => $sst])
    
            <div class="breakNow"><br></div>
    
            @include('contract.post.acceptance-letter.report.partials.bekalan.sub-report.sec_akuan_terima', ['sst' => $sst])
    
            <div class="breakNow"><br></div>
    
            @include('contract.post.acceptance-letter.report.partials.bekalan.sub-report.sec_butiran', ['sst' => $sst])
    
            <div class="breakNow"><br></div>
    
            @include('contract.post.acceptance-letter.report.partials.bekalan.sub-report.sec_akuan_pembida', ['sst' => $sst])
    
            <div class="breakNow"><br></div>
    
            @include('contract.post.acceptance-letter.report.partials.bekalan.sub-report.sec_akuan_sumpah', ['sst' => $sst])
    
        </main>
    </body>
</html>