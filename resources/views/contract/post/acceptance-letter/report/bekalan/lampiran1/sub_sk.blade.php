<div class="content">s.k</div>
<div>
    <table width="100%" class="content">
		<tr>
			<td width="10%" valign="top">1.</td>
			<td width="90%" valign="top">Setiausaha<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">2.</td>
			<td valign="top">Naib Presiden (Kewangan)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">3.</td>
			<td valign="top">Naib Presiden (Audit dan Kualiti Asurans)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">4.</td>
			<td valign="top">Pengarah (Bahagian {{ $sst->department->name }})<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">5.</td>
			<td valign="top">Pengarah (Bahagian Perolehan dan Ukur Bahan)<br/>Jabatan Kewangan<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">6.</td>
			<td valign="top">
				Ketua Setiausaha Perbendaharaan <br/>
				Bahagian Perolehan Kerajaan<br>
				Aras 4 Blok Utara<br/>
				Kementerian Kewangan<br/>
				Presint 2<br/>
				62592 Putrajaya<br/>
				(u.p.: Ketua Seksyen Dasar Perolehan)
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">7.</td>
			<td valign="top">
				Ketua Pegawai Eksekutif<br/>
				Ibu Pejabat Lembaga Hasil Dalam Negeri Malaysia<br/>
				Menara Hasil Aras 18<br/>
				Persiaran Rimba Permai, CYber 8<br/>
				63000 Cyberjaya<br/>
				(u.p.: Pengarah Jabatan Pematuhan Cukai)
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">8.</td>
			<td valign="top">
				Professional Training & Education For Growing Entrepreneurs (PROTEGE)<br/>
				Aras 2, Blok E4/5, Parcel E<br/>
				Kementerian Pembangunan Usahawan<br/>
				Pusat Pentadbiran Kerajaan Persekutuan<br/>
				62668 Putrajaya<br/>
				(u.p.: Ketua Sekretariat PROTEGE)
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">9.</td>
			<td valign="top">
				Ketua Pengarah Kastam<br/>
				Ibu Pejabat Kastam Diraja Malaysia<br/>
				Bahagian Cukai Dalam Negeri (SST)<br/>
				Aras 3-7, Blok A, Menara Tulus<br/>
				No. 22, Persiaran Perdana, Presint 3<br/>
				61200 Putrajaya<br/>
				(u.p.: Pengarah Bahagian Cukai Dalam Negeri (SST))
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">10.</td>
			<td valign="top">
				Sekretariat<br/>
				Majlis Perundingan Gaji Negara<br/>
				Kementerian Sumber Manusia<br/>
				Aras 7, Blok D3, Kompleks D<br/>
				62530 Putrajaya
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">11.</td>
			<td valign="top">
				Perkeso Kajang<br/>
				No. 15, Jalan Hentian 1A<br/>
				Pusat Hentian Kajang<br/>
				Jalan Reko, 43000 Kajang<br/>
				(u.p.: Unit Penguatkuasaan)
			</td>
        </tr>	
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">12.</td>
			<td valign="top">
				Sime Darby Lockton Insurance Broker Sdn. Bhd.<br/>
				19th Floor, Menara JKG<br/>
				No. 282, Jalan Raja Laut<br/>
                PO Box 12355<br/>
                50774 Kuala Lumpur
			</td>
		</tr>        																																																						
	</table>
</div>
