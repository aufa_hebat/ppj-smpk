@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp

<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
    Lampiran A
</div>
<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
    Butiran Kontrak<br/><br/>
    {{ $sst->acquisition->title }}
</div>

<div>
    <table width="100%" class="content" >
        <tr>
            <td width="7%" valign="top" >1.</td>
            <td width="93%" valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran Syarikat Dengan Suruhanjaya Syarikat Malaysia (SSM) Atau Pendaftaran Koperasi Dengan Suruhanjaya Koperasi Malaysia (SKM) (jika berkaitan)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">1.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->ssm_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>1.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->ssm_start_date) && !empty($sst->company->ssm_end_date)){{date('d/m/Y', strtotime($sst->company->ssm_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->ssm_end_date))}}@endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >2.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Kementerian Kewangan (jika berdaftar)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">2.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->mof_no }}</td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>2.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->mof_start_date) && !empty($sst->company->mof_end_date))
                                {{date('d/m/Y', strtotime($sst->company->mof_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->mof_end_date))}}
                            @endif
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>2.3</td>
                        <td>Kod Bidang</td>
                        <td>:
                            @foreach($sst->acquisition->approval->mofQualifications as $qualification)
                                @if('khusus' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach 

                            {{-- @if(!empty($sst->company->syarikatMOFBidang)) 
                                @foreach($sst->company->syarikatMOFBidang as $bidang)
                                    {{$bidang->areas}}   
                                @endforeach 
                            @endif --}}
                        </td>
                    </tr>		
                    <tr valign="top">
                        <td></td>
                        <td>2.4</td>
                        <td>Taraf Syarikat</td>
                        <td>:
                            @if(!empty($sst->company->mof_no))
                                @if(!empty($sst->company->bumiputra_no))Bumiputera @else Bukan Bumiputera @endif
                            @endif
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>2.5</td>
                        <td>Tempoh Sah Laku Taraf Bumiputera</td>
                        <td>:
                            @if(!empty($sst->company->mof_no))
                                @if(!empty($sst->company->bumiputera_start_date) && !empty($sst->company->bumiputera_end_date))
                                    {{date('d/m/Y', strtotime($sst->company->bumiputera_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->bumiputera_end_date))}}
                                @endif
                            @endif
                        </td>
                    </tr>																						
                </table>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >3.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran Cukai Jualan dengan Jabatan Kastam Diraja Malaysia (jika berdaftar), sekiranya berkaitan
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">3.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->gst_registered_no }}</td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>3.2</td>
                        <td>Tarikh Kuatkuasa</td>
                        <td>: </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >4.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Harga dan Tempoh Kontrak
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">4.1</td>
                        <td width="25%">Harga {{ $acqCategory }} (butiran harga seperti di <span style="font-style:italic;font-weight:bold;">Lampiran A1</span>)</td>
                        <td width="68%">: {{ money()->toHuman($appointed->offered_price) }}</td>
                    </tr>
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">4.2</td>
                        <td width="25%"Peruntukan Cukai Jualan (sekiranya berkaitan)</td>
                        <td width="68%">: </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>4.3</td>
                        <td>Fi Perkhidmatan e-Perolehan (sekiranya berkaitan)</td>
                        <td>: </td>
                    </tr>	
                    <tr valign="top">
                        <td></td>
                        <td>4.4</td>
                        <td>Harga Kontrak</td>
                        <td>:
                            {{ money()->toHuman($appointed->offered_price) }}
                        </td>
                    </tr>  
                    <tr valign="top">
                        <td></td>
                        <td>4.5</td>
                        <td>Tempoh Kontrak</td>
                        <td>:
                            {!! $sale->box->period. ' ' . $sale->box->period_type->name !!}
                        </td>
                    </tr>    
                    <tr valign="top">
                        <td></td>
                        <td>4.6</td>
                        <td>Tarikh Mula Kontrak</td>
                        <td>:
                            {{ \Carbon::intlFormat($sst->start_working_date) }}
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>4.7</td>
                        <td>Tarikh Tamat Kontrak</td>
                        <td>:
                            {{ \Carbon::intlFormat($sst->end_working_date) }}
                        </td>
                    </tr>                                                                                           																							
                </table>							
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >5.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Tempoh dan Jadual Pembekalan Barang 
                </span>
                <br/>
                *Senarai item, kuantiti dan/atau tempoh serta jadual pembekalan yang ditetapkan seperti di 
                <span style="font-weight:bold; font-style:italic;">Lampiran A2</span>
                <br/>
                <span style="color:grey; font-style:italic;"><span style="font-weight:bold;">Lampiran A2</span>
                adalah untuk menyatakan tempoh atau jadual pembekalan mengikut tawaran syarikat dan disediakan 
                oleh Jabatan Pelaksana.</span>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >6.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Spesifikasi Pembekalan (<span style="font-style:italic">sekiranya berkaitan</span>)
                </span>
                <br/>
                *Spesifikasi pembekalan yang ditetapkan seperti di 
                <span style="font-weight:bold; font-style:italic;">Lampiran A3</span>
                <br/>
                <span style="color:grey; font-style:italic;"><span style="font-weight:bold;">Lampiran A3</span>
                akan disediakan oleh Jabatan Pelaksana.</span>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >7.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    *Bon Pelaksanaan 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">7.1</td>
                        <td width="30%">Kadar Bon Pelaksanaan</td>
                        <td width="63%">: 
                            @php
                                $periodType = $sale->box->period_type->id;
                                $period = $sale->box->period;
                                $amount = $appointed->offered_price;
                                $amountPerYear = 0.0;
                                $percentage = '';

                                if($periodType == 4 && $period >= 2){
                                    $amountPerYear = $amount/$period;
                                }else if($periodType == 3 && $period >= 24){
                                    $amountPerYear = ($amount/($period/12));
                                }else if($periodType == 2 && $period >= 104){
                                    $amountPerYear = ($amount/($period/52));
                                }else if($periodType == 1 && $period >= 730){
                                    $amountPerYear = ($amount/($period/365));
                                }else{
                                    $amountPerYear = $amount;
                                }

                                if($amount >= 20000000 && $amount < 50000000){
                                    $percentage = "2.5%";
                                }else if($amount >= 50000000){
                                    $percentage = "5%";
                                }else{
                                    $percentage = "0%";
                                }
                            @endphp
                            {{ $percentage }} dari nilai kontrak
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>7.2</td>
                        <td>Formula Bon Pelaksanaan</td>
                        <td>: 
                            {{ $percentage }}  x  {{ money()->toHuman($appointed->offered_price) }}
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>7.3</td>
                        <td>Nilai Bon Pelaksanaan</td>
                        <td>: {{ isset($sst->bon_amount) ? money()->toHuman($sst->bon_amount) : "-" }}</td>
                    </tr>		
                    <tr valign="top">
                        <td></td>
                        <td>7.4</td>
                        <td>Bentuk Bon Pelaksanaan</td>
                        <td style="text-align: justify;">: 
                            <span style="text-align: justify;">Jaminan Bank / Jaminan Syarikat Kewangan / Jaminan Insurans / Takaful / Deraf Bank atas nama PERBADANAN PUTRAJAYA</span>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>7.5</td>
                        <td>Tempoh Sah Laku</td>
                        <td>:  
                            {{date('d/m/Y', strtotime($sst->start_working_date))}} hingga {{date('d/m/Y', strtotime($sst->end_working_date->addMonths(12)))}}<br/><br/>
                            *Dari tarikh kuat kuasa kontrak sehingga 12 bulan selepas tarikh tamat Laku kontrak atau
                            tarikh obligasi terakhir mengikut mana yang terkemudian<br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">* Mengikut format yang ditetapkan oleh Perbadanan.
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >8.</td>	
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans (jika berkaitan) 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td></td>
                        <td>8.1</td>
                        <td>Nilai Polisi</td>
                        <td style="text-align: justify;">: {{ isset($sst->ins_work_amount) ? money()->toHuman($sst->ins_work_amount) : "-" }}</td>
                    </tr>   
                    <tr valign="top">
                        <td></td>
                        <td>8.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td style="text-align: justify;">: 
                            @if(!empty($sst->ins_work_start_date) && !empty($sst->ins_work_end_date)){{date('d/m/Y', strtotime($sst->ins_work_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_work_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak</span>
                        </td>
                    </tr>                                      																																																		
                </table>														
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >9.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    Kenaan *Denda/ Tolakan/ <i>Liquidated &amp; Ascertained Damages (LAD)</i> 
                </span>
                <br/>							
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">9.1</td>
                        <td width="30%">Formula</td>
                        <td width="63%">:
                            <span style="text-decoration: underline; font-weight: bold;">{{ isset($sst->document_blr) ? money()->toCommon($sst->document_blr) : "0" }} %  x  {{ money()->toHuman($appointed->offered_price) }}</span><br/>
                            <span valign="top" style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;365 hari</span>
                                
                            <br/><br/>
        
                            <span style="text-decoration: underline; font-weight: bold;">PR (Prime Rate) %  x  Harga Kontrak</span><br/>
                            <span valign="top" style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;365 hari</span>                        
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td valign="top" >10.</td>
            <td valign="top" >
                <span style="font-weight: bold; text-decoration: underline;">
                    *Professional Training And Education For Growing Entrepreneurs (PROTEGE) (jika berkaitan) 
                </span>
                <br/>
                (Bagi perolehan yang telah melebihi nilai ambang berdasarkan 1PP/PK1.2)
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">10.1</td>
                        <td width="30%">Tertakluk Kepada pelaksanaan Program PROTEGE</td>
                        <td width="63%">:
                            @if($sst->sl1m_entity == 0)
                                Tidak
                            @else
                                Ya
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>10.2</td>
                        <td>Bilangan Minimum Peserta PROTEGE</td>
                        <td>:  @if($sst->sl1m_entity == 0)
                                    Tiada
                                @else
                                    {{ $sst->sl1m_entity }} peserta
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" style="font-weight: bold;">
                            Formula Pengiraan
                        </td>
                        <td>
                            <span style="text-decoration: underline; font-weight: bold;">1 %  x  Harga Kontrak</span><br/>
                            <span valign="top" style="text-align: center; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RM24,000</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <span  style="font-weight: bold;font-size: 10px; color:grey"  align="center;">
                                *Bagi tujuan pengiraan PROTEGE, Harga Kontrak adalah nilai tawaran yang dipersetujui<br/>
                                **Elaun PROTEGE (RM2,000 seorang x 12 bulan)<br/><br/>
                            </span>                                
                        </td>									
                    </tr>																																																																			
                </table>							
            </td>
        </tr>																																																	
    </table>
</div>