@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp

<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
    Lampiran A
</div>

<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
    Butiran Kontrak<br/><br/>
    {{ $sst->acquisition->title }}
</div>

<div>
    <table width="100%" class="content">
        <tr>
            <td width="7%" valign="top" style="padding:1 1;">1.</td>
            <td width="93%" valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran Syarikat Dengan Suruhanjaya Syarikat Malaysia (SSM) Atau Pendaftaran Koperasi Dengan Suruhanjaya Koperasi Malaysia (SKM) (jika berkaitan)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">1.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->ssm_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>1.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->ssm_start_date) && !empty($sst->company->ssm_end_date))
                                {{date('d/m/Y', strtotime($sst->company->ssm_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->ssm_end_date))}}
                            @elseif(!empty($sst->company->ssm_start_date) && empty($sst->company->ssm_end_date))
                                {{date('d/m/Y', strtotime($sst->company->ssm_start_date))}}                             
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">2.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB) Di Bawah Perakuan Pendaftaran Kontraktor
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">2.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->cidb_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>2.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->cidb_start_date) && !empty($sst->company->cidb_end_date)){{date('d/m/Y', strtotime($sst->company->cidb_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->cidb_end_date))}}@endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>2.3</td>
                        <td>Gred</td>
                        <td>:

                                    @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                        @if('grade' == $qualification->code->type)
                                            {{ $qualification->code->code }}
                                        @endif
                                    @endforeach

                            {{--  @if(!empty($sst->company->syarikatCIDBgreds)) 
                                @foreach($sst->company->syarikatCIDBgreds as $gred)
                                    {{$gred->grades}}   
                                @endforeach 
                            @endif  --}}
                        </td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td>2.4</td>
                        <td>Kategori</td>
                        <td>:

                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('category' == $qualification->code->type)
                                {{ $qualification->code->code }}
                                @endif
                            @endforeach                        
                            {{--  @if(!empty($sst->company->syarikatCIDBkategori)) 
                                @foreach($sst->company->syarikatCIDBkategori as $kategori)
                                    {{$kategori->categories}}   
                                @endforeach 
                            @endif  --}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>2.5</td>
                        <td>Pengkhususan</td>
                        <td>:

                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('khusus' == $qualification->code->type)
                                {{ $qualification->code->code }}
                                @endif
                            @endforeach                        
                            {{--  @if(!empty($sst->company->syarikatCIDBkategori)) 
                                @foreach($sst->company->syarikatCIDBkategori as $kategori)                                 
                                    @if(!empty($kategori->syarikatCIDBKhusus)) 
                                        @foreach($kategori->syarikatCIDBKhusus as $khusus)
                                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                                @if('khusus' == $qualification->code->type)
                                                    @if($khusus->khusus == $qualification->code->code)
                                                        {{$khusus->khusus}} 
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endforeach 
                                    @endif 
                                @endforeach 
                            @endif   --}}
                        </td>
                    </tr>																						
                </table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">3.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB) Di Bawah Sijil Perolehan Kerja Perbadanan (jika berdaftar)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">3.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{$sst->company->spkk_no}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>3.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: @if(!empty($sst->company->spkk_start_date) && !empty($sst->company->spkk_end_date)){{date('d/m/Y', strtotime($sst->company->spkk_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->spkk_end_date))}}@endif</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>3.3</td>
                        <td>Gred</td>
                        <td>:

                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('grade' == $qualification->code->type)
                                {{ $qualification->code->code }}
                                @endif
                            @endforeach                        
                            {{--  @if(!empty($sst->company->syarikatCIDBgreds)) 
                            @foreach($sst->company->syarikatCIDBgreds as $gred)
                                {{$gred->grades}}   
                            @endforeach 
                            @endif  --}}
                        </td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td>3.4</td>
                        <td>Kategori</td>
                        <td>:
                            
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('category' == $qualification->code->type)
                                {{ $qualification->code->code }}
                                @endif
                            @endforeach
                            {{--  @if(!empty($sst->company->syarikatCIDBkategori)) 
                            @foreach($sst->company->syarikatCIDBkategori as $kategori)
                                {{$kategori->categories}}   
                            @endforeach 
                        @endif                          --}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>3.5</td>
                        <td>Pengkhususan</td>
                        <td>: 

                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('khusus' == $qualification->code->type)
                                {{ $qualification->code->code }}
                                @endif
                            @endforeach                        
                            {{--  @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('khusus' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach  --}}

                            {{--  @if(!empty($sst->company->syarikatCIDBkategori)) 
                                @foreach($sst->company->syarikatCIDBkategori as $kategori)                                 
                                    @if(!empty($kategori->syarikatCIDBKhusus)) 
                                        @foreach($kategori->syarikatCIDBKhusus as $khusus)
                                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                                @if('khusus' == $qualification->code->type)
                                                    @if($khusus->khusus == $qualification->code->code)
                                                        {{$khusus->khusus}} 
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endforeach 
                                    @endif 
                                @endforeach 
                            @endif                          --}}
                        </td>
                    </tr>																						
                </table>							
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">4.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Bahagian Pembangunan Kontraktor dan Usahawan (BPKU) (Sijil Bumiputera) (jika berdaftar) 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">4.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->bpku_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>4.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: @if(!empty($sst->company->bpku_start_date) && !empty($sst->company->bpku_end_date)){{date('d/m/Y', strtotime($sst->company->bpku_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->bpku_end_date))}}@endif</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>4.3</td>
                        <td>Gred Kontraktor</td>
                        <td>:
                            @if(!empty($sst->company->syarikatCIDBgreds)) 
                                @foreach($sst->company->syarikatCIDBgreds as $gred)
                                    {{$gred->grades}}   
                                @endforeach 
                            @endif
                        </td>
                    </tr>																								
                </table>							
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">5.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran Cukai Perkhidmatan dengan Jabatan Kastam Diraja Malaysia (jika berdaftar) sekiranya berkaitan 
                </span>
                <br/>
                <table width="100%" style="padding:5 5;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">5.1</td>
                        <td width="30%">No. Pendaftaran</td>
                        <td width="63%">: {{ $sst->company->gst_registered_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>5.2</td>
                        <td>Tarikh Kuat Kuasa</td>
                        <td>: 
                            @if(!empty($sst->company->gst_start_date))
                                {{date('d/m/Y', strtotime($sst->company->gst_start_date))}} 
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">6.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Harga dan Maklumat Kontrak 
                </span>
                <br/>
                <table width="100%" style="padding:5 5;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">6.1</td>
                        <td width="30%">Harga <span style="text-transform: capitalize;">{{ $acqCategory }}</span> (butiran harga seperti di 
                            <span style="font-weight:bold; font-style:italic">Lampiran A1)</span>

                        </td>
                        <td width="63%">: {{ money()->toHuman($appointed->offered_price) }}</td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>6.2</td>
                        <td>Peruntukan Cukai Perkhidmatan (sekiranya berkaitan)</td>
                        <td>: 
                            @php
                                $cukai = 0.0;
                                $percent = 0.06;
                                $cukai = $percent * $appointed->offered_price;
                            @endphp

                            {{ money()->toHuman($cukai,0,2) }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>6.3</td>
                        <td>Harga Kontrak</td>
                        <td>: {{ money()->toHuman($appointed->offered_price + $cukai) }}</td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td>6.4</td>
                        <td>Tempoh Kontrak</td>
                        <td>: 
                            {!! $sale->box->period. ' ' . $sale->box->period_type->name !!}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>6.5</td>
                        <td>Tarikh Mula Kontrak</td>
                        <td>: 
                            <b>{{ \Carbon::intlFormat($sst->start_working_date) }}</b>
                        </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td>6.6</td>
                        <td>Tarikh Tamat Kontrak</td>
                        <td>: 
                            <b>{{ \Carbon::intlFormat($sst->end_working_date) }}</b>
                        </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td>6.7</td>
                        <td>Tempoh Liabiliti Kecacatan</td>
                        <td>: Tidak berkaitan
                            {{--  {{ isset($sst->defects_liability_period) ? $sst->defects_liability_period : "0"}} bulan  --}}
                        </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td>6.8</td>
                        <td>Lokasi Kerja</td>
                        <td>: {!! $sst->acquisition->approval->locations_to_string or '-' !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>6.9</td>
                        <td>Wakil Perbadanan</td>
                        <td>:  
                                Naib Presiden Jabatan {{ $sst->department->name }}
                            {{--  @if(!empty($np_dept))
                            {{$np_dept->honourary}} {{ $np_dept->name }}
                            @endif  --}}
                        </td>
                    </tr>																																																			
                </table>							
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">7.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Bon Pelaksanaan 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">7.1</td>
                        <td width="30%">Kadar Bon Pelaksanaan</td>
                        <td width="63%">: 
                            @php
                                $periodType = $sale->box->period_type->id;
                                $period = $sale->box->period;
                                $amount = $appointed->offered_price;
                                $amountPerYear = 0.0;
                                $percentage = '';

                                if($periodType == 4 && $period >= 2){
                                    $amountPerYear = $amount/$period;
                                }else if($periodType == 3 && $period >= 24){
                                    $amountPerYear = ($amount/round(($period/12)));
                                }else if($periodType == 2 && $period >= 104){
                                    $amountPerYear = ($amount/round(($period/52)));
                                }else if($periodType == 1 && $period >= 730){
                                    $amountPerYear = ($amount/round(($period/365)));
                                }else{
                                    $amountPerYear = $amount;
                                }



                                if($amountPerYear >= 20000000 && $amountPerYear < 50000000){
                                    $percentage = "2.5%";
                                }else if($amountPerYear >= 50000000){
                                    $percentage = "5%";
                                }else{
                                    $percentage = "0%";
                                }
                            @endphp
                            {{ $percentage }} dari nilai {{ $acqCategory }}

                            {{--  @if($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 3)
                                2.5%
                            @elseif($sst->acquisition->approval->acquisition_type_id == 2)
                                5%
                            @elseif($sst->acquisition->approval->acquisition_type_id == 4)
                                5%
                            @endif 
                            dari nilai kontrak  --}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>7.2</td>
                        <td>Formula Bon Pelaksanaan</td>
                        <td>: 
                            @php 
                                $periodInYear = 0.0;

                                if($periodType == 4 && $period >= 2){
                                    $periodInYear = $period;
                                }else if($periodType == 3 && $period >= 24){
                                    $periodInYear = round(($period/12));
                                }else if($periodType == 2 && $period >= 104){
                                    $periodInYear = round(($period/52));
                                }else if($periodType == 1 && $period >= 730){
                                    $periodInYear = round(($period/365));
                                }

                            @endphp                            
                            
                            {{ $percentage }} x ({{ money()->toHuman($appointed->offered_price,0,2) }} / {!! $periodInYear !!})
                            {{--  @if($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 3)
                                2.5%
                            @elseif($sst->acquisition->approval->acquisition_type_id == 2)
                                5%
                            @elseif($sst->acquisition->approval->acquisition_type_id == 4)
                                5%
                            @endif  
                            x  {{ money()->toHuman($appointed->offered_price) }}  --}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>7.3</td>
                        <td>Nilai Bon Pelaksanaan</td>
                        <td>: {{ isset($sst->bon_amount) ? money()->toHuman($sst->bon_amount) : "-" }}</td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td valign="top">7.4</td>
                        <td valign="top">Bentuk Bon Pelaksanaan</td>
                        <td valign="top"  style="text-align: justify;">: <span style="text-align: justify;">Jaminan Bank / Jaminan Syarikat Kewangan / Jaminan Insurans / Takaful / Deraf Bank 
                            atas nama PERBADANAN PUTRAJAYA.</span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">7.5</td>
                        <td valign="top">Tempoh Sah Laku</td>
                        <td valign="top">:  
                            {{--  @if(!empty($bon->bank_start_date) && !empty($bon->bank_end_date)){{date('d/m/Y', strtotime($bon->bank_start_date))}} hingga {{date('d/m/Y', strtotime($bon->bank_end_date))}}@endif<br/><br/>  --}}
                            {{date('d/m/Y', strtotime($sst->ins_pli_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_pli_end_date->addMonths(12)))}} <br/><br/>

                            *Dari tarikh kuat kuasa kontrak sehingga 12 bulan selepas tamat tempoh kontrak.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">*Mengikut format yang ditetapkan oleh Perbadanan.
                    </tr>																																																			
                </table>														
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        {{--  <tr>
            <td valign="top" style="padding:1 1;">8.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans Kerja 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">8.1</td>
                        <td width="30%">Nilai Polisi</td>
                        <td width="63%">: {{ isset($sst->ins_work_amount) ? money()->toHuman($sst->ins_work_amount) : "-" }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>8.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td>:
                            @if(!empty($sst->ins_work_start_date) && !empty($sst->ins_work_end_date)){{date('d/m/Y', strtotime($sst->ins_work_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_work_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak</span>
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>  --}}
        {{--  <tr>
            <td valign="top" style="padding:1 1;">9.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans Tanggungan Awam 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">9.1</td>
                        <td width="30%">Nilai Polisi</td>
                        <td width="63%">: {{ isset($sst->ins_pli_amount) ? money()->toHuman($sst->ins_pli_amount) : "-" }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>9.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td>:
                            @if(!empty($sst->ins_pli_start_date) && !empty($sst->ins_pli_end_date)){{date('d/m/Y', strtotime($sst->ins_pli_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_pli_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak, tempoh tanggungan kecacatan dan 3 bulan 14 hari.</span>
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>  --}}
        {{--  <tr>
            <td valign="top" style="padding:1 1;">10.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans Pampasan Pekerja 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">10.1</td>
                        <td width="30%">Nilai Polisi</td>
                        <td width="63%">: {{ isset($sst->ins_ci_amount) ? money()->toHuman($sst->ins_ci_amount) : "-" }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>10.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td>:
                            @if(!empty($sst->ins_ci_start_date) && !empty($sst->ins_ci_end_date)){{date('d/m/Y', strtotime($sst->ins_ci_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_ci_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak, tempoh tanggungan kecacatan dan 3 bulan 14 hari.</span>
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>  --}}
        <tr>
            <td valign="top" style="padding:1 1;">8.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Kenaan Liquidated &amp; Ascertained Damages (LAD) / Penalti
                </span>
                <br/>	
                <span>
                    (Lewat menyiapkan kerja mengikut jadual yang ditetapkan)
                </span>
                <table width="100%" style="padding:5px 5px;" class="content">
                    {{--  <tr>
                        <td width="2%"></td>
                        <td width="5%">8.1</td>
                        <td width="30%">Formula</td>
                        <td width="63%">:
                            <span style="text-decoration: underline; font-weight: bold;">PR (Prime Rate) %  x  Harga Kontrak</span><br/>
                            <span valign="top" style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;365 hari</span>
                        </td>
                    </tr>  --}}
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">8.1</td>
                        <td width="30%">Kadar </td>
                        <td width="63%">: 
                            {{--  {{ money()->toHuman($sst->document_LAD_amount) }}  --}}
                            Rujuk SLA
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td valign="top" style="padding:1 1;">9.</td>
            <td valign="top" style="padding:1 1;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Professional Training And Education For Growing Entrepreneurs (PROTEGE) (jika berkaitan) 
                </span>
                <br/>
                <span>
                    (Bagi perolehan yang telah melebihi nilai ambang berdasarkan 1PP/PK1.2.)
                </span>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr valign="top">
                        <td width="2%"></td>
                        <td width="5%">9.1</td>
                        <td width="30%">Tertakluk Kepada pelaksanaan Program PROTEGE</td>
                        <td width="63%">:
                            @if($priceWithoutWps >= 1000000000)
                                Ya
                            @else
                                Tidak
                            @endif
                            (Berdasarkan nilai ambang (melebihi 10 Juta) yang ditetapkan dalam 1PP/PK1)
                        </td>
                    </tr>
                    <tr valign="top">
                        <td></td>
                        <td>9.2</td>
                        <td>Bilangan Minimum peserta PROTEGE</td>
                        <td>:  @if($sst->sl1m_entity == 0)
                                    Tiada
                                @else
                                    {{ $sst->sl1m_entity }} peserta
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            Formula Pengiraan
                        </td>
                        <td>
                            <span style="text-decoration: underline;">1 %  x  Harga Kontrak*</span><br/>
                            <span valign="top" style="text-align: center; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RM24,000**</span>
                        </td>
                    </tr>	
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <span  style="font-weight: bold;font-size: 10px; color:grey"  align="center;">
                                *Bagi tujuan pengiraan PROTEGE, Harga Kontrak adalah harga kerja pembina tanpa cukai<br/>
                                **Elaun PROTEGE (RM2,000 seorang x 12 bulan)<br/><br/>
                            </span>
                            {{-- <span align="left">
                            * Meliputi tempoh kontrak, tempoh tanggungan kecacatan dan 3 Bulan 14 hari
                            </span> --}}
                        </td>									
                    </tr>
                </table>							
            </td>
        </tr>																																																		
    </table>
</div>