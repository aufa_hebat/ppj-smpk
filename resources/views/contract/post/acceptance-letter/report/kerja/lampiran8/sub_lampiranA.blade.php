@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp

<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
    Lampiran A
</div>
<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
    Butiran Kontrak<br/><br/>
    {{ $sst->acquisition->title }}
</div>
    
<div>
    <table width="100%" class="content">
        <tr>
            <td width="7%" valign="top">1.</td>
            <td width="93%" valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran Syarikat Dengan Suruhanjaya Syarikat Malaysia (SSM) Atau Pendaftaran Koperasi Dengan Suruhanjaya Koperasi Malaysia (SKM) (jika berkaitan)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">1.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="65%">: {{ $sst->company->ssm_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td  valign="top">1.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->ssm_start_date) && !empty($sst->company->ssm_end_date)){{date('d/m/Y', strtotime($sst->company->ssm_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->ssm_end_date))}}@endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">2.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB) Di Bawah Perakuan Pendaftaran Kontraktor
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">2.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="65%">: {{ $sst->company->cidb_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">2.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->cidb_start_date) && !empty($sst->company->cidb_end_date)){{date('d/m/Y', strtotime($sst->company->cidb_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->cidb_end_date))}}@endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">2.3</td>
                        <td>Gred</td>
                        <td>:
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('grade' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach                            
                            {{-- @if(!empty($sst->company->syarikatCIDBgreds)) 
                                @foreach($sst->company->syarikatCIDBgreds as $gred)
                                    {{$gred->grades}}   
                                @endforeach 
                            @endif --}}
                        </td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td valign="top">2.4</td>
                        <td>Kategori</td>
                        <td>:
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('category' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach                             
                            {{-- @if(!empty($sst->company->syarikatCIDBkategori)) 
                                @foreach($sst->company->syarikatCIDBkategori as $kategori)
                                    {{$kategori->categories}}   
                                @endforeach 
                            @endif --}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">2.5</td>
                        <td>Pengkhususan</td>
                        <td>:
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('khusus' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach                             
                            {{-- @if(!empty($sst->company->syarikatCIDBkategori)) 
                                @foreach($sst->company->syarikatCIDBkategori as $kategori) 
                                    @if(!empty($kategori->syarikatCIDBKhusus)) 
                                        @foreach($kategori->syarikatCIDBKhusus as $khusus)
                                            {{$khusus->khusus}} 
                                        @endforeach 
                                    @endif 
                                @endforeach 
                            @endif --}}
                        </td>
                    </tr>																						
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">3.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB) Di Bawah Sijil Perolehan Kerja Perbadanan (jika berdaftar)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">3.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="65%">: {{$sst->company->spkk_no}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">3.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: @if(!empty($sst->company->spkk_start_date) && !empty($sst->company->spkk_end_date)){{date('d/m/Y', strtotime($sst->company->spkk_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->spkk_end_date))}}@endif</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">3.3</td>
                        <td>Gred</td>
                        <td>:
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('grade' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach                                                        
                        </td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td valign="top">3.4</td>
                        <td>Kategori</td>
                        <td>:
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('category' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach                            
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">3.5</td>
                        <td>Pengkhususan</td>
                        <td>:
                            @foreach($sst->acquisition->approval->cidbQualifications as $qualification)
                                @if('khusus' == $qualification->code->type)
                                    {{ $qualification->code->code }}
                                @endif
                            @endforeach                            
                        </td>
                    </tr>																						
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top">4.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran dengan Bahagian Pembangunan Kontraktor dan Usahawan (BPKU) (Sijil Bumiputera) (jika berdaftar) 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">4.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="65%">: {{ $sst->company->bpku_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">4.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: @if(!empty($sst->company->bpku_start_date) && !empty($sst->company->bpku_end_date)){{date('d/m/Y', strtotime($sst->company->bpku_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->bpku_end_date))}}@endif</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">4.3</td>
                        <td>Gred Kontraktor</td>
                        <td>:
                            @if(!empty($sst->company->syarikatCIDBgreds)) 
                                @foreach($sst->company->syarikatCIDBgreds as $gred)
                                    {{$gred->grades}}   
                                @endforeach 
                            @endif                            
                            {{-- @if(!empty($sst->company->syarikatCIDBgreds)) 
                                @foreach($sst->company->syarikatCIDBgreds as $gred)
                                    {{$gred->grades}}   
                                @endforeach 
                            @endif --}}
                        </td>
                    </tr>																								
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top">5.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    Harga dan Maklumat Kontrak 
                </span>
                <br/>
                <table width="100%" style="padding:5 5;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">5.1</td>
                        <td width="25%">Harga <span style="text-transform:capitalize">{{ $acqCategory }}</span> (butiran harga seperti di 
                            <span style="font-weight:bold; font-style:italic">Lampiran A1)</span>

                        </td>
                        <td width="65%">: {{ money()->toHuman($appointed->offered_price) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">5.2</td>
                        <td>Harga Kontrak</td>
                        <td>: {{ money()->toHuman($appointed->offered_price) }}</td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td valign="top">5.3</td>
                        <td>Tempoh Kontrak</td>
                        <td>: 
                            {!! $sale->box->period. ' ' . $sale->box->period_type->name !!}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">5.4</td>
                        <td>Tarikh Milik Tapak</td>
                        <td>: 
                            {{ \Carbon::intlFormat($sst->start_working_date) }}
                        </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td valign="top">5.5</td>
                        <td>Tarikh Siap Kerja</td>
                        <td>: 
                            {{ \Carbon::intlFormat($sst->end_working_date) }}
                        </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td valign="top">5.6</td>
                        <td>Tempoh Liabiliti Kecacatan</td>
                        <td>: {{ isset($sst->defects_liability_period) ? $sst->defects_liability_period : "0"}} bulan</td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td valign="top">5.7</td>
                        <td>Lokasi Kerja</td>
                        <td>: {!! $sst->acquisition->approval->locations_to_string or '-' !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">5.8</td>
                        <td>Wakil Perbadanan</td>
                        <td>:  
                            @if(!empty($np_dept))
                                {{ $np_dept->name }}
                            @endif
                        </td>
                    </tr>																																																			
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top">6.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Bon Pelaksanaan 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">6.1</td>
                        <td width="25%">Kadar Bon Pelaksanaan</td>
                        <td width="65%">: 
                            @if($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 3)
                                2.5%
                            @else
                                5%
                            @endif 
                            dari nilai kontrak
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">6.2</td>
                        <td>Formula Bon Pelaksanaan</td>
                        <td>: 
                            @if($sst->acquisition->approval->acquisition_type_id == 1 || $sst->acquisition->approval->acquisition_type_id == 3)
                                2.5%
                            @else
                                5%
                            @endif  
                            x  {{ money()->toHuman($appointed->offered_price) }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">6.3</td>
                        <td>Nilai Bon Pelaksanaan</td>
                        <td>: {{ isset($sst->bon_amount) ? money()->toHuman($sst->bon_amount) : "-" }}</td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td valign="top">6.4</td>
                        <td valign="top">Bentuk Bon Pelaksanaan</td>
                        <td valign="top"  style="text-align: justify;">: <span style="text-align: justify;">Jaminan Bank / Jaminan Syarikat Kewangan / Jaminan Insurans / Takaful / Deraf Bank 
                            atas nama PERBADANAN PUTRAJAYA<br/><br/>
                            Jikalau Bon Pelaksanaan gagal dikemukakan pada tarikh milik tapak, Perbadanan berhak 
                            untuk melaksanakan kaedah Wang Jaminan Pelaksanaan yang dikenakan potongan 10% daripada 
                            setiap bayaran interim sehingga mencapai 5% daripada Harga Kontrak.</span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">6.5</td>
                        <td valign="top">Tempoh Sah Laku</td>
                        <td valign="top">:  
                            {{-- @if(!empty($bon->bank_start_date) && !empty($bon->bank_end_date)){{date('d/m/Y', strtotime($bon->bank_start_date))}} hingga {{date('d/m/Y', strtotime($bon->bank_end_date))}}@endif<br/><br/> --}}
    
                            *Dari tarikh kuat kuasa kontrak sehingga 12 bulan selepas tamat Tempoh Tanggungan Kecacatan (DLP) - bagi projek bernilai sehingga RM 10 juta<br/>atau<br/>

                            *Dari tarikh kuat kuasa kontrak sehingga 24 bulan selepas tamat Tempoh Tanggungan Kecacatan (DLP) - bagi projek bernilai melebihi RM 10 juta<br/><br/>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            Mengikut format yang ditetapkan oleh Perbadanan seperti di <i><b>Lampiran A4</b></i><br/><br/>
                            <span style="font-size:12px; color:grey; font-style: italic;">Lampiran A4 akan disediakan oleh Jabatan Pelaksana berdasarkan format 1PP/PK4</span>
                        </td>
                    </tr>																																																			
                </table>														
            </td>
        </tr>
        <tr>
            <td valign="top">7.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans (Jika Berkaitan) 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">7.1</td>
                        <td width="25%">Nilai Polisi</td>
                        <td width="65%">: 
                            {{-- {{ isset($sst->ins_work_amount) ? money()->toHuman($sst->ins_work_amount) : "-" }} --}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">7.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td>:
                            {{-- @if(!empty($sst->ins_work_start_date) && !empty($sst->ins_work_end_date)){{date('d/m/Y', strtotime($sst->ins_work_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_work_end_date))}}@endif <br/> --}}
                            <span style="font-size: 10px">*Meliputi tempoh kontrak</span>
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top">8.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans Tanggungan Awam 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">8.1</td>
                        <td width="25%">Nilai Polisi</td>
                        <td width="65%">: {{ isset($sst->ins_pli_amount) ? money()->toHuman($sst->ins_pli_amount) : "-" }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">8.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td>:
                            @if(!empty($sst->ins_pli_start_date) && !empty($sst->ins_pli_end_date)){{date('d/m/Y', strtotime($sst->ins_pli_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_pli_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak, tempoh tanggungan kecacatan dan 3 bulan 14 hari.</span>
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>
        {{-- <tr>
            <td valign="top">9.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Polisi Insurans Pampasan Pekerja 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">9.1</td>
                        <td width="25%">Nilai Polisi</td>
                        <td width="65%">: {{ isset($sst->ins_ci_amount) ? money()->toHuman($sst->ins_ci_amount) : "-" }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">9.2</td>
                        <td>Tempoh Perlindungan</td>
                        <td>:
                            @if(!empty($sst->ins_ci_start_date) && !empty($sst->ins_ci_end_date)){{date('d/m/Y', strtotime($sst->ins_ci_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_ci_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak, tempoh tanggungan kecacatan dan 3 bulan 14 hari.</span>
                        </td>
                    </tr>																																																		
                </table>							
            </td>
        </tr> --}}
        <tr>
            <td valign="top">9.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    Kenaan Liquidated &amp; Ascertained Damages (LAD) 
                </span>
                <br/>							
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">9.1</td>
                        <td width="25%" valign="top">Formula</td>
                        <td width="65%">:
                            <span style="text-decoration: underline; font-weight: bold;">PR (Prime Rate) %  x  Harga Kontrak</span><br/>
                            <span valign="top" style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;365 hari</span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">9.2</td>
                        <td>Kadar Sehari</td>
                        <td>: {{ money()->toHuman($sst->document_LAD_amount) }}</td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top">10.</td>
            <td valign="top">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Professional Training And Education For Growing Entrepreneurs (PROTEGE) (jika berkaitan) 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="8%" valign="top">10.1</td>
                        <td width="25%">Tertakluk Kepada pelaksanaan Program PROTEGE</td>
                        <td width="65%">:</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">10.2</td>
                        <td>Bilangan Minimum peserta PROTEGE</td>
                        <td>:  @if($sst->sl1m_entity == 0)
                                    Tiada
                                @else
                                    {{ $sst->sl1m_entity }} peserta
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="3" style="text-align:left;font-weight: bold;font-size:11px">
                            Formula Pengiraan : 
                                    
                            <span style="text-decoration: underline; font-weight: bold;">1 %  x  Harga Kontrak</span><br/>
                            <span valign="top" style="text-align: center; font-weight: bold;">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        RM24,000
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <span  style="font-weight: bold;font-size: 10px; color:grey"  align="center;">
                                *Bagi tujuan pengiraan PROTEGE, Harga Kontrak adalah nilai tawaran yang dipersetujui<br/>
                                **Elaun PROTEGE (RM2,000 seorang x 12 bulan)<br/><br/>
                            </span>                                
                        </td>									
                    </tr>
                </table>							
            </td>
        </tr>																																																		
    </table>
</div>