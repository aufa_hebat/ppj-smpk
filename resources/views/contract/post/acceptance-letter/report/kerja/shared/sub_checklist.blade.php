<div style="text-align:center; font-weight:bold">
    SENARAI SEMAK DOKUMEN SOKONGAN UNTUK <br/>
    SEMAKAN SURAT-SURAT SETUJU TERIMA
</div>
<br/>
<div>
    <table width="100%" style="border-collapse: collapse; border:1px solid black;" class="content">
        <tr>
            <td width="5%" style="border-collapse: collapse; border:1px solid black; text-align:center; font-weight:bold">BIL.</td>
            <td width="50%" style="border-collapse: collapse; border:1px solid black; text-align:center; font-weight:bold"colspan="2">SURAT CARA</td>
            <td width="15%" style="border-collapse: collapse; border:1px solid black; text-align:center; font-weight:bold">ADA/TIADA</td>
            <td width="30%" style="border-collapse: collapse; border:1px solid black; text-align:center; font-weight:bold">CATATAN</td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center; vertical-align:top" rowspan="5">1.</td>
            <td style="border-collapse: collapse; border:1px solid black; font-weight:bold" colspan="2">SURAT SETUJU TERIMA (NILAI KURANG RM500,000)</td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center" width="10px">(i)</td>
            <td style="border-collapse: collapse; border:1px solid black;" width="45%">Keputusan Jawatankuasa Sebut Harga;</td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center">(ii)</td>
            <td style="border-collapse: collapse; border:1px solid black;">
                Syarat-syarat yang telah ditetapkan oleh Jawatankuasa Sebut Harga / dokumen Sebut Harga (keperluan BPUB shj);
            </td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr> 
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center">(iii)</td>
            <td style="border-collapse: collapse; border:1px solid black;">
                Surat Kelulusan Kementerian Kewangan sekiranya melalui rundingan terus; atau
            </td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>   
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center">(iv)</td>
            <td style="border-collapse: collapse; border:1px solid black;">
                Apa-apa dokumen lain yang difikirkan perlu
            </td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr> 
        
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center; vertical-align:top" rowspan="5">2.</td>
            <td style="border-collapse: collapse; border:1px solid black; font-weight:bold" colspan="2">SURAT SETUJU TERIMA (NILAI RM500,000 KE ATAS)</td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center" width="10px">(i)</td>
            <td style="border-collapse: collapse; border:1px solid black;" width="45%">Keputusan Lembaga Perolehan;</td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center">(ii)</td>
            <td style="border-collapse: collapse; border:1px solid black;">
                Dokumen-dokumen tender (terdapat butiran atau maklumat yang perlu dimasukkan sebagai sebahagian daripada Perjanjian contohnya skop, spesifikasi, jadual pembekalan / kerja dan lain-lain) – keperluan BPUB shj;
            </td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr> 
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center">(iii)</td>
            <td style="border-collapse: collapse; border:1px solid black;">
                Surat kelulusan Kementerian Kewangan sekiranya melalui rundingan terus; atau
            </td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>   
        <tr>
            <td style="border-collapse: collapse; border:1px solid black; text-align:center">(iv)</td>
            <td style="border-collapse: collapse; border:1px solid black;">
                Apa-apa dokumen lain yang difikirkan perlu
            </td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
            <td style="border-collapse: collapse; border:1px solid black;"></td>
        </tr>        
    </table>
</div>