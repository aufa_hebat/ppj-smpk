@php
$acqCategory = "";

if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
    $acqCategory = "sebut harga";
}elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
    $acqCategory = "tender";
}
@endphp

<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
Lampiran D
</div>

<br/><br/>

<div style="text-transform: uppercase; font-weight: bold;" class="content">
<div style="text-align: center;">
    Surat Akuan Petender Untuk Melaksanakan Program Skim Latihan 1Malaysia (SL1M)<br/><br/>
    {{ $sst->acquisition->title }}<br/><br>
</div>
No.Kontrak : {{ $sst->contract_no }}
</div>	

<br/>

<div>
<table width="100%" class="content">
    <tr>
        <td style="text-align: justify;">
            Saya <span style="font-weight: bold;">{{ $sst->owner->name or '-' }}</span> nombor kad pengenalan <span style="font-weight: bold;"> {{ $sst->owner->ic_number }} </span>
            yang mewakili syarikat <span style="font-weight: bold;"> {{ $sst->company->company_name }} </span>
            bahawa saya akan melaksanakan program Skim Latihan 1Malaysia (SL1M) sekiranya syarikat dipilih sebagai 
            petender berjaya bagi {{ $acqCategory }} ini mengikut had nilai ambang (threshold value) dan bilangan 
            minimum peserta program SL1M seperti yang ditetapkan di dalam Arahan Kepada Petender.
        </td>
    </tr>
    <tr><td><br/></td></tr>
    <tr>
        <td style="text-align: justify;">
            2. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sekiranya syarikat tidak melaksanakan program SL1M mengikut had nilai ambang 
            <span style="font-style: italic;">(threshold value)</span> dan bilangan minimum peserta setelah 
            syarikat dipilih, maka Perbadanan berhak mengambil tindakan ke atas syarikat berdasarkan terma dan syarat kontrak yang 
            ditetapkan dan tidak mempertimbangkan tawaran kontrak kepada syarikat pada masa hadapan.
        </td>
    </tr>    
    <tr><td><br/></td></tr>
    <tr>
        <td>
            Yang Benar,<br/><br/><br/>
            ............................................................<br/><br/>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" class="content">
                <tr>
                    <td width="30%">Nama</td>
                    <td width="70%">:   <span style="font-weight: bold;">{{ $sst->owner->name or '-' }} </span></td>
                </tr>
                <tr>
                    <td>No. Kad Pengenalan</td>
                    <td>:   <span style="font-weight: bold;">{{ $sst->owner->ic_number or '-' }} </span></td>
                </tr>
                <tr>
                    <td>Tarikh</td>
                    <td>:........................................................</td>
                </tr>
                <tr>
                    <td>Cap Syarikat</td>
                    <td>:........................................................</td>
                </tr>								
            </table>
        </td>
    </tr>    
</table>
</div>