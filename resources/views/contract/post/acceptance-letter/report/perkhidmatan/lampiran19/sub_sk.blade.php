<div class="content">s.k</div>
<div>
    <table width="100%" class="content">
		<tr>
			<td width="10%" valign="top">1.</td>
			<td width="90%" valign="top">Setiausaha<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">2.</td>
			<td valign="top">Naib Presiden (Kewangan)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">3.</td>
			<td valign="top">Naib Presiden (Audit dan Kualiti Asurans)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">4.</td>
			<td valign="top">Pengarah (Bahagian {{ $sst->department->name }})<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">5.</td>
			<td valign="top">Pengarah (Bahagian Perolehan dan Ukur Bahan)<br/>Jabatan Kewangan<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">6.</td>
			<td valign="top">
				Pengarah <br/>
				Jabatan Pungutan Hasil<br>
				Ibu Pejabat Lembaga Hasil Dalam Negeri Malaysia<br/>
				Menara Hasil Aras 15<br/>
				Persiaran Rimba Permai<br/>
				Cyber 8, 63000 Cyberjaya<br/>
				<b>SELANGOR</b>
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">7.</td>
			<td valign="top">
				Perkeso Kajang<br/>
				No. 15, Jalan Hentian 1A<br/>
				Pusat Hentian Kajang<br/>
				Jalan Reko 43000 Kajang<br/>
				<b>SELANGOR</b><br/>
				(U/P: Unit Penguatkuasaan)
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">8.</td>
			<td valign="top">
				Pengarah GST<br/>
				Ibu Pejabat Kastam Diraja Malaysia<br/>
				Bahagian GST<br/>
				Aras 3 &amp; 4, Blok A, Menara Tulus<br/>
				No. 22, Persiaran Perdana, Presint 3<br/>
				<b>61200 PUTRAJAYA</b>
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">9.</td>
			<td valign="top">
				Sekretariat<br/>
				Majlis Perundingan Gaji Negara<br/>
				Kementerian Sumber Manusia<br/>
				Aras 7, Blok D3, Kompleks D<br/>
				<b>62530 PURTAJAYA</b>
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">10.</td>
			<td valign="top">
				Sime Darby Lockton Insurance Broker Sdn Bhd<br/>
				19th Floor, Menara JKG<br/>
				No. 282, Jalan Raja Laut<br/>
				P.O  Box 12355<br/>
				<b>50774 KUALA LUMPUR</b>
			</td>
        </tr>  																																																						
	</table>
</div>
