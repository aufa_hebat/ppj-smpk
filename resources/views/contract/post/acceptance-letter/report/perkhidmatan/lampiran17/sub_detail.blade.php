@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp

<div>
<table width="100%" style="text-align: justify;" class="content">
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            <span>
                &nbsp;&nbsp;&nbsp;&nbsp;Dengan ini dimaklumkan bahawa Perbadanan Putrajaya ("Perbadanan") telah 
                bersetuju menerima tawaran <span style="text-transform: capitalize;">{{ $acqCategory }}</span> syarikat 
                tuan dengan harga sebanyak <span style="text-transform: capitalize; font-weight: bold;">Ringgit Malaysia:
                    
                    @php

                        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                        $amaun = money()->toCommon($appointed->offered_price ?? "0" , 2);
                        $format = explode('.',$amaun);
                        $formats = substr($amaun,-2);
                        $partringgit = str_replace(',', '', $format[0]);
                        $partsen = $format[1];
                        $partkosongsen = (int)$formats;

                        if($partkosongsen == '.00'){
                            echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Sahaja" . '</span>';
                        }
                        else{
                            echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " DAN " . $f->format($partkosongsen) . " Sen Sahaja" . '</span>';
                        }

                    @endphp	
                    ({{ money()->toHuman($appointed->offered_price) }}	)
                </span>

                yang merupakan harga kontrak bagi tempoh kontrak selama  {!! $sale->box->period.' '.$sale->box->period_type->name !!}
                tertakluk kepada dokumen <span style="text-transform: capitalize;">{{ $acqCategory }}</span>  yang
                menjadi sebahagian daripada perolehan ini dan Surat Setuju Terima ini berserta dengan 
                <span style="text-transform: capitalize; font-style: italic;font-weight:bold">Lampiran A</span> kepada Surat Setuju Terima iaitu 
                maklumat terperinci kontrak (selepas ini disebut sebagai "Surat ini").
            </span>
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            2.&nbsp;&nbsp;&nbsp; Dengan pengakuan penerimaan Surat ini, suatu kontrak yang mengikat terbentuk antara 
            Perbadanan dengan syarikat tuan.
        </td>
    </tr>   
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            3.&nbsp;&nbsp;&nbsp; Perbadanan telah mengambil polisi insurans melalui 
            <span style="font-style:italic;text-transform:capitalize;">Principal Control Insurance Programme</span> untuk semua perolehan 
            Perbadanan dan syarikat tuan <span style="font-weight:bold;">tidak perlu</span> mengambil polisi insurans kerja, tanggungan 
            awam, pampasan pekerja dan lain-lain.  Walau bagaimanapun, tuan adalah diingatkan bahawa tiada pembekalan boleh dibuat 
            <span style="font-weight:bold;">melainkan</span> jika syarikat tuan telah mengemukakan kepada Perbadanan dokumen-dokumen berikut:
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            <table width="100%" class="content">
                <tr>
                    <td width="5%"></td>
                    <td width="8%" valign="top" style="text-align: justify;padding: 4 4;">*(a)</td>
                    <td width="87%" valign="top" style="text-align: justify;padding: 4 4;">
                        suatu bon pelaksanaan yang tidak boleh dibatalkan yang berjumlah             
                        <span style="text-transform: capitalize; font-weight: bold;">Ringgit Malaysia :                         
                            @php    
                                $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                                $amaun = money()->toCommon($sst->bon_amount ?? "0" , 2);
                                $format = explode('.',$amaun);
                                $formats = substr($amaun,-2);
                                $partringgit = str_replace(',', '', $format[0]);
                                $partsen = $format[1];
                                $partkosongsen = (int)$formats;
        
                                if($partkosongsen == '.00'){
                                    echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Sahaja" . '</span>';
                                }
                                else{
                                    echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " DAN " . $f->format($partkosongsen) . " Sen Sahaja" . '</span>';
                                }
        
                            @endphp	
                            ({{ money()->toHuman($sst->bon_amount) }}	)
                        </span><br/>
                        <span style="font-size: 10px;">*Bon Pelaksanaan hanya dikenakan kepada perolehan bernilai RM200,000.00 ke atas sahaja.</span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top" style="text-align: justify;padding: 4 4;">*(b)</td>
                    <td valign="top" style="text-align: justify;padding: 4 4;">
                        surat dan nombor pendaftaran Kod Majikan di bawah Skim Keselamatan Sosial Pekerja (PERKESO) untuk kontrak ini;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            mengikut ketetapan seperti di <span style="font-weight: bold; font-style: italic;">Lampiran A</span> 
            tidak melebihi 14 hari dari tarikh pengakuan penerimaan Surat ini oleh syarikat tuan. 
            Apa-apa kegagalan dalam mematuhi kehendak di perenggan ini dalam tempoh masa yang ditetapkan, boleh 
            mengakibatkan Surat ini terbatal dan Perbadanan tidaklah dengan apa-apa cara jua bertanggungan terhadap 
            syarikat tuan <span style="font-weight: bold;">melainkan jika </span>penepian bertulis diberikan oleh orang yang 
            diberi kuasa, bagi bekalan barang yang perlu dibuat dengan segera atau serta-merta apabila kelewatan itu akan memudarat 
            dan menjejaskan perkhidmatan dan kepentingan awam. 
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            4.&nbsp;&nbsp;&nbsp; Setelah pesanan/arahan dikeluarkan oleh Perbadanan, syarikat tuan dikehendaki melaksanakan 
            pembekalan barang dalam tempoh yang ditetapkan dan kualiti bekalan tersebut hendaklah memuaskan hati serta memenuhi 
            kehendak Perbadanan. Sekiranya syarikat tuan gagal melaksanakan pembekalan barang dalam tempoh dan/atau kualiti yang 
            ditetapkan, Perbadanan berhak membatalkan pesanan/arahan yang dikeluarkan dan/atau mengenakan 
            *Denda/ Tolakan/ <span style="font-style: italic;">Liquidated & Ascertained Damages</span> (LAD) seperti yang 
            ditetapkan dalam <span style="font-style: italic; font-weight: bold;">Lampiran A</span>.  
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>   
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            5.&nbsp;&nbsp;&nbsp; Syarikat tuan juga adalah diingatkan bahawa Perbadanan berhak untuk membatalkan Surat ini sekiranya:
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>	        
    <tr >
        <td width="5%"></td>
        <td width="8%" valign="top" style="text-align: justify;padding: 4 4;">(a)</td>
        <td width="87%" valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan gagal memenuhi mana-mana terma di perenggan 3 dalam tempoh masa yang ditetapkan;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify; padding: 4 4;">(b)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan gagal memenuhi mana-mana terma yang dinyatakan dalam Surat Akuan Pembida Berjaya;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(c)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan telah membuat salah nyataan <span style="font-style: italic;">(misrepresentation)</span>
            atau mengemukakan maklumat palsu semasa berurusan dengan Perbadanan bagi perolehan ini atau melakukan 
            apa-apa perbuatan lain, seperti memalsukan maklumat dalam Sijil Akuan Pendaftaran Syarikat, mengemukakan 
            bon pelaksanaan atau dokumen lain yang palsu atau yang telah diubah suai;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top"v>(d)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan hendaklah mengekalkan keesahan Sijil Perolehan Kerja Kerajaan dan Sijil Taraf Bumiputera 
            sepanjang tempoh kontrak sepertimana dinyatakan dalam 1 Pekeliling Perbendaharaan PK 8 Ketidakpatuhan Dalam 
            Perolehan Kerajaan;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(e)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan telah disenaraihitamkan oleh mana-mana badan kawal selia yang diiktiraf;
        </td>
    </tr> 
    {{--  <tr><td colspan="3">&nbsp;</td></tr>     --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(f)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan membenarkan Sijil Akuan Pendaftaran Syarikat disalahgunakan oleh individu/syarikat lain;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>	  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(g)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan terlibat dalam membuat pakatan harga dengan syarikat-syarikat lain atau apa-apa 
            pakatan sepanjang proses <span style="text-transform: capitalize;">{{ $acqCategory }}</span> sehingga dokumen kontrak ditandatangani;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(h)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan telah memberikan subkontrak sama ada sepenuhnya atau sebahagiannya 
            pembekalan barang tanpa kelulusan Perbadanan terlebih dahulu;  
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(i)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan gagal membekalkan barang/menyempurnakan perkhidmatan dalam tempoh yang ditetapkan seperti di 
            <span style="font-weight: bold; font-style: italic;">Lampiran A</span>;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(j)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan gagal mematuhi mana-mana terma/arahan di dalam dokumen <span style="text-transform: capitalize;">{{ $acqCategory }}</span>;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(k)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan/ pemilik/ rakan kongsi/ pengarah telah disabitkan atas kesalahan jenayah di dalam atau luar Malaysia; 
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(l)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan digulungkan;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(m)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan membekalkan barang-barang yang tidak tulen, bukan baharu atau yang terpakai;
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(n)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan gagal mematuhi spesifikasi pembekalan yang ditetapkan; atau
        </td>
    </tr>
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(o)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            syarikat tuan tidak mendapat kelulusan daripada Perbadanan terlebih dahulu bagi apa-apa 
            penjualan atau pemindahan ekuiti sepanjang tempoh kontrak berkuat kuasa; atau 
        </td>
    </tr>    
    {{--  <tr><td colspan="3">&nbsp;</td></tr>  --}}
    <tr>
        <td></td>
        <td valign="top" style="text-align: justify;padding: 4 4;">(p)</td>
        <td valign="top" style="text-align: justify;padding: 4 4;">
            terdapat perkara yang melibatkan kepentingan awam atau keselamatan dan kepentingan negara.
        </td>
    </tr>								
    <tr><td colspan="3">&nbsp;</td></tr>																																																																																							    
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            6.&nbsp;&nbsp;&nbsp; Sekiranya Surat ini dibatalkan atas alasan seperti yang ditetapkan di perenggan 5, 
            Perbadanan tidak akan bertanggungan terhadap apa-apa kerugian syarikat tuan termasuk kerugian masa hadapan. 
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            7.&nbsp;&nbsp;&nbsp; Bersama-sama Surat ini disertakan Surat Akuan Pembida Berjaya dan Surat Akuan Sumpah Syarikat 
            seperti di 
            <span style="font-style: italic; font-weight: bold;">Lampiran B</span> dan 
            <span style="font-style: italic; font-weight: bold;">Lampiran C</span> untuk ditandatangani 
            oleh syarikat tuan dan dikembalikan bersama-sama dengan Surat ini. 
        </td>
    </tr>					
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
            8.&nbsp;&nbsp;&nbsp; Surat ini dihantar kepada syarikat tuan dalam dua (2) salinan.
            Sila kembalikan ke pejabat ini salinan <span style="font-weight: bold;">ASAL</span> berserta lampiran
            yang berkaitan yang telah ditandatangani dengan sempurna oleh syarikat tuan dan saksi syarikat tuan tidak melebihi *3/7/14 hari 
            dari tarikh Surat ini diterima untuk tindakan kami selanjutnya. Apa-apa kegagalan dalam mematuhi kehendak di perenggan ini 
            dalam tempoh masa yang ditetapkan boleh mengakibatkan Surat ini terbatal dan Perbadanan tidaklah dengan apa-apa jua 
            bertanggungan terhadap syarikat tuan.
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
            <td colspan="3">
                <table width="100%" style="text-align: justify;" class="content">
                    <tr>
                        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
                            Sekian, terima kasih.
                        </td>
                    </tr>	
                    <tr><td colspan="3">&nbsp;</td></tr>
                    <tr>
                        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
                            <span style="font-weight: bold; text-transform: uppercase;">
                                "Bandar Raya Bestari, Kehidupan Berkualiti"<br/>
                                "Berkhidmat Untuk Negara"
                            </span>
                        </td>
                    </tr>
                    <tr><td colspan="3">&nbsp;</td></tr>
                    <tr>
                        <td valign="top" style="text-align: justify;padding: 1 1;" colspan="3">
                            Saya yang menjalankan amanah,<br/><br/><br/>
                            .............................................<br/>
                            <span style="font-weight: bold; text-transform: uppercase;">({{ (!empty($president)) ? $president->honourary . ' ' . $president->name : "" }} )</span><br/>
                            Presiden<br/>
                            Perbadanan Putrajaya
                        </td>
                    </tr>
                </table>
            </td>
        </tr>																																																																																																						
</table>
</div>