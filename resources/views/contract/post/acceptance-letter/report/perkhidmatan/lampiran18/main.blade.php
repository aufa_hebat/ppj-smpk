<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 100px 60px 100px 100px;
            }
            
            header {
                position: fixed;
                top: -70px;
                left: 0px;
                right: 0px;
                height: 250px;
                
                /** Extra personal styles **/
                /* background-color: #03a9f4; */
            
                color: black;
                text-align: right;
            
                /* line-height: 35px; */
            }
                
            footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                
                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;                    
            }
                
            footer .pagenum:before {
                content: counter(page);
            }
                
            div.breakNow { page-break-inside:avoid; page-break-after:always; }            
                    
            .title {
                font-size: 16px;
                font-family: "Arial, Helvetica, sans-serif";
            }
                            
            .title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
                            
            .title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
                
            .content {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
            }
                
        </style>
    </head>
    <body>
        <footer>
            <div class="pagenum-container title-footer">Page <span class="pagenum"></span></div>
        </footer>
        <main>
            @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_checklist', ['sst' => $sst])
            <div class="breakNow"><br></div>
            @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_disclaimer', ['sst' => $sst])
            <div class="breakNow"><br></div>             
            <br/><br/>
            @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_info', ['sst' => $sst])
            @include('contract.post.acceptance-letter.report.perkhidmatan.lampiran18.sub_detail', ['sst' => $sst])
            <br/>
            @include('contract.post.acceptance-letter.report.perkhidmatan.lampiran18.sub_sk', ['sst' => $sst])
            <div class="breakNow"><br></div>
            @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_terima', ['sst' => $sst])
            <div class="breakNow"><br></div>
            @include('contract.post.acceptance-letter.report.perkhidmatan.lampiran18.sub_lampiranA', ['sst' => $sst])
            <div class="breakNow"><br></div>
            @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_lampiranB', ['sst' => $sst])
            <div class="breakNow"><br></div>
            @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_lampiranC', ['sst' => $sst])
            {{--  <div class="breakNow"><br></div>  --}}
            {{--  @include('contract.post.acceptance-letter.report.perkhidmatan.shared.sub_lampiranD', ['sst' => $sst])  --}}
        </main>    
    </body>
</html>