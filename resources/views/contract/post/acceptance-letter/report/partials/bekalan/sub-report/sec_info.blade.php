@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp

<div>
    <table width="100%">
        <tr>
            <td width="10%"></td>
            <td width="90%" class="content">{{ $sst->acquisition->approval->reference }}</td>
        </tr>
        <tr>
            <td></td>
            <td class="content">
                {{ \Carbon::intlFormat(now()) }}
            </td>
        </tr>
    </table>
</div>
<br/><br/>

<div>
    <table width="100%" class="content">
        <tr>
            <td style="font-weight: bold; text-transform: uppercase;" colspan="2">
                {{ $sst->company->company_name }}
            </td>
        </tr>
        <tr>
            <td style="font-weight: normal; text-transform: capitalize;" width="75%">
                @if(!empty($sst->company->addresses[0]))
                    @if(!empty($sst->company->addresses[0]->primary))
                        {{ $sst->company->addresses[0]->primary }}
                        <br/>
                    @endif
                    @if(!empty($sst->company->addresses[0]->secondary))
                        {{ $sst->company->addresses[0]->secondary }}
                        <br/>
                    @endif	
                    @if(!empty($sst->company->addresses[0]->postcode))
                        {{ $sst->company->addresses[0]->postcode }}, {{ $sst->company->addresses[0]->state }}
                    @endif				
                @endif
            </td>
            <td style="font-weight: bold;" valign="top" width="25%">
                Tel : 	@if(!empty($sst->company->phones[0]))
                            @foreach($sst->company->phones as $phone)
                                @if($phone->phone_type_id == 3)
                                    {{ $phone->phone_number or "-" }}
                                @endif
                            @endforeach
                        @endif
                        <br/>
                Faks : 	@if(!empty($sst->company->phones[0]))
                            @foreach($sst->company->phones as $phone)
                                @if($phone->phone_type_id == 4)
                                    {{ $phone->phone_number or "-" }}
                                @endif
                            @endforeach
                        @endif				
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;" colspan="2">
                (u.p. : 
                    @if(!empty($sst->owner))
                        <span style="text-transform: capitalize;">{{ $sst->owner->name }}</span>
                    @endif
                )
            </td>
        </tr>
    </table>
</div>
<br/><br/>
<div>
    <span class="content">Tuan,</span><br/><br/>
    <span style="font-weight: bold; text-transform: uppercase;"  class="content">
        <table width="100%">
            <tr>	
                <td colspan="3">Surat Setuju Terima</td>
            </tr>
            <tr>
                <td width="20%" valign="top">
                    {{ $acqCategory }}
                </td>
                <td width="5%" valign="top"> : </td>
                <td width="75%" valign="top">{{ $sst->acquisition->title }}</td>
            </tr>
            <tr>
                <td valign="top">No. Kontrak</td>
                <td valign="top"> : </td>
                <td valign="top">{{ $sst->acquisition->reference }}</td>
            </tr>
        </table>
    </span>
    <hr/>
</div>