@php
$acqCategory = "";

if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
    $acqCategory = "sebut harga";
}elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
    $acqCategory = "tender";
}
@endphp

<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
Lampiran A
</div>
<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
Butiran Kontrak<br/><br/>
{{ $sst->acquisition->title }}
</div>
<div>
<table width="100%" class="content">
    <tr>
        <td width="7%" valign="top" style="padding:6 6;">1.</td>
        <td width="93%" valign="top" style="padding:6 6;">
            <span style="font-weight: bold; text-decoration: underline;">
                Pendaftaran Syarikat Dengan Suruhanjaya Syarikat Malaysia (SSM) Atau Pendaftaran Koperasi Dengan Suruhanjaya Koperasi Malaysia (SKM) (jika berkaitan)
            </span>
            <br/>
            <table width="100%" style="padding:5px 5px;" class="content">
                <tr>
                    <td width="2%"></td>
                    <td width="5%">1.1</td>
                    <td width="25%">No. Pendaftaran</td>
                    <td width="68%">: {{ $sst->company->ssm_no }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>1.2</td>
                    <td>Tempoh Sah Laku</td>
                    <td>: 
                        @if(!empty($sst->company->ssm_start_date) && !empty($sst->company->ssm_end_date)){{date('d/m/Y', strtotime($sst->company->ssm_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->ssm_end_date))}}@endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" style="padding:6 6;">2.</td>
        <td valign="top" style="padding:6 6;">
            <span style="font-weight: bold; text-decoration: underline;">
                Pendaftaran dengan Kementerian Kewangan (jika berdaftar)
            </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">2.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->mof_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>2.2</td>
                        <td>Tempoh Sah Laku</td>
                        <td>: 
                            @if(!empty($sst->company->mof_start_date) && !empty($sst->company->mof_end_date))
                                {{date('d/m/Y', strtotime($sst->company->mof_start_date))}} hingga {{date('d/m/Y', strtotime($sst->company->mof_end_date))}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>2.3</td>
                        <td>Kod Bidang</td>
                        <td>:
                            @if(!empty($sst->company->syarikatMOFBidang)) 
                                  @foreach($sst->company->syarikatMOFBidang as $bidang)
                                    {{$bidang->areas}}   
                                  @endforeach 
                            @endif
                        </td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td>2.4</td>
                        <td>Taraf Syarikat</td>
                        <td>:

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>2.5</td>
                        <td>Tempoh Sah Laku Taraf Bumiputera</td>
                        <td>:

                        </td>
                    </tr>																						
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">3.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Pendaftaran Cukai Barang dan Perkhidmatan (CBP) dengan Jabatan Kastam Diraja Malaysia (jika berdaftar)
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">3.1</td>
                        <td width="25%">No. Pendaftaran</td>
                        <td width="68%">: {{ $sst->company->gst_no }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>3.2</td>
                        <td>Tarikh Kuat Kuasa</td>
                        <td>: </td>
                    </tr>																						
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">4.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Harga dan Tempoh Kontrak
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">4.1</td>
                        <td width="25%">Harga {{ $acqCategory }} (butiran harga seperti di <span style="font-style:italic;font-weight:bold;">Lampiran A1</span>)</td>
                        <td width="68%">: {{ money()->toHuman($appointed->offered_price) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>4.2</td>
                        <td>Peruntukan CBP (sekiranya berkaitan)</td>
                        <td>: </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td>4.3</td>
                        <td>Harga Kontrak</td>
                        <td>:
                            {{ money()->toHuman($appointed->offered_price) }}
                        </td>
                    </tr>  
                    <tr>
                        <td></td>
                        <td>4.4</td>
                        <td>Tempoh Kontrak</td>
                        <td>:
                            {!! $sale->box->period. ' ' . $sale->box->period_type->name !!}
                        </td>
                    </tr>    
                    <tr>
                        <td></td>
                        <td>4.5</td>
                        <td>Tarikh Mula Kontrak</td>
                        <td>:
                            {{ \Carbon::intlFormat($sst->start_working_date) }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>4.6</td>
                        <td>Tarikh Tamat Kontrak</td>
                        <td>:
                            {{ \Carbon::intlFormat($sst->end_working_date) }}
                        </td>
                    </tr>                                                                                           																							
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">5.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Tempoh dan Jadual Pembekalan Barang 
                </span>
                <br/>
                *Senarai item, kuantiti dan/atau tempoh serta jadual pembekalan yang ditetapkan seperti di 
                <span style="font-weight:bold; font-style:italic;">Lampiran A2</span>
                <br/>
                <span style="color:grey; font-style:italic;"><span style="font-weight:bold;">Lampiran A2</span>
                adalah untuk menyatakan tempoh atau jadual pembekalan mengikut tawaran syarikat dan disediakan 
                oleh Jabatan Pelaksana.</span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">6.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Spesifikasi Pembekalan (<span style="font-style:italic">sekiranya berkaitan</span>)
                </span>
                <br/>
                *Spesifikasi pembekalan yang ditetapkan seperti di 
                <span style="font-weight:bold; font-style:italic;">Lampiran A3</span>
                <br/>
                <span style="color:grey; font-style:italic;"><span style="font-weight:bold;">Lampiran A3</span>
                akan disediakan oleh Jabatan Pelaksana.</span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">7.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Bon Pelaksanaan 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">7.1</td>
                        <td width="30%">Kadar Bon Pelaksanaan</td>
                        <td width="63%">: 
                            @php
                                $periodType = $sale->box->period_type->id;
                                $period = $sale->box->period;
                                $amount = $appointed->offered_price;
                                $amountPerYear = 0.0;
                                $percentage = '';

                                if($periodType == 4 && $period >= 2){
                                    $amountPerYear = $amount/$period;
                                }else if($periodType == 3 && $period >= 24){
                                    $amountPerYear = ($amount/($period/12));
                                }else if($periodType == 2 && $period >= 104){
                                    $amountPerYear = ($amount/($period/52));
                                }else if($periodType == 1 && $period >= 730){
                                    $amountPerYear = ($amount/($period/365));
                                }else{
                                    $amountPerYear = $amount;
                                }

                                if($amount >= 20000000 && $amount < 50000000){
                                    $percentage = "2.5%";
                                }else if($amount >= 50000000){
                                    $percentage = "5%";
                                }else{
                                    $percentage = "0%";
                                }
                            @endphp
                            {{ $percentage }} dari nilai kontrak
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>7.2</td>
                        <td>Formula Bon Pelaksanaan</td>
                        <td>: 
                            {{ $percentage }}  x  {{ money()->toHuman($appointed->offered_price) }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>7.3</td>
                        <td>Nilai Bon Pelaksanaan</td>
                        <td>: {{ isset($sst->bon_amount) ? money()->toHuman($sst->bon_amount) : "-" }}</td>
                    </tr>		
                    <tr>
                        <td></td>
                        <td valign="top">7.4</td>
                        <td valign="top">Bentuk Bon Pelaksanaan</td>
                        <td valign="top"  style="text-align: justify;">: 
                            <span style="text-align: justify;">Jaminan Bank / Jaminan Syarikat Kewangan / Jaminan Insurans / Takaful / Deraf Bank atas nama PERBADANAN PUTRAJAYA</span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top">7.5</td>
                        <td valign="top">Tempoh Sah Laku</td>
                        <td valign="top">:  
                            @if(!empty($bon->bank_start_date) && !empty($bon->bank_end_date)){{date('d/m/Y', strtotime($bon->bank_start_date))}} hingga {{date('d/m/Y', strtotime($bon->bank_end_date))}}@endif<br/><br/>

                            *Dari tarikh kuat kuasa kontrak sehingga 12 bulan selepas tarikh tamat Laku kontrak atau
                            tarikh obligasi terakhir mengikut mana yang terkemudian<br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">Mengikut format yang ditetapkan oleh Perbadanan.
                    </tr>	
                    <tr>
                        <td></td>
                        <td valign="top">7.6</td>
                        <td valign="top">Nilai Polisi</td>
                        <td valign="top"  style="text-align: justify;">: {{ isset($sst->ins_work_amount) ? money()->toHuman($sst->ins_work_amount) : "-" }}</td>
                    </tr>   
                    <tr>
                        <td></td>
                        <td valign="top">7.7</td>
                        <td valign="top">Tempoh</td>
                        <td valign="top"  style="text-align: justify;">: 
                            @if(!empty($sst->ins_work_start_date) && !empty($sst->ins_work_end_date)){{date('d/m/Y', strtotime($sst->ins_work_start_date))}} hingga {{date('d/m/Y', strtotime($sst->ins_work_end_date))}}@endif <br/>
                            <span style="font-size: 10px">*Meliputi tempoh kontrak</span>
                        </td>
                    </tr>                                      																																																		
                </table>														
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">8.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    Kenaan *Denda/ Tolakan/ Liquidated &amp; Ascertained Damages (LAD) 
                </span>
                <br/>							
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">9.1</td>
                        <td width="30%">Formula</td>
                        <td width="63%">:
                                <span style="text-decoration: underline; font-weight: bold;">PR (Prime Rate) %  x  Harga Kontrak</span><br/>
                                <span valign="top" style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;365 hari</span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>9.2</td>
                        <td>Kadar Sehari</td>
                        <td>: {{ money()->toHuman($sst->document_LAD_amount) }}</td>
                    </tr>																																																		
                </table>							
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:6 6;">9.</td>
            <td valign="top" style="padding:6 6;">
                <span style="font-weight: bold; text-decoration: underline;">
                    *Skim Latihan 1Malaysia (SL1M) (jika berkaitan) 
                </span>
                <br/>
                <table width="100%" style="padding:5px 5px;" class="content">
                    <tr>
                        <td width="2%"></td>
                        <td width="5%">10.1</td>
                        <td width="30%">Tertakluk Kepada pelaksanaan Program SL1M</td>
                        <td width="63%">:</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>10.2</td>
                        <td>Bilangan Minimum Peserta SL1M</td>
                        <td>:  @if($sst->sl1m_entity == 0)
                                    Tiada
                                @else
                                    {{ $sst->sl1m_entity }} peserta
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" style="font-weight: bold;">
                            Formula Pengiraan
                        </td>
                        <td>
                            <span style="text-decoration: underline; font-weight: bold;">1 %  x  Harga Kontrak</span><br/>
                            <span valign="top" style="text-align: center; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RM24,000</span>
                        </td>
                    </tr>																																																																			
                </table>							
            </td>
        </tr>																																																	
    </table>
</div>