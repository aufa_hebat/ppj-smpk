@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp

<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
    Pengakuan Penerimaan Surat Setuju Terima Dan Lampiran Yang Berkaitan Oleh Syarikat    
</div>
<div style="text-align: left;">
    <table width="100%" class="content">
        <tr>
            <td width="20%">Rujukan Tuan</td>
            <td width="5%"> : </td>
            <td width="75%">PPj/{{ $sst->acquisition->approval->file_reference }}</td>
        </tr>
        <tr>
            <td>Rujukan Kami</td>
            <td> : </td>
            <td>{{ $sst->acquisition->approval->reference }}</td>
        </tr>	
        <tr><td colspan="3"><br/></td></tr>				
        <tr>
            <td>Tarikh</td>
            <td> : </td>
            <td>{{ \Carbon::intlFormat(now()) }}</td>
        </tr>
        <tr><td colspan="3"><br/></td></tr>				
        <tr>
            <td colspan="3">
                Presiden<br/>
                <span style="font-weight: bold; text-transform: uppercase;">Perbadanan Putrajaya</span><br/>
                Kompleks Perbadanan Putrajaya<br/>
                24, Persiaran Perdana, Presint 3<br/>
                62675 PUTRAJAYA<br/>
                (u.p. : Naib Presiden)
            </td>
        </tr>
        <tr><td colspan="3"><br/></td></tr>	
        <tr><td colspan="3">Tuan,</td></tr>
        <tr><td colspan="3"><br/></td></tr>	
        <tr>
            <td colspan="3" style="font-weight: bold; text-transform: uppercase; text-decoration: underline;">Akuan Penerimaan</td>
        </tr>	
    </table>
</div>
<br/>
<div>
    <span style="font-weight: bold; text-transform: uppercase;" class="content">
        <table width="100%">
            <tr>	
                <td colspan="3">Surat Setuju Terima</td>
            </tr>            
            <tr>
                <td width="20%">{{ $acqCategory }}</td>
                <td width="5%"> : </td>
                <td width="75%">{{ $sst->acquisition->title }}</td>
            </tr>
            <tr>
                <td>No. Kontrak</td>
                <td> : </td>
                <td>{{ $sst->acquisition->reference }}</td>
            </tr>
        </table>
    </span>
    <hr/>
</div>
<div>
    <table width="100%" border="0" class="content" >
        <tr>
            <td colspan="5" style="text-align: justify;">
                Saya/Kami yang bertandatangan di bawah ini mengakui penerimaan 
                Surat Setuju Terima bertarikh {{ \Carbon::intlFormat(now()) }}.<br/><br/>
                Bersama ini juga dikembalikan salinan asal Surat Setuju Terima yang telah 
                disempurnakan dan ditandatangani dan surat penduanya telah disimpan oleh saya/kami.
            </td>
        </tr>
        <tr><td colspan="5"><br/><br/></td></tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                ...............................................
            </td>
            <td width="6%"></td>
            <td colspan="2" style="text-align: center;">
                ...............................................							
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">(Tandatangan Kontraktor)</td>
            <td></td>
            <td colspan="2" style="text-align: center;">(Tandatangan Saksi)</td>
        </tr>
        <tr>
            <td width="10%">Nama Penuh</td>
            <td width="37%">:................................................</td>
            <td width="6%"></td>
            <td width="10%">Nama Penuh</td>
            <td width="37%">:................................................</td>
        </tr>
        <tr>
            <td>No. K/P</td>
            <td>:................................................</td>
            <td></td>
            <td>No. K/P</td>
            <td>:................................................</td>
        </tr>
        <tr>
            <td>Atas Sifat</td>
            <td>:................................................</td>
            <td></td>
            <td>Pekerjaan</td>
            <td>:................................................</td>
        </tr>
        <tr>
            <td colspan="2">
                Yang diberi kuasa dengan sempurnanya untuk menandatangani untuk dan bagi pihak:<br/><br/>							
            </td>
            <td></td>
            <td>Alamat</td>
            <td>:................................................<br/>
                ................................................
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                ...............................................
            </td>
            <td width="6%"></td>
            <td colspan="2" style="text-align: center;">
                                                    
            </td>
        </tr>	
        <tr>
            <td colspan="2" style="text-align: center;">(Meterai Cap Syarikat)</td>
            <td></td>
            <td colspan="2" style="text-align: center;"></td>
        </tr>
        <tr>
            <td>Tarikh</td>
            <td>:................................................</td>
            <td></td>
            <td>Tarikh</td>
            <td>:................................................</td>
        </tr>					
    </table>
</div>