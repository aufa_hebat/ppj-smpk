@php
    $acqCategory = "";

    if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
        $acqCategory = "sebut harga";
    }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
        $acqCategory = "tender";
    }
@endphp
<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
    Lampiran B
</div>
<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
    Surat Akuan Pembida Berjaya<br/><br/>
    {{ $sst->acquisition->title }}<br/><br>
    No.Kontrak : {{ $sst->acquisition->reference }}
</div>	
<br/><br/>
<div>
    <table width="100%" class="content">
        <tr>
            <td style="text-align: justify;">
                Saya <span style="font-weight: bold;">{{ $sst->owner->name or '-' }}</span> no K.P <span style="font-weight: bold;"> {{ $sst->owner->ic_number }} </span>
                yang mewakili <span style="font-weight: bold;"> {{ $sst->company->company_name }} </span>
                No. Pendaftaran 
                @if(!empty($sst->company->cidb_no))
                    (*<span style="text-decoration: line-through;">MOF</span>/CIDB/<span style="text-decoration: line-through;">SSM</span>) [ <span style="font-weight: bold;">{{ $sst->company->cidb_no }}</span> ] 
                @elseif(!empty($sst->company->ssm_no))
                    (*<span style="text-decoration: line-through;">MOF</span>/<span style="text-decoration: line-through;">CIDB</span>/SSM) [ <span style="font-weight: bold;">{{ $sst->company->ssm_no }}</span> ] 
                @elseif(!empty($sst->company->mof_no))
                    (*MOF/<span style="text-decoration: line-through;">CIDB</span>/<span style="text-decoration: line-through;">SSM</span>) [ <span style="font-weight: bold;">{{ $sst->company->mof_no }}</span> ] 
                @else
                    (*<span style="text-decoration: line-through;">MOF</span>/<span style="text-decoration: line-through;">CIDB</span>/SSM) [ <span style="font-weight: bold;">{{ $sst->company->ssm_no }}</span> ] 
                @endif
                                
                dengan ini 
                mengisytiharkan bahawa saya atau mana-mana individu yang mewakili syarikat ini tidak akan menawar atau memberi rasuah kepada 
                mana-mana individu dalam Perbadanan Putrajaya atau mana-mana individu lain, sebagai ganjaran mendapatkan sebut harga seperti 
                di atas.  Bersama ini dilampirkan Surat Perwakilan Kuasa bagi saya mewakili syarikat ini seperti tercatat di atas untuk 
                membuat pengisytiharan ini.
            </td>
        </tr>
        <tr><td><br/></td></tr>
        <tr>
            <td style="text-align: justify;">
                2. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sekiranya saya, atau mana-mana individu yang mewakili syarikat ini didapati cuba menawar 
                atau memberi rasuah kepada mana-mana individu dalam Perbadanan Putrajaya atau mana-mana individu lain sebagai ganjaran mendapatkan 
                sebut harga seperti di atas, maka saya sebagai wakil syarikat bersetuju tindakan-tindakan berikut diambil:
            </td>
        </tr>
        <tr><td><br/></td></tr>
        <tr>
            <td style="text-align: justify;">
                2.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Penarikan balik tawaran kontrak bagi sebut harga di atas; atau<br/>
                2.2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Penamatan kontrak bagi sebut harga di atas; dan<br/>
                2.3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lain-lain tindakan tatatertib mengikut peraturan perolehan Perbadanan Putrajaya yang berkuatkuasa.
            </td>
        </tr>
        <tr><td><br/></td></tr>
        <tr>
            <td style="text-align: justify;">
                3. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sekiranya terdapat mana-mana individu cuba meminta rasuah daripada saya atau mana-mana individu 
                yang berkaitan dengan syarikat ini sebagai ganjaran mendapatkan sebut harga seperti di atas, maka saya berjanji akan dengan segera 
                melaporkan perbuatan tersebut kepada pejabat Suruhanjaya Pencegahan Rasuah Malaysia (SPRM) atau balai polis yang berhampiran.
            </td>
        </tr>	
        <tr><td><br/></td></tr>
        <tr>
            <td>
                Yang Benar,<br/><br/><br/>
                ............................................................<br/><br/>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" class="content">
                    <tr>
                        <td width="30%">Nama</td>
                        <td width="70%">:   <span style="font-weight: bold;">{{ $sst->owner->name or '-' }} </span></td>
                    </tr>
                    <tr>
                        <td>No. Kad Pengenalan</td>
                        <td>:   <span style="font-weight: bold;">{{ $sst->owner->ic_number or '-' }} </span></td>
                    </tr>
                    <tr>
                        <td>Tarikh</td>
                        <td>:........................................................</td>
                    </tr>
                    <tr>
                        <td>Cap Syarikat</td>
                        <td>:........................................................</td>
                    </tr>								
                </table>
            </td>
        </tr>
        <tr><td><br/></td></tr>
        <tr>
            <td>
                Catatan:<br/>
                i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *Potong mana yang tidak berkenaan<br/>
                ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Surat akuan ini hendaklah dikemukakan 
                Bersama Surat Perwakilan Kuasa yang ditandatangani oleh Pemilik Syarikat
            </td>
        </tr>
    </table>
</div>