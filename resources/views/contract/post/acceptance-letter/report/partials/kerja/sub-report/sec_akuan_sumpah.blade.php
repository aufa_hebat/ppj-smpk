<div style="text-align: right; font-style: italic; font-weight: bold;" class="content">
	Lampiran C
</div>
<div style="text-align: center; text-transform: uppercase; font-weight: bold;" class="content">
	Surat Akuan Sumpah Syarikat<br/>
</div>
<br/>
<div>
	<table width="100%" class="content">
		<tr>
			<td style="text-align: justify; padding:5 5;">
				Saya <span style="font-weight: bold;">{{ $sst->owner->name or '-' }}</span> nombor kad pengenalan <span style="font-weight: bold;">{{ $sst->owner->ic_number or '-' }}</span> yang 
				mewakili syarikat <span style="font-weight: bold;">{{ $sst->company->company_name or '-' }}</span> 
				nombor pendaftaran 
				@if(!empty($sst->company->cidb_no))
					<span style="font-weight: bold;">{{ $sst->company->cidb_no }}</span> &nbsp;&nbsp; (*<span style="text-decoration: line-through;">MOF</span>/CIDB/<span style="text-decoration: line-through;">SSM</span>)
				@elseif(!empty($sst->company->ssm_no))
					<span style="font-weight: bold;">{{ $sst->company->ssm_no }}</span> &nbsp;&nbsp;  (*<span style="text-decoration: line-through;">MOF</span>/<span style="text-decoration: line-through;">CIDB</span>/SSM)
				@elseif(!empty($sst->company->mof_no))
					<span style="font-weight: bold;">{{ $sst->company->mof_no }}</span> &nbsp;&nbsp;  (*MOF/<span style="text-decoration: line-through;">CIDB</span>/<span style="text-decoration: line-through;">SSM</span>)
				@else
					<span style="font-weight: bold;">{{ $sst->company->ssm_no }}</span> &nbsp;&nbsp;  (*<span style="text-decoration: line-through;">MOF</span>/<span style="text-decoration: line-through;">CIDB</span>/SSM)
				@endif
				dengan sesungguhnya dan sebenarnya mengaku bahawa:
			</td>
		</tr>
		<tr>
			<td style="padding:5 5;">
				<table width="100%" class="content">
					<tr>
						<td width="5%"></td>
						<td width="5%" valign="top" style="padding:5 5;">(a)</td>
						<td width="90%" valign="top" style="text-align: justify; padding:5 5;">
							syarikat <span style="font-weight: bold; text-transform: uppercase;"> tidak </span> membuat 
							salah nyataan (<span style="font-style: italic;">misrepresentation</span>) atau mengemukakan 
							maklumat palsu semasa berurusan dengan Perbadanan bagi perolehan ini atau melakukan apa-apa 
							perbuatan lain, seperti memalsukan maklumat dalam Sijil Akuan Pendaftaran Syarikat, mengemukakan 
							bon pelaksanaan  atau dokumen lain yang palsu atau yang telah diubah suai;
						</td>
					</tr>
					<tr>
						<td></td>
						<td valign="top" style="padding:5 5;">(b)</td>
						<td valign="top" style="text-align: justify; padding:5 5;">
							syarikat <span style="font-weight: bold; text-transform: uppercase;"> tidak </span> membenarkan 
							Sijil Akuan Pendaftaran Syarikat disalahgunakan oleh individu/syarikat lain;
						</td>
					</tr>
					<tr>
						<td></td>
						<td valign="top" style="padding:5 5;">(c)</td>
						<td valign="top" style="text-align: justify; padding:5 5;">
							syarikat <span style="font-weight: bold; text-transform: uppercase;"> tidak </span> terlibat  dalam 
							membuat pakatan harga dengan syarikat-syarikat lain atau apa-apa pakatan sepanjang proses *sebut harga/tender 
							sehingga dokumen kontrak ditandatangani;
						</td>
					</tr>
					<tr>
						<td></td>
						<td valign="top" style="padding:5 5;">(d)</td>
						<td valign="top" style="text-align: justify; padding:5 5;">
							syarikat/pemilik/rakan kongsi/pengarah <span style="font-weight: bold; text-transform: uppercase;"> tidak </span> disabitkan 
							atas kesalahan jenayah di dalam atau luar Malaysia; dan
						</td>
					</tr>
					<tr>
						<td></td>
						<td valign="top" style="padding:5 5;">(e)</td>
						<td valign="top" style="text-align: justify; padding:5 5;">
							syarikat <span style="font-weight: bold; text-transform: uppercase;"> tidak </span> digulungkan.
						</td>
					</tr>															
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align: justify; padding:5 5;">
				Sekiranya pada bila-bila masa, dibuktikan bahawa pengisytiharan perenggan di atas adalah tidak benar, Perbadanan berhak menarik 
				balik tawaran kontrak atau menamatkan perkhidmatan syarikat bagi projek ini. 
			</td>
		</tr>
		<tr>
			<td style="text-align: justify; padding:5 5;">
				Dan saya membuat Surat Akuan Bersumpah ini dengan kepercayaan bahawa apa-apa yang tersebut di dalamnya adalah benar serta menurut 
				Akta Akuan Berkanun 1960. 
			</td>
		</tr>	
		<tr>
			<td style="padding:5 5;">
				<table width="100%" class="content">
					<tr>
						<td width="50%" valign="top">
							Diperbuat dan dengan sebenar-benarnya <br/>
							diakui oleh<br/><br/>
							<span style="font-weight: bold;">{{ $sst->owner->name or '-' }} </span> <br/>
							di .....................................<br/>
							pada....................................<br/>
						</td>
						<td width="50%" valign="middle">
							Tandatangan.................................
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br/></td></tr>
		<tr>
			<td align="center">
				<table width="50%" align="center" class="content">
					<tr>
						<td style="text-align: center;">
							Di hadapan saya,<br/><br/>
							................................................<br/>
							Pesuruhjaya Sumpah
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><br/></td></tr>
		<tr>
			<td>
				Catatan:<br/>
				i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *Potong mana yang tidak berkenaan<br/>
				ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Surat akuan ini hendaklah ditandatangani oleh hanya penama di sijil 
				pendaftaran MOF/CIDB/SSM
			</td>
		</tr>
	</table>
</div>					