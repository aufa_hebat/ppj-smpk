<div class="content">s.k</div>
<div>
    <table width="100%" class="content">
		<tr>
			<td width="10%" valign="top">1.</td>
			<td width="90%" valign="top">Setiausaha<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">2.</td>
			<td valign="top">Naib Presiden (Kewangan)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">3.</td>
			<td valign="top">Naib Presiden (Audit dan Kualiti Asurans)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">4.</td>
			<td valign="top">Pengarah (Bahagian Pelaksana)<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">5.</td>
			<td valign="top">Pengarah (Bahagian Perolehan dan Ukur Bahan)<br/>Jabatan Kewangan<br/>Perbadanan Putrajaya</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">6.</td>
			<td valign="top">
				Pengarah<br/>Bahagian Pembangunan Kontraktor dan Usahawan (BPKU)<br/>Cawangan Negeri 
				@if(!empty($sst->company->addresses[0]))
					@if(!empty($sst->company->addresses[0]->state))
						{{ $sst->company->addresses[0]->state }}
					@endif
				@endif
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">7.</td>
			<td valign="top">
				Ketua Eksekutif<br/>
				Lembaga Pembangunan Industri Pembinaan Malaysia<br/>
				Tingkat 10, No. 45, Menara Dato' Onn<br/>
				Pusat Dagangan Dunia Putra (PWTC)<br/>
				50480 KUALA LUMPUR
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">8.</td>
			<td valign="top">
				Pengarah <br/>
				Jabatan Pungutan Hasil<br>
				Ibu Pejabat Lembaga Hasil Dalam Negeri Malaysia<br/>
				Menara Hasil Aras 15<br/>
				Persiaran Rimba Permai<br/>
				Cyber 8, 63000 Cyberjaya<br/>
				SELANGOR
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">9.</td>
			<td valign="top">
				Perkeso Kajang<br/>
				No. 15, Jalan Hentian 1A<br/>
				Pusat Hentian Kajang<br/>
				Jalan Reko 43000 Kajang<br/>
				SELANGOR<br/>
				(U/P: Unit Penguatkuasaan)
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">10.</td>
			<td valign="top">
				Ketua Sekretariat<br/>
				Skim Latihan 1Malaysia (SL1M)<br/>
				Unit Perancang Ekonomi<br/>
				Jabatan Perdana Menteri<br/>
				Kompleks B, Blok B5, Aras 2<br/>
				Pusat Pentadbiran Kerajaan Persekutuan<br/>
				62502 PUTRAJAYA
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">11.</td>
			<td valign="top">
				Pengarah GST<br/>
				Ibu Pejabat Kastam Diraja Malaysia<br/>
				Bahagian GST<br/>
				Aras 3 &amp; 4, Blok A, Menara Tulus<br/>
				No. 22, Persiaran Perdana, Presint 3<br/>
				61200 PUTRAJAYA
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">12.</td>
			<td valign="top">
				Sekretariat<br/>
				Majlis Perundingan<br/>
				Gaji Negara<br/>
				Kementerian Sumber Manusia<br/>
				Aras 7, Blok D3, Kompleks D<br/>
				62530 PURTAJAYA
			</td>
		</tr>
        <tr><td colspan="2" style="padding: 5 5;"></td></tr>
		<tr>
			<td valign="top">13.</td>
			<td valign="top">
				Sime Darby Lockton Insurance Broker Sdn Bhd<br/>
				19th Floor, Menara JKG<br/>
				No. 282, Jalan Raja Laut<br/>
				P.O  Box 12355<br/>
				50774 KUALA LUMPUR
			</td>
		</tr>																																																							
	</table>
</div>
