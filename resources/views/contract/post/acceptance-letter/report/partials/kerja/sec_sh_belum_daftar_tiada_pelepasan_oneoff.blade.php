<div>
    <table width="100%" style="text-align: justify;" class="content">
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                <span>
                    Dengan ini dimaklumkan bahawa Perbadanan Putrajaya ("Perbadanan") telah bersetuju menerima tawaran
                    {{ $sst->acquisition->category->name }} tuan dengan harga sebanyak 
                    <span style="text-transform: capitalize; font-weight: bold;">Ringgit Malaysia: 

                    @php

                        $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                        $amaun = money()->toCommon($appointed->offered_price ?? "0" , 2);
                        $format = explode('.',$amaun);
                        $formats = substr($amaun,-2);
                        $partringgit = str_replace(',', '', $format[0]);
                        $partsen = $format[1];
                        $partkosongsen = (int)$formats;

                        if($partkosongsen == '.00'){
                            echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Sahaja" . '</span>';
                        }
                        else{
                            echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Dan " . $f->format($partkosongsen) . " Sen Sahaja" . '</span>';
                        }

                    @endphp	
                    ({{ money()->toHuman($appointed->offered_price) }}	)
                    </span>
                                                                            
                    yang merupakan harga kontrak bagi tempoh kontrak selama  {!! $sale->box->period.' '.$sale->box->period_type->name !!}
                    tertakluk kepada dokumen <span style="text-transform: capitalize">{{ $sst->acquisition->category->name }}</span> 
                    menjadi sebahagian daripada perolehan ini dan Surat Setuju Terima ini berserta dengan 
                    <span style="text-transform: capitalize; font-style: italic;">Lampiran A</span> kepada Surat Setuju Terima iaitu maklumat terperinci kontrak 
                    (selepas ini disebut sebagai "Surat ini").
                </span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                2.&nbsp;&nbsp;&nbsp; Dengan penerimaan tersebut, suatu ikatan kontrak telah wujud di antara Perbadanan dengan pihak tuan. 
                Surat Setuju Terima ini dan Dokumen Sebut Harga tuan hendaklah menjadi dokumen kontrak yang mengikat di antara Perbadanan 
                dengan tuan secara sah
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                3.&nbsp;&nbsp;&nbsp; Harga kontrak adalah tidak termasuk peruntukan Perbadanan sebanyak 6% Cukai Barang dan Perkhidmatan (CBP) 
                memandangkan keadaan berikut:
            </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td width="8%" valign="top"> (a) </td>
            <td width="87%" valign="top" style="text-align: justify;padding: 6 6;">
                Syarikat tuan belum berdaftar dengan Jabatan Kastam Diraja Malaysia (JKDM), tetapi pembekalan barang bercukai syarikat 
                tuan dalam bulan ini dan sebelas (11) bulan sebaik sebelum bulan ini telah melebihi RM500,000.00, di mana syarikat tuan 
                adalah dikehendaki untuk berdaftar dengan JKDM; 
            </td>
        </tr>
        <tr>
            <td valign="top"></td>
            <td valign="top"> (b) </td>
            <td valign="top" style="text-align: justify;padding: 6 6;">
                Pembekalan barang bercukai syarikat tuan dalam bulan ini dan sebelas (11) bulan sebaik sebelum bulan ini belum lagi 
                melebihi RM500,000.00, tetapi syarikat tuan berniat untuk berdaftar dengan JKDM secara sukarela; atau 
            </td>
        </tr>
        <tr>
            <td valign="top"></td>
            <td valign="top"> (c) </td>
            <td valign="top" style="text-align: justify;padding: 6 6;">
                Syarikat tuan telah berdaftar dengan JKDM, tetapi pendaftaran syarikat tuan hanya berkuat kuasa selepas tarikh Surat 
                ini dikeluarkan
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" style="text-align: justify;padding: 6 6;">
                3.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bagi perenggan 3 (a) dan (b), syarikat tuan adalah dikehendaki untuk 
                memaklumkan nombor pendaftaran CBP syarikat tuan dan tarikh kuat kuasa pendaftaran tersebut kepada Perbadanan 
                untuk pelarasan harga kontrak, dalam tempoh tujuh (7) hari dari tarikh surat dikeluarkan oleh JKDM. Pembayaran 
                CBP ini adalah dikira berdasarkan tuntutan sebenar dan tarikh kuat kuasa pendaftaran CBP syarikat tuan dengan JKDM
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" style="text-align: justify;padding: 6 6;">
                3.2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bagi perenggan 3 (c), sebaik sahaja pendaftaran CBP syarikat tuan telah 
                berkuat kuasa, syarikat tuan adalah dikehendaki memaklumkan tarikh kuat kuasa pendaftaran tersebut kepada Perbadanan 
                untuk pelarasan harga kontrak, dalam tempoh tujuh (7) hari dari tarikh surat dikeluarkan oleh JKDM. Pembayaran CBP 
                ini adalah dikira berdasarkan tuntutan sebenar dan tarikh kuat kuasa pendaftaran CBP syarikat tuan dengan JKDM
            </td>
        </tr>                                
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                4.&nbsp;&nbsp;&nbsp; Pelarasan harga dan kadar harga dalam *Jadual Kadar Harga dan/atau Ringkasan Sebut harga atau Senarai 
                Kuantiti, mengikut yang mana berkenaan setelah diteliti dan diselaraskan oleh Perbadanan tentang kemunasabahannya, yang mana 
                akan menjadi sebahagian daripada terma-terma kontrak. Walau bagaimana pun, Jumlah Harga Kontrak seperti di atas adalah kekal 
                tidak berubah
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                5.&nbsp;&nbsp;&nbsp; Tarikh milik tapak seperti yang disebutkan dalam Syarat-syarat Kontrak ialah pada 
                {{ locale_date($sst->start_working_date) }} Perbadanan telah mengambil polisi insurans melalui 
                <span style="text-transform: capitalize; font-style: italic;">Principal Control Insurance Programme </span>
                untuk semua perolehan Perbadanan dan syarikat tuan <span style="font-weight: bold;">tidak perlu </span> 
                mengambil polisi insurans Kerja, Tanggungan Awam, Pampasan Pekerja dan lain-lain.  Walau bagaimanapun, 
                tuan adalah diingatkan bahawa tiada kerja boleh dibuat <span style="font-weight: bold;">melainkan </span> 
                jika tuan telah mengemukakan kepada Perbadanan dokumen-dokumen berikut:
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;"  colspan="3">
                <table width="100%">
                    <tr>
                        <td width="5%"></td>
                        <td width="8%" valign="top" style="text-align: justify;padding: 4 4;">*(a)</td>
                        <td width="87%" valign="top" style="text-align: justify;padding: 4 4;">
                            suatu bon pelaksanaan yang tidak boleh dibatalkan yang berjumlah 

                            <span style="text-transform: capitalize; font-weight: bold;">Ringgit Malaysia : 
                    
                                @php

                                    $f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
                                    $amaun = money()->toCommon($sst->bon_amount ?? "0" , 2);
                                    $format = explode('.',$amaun);
                                    $formats = substr($amaun,-2);
                                    $partringgit = str_replace(',', '', $format[0]);
                                    $partsen = $format[1];
                                    $partkosongsen = (int)$formats;

                                    if($partkosongsen == '.00'){
                                        echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Sahaja" . '</span>';
                                    }
                                    else{
                                        echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Dan " . $f->format($partkosongsen) . " Sen Sahaja" . '</span>';
                                    }

                                @endphp	
                                ({{ money()->toHuman($sst->bon_amount) }}	)
                            </span> 
                            dan jikalau Bon Pelaksanaan gagal dikemukakan pada tarikh milik tapak, Perbadanan berhak untuk 
                            melaksanakan kaedah Wang Jaminan Pelaksanaan;<br/>
                            <span style="font-size: 10px;">*Bon Pelaksanaan hanya dikenakan kepada perolehan bernilai RM200,00 ke atas sahaja.</span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">b)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            Surat dan Nombor Pendaftaran Majikan di bawah Skim Keselamatan Sosial Pekerja (PERKESO) untuk kontrak ini.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                mengikut ketetapan seperti di <span style="font-weight: bold; font-style: italic;">Lampiran A</span>.  
                Apa-apa kegagalan dalam mematuhi kehendak di perenggan ini dalam tempoh masa yang ditetapkan, boleh 
                mengakibatkan Surat ini terbatal dan Perbadanan tidaklah dengan apa-apa cara jua bertanggungan terhadap 
                tuan <span style="font-weight: bold;">melainkan jika </span>penepian bertulis diberikan oleh orang yang 
                diberi kuasa, bagi kerja yang perlu dibuat dengan segera atau sertamerta apabila kelewatan itu akan memudarat 
                dan menjejaskan perkhidmatan dan kepentingan awam. 
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                6.&nbsp;&nbsp;&nbsp; Setelah arahan dikeluarkan oleh Perbadanan, tuan dikehendaki melaksanakan kerja dalam tempoh 
                yang ditetapkan dan kualiti kerja tersebut hendaklah memuaskan hati serta memenuhi kehendak Perbadanan. Sekiranya 
                tuan gagal melaksanakan kerja dalam tempoh yang ditetapkan, Perbadanan berhak membatalkan arahan yang dikeluarkan 
                dan/atau mengenakan <span style="font-style: italic;">Liquidated & Ascertained Damages</span> (LAD) seperti yang 
                ditetapkan dalam <span style="font-style: italic; font-weight: bold;">Lampiran A</span>.  
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                7.&nbsp;&nbsp;&nbsp; Syarikat tuan juga adalah digalakkan melaksanakan program Skim Latihan 1Malaysia (SL1M) seperti 
                yang ditetapkan oleh Perbadanan.  <br/>
                <span style="font-size: 10px;">
                * Perenggan ini terpakai sekiranya Harga Kontrak <span style="font-weight: bold;">di bawah nilai ambang </span>
                dan syarikat <span style="font-weight: bold;">digalakkan</span> untuk melaksanakan Program SL1M berdasarkan 1PP/PK 1.2. 
                </span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                8.&nbsp;&nbsp;&nbsp; Bagi tujuan program SL1M ini, syarikat tuan adalah dikehendaki untuk: 
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                <table width="100%" border="0">
                    <tr >
                        <td width="5%"></td>
                        <td width="8%" valign="top" style="text-align: justify;padding: 4 4;">(a)</td>
                        <td width="87%" valign="top" style="text-align: justify;padding: 4 4;">
                            mengemukakan Jadual Pelaksanaan Program SL1M berdasarkan tempoh kontrak kepada Sekretariat SL1M untuk kelulusan dalam tempoh dua (2) minggu selepas tarikh pengakuan penerimaan Surat ini oleh syarikat tuan; 
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify; padding: 4 4;">(b)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            melaksanakan program ini mengikut Jadual Pelaksanaan Program SL1M yang diluluskan oleh Sekretariat SL1M; 
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(c)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            mengemaskini maklumat berkaitan pengalaman syarikat melaksanakan program SL1M dalam sistem ePerolehan di Kementerian Kewangan atau sistem di Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB), mengikut mana yang berkaitan;  
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(d)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            mengemukakan sijil atau surat pengesahan oleh Sekretariat SL1M kepada Agensi sebaik sahaja pelaksanaan program SL1M selesai; dan 
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(e)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            mengemukakan laporan berkaitan pelaksanaan program SL1M kepada Sekretariat SL1M.
                        </td>
                    </tr>																								
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                9.&nbsp;&nbsp;&nbsp; Syarikat tuan juga adalah diingatkan bahawa Perbadanan berhak untuk membatalkan Surat ini sekiranya:
            </td>
        </tr>	
    
                    <tr >
                        <td width="5%"></td>
                        <td width="8%" valign="top" style="text-align: justify;padding: 4 4;">(a)</td>
                        <td width="87%" valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan gagal memenuhi mana-mana terma di perenggan 5 dalam tempoh masa yang ditetapkan;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify; padding: 4 4;">(b)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan gagal memenuhi mana-mana terma yang dinyatakan dalam Surat Akuan Pembida Berjaya;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(c)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan gagal memulakan kerja dalam tempoh dua (2) minggu dari tarikh milik tapak;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(d)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan telah membuat salah nyataan (<span style="font-style: italic;">misrepresentation</span>) atau 
                            mengemukakan maklumat palsu semasa berurusan dengan Perbadanan bagi perolehan ini atau melakukan apa-apa 
                            perbuatan lain, seperti memalsukan maklumat dalam Sijil Akuan Pendaftaran Syarikat,
                            mengemukakan bon pelaksanaan atau dokumen lain yang palsu atau yang telah diubah suai;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(e)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan membenarkan Sijil Akuan Pendaftaran Syarikat disalahgunakan oleh individu/syarikat lain;
                        </td>
                    </tr>	
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(f)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan terlibat dalam membuat pakatan harga dengan syarikat-syarikat lain atau apa-apa 
                            pakatan sepanjang proses sebut harga sehingga dokumen kontrak ditandatangani;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(g)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan telah memberikan subkontrak sama ada sepenuhnya atau sebahagiannya 
                            perkhidmatan tanpa kelulusan Perbadanan terlebih dahulu. Sekiranya Perbadanan meluluskan 
                            permohonan syarikat tuan untuk memberikan subkontrak sebahagian kerja atau keseluruhan 
                            kerja, kelulusan tersebut adalah tertakluk kepada syarikat tuan mengikat perjanjian hak 
                            (<span style="font-style: italic;">Deed Of Assignment</span>) dengan Subkontraktor terlebih dahulu;  
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(h)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat gagal menyempurnakan kerja dalam tempoh yang ditetapkan seperti di 
                            <span style="font-weight: bold; font-style: italic;">Lampiran A</span>;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(i)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan gagal mematuhi mana-mana terma/arahan di dalam Sebut Harga;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(j)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan/ pemilik/ rakan kongsi/ pengarah telah disabitkan atas kesalahan jenayah di dalam atau luar Malaysia; 
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(k)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan digulungkan;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(l)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            syarikat tuan membekalkan barang-barang yang tidak tulen, bukan baharu atau yang terpakai;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(m)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            kontraktor gagal/mungkir dalam melaksanakan tanggung jawabnya sepertimana ditetapkan dalam Syarat-syarat Kontrak; atau
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">(n)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            terdapat perkara yang melibatkan kepentingan awam atau keselamatan dan kepentingan negara.
                        </td>
                    </tr>																																																																																															

        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                10.&nbsp;&nbsp;&nbsp; Sekiranya Surat ini dibatalkan atas alasan seperti yang ditetapkan di perenggan 9, Perbadanan tidak akan bertanggungan terhadap apa-apa kerugian syarikat tuan termasuk kerugian masa hadapan. 
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                11.&nbsp;&nbsp;&nbsp; Bersama-sama Surat ini disertakan Surat Akuan Pembida Berjaya, dan Surat Akuan Sumpah Syarikat 
                seperti di <span style="font-style: italic; font-weight: bold;">Lampiran B</span> dan <span style="font-style: italic; font-weight: bold;">Lampiran C</span> 
                untuk ditandatangani oleh syarikat tuan dan dikembalikan bersama-sama dengan Surat ini. 
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                12.&nbsp;&nbsp;&nbsp; Syarikat tuan juga adalah dikehendaki untuk mengemukakan dokumen berikut bersama-sama 
                dengan Surat ini yang telah ditandatangan balas oleh tuan, untuk kelulusan Wakil Perbadanan sebelum 
                memulakan kerja di tapak bina:  
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                <table width="100%">
                    <tr>
                        <td width="5%"></td>
                        <td width="8%" valign="top" style="text-align: justify;padding: 4 4;">*(a)</td>
                        <td width="87%" valign="top" style="text-align: justify;padding: 4 4;">
                            Senarai nama subkontraktor berserta pengalamannya dengan menyatakan bahagian kerja yang terlibat; dan
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">b)</td>
                        <td valign="top" style="text-align: justify;padding: 4 4;">
                            Jadual Pelaksanaan dan Program Kerja Harian.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                13.&nbsp;&nbsp;&nbsp; Berdasarkan kepada Tempoh Siap Kerja yang disebut hargakan selama 
                <span style="font-weight: bold;">{!! $sale->box->period.' '.$sale->box->period_type->name !!}</span>, 
                Tarikh Siap untuk seluruh kerja-kerja di bawah kontrak ini ialah pada <span style="font-weight: bold;"> {{ \Carbon::intlFormat($sst->end_working_date) }}</span>
            </td>
        </tr>	
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                14.&nbsp;&nbsp;&nbsp; Surat ini dihantar kepada syarikat tuan <span style="font-weight: bold;">dalam dua (2) salinan</span>. 
                Sila kembalikan ke pejabat ini salinan asal berserta lampiran yang berkaitan yang telah ditandatangani dengan sempurna oleh 
                syarikat tuan dan saksi syarikat tuan tidak melebihi *3/7/14 hari dari tarikh Surat ini diterima untuk tindakan kami 
                selanjutnya. Apa-apa kegagalan dalam mematuhi kehendak di perenggan ini dalam tempoh masa yang ditetapkan boleh 
                mengakibatkan Surat ini terbatal dan Perbadanan tidaklah dengan apa-apa jua bertanggungan terhadap syarikat tuan.
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                Sekian, terima kasih.
            </td>
        </tr>	
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                <span style="font-weight: bold; text-transform: uppercase;">
                    "Bandar Raya Bestari, Kehidupan Berkualiti"<br/>
                    "Berkhidmat Untuk Negara"
                </span>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: justify;padding: 6 6;" colspan="3">
                Saya yang menurut perintah,<br/><br/><br/>
                .............................................<br/>
                <span style="font-weight: bold; text-transform: uppercase;">({{ (!empty($president)) ? $president->honourary . ' ' . $president->name : "" }} )</span><br/>
                Presiden<br/>
                Perbadanan Putrajaya
            </td>
        </tr>																																																			
    </table>
</div>