<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 100px 50px 70px 100px;
            }

            header {
                position: fixed;
                top: -70px;
                left: 0px;
                right: 0px;
                height: 250px;

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                /* line-height: 35px; */
            }

            footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 30px; 

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;
				
				
            }

			footer .pagenum:before {
      			content: counter(page);
			}

			div.breakNow { page-break-inside:avoid; page-break-after:always; }

			.title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
			}
			
			.title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
			}
			
			.title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

        </style>
    </head>
    <body>

        @php
            $acqCategory = "";
        
            if($sst->acquisition->category->id == 1 || $sst->acquisition->category->id == 2) {
                $acqCategory = "sebut harga";
            }elseif($sst->acquisition->category->id == 3 || $sst->acquisition->category->id == 4) {
                $acqCategory = "tender";
            }
        @endphp
        <!-- Define header and footer blocks before your content -->
        <header>
			<div style="text-align: right; " class="title-header">

                @if($sst->company->gst_registered_no != '')
                    <span style="text-transform:capitalize">SST (Bagi {{ $acqCategory }} Perkhidmatan)</span><br>
                    <span>Untuk Syarikat Yang <b>Telah Berdaftar</b> dengan JKDM Di Bawah Akta 762 Dan</span><br/>
                    <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span><br/>
                    <span><b>(Ada SL1M)</b></span>
                @else
                    @if($sst->sl1m_entity == 0)
                    <span style="text-transform:capitalize">SST (Bagi {{ $acqCategory }} Perkhidmatan)</span><br>
                        <span>Untuk Syarikat Yang <b>Belum Berdaftar</b> dengan JKDM Di Bawah Akta 762 Dan</span><br/>
                        <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span><br/>
                        <span><b>(Tiada SL1M)</b></span>
                    @else
                    <span style="text-transform:capitalize">SST (Bagi {{ $acqCategory }} Perkhidmatan)</span><br>
                        <span>Untuk Syarikat Yang <b>Belum Berdaftar</b> dengan JKDM Di Bawah Akta 762 Dan</span><br/>
                        <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span><br/>
                        <span><b>Ada SL1M</b></span>
                    @endif
                @endif
                   

            </div>
			
			<div style="text-align: left;" class="content">
				@if($sst->acquisition->category->id == '1' || $sst->acquisition->category->id == '2')
					<span>*Sebut Harga : {{ $sst->acquisition->reference }}</span>
				@elseif($sst->acquisition->category->id == '3' || $sst->acquisition->category->id == '4')
					<span>*Tender : {{ $sst->contract_no }}</span>
				@endif				
			</div>
			<br/><br/>
        </header>

        <footer>
			<div class="pagenum-container title-footer">Page <span class="pagenum"></span></div>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
			<br/><br/>

			@include('contract.post.acceptance-letter.report.partials.perkhidmatan.sub-report.sec_info', ['sst' => $sst])

            @if($sst->company->gst_registered_no != '')
                @include('contract.post.acceptance-letter.report.partials.perkhidmatan.sec_daftar_tiada_pelepasan', ['sst' => $sst, 'appointed' => $appointed])
            @else
                @if($sst->sl1m_entity == 0)
                    @include('contract.post.acceptance-letter.report.partials.perkhidmatan.sec_belum_daftar_tiada_pelepasan_tiada_sl1m', ['sst' => $sst, 'appointed' => $appointed])
                @else
                    @include('contract.post.acceptance-letter.report.partials.perkhidmatan.sec_belum_daftar_tiada_pelepasan_ada_sl1m', ['sst' => $sst, 'appointed' => $appointed])
                @endif
            @endif

			<br/>

			@include('contract.post.acceptance-letter.report.partials.perkhidmatan.sub-report.sec_sk', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.perkhidmatan.sub-report.sec_akuan_terima', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.perkhidmatan.sub-report.sec_butiran', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.perkhidmatan.sub-report.sec_akuan_pembida', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.perkhidmatan.sub-report.sec_akuan_sumpah', ['sst' => $sst])

        </main>
    </body>
</html>