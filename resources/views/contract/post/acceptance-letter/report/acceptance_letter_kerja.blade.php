<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 70px 50px 70px 100px;
            }

            header {
                position: fixed;
                top: -50px;
                left: 0px;
                right: 0px;
                height: 180px;

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                /* line-height: 35px; */
            }

            footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 30px; 

                /** Extra personal styles **/
                /* background-color: #03a9f4; */
                color: black;
                text-align: right;
                line-height: 35px;
				
				
            }

			footer .pagenum:before {
      			content: counter(page);
			}

			div.breakNow { page-break-inside:avoid; page-break-after:always; }

			.title {
                font-size: 14px;
                font-family: "Arial, Helvetica, sans-serif";
			}
			
			.title-header {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
			}
			
			.title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }

            .content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
            }

        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
			<div style="text-align: right; " class="title-header">
                @if($sst->acquisition->acquisition_category_id == '1' || $sst->acquisition->acquisition_category_id == '2')
                    @if($period_length)
                        @if($sst->company->gst_registered_no != '')
                            <span>SST Sebut Harga Kerja (<i>one-off</i> dan kurang 4 bulan) (Syarikat Yang <b>Telah Berdaftar</b> Dengan JKDM) Dan</span><br>
                            <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span>
                        @else
                            <span>SST Sebut Harga Kerja (<i>one-off</i> dan kurang 4 bulan) (Syarikat Yang <b>Belum Berdaftar</b> Dengan JKDM) Dan</span><br>
                            <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span>
                        @endif
                    @else
                        <span>SST Sebut Harga Kerja (Syarikat Yang <b>Telah Berdaftar</b> Dengan JKDM) Dan</span><br>
                        <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span>
                    @endif
                @elseif($sst->acquisition->acquisition_category_id == '3' || $sst->acquisition->acquisition_category_id == '4')
                    @if($sst->company->gst_registered_no != '')
                        <span>SST Tender Kerja (Syarikat Yang <b>Telah Berdaftar</b> Dengan JKDM) Dan</span><br>
                        <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span>
                    @else
                        <span>SST Tender Kerja (Syarikat Yang <b>Belum Berdaftar</b> Dengan JKDM) Dan</span><br>
				        <span><b>Tidak Diberi Pelepasan</b> Di Bawah Perintah Cukai Barang Dan Perkhidmatan (Pelepasan) 2014</span>
                    @endif
                @endif

            </div>
			
			<div style="text-align: left;" class="content">
				@if($sst->acquisition->category->id == '1' || $sst->acquisition->category->id == '2')
					<span>*No. {{ $sst->acquisition->category->name }}: {{ $sst->acquisition->reference }}</span>
				@elseif($sst->acquisition->category->id == '3' || $sst->acquisition->category->id == '4')
					<span>*No. {{ $sst->acquisition->category->name }}: {{ $sst->contract_no }}</span>
				@endif
				
			</div>
			<br/><br/>
        </header>

        <footer>
			<div class="pagenum-container title-footer">Page <span class="pagenum"></span></div>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
			<br/><br/>

			@include('contract.post.acceptance-letter.report.partials.kerja.sub-report.sec_info', ['sst' => $sst])

            @if($sst->acquisition->acquisition_category_id == '1' || $sst->acquisition->acquisition_category_id == '2')
                @if($period_length)
                    @if($sst->company->gst_registered_no != '')
                        @include('contract.post.acceptance-letter.report.partials.kerja.sec_sh_daftar_tiada_pelepasan_oneoff', ['sst' => $sst, 'appointed' => $appointed])
                    @else
                        @include('contract.post.acceptance-letter.report.partials.kerja.sec_sh_belum_daftar_tiada_pelepasan_oneoff', ['sst' => $sst, 'appointed' => $appointed])
                    @endif
                @else
                    @include('contract.post.acceptance-letter.report.partials.kerja.sec_sh_daftar_tiada_pelepasan', ['sst' => $sst, 'appointed' => $appointed])
                @endif
            @elseif($sst->acquisition->acquisition_category_id == '3' || $sst->acquisition->acquisition_category_id == '4')
                @if($sst->company->gst_registered_no != '')
                    @include('contract.post.acceptance-letter.report.partials.kerja.sec_t_daftar_tiada_pelepasan', ['sst' => $sst, 'appointed' => $appointed])
                @else
                    @include('contract.post.acceptance-letter.report.partials.kerja.sec_t_belum_daftar_tiada_pelepasan', ['sst' => $sst, 'appointed' => $appointed])
                @endif
            @endif
			<br/>

			@include('contract.post.acceptance-letter.report.partials.kerja.sub-report.sec_sk', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.kerja.sub-report.sec_akuan_terima', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.kerja.sub-report.sec_butiran', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.kerja.sub-report.sec_akuan_pembida', ['sst' => $sst])

			<div class="breakNow"><br></div>

			@include('contract.post.acceptance-letter.report.partials.kerja.sub-report.sec_akuan_sumpah', ['sst' => $sst])

        </main>
    </body>
</html>