@extends('layouts.admin')

@push('styles')
	<style>

	</style>
@endpush

@push('scripts')
	<script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
           /* $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });*/


            $(document).on('click', '.save-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $sst->id ?? ''}}';
                var route_name = 'api.contract.upload-sst';
                var form_id = 'acceptance_letter_form';
                var form = document.forms[form_id];
                var data = new FormData(form);
                data.append('action', '');

                axios.post(route(route_name, id), data).then(response => {
                    swal('', response.data.message, 'success');
                    location.reload();
                });
            });

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = '{{ $sst->id ?? ''}}';
                var route_name = 'api.contract.upload-sst';
                var form_id = 'acceptance_letter_form';
                var form = document.forms[form_id];
                var data = new FormData(form);
                data.append('action', 'submit');

                axios.post(route(route_name, id), data).then(response => {
                    swal('', response.data.message, 'success');
                    location.reload();
                });
            });


            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh
            
        	// button for review

            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_rev_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_rev_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_rev_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($s6) && $s6 = $review)
                @if($s6->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6->remarks) !!}');
                @endif
            @elseif(!empty($s7) && $s7 = $review)
                @if($s7->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif

			@if(isset($sst->vp_sign_date))
                $("#vp_sign_date").val('{{ $sst->vp_sign_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->al_issue_date))
                $("#al_issue_date").val('{{ $sst->al_issue_date->format('d/m/Y') }}');
            @endif
            @if(isset($sst->al_contractor_date))
                $("#al_contractor_date").val('{{ $sst->al_contractor_date->format('d/m/Y') }}');
            @endif        
            @if(isset($sst->report_type_id))
                $("#report_type_id").val('{{ $sst->report_type_id }}').change();
            @endif

        });
	</script>
@endpush

@section('content')
	@component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk: </span>{{ $appointed->acquisition->title ?? '' }}
            <br>
            <span class="font-weight-bold">No. Kontrak: </span>{{ $appointed->acquisition->reference ?? '' }}
        @endslot
    @endcomponent
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#acquisition-details" role="tab" aria-controls="acquisition-details" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Perolehan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#sst" role="tab" aria-controls="sst" aria-selected="false">
                    @icon('fe fe-award')&nbsp;{{ __('SST/LOA') }}
                </a>
            </li>
            @if(!empty($review))
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#sst-reviews" role="tab" aria-controls="sst-reviews" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan') }}
                    </a>
                </li>

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#sst-review-log" role="tab" aria-controls="sst-review-log" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif

                @if((user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'SST') 
                || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S4') && $review->status != 'Selesai') 
                || (!empty($semakan4) && user()->id == $semakan4->approved_by && user()->id != $review->created_by && (empty($semakan5) || $reviewlog->status != 'S5') && $review->status != 'Selesai') 
                || (!empty($semakan5) && user()->id == $semakan5->approved_by && user()->id != $review->created_by && (empty($cetaknotisbpub) || $reviewlog->status != 'CNBPUB') && $review->status != 'Selesai') 
                || (!empty($semakan6) && user()->id == $semakan6->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'S7') && $review->status != 'Selesai') 
                || (!empty($semakan7) && user()->id == $semakan7->approved_by && user()->id != $review->created_by && ($reviewlog->status != 'CNUU') && $review->status != 'Selesai'))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#sst-review" role="tab" aria-controls="sst-review" aria-selected="false">
                            @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                        </a>
                    </li>
                @endif

            @endif
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#bon" role="tab" aria-controls="bon" aria-selected="false">
                    @icon('fe fe-dollar-sign')&nbsp;{{ __('Bon') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                    @icon('fe fe-shield')&nbsp;{{ __('Insurans') }}
                </a>
            </li>
            <!-- <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#approval" role="tab" aria-controls="approval" aria-selected="false">
                    @icon('fe fe-thumbs-up')&nbsp;{{ __('Kelulusan') }}
                </a>
            </li> -->
            @role('penyedia')
                @if(!empty($review) && $review->status == 'Selesai')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#upload" role="tab" aria-controls="upload" aria-selected="false">
                            @icon('fe fe-upload')&nbsp;{{ __('Muat Naik Kelulusan') }}
                        </a>
                    </li>
                @endif
            @endrole
        </ul>
    </div>
    <div class="col-10">
    	<div id="acceptance_letter_form">
    		<input type="hidden" name="company_id" id="company_id">
    		<input type="hidden" name="acquisition_id" id="acquisition_id">
	        @component('components.card')
	            @slot('card_body')
	                @component('components.tab.container', ['id' => 'document'])
	                    @slot('tabs')
	                     	@component('components.tab.content', ['id' => 'acquisition-details', 'active' => true])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.shows.acquisition-details', ['appointed' => $appointed, 'sst' => $sst])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'sst'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.shows.sst', ['appointed' => $appointed, 'sst' => $sst])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'bon'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.shows.bon', ['appointed' => $appointed, 'sst' => $sst])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'insurance'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.shows.insurance', ['appointed' => $appointed, 'sst' => $sst])
	                            @endslot
	                        @endcomponent

	                        @component('components.tab.content', ['id' => 'approval'])
	                            @slot('content')
	                                @include('contract.post.acceptance-letter.partials.shows.approval', ['appointed' => $appointed, 'sst' => $sst])
	                            @endslot
	                        @endcomponent						

	                        <div class="tab-pane fade show" id="sst-reviews" role="tabpanel" aria-labelledby="sst-details-tab">
			                    
	                        	@if(!empty($review))
                                    @if(user()->current_role_login == 'penyedia')
                                    
                                        @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl7 as $s7log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s7log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                        @endif

                                        {{-- @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne7" aria-expanded="false" aria-controls="collapseOne7">
                                                            Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne7" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6 as $s6log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5 as $s5log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                            Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4 as $s4log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                            Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3 as $s3log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif --}}

                                        @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2 as $s2log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1 as $s1log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                        {{-- pegawai bpub --}}
                                        @if(user()->department->id == '9')

                                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        @endif

                                        {{-- pegawai pelaksana --}}
                                        @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2 as $s2log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1 as $s1log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                        {{-- pegawai undang-undang --}}
                                        @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7 as $s7log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                            Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6 as $s6log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- pegawai pelaksana --}}
                                        @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2 as $s2log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1 as $s1log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @else
                                        {{-- pegawai undang-undang --}}
                                        @if(user()->department->id == '3')

                                            @if(!empty($cetaknotisuulog) && !empty($cetaknotisuulog->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                                                Ulasan Semakan Oleh {{$cetaknotisuulog->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne6" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl7 as $s7log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s7log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s7log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan7log) && !empty($semakan7log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$semakan7log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl6 as $s6log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s6log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s6log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        @endif

                                        {{-- pegawai bpub --}}
                                        @if(user()->department->id == '9')

                                            @if(!empty($cetaknotisbpubs) && !empty($cetaknotisbpubs->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                                Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl5 as $s5log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s5log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan5log) && !empty($semakan5log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                                Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl4 as $s4log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s4log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan4log) && !empty($semakan4log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                                Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl3 as $s3log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s3log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                                    <div class="mb-0 card card-primary">
                                                        <div class="card-header" role="tab" id="headingOne2">
                                                            <h4 class="card-title">
                                                                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                    Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                            <div class="card-body">
                                                                @foreach($sl2 as $s2log)
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Terima</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Tarikh Hantar</div>
                                                                            <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <div class="text-muted">Status</div>
                                                                            <p>{!! $s2log->reviews_status !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="text-muted">Ulasan</div>
                                                                            <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif

                                        @endif

                                        {{-- pegawai pelaksana --}}
                                        @if((!empty($review)) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)
                                            
                                            @if(!empty($semakan3log) && !empty($semakan3log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if(!empty($semakan2log) && !empty($semakan2log->requested_by))
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl1 as $s1log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s1log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        @endif
                                    @endif
                                @endif

			                </div>
			                @if(!empty($sst))
				                <div class="tab-pane fade show " id="sst-review-log" role="tabpanel" aria-labelledby="sst-review-log-tab">
				                    <form id="log-form">
				                        <div class="row justify-content-center w-100">
				                            <div class="col-12 w-100">
				                                @include('contract.pre.box.partials.scripts')
				                                @component('components.card')
				                                    @slot('card_body')
				                                        @component('components.datatable', 
				                                            [
				                                                'table_id' => 'contract-pre-box',
				                                                'route_name' => 'api.datatable.contract.review-log-sst',
				                                                'param' => 'acquisition_id=' . $sst->acquisition_id,
				                                                'columns' => [
				                                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
				                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                    ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
				                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                    ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                    ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
				                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
				                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
				                                                ],
				                                                'headers' => [
				                                                    __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'), __('Status'), __('')
				                                                ],
				                                                'actions' => minify('')
				                                            ]
				                                        )
				                                        @endcomponent
				                                    @endslot
				                                @endcomponent
				                            </div>
				                        </div>
				                    </form>
				                </div>
			                @endif
			                @if(!empty($sst))
    			                <div class="tab-pane fade show" id="sst-review" role="tabpanel" aria-labelledby="sst-review-tab">

    		                        @include('components.forms.hidden', [
    		                            'id' => 'id',
    		                            'name' => 'id',
    		                            'value' => ''
    		                        ])

    		                        @if(!empty($sst))
    			                        @include('components.forms.hidden', [
    			                            'id' => 'acquisition_rev_id',
    			                            'name' => 'acquisition_id',
    			                            'value' => $sst->acquisition_id
    			                        ])
    		                        @endif

    		                        @if(!empty($review))
    		                        <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
    		                        <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

    		                        <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
    		                        @endif

    		                        <div class="row">
                                        <div class="col-6">
                                            @include('components.forms.input', [
                                                'input_label' => __('Nama Pegawai Semakan'),
                                                'id' => '',
                                                'name' => '',
                                                'value' => user()->name,
                                                'readonly' => true
                                            ])
                                        </div>
    	                                @include('components.forms.hidden', [
    	                                    'id' => 'requested_by',
    	                                    'name' => 'requested_by',
    	                                    'value' => user()->id
    	                                ])
    	                                @include('components.forms.hidden', [
    	                                    'id' => 'approved_by',
    	                                    'name' => 'approved_by',
    	                                    'value' => user()->supervisor->id
    	                                ])

                        				@if(!empty($review_previous))
    		                                <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
    			                            <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
    		                                <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
    		                        		<input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                        					<input type="text" id="type" name="type" value="SST" hidden>
                    					@endif

                                        <div class="col-3">
                                            @if(!empty($review))
                                                @include('components.forms.input', [
                                                    'input_label' => __('Tarikh Terima'),
                                                    'id' => 'requested_at',
                                                    'name' => 'requested_at',
                                                ])
                                            @endif
                                        </div>
                                        <div class="col-3">
                                            @if(!empty($review))
                                                @include('components.forms.input', [
                                                    'input_label' => __('Tarikh Hantar'),
                                                    'id' => 'approved_ats',
                                                    'name' => 'approved_at',
                                                ])
                                            @endif
                                        </div>
    		                        </div>

    		                        @if(!empty($review) && $review->created_by != user()->id)
    		                            @include('components.forms.textarea', [
    		                                'input_label' => __('Ulasan'),
    		                                'id' => 'remarks',
    		                                'name' => 'remarks'
    		                            ])
    		                        @else

    		                        @endif
                                
                                    <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                    <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                                    <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                                    <div class="btn-group float-right">
                                        <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                        <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                            @icon('fe fe-send') {{ __('Teratur') }}
                                        </button>
                                    </div>
    			                </div>
			                @endif

							@component('components.tab.content', ['id' => 'upload'])
                                @slot('content')
                                    @include('contract.post.acceptance-letter.partials.shows.upload', ['appointed' => $appointed, 'sst' => $sst])                              
	                            @endslot
	                        @endcomponent	

	                    @endslot
	                @endcomponent
	                @slot('card_footer')
	                    <div class="btn-group float-right">
                            @if(!empty($sst))
                                @if(!empty($sst->sofa_signature_date))
                                    <a href="{{ route('contract.post.contract-completed.showPerolehan', $sst->hashslug) }}" 
                                        class="btn btn-default border-primary">
                                        {{ __('Kembali') }}
                                    </a>
                                @else
                                    <a href="{{ route('acceptance-letter.index') }}" 
                                        class="btn btn-default border-primary">
                                        {{ __('Kembali') }}
                                    </a>
                                    @role('penyedia')
                                        @if(!empty($review) && $review->status == 'Selesai' && $sst->status != '3')
                                            <button type="submit" class="btn btn-primary save-action-btn">
                                                @icon('fe fe-save') {{ __('Simpan') }}
                                            </button>
                                            <button type="submit" class="btn btn-success submit-action-btn">
                                                @icon('fe fe-save') {{ __('Selesai') }}
                                            </button>                                            
                                        @endif
                                    @endrole
                                @endif
                            @endif
	                    </div>
	                @endslot
	            @endslot
	        @endcomponent
    	</div>
    </div>
</div>
@endsection