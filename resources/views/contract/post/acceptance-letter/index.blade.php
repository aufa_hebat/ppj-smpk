@extends('layouts.sst.admin')

@section('content')
	@include('contract.post.acceptance-letter.partials.scripts')
			@component('components.forms.hidden-form', 
				[
					'id' => 'destroy-record-form',
					'action' => route('contract.pre.destroy', 'dummy')
				])
				@slot('inputs')
					@method('DELETE')
				@endslot
			@endcomponent


@if($tag == '6MonthToEnd' || $tag == 'ContractOnGoing' || $tag == 'contractsNotSignedYet' || $tag == 'contractsDoneButNotSofaYet')
                            @component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'acceptance-letter',
							'route_name' => 'api.datatable.contract.acceptance-letter',
                                                        'param' => ['tag' => $tag],
							'columns' => [
								['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'acquisition_title', 'title' => __('Perolehan'), 'defaultContent' => '-'],
								['data' => 'company_name', 'title' => __('Syarikat Berjaya'), 'defaultContent' => '-'],
								['data' => 'created_at', 'title' => __('table.created_at'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Perolehan'), __('Perolehan'), __('Syarikat Berjaya'), __('table.created_at'), __('table.action')
							],
							'actions' => minify(view('contract.post.acceptance-letter.partials.actions_sst')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
                                                    
                                                   
                                               
                            @else
                             <div class="row justify-content-center">
		<div class="col">
                    
			
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'acceptance-letter',
							'route_name' => 'api.datatable.contract.acceptance-letter',
                                                        'param' => ['tag' => $tag],
							'columns' => [
								['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
								['data' => 'acquisition_title', 'title' => __('Perolehan'), 'defaultContent' => '-'],
								['data' => 'company_name', 'title' => __('Syarikat Berjaya'), 'defaultContent' => '-'],
								['data' => 'created_at', 'title' => __('table.created_at'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Perolehan'), __('Perolehan'), __('Syarikat Berjaya'), __('table.created_at'), __('table.action')
							],
							'actions' => minify(view('contract.post.acceptance-letter.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
                            @endif




@endsection
