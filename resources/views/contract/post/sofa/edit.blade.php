@extends('layouts.admin')
@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
            /*$('#sofa-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });*/

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#sofa-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh

            // button for review

            $('#send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });
            $('#savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#saveStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#sentStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#rejectStatus').val(),
                    acquisition_id : $('#acquisition_id').val(),
                    requested_by : $('#requested_by').val(),
                    progress : $('#progress').val(),
                    task_by : $('#task_by').val(),
                    law_task_by : $('#law_task_by').val(),
                    approved_by : $('#approved_by').val(),
                    created_by : $('#created_by').val(),
                    approved_at : $('#approved_at').val(),
                    type : $('#type').val(),
                    status : $('#status').val(),
                    department : $('#department').val(),
                    remarks : $('#remarks').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#savDraf').hide();
                                $('#savSent').hide();
                                $('#rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_ats').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif
        });
    </script>
@endpush
@section('content')
    
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="sofa-tab-content" role="tablist">
                <li class="list-group-item" id="contract">
                    <a class="list-group-item-action active" data-toggle="tab" href="#contract-details" role="tab" aria-controls="contract-details" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Maklumat Kontrak') }}
                    </a>
                </li>
                <li class="list-group-item" id="akuan">
                    <a class="list-group-item-action" data-toggle="tab" href="#akuan-details" role="tab" aria-controls="akuan-details" aria-selected="false">
                        @icon('fe fe-dollar-sign')&nbsp;{{ __('Perakuan Akaun') }}
                    </a>
                </li>     
                <li class="list-group-item" id="subcontractor">
                    <a class="list-group-item-action" data-toggle="tab" href="#subcontractor-details" role="tab" aria-controls="subcontractor-details" aria-selected="false">
                        @icon('fas fa-users')&nbsp;{{ __('Subkontraktor') }}
                    </a>
                </li>    
                <li class="list-group-item" id="statement">
                    <a class="list-group-item-action" data-toggle="tab" href="#statement-details" role="tab" aria-controls="statement-details" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Penyata') }}
                    </a>
                </li>  
                {{--  <li class="list-group-item" id="final">
                    <a class="list-group-item-action" data-toggle="tab" href="#final-details" role="tab" aria-controls="final-details" aria-selected="false">
                        @icon('fas fa-calculator')&nbsp;{{ __('IPC Akhir') }}
                    </a>
                </li>         --}}
                @if(!empty($review))
                
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#sst-reviews" role="tab" aria-controls="sst-reviews" aria-selected="false">
                            @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan') }}
                        </a>
                    </li>

                    @if(user()->current_role_login == 'administrator')
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#sst-review-log" role="tab" aria-controls="sst-review-log" aria-selected="false">
                                @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                            </a>
                        </li>
                    @endif

                    @if((user()->id == $review->approved_by && user()->id != $review->created_by && $review->type == 'SOFA') || (!empty($semakan3) && user()->id == $semakan3->approved_by && user()->id != $review->created_by && (empty($semakan4))) || (!empty($semakan4) && user()->id == $semakan4->approved_by && user()->id != $review->created_by && (empty($semakan5))) || (!empty($semakan5) && user()->id == $semakan5->approved_by && user()->id != $review->created_by && (empty($cetaknotisbpub))))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#sst-review" role="tab" aria-controls="sst-review" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai') }}
                            </a>
                        </li>
                    @endif

                @endif      
                @role('penyedia')  
                    @if(!empty($review) && $review->status == 'Selesai')                     
                        <li class="list-group-item" id="upload">
                            <a class="list-group-item-action" data-toggle="tab" href="#upload-sofa" role="tab" aria-controls="upload-sofa" aria-selected="false">
                                @icon('fa fa-upload')&nbsp;{{ __('Muat Naik Sofa') }}
                            </a>
                        </li>        
                    @endif    
                @endrole                                        
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'sofa'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'contract-details', 'active' => true])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.contract', ['sst' => $sst])
                                @endslot
                            @endcomponent  
                            @component('components.tab.content', ['id' => 'akuan-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.akuan', ['sst' => $sst])
                                @endslot
                            @endcomponent 
                            @component('components.tab.content', ['id' => 'subcontractor-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.contractor', ['sst' => $sst])
                                @endslot
                            @endcomponent 
                            @component('components.tab.content', ['id' => 'statement-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.statement', ['sst' => $sst])
                                @endslot
                            @endcomponent   
                            @component('components.tab.content', ['id' => 'final-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.final', ['sst' => $sst])
                                @endslot
                            @endcomponent 
                            <div class="tab-pane fade show" id="sst-reviews" role="tabpanel" aria-labelledby="sst-details-tab">
                                
                                @role('penyedia')

                                    @if((!empty($review)) && (user()->id == $review->created_by))

                                        {{-- penyedia --}}
                                        @if(!empty($cetaknotisbpubs))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5 as $s5log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                            Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4 as $s4log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                            Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3 as $s3log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan3log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2 as $s2log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1 as $s1log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                @else

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubs))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubs->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne5" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5 as $s5log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="false" aria-controls="collapseOne3">
                                                            Ulasan Semakan Oleh {{$semakan5log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne3" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4 as $s4log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4">
                                                            Ulasan Semakan Oleh {{$semakan4log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne4" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3 as $s3log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan3log))
                                            @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                                Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2 as $s2log)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2log->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($review) && !empty($review->create) && !empty($review->create->department) && $review->create->department->id == user()->department->id)

                                        @if(!empty($semakan3log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                                                            Ulasan Semakan Oleh {{$semakan3log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2 as $s2log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2log))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Ulasan Semakan Oleh {{$semakan2log->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1 as $s1log)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1log->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1log->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1log->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                @endrole

                            </div>
                            @if(!empty($sst))
                                <div class="tab-pane fade show " id="sst-review-log" role="tabpanel" aria-labelledby="sst-review-log-tab">
                                    <form id="log-form">
                                        <div class="row justify-content-center w-100">
                                            <div class="col-12 w-100">
                                                @include('contract.pre.box.partials.scripts')
                                                @component('components.card')
                                                    @slot('card_body')
                                                        @component('components.datatable', 
                                                            [
                                                                'table_id' => 'contract-pre-box',
                                                                'route_name' => 'api.datatable.contract.review-log-sofa',
                                                                'param' => 'acquisition_id=' . $sst->acquisition_id,
                                                                'columns' => [
                                                                    ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                    ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                    ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                    ['data' => 'tarikh', 'title' => __('Tarikh Semakan'), 'defaultContent' => '-'],
                                                                    ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                                ],
                                                                'headers' => [
                                                                    __('Bil'),__('Penyemak'), __('Ulasan Semakan'), __('Tarikh Semakan'), __('Status'), __('')
                                                                ],
                                                                'actions' => minify('')
                                                            ]
                                                        )
                                                        @endcomponent
                                                    @endslot
                                                @endcomponent
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                            @if(!empty($sst))
                                <div class="tab-pane fade show" id="sst-review" role="tabpanel" aria-labelledby="sst-review-tab">
                                    <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                        @csrf

                                        @include('components.forms.hidden', [
                                            'id' => 'id',
                                            'name' => 'id',
                                            'value' => ''
                                        ])

                                        @if(!empty($sst))
                                            @include('components.forms.hidden', [
                                                'id' => 'acquisition_id',
                                                'name' => 'acquisition_id',
                                                'value' => $sst->acquisition_id
                                            ])
                                        @endif

                                        @if(!empty($review))
                                        <input type="hidden" id="task_by" name="task_by" value="{{$review->task_by}}">
                                        <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                        <input type="hidden" id="created_by" name="created_by" value="{{$review->created_by}}">
                                        @endif

                                        <div class="row">
                                            <div class="col-6">
                                                @include('components.forms.input', [
                                                    'input_label' => __('Nama Pegawai Semakan'),
                                                    'id' => '',
                                                    'name' => '',
                                                    'value' => user()->name,
                                                    'readonly' => true
                                                ])
                                            </div>
                                                @include('components.forms.hidden', [
                                                    'id' => 'requested_by',
                                                    'name' => 'requested_by',
                                                    'value' => user()->id
                                                ])
                                                @include('components.forms.hidden', [
                                                    'id' => 'approved_by',
                                                    'name' => 'approved_by',
                                                    'value' => user()->supervisor->id
                                                ])

                                            @if(!empty($review_previous))
                                                <input type="text" id="created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                                <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                                <input type="text" id="task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                                <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                                <input type="text" id="type" name="type" value="SOFA" hidden>
                                            @endif

                                            <div class="col-3">
                                                @if(!empty($review))
                                                    @if(!empty($review->status))
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Terima'),
                                                            'id' => 'requested_at',
                                                            'name' => 'requested_at',
                                                        ])
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="col-3">
                                                @if(!empty($review))
                                                    @if(!empty($review->status))
                                                        @include('components.forms.input', [
                                                            'input_label' => __('Tarikh Hantar'),
                                                            'id' => 'approved_ats',
                                                            'name' => 'approved_at',
                                                        ])
                                                    @endif
                                                @endif
                                            </div>
                                        </div>

                                        @if(!empty($review) && $review->created_by != user()->id)
                                            @include('components.forms.textarea', [
                                                'input_label' => __('Ulasan'),
                                                'id' => 'remarks',
                                                'name' => 'remarks'
                                            ])
                                        @else

                                        @endif
                                        
                                        <input id="rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                        <input id="saveStatus" name="saveStatus" type="hidden" value="1" />
                                        <input id="sentStatus" name="sentStatus" type="hidden" value="1" />

                                        <div class="btn-group float-right">
                                            @if(!empty($review) && $review->created_by != user()->id)
                                                <button type="button" id="rejSent" class="btn btn-danger btn-default border-primary ">
                                                    @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                                </button>
                                                <button type="button" id="savDraf" class="btn btn-primary btn-group float-right ">
                                                    @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                                </button>
                                            @endif
                                            <button type="button" id="savSent" class="btn btn-default float-middle border-default ">
                                                @icon('fe fe-send') {{ __('Teratur') }}
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            @endif                                                                      
                            @component('components.tab.content', ['id' => 'upload-sofa'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.upload', ['sst' => $sst])
                                @endslot
                            @endcomponent
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

