@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {

        $(document).on('click', '.print-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('sofaRpt', {hashslug:id}));
		});

        $(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.sofa.edit', id));
		});	

        $(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.sofa.show', id));
		});				
    });
	
</script>
@endpush
