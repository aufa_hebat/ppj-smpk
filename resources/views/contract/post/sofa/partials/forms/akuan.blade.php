<div style="text-align: right; font-size: 14px;">
<span>BPUB/SOFA/AKUAN MUKTAMAD</span>
</div>
<div><br/></div>
<div style="text-align: center; font-weight: bold; text-transform: uppercase; padding:3px 3px;">
<span>Perbadanan Putrajaya</span>
</div>
<div style="text-align: center; font-weight: bold; text-transform: uppercase; padding:3px 3px;">
<span>Perakuan akaun dan bayaran muktamad</span>
</div>
<div class="row" style="text-align: center; font-weight: bold; text-transform: uppercase; padding:3px 3px;">
<table width="100%">
	<tr>
		<td width="50%">Peruntukan Pembangunan : </td>
		<td width="25%">Maksud : </td>
		<td width="25%">Butiran : </td>
	</tr>
</table>
</div>
<div style="text-align: left; text-transform: uppercase; padding:4px 4px; font-size:14px;">
<table width="100%">
	<tr>
		<td width="25%" valign="top">Tajuk Kerja</td>
		<td width="5%" valign="top"> : </td>
		<td width="70%" style="text-weight: bold;">{{ $sst->acquisition->title }}</td>
	</tr>
	<tr>
		<td valign="top">Nama &amp; Alamat Kontraktor</td>
		<td valign="top"> : </td>
		<td>
			<span class="text-weight: bold;">{{ $sst->company->company_name }}</span><br>
			@if(!empty($sst->company->addresses[0]))
				@if(!empty($sst->company->addresses[0]->primary))
					{{ $sst->company->addresses[0]->primary }}<br>
				@endif
				@if(!empty($sst->company->addresses[0]->secondary))
					{{ $sst->company->addresses[0]->secondary }}<br>
				@endif
				@if(!empty($sst->company->addresses[0]->postcode))
					{{ $sst->company->addresses[0]->postcode }}, 
					{{ $sst->company->addresses[0]->state }}   
				@endif
			@endif
		</td>
	</tr>
	<tr>
		<td>No. Kontrak</td>
		<td> : </td>
		<td style="text-weight: bold;">
			{{ $sst->acquisition->reference }}
		</td>
	</tr>
</table>
</div>
<div class="row" style="text-align: left; font-weight: bold; text-transform: uppercase; padding:4px 4px; font-size:14px;">
<table width="100%">
	<tr>
		<td width="50%">Jumlah Harga Asal Kontrak</td>
		<td width="25%"></td>
		<td width="5%"> RM </td>
		<td width="20%" style="text-align: right;">{{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}</td>
	</tr>

	@php
		$total_sum_reduced = 0.0;
		$total_sum_additional = 0.0;
		$total_overal = 0.0;
	@endphp

	@if($ppjhk->count() > 0)
		@foreach($ppjhk as $index => $vod)
			
			@php
				$sum = 0;
				$sum = $vod->elements->pluck('net_amount')->sum() + $vod->cancels->pluck('amount')->sum();
				$total_overal = $total_overal + $sum;
			@endphp

			{{--  @php

				$sum_reduced = 0.0;
				$sum_additional = 0.0;

				foreach($vo_element as $index => $voe) {
					if($voe->variationOrder->id == $vod->id){
						$sum_reduced = $sum_reduced + $voe->estimated_reduced_amount; 
						$sum_additional = $sum_additional + $voe->estimated_additional_amount; 

						$sum_add_red    =   $sum_additional - $sum_reduced;					
					}
				}

				if($sum_additional > $sum_reduced){
					$total_sum_additional = $total_sum_additional + $sum_add_red;
				}else{
					$total_sum_reduced = $total_sum_reduced + $sum_add_red;
				}
				
				
				$total_overal = $total_sum_additional + $total_sum_reduced;				
			@endphp  --}}

			<tr>
				<td>
					<span style="text-transform: uppercase;">
						ppjhk No. {{ $vod->no }}
					</span>
				</td>
				<td>
					@if($sum > 0)
						{{ money()->toHuman($sum ?? "0" , 2) }}
					@else
						({{ money()->toHuman($sum ?? "0" , 2) }})
					@endif
				</td>
				<td colspan="2"></td>
			</tr>
		@endforeach
	@endif

	<tr>
		<td></td>
		<td></td>
		<td>RM</td>
		<td style="text-align: right;">
			@if($total_overal > 0)
				{{ money()->toCommon($total_overal ?? "0" , 2) }}
			@else
				({{ money()->toCommon($total_overal ?? "0" , 2) }})
			@endif
		</td>
	</tr>
	<tr>
		<td>Harga Muktamad Kontrak</td>
		<td></td>
		<td> RM </td>
		<td style="border-top: 1px solid #959594; text-align: right;">
			@php
				$total_last_payment = $appointed->offered_price;
				$total_last_payment = $total_last_payment + $total_overal;

				//$total_reduced = ($prev_total_invoice + ($total_adv_amt - $prev_gv_adv_amt) + $total_lad);
				$total_reduced = ($prev_total_invoice + $prev_gv_adv_amt + $total_lad);
				$total_payment = ($total_last_payment - $total_reduced);
				$total_wjp = 0;
				$total_other_amount = 0;
				$total_contractor = 0;

				if($sst->bon->bon_type == 4){
					$total_wjp = $wjp_amt - $wjp_lepas;							
				}

				$total_other_amount = $total_payment - $total_wjp - $total_balanced;
				$total_contractor = $total_other_amount + $total_wjp;
			
			@endphp
			{{ money()->toCommon($total_last_payment ?? "0" , 2) }}
		</td>
	</tr>						
</table>
</div>
<div><hr/></div>
<div class="row" style="text-align: left; padding:3px 3px; font-size:14px;">
<table width="100%">
	<tr style="text-align: left; font-weight: bold; text-transform: uppercase; padding:3px 3px;">
		<td width="5%">A.</td>
		<td width="70%">Butir-butir Jumlah Potongan / Kurangan</td>
		<td width="5%"></td>
		<td width="20%"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>1. Bayaran Interim Terdahulu No 1 @if($last_ipc->ipc_no > 1) - {{ $last_ipc->ipc_no }} @endif</td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; ">{{ money()->toCommon($prev_total_invoice ?? "0" , 2) }}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>2. Bayaran Wang Pendahuluan </td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; ">
			 {{ money()->toCommon($deposit->total_amount ?? "0" , 2) }} 
			{{-- {{ money()->toCommon($total_adv_amt-$prev_gv_adv_amt ?? "0" , 2) }} --}}
		</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>3. Potongan lain jika ada</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;a) Gantirugi Tertentu Dan Ditetapkan @ RM {{money()->toCommon($sst->document_LAD_amount) ?? "0" , 2  }} / hari </td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; ">
			{{ money()->toCommon($total_lad ?? "0" , 2) }}			
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;b) Kerja Membaiki Kecacatan</td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; ">0.00</td>
	</tr>
	<!-- <tr>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;c) Surcharge 10% untuk kerja sebutharga</td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; "></td>
	</tr> -->
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center; font-weight: bold; text-transform: uppercase;">Bayaran Muktamad Di Bawah Kontrak</td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; border-top: 1px solid #959594; border-bottom: 1px double #959594;">
			{{ money()->toCommon($total_payment ?? "0" , 2) }}
		</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tarikh : .................</td>
		<td colspan="2">
		..................................<br>
		<span style="text-align: center; font-size: 13px;">(Pegawai Pengesyor / wakil P.P)</span>
		</td>
	</tr>
</table>
</div>
<div><hr/></div>
<div class="row" style="text-align: left; padding:3px 3px; font-size:14px;">
<table width="100%">
	<tr>
		<td width="5%" style="text-align: left; font-weight: bold;" valign="top">B. </td>
		<td width="95%" colspan="3">

			<span>
				Saya dengan ini memperakukan bahawa bayaran muktamad di bawah ini berjumlah 
				<span style="text-transform: capitalize; font-weight: bold;">Ringgit Malaysia :
				
				@php

					$f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
					$amaun = money()->toCommon($total_payment ?? "0" , 2);
					$format = explode('.',$amaun);
					$formats = substr($amaun,-2);
					$partringgit = str_replace(',', '', $format[0]);
					$partsen = $format[1];
					$partkosongsen = (int)$formats;

					if($partkosongsen == '.00'){
						echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Sahaja" . '</span>';
					}
					else{
						echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Dan " . $f->format($partkosongsen) . " Sen Sahaja" . '</span>';
					}

				@endphp					
				
				({{ money()->toHuman($total_payment ?? "0" , 2) }}	)</span>
				menunjukkan baki muktamad di bawah kontrak adalah patut dan kena dibayar oleh Perbadanan seperti berikut :
			</span>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width="95%" colspan="3">1.&nbsp;&nbsp;&nbsp;Kontraktor Utama</td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="65%">&nbsp;&nbsp;&nbsp;(i) Nilai kerja dan amaun-amaun lain</td>
		<td width="15%">
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				{{ money()->toCommon($total_other_amount) }}
			</span>
		</td>
		<td width="15%"></td>
	</tr>
	<tr>
		<td></td>
		<td>&nbsp;&nbsp;&nbsp;(ii) Pelepasan Deposit Wang Jaminan Pelaksanaan</td>
		<td>
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				@if($sst->bon->bon_type == 4)
					{{ money()->toCommon($total_wjp) }}
				@endif
			</span>
		</td>
		<td>
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				{{ money()->toCommon($total_contractor) }}
			</span>			
		</td>
	</tr>	
	<tr>
		<td></td>
		<td colspan="2">2.&nbsp;&nbsp;&nbsp;Kepada Subkontraktor / Penerima Bayaran seperti di Lampiran A</td>
		<td width="20%">
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				{{ money()->toCommon($total_balanced) }}
			</span>			
		</td>
	</tr>	
	<tr>
		<td></td>
		<td>
			3.&nbsp;&nbsp;&nbsp;Kredit Hasil Bagi Gantirugi Tertentu Dan Ditetapkan<br>
			&nbsp;&nbsp;&nbsp;@................... selama ...............hari
		</td>
		<td width="20%"></td>
		<td width="20%">
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">0.00</span>			
		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="2"  style="text-align: center; text-transform: uppercase; font-weight: bold;">Jumlah Bayaran Muktamad</td>
		<td style="text-align: right; font-weight: bold; border-top: 1px solid #959594; border-bottom: 1px double #959594;">
			<span style="text-align: left"> RM </span>
			{{ money()->toCommon($total_payment ?? "0" , 2) }}		
		</td>
	</tr>
	<tr>
		<td colspan="4">
			Akuan Statutori atau Perakuan yang ditandatangani oleh atau bagi pihak Ketua Pengarah Buruh yang dikehendaki 
			di bawah fasal 6.2.2** syarat-syarat kontrak telah dikemukakan oleh kontraktor.
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Tarikh : .........................</td>
		<td>
			..............................<br>
			Wakil Perbadanan
		</td>
	</tr>
</table>
</div>

<div><hr/></div>
<div class="row" style="text-align: left; padding:3px 3px; font-size:14px;">
*&nbsp;&nbsp;&nbsp;Tambahan/potongan yang telah diluluskan seperti Penyata Pelarasan Harga Kontrak<br>
**&nbsp;&nbsp;&nbsp;Potong yang tidak berkaitan
</div>

<div class="row" style="text-align: justify; padding:3px 3px; text-transform: uppercase; font-weight: bold;">
C. Pengakuan Persetujuan Kontraktor Ke Atas Perakuan Akaun Dan Bayaran Muktamad
</div>
<div><br/><br/></div>
<div class="row" style="text-align: justify; padding:3px 3px;">
<table width="100%">
	<tr>
		<td colspan="2">
			<span style="font-style: italic;">
				(Kontraktor dikehendaki menurunkan tandatangan dan mengembalikan dokumen ini dalam tempoh tiga puluh (30) hari dari tarikh penerimaan jika Perakuan Bayaran Muktamad ini dipersetujui.  Jika kontraktor gagal berbuat demikian, pejabat ini akan meneruskan dengan bayaran muktamad)
			</span>
		</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2">
			<span>
				Saya/Kami yang bertandatangani di bawah ini mengaku penerimaan Perakuan Akaun dan bayaran Muktamad di atas dan setelah meneliti butir-butir terkandung di dalamnya, <b>BERSETUJU</b> dengan Bayaran Muktamad yang diperakukan dan <b>mengaku Saya/Kami tidak ada tuntutan lanjut</b> di bawah kontrak ini.
			</span>
		</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td width="50%">
			..........................................<br>
			Tandatangan Saksi<br>
			Nama Penuh:<br>
			No K.P :<br>
			Alamat : <br>
			Tarikh :
		</td>
		<td  width="50%">
			..........................................<br>
			Tandatangan Kontraktor<br>
			Nama Penuh:<br>
			No K.P :<br>
			Alamat : <br>
			Tarikh :
		</td>			
	</tr>
</table>
</div>
