@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($sofa) && ($sofa->documents->count() > 0))
                @foreach($sofa->documents as $doc)
                    t.row.add( [
                        '<div class="form-group">\n' +
                        '   <div class="col input-group">\n' +
                        '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                        '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                        '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                        '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                        '   </div>\n' +
                        '</div>',
                        '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                    ] ).draw( false );

                    counter++;
                @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow').click();
            @endif
            

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            });

            $(document).on('click', '#sofa-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                console.log(data);
                console.log(form);
                console.log(route_name);
                console.log(form_id);
                console.log(id);

                axios.post(route(route_name, id), data).then( (response) => {
                    swal('{!! __('Dokumen Sofa') !!}', response.data.message, 'success');
                    redirect(route('contract.post.sofa.index'));
                }).catch((error)=>{
                    console.log(error.response);
                });
            });
        });
    </script>
@endpush

<h4>Muat Naik Dokumen SOFA</h4>
<div class="row">

    <div class="col-12">
        <form id="sofa-form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            @method('PUT')

            <div class="row">
                <div class="col-12">
                @include('components.forms.datetimepicker', [
                    'input_label' => __('Tarikh Pengesahan SOFA'),
                    'id' => 'signature_date',
                    'name' => 'signature_date',
                    'config' => ['format' => config('datetime.display.date')],
                ])
                </div>
            </div>
            <div id="uploads">
                <hr>
                <h4>Dokumen</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="float-right">
                            <button type="button" id="addRow" href="#" class="btn btn-primary">
                                @icon('fe fe-plus')
                                {{ __('Tambah Dokumen') }}
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table id="tblUpload" class="table table-sm table-transparent">
                                <thead>
                                <tr>
                                    <th>Muat Naik Dokumen</th>
                                    <th width="70px">Hapus</th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </form>
        <div class="btn-group float-right" id="submit">
	        {{ html()->a(route('contract.post.sofa.index'), __('Batal'))->class('btn btn-outline-default') }}
	        <button type="submit" class="btn btn-primary float-right submit-action-btn" id="sofa-submit"
			    data-route="api.contract.acquisition.sofa.update"
			    data-form="sofa-form">
	            @icon('fe fe-save') {{ __('Simpan') }}
	        </button>
            
        </div>
    </div>
</div>