<style type="text/css">
	.bordered {
    	border-color: #959594;
    	border-style: solid;
    	border-width: 1px;
	}
</style>
<div style="text-align: center; text-transform: uppercase; text-decoration: underline;">
    <span>Penyata Pelaksanaan Harga Kontrak</span>
</div>
<br>
<div class="row">
	<div class="col-12">
		<table width="100%">
			<tr>
				<td width="25%" valign="top">Tajuk Kerja</td>
				<td width="5%" valign="top"> : </td>
				<td width="70%" style="text-weight: bold;">{{ $sst->acquisition->title }}</td>
			</tr>
			<tr>
				<td>No. Kontrak</td>
				<td> : </td>
				<td style="text-weight: bold;">
					{{ $sst->acquisition->reference }}
				</td>
			</tr>
			<tr>
				<td valign="top">Nama &amp; Alamat Kontraktor</td>
				<td valign="top"> : </td>
				<td>
					<span class="text-weight: bold;">{{ $sst->company->company_name }}</span><br>
					@if(!empty($sst->company->addresses[0]))
						@if(!empty($sst->company->addresses[0]->primary))
							{{ $sst->company->addresses[0]->primary }}<br>
						@endif
						@if(!empty($sst->company->addresses[0]->secondary))
							{{ $sst->company->addresses[0]->secondary }}<br>
						@endif
						@if(!empty($sst->company->addresses[0]->postcode))
							{{ $sst->company->addresses[0]->postcode }}, 
							{{ $sst->company->addresses[0]->state }}   
						@endif
					@endif
				</td>
			</tr>
			<tr>
				<td>Harga Asal Kontrak</td>
				<td> : </td>
				<td style="text-weight: bold;">
					{!! money()->toHuman($appointed->offered_price) !!}
				</td>
			</tr>
		</table>
	</div>	
</div>
<br/>
<div class="row">
    <table width="100%" style="border-collapse: collapse;padding: 4 4;">
        <tr>
            <td width="16%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Pelarasan Harga Kontrak No</span>
            </td>
            <td width="50%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Butiran Kerja</span>
            </td>
            <td width="17%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Potongan (RM)</span>
            </td>
            <td width="17%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;">Tambahan (RM)</span>
            </td>
        </tr>
        @php
            $total_overal = 0.0;
            $sum_reduced = 0.0;
            $sum_additional = 0.0;            
        @endphp

        @if($ppjhk->count() > 0)
            @foreach($ppjhk as $index => $vod)

                @php
                    $collection = collect(['']);
                    $butiran_kerja = '';

                @endphp

                @php
				    $sum = 0;
                    $sum = $vod->elements->pluck('net_amount')->sum() + $vod->cancels->pluck('amount')->sum();

                    if($sum > 0){
                        $sum_additional = $sum_additional + $sum;
                    }else{
                        $sum_reduced = $sum_reduced + $sum;
                    }

                    $total_overal = $total_overal + $sum;
                    
                    foreach($vod->elements as $element){
                        if(! $collection->contains($element->voElement->vo_type->variation_order_type->name)){
                            
                            $collection = $collection->concat([$element->voElement->vo_type->variation_order_type->name]);

                            if($butiran_kerja == ''){
                                $butiran_kerja = $element->voElement->vo_type->variation_order_type->name;
                            }else{
                                $butiran_kerja = $butiran_kerja . ', ' . $element->voElement->vo_type->variation_order_type->name;
                            }     
                        }                        
                    }
			    @endphp

            <tr>
                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;">
                    <span style="text-transform: uppercase;">
                        ppjhk No. {{ $vod->no }}
                    </span>
                </td>
                <td  style="border-right: 1px solid #959594;">
                    <span style="text-transform: uppercase;">
                        {{ $butiran_kerja }}
                    </span>            
                </td>
                <td  style="border-right: 1px solid #959594; text-align: right;">
                    @if($sum < 0)
                        {{ money()->toCommon($sum ?? "0" , 2) }}
                    @endif
                </td>
                <td  style="border-right: 1px solid #959594; text-align: right;">
                    @if($sum > 0)
                        {{ money()->toCommon($sum ?? "0" , 2) }}
                    @endif
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"><br/></td>
                <td  style="border-right: 1px solid #959594;"></td>
                <td  style="border-right: 1px solid #959594;"></td>
                <td  style="border-right: 1px solid #959594;"></td>            
            </tr>
            @endforeach
        @endif

        <tr>
            <td class="bordered"></td>
            <td class="bordered">
                <span style="text-transform: uppercase;">Jumlah</span>
            </td>
            <td class="bordered" style="text-align: right;">
                {{ money()->toCommon($sum_reduced ?? "0" , 2) }}
            </td>
            <td class="bordered" style="text-align: right;">
                {{ money()->toCommon($sum_additional ?? "0" , 2) }}
            </td>
        </tr>
        <tr>
            <td class="bordered"></td>
            <td class="bordered">
                <span style="text-transform: uppercase;">Jumlah Bersih Tolakan/Tambahan</span>
            </td>
            <td class="bordered" style="text-align: right;">
                @if($total_overal < 0)
                    {{ money()->toCommon($total_overal ?? "0" , 2) }}
                @endif
            </td>
            <td class="bordered" style="text-align: right;">
                @if($total_overal > 0)
                    {{ money()->toCommon($total_overal ?? "0" , 2) }}
                @endif            
            </td>
        </tr>         
    </table>
</div>
<br/><br/>
@role('penyedia')
<div class="btn-group float-right">
    @if(!empty($sst) && $sst->user_id == user()->id)
        @if(empty($review) || empty($review->status))
            <form method="POST" action="{{ route('acquisition.review.store') }}">
                @csrf
                <input id="sentStatus" name="sentStatus" type="hidden" value="1" />
                <input type="text" id="acquisition_id" name="acquisition_id" value="{{ $sst->acquisition_id }}" hidden>

                <input type="text" id="requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                <input type="text" id="approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                <input type="text" id="created_by" name="created_by" value="{{user()->id}}" hidden>

                <input type="text" id="approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                @if(!empty($review_previous))
                    <input type="hidden" id="task_by" name="task_by" value="{{$review_previous->task_by}}">
                    <input type="hidden" id="law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                    <input type="text" id="type" name="type" value="SOFA" hidden>
                @endif
                
                <button type="button" id="send" class="btn btn-default float-middle border-default">
                    @icon('fe fe-send') {{ __('Teratur') }}
                </button>
            </form>
        @endif
    @endif
</div>
@endrole