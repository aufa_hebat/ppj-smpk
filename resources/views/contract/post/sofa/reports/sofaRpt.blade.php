<html>
	<head>
		<style type="text/css">
			@page {
                margin: 40px 30px 40px 70px;
            }
			div.breakNow { page-break-inside:avoid; page-break-after:always; }

			.bordered {
				border-color: #959594;
				border-style: solid;
				border-width: 1px;
			}

			title{
				font-size: 16px;
			}

			body{
				font-family: "Arial, Helvetica, sans-serif";
				font-size: 12px;
				counter-reset:page -1; 
			}

			footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px;
                height: 30px; 
                color: black;
                text-align: center;
				line-height: 35px;	
				font-size:11px;
			}
			
			.content {
                font-size: 12px;
                font-family: "Arial, Helvetica, sans-serif";
			}

			.pagenum:before {
                content: counter(page);                
			}
			
			.title-footer {
                font-size: 10px;
                font-family: "Arial, Helvetica, sans-serif";
            }
		</style>
	</head>
	<body>
		@php
			$letter = collect(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']);
			$roman = collect(['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix', 'x']);
		@endphp
		@include('contract.post.sofa.reports.partials.coverRpt', ['sst' => $sst])		
		<div class="breakNow"><br></div>

		@include('contract.post.sofa.reports.partials.contractRpt', ['sst' => $sst])
		<div class="breakNow"><br></div>

		@include('contract.post.sofa.reports.partials.akuan1Rpt', ['sst' => $sst])
		<div class="breakNow"><br></div>

		@include('contract.post.sofa.reports.partials.akuan2Rpt', ['sst' => $sst])
		<div class="breakNow"><br></div>

		@include('contract.post.sofa.reports.partials.contractorRpt', ['sst' => $sst])
		<div class="breakNow"><br></div>

		@include('contract.post.sofa.reports.partials.statementRpt', ['sst' => $sst])
	</body>
</html>