<div style="text-align: right; font-size: 14px;">
	<span>BPUB/Senarai Semak/IPC Akhir (BIRU)
</div>
<div><br/></div>
<div style="text-align: left; text-transform: uppercase;">
	<u>Senarai Semakan Penyediaan Perakuan Muktamad / Ipc Akhir</u>
</div>
<br>
<div class="row">
	<div class="col-12">
		<table width="100%" style="text-align: left; text-transform: uppercase;">
			<tr>
				<td width="25%">Nama Projek</td>
				<td width="5%"> : </td>
				<td width="70%" style="text-decoration: underline;">{{ $sst->acquisition->title }}</td>
			</tr>
			<tr>
				<td>No. Kontrak</td>
				<td> : </td>
				<td>{{ $sst->acquisition->reference }}</td>
			</tr>
			<tr>
				<td>Nama Kontraktor</td>
				<td> : </td>
				<td>{{ $sst->company->company_name }}</td>
			</tr>
			<tr>
				<td>Jabatan/Bahagian</td>
				<td> : </td>
				<td></td>
			</tr>
		</table>
	</div>	
</div>
<br>
<div class="row">
	<div class="col-12">
		<table style="width: 100%;border-collapse: collapse;padding: 4 4;">
			<thead>
				<tr>	
					<td class="bordered" width="10%" style="text-align: center; padding: 7 7;">No.</td>
					<td class="bordered" width="70%" style="text-align: center;">Perkara</td>
					<td class="bordered" width="10%" style="text-align: center;">Penyedia</td>
					<td class="bordered" width="10%" style="text-align: center;">BPUB</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="bordered" style="text-align: center;  padding: 7 7;">1</td>
					<td class="bordered" style="text-align: left;">Butiran Maklumat Kontrak**</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">2</td>
					<td class="bordered" style="text-align: left;">Perakuan Akaun Dan Bayaran Muktamad** Berwarna Biru</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">3</td>
					<td class="bordered" style="text-align: left;">
						Sijil Bayaran Interim (IPC Akhir)**<br>
						Semakan : WJP telah dibayar.... &nbsp;&nbsp; belum dibayar....&nbsp;&nbsp; Bon Pelaksanaan
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">4</td>
					<td class="bordered" style="text-align: left;">Ringkasan Bayaran, Status Bayaran Interim, Butiran Bayaran**</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">5</td>
					<td class="bordered" style="text-align: left;">Surat Tuntutan/Inbois/Inbois Cukai (perlu ada bukti Cop Penerimaan oleh Jabatan)**</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">6</td>
					<td class="bordered" style="text-align: left;">
						Perakuan Pelarasan Jumlah Harga Kontrak (PPJHK) Berwarna Hijau<br>
						**PPJHK No........... <br>
						**PPJHK No...........<br>
						<span style="font-size: 14px; font-style: italic;">* Jika Berkaitan</span>
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">7</td>
					<td class="bordered" style="text-align: left;">
						Salinan Sijil Siap Kerja (CPC)<br>
						Salinan Perakuan Siap Kerja-Kerja Penyelenggaraan (CCMW)
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">8</td>
					<td class="bordered" style="text-align: left;">
						Salinan Lanjutan Masa (EOT) No. .............Hingga.........<br>
						<span style="font-size: 14px; font-style: italic;">* Jika Berkaitan</span>
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">9</td>
					<td class="bordered" style="text-align: left;">
						Salinan Sijil Tidak Siap Kerja / CNC / LAD<br>
						<span style="font-size: 14px; font-style: italic;">* Jika Berkaitan</span>
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">10</td>
					<td class="bordered" style="text-align: left;">
						Salinan Sijil Siap Membaiki Kecacatan (CMGD)<br>
						<span style="font-size: 14px; font-style: italic;">* Jika Berkaitan</span>
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">11</td>
					<td class="bordered" style="text-align: left;">
						Surat Pengecualian LAD<br>
						<span style="font-size: 14px; font-style: italic;">* Jika Berkaitan</span>
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">12</td>
					<td class="bordered" style="text-align: left;">
						Salinan Sijil IPC terdahulu yang telah dibayar**<br>
						Jumlah kesemua IPC Bil .....hingga.....
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>
				<tr>
					<td class="bordered" style="text-align: center; padding: 7 7;">13</td>
					<td class="bordered" style="text-align: left;">
						Akuan Statutori (Akuan Bersumpah)**
					</td>
					<td class="bordered"></td>
					<td class="bordered"></td>
				</tr>																																																
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<table width="80%">
			<tr>
				<td width="20%"><span style="font-size:14px; font-style: italic; text-decoration: underline;">Nota<span></td>
				<td width="80%">
					<span style="font-size:14px; font-style: italic;">
					1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Setiap flysheet menggunakan kertas berwarna dan ditagging<br>
					2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Sekiranya berkaitan<br>
					3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Perlu dipatuhi dan maklumat yang diperlukan
					</span>
				</td>
			</tr>
		</table>
	</div>
</div>
<br>
<div class="row">
	<div class="col-12">
		<table width="100%" align="center">
			<tr>
				<td>Disediakan Oleh </td>
				<td>Disemak Oleh (BPUB) </td>
			</tr>
			<tr>
				<td>Tandatangan dan Cop : </td>
				<td>Tandatangan dan Cop : </td>
			</tr>
			<tr>
				<td>Jawatan : </td>
				<td>Jawatan : </td>
			</tr>
			<tr>
				<td>Tarikh : </td>
				<td>Tarikh : </td>
			</tr>						
		</table>
	</div>	
</div>
