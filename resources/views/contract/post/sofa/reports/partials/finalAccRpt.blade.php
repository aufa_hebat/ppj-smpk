<style type="text/css">
	body{
		font-size: 12px;
	}
</style>
<body>    
    <div style="text-align: right">
        <i>BPUB/IPC/Bayaran Akhir</i>
    </div>
    <div>
        <center>
            <b>PERBADANAN PUTRAJAYA <br> SIJIL BAYARAN INTERIM</b>
        </center>
    </div><br>
    <div>
        <table border="1" style="border-collapse: collapse;width: 100%;">
            <tr>
                <td style="width: 50%;">Rujukan Fail / No. Kontrak  : {{$sst->contract_no}}</td>
                <td style="width: 50%;">Kod Projek : </td>
            </tr>
            <tr>
                <td>Tarikh Surat Setuju Terima : {{(!empty($sst->letter_date))? \Carbon::intlFormat($sst->letter_date):''}}</td>
                <td>No Sijil Interim : FINAL ACCOUNT</td>
            </tr>
            <tr>
                <td>Tarikh Kontrak Ditandatangani : {{(!empty($sst->document->document_revenue_stamp_date))? \Carbon::intlFormat($sst->document->document_revenue_stamp_date):''}}</td>
                <td>No. Invois   : </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Tajuk Kontrak  : {{$sst->acquisition->title}}</td>
                <td>Tarikh Invois  : </td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold;">Nama Kontraktor : {{ $sst->company->company_name}}</td>
            </tr>
            <tr>
                <td>No. Pendaftaran GST : 
                    {{(!empty($sst->company->gst_registered_no)) ? $sst->company->gst_registered_no : '-'}}
                </td>
                <td>Tarikh Penilaian : </td>
            </tr>
        </table>
    </div><br>

    @php
        $total_sum_reduced = 0.0;
        $total_sum_additional = 0.0;
        $total_overal = 0.0;

		$total_last_payment = $appointed->offered_price;
		$total_last_payment = $total_last_payment + $total_overal;

        //$total_reduced = ($prev_total_invoice + ($total_adv_amt - $prev_gv_adv_amt) + $total_lad);
        $total_reduced = ($prev_total_invoice + $prev_gv_adv_amt + $total_lad);
		$total_payment = ($total_last_payment - $total_reduced);
		$total_wjp = 0;
		$total_other_amount = 0;
		$total_contractor = 0;

		if($sst->bon->bon_type == 4){
			$total_wjp = $wjp_amt - $wjp_lepas;	
		}

		$total_other_amount = $total_payment - $total_wjp - $total_balanced;
		$total_contractor = $total_other_amount + $total_wjp;
				
	@endphp

    <div>
        <table border="1" style="border-collapse: collapse;width: 100%;">
            <tr>
                <th style="background-color: #d8d6d6">BUTIRAN</th>
                <th style="background-color: #d8d6d6">Jumlah Kontrak (RM)</th>
                <th style="background-color: #d8d6d6">Pengesahan Terkini (RM)</th>
                <th style="background-color: #d8d6d6">Pengesahan Terdahulu (RM)</th>
                <th style="background-color: #d8d6d6">Pengesahan Semasa (RM)</th>
            </tr>
            <tr>
                <td>Harga Tawaran/Nilai Kontrak</td>
                <td style="text-align: right;">
                    {{money()->toCommon($appointed->offered_price ?? 0, 2)}}
                </td>
                <td style="text-align: right;">
                    {{money()->toCommon($prev_total_invoice + $total_other_amount ?? 0, 2)}}
                </td>
                <td style="text-align: right;">
                    {{money()->toCommon($prev_total_invoice ?? 0, 2) }}
                </td>                
                <td style="text-align: right;">
                    {{ money()->toCommon($total_other_amount ?? 0, 2) }}
                </td>
            </tr>
            <tr>
                <td>JUMLAH HARGA KONTRAK</td>
                <td></td>
                <td style="text-align: right;">
                    {{money()->toCommon($prev_total_invoice + $total_other_amount ?? 0, 2)}}
                </td>
                <td style="text-align: right;">
                    {{money()->toCommon($prev_total_invoice ?? 0, 2) }}
                </td>
                <td style="text-align: right;">
                    {{ money()->toCommon($total_other_amount ?? 0, 2) }}
                </td>
            </tr>
            <tr>
                <td>Perubahan Kerja</td>
                <td></td>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>JUMLAH PERUBAHAN KERJA (+/-)</td>
                <td></td>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td >
                    
                </td>
            </tr>
            <tr>
                <td><b>JUMLAH NILAI KONTRAK BARU</b></td>
                <td><b></b></td>
                <td><b>
                    
                </b></td>
                <td><b>
                    
                </b></td>
                <td><b>
                    
                </b></td>
            </tr>
            <tr>
                <td>Cukai Barang dan Perkhidmatan (0% GST)</td>
                <td></td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
            </tr>
            <tr>
                <td><b>JUMLAH KASAR SELEPAS GST</b></td>
                <td><b></b></td>
                <td><b>
                    
                </b></td>
                <td><b>
                    
                </b></td>
                <td><b>
                    
                </b></td>
            </tr>
            <tr>
                <td>(+) Bayaran Pendahuluan</td>
                <td style="text-align: right;">
                    
                </td>
                <td style="text-align: right;">
                    {{ money()->toCommon($sst->deposit->qualified_amount ?? 0, 2) }}
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="text-align: right;">
                    
                </td>
            </tr>
            <tr>
                <td>(+) Wang Tahanan Dilepaskan</td>
                <td></td>
                <td style="text-align: right;">
                    {{ money()->toCommon($wjp_amt ?? 0, 2) }}
                </td>
                <td style="text-align: right;">
                    {{ money()->toCommon($wjp_lepas ?? 0, 2) }}
                </td>
                <td style="text-align: right;">
                    {{ money()->toCommon($total_wjp ?? 0, 2) }}
                </td>
            </tr>
            <tr>
                <td>( -)  Bayaran Balik Pendahuluan</td>
                <td></td>
                <td style="text-align: right;">
                    <!-- {{ money()->toCommon($sst->deposit->qualified_amount ?? 0, 2) }} -->
                </td>
                <td style="text-align: right;">
                    {{ money()->toCommon($prev_gv_adv_amt ?? 0, 2) }}
                    {{-- {{ money()->toCommon($total_adv_amt - $prev_gv_adv_amt ?? 0, 2) }} --}}
                </td>
                <td style="text-align: right;">
                    <!-- {{ money()->toCommon($total_adv_amt - $prev_gv_adv_amt ?? 0, 2) }} -->
                </td>
            </tr>
            <tr>
                <td>( -)  Jumlah Wang Tahanan :</td>
                <td style="text-align: right;">
                    <!-- {{ ($sst->bon->bon_type == 4)? money()->toCommon($sst->bon->money_amount ?? 0, 2):'0.00' }} -->
                </td>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td>
                    
                </td>

            </tr>
            <tr>
                <td>Jumlah & No. Bon Pelaksanaan* : </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>( -) Ganti Rugi tertentu dan ditetapkan (LAD)</td>
                <td></td>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td><b>JUMLAH BERSIH KENA BAYAR</b></td>
                <td><b></b></td>
                <td style="text-align: right;">
                <b>
                    {{ money()->toCommon($total_payment + $prev_total_invoice + $wjp_lepas + ($total_adv_amt - $prev_gv_adv_amt) ?? 0, 2)}}
                </b></td>
                <td style="text-align: right;"><b>
                    {{ money()->toCommon($prev_total_invoice + $wjp_lepas + ($total_adv_amt - $prev_gv_adv_amt) ?? 0, 2) }}
                </b></td>
                <td style="text-align: right;"><b>
                    {{ money()->toCommon($total_payment ?? 0, 2) }}
                </b></td>
            </tr>
            <tr>
                <td>(Ringgit Malaysia : Dalam Perkataan)</td>
                <td colspan="4" style="text-transform: uppercase"><b>
                    
                </b></td>
            </tr>
            <tr>
                <th colspan="5" style="background-color: #d8d6d6">Pengesahan oleh Pengurusan Projek (JIKA PERLU)</th>
            </tr>
            <tr>
                <td>
                    Disediakan <br><br><br><br>
                    Tarikh : <br>
                </td>
                <td colspan="2">
                    Disemak <br><br><br><br>
                    Tarikh : <br>
                </td>
                <td colspan="2">
                    Diakui <br><br><br><br>
                    Tarikh : <br>
                </td>
            </tr>
            <tr>
                <th colspan="5" style="background-color: #d8d6d6">Pengesahan oleh Pengurusan Projek (JIKA PERLU)</th>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Disedia, </b><br>
                    Nama : <br>
                    Jawatan : <b>JABATAN PELAKSANA</b><br>
                    Tarikh : <br>
                </td>
                <td colspan="3">
                    <b>Diakui dan disahkan, </b><br>
                    Nama : <br>
                    Jawatan : <b>PENGARAH BAHAGIAN, JABATAN PELAKSANA</b><br>
                    Tarikh : <br>
                </td>
            </tr>
            <tr>
                <th colspan="5"><center>Diperaku dan disahkan kerja/perkhidmatan/bekalan telah sempurna dilaksanakan seperti tuntutan yang dikemukakan untuk tujuan pembayaran</center></th>
            </tr>
            <tr>
                <td colspan="5">
                    Nama : <br>
                    Jawatan : <b>NAIB PRESIDEN, JABATAN PELAKSANA</b><br>
                    Tarikh :
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <center><b>Diakui tuntutan ini telah disemak dan diperakui untuk tujuan pembayaran</b></center>
                    Nama : <br>
                    Jawatan : <br>
                    Tarikh :
                </td>
            </tr>
        </table>
    </div>
</body>