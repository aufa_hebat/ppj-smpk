<div style="text-align: right; font-size: 11px;">
<span>BPUB/SOFA/Maklumat Kontrak</span>
</div>
<div><br/></div>
<div style="text-align: center; font-weight: bold; text-transform: uppercase;font-size:14px;">
<span>Butiran Maklumat Kontrak</span>
</div>
<div><br/></div>
<div style="text-align: justify; font-weight: bold; text-transform: uppercase; font-size:14px;">
<span>{{ $sst->acquisition->title }}</span>
</div>
<div><br/></div>
<div style="font-size:14px">
<table width="100%">
	<tr>
		<td width="46%" style="padding: 5px 5px">No. Kontrak</td>
		<td width="8%"> : </td>
		<td width="46%">{{ $sst->acquisition->reference }}</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Kaedah Perolehan / Jenis Tawaran</td>
		<td> : </td>
		<td><span style="text-transform: uppercase">{{ $sst->acquisition->category->name }} </span></td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Klient</td>
		<td> : </td>
		<td>
			<span style="text-transform: uppercase">
				{!! optional($sst->acquisition->approval->allocation_resource)->name !!}@if(!empty($sst->acquisition->approval->other)) : {!! $sst->acquisition->approval->other !!}@endif
			</span>
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Kontraktor Utama</td>
		<td> : </td>
		<td>
			<span style="text-transform: uppercase">
				{{ $sst->company->company_name}}
			</span>
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Kelas Pendaftaran</td>
		<td> : </td>
		<td>                
			@foreach($sst->acquisition->approval->cidbQualifications as $cetak)
				@if($cetak->code->type == 'khusus')
					{{$cetak->code->code}}
					@if($cetak->status == 1)
						dan
					@elseif($cetak->status == 0)
						atau
					@else
						,
					@endif                        
				@endif
			@endforeach  
		
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Taraf</td>
		<td> : </td>
		<td>
			<span style="text-transform: uppercase">
				@if($sst->acquisition->approval->bpku_bumiputera == 1) Bumiputera @else Bukan Bumiputera @endif
			</span>
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Pembiayaan Projek</td>
		<td> : </td>
		<td>
			{{ $sst->acquisition->approval->loyalty_code }}
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Nilai Kontrak Asal</td>
		<td> : </td>
		<td>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Tarikh Mula</td>
		<td> : </td>
		<td>
			 {{ \Carbon::intlFormat($sst->start_working_date) }}
		</td>
	</tr>				
	<tr>
		<td style="padding: 5px 5px">Tarikh Siap</td>
		<td> : </td>
		<td>
			 {{ \Carbon::intlFormat($sst->end_working_date) }}
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Tarikh Sebenar Siap Kerja</td>
		<td> : </td>
		<td>
			 {{ (!empty($cpc) && null != $cpc) ? \Carbon::intlFormat($cpc->signature_date) : '-' }}
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Lanjutan Masa Yang Diperakukan</td>
		<td> : </td>
		<td>
			@if($totalYear == 0 && $totalMonth == 0 && $totalWeek == 0 && $totalDay ==0)
				TIADA 
			@endif
			@if($totalYear > 0)
				{{ $totalYear }} Tahun &nbsp;&nbsp;
			@endif
			@if($totalMonth > 0)
				{{ $totalMonth }} Bulan &nbsp;&nbsp;
			@endif
			@if($totalWeek > 0)
				{{ $totalWeek }} Minggu &nbsp;&nbsp;
			@endif
			@if($totalDay > 0)
				{{ $totalDay }} Hari &nbsp;&nbsp;
			@endif												
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Tarikh Perakuan Siap Kerja</td>
		<td> : </td>
		<td>
			 {{ (!empty($cpc) && null != $cpc) ? \Carbon::intlFormat($cpc->signature_date) : '-' }}
		</td>
	</tr>	
	<tr>
		<td style="padding: 5px 5px">Tarikh Perakuan Siap Membaiki Kecacatan</td>
		<td> : </td>
		<td>
			 {{ (!empty($cmgd) && null != $cmgd) ? \Carbon::intlFormat($cmgd->signature_date) : '-' }}
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Jumlah Hari Yang Dikenakan Gantirugi Tertentu Dan Ditetapkan (LAD)</td>
		<td valign="top"> : </td>
		<td valign="top">
			{{ isset($cnc->days_passed) ? $cnc->days_passed : "0"}}
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Tempoh Liabiliti Kecacatan</td>
		<td> : </td>
		<td>
			{{ isset($sst->defects_liability_period) ? $sst->defects_liability_period : "0"}} bulan
		</td>
	</tr>		
	<tr>
		<td style="padding: 5px 5px">Harga Asal Kontrak</td>
		<td> : </td>
		<td>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}</td>
	</tr>
	<tr>
		<td style="padding: 5px 5px">Pelarasan Jumlah Harga Kontrak</td>
		<td> : </td>
		<td></td>
	</tr>


	@php
		$total_sum_reduced = 0.0;
		$total_sum_additional = 0.0;
		$total_overal = 0.0;
		$total_last_payment = 0.0;
	@endphp

	@if($ppjhk->count() > 0)
		@foreach($ppjhk as $index => $vod)
			@php
				$sum = 0;
				$sum = $vod->elements->pluck('net_amount')->sum() + $vod->cancels->pluck('amount')->sum();
				$total_overal = $total_overal + $sum;
			@endphp		

			{{--  @php

				$sum_reduced = 0.0;
				$sum_additional = 0.0;

				foreach($vo_element as $index => $voe) {
					if($voe->variationOrder->id == $vod->id){
						$sum_reduced = $sum_reduced + $voe->estimated_reduced_amount; 
						$sum_additional = $sum_additional + $voe->estimated_additional_amount; 

						$sum_add_red    =   $sum_additional - $sum_reduced;					
					}
				}

				if($sum_additional > $sum_reduced){
					$total_sum_additional = $total_sum_additional + $sum_add_red;
				}else{
					$total_sum_reduced = $total_sum_reduced + $sum_add_red;
				}
				
				
				$total_overal = $total_sum_additional + $total_sum_reduced;				
			@endphp  --}}

			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span style="padding: 10px 10px; font-size:14px; text-transform: uppercase;">
						ppjhk No. {{ $vod->no }}
					</span>
				</td>
				<td> : </td>
				<td>
					@if($sum > 0)
						{{ money()->toHuman($sum ?? "0" , 2) }}
					@else
						({{ str_replace("-","",money()->toHuman($sum ?? "0" , 2)) }})
					@endif
				</td>
			</tr>
		@endforeach
	@endif

	@php	
		/* $lad_rate = $total_lad ? $total_lad->rate : 0;*/
		$total_last_payment = $appointed->offered_price;
		$total_last_payment = $total_last_payment + $total_overal;
		/*$total_reduced = ($prev_total_invoice + ($deposit->total_amount ?? 0) + $lad_rate);*/
		/*$total_payment = ($total_last_payment - $total_reduced);*/
	@endphp


	<tr>
		<td style="padding: 10px 10px">Harga Muktamad Kontrak</td>
		<td> : </td>
		<td> 
			{{--  {{ money()->toHuman($total_payment ?? "0" , 2) }}  --}}
			{{ money()->toHuman($total_last_payment ?? "0" , 2) }}
		</td>
	</tr>																	
</table>

</div>
