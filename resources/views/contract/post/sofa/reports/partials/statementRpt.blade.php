<div style="text-align: center; text-transform: uppercase; text-decoration: underline;font-size:14px;">
    <span>Penyata Pelaksanaan Harga Kontrak</span>
</div>
<br>
<div class="row" style="font-size:14px;">
	<div class="col-12">
		<table width="100%" style="font-size:14px;">
			<tr>
				<td width="25%" valign="top">Tajuk Kerja</td>
				<td width="5%" valign="top"> : </td>
				<td width="70%" style="text-weight: bold;">{{ $sst->acquisition->title }}</td>
			</tr>
			<tr>
				<td>No. Kontrak</td>
				<td> : </td>
				<td style="text-weight: bold;">
					{{ $sst->acquisition->reference }}
				</td>
			</tr>
			<tr>
				<td valign="top">Nama &amp; Alamat Kontraktor</td>
				<td valign="top"> : </td>
				<td>
					<span class="text-weight: bold;">{{ $sst->company->company_name }}</span><br>
					@if(!empty($sst->company->addresses[0]))
						@if(!empty($sst->company->addresses[0]->primary))
							{{ $sst->company->addresses[0]->primary }}<br>
						@endif
						@if(!empty($sst->company->addresses[0]->secondary))
							{{ $sst->company->addresses[0]->secondary }}<br>
						@endif
						@if(!empty($sst->company->addresses[0]->postcode))
							{{ $sst->company->addresses[0]->postcode }}, 
							{{ $sst->company->addresses[0]->state }}   
						@endif
					@endif
				</td>
			</tr>
			<tr>
				<td>Harga Asal Kontrak</td>
				<td> : </td>
				<td style="text-weight: bold;">
					{!! money()->toHuman($appointed->offered_price) !!}
				</td>
			</tr>
		</table>
	</div>	
</div>
<br/>
<div class="row">
    <table width="100%" style="border-collapse: collapse;padding: 4 4; font-size:14px;">
        <tr>
            <td width="16%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;font-weight:bold;">Pelarasan Harga Kontrak No</span>
            </td>
            <td width="50%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;font-weight:bold;">Butiran Kerja</span>
            </td>
            <td width="17%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;font-weight:bold;">Potongan (RM)</span>
            </td>
            <td width="17%" class="bordered" style="text-align: center;">
                <span style="text-transform: uppercase;font-weight:bold;">Tambahan (RM)</span>
            </td>
        </tr>
        @php
            $total_overal = 0.0;
            $sum_reduced = 0.0;
            $sum_additional = 0.0;            
        @endphp

        @if($ppjhk->count() > 0)
        @foreach($ppjhk as $index => $vod)

            @php
                $collection = collect(['']);
                $butiran_kerja = '';

            @endphp

            @php
                $sum = 0;
                $sum = $vod->elements->pluck('net_amount')->sum() + $vod->cancels->pluck('amount')->sum();

                if($sum > 0){
                    $sum_additional = $sum_additional + $sum;
                }else{
                    $sum_reduced = $sum_reduced + $sum;
                }
                $total_overal = $total_overal + $sum;
                
                foreach($vod->elements as $element){
                    if(! $collection->contains($element->voElement->vo_type->variation_order_type->name)){
                        
                        $collection = $collection->concat([$element->voElement->vo_type->variation_order_type->name]);

                        if($butiran_kerja == ''){
                            $butiran_kerja = $element->voElement->vo_type->variation_order_type->name;
                        }else{
                            $butiran_kerja = $butiran_kerja . ', ' . $element->voElement->vo_type->variation_order_type->name;
                        }     
                    }                        
                }

            @endphp

        <tr>
            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;">
                <span style="text-transform: uppercase;">
                    ppjhk No. {{ $vod->no }}
                </span>
            </td>
            <td  style="border-right: 1px solid #959594;">
                <span style="text-transform: uppercase;">
                    {{ $butiran_kerja }}
                </span>            
            </td>
            <td  style="border-right: 1px solid #959594; text-align: right;">
                @if($sum < 0)
                    {{ money()->toCommon($sum ?? "0" , 2) }}
                @endif
            </td>
            <td  style="border-right: 1px solid #959594; text-align: right;">
                @if($sum > 0)
                    {{ money()->toCommon($sum ?? "0" , 2) }}
                @endif
            </td>
        </tr>
        <tr>
            <td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"><br/></td>
            <td  style="border-right: 1px solid #959594;"></td>
            <td  style="border-right: 1px solid #959594;"></td>
            <td  style="border-right: 1px solid #959594;"></td>            
        </tr>
        @endforeach
        @endif

        <tr>
            <td class="bordered" style="padding: 20px 10px;"></td>
            <td class="bordered">
                <span style="text-transform: uppercase;font-weight:bold;">Jumlah</span>
            </td>
            <td class="bordered" style="text-align: right;">
                {{ money()->toCommon($sum_reduced ?? "0" , 2) }}
            </td>
            <td class="bordered" style="text-align: right;">
                {{ money()->toCommon($sum_additional ?? "0" , 2) }}
            </td>
        </tr>
        <tr>
            <td class="bordered" style="padding: 20px 10px;"></td>
            <td class="bordered">
                <span style="text-transform: uppercase;font-weight:bold;">Jumlah Bersih Tolakan/Tambahan</span>
            </td>
            <td class="bordered" style="text-align: right;">
                @if($total_overal < 0)
                    {{ money()->toCommon($total_overal ?? "0" , 2) }}
                @endif
            </td>
            <td class="bordered" style="text-align: right;">
                @if($total_overal > 0)
                    {{ money()->toCommon($total_overal ?? "0" , 2) }}
                @endif            
            </td>
        </tr> 
    </table>
    <br/><br/>
    <table width="100%" style="border-collapse: collapse;padding: 4 4;">
        <tr>
            <td width="30%">Tarikh : .........................</td>
            <td width="70%" style="text-align:right">
                ......................................................<br>
                <span style="text-align:center;">(Wakil Perbadanan)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </td>
        </tr>
    </table>
</div>