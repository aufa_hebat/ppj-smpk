<div style="text-align: right; font-size: 11px;">
<span>BPUB/SOFA/AKUAN MUKTAMAD</span>
</div>
<div><br/></div>
<div style="text-align: center; font-weight: bold; text-transform: uppercase; padding:3px 3px;font-size:14px;">
<span>Perbadanan Putrajaya</span>
</div>
<div style="text-align: center; font-weight: bold; text-transform: uppercase; padding:3px 3px;font-size:14px;">
<span>Perakuan akaun dan bayaran muktamad</span>
</div>
<div class="row" style="text-align: center; font-weight: bold; text-transform: uppercase; padding:3px 3px;font-size:14px;">
<table width="100%">
	<tr>
		<td width="50%">Peruntukan Pembangunan : </td>
		<td width="25%">Maksud : </td>
		<td width="25%">Butiran : </td>
	</tr>
</table>
</div>
<div style="text-align: left; text-transform: uppercase; padding:4px 4px; font-size:14px;">
<table width="100%">
	<tr>
		<td width="25%" valign="top">Tajuk Kerja</td>
		<td width="5%" valign="top"> : </td>
		<td width="70%" style="text-weight: bold;">{{ $sst->acquisition->title }}</td>
	</tr>
	<tr>
		<td valign="top">Nama &amp; Alamat Kontraktor</td>
		<td valign="top"> : </td>
		<td>
			<span class="text-weight: bold;">{{ $sst->company->company_name }}</span><br>
			@if(!empty($sst->company->addresses[0]))
				@if(!empty($sst->company->addresses[0]->primary))
					{{ $sst->company->addresses[0]->primary }}<br>
				@endif
				@if(!empty($sst->company->addresses[0]->secondary))
					{{ $sst->company->addresses[0]->secondary }}<br>
				@endif
				@if(!empty($sst->company->addresses[0]->postcode))
					{{ $sst->company->addresses[0]->postcode }}, 
					{{ $sst->company->addresses[0]->state }}   
				@endif
			@endif
		</td>
	</tr>
	<tr>
		<td>No. Kontrak</td>
		<td> : </td>
		<td style="text-weight: bold;">
			{{ $sst->acquisition->reference }}
		</td>
	</tr>
</table>
</div>
<div class="row" style="text-align: left; text-transform: uppercase; padding:4px 4px; font-size:14px;">
<table width="100%">
	@php
		$total_sum_reduced = 0.0;
		$total_sum_additional = 0.0;
		$total_overal = 0.0;
	
		if($ppjhk->count() > 0){
			foreach($ppjhk as $index => $vod){
				$sum = 0;
				$sum = $vod->elements->pluck('net_amount')->sum() + $vod->cancels->pluck('amount')->sum();
				$total_overal = $total_overal + $sum;
			}
		}

		$total_last_payment = $appointed->offered_price;
		$total_last_payment = $total_last_payment + $total_overal;

		//$total_reduced = ($prev_total_invoice + ($total_adv_amt - $prev_gv_adv_amt) + $total_lad);
		$total_reduced = ($prev_total_invoice + $prev_gv_adv_amt + $total_lad);
		$total_payment = ($total_last_payment - $total_reduced);

		$total_wjp = 0;
		$total_other_amount = 0;
		$total_contractor = 0;

		if($sst->bon->bon_type == 4){
			$total_wjp = $wjp_amt - $wjp_lepas;						
		}

		$total_other_amount = $total_payment - $total_wjp - $total_balanced;
		$total_contractor = $total_other_amount + $total_wjp;

	@endphp
	<tr>
		<td width="50%">
			<table width="100%" >
				<tr><td>Jumlah Harga Asal Kontrak</td></tr>
				<tr><td>Jumlah Bersih @if($total_overal > 0) Tambahan @elseif($total_overal < 0) Potongan @endif </td></tr>
				<tr><td>Harga Muktamad Kontrak</td></tr>
			</table>
		</td>
		<td width="20%"></td>
		<td width="30%">
			<table width="100%" >
				<tr>
					<td width="10%">RM</td>
					<td width="90%" style="text-align: right;">{{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}</td>
				</tr>						
				<tr>
					<td>RM</td>
					<td style="text-align: right;">
						@if($total_overal > 0)
							{{ money()->toCommon($total_overal ?? "0" , 2) }}
						@else
							({{ money()->toCommon($total_overal ?? "0" , 2) }})
						@endif
					</td>
				</tr>					
				<tr>
					<td style="border-top: 2px solid #0; border-bottom: 2px double #000;">RM</td>
					<td style="border-top: 2px solid #0; border-bottom: 2px double #000; text-align: right;font-weight: bold;">{{ money()->toCommon($total_last_payment ?? "0" , 2) }}	</td></tr>
			</table>

		</td>
	</tr>
</table>
{{--  <table width="100%" class="bordered">
	<tr>
		<td width="50%">Jumlah Harga Asal Kontrak</td>
		<td width="25%"></td>
		<td width="5%"> RM </td>
		<td width="20%" style="text-align: right;">{{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}</td>
	</tr>

	@php
		$total_sum_reduced = 0.0;
		$total_sum_additional = 0.0;
		$total_overal = 0.0;
	@endphp

	@if($ppjhk->count() > 0)
		@foreach($ppjhk as $index => $vod)
			@php
				$sum = 0;
				$sum = $vod->elements->pluck('net_amount')->sum() + $vod->cancels->pluck('amount')->sum();
				$total_overal = $total_overal + $sum;
			@endphp

			<tr>
				<td>
					<span style="text-transform: uppercase;">
						ppjhk No. {{ $vod->no }}
					</span>
				</td>
				<td>
					@if($sum > 0)
						{{ money()->toHuman($sum ?? "0" , 2) }}
					@else
						({{ str_replace("-","",money()->toHuman($sum ?? "0" , 2)) }})
					@endif
				</td>
				<td colspan="2"></td>
			</tr>
		@endforeach
	@endif
	<tr>
		<td>Jumlah Bersih @if($total_overal > 0) Tambahan @elseif($total_overal < 0) Potongan @endif</td>
		<td></td>
		<td>RM</td>
		<td style="text-align: right;">
			@if($total_overal > 0)
				{{ money()->toCommon($total_overal ?? "0" , 2) }}
			@else
				({{ money()->toCommon($total_overal ?? "0" , 2) }})
			@endif
		</td>
	</tr>
	<tr>
		<td>Harga Muktamad Kontrak</td>
		<td></td>
		<td> RM </td>
		<td style="border-top: 1px solid #959594; text-align: right;">
			@php
				/*$lad_rate = $total_lad ? $total_lad->rate : 0;*/
				
				$total_last_payment = $appointed->offered_price;
				$total_last_payment = $total_last_payment + $total_overal;

				$total_reduced = ($prev_total_invoice + ($total_adv_amt - $prev_gv_adv_amt) + $total_lad);
				$total_payment = ($total_last_payment - $total_reduced);
				$total_wjp = 0;
				$total_other_amount = 0;
				$total_contractor = 0;

				if($sst->bon->bon_type == 4){
					$total_wjp = $wjp_amt - $wjp_lepas;						
				}

				$total_other_amount = $total_payment - $total_wjp - $total_balanced;
				$total_contractor = $total_other_amount + $total_wjp;
			
			@endphp
			{{ money()->toCommon($total_last_payment ?? "0" , 2) }}
		</td>
	</tr>						
</table>  --}}
</div>
<div><hr/></div>
<div class="row" style="text-align: left; padding:3px 3px; font-size:14px;">
<table width="100%">
	<tr style="text-align: left; font-weight: bold; text-transform: uppercase; padding:3px 3px;">
		<td width="5%">A.</td>
		<td width="65%">Butir-butir Jumlah Potongan / Kurangan</td>
		<td width="5%"></td>
		<td width="5%"></td>
		<td width="20%"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">
			@if(!empty($last_ipc))
				1. Bayaran Interim Terdahulu No. 1 @if($last_ipc->ipc_no > 1) hingga No. {{ $last_ipc->ipc_no }} @endif
			@else
				1. Bayaran Interim Terdahulu
			@endif
		</td>
		<td style="text-align: center;"> RM </td>
		<td style="text-align: right; ">{{ money()->toCommon($prev_total_invoice ?? "0" , 2) }}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">2. Bayaran Wang Pendahuluan </td>
		<td style="text-align: center;"> RM </td>
		<td style="text-align: right;">
			{{-- {{ money()->toCommon($total_adv_amt-$prev_gv_adv_amt ?? "0" , 2) }} --}}
			{{ money()->toCommon($prev_gv_adv_amt ?? "0" , 2) }}
		</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">3. Potongan lain jika ada</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">&nbsp;&nbsp;&nbsp;a) Gantirugi Tertentu Dan Ditetapkan @ RM {{money()->toCommon($sst->document_LAD_amount) ?? "0" , 2  }} / hari </td>
		<td style="text-align: center;"> RM </td>
		<td style="text-align: right;">
			{{-- {{ money()->toCommon($total_lad->lad_rate ?? "0" , 2) }}			 --}}
			{{ money()->toCommon($total_lad ?? "0" , 2) }}			
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">&nbsp;&nbsp;&nbsp;b) Kerja Membaiki Kecacatan</td>
		<td style="text-align: center; "> RM </td>
		<td style="text-align: right;">0.00</td>
	</tr>
	<!-- <tr>
		<td>&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;c) Surcharge 10% untuk kerja sebutharga</td>
		<td style="text-align: center; font-weight: bold; "> RM </td>
		<td style="text-align: right; font-weight: bold; "></td>
	</tr> -->
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center; font-weight: bold; text-transform: uppercase;">Bayaran Muktamad Di Bawah Kontrak</td>
		<td style="text-align: center; font-weight: bold; border-top: 1px double #000; border-bottom: 1px double #000;"> RM </td>
		<td style="text-align: right; font-weight: bold; border-top: 1px double #000; border-bottom: 1px double #000;">
			{{ money()->toCommon($total_payment ?? "0" , 2) }}
		</td>
	</tr>
	<tr><td colspan="5">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tarikh : .................</td>
		<td colspan="3">
		..............................................<br>
		<span style="text-align: center; font-size: 13px;">(Pegawai Pengesyor / wakil P.P)</span>
		</td>
	</tr>
</table>
</div>
<div><hr/></div>
<div class="row" style="text-align: left; padding:3px 3px; font-size:14px;">
<table width="100%">
	<tr>
		{{--  <td width="5%" style="text-align: left; font-weight: bold;" valign="top">B. </td>  --}}
		{{--  <td width="95%" colspan="3">  --}}
		<td colspan="4">
			<span>
				Saya dengan ini memperakukan bahawa bayaran muktamad di bawah ini berjumlah 
				<span style="text-transform: capitalize; font-weight: bold;">Ringgit Malaysia :
				
				@php

					$f = new \NumberFormatter("ms", \NumberFormatter::SPELLOUT);
					$amaun = money()->toCommon($total_payment ?? "0" , 2);
					$format = explode('.',$amaun);
					$formats = substr($amaun,-2);
					$partringgit = str_replace(',', '', $format[0]);
					$partsen = $format[1];
					$partkosongsen = (int)$formats;

					if($partkosongsen == '.00'){
						echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Sahaja" . '</span>';
					}
					else{
						echo '<span style="text-transform: capitalize; font-weight: bold;">' . $f->format($partringgit) . " Dan Sen " . $f->format($partkosongsen) . " Sahaja" . '</span>';
					}

				@endphp					
				
				({{ money()->toHuman($total_payment ?? "0" , 2) }}	)</span> 
				menunjukkan baki muktamad di bawah kontrak adalah patut dan kena dibayar oleh Perbadanan seperti berikut :
			</span>
		</td>
	</tr>
	<tr>
		<td width="5%">1.&nbsp;&nbsp;&nbsp;</td>
		<td width="95%" colspan="3">Kontraktor Utama</td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="65%">(i) Nilai kerja dan amaun-amaun lain</td>
		<td width="15%">
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				{{ money()->toCommon($total_other_amount ?? "0", 2) }}
			</span>
		</td>
		<td width="15%"></td>
	</tr>
	<tr>
		<td></td>
		<td>(ii) Pelepasan Deposit Wang Jaminan Pelaksanaan</td>
		<td>
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				@if($sst->bon->bon_type == 4)
					{{ money()->toCommon($total_wjp ?? "0", 2) }}
				@endif
			</span>
		</td>
		<td>
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				{{ money()->toCommon($total_contractor ?? "0", 2) }}
			</span>			
		</td>
	</tr>	
	<tr>
		<td>2.&nbsp;&nbsp;&nbsp;</td>
		<td colspan="2">Kepada Subkontraktor / Penerima Bayaran seperti di Lampiran A</td>
		<td width="20%">
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">
				{{ money()->toCommon($total_balanced ?? "0", 2) }}
			</span>			
		</td>
	</tr>	
	<tr>
		<td>3.&nbsp;&nbsp;&nbsp;</td>
		<td>
			Kredit Hasil Bagi Gantirugi Tertentu Dan Ditetapkan<br>
			&nbsp;&nbsp;&nbsp;@................... selama ...............hari
		</td>
		<td width="20%"></td>
		<td width="20%">
			<span style="text-align: left"> RM </span>
			<span style="text-align: right">0.00</span>			
		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="2"  style="text-align: center; text-transform: uppercase; font-weight: bold;">Jumlah Bayaran Muktamad</td>
		<td style="text-align: right; font-weight: bold; border-top: 1px double #000; border-bottom: 1px double #000;">
			<span style="text-align: left"> RM </span>
			{{ money()->toCommon($total_payment ?? "0" , 2) }}		
		</td>
	</tr>
	<tr>
		<td colspan="4">
			Akuan Statutori atau Perakuan yang ditandatangani oleh atau bagi pihak Ketua Pengarah Buruh yang dikehendaki 
			di bawah fasal 6.2.2** syarat-syarat 
			@if($sst->acquisition->category->name == 'Sebut Harga' || $sst->acquisition->category->name == 'Sebut Harga B') 
				sebut harga
			@elseif($sst->acquisition->category->name == 'Tender Terhad' || $sst->acquisition->category->name == 'Tender Terbuka') 
				tender
			@endif
			telah dikemukakan oleh kontraktor.
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tarikh : .........................</td>
		<td colspan="2" style="text-align:center">
			..........................................<br>
			Wakil Perbadanan
		</td>
	</tr>
</table>
</div>

<div><hr/></div>
<div class="row" style="text-align: left; padding:3px 3px; font-size:14px;">
*&nbsp;&nbsp;&nbsp;Tambahan/potongan yang telah diluluskan seperti Penyata Pelarasan Harga Kontrak<br>
**&nbsp;&nbsp;&nbsp;Potong yang tidak berkaitan
</div>
