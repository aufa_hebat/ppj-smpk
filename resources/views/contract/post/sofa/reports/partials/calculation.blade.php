@php

    $total_sum_reduced = 0.0;
    $total_sum_additional = 0.0;
    $total_overal = 0.0;
    
    if($vo > 0) {
        foreach($vo as $index => $vod){
            $sum_reduced = 0.0;
            $sum_additional = 0.0;

            foreach($vo_element as $index => $voe) {
                if($voe->variationOrder->id == $vod->id){
                    $sum_reduced = $sum_reduced + $voe->estimated_reduced_amount; 
                    $sum_additional = $sum_additional + $voe->estimated_additional_amount; 
                    $sum_add_red    =   $sum_additional - $sum_reduced;					
                }
            }

            if($sum_additional > $sum_reduced){
                $total_sum_additional = $total_sum_additional + $sum_add_red;
            }else{
                $total_sum_reduced = $total_sum_reduced + $sum_add_red;
            }
                    
            $total_overal = $total_sum_additional + $total_sum_reduced;	
        }
    }



	$total_last_payment = $appointed->offered_price;
	$total_last_payment = $total_last_payment + $total_overal;

	$total_reduced = ($prev_total_invoice + ($deposit->total_amount ?? 0));
	$total_payment = ($total_last_payment - $total_reduced);
	$total_wjp = 0;
	$total_other_amount = 0;
	$total_contractor = 0;

	if($sst->bon->bon_type == 4){
		$total_wjp = $sst->bon->money_amount;						
	}

	$total_other_amount = $total_payment - $total_wjp;
	$total_contractor = $total_other_amount + $total_wjp;
				
@endphp