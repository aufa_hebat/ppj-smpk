<footer>
	<div class="pagenum-container title-footer">Page <span class="pagenum"></span></div>
</footer>

<div class="row" style="font-size:14px; text-align: justify; padding:3px 3px; text-transform: uppercase; font-weight: bold;">
	C. Pengakuan Persetujuan Kontraktor Ke Atas Perakuan Akaun Dan Bayaran Muktamad
</div>
<div><br/><br/></div>
<div class="row" style="text-align: justify; padding:3px 3px; font-size:14px;">
	<table width="100%">
		<tr>
			<td colspan="2">
				<span style="font-style: italic;">
					(Kontraktor dikehendaki menurunkan tandatangan dan mengembalikan dokumen ini dalam tempoh tiga puluh (30) hari dari tarikh penerimaan jika Perakuan Bayaran Muktamad ini dipersetujui.  Jika kontraktor gagal berbuat demikian, pejabat ini akan meneruskan dengan bayaran muktamad)
				</span>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan="2">
				<span>
					Saya/Kami yang bertandatangani di bawah ini mengaku penerimaan Perakuan Akaun dan bayaran Muktamad di atas dan setelah meneliti butir-butir terkandung di dalamnya, <b>BERSETUJU</b> dengan Bayaran Muktamad yang diperakukan dan mengaku Saya/Kami tidak ada tuntutan lanjut di bawah kontrak ini.
				</span>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td width="50%">
				..........................................<br>
				Tandatangan Saksi<br><br/>
				Nama Penuh:<br>
				No K.P :<br>
				Alamat : <br><br/><br/><br/>
				Tarikh :
			</td>
			<td  width="50%">
				..........................................<br>
				Tandatangan Kontraktor<br><br/>
				Nama Penuh:<br>
				No K.P :<br>
				Alamat : <br><br/><br/><br/>
				Tarikh :
			</td>			
		</tr>
	</table>
</div>