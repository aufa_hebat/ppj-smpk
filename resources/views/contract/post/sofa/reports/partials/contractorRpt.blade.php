@php
	$total_amount = 0.0;
	$total_invoice = 0.0;
	$total_balanced = 0.0;
@endphp
<div class="row" style="font-size:14px;">
	<div class="col-12" style="text-transform: uppercase; font-weight: bold; text-align: left;">
		penyata perakuan akaun bayaran muktamad
	</div>
	<div><br/></div>
	<div class="col-12">
		Amaun-amaun yang kena dibayar kepada Subkontraktor / Pembekal Dinamakan, 
		Penerima hak dan Pegawai Penyelia di Tapakbina
	</div>
	<div><br/></div>
	<div class="col-12">
		<table  style="width: 100%;border-collapse: collapse;padding: 4 4;">
			<tr>
				<td width="5%" valign="top" style="text-align:center" class="bordered">Ruj.<br><br><br><br>(1)</td>
				<td width="35%" valign="top" style="text-align:center" class="bordered">Penerima Bayaran<br><br><br><br>(2)</td>
				<td width="24%" valign="top" style="text-align:center" class="bordered">Jenis Kerja atau Bulan<br><br><br><br>(3)</td>
				<td width="12%" valign="top" style="text-align:center" class="bordered">Jumlah Harga<br><br>(RM)<br>(4)</td>
				<td width="12%" valign="top" style="text-align:center" class="bordered">Jumlah Bayaran Terdahulu<br>(RM)<br>(5)</td>
				<td width="12%" valign="top" style="text-align:center" class="bordered">Amaun yang Diperlakukan<br><br>(RM)<br>(6)</td>
			</tr>
			<tr>
				<td class="bordered" style="text-align:center" valign="top">A.</td>
				<td class="bordered" style="text-align:left; ">
					<span style="text-decoration: underline; ">Subkontraktor Yang Dinamakan</span><br>
					@if(!empty($sub_contract))
						@foreach($sub_contract as $key => $row)
							<span style="padding: 5px 0px;">{{$roman[$key]}}) &nbsp;{{ $row->company->company_name }}</span><br/>
						@endforeach
					@endif
					<br/>					
				</td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center">
					@if(!empty($sub_contract))
						@foreach($sub_contract as $row)
							{{ $row->amount ? money()->toCommon($row->amount ?? "0", 2):null }}<br/>
							@php
								$total_amount = $total_amount + $row->amount;
							@endphp
						@endforeach
					@endif
				</td>
				<td class="bordered" style="text-align:center">
					@if(!empty($sub_contract))
						@foreach($sub_contract as $row)
							{{ $row->invoices->pluck('invoice')->sum() ? money()->toCommon($row->invoices->pluck('invoice')->sum() ?? "0", 2) : "0.00" }}<br/>
							@php
								$total_invoice = $total_invoice + $row->invoices->pluck('invoice')->sum();
							@endphp
						@endforeach
					@endif
				</td>
				<td class="bordered" style="text-align:center">
					@if(!empty($sub_contract))
						@foreach($sub_contract as $row)
							@php
								$balanced = $row->amount - $row->invoices->pluck('invoice')->sum();
								$total_balanced = $total_balanced + $balanced;
							@endphp
							{{ $balanced ? money()->toCommon($balanced ?? "0", 2) : "0.00" }}							
							<br/>
						@endforeach
					@endif
				</td>
			</tr>
			<tr>
				<td style="border-right: 1px solid #959594; border-left: 1px solid #959594;"></td>
				<td style="border-right: 1px solid #959594;" colspan="4"><span valign="bottom">Jumlah Kecil :- </span></td>
				<td style="border-right: 1px solid #959594;border-top: 1px solid #959594; text-align: center;">
					{{ $total_balanced ? money()->toCommon($total_balanced ?? "0", 2) : 0.00 }}
				</td>
			</tr>
			<tr>
				<td class="bordered" style="text-align:center" valign="top">B.</td>
				<td class="bordered" style="text-align:left">
					<span style="text-decoration: underline;">Pembekal Dinamakan</span><br>
					i)<br>
					ii)<br>
					iii)<br><br>
					Jumlah Kecil :- 
				</td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
			</tr>
			<tr>
				<td class="bordered" style="text-align:center" valign="top">C.</td>
				<td class="bordered" style="text-align:left">
					<span style="text-decoration: underline;">Penerima Hak</span><br>
					i)<br>
					ii)<br>
					iii)<br><br>
					Jumlah Kecil :- 
				</td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
			</tr>
			<tr>
				<td class="bordered" style="text-align:center" valign="top">D.</td>
				<td class="bordered" style="text-align:left">
					<span style="text-decoration: underline;">Pegawai Penyelia</span><br>
					i)<br>
					ii)<br>
					iii)<br><br>
					Jumlah Kecil :- 
				</td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
				<td class="bordered" style="text-align:center"></td>
			</tr>									
		</table>
	</div>
</div>
<div class="row"><br></div>
<div class="row"><br></div>
<div class="row"><br></div>
<div class="row"><br></div>
<div class="row"><br></div>
<div class="row">
	<table width="100%">
		<tr>
			<td width="70%">
			<td width="30%" style="text-align: center">
				.....................................<br>
				(Pegawai Pengesyor)
			</td>
		</tr>
	</table>
</div>
<div class="row" style="font-size: 12px">
	Nota : <br/>
	(2) Isikan nama subkontraktor / penerima bayaran yang berkenaan<br>
	(3) Nyatakan jenis kerja subkontraktor atau bekalan atau bulan yang berkenaan bagi kategori pegawai penyelia. Tidak berkenaan bagi penerima hak<br>
	(4) Isikan jumlah harga subkontrak / jumlah yang diserahhak.  Tidak berkenaan untuk kategori pegawai penyelia.
</div>