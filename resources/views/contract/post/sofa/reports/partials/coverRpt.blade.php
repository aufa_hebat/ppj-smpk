<table width="100%">
	<tr>
		<td class="bordered" height="750">
			<center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center>
			<br/><br/><br/>
			<table width="100%">
				<tr>
					<td style="text-align: center; text-transform: uppercase; font-size: 26px; font-weight:bold; font-family: Arial, Helvetica, sans-serif;">
						{{ $sst->acquisition->title }}</td>
				</tr>
				<tr>
					<td style="text-align: center; text-transform: uppercase; font-size: 26px; font-family: Arial, Helvetica, sans-serif;">
						<br/>
						No. Kontrak : {{ $sst->acquisition->reference }}
						<br/>						
					</td>
				</tr>
				<tr>
					<td>
						<br/>
						<hr/>
						<br/>
					</td>
				</tr>
				<tr>
					<td style="text-align: center; text-transform: uppercase; font-size: 26px; font-family: Arial, Helvetica, sans-serif;">
						PERAKUAN AKAUN DAN BAYARAN MUKTAMAD
						<br/>
					</td>
				</tr>
				<tr>
					<td>
						<br/>
						<hr/>
						<br/>
					</td>
				</tr>
				<tr>
					<td  style="text-align: center; text-transform: uppercase; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">
						<b><u>PEMILIK</u></b>
						<br/><br/>
						<b>PERBADANAN PUTRAJAYA</b><br/>
						KOMPLEKS PERBADANAN PUTRAJAYA<br/>
						24, PERSIARAN PERDANA, PRESINT 3<br/>
						62675, PUTRAJAYA
						<br/><br/><br/><br/>
						<b><u>KONTRAKTOR</u></b>
						<br/><br/>
						<b>{{ $sst->company->company_name }}</b><br/>
						
						@if(!empty($sst->company->addresses[0]))
                    		{{ $sst->company->addresses[0]->primary }}<br/>
                        	{{ $sst->company->addresses[0]->secondary }}<br/>
                        	{{ $sst->company->addresses[0]->postcode }}, {{ $sst->company->addresses[0]->state }}
                        @endif

					</td>
				</tr>				
			</table>			
		</td>
	</tr>
</table>