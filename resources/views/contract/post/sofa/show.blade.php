@extends('layouts.admin')
@push('scripts')

@endpush
@section('content')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="sofa-tab-content" role="tablist">
                <li class="list-group-item" id="contract">
                    <a class="list-group-item-action" data-toggle="tab" href="#contract-details" role="tab" aria-controls="contract-details" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Maklumat Kontrak') }}
                    </a>
                </li>
                <li class="list-group-item" id="akuan">
                    <a class="list-group-item-action" data-toggle="tab" href="#akuan-details" role="tab" aria-controls="akuan-details" aria-selected="false">
                        @icon('fe fe-dollar-sign')&nbsp;{{ __('Perakuan Akaun') }}
                    </a>
                </li>     
                <li class="list-group-item" id="subcontractor">
                    <a class="list-group-item-action" data-toggle="tab" href="#subcontractor-details" role="tab" aria-controls="subcontractor-details" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Subkontraktor') }}
                    </a>
                </li>    
                <li class="list-group-item" id="statement">
                    <a class="list-group-item-action" data-toggle="tab" href="#statement-details" role="tab" aria-controls="statement-details" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Penyata') }}
                    </a>
                </li>                                           
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'sofa'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'contract-details', 'active' => true])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.contract', ['sst' => $sst])
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'akuan-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.akuan', ['sst' => $sst])
                                @endslot
                            @endcomponent 
                            @component('components.tab.content', ['id' => 'subcontractor-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.contractor', ['sst' => $sst])
                                @endslot
                            @endcomponent 
                            @component('components.tab.content', ['id' => 'statement-details'])
                                @slot('content')
                                    @include('contract.post.sofa.partials.forms.statement', ['sst' => $sst])
                                @endslot
                            @endcomponent                                                                                  
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection