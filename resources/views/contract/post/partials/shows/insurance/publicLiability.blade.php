<div id="publicLiability">
    <div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('Nama Pengeluar Insuran') }}</div>
            <div id="public_insurance_name"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Nilai Polisi (RM)') }}</div>
            <div id="public_policy_amount"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Mula Sah Laku Polisi') }}</div>
            <div id="public_start_date"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Akhir Mengemukakan Polisi') }}</div>
            <div id="public_proposed_date"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Penerimaan Pengesahan Polisi') }}</div>
            <div id="public_approval_received_date"></div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('No Polisi') }}</div>
            <div id="public_policy_no"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Terima Insuran') }}</div>
            <div id="public_received_date"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Luput Polisi') }}</div>
            <div id="public_expired_date"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Mengemukakan Pengesahan Polisi') }}</div>
            <div id="public_approval_proposed_date"></div>
        </div>
    </div>
</div>
    @include('contract.post.partials.shows.insurance.uploads_publicLiability')
</div>