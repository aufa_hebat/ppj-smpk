<div id="jaminanInsuran">
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Nama Pengeluar Insuran') }}</div>
                <div id="insurance_name"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('No Jaminan Insuran') }}</div>
                <div id="insurance_no"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Bermula Jaminan Insuran') }}</div>
                <div id="insurance_start_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Akhir Mengemukakan Jaminan Insuran') }}</div>
                <div id="insurance_proposed_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Insuran') }}</div>
                <div id="insurance_approval_received_date"></div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Terima Insuran') }}</div>
                <div id="insurance_received_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Nilai Jaminan Insuran (RM)') }}</div>
                <div id="insurance_amount"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Luput Jaminan Insuran') }}</div>
                <div id="insurance_expired_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Mengemukakan Surat Pengesahan Jaminan Insuran') }}</div>
                <div id="insurance_approval_proposed_date"></div>
            </div>
        </div>
    </div>

</div>