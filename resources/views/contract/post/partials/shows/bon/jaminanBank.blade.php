<div id="jaminanBank">
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Nama Pengeluar Bank') }}</div>
                <div id="bank_name"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('No Jaminan Bank') }}</div>
                <div id="bank_no"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Bermula Jaminan Bank') }}</div>
                <div id="bank_start_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Akhir Mengemukakan Jaminan Bank') }}</div>
                <div id="bank_proposed_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Bank') }}</div>
                <div id="bank_approval_received_date"></div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Terima Bon Pelaksanaan') }}</div>
                <div id="bank_received_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Nilai Jaminan Bank (RM)') }}</div>
                <div id="bank_amount"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Luput Jaminan Bank') }}</div>
                <div id="bank_expired_date"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Mengemukakan Surat Pengesahan Jaminan Bank') }}</div>
                <div id="bank_approval_proposed_date"></div>
            </div>
        </div>
    </div>

</div>