<div id="cekBank">
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Nama Pengeluar Bank') }}</div>
                <div id="cheque_name"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('No Resit Dari Bahagian Pengurusan Kewangan (BPK)') }}</div>
                <div id="cheque_resit_no"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Terima Resit Dari Bahagian Pengurusan Kewangan') }}</div>
                <div id="cheque_received_date"></div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('No Cek') }}</div>
                <div id="cheque_no"></div>
            </div>
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Hantar Cek Ke Bahagian Pengurusan Kewangan (BPK)') }}</div>
                <div id="cheque_submitted_date"></div>
            </div>
        </div>
    </div>

</div>