<div id="wangJaminan">
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Tarikh Terima Surat Permohonan WJP') }}</div>
                <div id="money_received_at"></div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <div class="text-muted"> {{ __('Nilai Wang Jaminan Pelaksanaan') }}</div>
                <div id="money_amount"></div>
            </div>
        </div>
    </div>

</div>