<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('No Rujukan') }}</div>
            <div id="deposit_reference_no"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Wang Kos Prima (RM)') }}</div>
            <div id="deposit_prime_cost"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Amaun (RM)') }}</div>
            <div id="deposit_amount"></div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('Jumlah Wang Pendahuluan (RM)') }}</div>
            <div id="deposit_total_amount"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Nilai Layak Bayaran Pendahuluan (RM)') }}</div>
            <div id="deposit_qualified_amount"></div>
        </div>
    </div>
</div>