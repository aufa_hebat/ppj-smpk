@if(!empty($sst->sofa_signature_date))
	<div class="btn-group float-right" id="submit">
		<a href="{{ route('contract.post.contract-completed.showPerolehan', $sst->hashslug) }}" 
			class="btn btn-default border-primary">
			{{ __('Kembali') }}
		</a>
	</div>
@else
	<div class="btn-group float-right" id="submit">
		{{ html()->a(route('contract.post.index'), __('Kembali'))->class('btn btn-outline-primary') }}

	</div>
@endif
