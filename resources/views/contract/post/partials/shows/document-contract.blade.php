@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            // onload fields
            @if(!empty($document))
                $('#document_revenue_stamp_date').html('{{ $document->revenue_stamp_date? \Carbon\Carbon::createFromFormat('Y-m-d',$document->revenue_stamp_date)->format('d/m/Y'):null }}');
                $('#document_preparation_period').html('{{ $document->preparation_period }}' + ' ' + 'Hari');
                $('#document_blr').html('{{ $document->base_lending_rate }}');
                $('#document_LAD_amount').html('{{ $document->LAD_rate ? money()->toHuman($document->LAD_rate) : null }}');
                $('#document_submitted_date').html('{{ $document->submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d',$document->submitted_date)->format('d/m/Y'):null }}');
                $('#document_proposed_date').html('{{ $document->proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$document->proposed_date)->format('d/m/Y'):null }}');
            @endif

        });


    </script>
@endpush

<h4>Dokumen Kontrak</h4>
<br>
<div class="row">
    <div class="col-12">
        @include('contract.post.partials.shows.document.info')
        @include('contract.post.partials.shows.return')
    </div>
</div>