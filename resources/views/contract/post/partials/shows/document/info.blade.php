<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Mati Setem') }}</div>
            <div id="document_revenue_stamp_date"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tempoh Penyediaan Dokumen Kontrak') }}</div>
            <div id="document_preparation_period"></div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Terima Dokumen') }}</div>
            <div id="document_submitted_date"></div>
        </div>
        <div class="form-group">
            <div class="text-muted"> {{ __('Tarikh Akhir Penyediaan Dokument Kontrak') }}</div>
            <div id="document_proposed_date"></div>
        </div>
    </div>
</div>