@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {
            @foreach($sst->acquisition->bqElements as $bq)
                @foreach($bq->bqItems as $it)
                    $heys = $("#ulasan_bq{{ $it->id }}").DataTable({
                        //order: false,
                        //paging:false,
                    });
                @endforeach
            @endforeach
        });
    </script>
@endpush
<h5>Pelarasan Harga</h5>
<br>
<div id="table" class="table-editable table-bordered border-0">
    @foreach($sst->acquisition->bqElements->sortBy('no') as $bq)
    <div class="mb-0 card" >
        <div class="card-header" role="tab" id="heading{{$bq->id}}">
            <h4 class="card-title cardElemen">
                <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{$bq->id}}" href="#collapse{{$bq->id}}" aria-expanded="true" aria-controls="collapse{{$bq->id}}">
                    <i class="mr-2 fa fa-folder-open"></i><u>{{$bq->no}}&nbsp;:&nbsp;{{$bq->element}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($bq->amount ?? 0,2 )}}</u>
                </a>
            </h4>
        </div>
        <div id="collapse{{$bq->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{$bq->id}}" >
            <div class="card-body">
                <div class="row">
                    <div id="semakanBq" class="col-md-12 table-responsive">
                        <p>@if(!empty($bq->description)) <u>{{ $bq->description }}</u> @endif</p>
                        <table class="table1  table-no-border" width="100%" cellspacing="0">
                            @if(!empty($bq->bqItems) && $bq->bqItems->count() > 0)
                            @foreach($bq->bqItems->sortBy('no') as $it)
                            
                            <thead >
                                <th style="width:100%;border:none" >
                                    <h5>{{$it->bq_no_element}}.{{$it->no}}&nbsp;:&nbsp;{{$it->item}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($it->amount ?? 0,2 )}}</h5><br>
                                    <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                </th>
                            </thead>
                            <tbody class="list">
                                <tr style="width:100%;border:none">
                                    <td class="ulasanBQs">
                                        <table id="ulasan_bq{{$it->id}}" class="table table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <th style="width:5%">NO SUB ITEM</th>
                                                <th style="width:30%">SUB ITEM</th>
                                                <th style="width:25%">KETERANGAN</th>
                                                <th style="width:10%">UNIT</th>
                                                <th style="width:10%">KUANTITI</th>
                                                <th style="width:10%">HARGA SEUNIT</th>
                                                <th style="width:10%">AMAUN</th>
                                                <th style="width:10%">HARGA SEUNIT TERLARAS</th>
                                                <th style="width:10%">AMAUN TERLARAS</th>
                                            </thead>
                                            <tbody>
                                            @if(!empty($it->bqSubItems) && $it->bqSubItems->count() > 0)
                                            @foreach($it->bqSubItems->sortBy('no') as $bqs)
                                            <tr>
                                                <td>{{$bqs->bq_no_element}}.{{$bqs->bq_no_item}}.{{$bqs->no}}</td>
                                                <td>{{$bqs->item}}</td>
                                                <td>{{$bqs->description}}</td>
                                                <td>{{$bqs->unit}}</td>
                                                <td>{{money()->toCommon($bqs->quantity ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->rate_per_unit ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->amount ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->rate_per_unit_adjust ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->amount_adjust ?? 0,2 )}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<br>
@include('contract.post.partials.shows.return')