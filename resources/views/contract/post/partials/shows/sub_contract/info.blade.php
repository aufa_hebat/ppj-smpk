<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-sm table-transparent table-hover" >
                <thead>
                <th>Syarikat</th>
                <th>Harga</th>
                </thead>
                <tbody>
                @if(!empty($sub_contract))
                    @foreach($sub_contract as $row)
                        <tr>
                            <td>
                                {{ $row->company->company_name }}
                            </td>
                            <td>
                                {{ $row->amount ? money()->toHuman($row->amount):null }}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>

            </table>
        </div>

    </div>
</div>