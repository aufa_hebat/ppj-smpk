@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.select2')

    <script>
        jQuery(document).ready(function($) {

            $('#doc_send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#doc_sentStatus').val(),
                    acquisition_id : $('#doc_acquisition_id').val(),
                    requested_by : $('#doc_requested_by').val(),
                    progress : $('#doc_progress').val(),
                    task_by : $('#doc_task_by').val(),
                    law_task_by : $('#doc_law_task_by').val(),
                    approved_by : $('#doc_approved_by').val(),
                    created_by : $('#doc_created_by').val(),
                    approved_at : $('#doc_approved_at').val(),
                    type : $('#doc_type').val(),
                    document_contract_type : $('#doc_document_contract_type').val(),
                    status : $('#doc_status').val(),
                    department : $('#doc_department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#doc_send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // onload fields
            @if(!empty($document))
                $('#document_revenue_stamp_date').val('{{ $document->revenue_stamp_date? \Carbon\Carbon::createFromFormat('Y-m-d',$document->revenue_stamp_date)->format('d/m/Y'):null }}');
                $('#document_preparation_period').val('{{ $document->preparation_period }}' + ' ' + 'Hari');
                $('#document_blr').val('{{ $document->base_lending_rate }}');
                $('#document_LAD_amount').val('{{ $document->LAD_rate ? money()->toHuman($document->LAD_rate) : null }}');
                $('#document_submitted_date').val('{{ $document->submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d',$document->submitted_date)->format('d/m/Y'):null }}');
                $('#document_late_days').val('{{ $document->late_days }}');
                if($('#document_late_days').val() > 0){
                    $('#document_late_days').addClass("is-invalid");
                }
            @endif

            $('#document_proposed_date').val('{{ ($document_proposed_date != null)? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$document_proposed_date)->format('d/m/Y'):null }}');


            $('#datetimepicker-document_revenue_stamp_date').on('change.datetimepicker',function(selectedDate){
                var revenue_stamp_date = moment(selectedDate.date);
                var sst_received_date = moment('{{ $sst->al_contractor_date }}', 'YYYY-MM-DD');
                var diffDays = revenue_stamp_date.diff(sst_received_date,'days');
                $('#document_preparation_period').val(diffDays + ' ' + 'Hari');

                var late_days = 0;
                $('#document_late_days').removeClass("is-invalid");
                if(diffDays > 120){
                    late_days = diffDays - 120;
                    $('#document_late_days').addClass("is-invalid");
                }
                $('#document_late_days').val(late_days + ' ' + 'Hari');

            });

            

            $(document).on('click', '#document-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                console.log(route(route_name, {id:id}));
                console.log(data);
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Dokumen Kontrak') !!}', response.data.message, 'success');
                    location.reload();
            }).catch((error)=>{
                    console.log(error.response.data);
            });
            });
        });


    </script>
@endpush

<h4>Dokumen Kontrak</h4>
<br>
<div class="row">
    <div class="col-12">
        <form id="document-form">
            @include('contract.post.partials.indexes.document.info')
        </form>
        <div class="btn-group float-right">
            @role('penyedia')
                @if(!empty($document) && $document->sst->user_id == user()->id)
                    @if(empty($reviewdoc) || empty($reviewdoc->status) || $reviewdoc->status == 'CN')
                        <form method="POST" action="{{ route('acquisition.review.store') }}">
                            @csrf
                            <input id="doc_sentStatus" name="sentStatus" type="hidden" value="1" />
                            <input type="text" id="doc_acquisition_id" name="acquisition_id" value="@if(!empty($document)){{ $document->sst->acquisition_id }}@endif" hidden>

                            <input type="text" id="doc_requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                            <input type="text" id="doc_approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                            <input type="text" id="doc_created_by" name="created_by" value="{{user()->id}}" hidden>

                            <input type="text" id="doc_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                            @if(!empty($review_previous))
                                <input type="hidden" id="doc_task_by" name="task_by" value="{{$review_previous->task_by}}">
                                <input type="hidden" id="doc_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                <input type="text" id="doc_type" name="type" value="Dokumen Kontrak" hidden>
                                <input type="text" id="doc_document_contract_type" name="document_contract_type" value="Dokumen Kontrak" hidden>
                            @endif
                            
                            <button type="button" id="doc_send" class="btn btn-default float-middle border-default">
                                @icon('fe fe-send') {{ __('Teratur') }}
                            </button>
                        </form>
                    @endif
                @endif
            @endrole
        </div>
        @include('contract.post.partials.indexes.document.submit', [
            'route_name' => 'api.contract.acquisition.contract-document.update',
            'form' => 'document-form'
        ])
    </div>
</div>