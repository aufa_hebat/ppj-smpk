@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            $('#bon_send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#bon_sentStatus').val(),
                    acquisition_id : $('#bon_acquisition_id').val(),
                    requested_by : $('#bon_requested_by').val(),
                    progress : $('#bon_progress').val(),
                    task_by : $('#bon_task_by').val(),
                    law_task_by : $('#bon_law_task_by').val(),
                    approved_by : $('#bon_approved_by').val(),
                    created_by : $('#bon_created_by').val(),
                    approved_at : $('#bon_approved_at').val(),
                    type : $('#bon_types').val(),
                    document_contract_type : $('#bon_document_contract_type').val(),
                    status : $('#bon_status').val(),
                    department : $('#bon_department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#bon_send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#jaminanBank').hide();
            $('#jaminanInsuran').hide();
            $('#cekBank').hide();
            $('#wangJaminan').hide();
            $('#submit').hide();

            @if(!empty($bon))
                @if( $bon->bon_type == 1 )
                $('#jaminanBank').show();
                @elseif($bon->bon_type == 2)
                $('#jaminanInsuran').show();
                @elseif($bon->bon_type == 3)
                $('#cekBank').show();
                @elseif($bon->bon_type == 4)
                 @if($sst->acquisition->approval->acquisition_type_id != 4)
                 $('#wangJaminan').show();
                 @endif
                @endif

                // onload fields
                $("#bon_type").val('{{ $bon->bon_type }}');

                @if($bon->bon_type)
                $('#submit').show();
                @endif

                $('#bank_name').val('{{ $bon->bank_name }}');
                $('#bank_no').val('{{ $bon->bank_no }}');
                $('#bank_received_date').val('{{ $bon->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_received_date)->format('d/m/Y'):null }}');

                $('#insurance_name').val('{{ $bon->insurance_name }}');
                $('#insurance_no').val('{{ $bon->insurance_no }}');
                $('#insurance_received_date').val('{{ $bon->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_received_date)->format('d/m/Y'):null }}');

                $('#cheque_name').val('{{ $bon->cheque_name }}');
                $('#cheque_resit_no').val('{{ $bon->cheque_resit_no }}');
                $('#cheque_received_date').val('{{ $bon->cheque_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->cheque_received_date)->format('d/m/Y'):null }}');
                $('#cheque_no').val('{{ $bon->cheque_no }}');
                $('#cheque_submitted_date').val('{{ $bon->cheque_submitted_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->cheque_submitted_date)->format('d/m/Y'):null }}');

                $('#money_received_at').val('{{ $bon->money_received_at? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->money_received_at)->format('d/m/Y'):null }}');
                $('#money_amount').val('{{ $bon->money_amount }}');
            @endif

            @if(!empty($sst))
                $('#bank_amount').val('{{ money()->toHuman($sst->bon_amount) }}');
                $('#bank_start_date').val('{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}');
                $('#bank_expired_date').val('{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}');
                $('#bank_proposed_date').val('{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}');

                $('#insurance_amount').val('{{ money()->toHuman($sst->bon_amount) }}');
                $('#insurance_start_date').val('{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}');
                $('#insurance_expired_date').val('{{ $expiry_date_insuran ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date_insuran)->format('d/m/Y'):null }}');
                $('#insurance_proposed_date').val('{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}');

                $('#money_amount').val('{{ money()->toHuman($sst->bon_amount) }}');
            @endif

            $('#bon_type').change(function () {
                $('#submit').show();

                //clear all field
                $('#bank_name').val(null);
                $('#bank_no').val(null);
                $('#bank_approval_proposed_date').val(null);
                $('#bank_approval_received_date').val(null);
                $('#bank_received_date').val(null);

                $('#insurance_name').val(null);
                $('#insurance_no').val(null);
                $('#insurance_approval_proposed_date').val(null);
                $('#insurance_approval_received_date').val(null);
                $('#insurance_received_date').val(null);

                $('#cheque_name').val(null);
                $('#cheque_resit_no').val(null);
                $('#cheque_received_date').val(null);
                $('#cheque_no').val(null);
                $('#cheque_submitted_date').val(null);

                $('#money_received_at').val(null);
                $('#money_value').val(null);

                if(this.value == '1'){
                    $('#jaminanBank').show();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').hide();
                    $('#wangJaminan').hide();
                }
                else if (this.value == '2'){
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').show();
                    $('#cekBank').hide();
                    $('#wangJaminan').hide();
                }
                else if (this.value == '3'){
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').show();
                    $('#wangJaminan').hide();
                }
                else if (this.value == '4'){
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').hide();
                    $('#wangJaminan').show();
                }
                else{
                    $('#jaminanBank').hide();
                    $('#jaminanInsuran').hide();
                    $('#cekBank').hide();
                    $('#wangJaminan').hide();
                    $('#submit').hide();
                }
            });

            /* DOCUMENT UPLOAD */
            var t = $('#tblUpload').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRow').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile'+ counter +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
            } );

            @if(!empty($bon) && ($bon->documents->count() > 0))
            @foreach($bon->documents as $doc)
            t.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile'+ counter +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId'+ counter +'" name="hDocumentId[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile'+ counter +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile" id="uploadFile'+ counter +'" name="uploadFile[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow').click();
            @endif

            $("#tblUpload").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                    }
                });
            });

            $("#tblUpload").on('change','.uploadFile',function(){
                var no = $(this).data('counter');
                $('#subFile' + no).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD */

            $(document).on('click', '#bon-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                axios.post(route(route_name, id), data).then( (response) => {
                        swal(response.data);
                        location.reload();
                    }).catch((error)=>{
                        console.log(error.response);
                    });
            });
        });


    </script>
@endpush

<h4>Bon Pelaksanaan</h4>
<div class="row">

    <div class="col-12">
        <form id="bon-form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="bon_type"
                       class="col col-form-label">{{ __('Jenis Bon Pelaksanaan') }}
                </label>

                <div class="col">
                    <select id="bon_type" name="bon_type" class="form-control w-100" {{ $bon_has_reviewed ? 'disabled':'' }}>
                        <option value="">{{ __('Sila Pilih') }}</option>
                        @foreach($bon_types as $index => $type)
                            <option value="{{ $type->code }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        @include('contract.post.partials.indexes.bon.jaminanBank')
        @include('contract.post.partials.indexes.bon.jaminanInsuran')
        @include('contract.post.partials.indexes.bon.cekBank')
         @if($sst->acquisition->approval->acquisition_type_id == 2)
          @include('contract.post.partials.indexes.bon.wangJaminan')
         @endif
        @include('contract.post.partials.indexes.bon.uploads')

        </form>
        <div class="btn-group float-right">
            @role('penyedia')
                @if(!empty($bon) && $bon->sst->user_id == user()->id)
                    @if(empty($reviewbon) || empty($reviewbon->status))
                        <form method="POST" action="{{ route('acquisition.review.store') }}">
                            @csrf
                            <input id="bon_sentStatus" name="sentStatus" type="hidden" value="1" />
                            <input type="text" id="bon_acquisition_id" name="acquisition_id" value="@if(!empty($bon)){{ $bon->sst->acquisition_id }}@endif" hidden>

                            <input type="text" id="bon_requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                            <input type="text" id="bon_approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                            <input type="text" id="bon_created_by" name="created_by" value="{{user()->id}}" hidden>

                            <input type="text" id="bon_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                            @if(!empty($review_previous))
                                <input type="hidden" id="bon_task_by" name="task_by" value="{{$review_previous->task_by}}">
                                <input type="hidden" id="bon_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                <input type="text" id="bon_types" name="type" value="Dokumen Kontrak" hidden>
                                <input type="text" id="bon_document_contract_type" name="document_contract_type" value="Bon Pelaksanaan" hidden>
                            @endif
                            
                            <button type="button" id="bon_send" class="btn btn-default float-middle border-default">
                                @icon('fe fe-send') {{ __('Teratur') }}
                            </button>
                        </form>
                    @endif
                @endif
            @endrole
        </div>
        @if(empty($reviewbon) || empty($reviewbon->status) || empty($reviewbon->status == 'Selesai'))
            @include('contract.post.partials.indexes.bon.submit', [
                'route_name' => 'api.contract.acquisition.bon.update',
                'form' => 'bon-form'
            ])
        @endif
    </div>


</div>