@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
    <script>
        function show_alert(){
            $elemen   = $('#element_value').val();
            $elemen   = $elemen.replace(/[,]/g, "");
            $elemen   = $elemen.split(' ').join('');
            $elemen   = parseFloat($elemen);
            
            $contract = $('#contract_value').val();
            $contract = $contract.replace(/[,]/g, "");
            $contract = $contract.split(' ').join('');
            $contract = parseFloat($contract);
            
            if($elemen > $contract){
                $('#show_msq_over').show();
                $('#show_msq_least').hide();
                $('#price-submit').prop('disabled',true);
            }else if($contract > $elemen){
                $('#show_msq_over').hide();
                $('#show_msq_least').show();
                $('#price-submit').prop('disabled',true);
            }else{
                $('#show_msq_over').hide();
                $('#show_msq_least').hide();
                $('#price-submit').prop('disabled',false);
            }
        }
        
        function getAllVal(){
            var tot = 0;
            @foreach($sst->acquisition->bqElements as $bq)
                @foreach($bq->bqItems as $it)
                    var table = $("#ulasan_bq{{ $it->id }}").DataTable();
                    var x = table.$('input[id^=adjust_amt]').serializeArray();
                    $.each(x, function(i, field){
                        if(field.value != '')
                        {
                            $tot = field.value;
                            $tot = $tot.replace(/[,]/g, "");
                            $tot = $tot.split(' ').join('');
                            tot += parseFloat($tot.replace(/[^\d\.]/g,''));
                        }
                    });
                    
                    $('#element_value').val(parseFloat(tot).toFixed(2).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,"));
                @endforeach
            @endforeach
            show_alert();
        }
        
        jQuery(document).ready(function($) {
            show_alert();
            @foreach($sst->acquisition->bqElements as $bq)
                @foreach($bq->bqItems as $it)
                    $heys = $("#ulasan_bq{{ $it->id }}").DataTable({
                        order: false,
                        paging:false,
                    });
                @endforeach
            @endforeach
            
            $(document).on('click', '#price-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                
                var form = document.forms[form_id];
                var data = new FormData(form);
                
                @foreach($sst->acquisition->bqElements as $bq)
                    @foreach($bq->bqItems as $it)
                        var table = $("#ulasan_bq{{ $it->id }}").DataTable();
                        var x = table.$('input').serializeArray();
                        $.each(x, function(i, field){
                            data.append(field.name,field.value);
                        });
                    @endforeach
                @endforeach
                
                axios.post(route(route_name, id), data).then( (response) => {
                        swal('Pelarasan Harga',response.data.message,'success');
                        location.reload();
                    }).catch((error)=>{
                        console.log(error.response);
                    });
            });
        });
    </script>
@endpush
<h5>Pelarasan Harga</h5>

@if(!empty($sst->acquisition) && !empty($sst->acquisition->bqElements) && $sst->acquisition->bqElements->count() > 0)
<form id="price-form" files = "true" enctype="multipart/form-data" method="post">
@csrf
@method('PUT')
<div class="alert alert-danger text-center" id="show_msq_over" style="display:none">
    @icon('fe fe-alert-octagon') Jumlah Amaun Senarai Kuantiti Melebihi dari Harga Kontrak 
</div>
<div class="alert alert-warning text-center" id="show_msq_least" style="display:none">
    @icon('fe fe-alert-octagon') Jumlah Amaun Senarai Kuantiti Kurang dari Harga Kontrak 
</div>
<input id="contract_value" name="contract_value" value="{{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}" hidden/>
<b>Jumlah Keseluruhan Terlaras: <input id="element_value" name="element_value" class="form-control" readonly
@if(!empty($sst->acquisition->bqElements) && $sst->acquisition->bqElements->pluck('amount_adjust')->sum() > 0)
    value="{{ money()->toCommon($sst->acquisition->bqElements->pluck('amount_adjust')->sum() ?? "0" , 2) }}"
@else
    value="{{ money()->toCommon($sst->acquisition->bqElements->pluck('amount')->sum() ?? "0" , 2) }}"
@endif
 /></b>
<div id="table" class="table-editable table-bordered border-0">
    @foreach($sst->acquisition->bqElements->sortBy('no') as $bq)
    <div class="mb-0 card" >
        <div class="card-header" role="tab" id="heading{{ $bq->id }}">
            <h4 class="card-title cardElemen">
                <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{ $bq->id }}" href="#collapse{{ $bq->id }}" aria-expanded="true" aria-controls="collapse{{ $bq->id }}">
                    <i class="mr-2 fa fa-folder-open"></i><u>{{ $bq->no }}&nbsp;:&nbsp;{{ $bq->element }} &nbsp;&nbsp;&nbsp;Harga Tidak Terlaras: RM{{ money()->toCommon($bq->amount ?? 0,2 ) }}&nbsp;&nbsp;&nbsp;Harga Terlaras: RM{{ money()->toCommon($bq->amount_adjust ?? 0,2 ) }}</u>
                </a>
            </h4>
        </div>
        <div id="collapse{{ $bq->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $bq->id }}" >
            <div class="card-body">
                <div class="row">
                    <div id="semakanBq" class="col-md-12 table-responsive">
                        <p>@if(!empty($bq->description)) <u>{{ $bq->description }}</u> @endif</p>
                        <table id="tabless{{$bq->id }}" class="table1  table-no-border" width="100%" cellspacing="0">
                            @if(!empty($bq->bqItems) && $bq->bqItems->count() > 0)
                            @foreach($bq->bqItems->sortBy('no') as $it)
                            <thead >
                                <th style="width:100%;border:none" >
                                    <h5>{{ $it->bq_no_element }}.{{ $it->no }}&nbsp;:&nbsp;{{ $it->item }} &nbsp;&nbsp;&nbsp;Harga Tidak Terlaras : RM{{ money()->toCommon($it->amount ?? 0,2 ) }}&nbsp;&nbsp;&nbsp;Harga Terlaras : RM{{ money()->toCommon($it->amount_adjust ?? 0,2 ) }}</h5><br>
                                    <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                    
                                </th>
                            </thead>
                            <tbody class="list">
                                <tr style="width:100%;border:none">
                                    <td class="ulasanBQs">
                                        <table id="ulasan_bq{{ $it->id }}" class="table table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <th style="width:5%">NO SUB ITEM</th>
                                                <th style="width:30%">SUB ITEM</th>
                                                <th style="width:25%">KETERANGAN</th>
                                                <th style="width:10%">UNIT</th>
                                                <th style="width:10%">KUANTITI</th>
                                                <th style="width:10%">HARGA SEUNIT</th>
                                                <th style="width:10%">AMAUN</th>
                                                <th style="width:10%">HARGA SEUNIT TERLARAS</th>
                                                <th style="width:10%">AMAUN TERLARAS</th>
                                            </thead>
                                            <tbody>
                                            @if(!empty($it->bqSubItems) && $it->bqSubItems->count() > 0)
                                            @foreach($it->bqSubItems->sortBy('no') as $bqs)
                                            <tr>
                                                <td>{{ $bqs->bq_no_element }}.{{ $bqs->bq_no_item }}.{{ $bqs->no }}
                                                <input type="text" id="adjust_hashslug{{ $bqs->id }}" name="adjust[{{ $bqs->id }}][hashslug]" value="{{ $bqs->hashslug }}" hidden>
                                                </td>
                                                <td>{{ $bqs->item }}</td>
                                                <td>{{ $bqs->description }}</td>
                                                <td>{{ $bqs->unit }}</td>
                                                <td><input id="adjust_qty{{ $bqs->id }}" name="adjust[{{ $bqs->id }}][qty]" onkeyup="calc_bq({{ $bqs->id }})" value="{{ money()->toCommon($bqs->quantity ?? 0,2 ) }}" hidden>
                                                    @if($bqs->quantity > 0 || $bqs->quantity < 0)
                                                        @php $quan = strlen($bqs->quantity); @endphp
                                                        {{ substr($bqs->quantity, 0, ($quan - 2) ) }}
                                                    @else
                                                        0
                                                    @endif
                                                </td>
                                                <td>{{ money()->toCommon($bqs->rate_per_unit ?? 0,2 ) }}</td>
                                                <td>{{ money()->toCommon($bqs->amount ?? 0,2 ) }}</td>
                                                <td>
                                                    <input id="adjust_rate{{ $bqs->id }}" onChange="getAllVal()"
                                                        @if(empty($bqs->rate_per_unit_adjust))
                                                            value = "{{ money()->toCommon($bqs->rate_per_unit ?? 0 ,2) }}"
                                                        @else
                                                            value = "{{ money()->toCommon($bqs->rate_per_unit_adjust ?? 0 ,2) }}"
                                                        @endif
                                                        name="adjust[{{ $bqs->id }}][rate]" onkeyup="calc_bq({{ $bqs->id }})" class="money allrate" style="width:100%">
                                                </td>
                                                <td><input id="adjust_amt{{ $bqs->id }}" 
                                                    @if(empty($bqs->amount_adjust))
                                                        value = "{{ money()->toCommon($bqs->amount ?? 0 ,2) }}"
                                                    @else
                                                        value = "{{ money()->toCommon($bqs->amount_adjust ?? 0 ,2) }}"
                                                    @endif
                                                    name="adjust[{{ $bqs->id }}][amt]" class="allamt" style="width:100%" readonly>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <input name="item[{{ $it->id }}][hashslug]" value="{{ $it->hashslug }}" hidden>
                                            <input name="item[{{ $it->id }}][amt]" id="adjust_amt{{ $it->id }}" class="allamt" value="{{ (!empty($it->amount_adjust) && $it->amount_adjust > 0)? money()->toCommon($it->amount_adjust ?? 0 ,2):money()->toCommon($it->amount ?? 0 ,2) }}" hidden>
                                            @endif
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                            @else
                            <input name="elemen[{{ $bq->id }}][hashslug]" value="{{ $bq->hashslug }}" hidden>
                            <input name="elemen[{{ $bq->id }}][amt]" id="adjust_amt{{ $it->id }}" onChange="getAllVal()" class="allamt" value="{{ (!empty($bq->amount_adjust) && $bq->amount_adjust > 0)? money()->toCommon($bq->amount_adjust ?? 0 ,2):money()->toCommon($bq->amount ?? 0 ,2) }}" hidden>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@include('contract.post.partials.indexes.price.submit', [
    'route_name' => 'api.contract.acquisition.adjustment.update',
    'form' => 'price-form'
])
</form>
@endif