@push('scripts')
    <script>
        $(function () {
            /* DOCUMENT UPLOAD */
            var t_ContractorDoc = $('#tblUpload_ContractorDoc').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_ContractorDoc = 1;

            $('#addRow_ContractorDoc').on( 'click', function () {
                t_ContractorDoc.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile_ContractorDoc'+ counter_ContractorDoc +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile_ContractorDoc'+ counter_ContractorDoc +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile_ContractorDoc" id="uploadFile_ContractorDoc'+ counter_ContractorDoc +'" name="uploadFile_ContractorDoc[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_ContractorDoc +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove_ContractorDoc'+ counter_ContractorDoc +'" class="remove_ContractorDoc btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter_ContractorDoc++;
            } );
            alert('{{ $sub_contract->documents }}');
            {{--@if(!empty($sub_contract) && ($sub_contract->documents->count() > 0))
            @foreach($sub_contract->documents as $doc)
            t_ContractorDoc.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile_ContractorDoc'+ counter_ContractorDoc +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId_ContractorDoc'+ counter_ContractorDoc +'" name="hDocumentId_ContractorDoc[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile_ContractorDoc'+ counter_ContractorDoc +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile_ContractorDoc" id="uploadFile_ContractorDoc'+ counter_ContractorDoc +'" name="uploadFile_ContractorDoc[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_ContractorDoc +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove_ContractorDoc'+ counter_ContractorDoc +'" class="remove_ContractorDoc btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter_ContractorDoc++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow_ContractorDoc').click();
            @endif--}}

            $("#tblUpload_ContractorDoc").on('click','.remove_ContractorDoc',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t_ContractorDoc.row($(this).closest("tr")).remove().draw(false);
                }
            });
            });

            $("#tblUpload_ContractorDoc").on('change','.uploadFile_ContractorDoc',function(){
                var no_ContractorDoc = $(this).data('counter');
                $('#subFile_ContractorDoc' + no_ContractorDoc).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD */
        })
    </script>
@endpush
<div id="uploads">
    <hr>
    <h4>Dokumen</h4>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                <button type="button" id="addRowContractorDoc" href="#" class="btn btn-primary">
                    @icon('fe fe-plus')
                    {{ __('Tambah Dokumen') }}
                </button>
            </div>
            <div class="table-responsive">
                <table id="tblUploadContractorDoc" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th>Muat Naik Dokumen</th>
                        <th width="70px">Hapus</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>

</div>