<div class="row">
    <div class="col-12">
        <div class="float-right">
            <button type="button" id="addRowContractor" href="#" class="btn btn-primary">
                @icon('fe fe-plus')
                {{ __('Tambah Sub Kontraktor') }}
            </button>
        </div>
        <div class="table-responsive">
            <table id="tblContractor" class="table table-sm table-transparent">
                <thead>
                <tr>
                    <th>Syarikat</th>
                    <th width="100px">Harga</th>
                    <th>Keterangan Kerja</th>
                    <th width="70px">Hapus</th>
                </tr>
                </thead>
            </table>
        </div>

    </div>
</div>