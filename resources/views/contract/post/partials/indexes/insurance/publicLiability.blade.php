<div id="publicLiability">
    <div class="row">
    <div class="col-6">
        @include('components.forms.select', [
            'input_label' => 'Nama Pengeluar Insuran',
            'id' => 'public_insurance_name',
            'name' => 'public_insurance_name',
            'options' => $insurances,
        ])
        @include('components.forms.input', [
           'input_label' => __('No. Polisi'),
           'id' => 'public_policy_no',
           'name' => 'public_policy_no',
        ])
        @include('components.forms.input', [
           'input_label' => __('Nilai Polisi (RM)'),
           'id' => 'public_policy_amount',
           'name' => 'public_policy_amount',
           'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Akhir Mengemukakan Polisi'),
            'id' => 'public_proposed_date',
            'name' => 'public_proposed_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Terima Insuran'),
            'id' => 'public_received_date',
            'name' => 'public_received_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mula Sah Laku Polisi'),
            'id' => 'public_start_date',
            'name' => 'public_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Luput Polisi'),
            'id' => 'public_expired_date',
            'name' => 'public_expired_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
    </div>
</div>
    @include('contract.post.partials.indexes.insurance.uploads_publicLiability')
</div>