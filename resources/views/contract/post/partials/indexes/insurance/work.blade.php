<div id="work">
    <div class="row">
    <div class="col-6">
        @include('components.forms.select', [
            'input_label' => 'Nama Pengeluar Insuran',
            'id' => 'work_insurance_name',
            'name' => 'work_insurance_name',
            'options' => $insurances,
        ])
        @include('components.forms.input', [
           'input_label' => __('No. Polisi'),
           'id' => 'work_policy_no',
           'name' => 'work_policy_no',
        ])
        @include('components.forms.input', [
           'input_label' => __('Nilai Polisi (RM)'),
           'id' => 'work_policy_amount',
           'name' => 'work_policy_amount',
           'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Akhir Mengemukakan Polisi'),
            'id' => 'work_proposed_date',
            'name' => 'work_proposed_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Terima Insuran'),
            'id' => 'work_received_date',
            'name' => 'work_received_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mula Sah Laku Polisi'),
            'id' => 'work_start_date',
            'name' => 'work_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Luput Polisi'),
            'id' => 'work_expired_date',
            'name' => 'work_expired_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
    </div>
</div>
    @include('contract.post.partials.indexes.insurance.uploads_work')
</div>