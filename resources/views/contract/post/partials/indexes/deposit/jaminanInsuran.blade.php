<div id="depositJaminanInsuran">
    <div class="row">
        <div class="col-6">
            @include('components.forms.select', [
                'input_label' => 'Nama Pengeluar Insuran',
                'id' => 'deposit_insurance_name',
                'name' => 'deposit_insurance_name',
                'options' => $insurances,
            ])
            @include('components.forms.input', [
                'input_label' => __('No Jaminan Insuran'),
                'id' => 'deposit_insurance_no',
                'name' => 'deposit_insurance_no',
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Bermula Jaminan Insuran'),
                'id' => 'deposit_insurance_start_date',
                'name' => 'deposit_insurance_start_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Akhir Mengemukakan Jaminan Insuran'),
                'id' => 'deposit_insurance_proposed_date',
                'name' => 'deposit_insurance_proposed_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Insuran'),
                'id' => 'deposit_insurance_approval_received_date',
                'name' => 'deposit_insurance_approval_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Insuran'),
                'id' => 'deposit_insurance_received_date',
                'name' => 'deposit_insurance_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
            @include('components.forms.input', [
               'input_label' => __('Nilai Jaminan Insuran (RM)'),
               'id' => 'deposit_insurance_amount',
               'name' => 'deposit_insurance_amount',
               'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Luput Jaminan Insuran'),
                'id' => 'deposit_insurance_expired_date',
                'name' => 'deposit_insurance_expired_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Insuran'),
                'id' => 'deposit_insurance_approval_proposed_date',
                'name' => 'deposit_insurance_approval_proposed_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
    </div>

</div>