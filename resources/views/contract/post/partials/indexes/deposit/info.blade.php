<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('No Rujukan'),
            'id' => 'deposit_reference_no',
            'name' => 'deposit_reference_no',
        ])
        @include('components.forms.input', [
            'input_label' => __('Wang Kos Prima (RM)'),
            'id' => 'deposit_prime_cost',
            'name' => 'deposit_prime_cost',
            'input_classes' => 'money',
            'readonly' => true,
        ])
        @include('components.forms.input', [
             'input_label' => __('Nilai Kerja Pembina (RM)'),
             'id' => 'deposit_amount',
             'name' => 'deposit_amount',
             'readonly' => true,
         ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
             'input_label' => __('Jumlah Wang Pendahuluan (RM)'),
             'id' => 'deposit_total_amount',
             'name' => 'deposit_total_amount',
             'readonly' => true,
         ])
        @include('components.forms.input', [
             'input_label' => __('Nilai Layak Bayaran Pendahuluan (RM)'),
             'id' => 'deposit_qualified_amount',
             'name' => 'deposit_qualified_amount',
             'readonly' => true,
         ])
        @include('components.forms.input', [
             'input_label' => __('Nilai Permohonan Wang Pendahuluan (RM)'),
             'id' => 'deposit_application_amount',
             'name' => 'deposit_application_amount',
         ])
    </div>
</div>