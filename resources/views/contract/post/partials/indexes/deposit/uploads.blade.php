<div id="uploads_deposit">
    <hr>
    <h4>Dokumen</h4>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                <button type="button" id="addRow_deposit" href="#" class="btn btn-primary">
                    @icon('fe fe-plus')
                    {{ __('Tambah Dokumen') }}
                </button>
            </div>
            <div class="table-responsive">
                <table id="tblUpload_deposit" class="table table-sm table-transparent">
                    <thead>
                    <tr>
                        <th>Muat Naik Dokumen</th>
                        <th width="70px">Hapus</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>

</div>