<div id="depositJaminanBank">
    <div class="row">
        <div class="col-6">
            @include('components.forms.select', [
                'input_label' => 'Nama Pengeluar Bank',
                'id' => 'deposit_bank_name',
                'name' => 'deposit_bank_name',
                'options' => $banks,
            ])
            @include('components.forms.input', [
                'input_label' => __('No Jaminan Bank'),
                'id' => 'deposit_bank_no',
                'name' => 'deposit_bank_no',
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Bermula Jaminan Bank'),
                'id' => 'deposit_bank_start_date',
                'name' => 'deposit_bank_start_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Akhir Mengemukakan Jaminan Bank'),
                'id' => 'deposit_bank_proposed_date',
                'name' => 'deposit_bank_proposed_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Bank'),
                'id' => 'deposit_bank_approval_received_date',
                'name' => 'deposit_bank_approval_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Bon Pelaksanaan'),
                'id' => 'deposit_bank_received_date',
                'name' => 'deposit_bank_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
            @include('components.forms.input', [
               'input_label' => __('Nilai Jaminan Bank (RM)'),
               'id' => 'deposit_bank_amount',
               'name' => 'deposit_bank_amount',
               'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Luput Jaminan Bank'),
                'id' => 'deposit_bank_expired_date',
                'name' => 'deposit_bank_expired_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Bank'),
                'id' => 'deposit_bank_approval_proposed_date',
                'name' => 'deposit_bank_approval_proposed_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
    </div>

</div>