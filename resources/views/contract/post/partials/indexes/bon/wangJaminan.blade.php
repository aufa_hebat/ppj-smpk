<div id="wangJaminan">
    <div class="row">
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Surat Permohonan WJP'),
                'id' => 'money_received_at',
                'name' => 'money_received_at',
                'config' => ['format' => config('datetime.display.date')],
            ])
        </div>
        <div class="col-6">
            @include('components.forms.input', [
               'input_label' => __('Nilai Wang Jaminan Pelaksanaan'),
               'id' => 'money_amount',
               'name' => 'money_amount',
               'readonly' => true,
            ])
        </div>
    </div>

</div>