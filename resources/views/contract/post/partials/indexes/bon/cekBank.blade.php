<div id="cekBank">
    <div class="row">
        <div class="col-6">
            @include('components.forms.select', [
                'input_label' => 'Nama Pengeluar Bank',
                'id' => 'cheque_name',
                'name' => 'cheque_name',
                'options' => $banks,
            ])
            @include('components.forms.input', [
               'input_label' => __('No Cek'),
               'id' => 'cheque_no',
               'name' => 'cheque_no',
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Resit Dari Bahagian Pengurusan Kewangan'),
                'id' => 'cheque_received_date',
                'name' => 'cheque_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('No Resit Dari Bahagian Pengurusan Kewangan (BPK)'),
                'id' => 'cheque_resit_no',
                'name' => 'cheque_resit_no',
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Hantar Cek Ke Bahagian Pengurusan Kewangan (BPK)'),
                'id' => 'cheque_submitted_date',
                'name' => 'cheque_submitted_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
        </div>
    </div>

</div>