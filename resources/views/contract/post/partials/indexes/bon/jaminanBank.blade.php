<div id="jaminanBank">
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('No Jaminan Bank'),
                'id' => 'bank_no',
                'name' => 'bank_no',
            ])
            @include('components.forms.select', [
                'input_label' => 'Nama Pengeluar Bank',
                'id' => 'bank_name',
                'name' => 'bank_name',
                'options' => $banks,
            ])
            @include('components.forms.input', [
               'input_label' => __('Nilai Jaminan Bank (RM)'),
               'id' => 'bank_amount',
               'name' => 'bank_amount',
               'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Akhir Mengemukakan Jaminan Bank'),
                'id' => 'bank_proposed_date',
                'name' => 'bank_proposed_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
        </div>
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Bon Pelaksanaan'),
                'id' => 'bank_received_date',
                'name' => 'bank_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Bermula Jaminan Bank'),
                'id' => 'bank_start_date',
                'name' => 'bank_start_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Luput Jaminan Bank'),
                'id' => 'bank_expired_date',
                'name' => 'bank_expired_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
        </div>
    </div>

</div>