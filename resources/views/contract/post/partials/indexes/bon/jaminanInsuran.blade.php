<div id="jaminanInsuran">
    <div class="row">
        <div class="col-6">
            @include('components.forms.input', [
                'input_label' => __('No Jaminan Insuran'),
                'id' => 'insurance_no',
                'name' => 'insurance_no',
            ])
            @include('components.forms.select', [
                'input_label' => 'Nama Pengeluar Insuran',
                'id' => 'insurance_name',
                'name' => 'insurance_name',
                'options' => $insurances,
            ])
            @include('components.forms.input', [
               'input_label' => __('Nilai Jaminan Insuran (RM)'),
               'id' => 'insurance_amount',
               'name' => 'insurance_amount',
               'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Akhir Mengemukakan Jaminan Insuran'),
                'id' => 'insurance_proposed_date',
                'name' => 'insurance_proposed_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
        </div>
        <div class="col-6">
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Terima Insuran'),
                'id' => 'insurance_received_date',
                'name' => 'insurance_received_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ]
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Bermula Jaminan Insuran'),
                'id' => 'insurance_start_date',
                'name' => 'insurance_start_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Luput Jaminan Insuran'),
                'id' => 'insurance_expired_date',
                'name' => 'insurance_expired_date',
                'config' => [
                    'format' => config('datetime.display.date'),
                ],
                'readonly' => true,
            ])
        </div>
    </div>

</div>