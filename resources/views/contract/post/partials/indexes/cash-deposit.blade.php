@push('scripts')
    @include('components.forms.assets.datetimepicker')
    @include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script>
        jQuery(document).ready(function($) {

            $('#deposit_send').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#deposit_sentStatus').val(),
                    acquisition_id : $('#deposit_acquisition_id').val(),
                    requested_by : $('#deposit_requested_by').val(),
                    progress : $('#deposit_progress').val(),
                    task_by : $('#deposit_task_by').val(),
                    law_task_by : $('#deposit_law_task_by').val(),
                    approved_by : $('#deposit_approved_by').val(),
                    created_by : $('#deposit_created_by').val(),
                    approved_at : $('#deposit_approved_at').val(),
                    type : $('#deposits_type').val(),
                    document_contract_type : $('#deposit_document_contract_type').val(),
                    status : $('#deposit_status').val(),
                    department : $('#deposit_department').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#deposit_send').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('.money').mask('000,000,000.00', {reverse: true});

            $('#info').show();
            $('#bon').hide();
            $('#depositJaminanBank').hide();
            $('#depositJaminanInsuran').hide();
            $('#uploads_deposit').hide();
            $("#deposit_div").hide();

            $("#deposit_cbBox").prop( "checked", false );


            @if(!empty($deposit))

                $("#deposit_cbBox").prop( "checked", true );
                $("#deposit_div").show();
                $('#uploads_deposit').show();
                @if( $deposit->bon_type == 1 )
                    $('#depositJaminanBank').show();
                @elseif($deposit->bon_type == 2)
                    $('#depositJaminanInsuran').show();
                @endif

                // onload fields
                $('#deposit_reference_no').val('{{ $deposit->reference_no }}');
                $("#deposit_bon_type").val('{{ $deposit->bon_type }}');

                $('#deposit_application_amount').val('{{ $deposit->application_amount ? money()->toCommon($deposit->application_amount) : null }}');

                $('#deposit_bank_name').val('{{ $deposit->bank_name }}');
                $('#deposit_bank_no').val('{{ $deposit->bank_no }}');
                $('#deposit_bank_approval_received_date').val('{{ $deposit->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_approval_received_date)->format('d/m/Y'):null }}');
                $('#deposit_bank_received_date').val('{{ $deposit->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_received_date)->format('d/m/Y'):null }}');
                $('#deposit_bank_approval_proposed_date').val('{{ $deposit->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_approval_proposed_date)->format('d/m/Y'):null }}');
                $('#deposit_bank_amount').val('{{ $deposit->total_amount ? money()->toHuman($deposit->total_amount) : null }}');

                $('#deposit_insurance_name').val('{{ $deposit->insurance_name }}');
                $('#deposit_insurance_no').val('{{ $deposit->insurance_no }}');
                $('#deposit_insurance_approval_received_date').val('{{ $deposit->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_approval_received_date)->format('d/m/Y'):null }}');
                $('#deposit_insurance_received_date').val('{{ $deposit->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_received_date)->format('d/m/Y'):null }}');
                $('#deposit_insurance_approval_proposed_date').val('{{ $deposit->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_approval_proposed_date)->format('d/m/Y'):null }}');
                $('#deposit_insurance_amount').val('{{ $deposit->total_amount ? money()->toHuman($deposit->total_amount) : null }}');
            @else
                $('#deposit_reference_no').val('{{ $sst->acquisition->approval->file_reference }}');
            @endif

            @if(!empty($sst))

            $('#deposit_bank_start_date').val('{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_expired_date').val('{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_proposed_date').val('{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}');

            $('#deposit_insurance_start_date').val('{{ $sst->start_working_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_expired_date').val('{{ $expiry_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$expiry_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_proposed_date').val('{{ $proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$proposed_date)->format('d/m/Y'):null }}');
            @endif

            $('#deposit_prime_cost').val('{{ $deposit_prime_cost ? money()->toHuman($deposit_prime_cost):'RM 0.00' }}');
            $('#deposit_amount').val('{{ $deposit_amount ? money()->toHuman($deposit_amount):'RM 0.00' }}');
            $('#deposit_total_amount').val('{{ $deposit_total_amount ? money()->toHuman($deposit_total_amount):'RM 0.00' }}');
            $('#deposit_qualified_amount').val('{{ $deposit_qualified_amount ? money()->toHuman($deposit_qualified_amount):'RM 0.00' }}');


            $('input[type=radio][name=deposit_type]').change(function () {

                if(this.value == '1'){
                    $('#info').show();
                    $('#bon').hide();
                }
                else if (this.value == '2'){
                    $('#info').hide();
                    $('#bon').show();
                }
            });

            $('#deposit_bon_type').change(function () {

                //clear all field
                $('#deposit_bank_name').val(null);
                $('#deposit_bank_no').val(null);
                $('#deposit_bank_approval_proposed_date').val(null);
                $('#deposit_bank_approval_received_date').val(null);
                $('#deposit_bank_received_date').val(null);

                $('#deposit_insurance_name').val(null);
                $('#deposit_insurance_no').val(null);
                $('#deposit_insurance_approval_proposed_date').val(null);
                $('#deposit_insurance_approval_received_date').val(null);
                $('#deposit_insurance_received_date').val(null);

                if(this.value == '1'){
                    $('#depositJaminanBank').show();
                    $('#depositJaminanInsuran').hide();
                    $('#uploads_deposit').show();
                }
                else if (this.value == '2'){
                    $('#depositJaminanBank').hide();
                    $('#depositJaminanInsuran').show();
                    $('#uploads_deposit').show();
                }
                else{
                    $('#depositJaminanBank').hide();
                    $('#depositJaminanInsuran').hide();
                    $('#uploads_deposit').hide();
                }
            });

            /* DOCUMENT UPLOAD _deposit */
            var t_deposit = $('#tblUpload_deposit').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_deposit = 1;

            $('#addRow_deposit').on( 'click', function () {
                t_deposit.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input id="subFile_deposit'+ counter_deposit +'" type="text"  class="form-control" readonly>\n' +
                    '       <label class="input-group-text" for="uploadFile_deposit'+ counter_deposit +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFile_deposit" id="uploadFile_deposit'+ counter_deposit +'" name="uploadFile_deposit[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_deposit +'">\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove_deposit'+ counter_deposit +'" class="remove_deposit btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter_deposit++;
            } );

            @if(!empty($deposit) && (optional($deposit->documents)->count() > 0))
            $('#uploads_deposit').show();
            @foreach($deposit->documents as $doc)
            t_deposit.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input type="text" id="subFile_deposit'+ counter_deposit +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                '       <input type="hidden" id="hDocumentId_deposit'+ counter_deposit +'" name="hDocumentId_deposit[]" value="{{ $doc->id }}">\n' +
                '       <label class="input-group-text" for="uploadFile_deposit'+ counter_deposit +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFile_deposit" id="uploadFile_deposit'+ counter_deposit +'" name="uploadFile_deposit[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counter_deposit +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove_deposit'+ counter_deposit +'" class="remove_deposit btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter_deposit++;
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRow_deposit').click();
            @endif

            $("#tblUpload_deposit").on('click','.remove_deposit',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        t_deposit.row($(this).closest("tr")).remove().draw(false);
                    }
                });

            });

            $("#tblUpload_deposit").on('change','.uploadFile_deposit',function(){
                var no_deposit = $(this).data('counter');
                $('#subFile_deposit' + no_deposit).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD */

            $("#deposit_cbBox").on('change', function(){

                if(this.checked){
                    $("#deposit_div").show();
                }
                else{
                    @if(!empty($deposit))
                        swal({
                            title: '{!! __('Amaran') !!}',
                            text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: '{!! __('Ya') !!}',
                            cancelButtonText: '{!! __('Batal') !!}'
                        }).then((result) => {
                            if (result.value) {
                                axios.delete(route('api.contract.acquisition.deposit.destroy',{{ $deposit->id }}));
                                $("#deposit_div").hide();
                            }else{
                                $("#deposit_cbBox").prop( "checked", true );
                            }
                        });
                    
                    @else
                        $("#deposit_div").hide();
                    @endif
                    
                }
            });

            $(document).on('click', '#deposit-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);

                console.log(route(route_name, {id:id}));
                axios.post(route(route_name, id), data).then(response => {
                    swal('{!! __('Wang Pendahuluan') !!}', response.data.message, 'success');
                    location.reload();
            }).catch((error)=>{
                    console.log(error.response.data);
            });
            });
        });


    </script>
@endpush

<table width="100%">
    <tr>
        <td><h4>Wang Pendahuluan</h4></td>
        <td width="10px">
            @if(!$deposit_has_reviewed)
            <label class="custom-switch">
                    <input type="checkbox" class="custom-switch-input" id="deposit_cbBox" name="deposit_cbBox" value="0">
            <span class="custom-switch-indicator"></span>
            <span class="custom-switch-description">Aktif</span>
            </label>
            @endif
        </td>
    </tr>
</table>

<br>
<div class="row" id="deposit_div">
    <div class="col-12">
        
        <form id="deposit-form" files = "true" enctype="multipart/form-data" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <div class="selectgroup w-100">
                    <label class="selectgroup-item">
                        <input type="radio" name="deposit_type" value="1" class="selectgroup-input" checked>
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Wang Pendahuluan') }}</span>
                    </label>
                    <label class="selectgroup-item">
                        <input type="radio" name="deposit_type" value="2" class="selectgroup-input">
                        <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Bon') }}</span>
                    </label>
                </div>
            </div>

            <div id="info">
                @include('contract.post.partials.indexes.deposit.info')
            </div>

            <div id="bon">
                <div class="form-group row">
                    <label for="deposit_bon_type"
                           class="col col-form-label">{{ __('Jenis Bon Pelaksanaan') }}
                    </label>
                    <div class="col">
                        <select id="deposit_bon_type" name="deposit_bon_type" class="form-control w-100">
                            <option value="">{{ __('Sila Pilih') }}</option>
                            @foreach($bon_types as $index => $type)
                                @if($index == 2)
                                    @break
                                @endif
                                <option value="{{ $type->code }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                @include('contract.post.partials.indexes.deposit.jaminanBank')
                @include('contract.post.partials.indexes.deposit.jaminanInsuran')
                @include('contract.post.partials.indexes.deposit.uploads')
            </div>

        </form>
        <div class="btn-group float-right">
            @role('penyedia')
                @if(!empty($deposit) && $deposit->sst->user_id == user()->id)
                    @if(empty($reviewwang) || empty($reviewwang->status))
                        <form method="POST" action="{{ route('acquisition.review.store') }}">
                            @csrf
                            <input id="deposit_sentStatus" name="sentStatus" type="hidden" value="1" />
                            <input type="text" id="deposit_acquisition_id" name="acquisition_id" value="@if(!empty($deposit)){{ $deposit->sst->acquisition_id }}@endif" hidden>

                            <input type="text" id="deposit_requested_by" name="requested_by" value="{{ user()->id }}" hidden>

                            <input type="text" id="deposit_approved_by" name="approved_by" value="{{ user()->supervisor->id }}" hidden>

                            <input type="text" id="deposit_created_by" name="created_by" value="{{user()->id}}" hidden>

                            <input type="text" id="deposit_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>

                            @if(!empty($review_previous))
                                <input type="hidden" id="deposit_task_by" name="task_by" value="{{$review_previous->task_by}}">
                                <input type="hidden" id="deposit_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}">
                                <input type="text" id="deposits_type" name="type" value="Dokumen Kontrak" hidden>
                                <input type="text" id="deposit_document_contract_type" name="document_contract_type" value="Wang Pendahuluan" hidden>
                            @endif
                            
                            <button type="button" id="deposit_send" class="btn btn-default float-middle border-default">
                                @icon('fe fe-send') {{ __('Teratur') }}
                            </button>
                        </form>
                    @endif
                @endif
            @endrole
        </div>
        @include('contract.post.partials.indexes.deposit.submit', [
            'route_name' => 'api.contract.acquisition.deposit.update',
            'form' => 'deposit-form'
        ])
    </div>
</div>