@push('scripts')

    <script>
        jQuery(document).ready(function($) {

            /* ADD SUB CON */
            var t = $('#tblContractor').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter = 1;

            $('#addRowContractor').on( 'click', function () {
                t.row.add( [
                    '<div class="form-group">\n' +
                        '<select id="company'+ counter +'" name="company'+ counter +'" class="form-control w-100">\n' +
                        '   <option value="">{{ __('Sila Pilih') }}</option>\n' +
                        '   @foreach($companies as $index => $company)\n' +
                        '       <option value="{{ $company->id }}">{{ $company->company_name }}</option>\n' +
                        '   @endforeach\n' +
                        '</select> \n'+
                    '</div>',
                    '<div class="form-group"><input type="text" id="amount'+ counter +'" name="amount'+ counter +'" class="form-control money"></div>',
                    '<div class="form-group"><input type="text" id="description'+ counter +'" name="description'+ counter +'" class="form-control"></div>',
                    '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counter++;
                $('.money').mask('000,000,000.00', {reverse: true});
            } );

            @if($sub_contract->count() > 0)
            @foreach($sub_contract as $sub)
            t.row.add( [
                '<div class="form-group">\n' +
                '<select id="company'+ counter +'" name="company'+ counter +'" class="form-control w-100">\n' +
                '   <option value="">{{ __('Sila Pilih') }}</option>\n' +
                '   @foreach($companies as $index => $company)\n' +
                '       <option value="{{ $company->id }}" @if($company->id == $sub->company_id)selected="true"@endif >{{ $company->company_name }}</option>\n' +
                '   @endforeach\n' +
                '</select> \n'+
                '</div>',
                '<div class="form-group"><input type="text" id="amount'+ counter +'" name="amount'+ counter +'" class="form-control money" value="{{ $sub->amount ? money()->toCommon($sub->amount):null }}"></div>',
                '<div class="form-group"><input type="text" id="description'+ counter +'" name="description'+ counter +'" class="form-control" value="{{ $sub->description }}"></div>',
                '<div class="form-group"><button type="button" id="remove'+ counter +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counter++;
            $('.money').mask('000,000,000.00', {reverse: true});
            @endforeach
            @else
            // Automatically add a first row of data
            $('#addRowContractor').click();
            @endif

            $("#tblContractor").on('click','.remove',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                    t.row($(this).closest("tr")).remove().draw(false);
                }
            });
            });
            /* END SUB CON */

            $(document).on('click', '#sub-contract-submit', function(event) {
                event.preventDefault();

                var id = '{{ $sst->hashslug }}';
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');

                var form = document.forms[form_id];
                var data = new FormData(form);
                data.append('counter', counter-1);

                console.log(route(route_name, {id:id}));
                // Display the key/value pairs
                for (var pair of data.entries()) {
                    console.log(pair[0]+ '= ' + pair[1]);
                }

                axios.post(route(route_name, id), data).then(response => {
                    swal('{!! __('Sub Kontraktor') !!}', response.data.message, 'success');
                    location.reload();
                }).catch((error)=>{
                        console.log(error.response.data);
                });
            });
        });


    </script>
@endpush

<h4>Sub Kontraktor</h4>
<br>
<div class="row">
    <div class="col-12">
        <form id="sub-contract-form" method="post">
            @csrf
            @method('put')
            @include('contract.post.partials.indexes.sub_contract.info')
        </form>
        @include('contract.post.partials.indexes.sub_contract.submit', [
            'route_name' => 'api.contract.acquisition.sub_contract.update',
            'form' => 'sub-contract-form'
        ])
    </div>
</div>