<div class="row">
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mati Setem'),
            'id' => 'document_revenue_stamp_date',
            'name' => 'document_revenue_stamp_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.input', [
             'input_label' => __('Tempoh Penyediaan Dokumen Kontrak'),
             'id' => 'document_preparation_period',
             'name' => 'document_preparation_period',
             'readonly' => true,
         ])
         @include('components.forms.input', [
             'input_label' => __('Tempoh Kelewatan'),
             'id' => 'document_late_days',
             'name' => 'document_late_days',
             'readonly' => true,
         ])
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Terima Dokumen'),
            'id' => 'document_submitted_date',
            'name' => 'document_submitted_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Akhir Penyediaan Dokument Kontrak'),
            'id' => 'document_proposed_date',
            'name' => 'document_proposed_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'readonly' => true,
        ])
    </div>
</div>