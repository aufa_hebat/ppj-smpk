<div class="text-center">
	<div class="item-action dropdown">
	  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
	  	<i class="fe fe-more-vertical"></i>
	  </a>
	  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
	  	style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
	  	{{ $prepend_action or '' }}
	    <a class="dropdown-item show-action-btn" style="cursor: pointer;"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			<i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
		</a>
		@role('penyedia||administrator||developer||urusetia')
		<a class="dropdown-item edit-action-btn" style="cursor: pointer;"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
		</a>
		@endrole
		{{ $append_action or '' }}
		  <a class="dropdown-item cpc-action-btn" style="cursor: pointer;"
			 data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			 <i class="fe fe-file-text"></i> {{ __('Sijil Siap Kerja') }}
		  </a>
		  <a class="dropdown-item cmgd-action-btn" style="cursor: pointer;"
			 data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			 <i class="fe fe-file-plus"></i> {{ __('Sijil Siap Membaiki Kecacatan') }}
		  </a>
		  <a class="dropdown-item cnc-action-btn" style="cursor: pointer;"
			 data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			  <i class="fe fe-file"></i> {{ __('Sijil Tidak Siap Kerja') }}
		  </a>
		  <a class="dropdown-item cpo-action-btn" style="cursor: pointer;"
			 data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			 <i class="fe fe-file-minus"></i> {{ __('Sijil Siap Keja Separa') }}
		  </a>
			<a class="dropdown-item terminate-action-btn" style="cursor: pointer;"
				data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
				<i class="fas fa-plus"></i> {{ __('Tamat Kontrak') }}
			</a>			
	  </div>
	</div>
</div>