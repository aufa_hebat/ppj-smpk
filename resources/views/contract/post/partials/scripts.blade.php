@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.show-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.show', id));
		});
		$(document).on('click', '.edit-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.edit', id));
		});
        $(document).on('click', '.cpc-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('hashslug');
            redirect(route('contract.post.cpc.edit', id));
        });
        $(document).on('click', '.cmgd-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('hashslug');
            redirect(route('contract.post.cmgd.edit', id));
        });
        $(document).on('click', '.cnc-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('hashslug');
            redirect(route('contract.post.cnc.edit', id));
        });
        $(document).on('click', '.cpo-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('hashslug');
            redirect(route('contract.post.cpo.edit', id));
        });
		$(document).on('click', '.terminate-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('termination.termination.edit', id));
		});
	});
</script>
@endpush