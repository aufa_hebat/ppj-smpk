@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    <script>

        jQuery(document).ready(function($) {
            $('#acquisition_approval_title').val("{{(!empty($sst->acquisition->approval->reference)) ? $sst->acquisition->approval->reference:''}} : {{$sst->acquisition->title}}");
            $('#acquisition_approval_id').val('{{ $sst->acquisition->acquisition_approval_id }}');
            $('#acquisition_category_id').val('{{ $sst->acquisition->acquisition_category_id }}').prop('disabled',true);
            $('#reference').val('{{ $sst->acquisition->reference }}').prop('readonly',true);

            $('#document_price').val('{{ money()->toCommon($sst->acquisition->document_price ?? "0" , 2) }}').prop('readonly',true);
            $('#gst').val('{{ $sst->acquisition->gst }}').prop('readonly',true);
            $('#total_document_price').val('{{ money()->toCommon($sst->acquisition->total_document_price ?? "0" , 2) }}').prop('readonly',true);
            
            @if(!empty($sst->acquisition->advertised_at))
            $('#advertised_at').val('{{ $sst->acquisition->advertised_at->format("d/m/Y") }}').prop('readonly',true);
            @endif

            @if(!empty($sst->acquisition->extended_closed_sale_at))
            $('#addenda_ext').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->acquisition->extended_closed_sale_at)->format('d/m/Y')  }}');
            @endif
            @if(!empty($sst->acquisition->closed_at))
            $('#addenda_close').val('{{ $sst->acquisition->closed_at->format("d/m/Y") }}').prop('readonly',true);
            @endif            

            @if(!empty($sst->acquisition->start_sale_at))
            $('#start_sale_at').val('{{ $sst->acquisition->start_sale_at->format("d/m/Y") }}').prop('readonly',true);
            @endif
            @if(!empty($sst->acquisition->closed_at))
            $('#closed_at').val('{{ $sst->acquisition->closed_at->format("d/m/Y") }}').prop('readonly',true);
            @endif
            @if(!empty($sst->acquisition->closed_sale_at))
            $('#closed_sale_at').val('{{ $sst->acquisition->closed_sale_at->format("d/m/Y") }}').prop('readonly',true);
            @endif
            @if(!empty($sst->acquisition->briefing_at))
            $('#briefing_at').val('{{ $sst->acquisition->briefing_at->format("d/m/Y g:i A") }}').prop('readonly',true);
            @endif
            @if(!empty($sst->acquisition->visit_date_time))
            $('#visit_date_time').val('{{ $sst->acquisition->visit_date_time->format("d/m/Y g:i A") }}').prop('readonly',true);
            @endif
            
            $('#experience_value').val('{{money()->toCommon($sst->acquisition->experience_value ?? "0" , 2)}}').prop('readonly',true);
            $('#experience_duration').val('{{$sst->acquisition->experience_duration}}').prop('readonly',true);

            $('#briefing_status').val('{{ $sst->acquisition->briefing_status }}').prop('readonly',true);
            $('#briefing_location').val('{{ $sst->acquisition->briefing_location }}').prop('readonly',true);
            $('#visit_status').val('{{ $sst->acquisition->visit_status }}').prop('readonly',true);
            $('#visit_location').val('{{ $sst->acquisition->visit_location }}').prop('readonly',true);
            $('#document_sale_location').val('{{ $sst->acquisition->document_sale_location }}').prop('readonly',true);
            
            $(".experience").prop('readonly', true);
            $(".briefing").prop('readonly', true);
            $(".visit").prop('readonly', true);
            $(".viewornot").hide();
        });
    </script>
@endpush
@section('content')
@component('components.pages.title-sub')
    @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk: </span>{{ $sst->acquisition->title }}
        <br>
        <span class="font-weight-bold">No. Perolehan: </span>{{ $sst->acquisition->reference }}
    @endslot
@endcomponent
<div class="row">
    {{--  <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#document-details" role="tab" aria-controls="document-details" aria-selected="false">
                    @icon('fe fe-search')&nbsp;{{ __('Perinci') }}
                </a>
            </li>
            
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-briefing" role="tab" aria-controls="document-briefing" aria-selected="false">
                    @icon('fe fe-message-square')&nbsp;{{ __('Taklimat / Lawatan Tapak / Jualan') }}
                </a>
            </li>
            
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-quantity" role="tab" aria-controls="document-quantity" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Senarai Kuantiti') }}
                </a>
            </li>

            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-addenda" role="tab" aria-controls="document-addenda" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Addenda') }}
                </a>
            </li>    
            
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-sst" role="tab" aria-controls="document-sst" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('SST') }}
                </a>
            </li>  

            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#document-contract" role="tab" aria-controls="document-contract" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Dokumen Kontrak') }}
                </a>
            </li>            
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                         @component('components.tab.content', ['id' => 'document-details', 'active' => true])
                            @slot('content')
                                @include('contract.post.contract-completed.partials.shows.pra.details', ['acquisition' => $sst->acquisition])
                            @endslot
                        @endcomponent
                        
                        @component('components.tab.content', ['id' => 'document-briefing'])
                            @slot('content')
                                @include('contract.post.contract-completed.partials.shows.pra.taklimat-ltapak-jualan', ['acquisition' => $sst->acquisition])
                            @endslot
                        @endcomponent
                        
                        @component('components.tab.content', ['id' => 'document-quantity'])
                            @slot('content')
                                @include('contract.post.contract-completed.partials.shows.pra.quantity', ['acquisition' => $sst->acquisition])
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-addenda'])
                            @slot('content')
                                @include('contract.pre.document.partials.forms.addenda', ['acquisition' => $sst->acquisition])
                            @endslot
                        @endcomponent                        

                        @if(!empty($sst->acquisition->sales))
                            @component('components.tab.content', ['id' => 'document-sales'])
                                @slot('content')
                                    @include('contract.pre.document.partials.forms.sales', ['acquisition' => $sst->acquisition])
                                @endslot
                            @endcomponent
                        @endif

                        @component('components.tab.content', ['id' => 'document-sst'])
                            @slot('content')
                                @include('contract.post.contract-completed.partials.shows.pasca.sst', ['sst' => $sst])
                            @endslot
                        @endcomponent   

                        @component('components.tab.content', ['id' => 'document-contract'])
                            @slot('content')
                                @include('contract.post.contract-completed.partials.shows.pasca.document-contract', ['sst' => $sst])
                            @endslot
                        @endcomponent 

                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('contract.post.contract-completed.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>

                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>  --}}
    <div class="col-12">
        @component('components.card')
            @slot('card_body')
                <table class="table table-bordered" width="90%">
                    <thead>
                        <tr>
                            <td width="10%">No.</td>
                            <td width="80%">Modul</td>
                            <td width="10%">Butiran</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>Surat Setuju Terima</td>
                            <td><a href="{{ route('acceptance-letter.show', $appointed->hashslug) }}">@icon('fe fe-eye')</a></td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Dokumen Kontrak</td>
                            <td><a href="{{ route('contract.post.show', $sst->hashslug) }}">@icon('fe fe-eye')</a></td>
                        </tr>   
                        <tr>
                            <td>3.</td>
                            <td>Sijil Bayaran Interim</td>
                        </tr> 
                        <tr>
                            <td>4.</td>
                            <td>Perubahan Kerja</td>
                        </tr> 
                        <tr>
                            <td>5.</td>
                            <td>Perakuan Kelambatan Dan Lanjutan Kontrak</td>
                        </tr>  
                        <tr>
                            <td>6.</td>
                            <td>Pelepasan Bon</td>
                        </tr>                         
                        <tr>
                            <td>7.</td>
                            <td>Bayaran Muktamad</td>
                        </tr> 
                                                                                                                                       
                    </tbody>
                </table>
            @endslot
            @slot('card_footer')
                <div class="btn-group float-right">
                    <a href="{{ route('contract.post.contract-completed.index') }}" 
                            class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                    </a>

                </div>
            @endslot
        @endcomponent
    </div>
</div>
@endsection