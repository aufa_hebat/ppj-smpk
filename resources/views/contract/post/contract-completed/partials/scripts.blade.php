@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {

        $(document).on('click', '.print-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('sofaRpt', {hashslug:id}));
		});

        $(document).on('click', '.show-acquisition-action-btn', function(event) {
			event.preventDefault();
			var id = $(this).data('hashslug');
			redirect(route('contract.post.contract-completed.showPerolehan', id));
		});				
    });
	
</script>
@endpush
