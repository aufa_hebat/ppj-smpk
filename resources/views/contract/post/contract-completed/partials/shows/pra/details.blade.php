<h5>Perinci</h5>
<div class="btn-group float-right viewornotprint">
    <a href="{{ route('acq_notice',['hashslug' => $acquisition->hashslug ?? 0,'np_dept' => $np_dept->name ?? '', 'np_honor' => $np_dept->honourary ?? '']) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Notis') }}
    </a>
    <a href="{{ route('acq_bq',['hashslug' => $acquisition->hashslug ?? 0]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Kuantiti') }}
    </a>
</div>
@include('components.forms.textarea', [
    'input_label' => __('Tajuk Kelulusan Perolehan'),
    'id' => 'acquisition_approval_title',
    'name' => 'acquisition_approval_title',
    'readonly' => true,
])
@include('components.forms.select', [
    'input_label' => __('Kategori Perolehan'),
    'options' => $acq_cat,
    'id' => 'acquisition_category_id',
    'name' => 'acquisition_category_id',
])
@include('components.forms.input', [
    'input_label' => __('No Perolehan'),
    'id' => 'reference',
    'name' => 'reference',
    'readonly' => true,
])
<br/><br/>
<h5>Tarikh dan Masa</h5>
<div class="row">
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Notis'),
            'id' => 'advertised_at',
            'name' => 'advertised_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Tutup Perolehan'),
            'id' => 'closed_at',
            'name' => 'closed_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])     
    </div>
    <div class="col-6">
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Mula Jualan'),
            'id' => 'start_sale_at',
            'name' => 'start_sale_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
        @include('components.forms.datetimepicker', [
            'input_label' => __('Tarikh Tutup Jualan'),
            'id' => 'closed_sale_at',
            'name' => 'closed_sale_at',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
        ])
    </div>
</div>
<br/><br/>
<h5>Harga</h5>
<div class="row">
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' 	=> __('Harga Dokumen (RM)'),
			'id' 			=> 'document_price',
			'name' 			=> 'document_price',
            'type' 			=> 'text',
			'maxlength'		=> '8',
            'onkeyup' 		=> 'calculate()',
			'input_classes'	=> 'money',
		])
	</div>
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => __('GST%'),
			'id' => 'gst',
			'name' => 'gst',
                        'type' => 'number',
                        'step' => '0',
                        'onkeyup' => 'calculate()',
		])
	</div>
    <input type="number" class="form-control input-lg" step="0.01"  id="gst_price" name="gst_price" hidden>
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => __('Jumlah Harga Dokumen(RM)'),
			'id' => 'total_document_price',
			'name' => 'total_document_price',
            'type' => 'text',
            'step' => '0.01',
            'onkeyup' => 'calculate()',
            'readonly' => 'readonly',
			'input_classes'	=> 'money',
		])
	</div>
</div>
<br/><br/>
<h5>Lokasi</h5>
@include('components.forms.input', [
    'input_label' => __('Tempat Jualan'),
    'id' => 'document_sale_location',
    'name' => 'document_sale_location',
])