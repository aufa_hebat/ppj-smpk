<h5>Addenda</h5>

<div class="row">
    <div class="col-md-6">
        @include('components.forms.input', [
            'input_label' => __('Tarikh Tutup Perolehan'),
            'id' => 'addenda_close',
            'name' => 'addenda_close',
            'readonly' => 'readonly',
        ])
    </div>
    <div class="col-md-6">
        @include('components.forms.input', [
            'input_label' => __('Tarikh Tutup Lanjutan (jika berkenaan)'),
            'id' => 'addenda_ext',
            'name' => 'addenda_ext',
            'readonly' => 'readonly',
        ])
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table">
            <tr>
                <th width="15%" ><b>No. Addenda</b></th>
                <th width="65%"><b>Perkara</b></th>
                <th width="20%"><b>Lampiran</b></th>
            </tr>

            @if(!empty($addendas))
                @foreach($addendas as $aden)
                    @foreach($aden->items as $per)
                        <tr>
                            <td><input type="text" id="exist_addendaID" name="exist[{{$per->id}}][addendaID]" value="{{$aden->id}}" hidden>{{$aden->no}}</td>
                            <td><input type="text" id="exist_perkaraID" name="exist[{{$per->id}}][perkaraID]" value="{{$per->id}}" hidden>{{$per->description}}</td>
                            <td><input type="text" id="exist_attachmentID" name="exist[{{$per->id}}][attachmentID]" value="{{$per->id}}" hidden>{{$per->attachment}}</td>
                        </tr>
                    @endforeach
                @endforeach
            @else
                <tr>
                    <td colspan="3">Tiada Data</td>
                </tr>
            @endif
        </table>
    </div>
</div>
