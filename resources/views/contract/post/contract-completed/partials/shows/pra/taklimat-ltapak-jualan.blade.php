<div class="btn-group float-right">
    <a href="{{ route('sale_list',['hashslug' => $acquisition->hashslug]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Jualan') }}
    </a>
    <a href="{{ route('sale_folder',['hashslug' => $acquisition->hashslug]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Folder Jualan') }}
    </a>
    <a href="{{ route('sale_folder_verify',['hashslug' => $acquisition->hashslug]) }}" 
        class="btn btn-success border-success" target="_blank">
        @icon('fe fe-printer'){{ __(' Senarai Semak') }}
    </a>
</div>

<h5>Taklimat</h5>
<div class="float-right viewornot">
    @include('components.forms.switch', [
        'id' => 'briefing_status', 
        'name' => 'briefing_status', 
        'label' => 'Taklimat',
        'checked' => old('briefing_status', (!empty($acquisition))? $acquisition->briefing_status: false)
    ])
</div>
    
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Dan Masa Taklimat'),
    'id' => 'briefing_at',
    'name' => 'briefing_at',
    'input_classes' => 'briefing',
    'config' => [
        'format' => config('datetime.display.date_time'),
    ]
])
    
@include('components.forms.input', [
    'input_label' => __('Tempat Taklimat'),
    'id' => 'briefing_location',
    'name' => 'briefing_location',
    'input_classes' => 'briefing',
])


@if(!empty($sst->acquisition->briefings))
    <br/><br/>
    <div class="row">
        <div class="col-12">
            <h5>Syarikat Yang Hadir Taklimat</h5>
            <table class="table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nama Syarikat</th>
                        <th>Status Hadir</th>
                        <th>Nama Wakil/Pemilik</th>
                        <th>No. Kad Pengenalan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($acquisition->briefings as $key => $briefing)
                        @if($briefing->attendant_status != 3)
                            <tr>
                                <td>{{$briefing->company->company_name}}</td>
                                <td>
                                    @if($briefing->attendant_status == 1)
                                        Pemilik
                                    @elseif($briefing->attendant_status == 2)
                                        Wakil
                                    @endif
                                </td>
                                <td>
                                    @if($briefing->attendant_status == 1)
                                        {{ $briefing->owner->name }}
                                    @elseif($briefing->attendant_status == 2)
                                        {{ $briefing->agent->name }}
                                    @endif
                                </td>
                                <td>
                                    @if($briefing->attendant_status == 1)
                                        {{ $briefing->owner->ic_number }}
                                    @elseif($briefing->attendant_status == 2)
                                        {{ $briefing->agent->ic_number }}
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif  

<br/><br/>
<h5>Lawatan Tapak</h5>
<div class="float-right viewornot">
    @include('components.forms.switch', [
        'id' => 'visit_status', 
        'name' => 'visit_status', 
        'label' => 'Lawatan Tapak',
        'checked' => old('visit_status', (!empty($acquisition))? $acquisition->visit_status:false)
    ])
</div>
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Dan Masa Lawatan Tapak'),
    'id' => 'visit_date_time',
    'name' => 'visit_date_time',
    'input_classes' => 'visit',
    'config' => [
        'format' => config('datetime.display.date_time'),
    ]
])
@include('components.forms.input', [
    'input_label' => __('Tempat Lawatan Tapak'),
    'id' => 'visit_location',
    'name' => 'visit_location',
    'input_classes' => 'visit',
])

@if(!empty($sst->acquisition->briefings))
    <br/><br/>
    <div class="row">
        <div class="col-12">
            <h5>Syarikat Yang Hadir Lawatan Tapak</h5>
            <table class="table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th style="width: 10px">Bil</th>
                    <th>Nama Syarikat</th>
                    <th>Status Hadir</th>
                    <th>Nama Wakil/Pemilik</th>
                    <th>No. Kad Pengenalan</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($acquisition->briefings as $key => $briefing)
                        @if($briefing->attendant_status != 3 && $briefing->site_visit == 1)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$briefing->company->company_name}}</td>
                                <td>
                                    @if($briefing->attendant_status == 1)
                                        Pemilik
                                    @elseif($briefing->attendant_status == 2)
                                        Wakil
                                    @endif
                                </td>
                                <td>
                                    @if($briefing->attendant_status == 1)
                                        {{ $briefing->owner->name }}
                                    @elseif($briefing->attendant_status == 2)
                                        {{ $briefing->agent->name }}
                                    @endif
                                </td>
                                <td>
                                    @if($briefing->attendant_status == 1)
                                        {{ $briefing->owner->ic_number }}
                                    @elseif($briefing->attendant_status == 2)
                                        {{ $briefing->agent->ic_number }}
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

<br/><br/>
<h5>Senarai Syarikat yang Membeli Dokumen</h5>
<div class="row">
    <div class="col-12">        
        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th style="width: 10px">Bil</th>
                    <th>Nama Syarikat</th>
                    <th>Status Pembeli</th>
                    <th>Nama Wakil/Pemilik</th>
                    <th>No. Kad Pengenalan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($acquisition->sales as $key => $jual)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$jual->company->company_name}}</td>
                        <td>
                            @if($jual->buyer_status == 1)
                                Pemilik
                            @elseif($jual->buyer_status == 2)
                                Wakil
                            @endif
                        </td>
                        <td>
                            @if($jual->buyer_status == 1)
                                {{ $jual->company->owners->pluck('name')->first() }}
                            @elseif($jual->buyer_status == 2)
                                {{ $jual->company->agents->pluck('name')->first() }}
                            @endif
                        </td>
                        <td>{{ $jual->ic_number }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
