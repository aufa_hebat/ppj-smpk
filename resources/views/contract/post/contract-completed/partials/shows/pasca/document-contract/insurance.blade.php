@push('scripts')
<script>
    jQuery(document).ready(function($) {

        $('#publicLiability').show();
        $('#work').hide();
        $('#compensation').hide();

        $('input[type=radio][name=insurance_type]').change(function () {

            if(this.value == '1'){
                $('#publicLiability').show();
                $('#work').hide();
                $('#compensation').hide();
            }
            else if (this.value == '2'){
                $('#publicLiability').hide();
                $('#work').show();
                $('#compensation').hide();
            }
            else if (this.value == '3'){
                $('#publicLiability').hide();
                $('#work').hide();
                $('#compensation').show();
            }
        });

        @if(!empty($insurance))
            $('#public_insurance_name').html('{{ $insurance->public_insurance ? $insurance->public_insurance->name : null }}');
            $('#public_policy_amount').html('{{ $insurance->public_policy_amount ? money()->toHuman($insurance->public_policy_amount) : null }}');
            $('#public_start_date').html('{{ $insurance->public_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_start_date)->format('d/m/Y') : null }}');
            $('#public_proposed_date').html('{{ $insurance->public_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_proposed_date)->format('d/m/Y') : null }}');
            $('#public_approval_received_date').html('{{ $insurance->public_approval_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_approval_received_date)->format('d/m/Y') : null }}');
            $('#public_policy_no').html('{{ $insurance->public_policy_no }}');
            $('#public_received_date').html('{{ $insurance->public_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_received_date)->format('d/m/Y') : null }}');
            $('#public_expired_date').html('{{ $insurance->public_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_expired_date)->format('d/m/Y') : null }}');
            $('#public_approval_proposed_date').html('{{ $insurance->public_approval_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_approval_proposed_date)->format('d/m/Y') : null }}');

            $('#work_insurance_name').html('{{ $insurance->work_insurance ? $insurance->work_insurance->name : null }}');
            $('#work_policy_amount').html('{{ $insurance->work_policy_amount ? money()->toHuman($insurance->work_policy_amount) : null }}');
            $('#work_start_date').html('{{ $insurance->work_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_start_date)->format('d/m/Y') : null }}');
            $('#work_proposed_date').html('{{ $insurance->work_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_proposed_date)->format('d/m/Y') : null }}');
            $('#work_approval_received_date').html('{{ $insurance->work_approval_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_approval_received_date)->format('d/m/Y') : null }}');
            $('#work_policy_no').html('{{ $insurance->work_policy_no }}');
            $('#work_received_date').html('{{ $insurance->work_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_received_date)->format('d/m/Y') : null }}');
            $('#work_expired_date').html('{{ $insurance->work_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_expired_date)->format('d/m/Y') : null }}');
            $('#work_approval_proposed_date').html('{{ $insurance->work_approval_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_approval_proposed_date)->format('d/m/Y') : null }}');

            $('#compensation_insurance_name').html('{{ $insurance->compensation_insurance ? $insurance->compensation_insurance->name : null }}');
            $('#compensation_policy_amount').html('{{ $insurance->compensation_policy_amount ? money()->toHuman($insurance->compensation_policy_amount) : null }}');
            $('#compensation_start_date').html('{{ $insurance->compensation_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_start_date)->format('d/m/Y') : null }}');
            $('#compensation_proposed_date').html('{{ $insurance->compensation_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_proposed_date)->format('d/m/Y') : null }}');
            $('#compensation_approval_received_date').html('{{ $insurance->compensation_approval_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_approval_received_date)->format('d/m/Y') : null }}');
            $('#compensation_policy_no').html('{{ $insurance->compensation_policy_no }}');
            $('#compensation_received_date').html('{{ $insurance->compensation_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_received_date)->format('d/m/Y') : null }}');
            $('#compensation_expired_date').html('{{ $insurance->compensation_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_expired_date)->format('d/m/Y') : null }}');
            $('#compensation_approval_proposed_date').html('{{ $insurance->compensation_approval_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_approval_proposed_date)->format('d/m/Y') : null }}');

            @if($insurance->documents_publicLiability->count() > 0)
            var t_publicLiability = $('#tblUpload_publicLiability').DataTable({
            searching: false,
            ordering: false,
            paging: false,
            info:false
            });

            var counter_publicLiability = 1;
            @foreach($insurance->documents_publicLiability as $doc)
            t_publicLiability.row.add( [
                counter_publicLiability,
                '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
            ] ).draw( false );

            counter_publicLiability++;
            @endforeach
            @endif

            @if($insurance->documents_work->count() > 0)
            var t_work = $('#tblUpload_work').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_work = 1;
            @foreach($insurance->documents_work as $doc)
            t_work.row.add( [
                counter_work,
                '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
            ] ).draw( false );

            counter_work++;
            @endforeach
            @endif

            @if($insurance->documents_compensation->count() > 0)
            var t_compensation = $('#tblUpload_compensation').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_compensation = 1;
            @foreach($insurance->documents_compensation as $doc)
            t_compensation.row.add( [
                counter_compensation,
                '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
            ] ).draw( false );

            counter_compensation++;
            @endforeach
            @endif

        @endif

    });

</script>
@endpush

<h5>Insuran</h5>
<br>
<div class="row">
<div class="col-12">
    <div class="form-group">
        <div class="selectgroup w-100">
            <label class="selectgroup-item">
                <input type="radio" name="insurance_type" value="1" class="selectgroup-input" checked>
                <span class="selectgroup-button selectgroup-button-icon">{{ __('Tanggungan Awam') }}</span>
            </label>
            <label class="selectgroup-item">
                <input type="radio" name="insurance_type" value="2" class="selectgroup-input">
                <span class="selectgroup-button selectgroup-button-icon">{{ __('Untuk Kerja') }}</span>
            </label>
            <label class="selectgroup-item">
                <input type="radio" name="insurance_type" value="3" class="selectgroup-input">
                <span class="selectgroup-button selectgroup-button-icon">{{ __('Pampasan Kerja') }}</span>
            </label>
        </div>
    </div>

    @include('contract.post.partials.shows.insurance.publicLiability')
    @include('contract.post.partials.shows.insurance.work')
    @include('contract.post.partials.shows.insurance.compensation')
</div>
</div>