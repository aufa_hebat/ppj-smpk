<h5>Maklumat SST/LOA</h5>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Tarikh Mula Kerja</div>
            <div>{{ isset($sst->start_working_date) ? $sst->start_working_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Tarikh Siap Kerja</div>
            <div>{{ isset($sst->end_working_date) ? $sst->end_working_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Tempoh Kerja</div>
            <div>
                {!! $sale->box->period. ' ' . $sale->box->period_type->name !!}                    
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Tarikh Surat</div>
            <div>{{ isset($sst->letter_date) ? $sst->letter_date->format('d/m/Y') : "-" }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Tempoh Liabiliti Kecacatan</div>
            <div>{{ isset($sst->defects_liability_period) ? $sst->defects_liability_period : "-" }} Bulan</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Elaun Skim Latihan 1Malaysia</div>
            <div>{{ isset($sst->sl1m_allowance) ? money()->toHuman($sst->sl1m_allowance) : "-" }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Skim Latihan 1Malaysia</div>
            <div>
                @if(isset($sst->sl1m_entity) && ($sst->sl1m_entity > 0))
                    {{ $sst->sl1m_entity }} Orang
                @else
                    Tiada
                @endif
                {{--  {{ isset($sst->sl1m_entity)  ? $sst->sl1m_entity : "-" }} Orang  --}}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Prime Rate</div>
            <div>{{ isset($sst->document_blr) ? money()->toCommon($sst->document_blr) : "-" }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Kadar LAD Sehari (RM)</div>
            <div>{{ isset($sst->document_LAD_amount) ? money()->toHuman($sst->document_LAD_amount) : "-" }}</div>
        </div>
    </div>
</div>

