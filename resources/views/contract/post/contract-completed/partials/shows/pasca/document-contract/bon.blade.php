@push('scripts')
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
<script>
    jQuery(document).ready(function($) {

        $('#jaminanBank').hide();
        $('#jaminanInsuran').hide();
        $('#cekBank').hide();
        $('#wangJaminan').hide();
        $('#uploads').hide();

        @if(!empty($bon))
            @if( $bon->bon_type == 1 )
                $('#jaminanBank').show();
            @elseif($bon->bon_type == 2)
                $('#jaminanInsuran').show();
            @elseif($bon->bon_type == 3)
                $('#cekBank').show();
            @elseif($bon->bon_type == 4)
                $('#wangJaminan').show();
            @endif

            $('#bon_type').html('{{ $bon->type ? $bon->type->description : null }}');

            $('#bank_name').html('{{ $bon->bank ? $bon->bank->name : null }}');
            $('#bank_no').html('{{ $bon->bank_no }}');
            $('#bank_start_date').html('{{ $bon->bank_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_start_date)->format('d/m/Y') : null }}');
            $('#bank_proposed_date').html('{{ $bon->bank_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_proposed_date)->format('d/m/Y') : null }}');
            $('#bank_approval_received_date').html('{{ $bon->bank_approval_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_approval_received_date)->format('d/m/Y') : null }}');
            $('#bank_received_date').html('{{ $bon->bank_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_received_date)->format('d/m/Y') : null }}');
            $('#bank_amount').html('{{ $bon->bank_amount ? money()->toHuman($bon->bank_amount) : null }}');
            $('#bank_expired_date').html('{{ $bon->bank_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_expired_date)->format('d/m/Y') : null }}');
            $('#bank_approval_proposed_date').html('{{ $bon->bank_approval_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_approval_proposed_date)->format('d/m/Y') : null }}');

            $('#insurance_name').html('{{ $bon->insurance_name ? $bon->insurance->name : null }}');
            $('#insurance_no').html('{{ $bon->insurance_no }}');
            $('#insurance_start_date').html('{{ $bon->insurance_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_start_date)->format('d/m/Y') : null }}');
            $('#insurance_proposed_date').html('{{ $bon->insurance_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_proposed_date)->format('d/m/Y') : null }}');
            $('#insurance_approval_received_date').html('{{ $bon->insurance_approval_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_approval_received_date)->format('d/m/Y') : null }}');
            $('#insurance_received_date').html('{{ $bon->insurance_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_received_date)->format('d/m/Y') : null }}');
            $('#insurance_amount').html('{{ $bon->insurance_amount ? money()->toHuman($bon->insurance_amount) : null }}');
            $('#insurance_expired_date').html('{{ $bon->insurance_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_expired_date)->format('d/m/Y') : null }}');
            $('#insurance_approval_proposed_date').html('{{ $bon->insurance_approval_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_approval_proposed_date)->format('d/m/Y') : null }}');

            $('#cheque_name').html('{{ $bon->cheque_bank ? $bon->cheque_bank->name : null }}');
            $('#cheque_resit_no').html('{{ $bon->cheque_resit_no }}');
            $('#cheque_received_date').html('{{ $bon->cheque_received_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->cheque_received_date)->format('d/m/Y') : null }}');
            $('#cheque_no').html('{{ $bon->cheque_no }}');
            $('#cheque_submitted_date').html('{{ $bon->cheque_submitted_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->cheque_submitted_date)->format('d/m/Y') : null }}');

            $('#money_received_at').html('{{ $bon->money_received_at ? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->money_received_at)->format('d/m/Y') : null }}');
            $('#money_amount').html('{{ $bon->money_amount ? money()->toHuman($bon->money_amount) : null }}');

            $('#uploads').show();
            @if($bon->documents->count() > 0)
                var t = $('#tblUpload').DataTable({
                    searching: false,
                    ordering: false,
                    paging: false,
                    info:false
                });

                var counter = 1;
                
                @foreach($bon->documents as $doc)
                    t.row.add( [
                        counter,
                        '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                    ] ).draw( false );

                    counter++;
                @endforeach
            @endif
        @endif
    });

</script>
@endpush

<h5>Bon Pelaksanaan</h5>
<br>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <div class="text-muted">{{ __('Jenis Bon Pelaksanaan') }}</div>
                    <div id="bon_type"></div>
                </div>
            </div>
        </div>
        @include('contract.post.partials.shows.bon.jaminanBank')
        @include('contract.post.partials.shows.bon.jaminanInsuran')
        @include('contract.post.partials.shows.bon.cekBank')
        @include('contract.post.partials.shows.bon.wangJaminan')
        @include('contract.post.partials.shows.bon.uploads')
    </div>
</div>