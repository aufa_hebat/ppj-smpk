@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.textarea')
    @include('components.forms.assets.select2')
    @include('components.forms.assets.datetimepicker')
    
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endpush
@section('content')
    <div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="contract-tab-content" role="tablist">
                <li class="list-group-item" id="acquisition">
                    <a class="list-group-item-action" data-toggle="tab" href="#acquisition-details" role="tab" aria-controls="acquisition-details" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Perolehan') }}
                    </a>
                </li> 
                <li class="list-group-item" id="sale">
                    <a class="list-group-item-action" data-toggle="tab" href="#sale-details" role="tab" aria-controls="sale-details" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Maklumat Jualan') }}
                    </a>
                </li> 
                <li class="list-group-item" id="sale">
                    <a class="list-group-item-action" data-toggle="tab" href="#visit-details" role="tab" aria-controls="visit-details" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Lawatan Tapak') }}
                    </a>
                </li> 
                <li class="list-group-item" id="briefing">
                    <a class="list-group-item-action" data-toggle="tab" href="#briefing-details" role="tab" aria-controls="briefing-details" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Maklumat Taklimat') }}
                    </a>
                </li>                                                           
                <li class="list-group-item" id="contract">
                    <a class="list-group-item-action" data-toggle="tab" href="#contract-details" role="tab" aria-controls="contract-details" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Dokumen Kontrak') }}
                    </a>
                </li>
                <li class="list-group-item" id="monitoring">
                    <a class="list-group-item-action" data-toggle="tab" href="#monitoring-details" role="tab" aria-controls="monitoring-details" aria-selected="false">
                        @icon('fe fe-dollar-sign')&nbsp;{{ __('Pemantauan') }}
                    </a>
                </li>     
                <li class="list-group-item" id="ipc">
                    <a class="list-group-item-action" data-toggle="tab" href="#ipc-details" role="tab" aria-controls="ipc-details" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('IPC') }}
                    </a>
                </li>    
                <li class="list-group-item" id="vo">
                    <a class="list-group-item-action" data-toggle="tab" href="#vo-details" role="tab" aria-controls="vo-details" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Pelarasan Harga') }}
                    </a>
                </li> 
                <li class="list-group-item" id="eot">
                    <a class="list-group-item-action" data-toggle="tab" href="#eot-details" role="tab" aria-controls="eot-details" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('Pelanjutan Kontrak') }}
                    </a>
                </li>
                <li class="list-group-item" id="sofa">
                    <a class="list-group-item-action" data-toggle="tab" href="#sofa-details" role="tab" aria-controls="sofa-details" aria-selected="false">
                        @icon('fe fe-briefcase')&nbsp;{{ __('SOFA') }}
                    </a>
                </li>                                                                              
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'contract-completed'])
                        @slot('tabs')
                            @component('components.tab.content', ['id' => 'acquisition-details', 'active' => true])
                                @slot('content')
                                    @include('contract.post.contract-completed.partials.shows.acquisition-details', ['sst' => $sst])
                                @endslot
                            @endcomponent
                            @component('components.tab.content', ['id' => 'akuan-details'])
                                @slot('content')
                                    
                                @endslot
                            @endcomponent 
                            @component('components.tab.content', ['id' => 'subcontractor-details'])
                                @slot('content')
                                    
                                @endslot
                            @endcomponent 
                            @component('components.tab.content', ['id' => 'statement-details'])
                                @slot('content')
                                    
                                @endslot
                            @endcomponent                                                                                  
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection