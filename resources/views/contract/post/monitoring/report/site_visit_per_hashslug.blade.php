<style type="text/css">
	div.breakNow { page-break-inside:avoid; page-break-after:always; }

    .bordered {
    	border-color: #000;
    	border-style: solid;
    	border-width: 1px;
	}
    .title {
        font-size: 16px;
        font-family: "Arial";
	}

    .content {
        font-size: 15px;
        font-family: "Arial";
	}
</style>

<center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>

<center>
  <style type="text/css">
	div.breakNow { page-break-inside:avoid; page-break-after:always; }

	.bordered {
    	border-color: #959594;
    	border-style: solid;
    	border-width: 1px;
	}

	.bordered-double {
    	border-color: #000000;
    	border-style: double;
    	border-width: 3px;
	}

	title{
		font-size: 16px;
	}
</style>

<h2>Lawatan Penilaian Tapak</h2>

<table border="0" style="width: 100%;border-collapse: collapse;padding: 5px 5px;" >
    <tr>
        <td valign="top" style="padding: 5px 5px;" width="20%">Tajuk Projek </td>
        <td valign="top" style="padding: 5px 5px;" width="2%"> : </td>
        <td width="78%" style="padding: 5px 5px; "><span style="display: inline-block">{{ $siteVisit->acquisition->title }}</span></td>
          
    </tr>
    
    <tr>
        <td style="padding: 5px 5px;">No Kontrak </td>
        <td style="padding: 5px 5px;" valign="top" width="2%"> : </td>
        <td><span >{{ $siteVisit->acquisition->reference }}</span></td>         
    </tr>
    
     <tr>
        <td style="padding: 5px 5px;">Tarikh Lawatan </td>
        <td style="padding: 5px 5px;" valign="top" width="2%"> : </td>
        <td><span style="padding: 5px 5px;">{{ $siteVisit->visit_date->format('d/m/Y') }}</span></td>       
    </tr>
    
     <tr>
        <td style="padding: 5px 5px;">Perkara </td>
        <td style="padding: 5px 5px;" valign="top" width="2%"> : </td>
        <td><span style=";padding: 5px 5px;">{{ $siteVisit->name }}</span></td>        
    </tr>
    
     <tr>
        <td style="padding: 5px 5px;">Ulasan Penilaian</td>
        <td style="padding: 5px 5px;" valign="top" width="2%"> : </td>
         <td><span style=";padding: 5px 5px;">{{ $siteVisit->remarks }}</span></td>        
    </tr>
    
    <tr>
        <td style="padding: 5px 5px;">Peratusan Siap (%)</td>
        <td style="padding: 5px 5px;" valign="top" width="2%"> : </td>
         <td><span style="padding: 5px 5px;">{{ $siteVisit->percentage_completed }}</span></td>       
    </tr>
    
    <tr>
        <td style="padding: 5px 5px;">Pegawai Hadir Lawatan :</td>
        <td style="padding: 5px 5px;" valign="top" width="2%"> : </td>
         <td><span style="padding: 5px 5px;">{{ $siteVisit->visitedBy->name }}</span></td>     
    </tr>
    
</table>
</center>
