<h3>EOT</h3>

<div class="row">
	<div class="col">
		@component('components.collapse.container', ['id' => 'eot'])
			@slot('content')
				@forelse($sst->eots as $eot)
					@include('contract.post.monitoring.partials.eot-content')
				@empty
					@include('components.cards.empty-info')
				@endforelse
			@endslot
		@endcomponent
	</div>
</div>