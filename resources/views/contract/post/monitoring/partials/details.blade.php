

<h3>{{ __('Kemajuan Fizikal Projek') }}</h3>
<div class="row">
	<div class="col-6">
        @include('components.cards.figure', [
            'header' => '',
            'icon' => '',
            'content_color' => 'primary',
            'content' => $sst->total_scheduled_completed . '%',
            'footer' => 'Jumlah Peratusan dijangka Siap',
        ])
    </div>
    <div class="col-6">
        @include('components.cards.figure', [
            'header' => '',
            'icon' => '',
            'content_color' => label_class($sst->total_actual_completed, $sst->total_scheduled_completed),
            'content' => $sst->total_actual_completed . '%',
            'footer' => 'Jumlah Peratusan Sebenar Siap',
        ])
    </div>
</div>
<hr>
<h3>{{ __('Kemajuan Pembayaran Projek') }}</h3>
<div class="row">
    <div class="col-6">
        @include('components.cards.figure', [
            'header' => '',
            'icon' => '',
            'content' => 10000 . '/' . 500000,
            'footer' => 'Jumlah Pembayaran Telah Dibayar',
        ])
    </div>
    <div class="col-6">
        @include('components.cards.figure', [
            'header' => '',
            'icon' => '',
            'content' => '50%',
            'footer' => 'Peratusan Pembayaran',
        ])
    </div>
</div>
<hr>


