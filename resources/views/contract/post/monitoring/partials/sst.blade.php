<!--@include('contract.post.acceptance-letter.partials.shows.acquisition-details', ['appointed' => $appointed, 'sst' => $sst])-->
<h3>Maklumat Perolehan</h3>

<div class="row">
    <div class="col-12">
        <div class="form-group">
            <div class="text-muted">No. Kontrak</div>
            <div>{{ $sst->contract_no or '-' }}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <div class="text-muted">Tajuk Perolehan</div>
            <div>{!! $appointed->acquisition->title !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Rujukan Fail</div>
            <div>{{ $appointed->acquisition->approval->file_reference }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Harga Kontrak</div>
            <div>{!! money()->toHuman($appointed->offered_price) !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Nama Kontraktor</div>
            <div>{{ $appointed->company->company_name }}</div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Untuk Perhatian</div>
            <div>{{ $sst->owner->name or '-' }}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Alamat Kontraktor</div>
            <div style="text-transform: capitalize">
                @if($appointed->acquisition->approval->type->name == 'Kerja')
                    @if(!empty($cidb->addresses) && count($cidb->addresses) > 0)
                        {{ $cidb->addresses[0]->primary .', '. $cidb->addresses[0]->secondary .', '. $cidb->addresses[0]->state .' '. $cidb->addresses[0]->postcode ?? "-" }}
                    @else
                        -
                    @endif
                @elseif($appointed->acquisition->approval->type->name == 'Bekalan' || $appointed->acquisition->approval->type->name == 'Perkhidmatan')
                    @if(count($mof->addresses) > 0)
                        {{ $mof->addresses[0]->primary .', '. $mof->addresses[0]->secondary .', '. $mof->addresses[0]->state .' '. $mof->addresses[0]->postcode }}
                    @else
                        -
                    @endif
                    
                @endif
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">Lokasi</div>
            <div>{!! $appointed->acquisition->approval->locations_to_string or '-' !!}</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">No. Telefon</div>
            <div>
                @foreach($appointed->company->phones as $phone)
                    @if($phone->phone_type_id == 3)
                        {{ $phone->phone_number or "-" }}
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <div class="text-muted">No. Faks</div>
            <div>
                @foreach($appointed->company->phones as $phone)
                    @if($phone->phone_type_id == 4)
                        {{ $phone->phone_number or "-" }}
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>


@include('contract.post.acceptance-letter.partials.shows.sst', ['appointed' => $appointed, 'sst' => $sst])
@include('contract.post.acceptance-letter.partials.shows.bon', ['appointed' => $appointed, 'sst' => $sst])
@include('contract.post.acceptance-letter.partials.shows.insurance', ['appointed' => $appointed, 'sst' => $sst])
@include('contract.post.acceptance-letter.partials.shows.approval', ['appointed' => $appointed, 'sst' => $sst])