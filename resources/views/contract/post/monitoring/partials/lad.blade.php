<h3>LAD</h3>

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            @if(!empty($sst->cnc))
                $('#cnc_reference_no').html('{{ $sst->cnc->reference_no }}');
                $('#cnc_signature_date').html('{{ $sst->cnc->signature_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->cnc->signature_date)->format('d/m/Y') : '' }}');
                $('#cnc_test_award_date').html('{{ $sst->cnc->test_award_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->cnc->test_award_date)->format('d/m/Y') : '' }}');
                $('#cnc_clause_1').html('{{ $sst->cnc->clause_1 }}');
                $('#cnc_clause_2').html('{{ $sst->cnc->clause_2 }}');

                $('#cnc_assessment_date').html('{{ $sst->cnc->assessment_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$sst->cnc->assessment_date)->format('d/m/Y') : '' }}');
                $('#cnc_lad_per_day').html('{{ $sst->cnc->lad_per_day     ? money()->toHuman($sst->cnc->lad_per_day   ) : 'RM 0.00' }}');

                // UPLOADS
                @if($sst->cnc->documents->count() > 0)
                    var cnc_t = $('#cnc_tblUpload').DataTable({
                            searching: false,
                            ordering: false,
                            paging: false,
                            info:false
                        });

                    var cno_counter = 1;
                    @foreach($sst->cnc->documents as $doc)
                        cnc_t.row.add( [
                            cno_counter,
                            '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                        ] ).draw( false );

                        cno_counter++;
                    @endforeach
                @endif
                // UPLOADS END
            @endif
        });

    </script>
@endpush

<h4>Sijil Tidak Siap Kerja</h4>

<div class="row">

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('No Rujukan Fail') }}</div>
                    <div id="cnc_reference_no"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Setuju Terima') }}</div>
                    <div id="cnc_clause_1"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Sijil Di tandatangan') }}</div>
                    <div id="cnc_signature_date"></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <div class="text-muted"> {{ __('Tarikh Pengujian Dan Pentauliah') }}</div>
                    <div id="cnc_test_award_date"></div>
                </div>
                <div class="form-group">
                    <div class="text-muted"> {{ __('Klausa Denda') }}</div>
                    <div id="cnc_clause_2"></div>
                </div>
            </div>
        </div>

        @include('contract.post.cnc.partials.shows.lad')
        @include('contract.post.cnc.partials.shows.uploads')
    </div>
</div>