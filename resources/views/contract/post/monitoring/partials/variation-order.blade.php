<h3>SENARAI PERAKUAN PELARASAN JUMLAH HARGA KONTRAK (PPJHK)</h3>

<div class="row">
	<div class="col">


           <!-- PPJHK	--> 	
            @component('components.collapse.container', ['id' => 'vo-ppjhk-content'])
			@slot('content')
				@if($ppjhk_count > 0)
					@include('contract.post.monitoring.partials.vo-ppjhk-content')
				@else
					@include('components.cards.empty-info')
				@endif
			@endslot
		@endcomponent

	</div>
</div>