@component('components.collapse.content', ['id' => $eot->hashslug, 'container_id' => 'eot'])
	@slot('header')
		Bil. {{ $eot->bil }}
	@endslot
	@slot('body')

        @includeWhen(!empty($eot), 'contract.post.extension.show_list.partials.forms.extension')
		@includeWhen(empty($eot), 'components.cards.empty-info')
		<hr>
		@includeWhen(
			!empty($eot->eot_bon), 
			'contract.post.extension.show_list.partials.forms.bon', 
			[
				'bon' => $eot->eot_bon,
				'expiry_date' => sst($sst)->getExpiredDate(),
				'proposed_date' => sst($sst)->getProposedDate(),
			]
		)
		<hr>
		@includeWhen(
			!empty($eot->eot_insurance), 
			'contract.post.extension.show_list.partials.forms.insurance', 
			[
				'insurance' => $eot->eot_insurance,
				'expiry_date' => sst($sst)->getExpiredDate(),
				'proposed_date' => sst($sst)->getProposedDate(),
			]
		)
	
	@endslot
@endcomponent