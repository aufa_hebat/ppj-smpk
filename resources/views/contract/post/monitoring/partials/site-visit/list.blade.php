
@component('components.datatable', 
    [
        'table_id' => 'contract-post-monitoring',
        'route_name' => 'api.datatable.contract.acquisition.monitoring.site-visit',
        'param' => [
            'hashslug' => $sst->hashslug,
        ],
        'columns' => [
            ['data' => null , 'name' => __('Bil'), 'searchable' => false, 'orderable' => false],
            ['data' => 'visit_date', 'title' => __('Tarikh Lawatan'), 'defaultContent' => '-'],
            ['data' => 'name', 'title' => __('Perkara'), 'defaultContent' => '-'],        
            ['data' => 'percentage_completed', 'title' => __('Peratusan Siap(%)'), 'defaultContent' => '-'],
            ['data' => 'attachment', 'title' => __('Lampiran'), 'defaultContent' => '-'],
            ['data' => 'visited_by', 'title' => __('Pegawai Hadir'), 'defaultContent' => '-'],
            ['data' => 'visited_at', 'title' => __('Tarikh (Carta)'), 'defaultContent' => '-'],
            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
        ],
        'headers' => [
           __('Bil'), __('Tarikh Lawatan'),__('Perkara'), 
           __('Peratusan Siap(%)'), __('Lampiran'), __('Pegawai Hadir'), 
           __('Tarikh (Carta)'), __('table.action')
        ],
        'actions' => minify(view('contract.post.monitoring.partials.site-visit.actions')->render()),
        'columnDefs' => minify(view('contract.post.monitoring.partials.site-visit.columnDefs')->render())
    ]
)
@endcomponent