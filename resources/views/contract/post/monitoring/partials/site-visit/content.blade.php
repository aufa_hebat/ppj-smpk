<nav>
  <div class="nav nav-tabs justify-content-end border-0" id="nav-tab" role="tablist">
    <!-- comment , no graph here <a class="nav-item nav-link active m-3" id="nav-site-visit-list-tab" data-toggle="tab" href="#nav-site-visit-list" role="tab" aria-controls="nav-site-visit-list" aria-selected="true">@icon('fe fe-list') Jadual</a>
    <a class="nav-item nav-link m-3" id="nav-site-visit-graph-tab" data-toggle="tab" href="#nav-site-visit-graph" role="tab" aria-controls="nav-site-visit-graph" aria-selected="false">@icon('fe fe-bar-chart-2') Graf</a>
   -->
    </div>
</nav>
<div class="tab-content mb-3" id="nav-site-visit-content">
  <div class="tab-pane fade show active mt-5" id="nav-site-visit-list" role="tabpanel" aria-labelledby="nav-site-visit-list-tab">
	@include('contract.post.monitoring.partials.site-visit.list')
  </div>
  <div class="tab-pane fade mt-5" id="nav-site-visit-graph" role="tabpanel" aria-labelledby="nav-site-visit-graph-tab">
	@include('contract.post.monitoring.partials.site-visit.chart')
  </div>
</div>