@push('style')
  #datetimepicker-visit_date{
    z-index:100;
  }
@endpush

@push('scripts')
<script type="text/javascript">
    function getBahagian(value) {
        
        var ddl = value;
        var selectedOption = ddl.options[ddl.selectedIndex];
        //alert(selectedOption.value);
    
            //first clear old value in list
            document.getElementById('visited_by').innerText = null
    
            //set value in options
            var select = document.getElementById("visited_by");
            
            //change to axios here
             var route_name = 'api.manage.finduserbydepartment'; 
                       axios.get(route(route_name, selectedOption.value)).then(response => {
                               try { 
                                  
                                  //alert(response.data.usersByDepartment[0].name);
                                  for (var j = 0; j < response.data.usersByDepartment.length; j++){
                                      var option = document.createElement("option");
                                      option.text = response.data.usersByDepartment[j].name;
                                      option.value = response.data.usersByDepartment[j].id;
                                      select.appendChild(option);
                                       
                                       
                                   }
                                  
                               }catch(err) {}             
                       });
            
            /*
            for (i = 0; i < 10; i++){
              var option = document.createElement("option");
              option.text = "value_"+i;
              option.value = ''+i;
              select.appendChild(option);
            }*/
        
    }

</script>
@endpush

@include('components.forms.assets.datetimepicker')
@include('components.forms.assets.select2')
@include('contract.post.monitoring.partials.site-visit.scripts')

@component('components.modals.base', [
	'id' => 'site-visit-modal',
	'tooltip' => __('Lawatan Penilaian Tapak'),
	'modal_title' => __('Lawatan Penilaian Tapak'),
	])
	@slot('modal_body')
		<div class="row">
			<div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		    <div class="col-md-12 col-lg-12 col-xl-12">
		      	<form id="site-visit-form" method="POST" enctype="multipart/form-data">
                            
                     @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Lawatan Sebenar'),
                        'id' => 'visit_date',
                        'name' => 'visit_date',
                        'config' => ['format' => config('datetime.display.date')],
                    ])
                            
                            
                            Tarikh Carta (Data paling baharu akan dipaparkan)
                                <select id="id_contract_progress" name="id_contract_progress" class="form-control"> 
                                    @foreach($contractProgresses as $obj)
                                    <option value="{{$obj->id}}">{{$obj->on_plan_date->format('d/m/Y')}}</option>
                                    @endforeach
                                </select>     
                            
		      		@include('components.forms.hidden', [
					    'value' => user()->id,
					    'id' => 'user_id',
					    'name' => 'user_id'
					])
					@include('components.forms.hidden', [
					    'value' => $sst->id,
					    'id' => 'sst_id',
					    'name' => 'sst_id'
					])
					@include('components.forms.hidden', [
					    'value' => $sst->department->id,
					    'id' => 'department_id',
					    'name' => 'department_id'
					])
					@include('components.forms.hidden', [
					    'value' => $sst->acquisition->id,
					    'id' => 'acquisition_id',
					    'name' => 'acquisition_id'
					])
					@include('components.forms.hidden', [
					    'value' => $sst->acquisition->approval->id,
					    'id' => 'acquisition_approval_id',
					    'name' => 'acquisition_approval_id'
					])
		      		@include('components.forms.input', [
					    'input_label' => __('Perkara'),
					    'id' => 'name',
					    'name' => 'name'
					])
					@include('components.forms.textarea', [
					    'input_label' => __('Ulasan Penilaian'),
					    'id' => 'remarks',
					    'name' => 'remarks'
					])
                                        
                   <div class="form-group{{ $errors->has('Jabatan') ? ' has-error' : '' }}">
		    <label for="Jabatan" class="control-label">Jabatan</label><br>
		       <select class="form-control input-lg"  onchange="getBahagian(this);"  name="department_id" id="JabatanPelaksana" style="width: 100%;">
		        <option value="" ></option>
		                @foreach($JabatanPelaksana as $jp)
                          @if($jp->id != 1)      
		            <option value="{{$jp->id}}" data-mail="{{$jp->code}}">{{$jp->name}}</option>
                          @endif
		                @endforeach
		    </select>
		</div>
                                        
                                       Pegawai Hadir Lawatan Tapak
                                         <select class="select2 form-control" id="visited_by" name="visited_by">     
                                           <option  value=""></option>         
                                                             
                                        </select>       
                                        
                                      
                                        
                                        
                                        
                                        
					@include('components.forms.input', [
					    'input_label' => __('Peratusan Siap (%)'),
					    'id' => 'percentage_completed',
					    'name' => 'percentage_completed',
					    'type' => 'number'
					])
					@include('components.forms.file', [
                        'input_label' => 'Muat Naik Laporan Lawatan Penilaian Tapak',
                        'name' => 'document',
                        'id' => 'document',
                      ])
                      <button type="reset" id="file_reset" style="display:none">
                      
                      @push('scripts')
                         <script type="text/javascript">
			    jQuery(document).ready(function($) {
                               // alert('hi');
                                $(document).on('click', '#site-visit-button', function(event) {					
					//alert('onclick didalam');
                                        $('#name').val("");
                                        $('#remarks').val("");
                                        $('#visited_by').val("");
                                        $('#percentage_completed').val("");
                                        //$('#document').val("");
                                        $('#file_reset').trigger('click');
	                           });                                  
                             });
                          </script>
    @endpush
                      
                      
                      
		      	</form>
		    </div>
		    <div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		</div>
	@endslot
	@slot('modal_footer')
		<button class="btn btn-primary site-visit-save-action-btn">{{ __('Simpan') }}</button> 
	@endslot
@endcomponent