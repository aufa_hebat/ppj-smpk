<p><strong>Dijana pada:</strong> {{ $eot->created_at->format(config('datetime.dashboard')) }}</p>
<p><strong>Dikemaskini pada:</strong> {{ $eot->updated_at->format(config('datetime.dashboard')) }}</p>

@component('components.table')
	@slot('tbody')
		<tr>
			<th>Bil.</th>
			<td>{{ $eot->bil }}</td>
		</tr>
		<tr>
			<th>Period</th>
			<td>{{ $eot->period_label }}</td>
		</tr>
		<tr>
			<th>Seksyen</th>
			<td>{{ $eot->section_label }}</td>
		</tr>
		<tr>
			<th>Tarikh Tamat Asal</th>
			<td>{{ $eot->original_end_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Tarikh Tamat Penangguhan</th>
			<td>{{ $eot->extended_end_date->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent