@push('scripts')
	<script>
		jQuery(document).ready(function($) {
			var ctx2 = document.getElementById('site-visit-chart').getContext('2d');
            var cfg = window.chartConfig(
                'line',
                'Graf Kemajuan Fizikal dan Lawatan Penilaian Tapak', 
                @json($site_visit_chart['labels']),
                @json($site_visit_chart['datasets'])
            );
            @isset($site_visit_chart['scales'])
                cfg.options.scales = @json($site_visit_chart['scales']);
            @endisset

            window.siteVisitChart = new Chart(ctx2, cfg);
		});
	</script>
@endpush

<h3>{{ __('Graf Kemajuan Fizikal dan Lawatan Penilaian Tapak') }}</h3>
<div class="row">
    <div class="col">
        <canvas id="site-visit-chart"></canvas>
    </div>
</div>