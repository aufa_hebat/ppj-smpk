<a class="btn btn-sm btn-default show-action-btn" style="cursor: pointer;"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
	<i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
</a>

<!-- Add cetak -->

<a class="btn btn-sm btn-default cetak-action-btn" style="cursor: pointer;"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
	<i class="fe fe-printer text-success"></i> {{ __('Cetak') }}
</a>
<!--
    <a target="blank" href="{{ route('cncCertificate', ['hashslug'=>'hashlug_id'] ) }}"
                           class="btn btn-success">
                            @icon('fe fe-printer') {{ __('Cetak') }}
     </a>-->
                    


<!--<a class="btn btn-sm btn-default destroy-action-btn" style="cursor: pointer;"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
	<i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
</a>-->