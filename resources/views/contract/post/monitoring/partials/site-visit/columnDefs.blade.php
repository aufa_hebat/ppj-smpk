{
    "targets": [0],
    "render": function(data, type, row, meta) {
      return meta.row + 1;
    }
},
{
    "targets": [4],
    "render": function(data, type, row, meta) {
    	if(row.attachment != '') {
    		return '<a class="btn btn-sm btn-default" style="cursor: pointer;" href="' + row.attachment + '" target="_blank">' + row.attachment_name + '</a>';	
    	} else {
    		return '-';
    	}
    }
}
