@push('scripts')
	<script>
		jQuery(document).ready(function($) {
			$(document).on('click', '.site-visit-save-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				var data = new FormData();

				$("#site-visit-form").each(function(){
				    $.each($(this).find(':input'), function(index, val) {
				    	 data.append($(val).prop('name'), $(val).val());
				    });
				});

				data.append('document', $('#site-visit-form input[type=file]')[0].files[0]); 

				axios.post(
						route('api.contract.acquisition.monitor.site-visit.store'), 
						data,
						{ headers: { 'Content-Type': 'multipart/form-data' } }
					).then(function(response) {
						$('#contract-post-monitoring').DataTable().ajax.reload();
						swal('{!! __('Lawatan Penilaian Tapak') !!}', response.data.message, 'success');
                                                 
                                                 $('#site-visit-modal').modal('toggle');
					});
			});
                        
                        //add cetak
                          $(document).on('click', '.cetak-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				 var id = $(this).data('hashslug');
                                 //alert('Cetak!!');
                                 window.open(route('report.monitoring.site-visit-per-hashslug', {hashslug:id}), '_blank');
                                 
                                 //TODO generate route for cetak
                                 
                                 
                                 
                                 
                          });
                        
                        
                        $(document).on('click', '.show-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				 var id = $(this).data('hashslug');
                                 var route_name = 'api.contract.acquisition.monitor.showsitevisit'; 
                                         axios.get(route(route_name, id)).then(response => {
                                              //TODO set here
                                              //alert(response.data.id);
                                              //alert(''+response.data.data.siteVisitData);
                                              //alert(response.data);site_visit
                                              //alert(response.data.site_visit);
                                             // DONE alert(response.data.siteVisit.remarks);
                                             //try to generate popup then clicked using code
                                             //id modal = site-visit-modal-view
                                             //$( "#target" ).click();
                                             //$('#site-visit-modal-view').click();
                                             
                                            
                                            
                                              //alert(response.data.contractProgress.id);
                                              try { 
                                                        //alert(response.data.siteVisit.percentage_completed);
                                                        $('#name_view').val(''+response.data.siteVisit.name); 
                                                        $('#remarks_view').val(''+response.data.siteVisit.remarks); 
                                                        $('#site_visit_percent_view').val(''+response.data.siteVisit.percentage_completed);
                                                        $('#visited_by_view').val(''+response.data.view_visit_by);
                                                        //alert(response.data.siteVisit.visited_at_original);
                                                        $('#visited_at_view').val(''+response.data.visited_at_view);
                                                        //alert(response.data.siteVisit.visit_date);
                                                        $('#visit_date_view').val(''+response.data.visit_date_view);
                                                        //visit_date
                                                        //$('#document_view').val(''+response.data.siteVisit.document);
                                                        $('#site-visit-modal-view').modal('show'); 
                                               }catch(err) {}
                                               
                                              
                                                      
                                                
                                              
                                              
                                             
                                              
                                        });
                        });
                        

			$(document).on('click', '.destroy-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				var id = $(this).data('hashslug');
				swal({
				  title: '{!! __('Amaran') !!}',
				  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonText: '{!! __('Ya') !!}',
				  cancelButtonText: '{!! __('Batal') !!}'
				}).then((result) => {
				  if (result.value) {
					axios.delete(
						route('api.contract.acquisition.monitor.site-visit.destroy', id), 
						{'_method' : 'DELETE'})
						.then(response => {
							$('#contract-post-monitoring').DataTable().ajax.reload();
							swal('{{ __('Padam Lawatan Penilaian Tapak') }}', response.data.message, 'success');
						}).catch(error => console.error(error));
				  }
				});
			});
		});
	</script>
@endpush
