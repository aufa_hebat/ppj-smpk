
@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var ctx = document.getElementById('progress_chart').getContext('2d');
            var config = window.chartConfig(
                'line',
                '', 
                @json($progress_chart['labels']),
                @json($progress_chart['datasets'])
            );
            @isset($chart['scales'])
                config.options.scales = @json($progress_chart['scales']);
            @endisset

            window.chart = new Chart(ctx, config);
                  
        });
    </script>
@endpush


<h3 class="mb-5">
	Jadual Item Kemajuan Fizikal
	@include('components.modals.button', [
		'modal_btn_classes' => 'create-action-btn float-right',
		'label' => __('Muatnaik Jadual Item Kemajuan Fizikal'),
		'icon' => 'fe fe-upload-cloud',
		'id' => 'item-progress-modal',
	])
</h3>

@component('components.table')
	@slot('thead')
	
		<tr>
<!--			<th>No</th>-->
			<th>Item</th>

			<th class="text-center" width="15%">Mula</th>
			<th class="text-center" width="15%">Tamat</th>

		
		</tr>
	@endslot

	@slot('tbody')
		@forelse($sst->activities as $key => $activity)
			<tr>
                                
<!--				<td class="text-center">{{ $key + 1 }}</td>-->
                            
                                @if($activity->scheduled_started_at == null)
                                <td colspan="2"><h4>{{ $activity->name }}</h4></td>


                                
                                @else
                                
                                <td>{{ $activity->name }}</td>

				<td>{{ $activity->scheduled_started_at->format('d-M-Y') }}</td>
				<td>{{ $activity->scheduled_ended_at->format('d-M-Y') }}</td>
                                
                                @endif

			</tr>	
		@empty
			<tr class="alert alert-warning text-center"">
				<td colspan="10">
					{{ __('Dokumen aktiviti tidak dijumpai. Sila muatnaik dokumen aktiviti.') }}
				</td>
			</tr>
		@endforelse
	@endslot
@endcomponent

<br/><br/>

<h3 class="mb-5">
	Jadual Carta Kemajuan Fizikal
	@include('components.modals.button', [
		'modal_btn_classes' => 'create-action-btn float-right',
		'label' => __('Muatnaik Jadual Carta Kemajuan Fizikal'),
		'icon' => 'fe fe-upload-cloud',
		'id' => 'carta-progress-modal',
	])
</h3>

@component('components.table')
	@slot('thead')
	
		<tr>
			<th>No</th>
			<th>On Plan Date</th>

			<th class="text-center" width="15%">On Plan %</th>
			<th class="text-center" width="15%">On Site %</th>
<!--                        <th class="text-center" width="15%">On Plan Payment %</th>-->
<!--                        <th class="text-center" width="15%">Payment %</th>-->
<!--                        <th class="text-center" width="15%">Tindakan</th>-->
		
		</tr>
	@endslot

	@slot('tbody')
		@forelse($contractProgresses as $key => $cp)
			<tr>
				<td>{{ $key + 1 }}</td>
				<td>{{ $cp->on_plan_date->format('d-M-Y') }}</td>
                                <td class="text-center">{{ $cp->on_plan_date_percent }}</td>
                                <td class="text-center">{{ !empty($cp->siteVisit) ? $cp->siteVisit->percentage_completed : null }}</td>
                                <!--<td>{{ $cp->on_plan_payment_percent }}</td>-->
<!--                                <td class="text-center">{{ $cp->on_site_payment_percent }}</td>-->
<!--                                <td class="text-center"><h3 class="mb-5">
          @if(empty(!$cp->siteVisit))
          
    
                                        
	@include('components.modals.button', [
		'modal_btn_classes' => 'create-action-btn float-right',
		'label' => __('Kemaskini'),
		'icon' => 'fe fe-edit',
		'id' => 'update-physical-progress-modal',
                'clickedId' => 'kemaskini_'.$cp->id,
	])
        @endif
</h3></td>-->

			</tr>	
		@empty
			<tr class="alert alert-warning text-center"">
				<td colspan="10">
					{{ __('Dokumen aktiviti tidak dijumpai. Sila muatnaik dokumen aktiviti.') }}
				</td>
			</tr>
		@endforelse
	@endslot
@endcomponent

<hr>
<h3>{{ __('Graf Kemajuan Kontrak') }}</h3>
<div class="row">
    <div class="col">
        <!--<canvas id="physical-chart"></canvas>-->
        <canvas id="progress_chart"></canvas>
    </div>
</div>





<!-- Modal Here  -->
@component('components.modals.base', [
	'id' => 'item-progress-modal',
	'tooltip' => __('Muatnaik Jadual Item Kemajuan Fizikal'),
	'modal_title' => __('Muatnaik Jadual Item Kemajuan Fizikal'),
	])
	@slot('modal_body')
		@include('contract.post.monitoring.partials.forms.activity')
	@endslot
@endcomponent

<!-- Modal Here  -->
@component('components.modals.base', [
	'id' => 'carta-progress-modal',
	'tooltip' => __('Muatnaik Jadual Carta Kemajuan Fizikal'),
	'modal_title' => __('Muatnaik Jadual Carta Kemajuan Fizikal'),
	])
	@slot('modal_body')
		@include('contract.post.monitoring.partials.forms.progress_chart')
	@endslot
@endcomponent


@component('components.modals.base', [
	'id' => 'update-physical-progress-modal',
	'tooltip' => __('Lawatan Penilaian Tapak'),
	'modal_title' => __('Lawatan Penilaian Tapak'),
	])
	@slot('modal_body')
		<div class="row">
			<div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		    <div class="col-md-12 col-lg-12 col-xl-12">
		      	<form id="update-physical-form" method="POST">
                            
                               @include('components.forms.hidden', [
					    'value' => '',
					    'id' => 'id',
					    'name' => 'id',
                                            
					])
                            
                            
                                @include('components.forms.input', [
					    'input_label' => __('Peratusan Siap %'),
					    'id' => 'site_visit_percent',
					    'name' => 'site_visit_percent',
					])
                                        
<!--                                 @include('components.forms.input', [
					    'input_label' => __('On Site Pembayaran Percent %'),
					    'id' => 'on_site_payment_percent',
					    'name' => 'on_site_payment_percent',
					])-->
		      		
                                        
                                
				
					
		      		
					
					
					
                      <button type="reset" id="file_reset" style="display:none">
                      
                      @push('scripts')
                         <script type="text/javascript">
			    jQuery(document).ready(function($) {
                                                                    
                                    $(document).on('click', '[id^="kemaskini_"]', function(event) {					
					//alert('onclick didalam');
                                        //var status = $(event.target).attr('id');
                                        var i = Number(this.id.slice(10));
                                       // alert(i);
                                         $('#id').val(i);
                                         $('#on_site_payment_percent').val('');
                                         $('#on_site_payment_percent').val('');
                                         
                                         var route_name = 'api.contract.acquisition.monitor.showcontractprogress'; 
                                         axios.get(route(route_name, i)).then(response => {
                                              //TODO set here
                                              //alert(response.data.id);
                                              //alert(''+response.data.data.siteVisitData);
                                              //alert(response.data);site_visit
                                              //alert(response.data.site_visit);
                                              
                                            
                                              //alert(response.data.contractProgress.id);
                                              try { 
                                                        $('#site_visit_percent').val(''+response.data.siteVisit.percentage_completed); 
                                               }catch(err) {}
                                               
                                               try {
                                                    if(response.data.contractProgress.on_site_payment_percent===null){
                                                       $('#on_site_payment_percent').val('0.00');
                                                    }else{
                                                        $('#on_site_payment_percent').val(''+response.data.contractProgress.on_site_payment_percent);
                                                    }
                                               }catch(err) {}
                                                      
                                                
                                              
                                              
                                             
                                              
                                        });
                                         
                                         
                                         
                                         //$('#file_reset').trigger('click');
                                         
	                           });   
                                   
                                   
                                    //post 
                             $(document).on('click', '.update-physical-save-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				var data = new FormData();

				$("#update-physical-form").each(function(){
				    $.each($(this).find(':input'), function(index, val) {
				    	 data.append($(val).prop('name'), $(val).val());
                                         
				    });
				});

				//data.append('document', $('#payment-monitor-form input[type=file]')[0].files[0]); 
                                //alert(data);
				axios.post(           
						route('api.contract.acquisition.monitor.contract-progress.store'), 
						data,
						{ headers: { 'Content-Type': 'multipart/form-data' } }
					).then(function(response) {
						//$('#payment-monitor-table').DataTable().ajax.reload();
						swal('{!! __('Penilaian Pembayaran') !!}', response.data.message, 'success');
                                                
                                                //alert(location);
                                                //
                                                  //window.location(location.concat('#physical'));
                                                 //location.reload(location.concat('#physical'));
                                                //location.reload();
                                                //todo, click button physical to open the page
                                                $( "#update-physical-progress-modal" ).click();
                                                //redirect main page
                                               

                                                
					});
                                        
                                        
                                 });
                             });
                          </script>
    @endpush
                      
                      
                      
		      	</form>
		    </div>
		    <div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		</div>
	@endslot
	@slot('modal_footer')
		<button class="btn btn-primary update-physical-save-action-btn">{{ __('Simpan') }}</button> 
	@endslot
@endcomponent
