@includeWhen(!is_null($bon), 'contract.post.monitoring.partials.eot.bon-bank')
@includeWhen(!is_null($bon), 'contract.post.monitoring.partials.eot.bon-insurance')
@includeWhen(!is_null($bon), 'contract.post.monitoring.partials.eot.bon-cheque')