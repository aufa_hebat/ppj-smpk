<h5>Compensation</h5>

@component('components.table')
	@slot('tbody')
		<tr>
			<th>Insurance Name</th>
			<td>{{ $insurance->compensation_insurance->name }}</td>
		</tr>
		<tr>
			<th>Policy No</th>
			<td>{{ $insurance->compensation_policy_no }}</td>
		</tr>
		<tr>
			<th>Policy Amount</th>
			<td>{{ money()->toHuman($insurance->compensation_policy_amount ?? 0) }}</td>
		</tr>
		<tr>
			<th>Received Date</th>
			<td>{{ $insurance->compensation_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Start Date</th>
			<td>{{ $insurance->compensation_start_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Expired Date</th>
			<td>{{ $insurance->compensation_expired_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Proposed Date</th>
			<td>{{ $insurance->compensation_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Proposed Date</th>
			<td>{{ $insurance->compensation_approval_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Received Date</th>
			<td>{{ $insurance->compensation_approval_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent