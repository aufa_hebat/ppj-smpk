<h5>Insurance</h5>

@component('components.table')
	@slot('tbody')
		<tr>
			<th>Insurance Name</th>
			<td>{{ $bon->insurance->name }}</td>
		</tr>
		<tr>
			<th>Insurance No</th>
			<td>{{ $bon->insurance_no }}</td>
		</tr>
		<tr>
			<th>Amount</th>
			<td>{{ money()->toHuman($bon->insurance_amount ?? 0) }}</td>
		</tr>
		<tr>
			<th>Received Date</th>
			<td>{{ $bon->insurance_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Start Date</th>
			<td>{{ $bon->insurance_start_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Expired Date</th>
			<td>{{ $bon->insurance_expired_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Proposed Date</th>
			<td>{{ $bon->insurance_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Proposed Date</th>
			<td>{{ $bon->insurance_approval_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Received Date</th>
			<td>{{ $bon->insurance_approval_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent