@includeWhen(!is_null($insurance), 'contract.post.monitoring.partials.eot.insurance-public')
@includeWhen(!is_null($insurance), 'contract.post.monitoring.partials.eot.insurance-work')
@includeWhen(!is_null($insurance), 'contract.post.monitoring.partials.eot.insurance-compensation')