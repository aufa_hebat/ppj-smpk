@component('components.table')
	@slot('thead')
		<tr>
			<th>Alasan</th>
			<th>Klausa</th>
			<th>Tambahan Masa</th>
		</tr>
	@endslot
	@slot('tbody')
		@foreach($details as $detail)
			<tr>
				<td>{{ $detail->reasons }}</td>
				<td>{{ $detail->clause }}</td>
				<td>{{ $detail->extended_period }}</td>
			</tr>
		@endforeach
	@endslot
@endcomponent