@component('components.table')
	@slot('tbody')
		<tr>
			<th>Bil. Meeting</th>
			<td>{{ $approve->bil_meeting }}</td>
		</tr>
		<tr>
			<th>Meeting Date</th>
			<td>{{ $approve->meeting_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Tempoh</th>
			<td>{{ $approve->period_label }}</td>
		</tr>
		<tr>
			<th>Status Kelulusan</th>
			<td>{{ $approve->approval_label }}</td>
		</tr>
		<tr>
			<th>Approved End Date</th>
			<td>{{ $approve->approved_end_date->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent