<h5>Work</h5>

@component('components.table')
	@slot('tbody')
		<tr>
			<th>Insurance Name</th>
			<td>{{ $insurance->work_insurance->name }}</td>
		</tr>
		<tr>
			<th>Policy No</th>
			<td>{{ $insurance->work_policy_no }}</td>
		</tr>
		<tr>
			<th>Policy Amount</th>
			<td>{{ money()->toHuman($insurance->work_policy_amount ?? 0) }}</td>
		</tr>
		<tr>
			<th>Received Date</th>
			<td>{{ $insurance->work_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Start Date</th>
			<td>{{ $insurance->work_start_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Expired Date</th>
			<td>{{ $insurance->work_expired_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Proposed Date</th>
			<td>{{ $insurance->work_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Proposed Date</th>
			<td>{{ $insurance->work_approval_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Received Date</th>
			<td>{{ $insurance->work_approval_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent