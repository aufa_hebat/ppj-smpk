<h5>Bank</h5>

@component('components.table')
	@slot('tbody')
		<tr>
			<th>Bank Name</th>
			<td>{{ $bon->bank->name }}</td>
		</tr>
		<tr>
			<th>Bank No</th>
			<td>{{ $bon->bank_no }}</td>
		</tr>
		<tr>
			<th>Amount</th>
			<td>{{ money()->toHuman($bon->bank_amount ?? 0) }}</td>
		</tr>
		<tr>
			<th>Received Date</th>
			<td>{{ $bon->bank_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Start Date</th>
			<td>{{ $bon->bank_start_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Expired Date</th>
			<td>{{ $bon->bank_expired_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Proposed Date</th>
			<td>{{ $bon->bank_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Proposed Date</th>
			<td>{{ $bon->bank_approval_proposed_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Approval Received Date</th>
			<td>{{ $bon->bank_approval_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent