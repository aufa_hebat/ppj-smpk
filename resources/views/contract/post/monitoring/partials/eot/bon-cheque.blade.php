<h5>Cheque</h5>

@component('components.table')
	@slot('tbody')
		<tr>
			<th>Cheque Name</th>
			<td>{{ $bon->cheque_bank->name }}</td>
		</tr>
		<tr>
			<th>Cheque No</th>
			<td>{{ $bon->cheque_no }}</td>
		</tr>
		<tr>
			<th>Cheque Receipt No</th>
			<td>{{ $bon->cheque_resit_no }}</td>
		</tr>
		<tr>
			<th>Submitted Date</th>
			<td>{{ $bon->cheque_submitted_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Received Date</th>
			<td>{{ $bon->cheque_received_date->format(config('datetime.dashboard')) }}</td>
		</tr>
		<tr>
			<th>Amount</th>
			<td>{{ money()->toHuman($bon->money_amount ?? 0) }}</td>
		</tr>
		<tr>
			<th>Money Received At</th>
			<td>{{ $bon->money_received_at->format(config('datetime.dashboard')) }}</td>
		</tr>
	@endslot
@endcomponent