@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            /*$('#approval_title').val('{!! $approval->file_reference . ' ' . $approval->title !!}').attr('readonly',true);*/
            $('#approval_title').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$approval->file_reference . ' ' . $approval->title) !!}').attr('readonly',true);
            $('#approval_reference').val('{!! $approval->reference !!}').attr('readonly',true);
            $('#approval_description').val('{!! $approval->description !!}').attr('readonly',true);
            $('#acquisition_type').val('{!! optional($approval['type'])->name !!}').attr('readonly',true);
            $('#period_length').val('{!! $approval->period_length or '-' . ' ' . optional($approval['period_type'])->name !!}').attr('readonly',true);

            $('#est_cost_project').val('{{ money()->toCommon((is_null($approval->est_cost_project) ? 0 : $approval->est_cost_project), true) }}');
            
            @if($approval->cidb == true)
                $('#cidb').val('Lembaga Pembangunan Industri Pembinaan (CIDB)').attr('readonly',true);
            @endif
            @if($approval->ssm == true)
                $('#ssm').val('Suruhanjaya Syarikat Malaysia (SSM)').attr('readonly',true);
            @endif
            @if($approval->pp == true)
                $('#pp').val('Bumiputera').attr('readonly',true);
            @endif
            @if($approval->bpku_bumiputera == true)
                $('#bpku_bumiputera').val('Bahagian Pembangunan Kontraktor dan Usahawan (BPKU)').attr('readonly',true);
            @endif
            @if($approval->spkk == true)
                $('#spkk').val('Sijil Perolehan Kerja Kerajaan (SPKK)').attr('readonly',true);
            @endif

            @if(!empty($approval->prepared_at))
                $('#prepared_date').val('{{ $approval->prepared_at->format("d/m/Y") }}').attr('readonly',true);
            @endif

            @if(!empty($approval->verified_at))
                $('#verify_date').val('{{ $approval->verified_at->format("d/m/Y") }}').attr('readonly',true);
            @endif

            @if(!empty($approval->approved_at))
                $('#approve_date').val('{{ $approval->approved_at->format("d/m/Y") }}').attr('readonly',true);
            @endif

            $('#prepare_name').val('{!! $approval->user->name !!}').attr('readonly',true);
            $('#verifier_name').val('{!! $approval->user->supervisor->name or "-" . ' ' . $approval->title !!}').attr('readonly',true);
            $('#finance_officer').val('{!! $approval->financeOfficer->name or "-" !!}').attr('readonly',true);
            $('#authority_id').val('{!! $approval->authority->name or "-" !!}').attr('readonly',true);
            $('#dataTable').DataTable();
        });
    </script>
@endpush
 @include('contract.pre.partials.shows.acquisition-details', ['approval' => $approval])
 @include('contract.pre.partials.shows.qualification-details', ['approval' => $approval])
 {{-- TODO: Fixed on empty result --}}
 @include('contract.pre.partials.shows.financial-details', ['hashslug' => $approval->hashslug])
 @include('contract.pre.partials.shows.approval-details', ['hashslug' => $approval->hashslug])
