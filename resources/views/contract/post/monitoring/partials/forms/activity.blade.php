
<script>
function myFunctionacquisition_activity_sample() {
  window.open("/downloadx/acquisition_activity_sample");
}
</script>

<form action="{{ route('acquisition.monitoring.update', $sst->hashslug) }}" method="post" enctype="multipart/form-data">
	@csrf
	@method('PUT')
	@include('components.forms.file', [
		'input_label' => '',
		'id' => 'activity',
		'name' => 'activity',
	])
        
         <input type="hidden" name="upload_type" value="activity" />
        
	<span class="text-secondary">
            Sila gunakan <strong><a onclick="myFunctionacquisition_activity_sample()" href="/downloadx/acquisition_activity_sample">lampiran</a></strong> ini
	</span>
	<br>
	
	<button class="btn btn-primary float-right">
	    @icon('fe fe-upload')
	    {{ __('Muatnaik') }}
	</button>
</form>