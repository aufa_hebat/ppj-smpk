<script>
function myFunctionacquisition_progress_sample() {
  window.open("/downloadx/acquisition_progress_sample");
}
</script>

<form action="{{ route('acquisition.monitoring.update', $sst->hashslug ) }}" method="post" enctype="multipart/form-data">
	@csrf
	@method('PUT')
	@include('components.forms.file', [
		'input_label' => '',
		'id' => 'progress_chart',
		'name' => 'progress_chart',
	])
        
        <input type="hidden" name="upload_type" value="progress_chart" />
        
	<span class="text-secondary">
            Sila gunakan <strong><a onclick="myFunctionacquisition_progress_sample()" href="">lampiran</a></strong> ini
	</span>
	<br>
	
	<button class="btn btn-primary float-right">
	    @icon('fe fe-upload')
	    {{ __('Muatnaik') }}
	</button>
</form>