<a class="btn btn-sm btn-default show-action-btn" style="cursor: pointer;"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
	<i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
</a>