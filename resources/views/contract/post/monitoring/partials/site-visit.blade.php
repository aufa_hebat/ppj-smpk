


<h3 class="mb-5">
	Lawatan Penilaian Tapak
	<!--@include('components.modals.button', [
		'modal_btn_classes' => 'create-action-btn float-right',
		'label' => __('Lawatan Penilaian Tapak Baru'),
		'icon' => 'fe fe-plus',
		'id' => 'site-visit-modal',
                'clickedId' => 'site-visit-modal-clicked-id',
	])-->
        
        <button class="btn btn-primary float-right kod-bajet" id="site-visit-button"
	@include('components.tooltip', ['tooltip' => __('Lawatan Penilaian Tapak Baru')])
  	data-toggle="modal" data-target="#site-visit-modal"
  	<i class="fe fe-plus"></i>
  	Lawatan Penilaian Tapak Baru
</button>
</h3>

@include('contract.post.monitoring.partials.site-visit.upload')
@include('contract.post.monitoring.partials.site-visit.content')


@component('components.modals.base', [
	'id' => 'site-visit-modal-view',
	'tooltip' => __('Lawatan Penilaian Tapak'),
	'modal_title' => __('Lawatan Penilaian Tapak'),
	])
	@slot('modal_body')
        
        <div class="row">
			<div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		    <div class="col-md-12 col-lg-12 col-xl-12">
                        
                       @include('components.forms.input', [
					    'input_label' => __('Tarikh Lawatan'),
					    'id' => 'visit_date_view',
					    'name' => 'visit_date_view',
                                            'readonly' => true,
					])     
                        
                         @include('components.forms.input', [
					    'input_label' => __('Tarikh (Carta)'),
					    'id' => 'visited_at_view',
					    'name' => 'visited_at_view',
                                            'readonly' => true,
					])    
                        
                         @include('components.forms.input', [
					    'input_label' => __('Perkara'),
					    'id' => 'name_view',
					    'name' => 'name_view',
                                            'readonly' => true,
					])
                                        
                                        @include('components.forms.textarea', [
					    'input_label' => __('Ulasan Penilaian'),
					    'id' => 'remarks_view',
					    'name' => 'remarks_view',
                                            'readonly' => true,
					])
                                        
                                          @include('components.forms.input', [
					    'input_label' => __('Pegawai Hadir'),
					    'id' => 'visited_by_view',
					    'name' => 'visited_by_view',
                                            'readonly' => true,
					])    
		                              
                                @include('components.forms.input', [
					    'input_label' => __('Peratusan Siap %'),
					    'id' => 'site_visit_percent_view',
					    'name' => 'site_visit_percent_view',
                                            'readonly' => true,
					])
                                        
                                       
                                        
                                       
                                        
<!--                                     @include('components.forms.input', [
					    'input_label' => __('Lampiran'),
					    'id' => 'document_view',
					    'name' => 'document_view',
                                            'readonly' => true,
					])    -->
                                        
                                        
                                        
                                        
                                        </div>
            
            </div>

        
        @endslot
	@slot('modal_footer')
		
	@endslot
@endcomponent