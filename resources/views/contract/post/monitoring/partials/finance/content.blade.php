<nav>
  <div class="nav nav-tabs justify-content-end border-0" id="nav-tab" role="tablist">
    <!-- comment , no graph here <a class="nav-item nav-link active m-3" id="nav-site-visit-list-tab" data-toggle="tab" href="#nav-site-visit-list" role="tab" aria-controls="nav-site-visit-list" aria-selected="true">@icon('fe fe-list') Jadual</a>
    <a class="nav-item nav-link m-3" id="nav-site-visit-graph-tab" data-toggle="tab" href="#nav-site-visit-graph" role="tab" aria-controls="nav-site-visit-graph" aria-selected="false">@icon('fe fe-bar-chart-2') Graf</a>
   -->
    </div>
</nav>
<div class="tab-content mb-3" id="nav-finance-content">
  <div class="tab-pane fade show active mt-5" id="nav-finance-list" role="tabpanel" aria-labelledby="nav-finance-list-tab">
	@include('contract.post.monitoring.partials.finance.list')
  </div>
  <div class="tab-pane fade mt-5" id="nav-finance-graph" role="tabpanel" aria-labelledby="nav-finance-graph-tab">
	@include('contract.post.monitoring.partials.finance.chart')
  </div>
</div>