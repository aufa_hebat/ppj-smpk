@push('scripts')
	<script>
		jQuery(document).ready(function($) {
			$(document).on('click', '.payment-monitor-save-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				var data = new FormData();

				$("#payment-monitor-form").each(function(){
				    $.each($(this).find(':input'), function(index, val) {
				    	 data.append($(val).prop('name'), $(val).val());
				    });
				});

				//data.append('document', $('#payment-monitor-form input[type=file]')[0].files[0]); 

				axios.post(
						route('api.contract.acquisition.monitor.payment-monitor.store'), 
						data,
						{ headers: { 'Content-Type': 'multipart/form-data' } }
					).then(function(response) {
						$('#payment-monitor-table').DataTable().ajax.reload();
						swal('{!! __('Penilaian Pembayaran') !!}', response.data.message, 'success');
					});
			});

			$(document).on('click', '.destroy-action-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				var id = $(this).data('hashslug');
				swal({
				  title: '{!! __('Amaran') !!}',
				  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonText: '{!! __('Ya') !!}',
				  cancelButtonText: '{!! __('Batal') !!}'
				}).then((result) => {
				  if (result.value) {
					axios.delete(
						route('api.contract.acquisition.monitor.payment-monitor.destroy', id), 
						{'_method' : 'DELETE'})
						.then(response => {
							$('#payment-monitor-table').DataTable().ajax.reload();
							swal('{{ __('Padam Penilaian Pembayaran') }}', response.data.message, 'success');
						}).catch(error => console.error(error));
				  }
				});
			});
		});
	</script>
@endpush