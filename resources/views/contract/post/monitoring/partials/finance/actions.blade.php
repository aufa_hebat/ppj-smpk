<a class="btn btn-sm btn-default destroy-action-btn" style="cursor: pointer;"
	data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
	<i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
</a>