
@component('components.datatable', 
    [
        'table_id' => 'payment-monitor-table',
        'route_name' => 'api.datatable.contract.acquisition.monitoring.payment-monitor',
        'param' => [
            'hashslug' => $sst->hashslug,
        ],
        'columns' => [
            ['data' => null , 'name' => __('Bil'), 'searchable' => false, 'orderable' => false],
            ['data' => 'payment_at', 'title' => __('Dibayar Pada'), 'defaultContent' => '-'],
            ['data' => 'payment_percent', 'title' => __('Peratusan Bayar(%)'), 'defaultContent' => '-'],
            
            
            ['data' => 'visited_at', 'title' => __('Tarikh dan Masa'), 'defaultContent' => '-'],
            ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
        ],
        'headers' => [
           __('Bil'), __('Dibayar Pada'), 
           __('Peratusan Bayar(%)'), 
           __('Tarikh dan Masa'), __('table.action')
        ],
        'actions' => minify(view('contract.post.monitoring.partials.finance.actions')->render()),
        'columnDefs' => minify(view('contract.post.monitoring.partials.finance.columnDefs')->render())
    ]
)
@endcomponent