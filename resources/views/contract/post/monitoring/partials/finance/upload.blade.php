@include('contract.post.monitoring.partials.finance.scripts')

@component('components.modals.base', [
	'id' => 'payment-monitor-modal',
	'tooltip' => __('Lawatan Penilaian Tapak'),
	'modal_title' => __('Lawatan Penilaian Tapak'),
	])
	@slot('modal_body')
		<div class="row">
			<div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		    <div class="col-md-12 col-lg-12 col-xl-12">
		      	<form id="payment-monitor-form" method="POST" enctype="multipart/form-data">
                            
                                
                                @include('components.forms.select', [
					    'input_label' => __('Tarikh Pantau'),
					    'id' => 'id_contract_progress',
					    'name' => 'id_contract_progress',
					    'options' => $contractProgresses->pluck('on_plan_date', 'id')->toArray()
					])
                            
		      		
					
					@include('components.forms.hidden', [
					    'value' => $sst->acquisition->id,
					    'id' => 'acquisition_id',
					    'name' => 'acquisition_id'
					])
					
		      							
					
					@include('components.forms.input', [
					    'input_label' => __('Peratusan Bayar (%)'),
					    'id' => 'payment_percent',
					    'name' => 'payment_percent',
					    'type' => 'number'
					])
					
                     
                      
                      @push('scripts')
                         <script type="text/javascript">
			    jQuery(document).ready(function($) {

                                $(document).on('click', '#payment-monitor-button', function(event) {					
                                        $('#id_contract_progress').val("");
                                        $('#payment_percent').val("");
	                           });                                  
                             });
                          </script>
    @endpush
                      
                      
                      
		      	</form>
		    </div>
		    <div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		</div>
	@endslot
	@slot('modal_footer')
		<button class="btn btn-primary payment-monitor-save-action-btn">{{ __('Simpan') }}</button> 
	@endslot
@endcomponent