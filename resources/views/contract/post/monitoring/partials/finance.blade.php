


<h5>Senarai Sijil Interim</h5>
<div class="row justify-content-center">
    <div class="col-12" >
      
        @component('components.card')
            @slot('card_body')
            <div class="table-responsive">
                <table id="table_owner" class="table table-sm table-transparent table-hover">
                    <thead>
                        <th>NO SIJIL INTERIM</th>
                        <th>PEMBAYARAN UNTUK</th>
                        <th>AMAUN TUNTUTAN (RM)</th>
                        <th>DOKUMEN ID</th>
                        <th>BAUCER ID</th>
                        <th>STATUS BAYARAN</th>
<!--                        <th>TINDAKAN</th>-->
                    </thead>
                    @if(!empty($sst->ipc))
                        @foreach($sst->ipc as $k => $ipc)
                        @php
                        $status_remark = '';
                        
                        @endphp
                        <tbody>
                            <tr>
                                <td>{{ $ipc->ipc_no }}</td>
                                <td>{{ $ipc->short_text }}</td>
                                <td>{{ money()->toCommon($ipc->demand_amount ?? 0,2) }}</td>
                                <td>
                                    @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                                        @foreach($ipc->ipcInvoice as $inv)
                                            {{ $inv->sap_doc_data }}&nbsp;&nbsp;
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                                        @foreach($ipc->ipcInvoice as $inv)
                                            {{ $inv->sap_bank_no }}&nbsp;&nbsp;
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($ipc->ipcInvoice) && $ipc->ipcInvoice->count() > 0)
                                        @foreach($ipc->ipcInvoice as $inv)
                                            @if (2 == $inv->status)
                                                Semakan
                                            @elseif (3 == $inv->status)
                                                Sedang diproses di SAP
                                            @elseif (4 == $inv->status)
                                                Sudah Dibayar
                                            @else
                                                Draf
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
<!--                                <td><div class="text-center">
                                        <div class="item-action dropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                                              <i class="fe fe-more-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                                              style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                
                                                @if($ipc->status == 1)
                                                @role('penyedia')
                                                <a class="dropdown-item edit-action-btn" href="{{ route('contract.post.ipc.ipc_invoice_edit',['hashslug' => $ipc->hashslug]) }}" data-backdrop="static">
                                                    <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                                </a>
                                                @if(empty($review))
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item destroy-ipc-action-btn"
                                                        data-hashslug="{{ $ipc->hashslug }}">
                                                        <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
                                                    </a>
                                                @endif
                                                @endrole
                                                @else
                                                <a class="dropdown-item edit-action-btn" href="{{ route('contract.post.ipc.ipc_show',['hashslug' => $ipc->hashslug]) }}" data-backdrop="static">
                                                    <i class="fe fe-edit text-primary"></i> {{ __('Butiran') }}
                                                </a>
                                                @endif
                                          </div>
                                        </div>
                                    </div></td>-->
                            </tr>
                        </tbody>
                        @endforeach
                    @endif
                </table>
            </div>
            @endslot                
        @endcomponent
    </div>
</div>

