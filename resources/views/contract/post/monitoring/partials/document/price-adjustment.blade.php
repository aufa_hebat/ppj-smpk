<h5>Pelarasan Harga</h5>
<br>
<div id="table" class="table-editable table-bordered border-0">
    @foreach($BQElemen as $bq)
    <div class="mb-0 card" >
        <div class="card-header" role="tab" id="heading{{$bq->id}}">
            <h4 class="card-title cardElemen">
                <a class=" withripple" role="button" data-toggle="collapse" data-parent="#accordion{{$bq->id}}" href="#collapse{{$bq->id}}" aria-expanded="true" aria-controls="collapse{{$bq->id}}">
                    <i class="mr-2 fa fa-folder-open"></i><u>{{$bq->no}}&nbsp;:&nbsp;{{$bq->element}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($bq->amount ?? 0,2 )}}</u>
                </a>
            </h4>
        </div>
        <div id="collapse{{$bq->id}}" class="card-collapse collapse " role="tabpanel" aria-labelledby="heading{{$bq->id}}" >
            <div class="card-body">
                <div class="row">
                    <div id="semakanBq" class="col-md-12 table-responsive">
                        <p>@if(!empty($bq->description)) <u>{{ $bq->description }}</u> @endif</p>
                        <table class="table1  table-no-border" width="100%" cellspacing="0">
                            @foreach($BQItem as $it)
                            @if($bq->no == $it->bq_no_element && $bq->sst_id == $it->sst_id)
                            <thead >
                                <th style="width:100%;border:none" >
                                    <h5>{{$it->bq_no_element}}.{{$it->no}}&nbsp;:&nbsp;{{$it->item}} &nbsp;&nbsp;&nbsp;RM{{money()->toCommon($it->amount ?? 0,2 )}}</h5><br>
                                    <p>@if(!empty($it->description)) {{ $it->description }} @endif</p>
                                </th>
                            </thead>
                            <tbody class="list">
                                <tr style="width:100%;border:none">
                                    <td class="ulasanBQs">
                                        <table id="ulasan_bq{{$it->id}}" class="table table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <th style="width:5%">NO SUB ITEM</th>
                                                <th style="width:30%">SUB ITEM</th>
                                                <th style="width:25%">KETERANGAN</th>
                                                <th style="width:10%">UNIT</th>
                                                <th style="width:10%">KUANTITI</th>
                                                <th style="width:10%">HARGA SEUNIT</th>
                                                <th style="width:10%">AMAUN</th>
                                                <th style="width:10%">HARGA SEUNIT TERLARAS</th>
                                                <th style="width:10%">AMAUN TERLARAS</th>
                                            </thead>
                                            <tbody>
                                            @foreach($BQSub as $key=>$bqs)
                                            @if($bqs->bq_no_element == $bq->no  && $bqs->sst_id == $bq->sst_id && $bqs->bq_no_item == $it->no)
                                            <tr>
                                                <td>{{$bqs->bq_no_element}}.{{$bqs->bq_no_item}}.{{$bqs->no}}</td>
                                                <td>{{$bqs->item}}</td>
                                                <td>{{$bqs->description}}</td>
                                                <td>{{$bqs->unit}}</td>
                                                <td>{{money()->toCommon($bqs->quantity ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->rate_per_unit ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->amount ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->rate_per_unit_adjust ?? 0,2 )}}</td>
                                                <td>{{money()->toCommon($bqs->amount_adjust ?? 0,2 )}}</td>
                                            </tr>
                                            @push('scripts')
                                            <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function($) {
                                                    $("#ulasan_bq{{$it->id}}").DataTable();
                                                });
                                            </script>
                                            @endpush
                                            @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<br>
