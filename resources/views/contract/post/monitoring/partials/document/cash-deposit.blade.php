@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $('#info').show();
            $('#bon').hide();
            $('#depositJaminanBank').hide();
            $('#depositJaminanInsuran').hide();
            $('#uploads_deposit').hide();

            @if(!empty($deposit))
                @if( $deposit->bon_type == 1 )
                $('#depositJaminanBank').show();
                @elseif($deposit->bon_type == 2)
                $('#depositJaminanInsuran').show();
                @endif

            // onload fields

            $('#deposit_bon_type').html('{{ $deposit->type ? $deposit->type->description : null }}');

            $('#deposit_reference_no').html('{{ $deposit->reference_no }}');
            $('#deposit_prime_cost').html('{{ $deposit->prime_cost ? money()->toHuman($deposit->prime_cost) : null }}');
            $('#deposit_amount').html('{{ $deposit->amount ? money()->toHuman($deposit->amount) : null }}');
            $('#deposit_total_amount').html('{{ $deposit->total_amount ? money()->toHuman($deposit->total_amount) : null }}');
            $('#deposit_qualified_amount').html('{{ $deposit->qualified_amount ? money()->toHuman($deposit->qualified_amount) : null }}');

            $('#deposit_bank_name').html('{{ $deposit->bank ? $deposit->bank->name : null }}');
            $('#deposit_bank_no').html('{{ $deposit->bank_no }}');
            $('#deposit_bank_approval_received_date').html('{{ $deposit->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_approval_received_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_received_date').html('{{ $deposit->bank_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_received_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_approval_proposed_date').html('{{ $deposit->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_approval_proposed_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_amount').html('{{ $deposit->bank_amount ? money()->toHuman($deposit->bank_amount) : null }}');
            $('#deposit_bank_start_date').html('{{ $deposit->bank_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_start_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_expired_date').html('{{ $deposit->bank_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_expired_date)->format('d/m/Y'):null }}');
            $('#deposit_bank_proposed_date').html('{{ $deposit->bank_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->bank_proposed_date)->format('d/m/Y'):null }}');

            $('#deposit_insurance_name').html('{{ $deposit->insurance ? $deposit->insurance->name : null }}');
            $('#deposit_insurance_no').html('{{ $deposit->insurance_no }}');
            $('#deposit_insurance_approval_received_date').html('{{ $deposit->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_approval_received_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_received_date').html('{{ $deposit->insurance_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_received_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_approval_proposed_date').html('{{ $deposit->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_approval_proposed_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_amount').html('{{ $deposit->insurance_amount ? money()->toHuman($deposit->insurance_amount) : null }}');
            $('#deposit_insurance_start_date').html('{{ $deposit->insurance_start_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_start_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_expired_date').html('{{ $deposit->insurance_expired_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_expired_date)->format('d/m/Y'):null }}');
            $('#deposit_insurance_proposed_date').html('{{ $deposit->insurance_proposed_date ? \Carbon\Carbon::createFromFormat('Y-m-d',$deposit->insurance_proposed_date)->format('d/m/Y'):null }}');

            $('#uploads_deposit').show();
            @if(optional($deposit->documents)->count() > 0)
                var t_deposit = $('#tblUpload_deposit').DataTable({
                searching: false,
                ordering: false,
                paging: false,
                info:false
            });

            var counter_deposit = 1;
            @foreach($deposit->documents as $doc)
            t_deposit.row.add( [
                counter_deposit,
                '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
            ] ).draw( false );

            counter_deposit++;
            @endforeach
            @endif

            @endif

            $('input[type=radio][name=deposit_type]').change(function () {

                if(this.value == '1'){
                    $('#info').show();
                    $('#bon').hide();
                }
                else if (this.value == '2'){
                    $('#info').hide();
                    $('#bon').show();
                }
            });

        });


    </script>
@endpush

<h4>Wang Pendahuluan</h4>
<br>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <div class="selectgroup w-100">
                <label class="selectgroup-item">
                    <input type="radio" name="deposit_type" value="1" class="selectgroup-input" checked>
                    <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Wang Pendahuluan') }}</span>
                </label>
                <label class="selectgroup-item">
                    <input type="radio" name="deposit_type" value="2" class="selectgroup-input">
                    <span class="selectgroup-button selectgroup-button-icon">{{ __('Maklumat Bon') }}</span>
                </label>
            </div>
        </div>

        <div id="info">
            @include('contract.post.partials.shows.deposit.info')
        </div>

        <div id="bon">
            <div class="form-group">
                <div class="text-muted">{{ __('Jenis Bon') }}</div>
                <div id="deposit_bon_type"></div>
            </div>

            @include('contract.post.partials.shows.deposit.jaminanBank')
            @include('contract.post.partials.shows.deposit.jaminanInsuran')
            @include('contract.post.partials.shows.deposit.uploads')
         
        </div>
    </div>
</div>