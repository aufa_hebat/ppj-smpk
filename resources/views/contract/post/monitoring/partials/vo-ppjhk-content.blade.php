

<!-- pelarasan harga kontrak , buat table from PPJHK - ialah pelarasan harga yang telah diluluskan -->

@component('components.table')

        @slot('thead')	
		<tr>
			<th>PPJHK No</th>
                        <th>Net Amount</th>
		</tr>
	@endslot
    
	@slot('tbody')
		@forelse($ppjhk as $key => $obj)
			<tr>
				<td>{{ $obj->no }} </td> 
                                
                                
                                
                                <td>{{ money()->toHuman( $obj->elements->pluck('net_amount')->sum() + $obj->cancels->pluck('amount')->sum() ?? "0",2) }} </td>
			</tr>	
		@empty
			<tr class="alert alert-warning text-center">
				<td colspan="10">
					{{ __('Tiada.') }}
				</td>
			</tr>
		@endforelse
	@endslot
@endcomponent
