@component('components.collapse.content', ['id' => $vo->hashslug, 'container_id' => 'variation-order'])
	@slot('header')
		No. {{ $vo->no }}
	@endslot
	@slot('body')
		<p><strong>Dijana pada:</strong> {{ $vo->created_at->format(config('datetime.dashboard')) }}</p>
		<p><strong>Dikemaskini pada:</strong> {{ $vo->updated_at->format(config('datetime.dashboard')) }}</p>
		@foreach($vo->categories as $category)
			<strong>{{ $category->variationOrderType->name }}</strong>
			@component('components.table')
				@slot('thead')
					<tr>
						<th>Item</th>
						<th>Sebab Diperlukan</th>
						<th>Anggaran Amaun Tambahan</th>
						<th>Anggaran Peratusan Tambahan</th>
						<th>Anggaran Amaun Pengurangan</th>
						<th>Anggaran Peratusan Pengurangan</th>
					</tr>
				@endslot
				@slot('tbody')
					@foreach($category->elements as $element)
						<tr>
							<td>{{ $element->name }}</td>
							<td>{{ $element->reason_required }}</td>
							<td class="text-center">{{ money()->toCommon($element->estimated_additional_amount ?? 0) }}</td>
							<td class="text-center">{{ $element->estimated_additional_percentage }}%</td>
							<td class="text-center">{{ money()->toCommon($element->estimated_reduced_amount ?? 0) }}</td>
							<td class="text-center">{{ $element->estimated_reduced_percentage }}%</td>
						</tr>
					@endforeach
				@endslot
			@endcomponent
		@endforeach
	@endslot
@endcomponent