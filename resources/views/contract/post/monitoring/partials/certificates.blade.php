<h3>Sijil-sijil</h3>

<h5>CPC</h5>

@if($sst->cpc)
	<ul>
		<li><strong>Tarikh Mula DLP</strong> - {{ optional($sst->cpc->dlp_start_date)->format('d/m/Y') }}</li>
		<li><strong>Tarikh Tamat DLP</strong> - {{ optional($sst->cpc->dlp_expiry_date)->format('d/m/Y') }}</li>
		<li><strong>Tarikh Tandatangan</strong> - {{ optional($sst->cpc->signature_date)->format('d/m/Y') }}</li>
		<li><strong>Tarikh Pentauliahan</strong> - {{ optional($sst->cpc->test_award_date)->format('d/m/Y') }}</li>
	</ul>
@else
	<p class="text-primary text-center"> Tiada sijil CPC</p>
@endif

<h5>CMGD</h5>

@if($sst->cmgd)
	<ul>
		<li><strong>Tarikh Tandatangan</strong> - {{ optional($sst->cmgd->signature_date)->format('d/m/Y') }}</li>
		<li><strong>Tarikh Lawatan Tapak</strong> - {{ optional($sst->cmgd->site_visit)->format('d/m/Y') }}</li>
		<li><strong>Tarikh MGD</strong> - {{ optional($sst->cmgd->mgd_date)->format('d/m/Y') }}</li>
	</ul>
@else
	<p class="text-primary text-center"> Tiada sijil CMGD</p>
@endif

<h5>CNC</h5>

@if($sst->cnc)
	<ul>
		{{-- <li><strong>LAD Amount</strong> - {{ $sst->cnc->lad_amount ? money()->toHuman($sst->cnc->lad_amount):'RM 0.00' }}</li> --}}
		<li><strong>Kadar Ganti Rugi Sehari</strong> - {{ $sst->cnc->lad_per_day ? money()->toHuman($sst->cnc->lad_per_day):'RM 0.00' }}</li>
		<li><strong>Tarikh Tandatangan</strong> - {{ optional($sst->cnc->signature_date)->format('d/m/Y') }}</li>
		<li><strong>Tarikh Ujian Pentauliahan</strong> - {{ optional($sst->cnc->test_award_date)->format('d/m/Y') }}</li>
		<li><strong>Klausa 1</strong> - {{ $sst->cnc->clause_1 }}</li>
		<li><strong>Klausa 2</strong> - {{ $sst->cnc->clause_2 }}</li>
	</ul>
@else
	<p class="text-primary text-center"> Tiada sijil CNC</p>
@endif

<h5>CPO</h5>

@if($sst->cpo)
	<ul>
		<li><strong>Estimation Amount</strong> - {{ money()->toHuman($sst->cpo->estimation_amount ?? 0) }}</li>
		<li><strong>CPO Amount</strong> - {{ money()->toHuman($sst->cpo->cpo_amount ?? 0) }}</li>
		<li><strong>Lad / Day</strong> - {{ money()->toHuman($sst->cpo->lad_per_day ?? 0) }}</li>
		<li><strong>Agreement Date</strong> - {{ optional($sst->cpo->agreement_date)->format('d/m/Y') }}</li>
		<li><strong>Partial Date</strong> - {{ optional($sst->cpo->partial_date)->format('d/m/Y') }}</li>
		<li><strong>Assessment Date</strong> - {{ optional($sst->cpo->assessment_date)->format('d/m/Y') }}</li>
		<li><strong>CPO Start Date</strong> - {{ optional($sst->cpo->cpo_start_date)->format('d/m/Y') }}</li>
		<li><strong>CPO End Date</strong> - {{ optional($sst->cpo->cpo_end_date)->format('d/m/Y') }}</li>
	</ul>
@else
	<p class="text-primary text-center"> Tiada sijil CPO</p>
@endif
