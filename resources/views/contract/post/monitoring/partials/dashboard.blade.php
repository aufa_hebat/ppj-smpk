@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
           @foreach(chart()->monitoring() as $key => $chart)
                @if(!empty($chart))
                    createChart(@json($chart), '{{ snake_case($key) }}');
                @endif
           @endforeach
        });
    </script>
@endpush

<div class="row">
    @forelse(dashboard()->monitoring() as $item)
        <div class="col-6">
            @component('components.cards.figure')
                @slot('content')
                    {{ $item['content'] }}
                @endslot
                @slot('footer')
                    {{ $item['footer'] }}
                    @if(!empty($item['data']))
                        <div class="m-3 text-left">
                            @foreach($item['data'] as $label => $url)
                                <a target="_blank" class="tag tag-primary m-1" href="{{ $url }}">{{ $label }}</a>
                            @endforeach
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    @empty
        <div class="col">
            @include('components.cards.empty-info', ['header' => 'Maklumat Pemantauan Projek'])
        </div>
    @endforelse
</div>

<!--<div class="row">
    @forelse(chart()->monitoring() as $key => $chart)
        @if(!empty($chart))
            <div class="col-6">
                @include('components.charts.box', ['chart_id' => snake_case($key)])
            </div>
        @endif
    @empty
        <div class="col">
            @include('components.cards.empty-info', ['header' => 'Maklumat Pemantauan Projek'])
        </div>
    @endforelse
</div>-->