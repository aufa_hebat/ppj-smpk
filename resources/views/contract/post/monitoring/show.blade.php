@extends('layouts.admin')

@push('scripts')
    @include('components.charts.utils')
    <script type="text/javascript" src="{{ asset('js/previous_next.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            
            //var new_url = window.location;
             //alert('{{$sst->hashslug}}');
            
            $( "#physical_link" ).click(function() {
             //alert( "Handler for .click() called." );
             //location.reload();
             
            // window.location = new_url;
             //alert(new_url);
             //var str2 = '';
            // var concat = new_url.concat(str2);
             //alert(concat);
            // alert(new_url);
             window.location = '{{$sst->hashslug}}';
             
            });

            //tab remain stay after refresh
            $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');
            //tab remain stay after refresh
        });
    </script>
@endpush

@section('content')
    @include('components.pages.title')
   
    <div class="row" ref="msgContainer" id="tab_button">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
                     
                 <li class="list-group-item">
                    <a class="list-group-item-action active" id="physical_link" data-toggle="tab" href="#physical" role="tab" aria-controls="physical" aria-selected="false">
                        @icon('fe fe-bar-chart-2')&nbsp;{{ __('Fizikal') }}
                    </a>
                </li>
                
                 <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#site-visit" role="tab" aria-controls="site-visit" aria-selected="false">
                        @icon('fe fe-map-pin')&nbsp;{{ __('Lawatan Penilaian Tapak') }}
                    </a>
                </li>
                
                <!--
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="false">
                        @icon('fe fe-search')&nbsp;{{ __('Perinci') }}
                    </a>
                </li>-->
                
                  <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#certificates" role="tab" aria-controls="certificates" aria-selected="false">
                        @icon('fe fe-award')&nbsp;{{ __('Sijil-sijil') }}
                    </a>
                </li>
                
                 <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#finance" role="tab" aria-controls="finance" aria-selected="false">
                        @icon('fe fe-dollar-sign')&nbsp;{{ __('Pembayaran') }}
                    </a>
                </li>
                
                <!--
                  <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#lad" role="tab" aria-controls="lad" aria-selected="false">
                        @icon('fe fe-list')&nbsp;{{ __('LAD') }}
                    </a>
                </li>-->
                
                 <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#eot" role="tab" aria-controls="eot" aria-selected="false">
                        @icon('fe fe-trending-up')&nbsp;{{ __('EOT') }}
                    </a>
                </li> 
                
                 <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#variation-order" role="tab" aria-controls="variation-order" aria-selected="false">
                        @icon('fe fe-sliders')&nbsp;{{ __('Perubahan Kerja') }}
                    </a>
                </li>
                
                 <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#documentx" role="tab" aria-controls="document" aria-selected="false">
                        @icon('fe fe-file-minus')&nbsp;{{ __('Dokumen') }}
                    </a>
                </li>
                
                 <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#sst" role="tab" aria-controls="sst" aria-selected="false">
                        @icon('fe fe-list')&nbsp;{{ __('SST') }}
                    </a>
                </li>
                
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#acquisition-approval" role="tab" aria-controls="acquisition-approval" aria-selected="false">
                        @icon('fe fe-check')&nbsp;{{ __('Kelulusan') }}
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-10">
            @component('components.card')
                @slot('card_body')
                    @component('components.tab.container', ['id' => 'document' ])
                        @slot('tabs')
                        
                          @component('components.tab.content', ['id' => 'physical' , 'active' => true])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.physical')
                                @endslot
                            @endcomponent
                            
                             @component('components.tab.content', ['id' => 'site-visit'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.site-visit')
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'certificates'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.certificates')
                                @endslot
                            @endcomponent
                            
                            @component('components.tab.content', ['id' => 'finance'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.finance')
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'eot'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.eot')
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'variation-order'])
                                @slot('content')
                                     @include('contract.post.monitoring.partials.variation-order') 
                                    {{--@include('components.cards.wip')--}}
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'documentx'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.document')
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'sst'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.sst')
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'acquisition-approval'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.acquisition-approval')
                                @endslot
                            @endcomponent

                         	{{-- @component('components.tab.content', ['id' => 'details'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.details')
                                @endslot
                            @endcomponent

                            @component('components.tab.content', ['id' => 'lad'])
                                @slot('content')
                                    @include('contract.post.monitoring.partials.lad')
                                @endslot
                            @endcomponent --}}
                        @endslot
                    @endcomponent
                    {{-- @component('components.tab.button')
                    @endcomponent --}}
                @endslot
            @endcomponent
        </div>
    </div>
@endsection




