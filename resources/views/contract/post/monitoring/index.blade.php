@extends('layouts.admin')

@push('scripts')
    @include('components.charts.utils')
     <script src="{{asset('js/hideShowPassword.min.js')}}"></script>
@endpush

  @push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
           @foreach(chart()->monitoring() as $key => $chart)
                @if(!empty($chart))
                    createChart(@json($chart), '{{ snake_case($key) }}');
                @endif
           @endforeach
           
           
            $('#est_cost_project').hidePassword(true);
           
        });
    </script>
   
@endpush

@section('content')




<div class="row">
<!--    @forelse(dashboard()->monitoring() as $item)
        <div class="col-6">
            @component('components.cards.figure')
                @slot('content')
                    {{ $item['content'] }}
                @endslot
                @slot('footer')
                    {{ $item['footer'] }}
                    @if(!empty($item['data']))
                        <div class="m-3 text-left">
                            @foreach($item['data'] as $label => $url)
                                <a target="_blank" class="tag tag-primary m-1" href="{{ $url }}">{{ $label }}</a>
                            @endforeach
                        </div>
                    @endif
                @endslot
            @endcomponent
        </div>
    @empty
        <div class="col">
            @include('components.cards.empty-info', ['header' => 'Maklumat Pemantauan Projek'])
        </div>
    @endforelse-->

<div class="col-6">
@component('components.cards.figure')
    @slot('content')
     {{$contractsNotAwardYet}}
    @endslot
    @slot('footer') 
    <a class="tag tag-primary m-1" href="{{ route('acquisition.acquisition.index') }}?tag=contractsNotAwardYet">
      Perolehan Belum Di Award Lagi
      </a>
       <div class="m-3 text-left">                         
<!-- if want link one by one 
<a target="_blank" class="tag tag-primary m-1" href="http://google.com">109</a>-->
      </div>
    @endslot
   @endcomponent
 </div>

<div class="col-6">
@component('components.cards.figure')
    @slot('content')
     {{$contractsNotSignedYet}}
    @endslot
    @slot('footer')
    <a class="tag tag-primary m-1" href="{{ route('acceptance-letter.index') }}?tag=contractsNotSignedYet">
        Kontrak Belum Di Tandatangan
    </a>
       <div class="m-3 text-left">                         
<!-- if want link one by one 
<a target="_blank" class="tag tag-primary m-1" href="http://google.com">109</a>-->
      </div>
    @endslot
   @endcomponent
 </div>

 <div class="col-6">
@component('components.cards.figure')
    @slot('content')
     {{$contractsOnGoing}}
    @endslot
    @slot('footer')
    <a class="tag tag-primary m-1" href="{{ route('acceptance-letter.index') }}?tag=ContractOnGoing">Kontrak Sedang Berjalan</a>
<!--      Kontrak Sedang Berjalan-->
       <div class="m-3 text-left">                         
<!-- if want link one by one 
<a target="_blank" class="tag tag-primary m-1" href="http://google.com">109</a>-->
      </div>
    @endslot
   @endcomponent
 </div>

<div class="col-6">
@component('components.cards.figure')
    @slot('content')
     {{$contractsDoneButNotSofaYet}}
    @endslot
    @slot('footer')
    <a class="tag tag-primary m-1" href="{{ route('acceptance-letter.index') }}?tag=contractsDoneButNotSofaYet">
        Kerja Telah Siap Tetapi Belum Di SOFA 
    </a>
      
       <div class="m-3 text-left">                         
<!-- if want link one by one 
<a target="_blank" class="tag tag-primary m-1" href="http://google.com">109</a>-->
      </div>
    @endslot
   @endcomponent
 </div>
   
   

 <div class="col-6">
@component('components.cards.figure')
    @slot('content')
     {{$contractsSixMonthToEnd}}
    @endslot
    @slot('footer')
      <a  class="tag tag-primary m-1" href="{{ route('acceptance-letter.index') }}?tag=6MonthToEnd">
          Kontrak akan Tamat dalam Tempoh 6 Bulan</a>
       <div class="m-3 text-left">                         
<!-- if want link one by one 
<a target="_blank" class="tag tag-primary m-1" href="http://google.com">109</a>-->
      </div>
    @endslot
   @endcomponent
 </div>



 
   

 

 
   
   
 

</div>
   



    <div class="row justify-content-center">
        <div class="col">
            @include('contract.post.monitoring.partials.scripts')
            @component('components.card')
                @slot('card_body')
                    @component('components.datatable', 
                        [
                            'table_id' => 'contract-post-monitoring',
                            'route_name' => 'api.datatable.contract.acquisition.monitoring',
                            'columns' => [
                                ['data' => 'no_kontrak', 'title' => __('No. Kontrak'), 'defaultContent' => '-'],
                                ['data' => 'tajuk', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                ['data' => 'syarikat_terpilih', 'title' => __('Syarikat Terpilih'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                            ],
                            'headers' => [
                                __('No. Kontrak'), __('Tajuk'), __('Syarikat Terpilih'), __('table.action')
                            ],
                            'actions' => minify(view('contract.post.monitoring.partials.actions')->render())
                        ]
                    )
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>
@endsection