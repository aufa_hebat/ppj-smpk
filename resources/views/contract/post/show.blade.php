@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script>
    	jQuery(document).ready(function($) {
            $('#document-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#document-tab-content a[href="' + hash + '"]').tab('show');

    		// button for review bon
            $('#bon_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#bon_saveStatus').val(),
                    acquisition_id : $('#bon_acquisition_id').val(),
                    requested_by : $('#bon_requested_by').val(),
                    progress : $('#bon_progress').val(),
                    task_by : $('#bon_task_by').val(),
                    law_task_by : $('#bon_law_task_by').val(),
                    approved_by : $('#bon_approved_by').val(),
                    created_by : $('#bon_created_by').val(),
                    approved_at : $('#bon_approved_at').val(),
                    type : $('#bons_type').val(),
                    document_contract_type : $('#bon_document_contract_type').val(),
                    status : $('#bon_status').val(),
                    department : $('#bon_department').val(),
                    remarks : $('#remarks_bon').val(),
                    bank_approval_proposed_date : $('#bank_approval_proposed_date_review').val(),
                    bank_approval_received_date : $('#bank_approval_received_date_review').val(),
                    insurance_approval_proposed_date : $('#insurance_approval_proposed_date_review').val(),
                    insurance_approval_received_date : $('#insurance_approval_received_date_review').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#bon_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#bon_sentStatus').val(),
                    acquisition_id : $('#bon_acquisition_id').val(),
                    requested_by : $('#bon_requested_by').val(),
                    progress : $('#bon_progress').val(),
                    task_by : $('#bon_task_by').val(),
                    law_task_by : $('#bon_law_task_by').val(),
                    approved_by : $('#bon_approved_by').val(),
                    created_by : $('#bon_created_by').val(),
                    approved_at : $('#bon_approved_at').val(),
                    type : $('#bons_type').val(),
                    document_contract_type : $('#bon_document_contract_type').val(),
                    status : $('#bon_status').val(),
                    department : $('#bon_department').val(),
                    remarks : $('#remarks_bon').val(),
                    bank_approval_proposed_date : $('#bank_approval_proposed_date_review').val(),
                    bank_approval_received_date : $('#bank_approval_received_date_review').val(),
                    insurance_approval_proposed_date : $('#insurance_approval_proposed_date_review').val(),
                    insurance_approval_received_date : $('#insurance_approval_received_date_review').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#bon_savDraf').hide();
                                $('#bon_savSent').hide();
                                $('#bon_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#bon_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#bon_rejectStatus').val(),
                    acquisition_id : $('#bon_acquisition_id').val(),
                    requested_by : $('#bon_requested_by').val(),
                    progress : $('#bon_progress').val(),
                    task_by : $('#bon_task_by').val(),
                    law_task_by : $('#bon_law_task_by').val(),
                    approved_by : $('#bon_approved_by').val(),
                    created_by : $('#bon_created_by').val(),
                    approved_at : $('#bon_approved_at').val(),
                    type : $('#bons_type').val(),
                    document_contract_type : $('#bon_document_contract_type').val(),
                    status : $('#bon_status').val(),
                    department : $('#bon_department').val(),
                    remarks : $('#remarks_bon').val(),
                    bank_approval_proposed_date : $('#bank_approval_proposed_date_review').val(),
                    bank_approval_received_date : $('#bank_approval_received_date_review').val(),
                    insurance_approval_proposed_date : $('#insurance_approval_proposed_date_review').val(),
                    insurance_approval_received_date : $('#insurance_approval_received_date_review').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#bon_savDraf').hide();
                                $('#bon_savSent').hide();
                                $('#bon_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review insurans
            $('#insurans_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#insurans_saveStatus').val(),
                    acquisition_id : $('#insurans_acquisition_id').val(),
                    requested_by : $('#insurans_requested_by').val(),
                    progress : $('#insurans_progress').val(),
                    task_by : $('#insurans_task_by').val(),
                    law_task_by : $('#insurans_law_task_by').val(),
                    approved_by : $('#insurans_approved_by').val(),
                    created_by : $('#insurans_created_by').val(),
                    approved_at : $('#insurans_approved_at').val(),
                    type : $('#insurans_type').val(),
                    document_contract_type : $('#insurans_document_contract_type').val(),
                    status : $('#insurans_status').val(),
                    department : $('#insurans_department').val(),
                    remarks : $('#remarks_insurans').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#insurans_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#insurans_sentStatus').val(),
                    acquisition_id : $('#insurans_acquisition_id').val(),
                    requested_by : $('#insurans_requested_by').val(),
                    progress : $('#insurans_progress').val(),
                    task_by : $('#insurans_task_by').val(),
                    law_task_by : $('#insurans_law_task_by').val(),
                    approved_by : $('#insurans_approved_by').val(),
                    created_by : $('#insurans_created_by').val(),
                    approved_at : $('#insurans_approved_at').val(),
                    type : $('#insurans_type').val(),
                    document_contract_type : $('#insurans_document_contract_type').val(),
                    status : $('#insurans_status').val(),
                    department : $('#insurans_department').val(),
                    remarks : $('#remarks_insurans').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#insurans_savDraf').hide();
                                $('#insurans_savSent').hide();
                                $('#insurans_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#insurans_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#insurans_rejectStatus').val(),
                    acquisition_id : $('#insurans_acquisition_id').val(),
                    requested_by : $('#insurans_requested_by').val(),
                    progress : $('#insurans_progress').val(),
                    task_by : $('#insurans_task_by').val(),
                    law_task_by : $('#insurans_law_task_by').val(),
                    approved_by : $('#insurans_approved_by').val(),
                    created_by : $('#insurans_created_by').val(),
                    approved_at : $('#insurans_approved_at').val(),
                    type : $('#insurans_type').val(),
                    document_contract_type : $('#insurans_document_contract_type').val(),
                    status : $('#insurans_status').val(),
                    department : $('#insurans_department').val(),
                    remarks : $('#remarks_insurans').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#insurans_savDraf').hide();
                                $('#insurans_savSent').hide();
                                $('#insurans_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review deposit
            $('#deposit_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#deposit_saveStatus').val(),
                    acquisition_id : $('#deposit_acquisition_id').val(),
                    requested_by : $('#deposit_requested_by').val(),
                    progress : $('#deposit_progress').val(),
                    task_by : $('#deposit_task_by').val(),
                    law_task_by : $('#deposit_law_task_by').val(),
                    approved_by : $('#deposit_approved_by').val(),
                    created_by : $('#deposit_created_by').val(),
                    approved_at : $('#deposit_approved_at').val(),
                    type : $('#deposits_type').val(),
                    document_contract_type : $('#deposit_document_contract_type').val(),
                    status : $('#deposit_status').val(),
                    department : $('#deposit_department').val(),
                    remarks : $('#remarks_wang').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#deposit_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#deposit_sentStatus').val(),
                    acquisition_id : $('#deposit_acquisition_id').val(),
                    requested_by : $('#deposit_requested_by').val(),
                    progress : $('#deposit_progress').val(),
                    task_by : $('#deposit_task_by').val(),
                    law_task_by : $('#deposit_law_task_by').val(),
                    approved_by : $('#deposit_approved_by').val(),
                    created_by : $('#deposit_created_by').val(),
                    approved_at : $('#deposit_approved_at').val(),
                    type : $('#deposits_type').val(),
                    document_contract_type : $('#deposit_document_contract_type').val(),
                    status : $('#deposit_status').val(),
                    department : $('#deposit_department').val(),
                    remarks : $('#remarks_wang').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#deposit_savDraf').hide();
                                $('#deposit_savSent').hide();
                                $('#deposit_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#deposit_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#deposit_rejectStatus').val(),
                    acquisition_id : $('#deposit_acquisition_id').val(),
                    requested_by : $('#deposit_requested_by').val(),
                    progress : $('#deposit_progress').val(),
                    task_by : $('#deposit_task_by').val(),
                    law_task_by : $('#deposit_law_task_by').val(),
                    approved_by : $('#deposit_approved_by').val(),
                    created_by : $('#deposit_created_by').val(),
                    approved_at : $('#deposit_approved_at').val(),
                    type : $('#deposits_type').val(),
                    document_contract_type : $('#deposit_document_contract_type').val(),
                    status : $('#deposit_status').val(),
                    department : $('#deposit_department').val(),
                    remarks : $('#remarks_wang').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#deposit_savDraf').hide();
                                $('#deposit_savSent').hide();
                                $('#deposit_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review doc
            $('#doc_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#doc_saveStatus').val(),
                    acquisition_id : $('#doc_acquisition_id').val(),
                    requested_by : $('#doc_requested_by').val(),
                    progress : $('#doc_progress').val(),
                    task_by : $('#doc_task_by').val(),
                    law_task_by : $('#doc_law_task_by').val(),
                    approved_by : $('#doc_approved_by').val(),
                    created_by : $('#doc_created_by').val(),
                    approved_at : $('#doc_approved_at').val(),
                    type : $('#doc_type').val(),
                    document_contract_type : $('#doc_document_contract_type').val(),
                    status : $('#doc_status').val(),
                    department : $('#doc_department').val(),
                    remarks : $('#remarks_doc').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#doc_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#doc_sentStatus').val(),
                    acquisition_id : $('#doc_acquisition_id').val(),
                    requested_by : $('#doc_requested_by').val(),
                    progress : $('#doc_progress').val(),
                    task_by : $('#doc_task_by').val(),
                    law_task_by : $('#doc_law_task_by').val(),
                    approved_by : $('#doc_approved_by').val(),
                    created_by : $('#doc_created_by').val(),
                    approved_at : $('#doc_approved_at').val(),
                    type : $('#doc_type').val(),
                    document_contract_type : $('#doc_document_contract_type').val(),
                    status : $('#doc_status').val(),
                    department : $('#doc_department').val(),
                    remarks : $('#remarks_doc').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#doc_savDraf').hide();
                                $('#doc_savSent').hide();
                                $('#doc_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#doc_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#doc_rejectStatus').val(),
                    acquisition_id : $('#doc_acquisition_id').val(),
                    requested_by : $('#doc_requested_by').val(),
                    progress : $('#doc_progress').val(),
                    task_by : $('#doc_task_by').val(),
                    law_task_by : $('#doc_law_task_by').val(),
                    approved_by : $('#doc_approved_by').val(),
                    created_by : $('#doc_created_by').val(),
                    approved_at : $('#doc_approved_at').val(),
                    type : $('#doc_type').val(),
                    document_contract_type : $('#doc_document_contract_type').val(),
                    status : $('#doc_status').val(),
                    department : $('#doc_department').val(),
                    remarks : $('#remarks_doc').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#doc_savDraf').hide();
                                $('#doc_savSent').hide();
                                $('#doc_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review cpc
            $('#cpc_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#cpc_saveStatus').val(),
                    acquisition_id : $('#cpc_acquisition_id').val(),
                    requested_by : $('#cpc_requested_by').val(),
                    progress : $('#cpc_progress').val(),
                    task_by : $('#cpc_task_by').val(),
                    law_task_by : $('#cpc_law_task_by').val(),
                    approved_by : $('#cpc_approved_by').val(),
                    created_by : $('#cpc_created_by').val(),
                    approved_at : $('#cpc_approved_at').val(),
                    type : $('#cpc_type').val(),
                    document_contract_type : $('#cpc_document_contract_type').val(),
                    status : $('#cpc_status').val(),
                    department : $('#cpc_department').val(),
                    remarks : $('#remarks_cpc').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cpc_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#cpc_sentStatus').val(),
                    acquisition_id : $('#cpc_acquisition_id').val(),
                    requested_by : $('#cpc_requested_by').val(),
                    progress : $('#cpc_progress').val(),
                    task_by : $('#cpc_task_by').val(),
                    law_task_by : $('#cpc_law_task_by').val(),
                    approved_by : $('#cpc_approved_by').val(),
                    created_by : $('#cpc_created_by').val(),
                    approved_at : $('#cpc_approved_at').val(),
                    type : $('#cpc_type').val(),
                    document_contract_type : $('#cpc_document_contract_type').val(),
                    status : $('#cpc_status').val(),
                    department : $('#cpc_department').val(),
                    remarks : $('#remarks_cpc').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#cpc_savDraf').hide();
                                $('#cpc_savSent').hide();
                                $('#cpc_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cpc_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#cpc_rejectStatus').val(),
                    acquisition_id : $('#cpc_acquisition_id').val(),
                    requested_by : $('#cpc_requested_by').val(),
                    progress : $('#cpc_progress').val(),
                    task_by : $('#cpc_task_by').val(),
                    law_task_by : $('#cpc_law_task_by').val(),
                    approved_by : $('#cpc_approved_by').val(),
                    created_by : $('#cpc_created_by').val(),
                    approved_at : $('#cpc_approved_at').val(),
                    type : $('#cpc_type').val(),
                    document_contract_type : $('#cpc_document_contract_type').val(),
                    status : $('#cpc_status').val(),
                    department : $('#cpc_department').val(),
                    remarks : $('#remarks_cpc').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#cpc_savDraf').hide();
                                $('#cpc_savSent').hide();
                                $('#cpc_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review cnc
            $('#cnc_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#cnc_saveStatus').val(),
                    acquisition_id : $('#cnc_acquisition_id').val(),
                    requested_by : $('#cnc_requested_by').val(),
                    progress : $('#cnc_progress').val(),
                    task_by : $('#cnc_task_by').val(),
                    law_task_by : $('#cnc_law_task_by').val(),
                    approved_by : $('#cnc_approved_by').val(),
                    created_by : $('#cnc_created_by').val(),
                    approved_at : $('#cnc_approved_at').val(),
                    type : $('#cnc_type').val(),
                    document_contract_type : $('#cnc_document_contract_type').val(),
                    status : $('#cnc_status').val(),
                    department : $('#cnc_department').val(),
                    remarks : $('#remarks_cnc').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cnc_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#cnc_sentStatus').val(),
                    acquisition_id : $('#cnc_acquisition_id').val(),
                    requested_by : $('#cnc_requested_by').val(),
                    progress : $('#cnc_progress').val(),
                    task_by : $('#cnc_task_by').val(),
                    law_task_by : $('#cnc_law_task_by').val(),
                    approved_by : $('#cnc_approved_by').val(),
                    created_by : $('#cnc_created_by').val(),
                    approved_at : $('#cnc_approved_at').val(),
                    type : $('#cnc_type').val(),
                    document_contract_type : $('#cnc_document_contract_type').val(),
                    status : $('#cnc_status').val(),
                    department : $('#cnc_department').val(),
                    remarks : $('#remarks_cnc').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#cnc_savDraf').hide();
                                $('#cnc_savSent').hide();
                                $('#cnc_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cnc_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#cnc_rejectStatus').val(),
                    acquisition_id : $('#cnc_acquisition_id').val(),
                    requested_by : $('#cnc_requested_by').val(),
                    progress : $('#cnc_progress').val(),
                    task_by : $('#cnc_task_by').val(),
                    law_task_by : $('#cnc_law_task_by').val(),
                    approved_by : $('#cnc_approved_by').val(),
                    created_by : $('#cnc_created_by').val(),
                    approved_at : $('#cnc_approved_at').val(),
                    type : $('#cnc_type').val(),
                    document_contract_type : $('#cnc_document_contract_type').val(),
                    status : $('#cnc_status').val(),
                    department : $('#cnc_department').val(),
                    remarks : $('#remarks_cnc').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#cnc_savDraf').hide();
                                $('#cnc_savSent').hide();
                                $('#cnc_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review cpo
            $('#cpo_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#cpo_saveStatus').val(),
                    acquisition_id : $('#cpo_acquisition_id').val(),
                    requested_by : $('#cpo_requested_by').val(),
                    progress : $('#cpo_progress').val(),
                    task_by : $('#cpo_task_by').val(),
                    law_task_by : $('#cpo_law_task_by').val(),
                    approved_by : $('#cpo_approved_by').val(),
                    created_by : $('#cpo_created_by').val(),
                    approved_at : $('#cpo_approved_at').val(),
                    type : $('#cpo_type').val(),
                    document_contract_type : $('#cpo_document_contract_type').val(),
                    status : $('#cpo_status').val(),
                    department : $('#cpo_department').val(),
                    remarks : $('#remarks_cpo').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cpo_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#cpo_sentStatus').val(),
                    acquisition_id : $('#cpo_acquisition_id').val(),
                    requested_by : $('#cpo_requested_by').val(),
                    progress : $('#cpo_progress').val(),
                    task_by : $('#cpo_task_by').val(),
                    law_task_by : $('#cpo_law_task_by').val(),
                    approved_by : $('#cpo_approved_by').val(),
                    created_by : $('#cpo_created_by').val(),
                    approved_at : $('#cpo_approved_at').val(),
                    type : $('#cpo_type').val(),
                    document_contract_type : $('#cpo_document_contract_type').val(),
                    status : $('#cpo_status').val(),
                    department : $('#cpo_department').val(),
                    remarks : $('#remarks_cpo').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#cpo_savDraf').hide();
                                $('#cpo_savSent').hide();
                                $('#cpo_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cpo_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#cpo_rejectStatus').val(),
                    acquisition_id : $('#cpo_acquisition_id').val(),
                    requested_by : $('#cpo_requested_by').val(),
                    progress : $('#cpo_progress').val(),
                    task_by : $('#cpo_task_by').val(),
                    law_task_by : $('#cpo_law_task_by').val(),
                    approved_by : $('#cpo_approved_by').val(),
                    created_by : $('#cpo_created_by').val(),
                    approved_at : $('#cpo_approved_at').val(),
                    type : $('#cpo_type').val(),
                    document_contract_type : $('#cpo_document_contract_type').val(),
                    status : $('#cpo_status').val(),
                    department : $('#cpo_department').val(),
                    remarks : $('#remarks_cpo').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#cpo_savDraf').hide();
                                $('#cpo_savSent').hide();
                                $('#cpo_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            // button for review cmgd
            $('#cmgd_savDraf').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    saveStatus : $('#cmgd_saveStatus').val(),
                    acquisition_id : $('#cmgd_acquisition_id').val(),
                    requested_by : $('#cmgd_requested_by').val(),
                    progress : $('#cmgd_progress').val(),
                    task_by : $('#cmgd_task_by').val(),
                    law_task_by : $('#cmgd_law_task_by').val(),
                    approved_by : $('#cmgd_approved_by').val(),
                    created_by : $('#cmgd_created_by').val(),
                    approved_at : $('#cmgd_approved_at').val(),
                    type : $('#cmgd_type').val(),
                    document_contract_type : $('#cmgd_document_contract_type').val(),
                    status : $('#cmgd_status').val(),
                    department : $('#cmgd_department').val(),
                    remarks : $('#remarks_cmgd').val()
                };
                swal({
                    title: '{!! __('Simpan') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cmgd_savSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    sentStatus : $('#cmgd_sentStatus').val(),
                    acquisition_id : $('#cmgd_acquisition_id').val(),
                    requested_by : $('#cmgd_requested_by').val(),
                    progress : $('#cmgd_progress').val(),
                    task_by : $('#cmgd_task_by').val(),
                    law_task_by : $('#cmgd_law_task_by').val(),
                    approved_by : $('#cmgd_approved_by').val(),
                    created_by : $('#cmgd_created_by').val(),
                    approved_at : $('#cmgd_approved_at').val(),
                    type : $('#cmgd_type').val(),
                    document_contract_type : $('#cmgd_document_contract_type').val(),
                    status : $('#cmgd_status').val(),
                    department : $('#cmgd_department').val(),
                    remarks : $('#remarks_cmgd').val()
                };
                swal({
                    title: '{!! __('Hantar') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                            // if(response.data == 'success'){

                                $('#cmgd_savDraf').hide();
                                $('#cmgd_savSent').hide();
                                $('#cmgd_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            $('#cmgd_rejSent').click(function(){
                var route_name = 'acquisition.review.store';
                var data = {
                    rejectStatus : $('#cmgd_rejectStatus').val(),
                    acquisition_id : $('#cmgd_acquisition_id').val(),
                    requested_by : $('#cmgd_requested_by').val(),
                    progress : $('#cmgd_progress').val(),
                    task_by : $('#cmgd_task_by').val(),
                    law_task_by : $('#cmgd_law_task_by').val(),
                    approved_by : $('#cmgd_approved_by').val(),
                    created_by : $('#cmgd_created_by').val(),
                    approved_at : $('#cmgd_approved_at').val(),
                    type : $('#cmgd_type').val(),
                    document_contract_type : $('#cmgd_document_contract_type').val(),
                    status : $('#cmgd_status').val(),
                    department : $('#cmgd_department').val(),
                    remarks : $('#remarks_cmgd').val()
                };
                swal({
                    title: '{!! __('Kuiri') !!}',
                    text: '{!! __('Adakah anda pasti?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        axios.post(route(route_name), data).then(response => {
                                                   
                            // if(response.data == 'success'){

                                $('#cmgd_savDraf').hide();
                                $('#cmgd_savSent').hide();
                                $('#cmgd_rejSent').hide();
                                redirect(route('home'));
                                //
                                //go to
                            // }else{
                            //     swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            // }
                                                                
                         });
                    }
                });
            });

            @if(!empty($review->approved_at))
            $('#requested_at').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$review->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1) && $s1 = $review)
                @if($s1->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1->remarks) !!}');
                @endif
            @elseif(!empty($s2) && $s2 = $review)
                @if($s2->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2->remarks) !!}');
                @endif
            @elseif(!empty($s3) && $s3 = $review)
                @if($s3->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3->remarks) !!}');
                @endif
            @elseif(!empty($s4) && $s4 = $review)
                @if($s4->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4->remarks) !!}');
                @endif
            @elseif(!empty($s5) && $s5 = $review)
                @if($s5->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5->remarks) !!}');
                @endif
            @elseif(!empty($s6) && $s6 = $review)
                @if($s6->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6->remarks) !!}');
                @endif
            @elseif(!empty($s7) && $s7 = $review)
                @if($s7->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7->remarks) !!}');
                @endif
            @elseif(!empty($cn) && $cn = $review)
                @if($cn->reviews_status == 'Deraf')
                    $('#remarks').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cn->remarks) !!}');
                @endif
            @endif

            // bon
            @if(!empty($reviewbon->approved_at))
            $('#requested_at_bon').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewbon->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_bon').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($bon))
            $('#bank_approval_received_date_review').val('{{ $bon->bank_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_approval_received_date)->format('d/m/Y'):null }}');
            $('#bank_approval_proposed_date_review').val('{{ $bon->bank_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->bank_approval_proposed_date)->format('d/m/Y'):null }}');

            $('#insurance_approval_received_date_review').val('{{ $bon->insurance_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_approval_received_date)->format('d/m/Y'):null }}');
            $('#insurance_approval_proposed_date_review').val('{{ $bon->insurance_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$bon->insurance_approval_proposed_date)->format('d/m/Y'):null }}');
            @endif

            @if(!empty($s1bon) && $s1bon = $reviewbon)
                @if($s1bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1bon->remarks) !!}');
                @endif
            @elseif(!empty($s2bon) && $s2bon = $reviewbon)
                @if($s2bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2bon->remarks) !!}');
                @endif
            @elseif(!empty($s3bon) && $s3bon = $reviewbon)
                @if($s3bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3bon->remarks) !!}');
                @endif
            @elseif(!empty($s4bon) && $s4bon = $reviewbon)
                @if($s4bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4bon->remarks) !!}');
                @endif
            @elseif(!empty($s5bon) && $s5bon = $reviewbon)
                @if($s5bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5bon->remarks) !!}');
                @endif
            @elseif(!empty($s6bon) && $s6bon = $review)
                @if($s6bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6bon->remarks) !!}');
                @endif
            @elseif(!empty($s7bon) && $s7bon = $review)
                @if($s7bon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7bon->remarks) !!}');
                @endif
            @elseif(!empty($cnbon) && $cnbon = $reviewbon)
                @if($cnbon->reviews_status == 'Deraf')
                    $('#remarks_bon').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cnbon->remarks) !!}');
                @endif
            @endif

            // insurans
            @if(!empty($reviewinsurans->approved_at))
            $('#requested_at_insurans').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewinsurans->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_insurans').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($insurance))
            $('#public_approval_received_date_review').val('{{ $insurance->public_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_approval_received_date)->format('d/m/Y'):null }}');
            $('#public_approval_proposed_date_review').val('{{ $insurance->public_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->public_approval_proposed_date)->format('d/m/Y'):null }}');

            $('#work_approval_received_date_review').val('{{ $insurance->work_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_approval_received_date)->format('d/m/Y'):null }}');
            $('#work_approval_proposed_date_review').val('{{ $insurance->work_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->work_approval_proposed_date)->format('d/m/Y'):null }}');

            $('#compensation_approval_received_date_review').val('{{ $insurance->compensation_approval_received_date? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_approval_received_date)->format('d/m/Y'):null }}');
            $('#compensation_approval_proposed_date_review').val('{{ $insurance->compensation_approval_proposed_date? \Carbon\Carbon::createFromFormat('Y-m-d',$insurance->compensation_approval_proposed_date)->format('d/m/Y'):null }}');
            @endif

            @if(!empty($s1insurans) && $s1insurans = $reviewinsurans)
                @if($s1insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1insurans->remarks) !!}');
                @endif
            @elseif(!empty($s2insurans) && $s2insurans = $reviewinsurans)
                @if($s2insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2insurans->remarks) !!}');
                @endif
            @elseif(!empty($s3insurans) && $s3insurans = $reviewinsurans)
                @if($s3insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3insurans->remarks) !!}');
                @endif
            @elseif(!empty($s4insurans) && $s4insurans = $reviewinsurans)
                @if($s4insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4insurans->remarks) !!}');
                @endif
            @elseif(!empty($s5insurans) && $s5insurans = $reviewinsurans)
                @if($s5insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5insurans->remarks) !!}');
                @endif
            @elseif(!empty($s6insurans) && $s6insurans = $review)
                @if($s6insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6insurans->remarks) !!}');
                @endif
            @elseif(!empty($s7insurans) && $s7insurans = $review)
                @if($s7insurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7insurans->remarks) !!}');
                @endif
            @elseif(!empty($cninsurans) && $cninsurans = $reviewinsurans)
                @if($cninsurans->reviews_status == 'Deraf')
                    $('#remarks_insurans').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cninsurans->remarks) !!}');
                @endif
            @endif

            // wang pendahuluan
            @if(!empty($reviewwang->approved_at))
            $('#requested_at_wang').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewwang->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_wang').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1wang) && $s1wang = $reviewwang)
                @if($s1wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1wang->remarks) !!}');
                @endif
            @elseif(!empty($s2wang) && $s2wang = $reviewwang)
                @if($s2wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2wang->remarks) !!}');
                @endif
            @elseif(!empty($s3wang) && $s3wang = $reviewwang)
                @if($s3wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3wang->remarks) !!}');
                @endif
            @elseif(!empty($s4wang) && $s4wang = $reviewwang)
                @if($s4wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4wang->remarks) !!}');
                @endif
            @elseif(!empty($s5wang) && $s5wang = $reviewwang)
                @if($s5wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5wang->remarks) !!}');
                @endif
            @elseif(!empty($s6wang) && $s6wang = $review)
                @if($s6wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6wang->remarks) !!}');
                @endif
            @elseif(!empty($s7wang) && $s7wang = $review)
                @if($s7wang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7wang->remarks) !!}');
                @endif
            @elseif(!empty($cnwang) && $cnwang = $reviewwang)
                @if($cnwang->reviews_status == 'Deraf')
                    $('#remarks_wang').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cnwang->remarks) !!}');
                @endif
            @endif

            // dokumen kontract
            @if(!empty($reviewdoc->approved_at))
            $('#requested_at_doc').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewdoc->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_doc').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1doc) && $s1doc = $reviewdoc)
                @if($s1doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1doc->remarks) !!}');
                @endif
            @elseif(!empty($s2doc) && $s2doc = $reviewdoc)
                @if($s2doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2doc->remarks) !!}');
                @endif
            @elseif(!empty($s3doc) && $s3doc = $reviewdoc)
                @if($s3doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3doc->remarks) !!}');
                @endif
            @elseif(!empty($s4doc) && $s4doc = $reviewdoc)
                @if($s4doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4doc->remarks) !!}');
                @endif
            @elseif(!empty($s5doc) && $s5doc = $reviewdoc)
                @if($s5doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5doc->remarks) !!}');
                @endif
            @elseif(!empty($s6doc) && $s6doc = $review)
                @if($s6doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6doc->remarks) !!}');
                @endif
            @elseif(!empty($s7doc) && $s7doc = $review)
                @if($s7doc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7doc->remarks) !!}');
                @endif
            @elseif(!empty($cndoc) && $cndoc = $reviewdoc)
                @if($cndoc->reviews_status == 'Deraf')
                    $('#remarks_doc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cndoc->remarks) !!}');
                @endif
            @endif

            // cpc
            @if(!empty($reviewcpc->approved_at))
            $('#requested_at_cpc').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewcpc->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_cpc').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1cpc) && $s1cpc = $reviewcpc)
                @if($s1cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1cpc->remarks) !!}');
                @endif
            @elseif(!empty($s2cpc) && $s2cpc = $reviewcpc)
                @if($s2cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2cpc->remarks) !!}');
                @endif
            @elseif(!empty($s3cpc) && $s3cpc = $reviewcpc)
                @if($s3cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3cpc->remarks) !!}');
                @endif
            @elseif(!empty($s4cpc) && $s4cpc = $reviewcpc)
                @if($s4cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4cpc->remarks) !!}');
                @endif
            @elseif(!empty($s5cpc) && $s5cpc = $reviewcpc)
                @if($s5cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5cpc->remarks) !!}');
                @endif
            @elseif(!empty($s6cpc) && $s6cpc = $review)
                @if($s6cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6cpc->remarks) !!}');
                @endif
            @elseif(!empty($s7cpc) && $s7cpc = $review)
                @if($s7cpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7cpc->remarks) !!}');
                @endif
            @elseif(!empty($cncpc) && $cncpc = $reviewcpc)
                @if($cncpc->reviews_status == 'Deraf')
                    $('#remarks_cpc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cncpc->remarks) !!}');
                @endif
            @endif

            // cnc
            @if(!empty($reviewcnc->approved_at))
            $('#requested_at_cnc').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewcnc->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_cnc').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1cnc) && $s1cnc = $reviewcnc)
                @if($s1cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1cnc->remarks) !!}');
                @endif
            @elseif(!empty($s2cnc) && $s2cnc = $reviewcnc)
                @if($s2cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2cnc->remarks) !!}');
                @endif
            @elseif(!empty($s3cnc) && $s3cnc = $reviewcnc)
                @if($s3cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3cnc->remarks) !!}');
                @endif
            @elseif(!empty($s4cnc) && $s4cnc = $reviewcnc)
                @if($s4cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4cnc->remarks) !!}');
                @endif
            @elseif(!empty($s5cnc) && $s5cnc = $reviewcnc)
                @if($s5cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5cnc->remarks) !!}');
                @endif
            @elseif(!empty($s6cnc) && $s6cnc = $review)
                @if($s6cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6cnc->remarks) !!}');
                @endif
            @elseif(!empty($s7cnc) && $s7cnc = $review)
                @if($s7cnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7cnc->remarks) !!}');
                @endif
            @elseif(!empty($cncnc) && $cncnc = $reviewcnc)
                @if($cncnc->reviews_status == 'Deraf')
                    $('#remarks_cnc').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cncnc->remarks) !!}');
                @endif
            @endif

            // cmgd
            @if(!empty($reviewcmgd->approved_at))
            $('#requested_at_cmgd').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewcmgd->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_cmgd').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1cmgd) && $s1cmgd = $reviewcmgd)
                @if($s1cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1cmgd->remarks) !!}');
                @endif
            @elseif(!empty($s2cmgd) && $s2cmgd = $reviewcmgd)
                @if($s2cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2cmgd->remarks) !!}');
                @endif
            @elseif(!empty($s3cmgd) && $s3cmgd = $reviewcmgd)
                @if($s3cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3cmgd->remarks) !!}');
                @endif
            @elseif(!empty($s4cmgd) && $s4cmgd = $reviewcmgd)
                @if($s4cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4cmgd->remarks) !!}');
                @endif
            @elseif(!empty($s5cmgd) && $s5cmgd = $reviewcmgd)
                @if($s5cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5cmgd->remarks) !!}');
                @endif
            @elseif(!empty($s6cmgd) && $s6cmgd = $review)
                @if($s6cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6cmgd->remarks) !!}');
                @endif
            @elseif(!empty($s7cmgd) && $s7cmgd = $review)
                @if($s7cmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7cmgd->remarks) !!}');
                @endif
            @elseif(!empty($cncmgd) && $cncmgd = $reviewcmgd)
                @if($cncmgd->reviews_status == 'Deraf')
                    $('#remarks_cmgd').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cncmgd->remarks) !!}');
                @endif
            @endif

            // cpo
            @if(!empty($reviewcpo->approved_at))
            $('#requested_at_cpo').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$reviewcpo->approved_at)->format('d/m/Y g:i A')  }}');
            @endif
            $('#approved_at_cpo').val('{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}');

            @if(!empty($s1cpo) && $s1cpo = $reviewcpo)
                @if($s1cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s1cpo->remarks) !!}');
                @endif
            @elseif(!empty($s2cpo) && $s2cpo = $reviewcpo)
                @if($s2cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s2cpo->remarks) !!}');
                @endif
            @elseif(!empty($s3cpo) && $s3cpo = $reviewcpo)
                @if($s3cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s3cpo->remarks) !!}');
                @endif
            @elseif(!empty($s4cpo) && $s4cpo = $reviewcpo)
                @if($s4cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s4cpo->remarks) !!}');
                @endif
            @elseif(!empty($s5cpo) && $s5cpo = $reviewcpo)
                @if($s5cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s5cpo->remarks) !!}');
                @endif
            @elseif(!empty($s6cpo) && $s6cpo = $review)
                @if($s6cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s6cpo->remarks) !!}');
                @endif
            @elseif(!empty($s7cpo) && $s7cpo = $review)
                @if($s7cpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$s7cpo->remarks) !!}');
                @endif
            @elseif(!empty($cncpo) && $cncpo = $reviewcpo)
                @if($cncpo->reviews_status == 'Deraf')
                    $('#remarks_cpo').val('{!! str_replace(["\r\n","\r","\n"],"\\r\\n",$cncpo->remarks) !!}');
                @endif
            @endif
    	});
    </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
            <span class="font-weight-bold">Tajuk : </span>{{ $sst->acquisition->title }}
            <br>
            <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->acquisition->reference }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
            <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
            <br>
            <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if($sst->acquisition->approval->acquisition_type_id != 1 && $sst->acquisition->approval->acquisition_type_id != 3)
                <span class="font-weight-bold">Tempoh DLP : </span>{{ $sst->defects_liability_period." Bulan" }}&nbsp;&nbsp;&nbsp;&nbsp;
            @endif
            <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
            @if( !empty($sst->eots->pluck('eot_approve')->pluck('approved_end_date')->last()) )
                @if($sst->eots->count()>0)
                    <span class="font-weight-bold">Tarikh Siap Kerja Lanjutan : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->eots->pluck('eot_approve')->pluck('approved_end_date')->last())->format('d/m/Y') ?? ''}}&nbsp;&nbsp;&nbsp;&nbsp;
                @endif
            @endif
        @endslot
    @endcomponent

    <div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            {{-- Pelarasan Harga --}}
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#price-adjustment" role="tab" aria-controls="price-adjustment" aria-selected="false">
                    @icon('fe fe-sliders')&nbsp;{{ __('Pelarasan Harga') }}
                </a>
            </li>

            {{-- Sub Kontraktor --}}
            <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#sub-contract" role="tab" aria-controls="sub-contract" aria-selected="false">
                        @icon('fe fe-clipboard')&nbsp;{{ __('Sub Kontraktor') }}
                    </a>
                </li>            

            {{-- bon --}}
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#bon-implementation" role="tab" aria-controls="bon-implementation" aria-selected="false">
                    @icon('fe fe-book-open')&nbsp;{{ __('Bon Pelaksanaan') }}
                </a>
            </li>
            @if(!empty($review))
                @if(!empty($reviewbon))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-bon" role="tab" aria-controls="document-contract-reviews-bon" aria-selected="false">
                            @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Bon Pelaksanaan') }}
                        </a>
                    </li>
                    @if((user()->id == $reviewbon->approved_by && user()->id != $reviewbon->created_by && $reviewbon->type == 'Dokumen Kontrak') 
                    || (!empty($semakan3bon) && user()->id == $semakan3bon->approved_by && user()->id != $reviewbon->created_by && ($reviewlogbon->status != 'S4') && $reviewbon->status != 'Selesai') 
                    || (!empty($semakan4bon) && user()->id == $semakan4bon->approved_by && user()->id != $reviewbon->created_by && (empty($semakan5bon))) 
                    || (!empty($semakan5bon) && user()->id == $semakan5bon->approved_by && user()->id != $reviewbon->created_by && (empty($cetaknotisbpubbon))) 
                    || (!empty($semakan6bon) && user()->id == $semakan6bon->approved_by && user()->id != $reviewbon->created_by && ($reviewlogbon->status != 'S7') && $reviewbon->status != 'Selesai') 
                    || (!empty($semakan7bon) && user()->id == $semakan7bon->approved_by && user()->id != $reviewbon->created_by && ($reviewlogbon->status != 'CNUU') && $reviewbon->status != 'Selesai'))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-bon" role="tab" aria-controls="document-contract-review-bon" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Bon Pelaksanaan') }}
                            </a>
                        </li>
                    @endif
                @endif
            @endif

            {{-- Insurans --}}
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#insurance" role="tab" aria-controls="insurance" aria-selected="false">
                    @icon('fe fe-briefcase')&nbsp;{{ __('Insuran') }}
                </a>
            </li>
            @if(!empty($review))
                @if(!empty($reviewinsurans))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-insurance" role="tab" aria-controls="document-contract-reviews-insurance" aria-selected="false">
                            @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Insurans') }}
                        </a>
                    </li>
                    @if((user()->id == $reviewinsurans->approved_by && user()->id != $reviewinsurans->created_by && $reviewinsurans->type == 'Dokumen Kontrak') 
                    || (!empty($semakan3insurans) && user()->id == $semakan3insurans->approved_by && user()->id != $reviewinsurans->created_by && ($reviewloginsurans->status != 'S4') && $reviewinsurans->status != 'Selesai') 
                    || (!empty($semakan4insurans) && user()->id == $semakan4insurans->approved_by && user()->id != $reviewinsurans->created_by && (empty($semakan5insurans))) 
                    || (!empty($semakan5insurans) && user()->id == $semakan5insurans->approved_by && user()->id != $reviewinsurans->created_by && (empty($cetaknotisbpubinsurans))) 
                    || (!empty($semakan6insurans) && user()->id == $semakan6insurans->approved_by && user()->id != $reviewinsurans->created_by && ($reviewloginsurans->status != 'S7') && $reviewinsurans->status != 'Selesai') 
                    || (!empty($semakan7insurans) && user()->id == $semakan7insurans->approved_by && user()->id != $reviewinsurans->created_by && ($reviewloginsurans->status != 'CNUU') && $reviewinsurans->status != 'Selesai'))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-insurance" role="tab" aria-controls="document-contract-review-insurance" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Insurans') }}
                            </a>
                        </li>
                    @endif
                @endif
            @endif

            {{-- wang pendahuluan --}}
            @if(!empty($deposit))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#cash-deposit" role="tab" aria-controls="cash-deposit" aria-selected="false">
                        @icon('fe fe-dollar-sign')&nbsp;{{ __('Wang Pendahuluan') }}
                    </a>
                </li>
                
                @if(!empty($review))
                    @if(!empty($reviewwang))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-wang" role="tab" aria-controls="document-contract-reviews-wang" aria-selected="false">
                                @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Wang Pendahuluan') }}
                            </a>
                        </li>
                        @if((user()->id == $reviewwang->approved_by && user()->id != $reviewwang->created_by && $reviewwang->type == 'Dokumen Kontrak') 
                        || (!empty($semakan3wang) && user()->id == $semakan3wang->approved_by && user()->id != $reviewwang->created_by && ($reviewlogwang->status != 'S4') && $reviewwang->status != 'Selesai') 
                        || (!empty($semakan4wang) && user()->id == $semakan4wang->approved_by && user()->id != $reviewwang->created_by && (empty($semakan5wang))) 
                        || (!empty($semakan5wang) && user()->id == $semakan5wang->approved_by && user()->id != $reviewwang->created_by && (empty($cetaknotisbpubwang))) 
                        || (!empty($semakan6wang) && user()->id == $semakan6wang->approved_by && user()->id != $reviewwang->created_by && ($reviewlogwang->status != 'S7') && $reviewwang->status != 'Selesai') 
                        || (!empty($semakan7wang) && user()->id == $semakan7wang->approved_by && user()->id != $reviewwang->created_by && ($reviewlogwang->status != 'CNUU') && $reviewwang->status != 'Selesai'))
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-deposit" role="tab" aria-controls="document-contract-review-deposit" aria-selected="false">
                                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Wang Pendahuluan') }}
                                </a>
                            </li>
                        @endif
                    @endif
                @endif
            @endif

            {{-- dokumen kontrak --}}
            {{-- din : hidden kan link ini jika selesai semakan bpub, bon perlaksanaan dan insuran --}}
            @if(!empty($document))
                @if($isViewDocumentContract == true)
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-contract" role="tab" aria-controls="document-contract" aria-selected="false">
                            @icon('fe fe-clipboard')&nbsp;{{ __('Dokumen Kontrak') }}
                        </a>
                    </li>
                @endif
            @endif
            
            @if(!empty($review))
                @if(!empty($reviewdoc))
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-doc" role="tab" aria-controls="document-contract-reviews-doc" aria-selected="false">
                            @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Dokumen Kontrak') }}
                        </a>
                    </li>
                    @if((user()->id == $reviewdoc->approved_by && user()->id != $reviewdoc->created_by && $reviewdoc->type == 'Dokumen Kontrak') 
                    || (!empty($semakan3doc) && user()->id == $semakan3doc->approved_by && user()->id != $reviewdoc->created_by && ($reviewlogdoc->status != 'S4') && $reviewdoc->status != 'Selesai') 
                    || (!empty($semakan4doc) && user()->id == $semakan4doc->approved_by && user()->id != $reviewdoc->created_by && (empty($semakan5doc))) 
                    || (!empty($semakan5doc) && user()->id == $semakan5doc->approved_by && user()->id != $reviewdoc->created_by && (empty($cetaknotisbpubdoc))) 
                    || (!empty($semakan6doc) && user()->id == $semakan6doc->approved_by && user()->id != $reviewdoc->created_by && ($reviewlogdoc->status != 'S7') && $reviewdoc->status != 'Selesai') 
                    || (!empty($semakan7doc) && user()->id == $semakan7doc->approved_by && user()->id != $reviewdoc->created_by && ($reviewlogdoc->status != 'CNUU') && $reviewdoc->status != 'Selesai'))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-document" role="tab" aria-controls="document-contract-review-document" aria-selected="false">
                                @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Dokumen Kontrak') }}
                            </a>
                        </li>
                    @endif
                @endif
            @endif

            {{-- cnc --}}
            @if(!empty($cnc))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#cnc" role="tab" aria-controls="cnc" aria-selected="false">
                        @icon('fe fe-file')&nbsp;{{ __('Sijil Tidak Siap Kerja') }}
                    </a>
                </li>
                @if(!empty($review))
                    @if(!empty($reviewcnc))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-cnc" role="tab" aria-controls="document-contract-reviews-cnc" aria-selected="false">
                                @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Sijil Tidak Siap Kerja') }}
                            </a>
                        </li>
                        @if((user()->id == $reviewcnc->approved_by && user()->id != $reviewcnc->created_by && $reviewcnc->type == 'Dokumen Kontrak') 
                            || (!empty($semakan3cnc) && user()->id == $semakan3cnc->approved_by && user()->id != $reviewcnc->created_by && ($reviewlogcnc->status != 'S4') && $reviewcnc->status != 'Selesai') 
                            || (!empty($semakan4cnc) && user()->id == $semakan4cnc->approved_by && user()->id != $reviewcnc->created_by && (empty($semakan5cnc))) 
                            || (!empty($semakan5cnc) && user()->id == $semakan5cnc->approved_by && user()->id != $reviewcnc->created_by && (empty($cetaknotisbpubcnc))))
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-cnc" role="tab" aria-controls="document-contract-review-cnc" aria-selected="false">
                                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Sijil Tidak Siap Kerja') }}
                                </a>
                            </li>
                        @endif
                    @endif
                @endif  
            @endif

            {{-- cpc --}}
            @if(!empty($cpc))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#cpc" role="tab" aria-controls="cpc" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('Sijil Siap Kerja') }}
                    </a>
                </li>
                @if(!empty($review))
                    @if(!empty($reviewcpc))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-cpc" role="tab" aria-controls="document-contract-reviews-cpc" aria-selected="false">
                                @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Sijil Siap Kerja') }}
                            </a>
                        </li>
                        @if((user()->id == $reviewcpc->approved_by && user()->id != $reviewcpc->created_by && $reviewcpc->type == 'Dokumen Kontrak') 
                            || (!empty($semakan3cpc) && user()->id == $semakan3cpc->approved_by && user()->id != $reviewcpc->created_by && ($reviewlogcpc->status != 'S4') && $reviewcpc->status != 'Selesai') 
                            || (!empty($semakan4cpc) && user()->id == $semakan4cpc->approved_by && user()->id != $reviewcpc->created_by && (empty($semakan5cpc))) 
                            || (!empty($semakan5cpc) && user()->id == $semakan5cpc->approved_by && user()->id != $reviewcpc->created_by && (empty($cetaknotisbpubcpc))))
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-cpc" role="tab" aria-controls="document-contract-review-cpc" aria-selected="false">
                                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Sijil Siap Kerja') }}
                                </a>
                            </li>
                        @endif
                    @endif
                @endif
            @endif

            {{-- cmdg --}}
            @if(!empty($cmgd))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#cmgd" role="tab" aria-controls="cmgd" aria-selected="false">
                        @icon('fe fe-file-plus')&nbsp;{{ __('Sijil Siap Membaiki Kecacatan') }}
                    </a>
                </li>
                @if(!empty($review))
                    @if(!empty($reviewcmgd))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-cmgd" role="tab" aria-controls="document-contract-reviews-cmgd" aria-selected="false">
                                @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Sijil Siap Membaiki Kecacatan') }}
                            </a>
                        </li>
                        @if((user()->id == $reviewcmgd->approved_by && user()->id != $reviewcmgd->created_by && $reviewcmgd->type == 'Dokumen Kontrak') 
                            || (!empty($semakan3cmgd) && user()->id == $semakan3cmgd->approved_by && user()->id != $reviewcmgd->created_by && ($reviewlogcmgd->status != 'S4') && $reviewcmgd->status != 'Selesai') 
                            || (!empty($semakan4cmgd) && user()->id == $semakan4cmgd->approved_by && user()->id != $reviewcmgd->created_by && (empty($semakan5cmgd))) 
                            || (!empty($semakan5cmgd) && user()->id == $semakan5cmgd->approved_by && user()->id != $reviewcmgd->created_by && (empty($cetaknotisbpubcmgd))))
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-cmgd" role="tab" aria-controls="document-contract-review-cmgd" aria-selected="false">
                                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Sijil Siap Membaiki Kecacatan') }}
                                </a>
                            </li>
                        @endif
                    @endif
                @endif
            @endif

            {{-- cpo --}}
            @if(!empty($cpo))
                <li class="list-group-item">
                    <a class="list-group-item-action" data-toggle="tab" href="#cpo" role="tab" aria-controls="cpo" aria-selected="false">
                        @icon('fe fe-file-minus')&nbsp;{{ __('Sijil Siap Kerja Separa') }}
                    </a>
                </li>
                @if(!empty($review))
                    @if(!empty($reviewcpo))
                        <li class="list-group-item">
                            <a class="list-group-item-action" data-toggle="tab" href="#document-contract-reviews-cpo" role="tab" aria-controls="document-contract-reviews-cpo" aria-selected="false">
                                @icon('fe fe-briefcase')&nbsp;{{ __('Senarai Ulasan Sijil Siap Kerja Separa') }}
                            </a>
                        </li>
                        @if((user()->id == $reviewcpo->approved_by && user()->id != $reviewcpo->created_by && $reviewcpo->type == 'Dokumen Kontrak') 
                            || (!empty($semakan3cpo) && user()->id == $semakan3cpo->approved_by && user()->id != $reviewcpo->created_by && ($reviewlogcpo->status != 'S4') && $reviewcpo->status != 'Selesai') 
                            || (!empty($semakan4cpo) && user()->id == $semakan4cpo->approved_by && user()->id != $reviewcpo->created_by && (empty($semakan5cpo))) 
                            || (!empty($semakan5cpo) && user()->id == $semakan5cpo->approved_by && user()->id != $reviewcpo->created_by && (empty($cetaknotisbpubcpo))))
                            <li class="list-group-item">
                                <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-cpo" role="tab" aria-controls="document-contract-review-cpo" aria-selected="false">
                                    @icon('fe fe-edit text-primary')&nbsp;{{ __('Ulasan Pegawai - Sijil Siap Kerja Separa') }}
                                </a>
                            </li>
                        @endif
                    @endif
                @endif
            @endif

            @if(!empty($review))

                @if(user()->current_role_login == 'administrator')
                    <li class="list-group-item">
                        <a class="list-group-item-action" data-toggle="tab" href="#document-contract-review-log" role="tab" aria-controls="document-contract-review-log" aria-selected="false">
                            @icon('fe fe-file-text')&nbsp;{{ __('Log Semakan') }}
                        </a>
                    </li>
                @endif
            @endif
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document-tab-content'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'bon-implementation', 'active' => true])
                            @slot('content')
                                @include('contract.post.partials.shows.bon-implementation')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'insurance'])
                            @slot('content')
                                @include('contract.post.partials.shows.insurance')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'price-adjustment'])
                            @slot('content')
                                @include('contract.post.partials.shows.price-adjustment')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'cash-deposit'])
                            @slot('content')
                                @include('contract.post.partials.shows.cash-deposit')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'document-contract'])
                            @slot('content')
                                @include('contract.post.partials.shows.document-contract')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'sub-contract'])
                            @slot('content')
                                @include('contract.post.partials.shows.sub-contract')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'cnc'])
                            @slot('content')
                                @include('contract.post.cnc.show')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'cpc'])
                            @slot('content')
                                @include('contract.post.cpc.show')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'cmgd'])
                            @slot('content')
                                @include('contract.post.cmgd.show')
                            @endslot
                        @endcomponent

                        @component('components.tab.content', ['id' => 'cpo'])
                            @slot('content')
                                @include('contract.post.cpo.show')
                            @endslot
                        @endcomponent

                        {{-- bon pelaksanaan --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-bon" role="tabpanel" aria-labelledby="document-contract-reviews-bon-tab">
                            
                            @if(!empty($reviewbon))
                                @if(user()->current_role_login == 'penyedia')

                                    @if(!empty($cetaknotisuulogbon) && !empty($cetaknotisuulogbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuubon" aria-expanded="false" aria-controls="cnuubon">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuubon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7bon as $s7logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan7logbon) && !empty($semakan7logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7bon" aria-expanded="false" aria-controls="s7bon">
                                                        Ulasan Semakan Oleh {{$semakan7logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6bon as $s6logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubsbon) && !empty($cetaknotisbpubsbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubbon" aria-expanded="false" aria-controls="cnbpubbon">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubsbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubbon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5bon as $s5logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logbon) && !empty($semakan5logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5bon" aria-expanded="false" aria-controls="s5bon">
                                                        Ulasan Semakan Oleh {{$semakan5logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4bon as $s4logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    {{-- @if(!empty($semakan4logbon) && !empty($semakan4logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4bon" aria-expanded="false" aria-controls="s4bon">
                                                        Ulasan Semakan Oleh {{$semakan4logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3bon as $s3logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($semakan3logbon) && !empty($semakan3logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3bon" aria-expanded="false" aria-controls="s3bon">
                                                        Ulasan Semakan Oleh {{$semakan3logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2bon as $s2logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logbon) && !empty($semakan2logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2bon" aria-expanded="false" aria-controls="s2bon">
                                                        Ulasan Semakan Oleh {{$semakan2logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1bon as $s1logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubsbon) && !empty($cetaknotisbpubsbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubbon" aria-expanded="false" aria-controls="cnbpubbon">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubbon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5bon as $s5logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logbon) && !empty($semakan5logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5bon" aria-expanded="false" aria-controls="s5bon">
                                                            Ulasan Semakan Oleh {{$semakan5logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4bon as $s4logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logbon) && !empty($semakan4logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4bon" aria-expanded="false" aria-controls="s4bon">
                                                            Ulasan Semakan Oleh {{$semakan4logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3bon as $s3logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logbon) && !empty($semakan3logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3bon" aria-expanded="false" aria-controls="s3bon">
                                                        Ulasan Semakan Oleh {{$semakan3logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2bon as $s2logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logbon) && !empty($semakan2logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2bon" aria-expanded="false" aria-controls="s2bon">
                                                        Ulasan Semakan Oleh {{$semakan2logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1bon as $s1logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogbon) && !empty($cetaknotisuulogbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuubon" aria-expanded="false" aria-controls="cnuubon">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuubon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7bon as $s7logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logbon) && !empty($semakan7logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7bon" aria-expanded="false" aria-controls="s7bon">
                                                        Ulasan Semakan Oleh {{$semakan7logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6bon as $s6logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logbon) && !empty($semakan3logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3bon" aria-expanded="false" aria-controls="s3bon">
                                                        Ulasan Semakan Oleh {{$semakan3logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2bon as $s2logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logbon) && !empty($semakan2logbon->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2bon" aria-expanded="false" aria-controls="s2bon">
                                                        Ulasan Semakan Oleh {{$semakan2logbon->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1bon as $s1logbon)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logbon->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logbon->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else

                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogbon) && !empty($cetaknotisuulogbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuubon" aria-expanded="false" aria-controls="cnuubon">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuubon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7bon as $s7logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logbon) && !empty($semakan7logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7bon" aria-expanded="false" aria-controls="s7bon">
                                                            Ulasan Semakan Oleh {{$semakan7logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6bon as $s6logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubsbon) && !empty($cetaknotisbpubsbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubbon" aria-expanded="false" aria-controls="cnbpubbon">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubbon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5bon as $s5logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logbon) && !empty($semakan5logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5bon" aria-expanded="false" aria-controls="s5bon">
                                                            Ulasan Semakan Oleh {{$semakan5logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4bon as $s4logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logbon) && !empty($semakan4logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4bon" aria-expanded="false" aria-controls="s4bon">
                                                            Ulasan Semakan Oleh {{$semakan4logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3bon as $s3logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logbon) && !empty($semakan3logbon->requested_by))
                                            @if(!empty($semakan3bon) && $semakan3bon->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3bon" aria-expanded="false" aria-controls="s3bon">
                                                                Ulasan Semakan Oleh {{$semakan3logbon->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2bon as $s2logbon)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logbon->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logbon->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewbon) && !empty($reviewbon->create) && !empty($reviewbon->create->department) && $reviewbon->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logbon) && !empty($semakan3logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3bon" aria-expanded="false" aria-controls="s3bon">
                                                            Ulasan Semakan Oleh {{$semakan3logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2bon as $s2logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logbon) && !empty($semakan2logbon->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2bon" aria-expanded="false" aria-controls="s2bon">
                                                            Ulasan Semakan Oleh {{$semakan2logbon->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2bon" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1bon as $s1logbon)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logbon->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logbon->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logbon->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif

                        </div>

                        {{-- insurance --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-insurance" role="tabpanel" aria-labelledby="document-contract-reviews-insurance-tab">
                            
                            @if(!empty($reviewinsurans))
                                @if(user()->current_role_login == 'penyedia')

                                    @if(!empty($cetaknotisuuloginsurans) && !empty($cetaknotisuuloginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuins" aria-expanded="false" aria-controls="cnuuins">
                                                        Ulasan Semakan Oleh {{$cetaknotisuuloginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuuins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7insurans as $s7loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan7loginsurans) && !empty($semakan7loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7ins" aria-expanded="false" aria-controls="s7ins">
                                                        Ulasan Semakan Oleh {{$semakan7loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6insurans as $s6loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubsinsurans) && !empty($cetaknotisbpubsinsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubins" aria-expanded="false" aria-controls="cnbpubins">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubsinsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5insurans as $s5loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5loginsurans) && !empty($semakan5loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5ins" aria-expanded="false" aria-controls="s5ins">
                                                        Ulasan Semakan Oleh {{$semakan5loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4insurans as $s4loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    {{-- @if(!empty($semakan4loginsurans) && !empty($semakan4loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4ins" aria-expanded="false" aria-controls="s4ins">
                                                        Ulasan Semakan Oleh {{$semakan4loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3insurans as $s3loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($semakan3loginsurans) && !empty($semakan3loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3ins" aria-expanded="false" aria-controls="s3ins">
                                                        Ulasan Semakan Oleh {{$semakan3loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2insurans as $s2loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2loginsurans) && !empty($semakan2loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2ins" aria-expanded="false" aria-controls="s2ins">
                                                        Ulasan Semakan Oleh {{$semakan2loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1insurans as $s1loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubsinsurans) && !empty($cetaknotisbpubsinsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubins" aria-expanded="false" aria-controls="cnbpubins">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsinsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5insurans as $s5loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5loginsurans) && !empty($semakan5loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5ins" aria-expanded="false" aria-controls="s5ins">
                                                            Ulasan Semakan Oleh {{$semakan5loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4insurans as $s4loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4loginsurans) && !empty($semakan4loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4ins" aria-expanded="false" aria-controls="s4ins">
                                                            Ulasan Semakan Oleh {{$semakan4loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3insurans as $s3loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3loginsurans) && !empty($semakan3loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3ins" aria-expanded="false" aria-controls="s3ins">
                                                        Ulasan Semakan Oleh {{$semakan3loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2insurans as $s2loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2loginsurans) && !empty($semakan2loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2ins" aria-expanded="false" aria-controls="s2ins">
                                                        Ulasan Semakan Oleh {{$semakan2loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1insurans as $s1loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuuloginsurans) && !empty($cetaknotisuuloginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuins" aria-expanded="false" aria-controls="cnuuins">
                                                        Ulasan Semakan Oleh {{$cetaknotisuuloginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuuins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7insurans as $s7loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->requested_at)) !!}</p>

                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7loginsurans) && !empty($semakan7loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7ins" aria-expanded="false" aria-controls="s7ins">
                                                        Ulasan Semakan Oleh {{$semakan7loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6insurans as $s6loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3loginsurans) && !empty($semakan3loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3ins" aria-expanded="false" aria-controls="s3ins">
                                                        Ulasan Semakan Oleh {{$semakan3loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2insurans as $s2loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2loginsurans) && !empty($semakan2loginsurans->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2ins" aria-expanded="false" aria-controls="s2ins">
                                                        Ulasan Semakan Oleh {{$semakan2loginsurans->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1insurans as $s1loginsurans)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1loginsurans->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1loginsurans->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuuloginsurans) && !empty($cetaknotisuuloginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuins" aria-expanded="false" aria-controls="cnuuins">
                                                            Ulasan Semakan Oleh {{$cetaknotisuuloginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuuins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7insurans as $s7loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7loginsurans) && !empty($semakan7loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7ins" aria-expanded="false" aria-controls="s7ins">
                                                            Ulasan Semakan Oleh {{$semakan7loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6insurans as $s6loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubsinsurans) && !empty($cetaknotisbpubsinsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubins" aria-expanded="false" aria-controls="cnbpubins">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsinsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5insurans as $s5loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5loginsurans) && !empty($semakan5loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5ins" aria-expanded="false" aria-controls="s5ins">
                                                            Ulasan Semakan Oleh {{$semakan5loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4insurans as $s4loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4loginsurans) && !empty($semakan4loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4ins" aria-expanded="false" aria-controls="s4ins">
                                                            Ulasan Semakan Oleh {{$semakan4loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3insurans as $s3loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3loginsurans) && !empty($semakan3loginsurans->requested_by))
                                            @if(!empty($semakan3insurans) && $semakan3insurans->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3ins" aria-expanded="false" aria-controls="s3ins">
                                                                Ulasan Semakan Oleh {{$semakan3loginsurans->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2insurans as $s2loginsurans)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2loginsurans->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2loginsurans->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewinsurans) && !empty($reviewinsurans->create) && !empty($reviewinsurans->create->department) && $reviewinsurans->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3loginsurans) && !empty($semakan3loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3ins" aria-expanded="false" aria-controls="s3ins">
                                                            Ulasan Semakan Oleh {{$semakan3loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2insurans as $s2loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2loginsurans) && !empty($semakan2loginsurans->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2ins" aria-expanded="false" aria-controls="s2ins">
                                                            Ulasan Semakan Oleh {{$semakan2loginsurans->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2ins" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1insurans as $s1loginsurans)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1loginsurans->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1loginsurans->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1loginsurans->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif 

                        </div>

                        {{-- wang pendahuluan --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-wang" role="tabpanel" aria-labelledby="document-contract-reviews-wang-tab">
                            
                            @if(!empty($reviewwang))
                                @if(user()->current_role_login == 'penyedia')

                                    @if(!empty($cetaknotisuulogwang) && !empty($cetaknotisuulogwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuwang" aria-expanded="false" aria-controls="cnuuwang">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuuwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7wang as $s7logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan7logwang) && !empty($semakan7logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7wang" aria-expanded="false" aria-controls="s7wang">
                                                        Ulasan Semakan Oleh {{$semakan7logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6wang as $s6logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubswang) && !empty($cetaknotisbpubswang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubwang" aria-expanded="false" aria-controls="cnbpubwang">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubswang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5wang as $s5logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logwang) && !empty($semakan5logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5wang" aria-expanded="false" aria-controls="s5wang">
                                                        Ulasan Semakan Oleh {{$semakan5logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4wang as $s4logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    {{-- @if(!empty($semakan4logwang) && !empty($semakan4logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4wang" aria-expanded="false" aria-controls="s4wang">
                                                        Ulasan Semakan Oleh {{$semakan4logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3wang as $s3logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($semakan3logwang) && !empty($semakan3logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3wang" aria-expanded="false" aria-controls="s3wang">
                                                        Ulasan Semakan Oleh {{$semakan3logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2wang as $s2logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logwang) && !empty($semakan2logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2wang" aria-expanded="false" aria-controls="s2wang">
                                                        Ulasan Semakan Oleh {{$semakan2logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1wang as $s1logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubswang) && !empty($cetaknotisbpubswang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubwang" aria-expanded="false" aria-controls="cnbpubwang">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubswang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5wang as $s5logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logwang) && !empty($semakan5logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5wang" aria-expanded="false" aria-controls="s5wang">
                                                            Ulasan Semakan Oleh {{$semakan5logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4wang as $s4logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logwang) && !empty($semakan4logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4wang" aria-expanded="false" aria-controls="s4wang">
                                                            Ulasan Semakan Oleh {{$semakan4logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3wang as $s3logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logwang) && !empty($semakan3logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3wang" aria-expanded="false" aria-controls="s3wang">
                                                        Ulasan Semakan Oleh {{$semakan3logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2wang as $s2logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logwang) && !empty($semakan2logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2wang" aria-expanded="false" aria-controls="s2wang">
                                                        Ulasan Semakan Oleh {{$semakan2logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1wang as $s1logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogwang) && !empty($cetaknotisuulogwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuwang" aria-expanded="false" aria-controls="cnuuwang">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuuwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7wang as $s7logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logwang) && !empty($semakan7logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7wang" aria-expanded="false" aria-controls="s7wang">
                                                        Ulasan Semakan Oleh {{$semakan7logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6wang as $s6logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logwang) && !empty($semakan3logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3wang" aria-expanded="false" aria-controls="s3wang">
                                                        Ulasan Semakan Oleh {{$semakan3logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2wang as $s2logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logwang) && !empty($semakan2logwang->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2wang" aria-expanded="false" aria-controls="s2wang">
                                                        Ulasan Semakan Oleh {{$semakan2logwang->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1wang as $s1logwang)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logwang->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logwang->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogwang) && !empty($cetaknotisuulogwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuuwang" aria-expanded="false" aria-controls="cnuuwang">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuuwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7wang as $s7logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logwang) && !empty($semakan7logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7wang" aria-expanded="false" aria-controls="s7wang">
                                                            Ulasan Semakan Oleh {{$semakan7logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6wang as $s6logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubswang) && !empty($cetaknotisbpubswang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubwang" aria-expanded="false" aria-controls="cnbpubwang">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubswang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubwang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5wang as $s5logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logwang) && !empty($semakan5logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5wang" aria-expanded="false" aria-controls="s5wang">
                                                            Ulasan Semakan Oleh {{$semakan5logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4wang as $s4logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logwang) && !empty($semakan4logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4wang" aria-expanded="false" aria-controls="s4wang">
                                                            Ulasan Semakan Oleh {{$semakan4logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3wang as $s3logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logwang) && !empty($semakan3logwang->requested_by))
                                            @if(!empty($semakan3wang) && $semakan3wang->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3wang" aria-expanded="false" aria-controls="s3wang">
                                                                Ulasan Semakan Oleh {{$semakan3logwang->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2wang as $s2logwang)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logwang->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logwang->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewwang) && !empty($reviewwang->create) && !empty($reviewwang->create->department) && $reviewwang->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logwang) && !empty($semakan3logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3wang" aria-expanded="false" aria-controls="s3wang">
                                                            Ulasan Semakan Oleh {{$semakan3logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2wang as $s2logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logwang) && !empty($semakan2logwang->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2wang" aria-expanded="false" aria-controls="s2wang">
                                                            Ulasan Semakan Oleh {{$semakan2logwang->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2wang" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1wang as $s1logwang)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logwang->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logwang->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logwang->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif

                        </div>

                        {{-- dokumen kontrak --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-doc" role="tabpanel" aria-labelledby="document-contract-reviews-doc-tab">
                            
                            @if(!empty($reviewdoc))
                                @if(user()->current_role_login == 'penyedia')

                                    @if(!empty($cetaknotisuulogdoc) && !empty($cetaknotisuulogdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuudoc" aria-expanded="false" aria-controls="cnuudoc">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuudoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7doc as $s7logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan7logdoc) && !empty($semakan7logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7doc" aria-expanded="false" aria-controls="s7doc">
                                                        Ulasan Semakan Oleh {{$semakan7logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6doc as $s6logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubsdoc) && !empty($cetaknotisbpubsdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubdoc" aria-expanded="false" aria-controls="cnbpubdoc">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubsdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubdoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5doc as $s5logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logdoc) && !empty($semakan5logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5doc" aria-expanded="false" aria-controls="s5doc">
                                                        Ulasan Semakan Oleh {{$semakan5logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4doc as $s4logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    {{-- @if(!empty($semakan4logdoc) && !empty($semakan4logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4doc" aria-expanded="false" aria-controls="s4doc">
                                                        Ulasan Semakan Oleh {{$semakan4logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3doc as $s3logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($semakan3logdoc) && !empty($semakan3logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3doc" aria-expanded="false" aria-controls="s3doc">
                                                        Ulasan Semakan Oleh {{$semakan3logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2doc as $s2logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logdoc) && !empty($semakan2logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2doc" aria-expanded="false" aria-controls="s2doc">
                                                        Ulasan Semakan Oleh {{$semakan2logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1doc as $s1logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubsdoc) && !empty($cetaknotisbpubsdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubdoc" aria-expanded="false" aria-controls="cnbpubdoc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubdoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5doc as $s5logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logdoc) && !empty($semakan5logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5doc" aria-expanded="false" aria-controls="s5doc">
                                                            Ulasan Semakan Oleh {{$semakan5logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4doc as $s4logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logdoc) && !empty($semakan4logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4doc" aria-expanded="false" aria-controls="s4doc">
                                                            Ulasan Semakan Oleh {{$semakan4logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3doc as $s3logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logdoc) && !empty($semakan3logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3doc" aria-expanded="false" aria-controls="s3doc">
                                                        Ulasan Semakan Oleh {{$semakan3logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2doc as $s2logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logdoc) && !empty($semakan2logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2doc" aria-expanded="false" aria-controls="s2doc">
                                                        Ulasan Semakan Oleh {{$semakan2logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1doc as $s1logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogdoc) && !empty($cetaknotisuulogdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuudoc" aria-expanded="false" aria-controls="cnuudoc">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuudoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7doc as $s7logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logdoc) && !empty($semakan7logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7doc" aria-expanded="false" aria-controls="s7doc">
                                                        Ulasan Semakan Oleh {{$semakan7logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6doc as $s6logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logdoc) && !empty($semakan3logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3doc" aria-expanded="false" aria-controls="s3doc">
                                                        Ulasan Semakan Oleh {{$semakan3logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2doc as $s2logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logdoc) && !empty($semakan2logdoc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2doc" aria-expanded="false" aria-controls="s2doc">
                                                        Ulasan Semakan Oleh {{$semakan2logdoc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1doc as $s1logdoc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logdoc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logdoc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogdoc) && !empty($cetaknotisuulogdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuudoc" aria-expanded="false" aria-controls="cnuudoc">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuudoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7doc as $s7logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logdoc) && !empty($semakan7logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7doc" aria-expanded="false" aria-controls="s7doc">
                                                            Ulasan Semakan Oleh {{$semakan7logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6doc as $s6logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubsdoc) && !empty($cetaknotisbpubsdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubdoc" aria-expanded="false" aria-controls="cnbpubdoc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubsdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubdoc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5doc as $s5logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logdoc) && !empty($semakan5logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5doc" aria-expanded="false" aria-controls="s5doc">
                                                            Ulasan Semakan Oleh {{$semakan5logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4doc as $s4logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logdoc) && !empty($semakan4logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4doc" aria-expanded="false" aria-controls="s4doc">
                                                            Ulasan Semakan Oleh {{$semakan4logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3doc as $s3logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logdoc) && !empty($semakan3logdoc->requested_by))
                                            @if(!empty($semakan3doc) && $semakan3doc->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3doc" aria-expanded="false" aria-controls="s3doc">
                                                                Ulasan Semakan Oleh {{$semakan3logdoc->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2doc as $s2logdoc)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logdoc->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logdoc->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewdoc) && !empty($reviewdoc->create) && !empty($reviewdoc->create->department) && $reviewdoc->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logdoc) && !empty($semakan3logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3doc" aria-expanded="false" aria-controls="s3doc">
                                                            Ulasan Semakan Oleh {{$semakan3logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2doc as $s2logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logdoc) && !empty($semakan2logdoc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2doc" aria-expanded="false" aria-controls="s2doc">
                                                            Ulasan Semakan Oleh {{$semakan2logdoc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2doc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1doc as $s1logdoc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logdoc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logdoc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logdoc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif

                        </div>

                        {{-- cpc --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-cpc" role="tabpanel" aria-labelledby="cpcument-contract-reviews-cpc-tab">
                            
                            @if(!empty($reviewcpc))
                                @if(user()->current_role_login == 'penyedia')

                                    {{-- @if(!empty($cetaknotisuulogcpc) && !empty($cetaknotisuulogcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucpc" aria-expanded="false" aria-controls="cnuucpc">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cpc as $s7logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcpc) && !empty($semakan7logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cpc" aria-expanded="false" aria-controls="s7cpc">
                                                        Ulasan Semakan Oleh {{$semakan7logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cpc as $s6logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubscpc) && !empty($cetaknotisbpubscpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcpc" aria-expanded="false" aria-controls="cnbpubcpc">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubscpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubcpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5cpc as $s5logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logcpc) && !empty($semakan5logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cpc" aria-expanded="false" aria-controls="s5cpc">
                                                        Ulasan Semakan Oleh {{$semakan5logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4cpc as $s4logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan4logcpc) && !empty($semakan4logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cpc" aria-expanded="false" aria-controls="s4cpc">
                                                        Ulasan Semakan Oleh {{$semakan4logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3cpc as $s3logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($semakan3logcpc) && !empty($semakan3logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpc" aria-expanded="false" aria-controls="s3cpc">
                                                        Ulasan Semakan Oleh {{$semakan3logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cpc as $s2logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcpc) && !empty($semakan2logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpc" aria-expanded="false" aria-controls="s2cpc">
                                                        Ulasan Semakan Oleh {{$semakan2logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cpc as $s1logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    
                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscpc) && !empty($cetaknotisbpubscpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcpc" aria-expanded="false" aria-controls="cnbpubcpc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cpc as $s5logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcpc) && !empty($semakan5logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cpc" aria-expanded="false" aria-controls="s5cpc">
                                                            Ulasan Semakan Oleh {{$semakan5logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cpc as $s4logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcpc) && !empty($semakan4logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cpc" aria-expanded="false" aria-controls="s4cpc">
                                                            Ulasan Semakan Oleh {{$semakan4logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cpc as $s3logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcpc) && !empty($semakan3logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpc" aria-expanded="false" aria-controls="s3cpc">
                                                        Ulasan Semakan Oleh {{$semakan3logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cpc as $s2logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcpc) && !empty($semakan2logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpc" aria-expanded="false" aria-controls="s2cpc">
                                                        Ulasan Semakan Oleh {{$semakan2logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cpc as $s1logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogcpc) && !empty($cetaknotisuulogcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucpc" aria-expanded="false" aria-controls="cnuucpc">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cpc as $s7logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcpc) && !empty($semakan7logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cpc" aria-expanded="false" aria-controls="s7cpc">
                                                        Ulasan Semakan Oleh {{$semakan7logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cpc as $s6logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcpc) && !empty($semakan3logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpc" aria-expanded="false" aria-controls="s3cpc">
                                                        Ulasan Semakan Oleh {{$semakan3logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cpc as $s2logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcpc) && !empty($semakan2logcpc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpc" aria-expanded="false" aria-controls="s2cpc">
                                                        Ulasan Semakan Oleh {{$semakan2logcpc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cpc as $s1logcpc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcpc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogcpc) && !empty($cetaknotisuulogcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucpc" aria-expanded="false" aria-controls="cnuucpc">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuucpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7cpc as $s7logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logcpc) && !empty($semakan7logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cpc" aria-expanded="false" aria-controls="s7cpc">
                                                            Ulasan Semakan Oleh {{$semakan7logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6cpc as $s6logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscpc) && !empty($cetaknotisbpubscpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcpc" aria-expanded="false" aria-controls="cnbpubcpc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cpc as $s5logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcpc) && !empty($semakan5logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cpc" aria-expanded="false" aria-controls="s5cpc">
                                                            Ulasan Semakan Oleh {{$semakan5logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cpc as $s4logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcpc) && !empty($semakan4logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cpc" aria-expanded="false" aria-controls="s4cpc">
                                                            Ulasan Semakan Oleh {{$semakan4logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cpc as $s3logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logcpc) && !empty($semakan3logcpc->requested_by))
                                            @if(!empty($semakan3cpc) && $semakan3cpc->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpc" aria-expanded="false" aria-controls="s3cpc">
                                                                Ulasan Semakan Oleh {{$semakan3logcpc->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2cpc as $s2logcpc)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logcpc->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpc->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewcpc) && !empty($reviewcpc->create) && !empty($reviewcpc->create->department) && $reviewcpc->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logcpc) && !empty($semakan3logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpc" aria-expanded="false" aria-controls="s3cpc">
                                                            Ulasan Semakan Oleh {{$semakan3logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2cpc as $s2logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logcpc) && !empty($semakan2logcpc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpc" aria-expanded="false" aria-controls="s2cpc">
                                                            Ulasan Semakan Oleh {{$semakan2logcpc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2cpc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1cpc as $s1logcpc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcpc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logcpc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif

                        </div>

                        {{-- cnc --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-cnc" role="tabpanel" aria-labelledby="cncument-contract-reviews-cnc-tab">
                            
                            @if(!empty($reviewcnc))
                                @if(user()->current_role_login == 'penyedia')

                                    {{-- @if(!empty($cetaknotisuulogcnc) && !empty($cetaknotisuulogcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucnc" aria-expanded="false" aria-controls="cnuucnc">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cnc as $s7logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcnc) && !empty($semakan7logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cnc" aria-expanded="false" aria-controls="s7cnc">
                                                        Ulasan Semakan Oleh {{$semakan7logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cnc as $s6logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubscnc) && !empty($cetaknotisbpubscnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcnc" aria-expanded="false" aria-controls="cnbpubcnc">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubscnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubcnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5cnc as $s5logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logcnc) && !empty($semakan5logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cnc" aria-expanded="false" aria-controls="s5cnc">
                                                        Ulasan Semakan Oleh {{$semakan5logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4cnc as $s4logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan4logcnc) && !empty($semakan4logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cnc" aria-expanded="false" aria-controls="s4cnc">
                                                        Ulasan Semakan Oleh {{$semakan4logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3cnc as $s3logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($semakan3logcnc) && !empty($semakan3logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cnc" aria-expanded="false" aria-controls="s3cnc">
                                                        Ulasan Semakan Oleh {{$semakan3logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cnc as $s2logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcnc) && !empty($semakan2logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cnc" aria-expanded="false" aria-controls="s2cnc">
                                                        Ulasan Semakan Oleh {{$semakan2logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cnc as $s1logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscnc) && !empty($cetaknotisbpubscnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcnc" aria-expanded="false" aria-controls="cnbpubcnc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cnc as $s5logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcnc) && !empty($semakan5logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cnc" aria-expanded="false" aria-controls="s5cnc">
                                                            Ulasan Semakan Oleh {{$semakan5logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cnc as $s4logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcnc) && !empty($semakan4logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cnc" aria-expanded="false" aria-controls="s4cnc">
                                                            Ulasan Semakan Oleh {{$semakan4logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cnc as $s3logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcnc) && !empty($semakan3logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cnc" aria-expanded="false" aria-controls="s3cnc">
                                                        Ulasan Semakan Oleh {{$semakan3logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cnc as $s2logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcnc) && !empty($semakan2logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cnc" aria-expanded="false" aria-controls="s2cnc">
                                                        Ulasan Semakan Oleh {{$semakan2logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cnc as $s1logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogcnc) && !empty($cetaknotisuulogcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucnc" aria-expanded="false" aria-controls="cnuucnc">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cnc as $s7logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcnc) && !empty($semakan7logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cnc" aria-expanded="false" aria-controls="s7cnc">
                                                        Ulasan Semakan Oleh {{$semakan7logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cnc as $s6logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcnc) && !empty($semakan3logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cnc" aria-expanded="false" aria-controls="s3cnc">
                                                        Ulasan Semakan Oleh {{$semakan3logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cnc as $s2logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcnc) && !empty($semakan2logcnc->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cnc" aria-expanded="false" aria-controls="s2cnc">
                                                        Ulasan Semakan Oleh {{$semakan2logcnc->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cnc as $s1logcnc)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcnc->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcnc->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogcnc) && !empty($cetaknotisuulogcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucnc" aria-expanded="false" aria-controls="cnuucnc">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuucnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7cnc as $s7logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logcnc) && !empty($semakan7logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cnc" aria-expanded="false" aria-controls="s7cnc">
                                                            Ulasan Semakan Oleh {{$semakan7logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6cnc as $s6logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscnc) && !empty($cetaknotisbpubscnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcnc" aria-expanded="false" aria-controls="cnbpubcnc">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cnc as $s5logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcnc) && !empty($semakan5logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cnc" aria-expanded="false" aria-controls="s5cnc">
                                                            Ulasan Semakan Oleh {{$semakan5logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cnc as $s4logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcnc) && !empty($semakan4logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cnc" aria-expanded="false" aria-controls="s4cnc">
                                                            Ulasan Semakan Oleh {{$semakan4logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cnc as $s3logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logcnc) && !empty($semakan3logcnc->requested_by))
                                            @if(!empty($semakan3cnc) && $semakan3cnc->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cnc" aria-expanded="false" aria-controls="s3cnc">
                                                                Ulasan Semakan Oleh {{$semakan3logcnc->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2cnc as $s2logcnc)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logcnc->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcnc->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewcnc) && !empty($reviewcnc->create) && !empty($reviewcnc->create->department) && $reviewcnc->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logcnc) && !empty($semakan3logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cnc" aria-expanded="false" aria-controls="s3cnc">
                                                            Ulasan Semakan Oleh {{$semakan3logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2cnc as $s2logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logcnc) && !empty($semakan2logcnc->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cnc" aria-expanded="false" aria-controls="s2cnc">
                                                            Ulasan Semakan Oleh {{$semakan2logcnc->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2cnc" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1cnc as $s1logcnc)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcnc->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logcnc->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcnc->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif

                        </div>

                        {{-- cmgd --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-cmgd" role="tabpanel" aria-labelledby="cmgdument-contract-reviews-cmgd-tab">
                            
                            @if(!empty($reviewcmgd))
                                @if(user()->current_role_login == 'penyedia')

                                    {{-- @if(!empty($cetaknotisuulogcmgd) && !empty($cetaknotisuulogcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucmgd" aria-expanded="false" aria-controls="cnuucmgd">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cmgd as $s7logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcmgd) && !empty($semakan7logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cmgd" aria-expanded="false" aria-controls="s7cmgd">
                                                        Ulasan Semakan Oleh {{$semakan7logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cmgd as $s6logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubscmgd) && !empty($cetaknotisbpubscmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcmgd" aria-expanded="false" aria-controls="cnbpubcmgd">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubscmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubcmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5cmgd as $s5logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logcmgd) && !empty($semakan5logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cmgd" aria-expanded="false" aria-controls="s5cmgd">
                                                        Ulasan Semakan Oleh {{$semakan5logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4cmgd as $s4logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan4logcmgd) && !empty($semakan4logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cmgd" aria-expanded="false" aria-controls="s4cmgd">
                                                        Ulasan Semakan Oleh {{$semakan4logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3cmgd as $s3logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}
                                        
                                    @if(!empty($semakan3logcmgd) && !empty($semakan3logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cmgd" aria-expanded="false" aria-controls="s3cmgd">
                                                        Ulasan Semakan Oleh {{$semakan3logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cmgd as $s2logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcmgd) && !empty($semakan2logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cmgd" aria-expanded="false" aria-controls="s2cmgd">
                                                        Ulasan Semakan Oleh {{$semakan2logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cmgd as $s1logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscmgd) && !empty($cetaknotisbpubscmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcmgd" aria-expanded="false" aria-controls="cnbpubcmgd">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cmgd as $s5logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcmgd) && !empty($semakan5logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cmgd" aria-expanded="false" aria-controls="s5cmgd">
                                                            Ulasan Semakan Oleh {{$semakan5logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cmgd as $s4logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcmgd) && !empty($semakan4logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cmgd" aria-expanded="false" aria-controls="s4cmgd">
                                                            Ulasan Semakan Oleh {{$semakan4logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cmgd as $s3logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcmgd) && !empty($semakan3logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cmgd" aria-expanded="false" aria-controls="s3cmgd">
                                                        Ulasan Semakan Oleh {{$semakan3logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cmgd as $s2logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcmgd) && !empty($semakan2logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cmgd" aria-expanded="false" aria-controls="s2cmgd">
                                                        Ulasan Semakan Oleh {{$semakan2logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cmgd as $s1logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogcmgd) && !empty($cetaknotisuulogcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucmgd" aria-expanded="false" aria-controls="cnuucmgd">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cmgd as $s7logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcmgd) && !empty($semakan7logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cmgd" aria-expanded="false" aria-controls="s7cmgd">
                                                        Ulasan Semakan Oleh {{$semakan7logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cmgd as $s6logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcmgd) && !empty($semakan3logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cmgd" aria-expanded="false" aria-controls="s3cmgd">
                                                        Ulasan Semakan Oleh {{$semakan3logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cmgd as $s2logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcmgd) && !empty($semakan2logcmgd->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cmgd" aria-expanded="false" aria-controls="s2cmgd">
                                                        Ulasan Semakan Oleh {{$semakan2logcmgd->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cmgd as $s1logcmgd)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcmgd->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcmgd->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogcmgd) && !empty($cetaknotisuulogcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucmgd" aria-expanded="false" aria-controls="cnuucmgd">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuucmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7cmgd as $s7logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logcmgd) && !empty($semakan7logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cmgd" aria-expanded="false" aria-controls="s7cmgd">
                                                            Ulasan Semakan Oleh {{$semakan7logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6cmgd as $s6logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscmgd) && !empty($cetaknotisbpubscmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcmgd" aria-expanded="false" aria-controls="cnbpubcmgd">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cmgd as $s5logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcmgd) && !empty($semakan5logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cmgd" aria-expanded="false" aria-controls="s5cmgd">
                                                            Ulasan Semakan Oleh {{$semakan5logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cmgd as $s4logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcmgd) && !empty($semakan4logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cmgd" aria-expanded="false" aria-controls="s4cmgd">
                                                            Ulasan Semakan Oleh {{$semakan4logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cmgd as $s3logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logcmgd) && !empty($semakan3logcmgd->requested_by))
                                            @if(!empty($semakan3cmgd) && $semakan3cmgd->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cmgd" aria-expanded="false" aria-controls="s3cmgd">
                                                                Ulasan Semakan Oleh {{$semakan3logcmgd->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2cmgd as $s2logcmgd)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logcmgd->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcmgd->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if($reviewcmgd->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logcmgd) && !empty($semakan3logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cmgd" aria-expanded="false" aria-controls="s3cmgd">
                                                            Ulasan Semakan Oleh {{$semakan3logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2cmgd as $s2logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logcmgd) && !empty($semakan2logcmgd->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cmgd" aria-expanded="false" aria-controls="s2cmgd">
                                                            Ulasan Semakan Oleh {{$semakan2logcmgd->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2cmgd" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1cmgd as $s1logcmgd)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcmgd->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logcmgd->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcmgd->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endif
                            @endif

                        </div>

                        {{-- cpo --}}
                        <div class="tab-pane fade show" id="document-contract-reviews-cpo" role="tabpanel" aria-labelledby="cpoument-contract-reviews-cpo-tab">
                            
                            @if(!empty($reviewcpo))
                                @if(user()->current_role_login == 'penyedia')

                                    {{-- @if(!empty($cetaknotisuulogcpo) && !empty($cetaknotisuulogcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucpo" aria-expanded="false" aria-controls="cnuucpo">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cpo as $s7logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcpo) && !empty($semakan7logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cpo" aria-expanded="false" aria-controls="s7cpo">
                                                        Ulasan Semakan Oleh {{$semakan7logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cpo as $s6logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}

                                    @if(!empty($cetaknotisbpubscpo) && !empty($cetaknotisbpubscpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcpo" aria-expanded="false" aria-controls="cnbpubcpo">
                                                        Ulasan Semakan Oleh {{$cetaknotisbpubscpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnbpubcpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl5cpo as $s5logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s5logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s5logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- @if(!empty($semakan5logcpo) && !empty($semakan5logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cpo" aria-expanded="false" aria-controls="s5cpo">
                                                        Ulasan Semakan Oleh {{$semakan5logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s5cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl4cpo as $s4logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s4logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s4logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan4logcpo) && !empty($semakan4logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cpo" aria-expanded="false" aria-controls="s4cpo">
                                                        Ulasan Semakan Oleh {{$semakan4logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s4cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl3cpo as $s3logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s3logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s3logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif --}}
                                    
                                    @if(!empty($semakan3logcpo) && !empty($semakan3logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpo" aria-expanded="false" aria-controls="s3cpo">
                                                        Ulasan Semakan Oleh {{$semakan3logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cpo as $s2logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcpo) && !empty($semakan2logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpo" aria-expanded="false" aria-controls="s2cpo">
                                                        Ulasan Semakan Oleh {{$semakan2logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cpo as $s1logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    
                                @elseif(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscpo) && !empty($cetaknotisbpubscpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcpo" aria-expanded="false" aria-controls="cnbpubcpo">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cpo as $s5logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcpo) && !empty($semakan5logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cpo" aria-expanded="false" aria-controls="s5cpo">
                                                            Ulasan Semakan Oleh {{$semakan5logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cpo as $s4logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcpo) && !empty($semakan4logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cpo" aria-expanded="false" aria-controls="s4cpo">
                                                            Ulasan Semakan Oleh {{$semakan4logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cpo as $s3logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcpo) && !empty($semakan3logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpo" aria-expanded="false" aria-controls="s3cpo">
                                                        Ulasan Semakan Oleh {{$semakan3logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cpo as $s2logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcpo) && !empty($semakan2logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpo" aria-expanded="false" aria-controls="s2cpo">
                                                        Ulasan Semakan Oleh {{$semakan2logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cpo as $s1logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @elseif(user()->current_role_login == 'pengesah' && user()->department_id == 3)
                                    {{-- pegawai undang-undang --}}
                                    @if(!empty($cetaknotisuulogcpo) && !empty($cetaknotisuulogcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucpo" aria-expanded="false" aria-controls="cnuucpo">
                                                        Ulasan Semakan Oleh {{$cetaknotisuulogcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cnuucpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl7cpo as $s7logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s7logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s7logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan7logcpo) && !empty($semakan7logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cpo" aria-expanded="false" aria-controls="s7cpo">
                                                        Ulasan Semakan Oleh {{$semakan7logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s7cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl6cpo as $s6logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s6logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s6logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($semakan3logcpo) && !empty($semakan3logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpo" aria-expanded="false" aria-controls="s3cpo">
                                                        Ulasan Semakan Oleh {{$semakan3logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s3cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl2cpo as $s2logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s2logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($semakan2logcpo) && !empty($semakan2logcpo->requested_by))
                                        <div class="mb-0 card card-primary">
                                            <div class="card-header" role="tab" id="headingOne2">
                                                <h4 class="card-title">
                                                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpo" aria-expanded="false" aria-controls="s2cpo">
                                                        Ulasan Semakan Oleh {{$semakan2logcpo->request->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="s2cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                <div class="card-body">
                                                    @foreach($sl1cpo as $s1logcpo)
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Terima</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->approved_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Tarikh Hantar</div>
                                                                <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->requested_at)) !!}</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="text-muted">Status</div>
                                                                <p>{!! $s1logcpo->reviews_status !!}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="text-muted">Ulasan</div>
                                                                <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpo->remarks) !!}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    {{-- pegawai undang-undang --}}
                                    @if(user()->department->id == '3')

                                        @if(!empty($cetaknotisuulogcpo) && !empty($cetaknotisuulogcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnuucpo" aria-expanded="false" aria-controls="cnuucpo">
                                                            Ulasan Semakan Oleh {{$cetaknotisuulogcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnuucpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl7cpo as $s7logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s7logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s7logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s7logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan7logcpo) && !empty($semakan7logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s7cpo" aria-expanded="false" aria-controls="s7cpo">
                                                            Ulasan Semakan Oleh {{$semakan7logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s7cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl6cpo as $s6logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s6logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s6logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s6logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif

                                    {{-- pegawai bpub --}}
                                    @if(user()->department->id == '9')

                                        @if(!empty($cetaknotisbpubscpo) && !empty($cetaknotisbpubscpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cnbpubcpo" aria-expanded="false" aria-controls="cnbpubcpo">
                                                            Ulasan Semakan Oleh {{$cetaknotisbpubscpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cnbpubcpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl5cpo as $s5logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s5logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s5logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s5logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan5logcpo) && !empty($semakan5logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s5cpo" aria-expanded="false" aria-controls="s5cpo">
                                                            Ulasan Semakan Oleh {{$semakan5logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s5cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl4cpo as $s4logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s4logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s4logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s4logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan4logcpo) && !empty($semakan4logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s4cpo" aria-expanded="false" aria-controls="s4cpo">
                                                            Ulasan Semakan Oleh {{$semakan4logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s4cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl3cpo as $s3logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s3logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s3logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s3logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        
                                        @if(!empty($semakan3logcpo) && !empty($semakan3logcpo->requested_by))
                                            @if(!empty($semakan3cpo) && $semakan3cpo->approved_by == user()->id)
                                                <div class="mb-0 card card-primary">
                                                    <div class="card-header" role="tab" id="headingOne2">
                                                        <h4 class="card-title">
                                                            <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpo" aria-expanded="false" aria-controls="s3cpo">
                                                                Ulasan Semakan Oleh {{$semakan3logcpo->request->name}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="s3cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                        <div class="card-body">
                                                            @foreach($sl2cpo as $s2logcpo)
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Terima</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->approved_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Tarikh Hantar</div>
                                                                        <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->requested_at)) !!}</p>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="text-muted">Status</div>
                                                                        <p>{!! $s2logcpo->reviews_status !!}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="text-muted">Ulasan</div>
                                                                        <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpo->remarks) !!}</p>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif

                                    @endif

                                    {{-- pegawai pelaksana --}}
                                    @if(!empty($reviewcpo) && !empty($reviewcpo->create) && !empty($reviewcpo->create->department) && $reviewcpo->create->department->id == user()->department->id)
                                        
                                        @if(!empty($semakan3logcpo) && !empty($semakan3logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s3cpo" aria-expanded="false" aria-controls="s3cpo">
                                                            Ulasan Semakan Oleh {{$semakan3logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s3cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl2cpo as $s2logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s2logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s2logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s2logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(!empty($semakan2logcpo) && !empty($semakan2logcpo->requested_by))
                                            <div class="mb-0 card card-primary">
                                                <div class="card-header" role="tab" id="headingOne2">
                                                    <h4 class="card-title">
                                                        <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#s2cpo" aria-expanded="false" aria-controls="s2cpo">
                                                            Ulasan Semakan Oleh {{$semakan2logcpo->request->name}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s2cpo" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                                                    <div class="card-body">
                                                        @foreach($sl1cpo as $s1logcpo)
                                                            <div class="row">
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Terima</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->approved_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Tarikh Hantar</div>
                                                                    <p>{!! date('d/m/Y g:i A', strtotime($s1logcpo->requested_at)) !!}</p>
                                                                </div>
                                                                <div class="col-4">
                                                                    <div class="text-muted">Status</div>
                                                                    <p>{!! $s1logcpo->reviews_status !!}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="text-muted">Ulasan</div>
                                                                    <p>{!! str_replace(["\r\n","\r","\n"],"<br />",$s1logcpo->remarks) !!}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    @endif
                                @endrole
                            @endif

                        </div>
                            
                        @if(!empty($sst))
                            <div class="tab-pane fade show " id="document-contract-review-log" role="tabpanel" aria-labelledby="document-contract-review-log-tab">
                                <form id="log-form">
                                    <div class="row justify-content-center w-100">
                                        <div class="col-12 w-100">
                                            @include('contract.pre.box.partials.scripts')
                                            @component('components.card')
                                                @slot('card_body')
                                                    @component('components.datatable', 
                                                        [
                                                            'table_id' => 'contract-pre-box',
                                                            'route_name' => 'api.datatable.contract.review-log-document-contract',
                                                            'param' => 'acquisition_id=' . $sst->acquisition_id,
                                                            'columns' => [
                                                                ['data' => 'no', 'title' => __('Bil'), 'defaultContent' => '-'],
                                                                ['data' => 'penyemak', 'title' => __('Penyemak'), 'defaultContent' => '-'],
                                                                ['data' => 'jabatan', 'title' => __('Jabatan'), 'defaultContent' => '-'],
                                                                ['data' => 'semakan', 'title' => __('Ulasan Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'jenis', 'title' => __('Jenis'), 'defaultContent' => '-'],
                                                                ['data' => 'tarikh', 'title' => __('Tarikh Mula Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'tarikh1', 'title' => __('Tarikh Selesai Semakan'), 'defaultContent' => '-'],
                                                                ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
                                                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                                            ],
                                                            'headers' => [
                                                                __('Bil'),__('Penyemak'),__('Jabatan'), __('Ulasan Semakan'), __('Jenis'),__('Tarikh Mula Semakan'), __('Tarikh Selesai Semakan'),__('Status'), __('')
                                                            ],
                                                            'actions' => minify('')
                                                        ]
                                                    )
                                                    @endcomponent
                                                @endslot
                                            @endcomponent
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif

                        {{-- bon --}}
                        <div class="tab-pane fade show" id="document-contract-review-bon" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'bon_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($review))
                                <input type="hidden" id="bon_task_by" name="task_by" value="{{$review->task_by}}">
                                <input type="hidden" id="bon_law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                <input type="hidden" id="bon_created_by" name="created_by" value="{{$review->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="bon_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="bon_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="bon_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="bon_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="bons_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="bon_document_contract_type" name="document_contract_type" value="Bon Pelaksanaan" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'bon_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'bon_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewbon))
                                            {{-- @if(!empty($reviewbon->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_bon',
                                                'name' => 'requested_at',
                                            ])  
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewbon))
                                            {{-- @if(!empty($reviewbon->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_bon',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($semakan3) && $semakan3->approved_by == user()->id)
                                    @if($bon->bon_type == 1)
                                        {{-- jaminan bank --}}
                                        <div class="row">
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Bank'),
                                                    'id' => 'bank_approval_proposed_date_review',
                                                    'name' => 'bank_approval_proposed_date',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])   
                                            </div>
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Bank'),
                                                    'id' => 'bank_approval_received_date_review',
                                                    'name' => 'bank_approval_received_date',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])
                                            </div>
                                        </div>
                                    @elseif($bon->bon_type == 2)
                                        {{-- jaminan insurans --}}
                                        <div class="row">
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Insurans'),
                                                    'id' => 'insurance_approval_proposed_date_review',
                                                    'name' => 'insurance_approval_proposed_date',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])   
                                            </div>
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Insurans'),
                                                    'id' => 'insurance_approval_received_date_review',
                                                    'name' => 'insurance_approval_received_date',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])
                                            </div>
                                        </div>
                                    @endif
                                @elseif((!empty($semakan4) && $semakan4->approved_by == user()->id) || (!empty($semakan5) && $semakan5->approved_by == user()->id))

                                    @if($bon->bon_type == 1)
                                        {{-- jaminan bank --}}
                                        <div class="row">
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Bank'),
                                                    'id' => 'bank_approval_proposed_date_review',
                                                    'name' => 'bank_approval_proposed_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])   
                                            </div>
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Bank'),
                                                    'id' => 'bank_approval_received_date_review',
                                                    'name' => 'bank_approval_received_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])
                                            </div>
                                        </div>
                                    @elseif($bon->bon_type == 2)
                                        {{-- jaminan insurans --}}
                                        <div class="row">
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Insurans'),
                                                    'id' => 'insurance_approval_proposed_date_review',
                                                    'name' => 'insurance_approval_proposed_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])   
                                            </div>
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Insurans'),
                                                    'id' => 'insurance_approval_received_date_review',
                                                    'name' => 'insurance_approval_received_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])
                                            </div>
                                        </div>
                                    @endif
                                @elseif((!empty($semakan6) && $semakan6->approved_by == user()->id) || (!empty($semakan7) && $semakan7->approved_by == user()->id))

                                    @if($bon->bon_type == 1)
                                        {{-- jaminan bank --}}
                                        <div class="row d-none">
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Bank'),
                                                    'id' => 'bank_approval_proposed_date_review',
                                                    'name' => 'bank_approval_proposed_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])   
                                            </div>
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Bank'),
                                                    'id' => 'bank_approval_received_date_review',
                                                    'name' => 'bank_approval_received_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])
                                            </div>
                                        </div>
                                    @elseif($bon->bon_type == 2)
                                        {{-- jaminan insurans --}}
                                        <div class="row d-none">
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Mengemukakan Surat Pengesahan Jaminan Insurans'),
                                                    'id' => 'insurance_approval_proposed_date_review',
                                                    'name' => 'insurance_approval_proposed_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])   
                                            </div>
                                            <div class="col-6">
                                                @include('components.forms.datetimepicker', [
                                                    'input_label' => __('Tarikh Penerimaan Surat Pengesahan Jaminan Dari Insurans'),
                                                    'id' => 'insurance_approval_received_date_review',
                                                    'name' => 'insurance_approval_received_date',
                                                    'readonly' => 'true',
                                                    'config' => [
                                                        'format' => config('datetime.display.date'),
                                                    ]
                                                ])
                                            </div>
                                        </div>
                                    @endif

                                @endif

                                @if(!empty($reviewbon) && $reviewbon->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_bon',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="bon_document_contract_type" name="document_contract_type" value="Bon Pelaksanaan" type="hidden"/>
                                
                                <input id="bon_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="bon_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="bon_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewbon) && $reviewbon->created_by != user()->id)
                                        <button type="button" id="bon_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="bon_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="bon_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- insurans --}}
                        <div class="tab-pane fade show" id="document-contract-review-insurance" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'insurans_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($reviewinsurans))
                                <input type="hidden" id="insurans_task_by" name="task_by" value="{{$reviewinsurans->task_by}}">
                                <input type="hidden" id="insurans_law_task_by" name="law_task_by" value="{{$reviewinsurans->law_task_by}}">

                                <input type="hidden" id="insurans_created_by" name="created_by" value="{{$reviewinsurans->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="insurans_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="insurans_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="insurans_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="insurans_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="insurans_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="insurans_document_contract_type" name="document_contract_type" value="Insurans" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'insurans_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'insurans_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewinsurans))
                                            {{-- @if(!empty($reviewinsurans->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_insurans',
                                                'name' => 'requested_at',
                                            ]) 
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewinsurans))
                                            {{-- @if(!empty($reviewinsurans->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_insurans',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewinsurans) && $reviewinsurans->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_insurans',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="insurans_document_contract_type" name="document_contract_type" value="Insurans" type="hidden"/>
                                
                                <input id="insurans_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="insurans_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="insurans_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewinsurans) && $reviewinsurans->created_by != user()->id)
                                        <button type="button" id="insurans_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="insurans_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="insurans_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- wang pendahuluan --}}
                        <div class="tab-pane fade show" id="document-contract-review-deposit" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'deposit_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($reviewwang))
                                <input type="hidden" id="deposit_task_by" name="task_by" value="{{$reviewwang->task_by}}">
                                <input type="hidden" id="deposit_law_task_by" name="law_task_by" value="{{$reviewwang->law_task_by}}">

                                <input type="hidden" id="deposit_created_by" name="created_by" value="{{$reviewwang->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="deposit_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="deposit_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="deposit_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="deposit_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="deposits_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="deposit_document_contract_type" name="document_contract_type" value="Wang Pendahuluan" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'deposit_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'deposit_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewwang))
                                            {{-- @if(!empty($reviewwang->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_wang',
                                                'name' => 'requested_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewwang))
                                            {{-- @if(!empty($reviewwang->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_wang',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewwang) && $reviewwang->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_wang',
                                        'name' => 'remarks'
                                    ])
                                @else
                                    
                                @endif

                                <input id="deposit_document_contract_type" name="document_contract_type" value="Wang Pendahuluan" type="hidden"/>
                                
                                <input id="deposit_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="deposit_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="deposit_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewwang) && $reviewwang->created_by != user()->id)
                                        <button type="button" id="deposit_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="deposit_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="deposit_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- dokumen contract --}}
                        <div class="tab-pane fade show" id="document-contract-review-document" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'doc_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($reviewdoc))
                                <input type="hidden" id="doc_task_by" name="task_by" value="{{$reviewdoc->task_by}}">
                                <input type="hidden" id="doc_law_task_by" name="law_task_by" value="{{$reviewdoc->law_task_by}}">

                                <input type="hidden" id="doc_created_by" name="created_by" value="{{$reviewdoc->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="doc_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="doc_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="doc_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="doc_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="doc_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="doc_document_contract_type" name="document_contract_type" value="Dokumen Kontrak" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'doc_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'doc_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewdoc))
                                            {{-- @if(!empty($reviewdoc->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_doc',
                                                'name' => 'requested_at',
                                            ])  
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewdoc))
                                            {{-- @if(!empty($reviewdoc->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_doc',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewdoc) && $reviewdoc->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_doc',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="doc_document_contract_type" name="document_contract_type" value="Dokumen Kontrak" type="hidden"/>
                                
                                <input id="doc_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="doc_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="doc_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewdoc) && $reviewdoc->created_by != user()->id)
                                        <button type="button" id="doc_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="doc_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="doc_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- cpc --}}
                        <div class="tab-pane fade show" id="document-contract-review-cpc" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'cpc_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($review))
                                <input type="hidden" id="cpc_task_by" name="task_by" value="{{$review->task_by}}">
                                <input type="hidden" id="cpc_law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                <input type="hidden" id="cpc_created_by" name="created_by" value="{{$review->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="cpc_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="cpc_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="cpc_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="cpc_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="cpc_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="cpc_document_contract_type" name="document_contract_type" value="CPC" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'cpc_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'cpc_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewcpc))
                                            {{-- @if(!empty($reviewcpc->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_cpc',
                                                'name' => 'requested_at',
                                            ])  
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewcpc))
                                            {{-- @if(!empty($reviewcpc->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_cpc',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewcpc) && $reviewcpc->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_cpc',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="cpc_document_contract_type" name="document_contract_type" value="CPC" type="hidden"/>
                                
                                <input id="cpc_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="cpc_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="cpc_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewcpc) && $reviewcpc->created_by != user()->id)
                                        <button type="button" id="cpc_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="cpc_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="cpc_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- cnc --}}
                        <div class="tab-pane fade show" id="document-contract-review-cnc" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'cnc_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($review))
                                <input type="hidden" id="cnc_task_by" name="task_by" value="{{$review->task_by}}">
                                <input type="hidden" id="cnc_law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                <input type="hidden" id="cnc_created_by" name="created_by" value="{{$review->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="cnc_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="cnc_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="cnc_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="cnc_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="cnc_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="cnc_document_contract_type" name="document_contract_type" value="CNC" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'cnc_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'cnc_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewcnc))
                                            {{-- @if(!empty($reviewcnc->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_cnc',
                                                'name' => 'requested_at',
                                            ])  
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewcnc))
                                            {{-- @if(!empty($reviewcnc->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_cnc',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewcnc) && $reviewcnc->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_cnc',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="cnc_document_contract_type" name="document_contract_type" value="CNC" type="hidden"/>
                                
                                <input id="cnc_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="cnc_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="cnc_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewcnc) && $reviewcnc->created_by != user()->id)
                                        <button type="button" id="cnc_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="cnc_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="cnc_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- cpo --}}
                        <div class="tab-pane fade show" id="document-contract-review-cpo" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'cpo_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($review))
                                <input type="hidden" id="cpo_task_by" name="task_by" value="{{$review->task_by}}">
                                <input type="hidden" id="cpo_law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                <input type="hidden" id="cpo_created_by" name="created_by" value="{{$review->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="cpo_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="cpo_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="cpo_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="cpo_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="cpo_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="cpo_document_contract_type" name="document_contract_type" value="CPO" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'cpo_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'cpo_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewcpo))
                                            {{-- @if(!empty($reviewcpo->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_cpo',
                                                'name' => 'requested_at',
                                            ])  
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewcpo))
                                            {{-- @if(!empty($reviewcpo->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_cpo',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewcpo) && $reviewcpo->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_cpo',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="cpo_document_contract_type" name="document_contract_type" value="CPO" type="hidden"/>
                                
                                <input id="cpo_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="cpo_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="cpo_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewcpo) && $reviewcpo->created_by != user()->id)
                                        <button type="button" id="cpo_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="cpo_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="cpo_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                        {{-- cmgd --}}
                        <div class="tab-pane fade show" id="document-contract-review-cmgd" role="tabpanel" aria-labelledby="document-contract-review-tab">
                            <form  method="POST" action="{{ route('acquisition.review.store') }}" files="true" enctype="multipart/form-data">
                                @csrf

                                @include('components.forms.hidden', [
                                    'id' => 'id',
                                    'name' => 'id',
                                    'value' => ''
                                ])

                                @if(!empty($sst))
                                    @include('components.forms.hidden', [
                                        'id' => 'cmgd_acquisition_id',
                                        'name' => 'acquisition_id',
                                        'value' => $sst->acquisition_id
                                    ])
                                @endif

                                @if(!empty($review))
                                <input type="hidden" id="cmgd_task_by" name="task_by" value="{{$review->task_by}}">
                                <input type="hidden" id="cmgd_law_task_by" name="law_task_by" value="{{$review->law_task_by}}">

                                <input type="hidden" id="cmgd_created_by" name="created_by" value="{{$review->created_by}}">
                                @endif

                                @if(!empty($review_previous))
                                    <input type="text" id="cmgd_created_by" name="created_by" value="{{$review_previous->created_by}}" hidden>
                                    <input type="text" id="cmgd_approved_at" name="approved_at" value="{{ \Carbon\Carbon::now()->format('d/m/Y g:i A') }}" hidden>
                                    <input type="text" id="cmgd_task_by" name="task_by" value="{{$review_previous->task_by}}" hidden>
                                    <input type="text" id="cmgd_law_task_by" name="law_task_by" value="{{$review_previous->law_task_by}}" hidden>
                                    <input type="text" id="cmgd_type" name="type" value="Dokumen Kontrak" hidden>
                                    <input type="text" id="cmgd_document_contract_type" name="document_contract_type" value="CMGD" hidden>
                                @endif

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => __('Nama Pegawai Semakan'),
                                            'id' => '',
                                            'name' => '',
                                            'value' => user()->name,
                                            'readonly' => true
                                        ])
                                    </div>
                                        @include('components.forms.hidden', [
                                            'id' => 'cmgd_requested_by',
                                            'name' => 'requested_by',
                                            'value' => user()->id
                                        ])
                                        @include('components.forms.hidden', [
                                            'id' => 'cmgd_approved_by',
                                            'name' => 'approved_by',
                                            'value' => user()->supervisor->id
                                        ])
                                    <div class="col-3">
                                        @if(!empty($reviewcmgd))
                                            {{-- @if(!empty($reviewcmgd->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Terima'),
                                                'id' => 'requested_at_cmgd',
                                                'name' => 'requested_at',
                                            ])  
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                    <div class="col-3">
                                        @if(!empty($reviewcmgd))
                                            {{-- @if(!empty($reviewcmgd->status)) --}}
                                            @include('components.forms.input', [
                                                'input_label' => __('Tarikh Hantar'),
                                                'id' => 'approved_at_cmgd',
                                                'name' => 'approved_at',
                                            ])
                                            {{-- @endif --}}
                                        @endif
                                    </div>
                                </div>

                                @if(!empty($reviewcmgd) && $reviewcmgd->created_by != user()->id)
                                    @include('components.forms.textarea', [
                                        'input_label' => __('Ulasan'),
                                        'id' => 'remarks_cmgd',
                                        'name' => 'remarks'
                                    ])
                                @else

                                @endif

                                <input id="cmgd_document_contract_type" name="document_contract_type" value="CMGD" type="hidden"/>
                                
                                <input id="cmgd_rejectStatus" name="rejectStatus" type="hidden" value="1" />
                                <input id="cmgd_saveStatus" name="saveStatus" type="hidden" value="1" />
                                <input id="cmgd_sentStatus" name="sentStatus" type="hidden" value="1" />

                                <div class="btn-group float-right">
                                    @if(!empty($reviewcmgd) && $reviewcmgd->created_by != user()->id)
                                        <button type="button" id="cmgd_rejSent" class="btn btn-danger btn-default border-primary ">
                                            @icon('fe fe-corner-up-left')&nbsp; {{ __('Kuiri') }}
                                        </button>
                                        <button type="button" id="cmgd_savDraf" class="btn btn-primary btn-group float-right ">
                                            @icon('fe fe-save')&nbsp; {{ __('Simpan') }}
                                        </button>
                                    @endif
                                    <button type="button" id="cmgd_savSent" class="btn btn-default float-middle border-default ">
                                        @icon('fe fe-send') {{ __('Teratur') }}
                                    </button>
                                </div>

                            </form>
                        </div>

                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    </div>
</div>
@endsection