@extends('layouts.admin')

@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            $('#previous').show();
            $('#today').hide();
            $('#future').hide();
            $('#lastToday').hide();

            $('input[type=radio][name=sale_type]').change(function () {

                if(this.value == '1'){
                    $('#previous').show();
                    $('#today').hide();
                    $('#future').hide();
                    $('#lastToday').hide();
                }
                else if (this.value == '2'){
                    $('#previous').hide();
                    $('#today').show();
                    $('#future').hide();
                    $('#lastToday').hide();
                }
                else if (this.value == '3'){
                    $('#previous').hide();
                    $('#today').hide();
                    $('#future').show();
                    $('#lastToday').hide();
                }
                else if (this.value == '4'){
                    $('#previous').hide();
                    $('#today').hide();
                    $('#future').hide();
                    $('#lastToday').show();
                }
            });

        });
    </script>
@endpush    
@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @slot('card_body')
                    @if (session('status'))
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                
                <form method="POST" action="{{ route('sale.search') }}" >
                    @csrf
                    <div class="col pb-4">

                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalSenaraiJualan">
                        @icon('fe fe-shopping-cart') Senarai Jualan
                    </button><br>

                      <label for="ic_number">{{ __('No Kad Pengenalan') }}</label>
                      <div class="input-group">
                        <input type="text" class="form-control" required name="ic_number" id="ic_number" placeholder="IC Number">
                        <div class="input-group-append">
                            <label class="selectgroup-item mb-0">
                                <input type="radio" name="sale_status" value="1" class="selectgroup-input" checked>
                                <span class="selectgroup-button selectgroup-button-icon">{{ __('Jualan') }}</span>
                            </label>
                            <label class="selectgroup-item mb-0">
                                <input type="radio" name="sale_status" value="2" class="selectgroup-input">
                                <span class="selectgroup-button selectgroup-button-icon">{{ __('Kemaskini') }}</span>
                            </label>
                        </div>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary submit-action-btn">
                                @icon('fe fe-search') Carian
                            </button>
                        </div>
                      </div>
                    </div>
                </form>
                @endslot
            @endcomponent
        </div>
    </div>

<div class="modal fade" id="modalSenaraiJualan" role="dialog" aria-labelledby="modalSenaraiJualanModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSenaraiJualanModalLongTitle">Senarai Jualan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="selectgroup w-100">
                                <label class="selectgroup-item">
                                    <input type="radio" name="sale_type" value="1" class="selectgroup-input" checked>
                                    <span class="selectgroup-button selectgroup-button-icon">{{ __('Sudah Tamat') }}</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="sale_type" value="2" class="selectgroup-input">
                                    <span class="selectgroup-button selectgroup-button-icon">{{ __('Hari Ini') }}</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="sale_type" value="3" class="selectgroup-input">
                                    <span class="selectgroup-button selectgroup-button-icon">{{ __('Akan Datang') }}</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="sale_type" value="4" class="selectgroup-input">
                                    <span class="selectgroup-button selectgroup-button-icon">{{ __('Tamat Hari Ini') }}</span>
                                </label>
                            </div>
                        </div>

                        <div id="previous">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    @component('components.card')
                                        @slot('card_body')
                                            @component('components.datatable', 
                                                [
                                                    'table_id' => 'contract-pre-sale-previous',
                                                    'route_name' => 'api.datatable.contract.saleprevious',
                                                    'columns' => [
                                                        ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                                        ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                                        ['data' => 'start_sale_at', 'title' => __('Tarikh Mula'), 'defaultContent' => '-'],
                                                        ['data' => 'closed_sale_at', 'title' => __('Tarikh Tamat'), 'defaultContent' => '-'],
                                                        ['data' => '', 'title' => __(''), 'defaultContent' => '-'],
                                                    ],
                                                    'headers' => [
                                                        __('No. Perolehan'), __('Tajuk'), __('Tarikh Mula'), __('Tarikh Tamat'), __(''),
                                                    ],
                                                    'actions' => minify('')
                                                ]
                                            )
                                            @endcomponent
                                        @endslot
                                    @endcomponent
                                </div>
                            </div>
                        </div>

                        <div id="today">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    @component('components.card')
                                        @slot('card_body')
                                            @component('components.datatable', 
                                                [
                                                    'table_id' => 'contract-pre-sale-today',
                                                    'route_name' => 'api.datatable.contract.saletoday',
                                                    'columns' => [
                                                        ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                                        ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                                        ['data' => 'start_sale_at', 'title' => __('Tarikh Mula'), 'defaultContent' => '-'],
                                                        ['data' => 'closed_sale_at', 'title' => __('Tarikh Tamat'), 'defaultContent' => '-'],
                                                        ['data' => '', 'title' => __(''), 'defaultContent' => '-'],
                                                    ],
                                                    'headers' => [
                                                        __('No. Perolehan'), __('Tajuk'), __('Tarikh Mula'), __('Tarikh Tamat'), __(''),
                                                    ],
                                                    'actions' => minify('')
                                                ]
                                            )
                                            @endcomponent
                                        @endslot
                                    @endcomponent
                                </div>
                            </div>
                        </div>

                        <div id="future">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    @component('components.card')
                                        @slot('card_body')
                                            @component('components.datatable', 
                                                [
                                                    'table_id' => 'contract-pre-sale-future',
                                                    'route_name' => 'api.datatable.contract.salefuture',
                                                    'columns' => [
                                                        ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                                        ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                                        ['data' => 'start_sale_at', 'title' => __('Tarikh Mula'), 'defaultContent' => '-'],
                                                        ['data' => 'closed_sale_at', 'title' => __('Tarikh Tamat'), 'defaultContent' => '-'],
                                                        ['data' => '', 'title' => __(''), 'defaultContent' => '-'],
                                                    ],
                                                    'headers' => [
                                                        __('No. Perolehan'), __('Tajuk'), __('Tarikh Mula'), __('Tarikh Tamat'), __(''),
                                                    ],
                                                    'actions' => minify('')
                                                ]
                                            )
                                            @endcomponent
                                        @endslot
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                        
                        <div id="lastToday">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    @include('sales.partials.scripts')
                                    @component('components.card')
                                        @slot('card_body')
                                            @component('components.datatable', 
                                                [
                                                    'table_id' => 'contract-last-sale-today',
                                                    'route_name' => 'api.datatable.contract.salelasttoday',
                                                    'columns' => [
                                                        ['data' => 'reference', 'title' => __('No. Perolehan'), 'defaultContent' => '-'],
                                                        ['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
                                                        ['data' => 'start_sale_at', 'title' => __('Tarikh Mula'), 'defaultContent' => '-'],
                                                        ['data' => 'closed_sale_at', 'title' => __('Tarikh Tamat'), 'defaultContent' => '-'],
                                                        ['data' => null, 'title' => __('Laporan'), 'searchable' => false, 'orderable' => false],
                                                    ],
                                                    'headers' => [
                                                        __('No. Perolehan'), __('Tajuk'), __('Tarikh Mula'), __('Tarikh Tamat'), __(''),
                                                    ],
                                                    'actions' => minify(view('sales.partials.actions')->render())
                                                ]
                                            )
                                            @endcomponent
                                        @endslot
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
