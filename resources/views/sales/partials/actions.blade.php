<div class="text-center">
    <div class="item-action dropdown">
        <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
              <i class="fe fe-more-vertical"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
          style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
          {{ $prepend_action or '' }}
            <a class="dropdown-item file1-action-btn" style="cursor: pointer;"
                    data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '" >
                    {{ __('Senarai Jualan') }}
            </a>
<!--            <a class="dropdown-item file2-action-btn" style="cursor: pointer;"
                    data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
                    {{ __('Folder Jualan') }}
            </a>
            <a class="dropdown-item file3-action-btn" style="cursor: pointer;"
                    data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
                    {{ __('Senarai Semak') }}
            </a>-->
        </div>
    </div>
</div>

