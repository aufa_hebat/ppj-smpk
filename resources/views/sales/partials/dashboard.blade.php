<div class="row">
    @forelse(dashboard()->sales() as $item)
        <div class="col-4">
            @include('components.cards.figure', [
                'header' => $item['header'],
                'icon' => $item['icon'],
                'content' => $item['content'],
                'footer' => $item['footer'],
            ])
        </div>
    @empty
        <div class="col">
            @include('components.cards.empty-info', ['header' => 'Maklumat Jualan'])
        </div>
    @endforelse
</div>