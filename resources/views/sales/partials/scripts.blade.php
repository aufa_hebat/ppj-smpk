@push('scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
            $(document).on('click', '.file1-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    window.open(route('sale_list', {hashslug:id}), '_blank');
            });
            $(document).on('click', '.file2-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    window.open(route('sale_folder', {hashslug:id}), '_blank');
            });
            $(document).on('click', '.file3-action-btn', function(event) {
                    event.preventDefault();
                    var id = $(this).data('hashslug');
                    window.open(route('sale_folder_verify', {hashslug:id}), '_blank');
            });
	});
</script>
@endpush