@extends('layouts.admin')
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
@include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        function getName(id,val){
            var $value = val.value;
            $('#subFile'+id).val($value.split('\\').pop());
        }
        jQuery(document).ready(function($) {
            @if(!empty($sale))
                @foreach($sale as $key=>$sal)
                    @if(!empty($sal->document))
                        $('#subFile{{ $sal->document->document_id }}').val('{{ $sal->document->document_name }}');
                    @endif
                @endforeach
            @endif
            
            $("#table_resit").DataTable();
            
            $(document).on('click', '.buang', function(event) {
               
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('sale.destroy', id))
                        .then(response => {
                            swal('Jualan', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });
            
            $(document).on('click', '.hantar-action-btn', function(event) {
                event.preventDefault();
                var route_name = 'sale.update';
                var form_id = $(this).data('form');
                
                var form = document.forms[form_id];
                var data = new FormData(form);
                
                var table = $("#table_resit").DataTable(); 
                var y = table.$('input[type=text]');
                var z = table.$('input[type=file]');
                $.each(y, function(i, input){
                    data.append(input.name,input.value);
                });
                $.each(z, function(i, input){
                    if (input.value) data.append(input.name, input.files[0]);
                });
                
                axios.post(route(route_name, {{ $ic_number }}), data).then(response => {
                    if(response.data.message != '')
                        swal('Jualan', response.data.message, 'success').then((result)=>{
                            location.reload();
                        });
                    else
                        location.reload();
                });
            });
        });
    </script>
@endpush

@section('content')
<form id="form-receipt" files="true" enctype="multipart/form-data">
    @csrf  
    @method('PUT')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @slot('card_title')
                
                <div class="card">
                    <div class="row">
                        <div class="col-md-6 col-lg-6" style="text-align: center">
                            <div class="form-group">
                                <div class="text-muted">Nama Pembeli</div>
                                <input type="text" name="name" id="name" value="{{ $owner->pluck('name')->first() ??  ($agent->pluck('name')->first() ?? '') }}" hidden>
                                <div>{{ $owner->pluck('name')->first() ??  ($agent->pluck('name')->first() ?? '') }}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center">
                            <div class="form-group">
                                <div class="text-muted">No Kad Pengenalan</div>
                                <input type="text" id="ic_number" name="ic_number" value="{{ $ic_number }}" hidden>
                                <div>{{ $ic_number }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endslot
                @slot('card_body')
                <h4>Senarai Dokumen</h4>
                <div  class="table-responsive">
                    <table class="table table-bordered" id="table_resit" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th width="5%">Bil</th>
                                <th width="30%">Tajuk</th>
                                <th width="20%">Syarikat</th>
                                <th width="5%">Status Pembeli</th>
                                <th width="5%">No Siri</th>
                                <th width="10%">No Resit</th>
                                @if(!empty($check_lepas) && $check_lepas != null)
                                <th width="20%">Kebenaran Khas</th>
                                <th width="20%">Sebab</th>
                                @else
                                <th width="20%">Kebenaran Khas</th>
                                @endif
                                <th width="5%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($sale))
                                @php 
                                    $cnt1 = 0; $cnt2 = 0;
                                @endphp
                                @foreach($sale as $key=>$sal)
                                @if(user()->current_role_login == 'administrator' || user()->current_role_login == 'developer')
                                    @php $cnt1 = $cnt1 + 1; @endphp
                                    <tr>
                                        <td>{{ $cnt1 }}</td>
                                        <td>{{ $sal->acquisition->reference." : ".$sal->acquisition->title }}
                                            <input type="text" id="acquisition_id{{ $sal->id }}" name="receipt[{{ $sal->id }}][acq]" value="{{ $sal->acquisition_id }}" hidden/>
                                        </td>
                                        <td>{{ $sal->company->company_name }}</td>
                                        <td>{{ ($sal->buyer_status == 1)? "PEMILIK":"WAKIL" }}</td>
                                        <td>{{ $sal->series_no ?? null }}</td>
                                        <td>
                                            @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                            <input type="text" class="form-control" id="receipt_no{{ $sal->id }}" name="receipt[{{ $sal->id }}][resit]" value="{{ $sal->receipt_no ?? null }}">
                                            <input type="text" name="receipt[{{ $sal->id }}][edit]" id="edit{{ $sal->id }}" value="{{(!empty($sal->receipt_no)? '2':'1') }}" hidden/>
                                            <input type="text" name="receipt[{{ $sal->id }}][id]" id="receipt_id{{ $sal->id }}" value="{{ $sal->id }}" hidden/>
                                            @else
                                                {{ $sal->receipt_no }}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="form-group row">
                                                <div class="col input-group">
                                                    @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                                        <input id="subFile{{ $sal->id }}" type="text"  class="form-control" readonly>
                                                        <label class="input-group-text" for="uploadFile{{ $sal->id }}"><i class="fe fe-upload" ></i></label>
                                                        <input type="file" class="form-control" id="uploadFile{{ $sal->id }}" name="document{{ $sal->id }}" onChange="getName({{ $sal->id }},this)" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                                        @if(!empty($sal->document))
                                                            <label class="input-group-text" for="downloadFile">
                                                                <a href="/download/{{ $sal->document->document_path }}/{{ $sal->document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                                            </label>
                                                            <label class="input-group-text" for="deleteFile{{ $sal->document->id }}">
                                                                <input type="input" id="deleteFile{{ $sal->document->id }}" class="buang" value="{{ $sal->document->hashslug }}" hidden>
                                                                <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                                            </label>
                                                        @endif 
                                                    @else
                                                        @if(!empty($sal->document))
                                                            <a href="/download/{{ $sal->document->document_path }}/{{ $sal->document->document_name }}" target="_blank">{{ $sal->document->document_name }}</a>
                                                        @endif
                                                    @endif                            
                                                </div>
                                            </div>
                                        </td>
                                        @if(!empty($check_lepas) && $check_lepas != null)
                                        <td>
                                            @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                                @if(in_array($sal->id,$check_lepas))
                                                    <input type="text" id="reason_buy{{ $sal->id }}" name="receipt[{{ $sal->id }}][reason_buy]" class="form-control" 
                                                    @if(!empty($sal->reason_buy))
                                                        value = "{{ $sal->reason_buy }}"
                                                    @endif
                                                    >
                                                @endif
                                            @endif
                                        </td>
                                        @endif
                                        <td>
                                            <a href="{{ route('sale_form',['hashslug' => $sal->hashslug]) }}" target="_blank"
                                                    class="btn btn-success border-success">
                                                    @icon('fe fe-printer'){{ __(' Cetak') }}
                                            </a>
                                        </td>
                                    </tr>
                                @else
                                    @if(empty($sal->acquisition->sst))
                                    @php $cnt2 = $cnt2 + 1; @endphp
                                    <tr>
                                        <td>{{ $cnt2 }}</td>
                                        <td>{{ $sal->acquisition->reference." : ".$sal->acquisition->title }}
                                            @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                                <input type="text" id="acquisition_id{{ $sal->id }}" name="receipt[{{ $sal->id }}][acq]" value="{{ $sal->acquisition_id }}" hidden/>
                                            @else
                                                {{ $sal->acquisition_id }}
                                            @endif
                                        </td>
                                        <td>{{ $sal->company->company_name }}</td>
                                        <td>{{ ($sal->buyer_status == 1)? "PEMILIK":"WAKIL" }}</td>
                                        <td>{{ $sal->series_no ?? null }}</td>
                                        <td>
                                            @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                                <input type="text" class="form-control" id="receipt_no{{ $sal->id }}" name="receipt[{{ $sal->id }}][resit]" value="{{ $sal->receipt_no ?? null }}">
                                                <input type="text" name="receipt[{{ $sal->id }}][edit]" id="edit{{ $sal->id }}" value="{{(!empty($sal->receipt_no)? '2':'1') }}" hidden/>
                                                <input type="text" name="receipt[{{ $sal->id }}][id]" id="receipt_id{{ $sal->id }}" value="{{ $sal->id }}" hidden/>
                                            @else
                                                {{ $sal->receipt_no }}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="form-group row">
                                                <div class="col input-group">
                                                    @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                                        <input id="subFile{{ $sal->id }}" type="text"  class="form-control" readonly>
                                                        <label class="input-group-text" for="uploadFile{{ $sal->id }}"><i class="fe fe-upload" ></i></label>
                                                        <input type="file" class="form-control" id="uploadFile{{ $sal->id }}" name="document{{ $sal->id }}" onChange="getName({{ $sal->id }},this)" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">

                                                        @if(!empty($sal->document))
                                                            <label class="input-group-text" for="downloadFile">
                                                                <a href="/download/{{ $sal->document->document_path }}/{{ $sal->document->document_name }}" target="_blank"><i class="fas fa-download"></i></a>
                                                            </label>
                                                            <label class="input-group-text" for="deleteFile{{ $sal->document->id }}">
                                                                <input type="input" id="deleteFile{{ $sal->document->id }}" class="buang" value="{{ $sal->document->hashslug }}" hidden>
                                                                <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                                            </label>
                                                        @endif    
                                                    @else
                                                        @if(!empty($sal->document))
                                                            <a href="/download/{{ $sal->document->document_path }}/{{ $sal->document->document_name }}" target="_blank">{{ $sal->document->document_name }}</a>
                                                        @endif
                                                    @endif
                                                                                        
                                                </div>
                                            </div>
                                        </td>
                                        @if(!empty($check_lepas) && $check_lepas != null)
                                        <td>
                                            @if($sal->acquisition->status_task == 'Jualan' || $sal->acquisition->status_task == 'Buka Peti')
                                                @if(in_array($sal->id,$check_lepas))
                                                <input type="text" id="reason_buy{{ $sal->id }}" name="receipt[{{ $sal->id }}][reason_buy]" class="form-control" 
                                                @if(!empty($sal->reason_buy))
                                                    value = "{{ $sal->reason_buy }}"
                                                @endif
                                                >
                                                @endif
                                            @else
                                                {{ $sal->reason_buy }}
                                            @endif
                                        </td>
                                        @endif
                                        <td>
                                            <a href="{{ route('sale_form',['hashslug' => $sal->hashslug]) }}" target="_blank"
                                                    class="btn btn-success border-success">
                                                    @icon('fe fe-printer'){{ __(' Cetak') }}
                                            </a>
                                        </td>
                                    </tr>
                                    @endif
                                @endif
                                
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                <div class="btn-group float-right">
                    {{ html()->a(route('sale.index'), __('Carian'))->class('btn btn-default border-primary') }}
                    <button type="submit" class="btn btn-primary float-right hantar-action-btn" data-form="form-receipt">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
            
                @endslot
            @endcomponent
            
        </div>
    </div>
</form>
@endsection