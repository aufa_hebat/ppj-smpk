@extends('layouts.admin')
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
@include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        function release(val1,val2,val3,val4){
            var checkBox = document.getElementById("sale_"+val1+"_status");
            
            if (checkBox.checked == true){
                $("#sale_"+val1+"_letter").prop('readonly',false);
                $("#sale_"+val1+"_acq").val(val2);
                $("#sale_"+val1+"_buyer").val(val3);
                $("#sale_"+val1+"_comp").val(val4);
            } else {
                $("#sale_"+val1+"_letter").val('');
                $("#sale_"+val1+"_acq").val('');
                $("#sale_"+val1+"_buyer").val('');
                $("#sale_"+val1+"_comp").val('');
                $("#sale_"+val1+"_letter").prop('readonly',true);
            }
        }
        jQuery(document).ready(function($) {
            $('.select2').select2();
            $('.letter').prop('readonly',true);
            
            $("#ic_number").val("{{$ic_number}}")
            $("#name").val("{{ $owner->pluck('name')->first() ??  ($agent->pluck('name')->first() ?? '') }}");
            $("#telephone_num").val("{{ $owner->pluck('telephone_num')->first() ?? ( $agent->pluck('telephone_num')->first() ?? '' ) }}");
            
            
            $(document).on('click', '.hantar-action-btn', function(event) {
                event.preventDefault();
                var route_name = 'sale.store';
                var form_id = $(this).data('form');
                
                var form = document.forms[form_id];
                var data = new FormData(form);
                @foreach($list_acq as $row)
                    var table = $("#table_jualan{{ $row->id }}").DataTable();
                    var x = table.$('input');
                    $.each(x, function(i, input){
                        data.append(input.name,input.value);
//                        console.log(input.name,input.value);
                    });
                @endforeach
                
                
                axios.post(route(route_name), data).then(response => {
                    if(response.data.message != '')
                        swal('Jualan', response.data.message, 'success').then((result)=>{
                              redirect(route('sale.receipt', '{{$ic_number}}'));
                        });
                    else
                        redirect(route('sale.receipt', '{{$ic_number}}'));
                    
                        
                    
                });
            });
        });
        
        
    </script>
@endpush

@section('content')
<form id="form-jual" method="POST" action="{{ route('sale.store') }}" >
    @csrf  
    <!--@method('PUT')-->
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @slot('card_title')
                
                <div class="card">
                    <div class="row">
                        <div class="col-md-6 col-lg-6" style="text-align: center">
                            <div class="form-group">
                                <div class="text-muted">Nama Pembeli</div>
                                <input type="text" name="name" id="name" value="{{ $owner->pluck('name')->first() ??  ($agent->pluck('name')->first() ?? '') }}" hidden>
                                <div>{{ $owner->pluck('name')->first() ??  ($agent->pluck('name')->first() ?? '') }}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6" style="text-align: center">
                            <div class="form-group">
                                <div class="text-muted">No Kad Pengenalan</div>
                                <input type="text" id="ic_number" name="ic_number" value="{{ $ic_number }}" hidden>
                                <div>{{ $ic_number }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endslot
                @slot('card_body')
                <h4>Senarai Dokumen</h4>
                <div class="table-responsive table-bordered">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th width="5%">Bil.</th>
                            <th width="10%">No Perolehan</th>
                            <th width="70%">Tajuk</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list_acq as $bil => $row)
                        @push('scripts')
                            <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    $("#table_jualan{{ $row->id }}").DataTable();
                                });
                            </script>
                        @endpush
                        <tr>
                            <td>{{ $bil+1 }}</td>
                            <td>{{ $row->reference }}</td>
                            <td>{!! str_replace(["\r\n","\r","\n"],"  ",$row->title) !!}
                            <br> TAKLIMAT: {!! ($row->briefing_status == '1')?  "ADA":"<span style='color:red'>TIADA</span>" !!}
                            <br> 
                            @if(!empty($cidb))
                                @foreach($cidb as $cdb)
                                    @if($cdb['acq_id'] == $row->id)
                                        {!! $cdb['cidb'] !!}
                                    @endif
                                @endforeach
                            @endif
                            @if(!empty($mof))
                                @foreach($mof as $mf)
                                    @if($mf['acq_id'] == $row->id)
                                        {!! $mf['mof'] !!}
                                    @endif
                                @endforeach
                            @endif
                            <table class="table table-bordered" id="table_jualan{{ $row->id }}" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>SYARIKAT</th>
                                        <th>PEMILIK/WAKIL</th>
                                        <th>BIDANG KELAYAKAN</th>
                                        <th>KEBENARAN KHAS</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($sales as $bills => $sale)
                                @if($row->id == $sale['acq_id'])
                                    @if($sale['comp_name'] != null)
                                        
                                            <tr>
                                                <td>{{ $sale['comp_name'] }}</td>
                                                <td>{{ $sale['status_type'] }}</td>
                                                <td>
                                                @if($sale['cidb_grade'] != null)
                                                <b>CIDB : </b><br>GRED  
                                                    @foreach($sale['cidb_grade'] as $c1=>$sG) 
                                                        <b>{{ $sG }}</b> 
                                                    @endforeach
                                                @endif
                                                @if($sale['cidb_category'] != null)
                                                <br>KATEGORI 
                                                    @foreach($sale['cidb_category'] as $sC) 
                                                        <b>{{ $sC }}</b> 
                                                    @endforeach 
                                                @endif
                                                @if($sale['cidb_khusus'] != null)
                                                <br>PENGKHUSUSAN 
                                                    @foreach($sale['cidb_khusus'] as $sK) 
                                                        <b>{{ $sK }}</b> 
                                                    @endforeach 
                                                @endif
                                                @if($sale['mof_area'] != null)
                                                <b>MOF : </b><br>BIDANG 
                                                    @foreach($sale['mof_area'] as $mA) 
                                                        <b>{{ $mA }}</b> 
                                                    @endforeach
                                                @endif
                                                @if($sale['mof_khusus'] != null)
                                                <br>PENGKHUSUSAN 
                                                    @foreach($sale['mof_khusus'] as $mK) 
                                                        <b>{{ $mK }}</b> 
                                                    @endforeach
                                                @endif
                                                </td>
                                                <td @if(!empty($sale['kebenaranC1']) || !empty($sale['kebenaranC2']) || !empty($sale['kebenaranM1']) || !empty($sale['kebenaranM2'])) style="color:red" @endif>
                                                    
                                                    <!-- cidb -->
                                                    @if(!empty($sale['kebenaranC1']) || !empty($sale['kebenaranC2']))
                                                       *Tiada CIDB : 
                                                       @if(!empty($sale['kebenaranC1']))
                                                            <br>Gred {{ $sale['kebenaranC1'] }}
                                                       @endif   
                                                       @if(!empty($sale['kebenaranC3']))
                                                            <br>Kategori {{ $sale['kebenaranC3'] }}
                                                       @endif
                                                       @if(!empty($sale['kebenaranC2']))
                                                            <br>Pengkhususan {{ $sale['kebenaranC2'] }}
                                                       @endif
                                                        <input type="text" id="sale_{{ $bil+1 }}_{{ $bills+1 }}_letter" name="sale['{{ $bil+1 }}_{{ $bills+1 }}'][letter]" class="letter form-control">
                                                    @endif
                                                    <!-- cidb -->
                                                    <!-- mof -->
                                                    @if(!empty($sale['kebenaranM1']) || !empty($sale['kebenaranM2']))
                                                       *Tiada MOF : 
                                                       @if(!empty($sale['kebenaranM1']))
                                                            <br>Bidang {{ $sale['kebenaranM1'] }}
                                                       @endif   
                                                       @if(!empty($sale['kebenaranM2']))
                                                            <br>Pengkhususan {{ $sale['kebenaranM2'] }}
                                                       @endif
                                                        <input type="text" id="sale_{{ $bil+1 }}_{{ $bills+1 }}_letter" name="sale['{{ $bil+1 }}_{{ $bills+1 }}'][letter]" class="letter form-control">
                                                    @endif
                                                    <!-- mof -->
                                                    @if(!empty($sale['kebenaranC']))
                                                        @foreach($sale['kebenaranC'] as $kbnrn)
                                                            {!! $kbnrn !!}
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!empty($check_sale) && $check_sale->count() > 0 && $check_sale->where('acquisition_id',$sale['acq_id'])->where('company_id',$sale['comp_id'])->count() > 0)
                                                        ✔
                                                    @else
                                                        <input type="text" id="sale_{{ $bil+1 }}_{{ $bills+1 }}_acq" name="sale['{{ $bil+1 }}_{{ $bills+1 }}'][acq]" hidden>
                                                        <input type="text" id="sale_{{ $bil+1 }}_{{ $bills+1 }}_comp" name="sale['{{ $bil+1 }}_{{ $bills+1 }}'][comp]" hidden>
                                                        <input type="text" id="sale_{{ $bil+1 }}_{{ $bills+1 }}_buyer" name="sale['{{ $bil+1 }}_{{ $bills+1 }}'][buyer]" hidden>
                                                        <input id="sale_{{ $bil+1 }}_{{ $bills+1 }}_status" name="sale['{{ $bil+1 }}_{{ $bills+1 }}'][status]" onclick="release('{{ $bil+1 }}_{{ $bills+1 }}','{{ $row->id }}','{{ $sale['buyer'] }}','{{ $sale['comp_id'] }}')" type="checkbox">
                                                    @endif
                                                    
                                                </td>
                                            </tr>
                                        
                                    @endif
                                @endif
                                @endforeach
                                </tbody>
                            </table>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="btn-group float-right">
                    {{ html()->a(route('sale.index'), __('Kembali'))->class('btn btn-default border-primary') }}
                    <button type="submit" class="btn btn-primary float-right hantar-action-btn" data-form="form-jual">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
            
                @endslot
            @endcomponent
            
        </div>
    </div>
</form>
@endsection