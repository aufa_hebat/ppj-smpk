<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
        .sm-content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        .newbr {
            border-left: hidden;
            border-right: hidden;
        }
</style>

<center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>
<br>
<div class="title" style="text-align: center">
    <span style="text-transform: uppercase; font-weight: bold;">SENARAI 
            @if(strstr( $acq->category->name, "Tender"))
            Petender
            @elseif(strstr( $acq->category->name, "Sebut Harga"))
            Penyebut Harga
            @else
            Pembeli
            @endif
    </span>
</div>
<br>
<div class="title">
    <table border="0" style="width: 100%; padding: 4px;" align="center">
        <tr>
            <td style="width: 18%;font-weight: bold;">Jenis Perolehan</td>
            <td style="width: 2%;padding: 4px;">:</td>
            <td style="width: 30%;padding: 4px;" >
                @if(strstr( $acq->category->name, "Tender"))
                Tender
                @else
                Sebut Harga
                @endif
            </td>
            <td style="width: 18%;font-weight: bold;">No Perolehan</td>
            <td style="width: 2%;padding: 4px;">:</td>
            <td style="width: 30%;padding: 4px;" >{{ $acq->reference ?? '' }}</td>
        </tr>
        <tr>
            <td valign="top" style="font-weight: bold;padding: 4px;">Nama Projek</td>
            <td valign="top" style="padding: 4px;">:</td>
            <td colspan="4"  style="padding: 4px;">
                {!! $acq->title !!}
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;padding: 4px;">Tarikh Tutup</td>
            <td style="padding: 4px;">:</td>
            <td style="padding: 4px;">{{( !empty($acq->closed_at))? date('d/m/Y',strtotime($acq->closed_at)):'' }}</td>
            <td style="font-weight: bold;padding: 4px;">Bil. Kontraktor</td>
            <td style="padding: 4px;">:</td>
            <td style="padding: 4px;">{{ $sales_list->count() ?? '' }}</td>
        </tr>
    </table>
</div>
<div class="content">
    <table border="1" style="width: 100%;border-collapse: collapse; padding: 4px 4px;" align="center">
        <thead>
            <tr>
                <th style="background: lightgray; width:4%;padding: 4px; " align="center" >Bil</th>
                <th style="background: lightgray; width:20%;padding: 4px;" align="center" >Nama Syarikat</th>
                <th style="background: lightgray; width:25%;padding: 4px;" align="center" >Alamat</th>
                <th style="background: lightgray; width:10%;padding: 4px;" align="center" >No Telefon</th>
                <th style="background: lightgray; width:10%;padding: 4px;" align="center" >No Tel Bimbit</th>
                <th style="background: lightgray; width:10%;padding: 4px;" align="center" >No Faksimili</th>
                <th style="background: lightgray; width:5%;padding: 4px;" align="center" >No Siri</th>
                <th style="background: lightgray; width:10%;padding: 4px;" align="center" >No Resit</th>
            </tr>
        </thead>
        <tbody>
        @if(!empty($sales_list))
        @foreach($sales_list as $bil => $row)
        
        <tr>
            <td style="padding: 4px;" align="center">{{ $bil + 1}}</td>
            <td style="padding: 4px;">{{ $row->company->company_name}}</td>
            <td style="padding: 4px;">{{ $row->company->addresses[0]->primary ?? '' }}{{ ", ".$row->company->addresses[0]->secondary ?? '' }}{{ ", ".$row->company->addresses[0]->postcode ?? ''}}{{ ", ".$row->company->addresses[0]->state ?? ''}}</td>
            <td style="padding: 4px;">{{ $row->company->phones[0]->phone_number ?? '' }}</td>
            <td style="padding: 4px;">{{ $row->company->phones[1]->phone_number ?? '' }}</td>
            <td style="padding: 4px;">{{ $row->company->phones[2]->phone_number ?? '' }}</td>
            <td style="padding: 4px;">{{ $row->series_no ?? '' }}</td>
            <td style="padding: 4px;">{{ $row->receipt_no?? '' }}</td>
        </tr>
        
        @endforeach
        @endif
        <tr>
            <td style="background: lightgray;"></td>
            <td colspan="7" style="background: lightgray; font-weight: bold;padding: 4px;">
                Bilangan Penyebut Harga / Petender = ({{ $sales_list->count() ?? '' }})
            </td>
        </tr>
        </tbody>
    </table>
</div>
