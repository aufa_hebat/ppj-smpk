<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
        .sm-content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        .newbr {
            border-left: hidden;
            border-right: hidden;
        }
</style>

<center><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></center><br>

    <table border="1" style="width: 100%;border-collapse: collapse;padding: 4px;" >
        <tr class="title">
            <td colspan="6" style="background-color: lightgray;font-weight: bold" align="center">MAKLUMAT PEROLEHAN</td>
        </tr>
        <tr class="title">
            <td colspan="6" style="text-transform: uppercase;font-weight: bold" align="center">
                @if(strstr( $acq->category->name, "Tender"))
                Tender
                @else
                Sebut Harga
                @endif
            </td>
        </tr>
        <tr class="title">
            <td colspan="6" style="text-transform: uppercase;font-weight: bold" align="center">{{ $acq->title }}</td>
        </tr>
        <tr class="title">
            <td colspan="6" align="center" style="font-weight: bold">{{ $acq->reference }}</td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px">Pegawai Pelaksana 1 </td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($pelaksana1))
                    {{ $acq->user->name }}
                @endif
            </td>
            <td style="font-weight: bold;padding:4px">Samb Tel :</td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($pelaksana1))
                    {{ $acq->user->phone_no }}
                @endif
            </td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px">Pegawai Pelaksana 2 </td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($pelaksana1))
                    {{ $pelaksana1->name }}
                @endif
            </td>
            <td style="font-weight: bold;padding:4px">Samb Tel :</td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($pelaksana1))
                    {{ $pelaksana1->phone_no }}
                @endif
            </td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px">Pegawai BPUB 1</td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($bpub1))
                    {{ $bpub1->name }}
                @endif
            </td>
            <td style="font-weight: bold;padding:4px">Samb Tel :</td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($bpub1))
                    {{ $bpub1->phone_no }}
                @endif
            </td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px">Pegawai BPUB 2</td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($bpub2))
                    {{ $bpub2->name }}
                @endif
            </td>
            <td style="font-weight: bold;padding:4px">Samb Tel :</td>
            <td colspan="2" style="padding:4px;">
                @if(!empty($bpub2))
                    {{ $bpub2->phone_no }}
                @endif
            </td>
        </tr>
        <tr class="content">
            <td style="width: 30%;font-weight: bold;padding:4px">Kelayakan</td>
            <td  colspan="5" style="padding:4px;">
                @if(!empty($acq) && !empty($acq->approval) && ($acq->approval->acquisition_type_id == 1 || $acq->approval->acquisition_type_id == 3))
                    @if(!empty($acq) && !empty($acq->approval) && !empty($acq->approval->mofQualifications) && $acq->approval->mofQualifications->isNotEmpty())

                        Berdaftar dengan Kementarian Kewangan di bawah Kod Bidang 
                        
                        @php $cnt1 = 0; @endphp
                        {{--  Kategori : 
                        @foreach($acq->approval->mofQualifications as $cnt1=>$cetak5)                    
                            @if($cetak5->code->type == 'category')
                                @php $cnt1 = $cnt1 + 1; @endphp
                                {{$cetak5->code->name}}
                                @if($cetak5->status == 1)
                                    <b>DAN</b>
                                @elseif($cetak5->status == 0)
                                    <b>ATAU</b>
                                @else
                                    <b>;&nbsp;</b>
                                @endif
                            @endif
                        @endforeach 
                        <br />  --}}
                        {{--  Pengkhususan :   --}}
                        @php $cnt2 = 0; @endphp
                        @foreach($acq->approval->mofQualifications as $cnt2=>$cetak6)
                            @if($cetak6->code->type == 'khusus')
                                @php $cnt2 = $cnt2 + 1; @endphp
                                {{$cetak6->code->code}}
                                @if($cetak6->status == 1)
                                    <b>DAN</b>
                                @elseif($cetak6->status == 0)
                                    <b>ATAU</b>
                                @else
                                <b>&nbsp;</b>
                                @endif                        
                            @endif
                        @endforeach   
                        
                        , &nbsp;
                        
                        @if($acq->approval->mof == '1')Sijil Taraf Bumiputera (MOF), @endif 

                        Serta Surat Wakil Kuasa yang ditandatangani oleh PEMILIK SYARIKAT.

                    @endif
                @else
                    @if(!empty($acq) && !empty($acq->approval) && !empty($acq->approval->cidbQualifications) && $acq->approval->cidbQualifications->isNotEmpty())
                        @if($acq->approval->cidb == '1')Sijil Perakuan Pendaftaran Kontraktor CIDB (PPK), @endif 
                        @if($acq->approval->spkk == '1')Sijil Perolehan Kerja Kerajaan (SPKK) @endif                                                                         

                        @php $cnt3 = 0; @endphp
                        Gred 
                        @foreach($acq->approval->cidbQualifications as $cnt3=>$cetak)
                            @if($cetak->code->type == 'grade')
                                @php $cnt3 = $cnt3 + 1; @endphp
                                {{$cetak->code->code}} 
                                @if($cetak->status == 1)
                                    <b>DAN</b>
                                @elseif($cetak->status == 0)
                                    <b>ATAU</b>
                                @else
                                <b>&nbsp;</b>
                                @endif
                            @endif                    
                        @endforeach 
                        
                        &nbsp;
                        
                        @php $cnt4 = 0; @endphp
                        Kategori 
                        @foreach($acq->approval->cidbQualifications as $cnt4=>$cetak)                    
                            @if($cetak->code->type == 'category')
                                @php $cnt4 = $cnt4 + 1; @endphp
                                {{$cetak->code->code}}
                                @if($cetak->status == 1)
                                    <b>DAN</b>
                                @elseif($cetak->status == 0)
                                    <b>ATAU</b>
                                @else
                                    <b>&nbsp;</b>
                                @endif
                            @endif
                        @endforeach 
                        
                        &nbsp;

                        Pengkhususan 
                        @php $cnt5 = 0; @endphp
                        @foreach($acq->approval->cidbQualifications as $cnt5=>$cetak)
                            @if($cetak->code->type == 'khusus')
                                @php $cnt5 = $cnt5 + 1; @endphp
                                {{$cetak->code->code}}
                                @if($cetak->status == 1)
                                    <b>DAN</b>
                                @elseif($cetak->status == 0)
                                    <b>ATAU</b>
                                @else
                                    <b>&nbsp;</b>
                                @endif                      
                            @endif
                        @endforeach                                 
                        
                        ,&nbsp;
                        
                        @if($acq->approval->bpku_bumiputera == '1')Sijil Taraf Bumiputera (BPKU) @endif
                        Serta Surat Wakil Kuasa yang ditandatangani oleh PEMILIK SYARIKAT.

                    @endif
                @endif
            </td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px">Taklimat</td>
            <td colspan="2" style="padding:4px;">@if(($acq->briefing_status) == 1) DIWAJIBKAN @endif</td>
            <td style="font-weight: bold;padding:4px">Lawatan Tapak</td>
            <td colspan="2" style="padding:4px;">@if(($acq->visit_status) == 1) DIWAJIBKAN @endif</td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px">Harga Jualan (RM)</td>
            <td colspan="5" style="padding:4px;">{{ money()->toCommon($acq->document_price ?? "0" , 2) }}</td>
<!--            <td width="14%" style="font-weight: bold">Harga SST</td>
            <td width="15%" >{{ money()->toCommon($acq->gst_price ?? "0" , 2) }}</td>
            <td width="15%" style="font-weight: bold">Jumlah (RM)</td>
            <td width="13%" >{{ money()->toCommon($acq->total_document_price ?? "0" , 2) }}</td>-->
        </tr>
    </table>
    <br>
    <table border="1" style="width: 100%;border-collapse: collapse;padding: 4px;">
        <tr class="title">
            <td colspan="4" style="font-weight: bold;background-color: lightgray;" align="center">MAKLUMAT PENJUALAN</td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px;">Tarikh Jualan</td>
            <td style="padding:4px;">{{(!empty($acq->start_sale_at))? date('d/m/Y',strtotime($acq->start_sale_at)):''}}</td>
            <td style="font-weight: bold;padding:4px;">Hingga</td>
            <td style="padding:4px;">{{(!empty($acq->closed_sale_at))? date('d/m/Y',strtotime($acq->closed_sale_at)):''}}</td>
        </tr>
        <tr class="content">
            <td style="font-weight: bold;padding:4px;">Tarikh Tutup</td>
            <td style="padding:4px;">{{(!empty($acq->closed_at))? date('d/m/Y',strtotime($acq->closed_at)):''}}</td>
            <td></td>
            <td></td>
        </tr>
    </table>
