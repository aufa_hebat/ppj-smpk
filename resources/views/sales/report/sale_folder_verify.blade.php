<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
</style>

<div class="title" style="text-align:center">
    <span style="text-transform: uppercase; font-weight: bold;">SENARAI SEMAK FOLDER JUALAN</span>
</div>
<br>
<div class="title">
    <table style="width: 100%; border-collapse: collapse; padding: 4px;" class="content">
        <tr>
            <td style="text-transform: uppercase;padding: 4px;">TAJUK 
                @if($acq->category->id == 1 || $acq->category->id == 2)
                    Sebut Harga 
                @elseif($acq->category->id == 3 || $acq->category->id == 4)
                    Tender 
                @else
                    {{ $acq->category->name }} 
                @endif
                : <b>{{ $acq->title }}</b></td>
        </tr>
        <tr>
            <td><div style="height: 5px;"></div><td>
        </tr>
        <tr>
            <td style="text-transform: uppercase;padding: 4px;">NO. 
                @if($acq->category->id == 1 || $acq->category->id == 2)
                    Sebut Harga 
                @elseif($acq->category->id == 3 || $acq->category->id == 4)
                    Tender 
                @else
                    {{ $acq->category->name }} 
                @endif
                : <b>{{ $acq->reference }}</b></td>
        </tr>
        <tr>
            <td><div style="height: 5px;"></div><td>
        </tr>        
        <tr>
            <td style="padding: 4px;">TARIKH IKLAN/NOTIS : <b>{{(!empty($acq->advertised_at))? date('d/m/Y',strtotime($acq->advertised_at)):''}}</b></td>
        </tr>
        <tr>
            <td><div style="height: 5px;"></div><td>
        </tr>        
        <tr>
            <td style="padding: 4px;">TARIKH TUTUP : <b>{{(!empty($acq->closed_at))? date('d/m/Y',strtotime($acq->closed_at)):''}}</b></td>
        </tr>
    </table>
</div>
<div class="content">
    <table border="1" style="width: 100%;border-collapse: collapse;padding: 4px;" align="center" class="content">
        <tr style="background-color: lightgray;">
          <th style="width: 5%;padding: 4px;">BIL</th>
          <th style="width: 40%;padding: 4px;">PERKARA</th>
          <th style="width: 15%;padding: 4px;">TANDAKAN</th>
          <th style="width: 40%;padding: 4px;">CATATAN/DOKUMEN RUJUKAN</th>
        </tr>
        <tr>
          <td style="padding: 4px;" align="center">1.</td>
          <td style="padding: 4px;">Notis Sebut Harga/Iklan Tender</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="padding: 4px;" align="center">2.</td>
          <td style="padding: 4px;">Kertas Pertimbangan (YANG TELAH DILULUSKAN)</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="padding: 4px;" align="center">3.</td>
          <td style="padding: 4px;">Memo drp Jabatan untuk tujuan Notis dan Bilangan Dokumen Hantar untuk Penjualan</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td style="padding: 4px;" align="center">4.</td>
          <td style="padding: 4px;">Tampal Notis di Papan Kenyataan</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">5.</td>
            <td style="padding: 4px;">Pamer Dokumen Meja Tender. Muka surat yang wajib dicop<br>
                <table>
                    <tr>
                      <td></td>
                      <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;a)&nbsp;&nbsp;</td>
                      <td>Kulit muka depan - <b>COP</b> 'Dokumen Tawaran Meja' dan tarikh tutup</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;b)&nbsp;&nbsp;</td>
                      <td>Ringkasan Sebut Harga/Senarai Kuantiti - <b>COP</b> 'Dokumen Tawaran Meja'</td>
                    </tr>
                </table>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">6.</td>
            <td style="padding: 4px;">Senarai Taklimat dan Lawatan Tapak di terima(Jika Berkaitan)</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">7.</td>
            <td style="padding: 4px;">ADDENDUM Diterima untuk edaran</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">8.</td>
            <td style="padding: 4px;">Senarai Nama-Nama Pembeli di kemaskini</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">9.</td>
            <td style="padding: 4px;">ANGGARAN JABATAN telah dimasukkan ke dalam Peti Tender/Sebut Harga </td>
            <td></td>
            <td style="padding: 4px;">Borang Akuan Serahan Cadangan Harga Jabatan</td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">10.</td>
            <td style="padding: 4px;">Folder Jualan dipinjamkan untuk tujuan Penilaian Sebut Harga/Tender<br><br>Tujuan(Sila Catat)</td>
            <td></td>
            <td style="padding: 4px;">Nama Peminjam : <br>Jabatan : <br><br>Ext : <br><br>Dipinjam drp : </td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">11.</td>
            <td>Folder Jualan dikembalikan</td>
            <td></td>
            <td style="padding: 4px;">Nama Peminjam : <br>Jabatan : <br><br>Ext : </td>
        </tr>
        <tr>
            <td style="padding: 4px;" align="center">12.</td>
            <td style="padding: 4px;">Folder Jualan dalam keadaan teratur</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>
<br>
<div class="content">
    <table border="1" style="width: 100%;border-collapse: collapse;padding: 4px;" align="center" class="content">
        <tr>
            <th style="padding: 4px;">Nama dan Tandatangan Pengemaskini Terakhir Dan Cop Jawatan.</th>
            <!--<th style="width: 10%;"></th>-->
            <th style="padding: 4px;">Nama dan Tandatangan Pegawai yang memeriksa dan Cop Jawatan</th>
        </tr>
        <tr>
            <td style="padding: 4px;"><br><br><br><br>...........................................................<br>Tarikh:<br></td>
            <!--<td></td>-->
            <td style="padding: 4px;"><br><br><br><br>...........................................................<br>Tarikh:<br></td>
        </tr>
    </table>
</div>
