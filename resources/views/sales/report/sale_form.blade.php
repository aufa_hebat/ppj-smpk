<style type="text/css">
    @page {
            margin: 60px 70px 60px 100px;
        }
        div.breakNow { page-break-inside:avoid; page-break-after:always; }
        .title {
            font-size: 14px;
            font-family: "Arial, Helvetica, sans-serif";
        }

        .content {
            font-size: 12px;
            font-family: "Arial, Helvetica, sans-serif";
        }
        
        .bordered {
            border-color: #959594;
            border-style: solid;
            border-width: 1px;
	}
</style>

<table width="90%" align="center" class="title">
    <td width="20%"><img style="width: 100px; height: 100px;" src="{{ logo($print) }}"></td>
    <td width="80%">
        <span style="font-weight: bold; text-transform: uppercase;">
            BORANG JUALAN 
            @if(strstr( $sales->acquisition->category->name, "Tender"))
            Tender
            @else
            Sebut Harga
            @endif
        </span>
        <br /><br />
        <span style="font-weight: normal; text-transform: uppercase;">
            BAHAGIAN PEROLEHAN & UKUR BAHAN<br>JABATAN KEWANGAN<br>PERBADANAN PUTRAJAYA
        </span>
    </td>
</table>
<br />
<hr>
<table width="95%" align="center" class="content">
    <tr>
        <td>
            <div >
                <table border="0" style="width: 100%; padding: 4px;" align="center">
                    <tr>
                        <td style="width: 26%;"><b>No. Perolehan : </b></td>
                        <td style="width: 45%;" colspan="3" >{{ $sales->acquisition->reference }}</td>
                        <td style="width: 30%;" colspan="2"></td>
                    </tr>
                    <tr>
                        <td ><b>Harga Dokumen : </b></td>
                        <td colspan="5"> {{ money()->toHuman($sales->acquisition->document_price ?? "0" , 2) }}</td>
<!--                        <td style="width: 15%;"><b>&nbsp;*SST 0%</td>
                        <td style="width: 15%;" >&nbsp;&nbsp;{{ money()->toHuman($sales->acquisition->gst_price ?? "0" , 2) }}</td>
                        <td style="width: 15%;"><b>&nbsp;Jumlah : </b></td>
                        <td style="width: 15%;" >&nbsp;{{ money()->toHuman($sales->acquisition->total_document_price ?? "0" , 2) }}</td>-->
                    </tr>
                    <tr>
                        <td style="width: 26%;"><b>Nama Projek : </b></td>
                        <td style="width: 75%;"colspan="5" >{{ $sales->acquisition->title }}</td>
                    </tr>
                </table>
            </div>
            <div style="height: 5px;"></div>
            <div >
                <table border="0" style="width: 100%; padding: 4px;" align="center">
                    <tr>
                        <td><b>Nama Syarikat : </b></td>
                        <td colspan="2">{{ $sales->company->company_name }}</td>
                    </tr>
                    <tr>
                        <td><b>Alamat Surat-Menyurat : </b></td>
                        <td colspan="2" >{{ $sales->company->addresses[0]->primary ?? '' }} {{ $sales->company->addresses[0]->secondary ?? '' }} {{ $sales->company->addresses[0]->postcode ?? '' }} {{ $sales->company->addresses[0]->state ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><b>Nama Pembeli : </b></td>
                        <td colspan="2" >
                            {{ (!empty($pembeli)? $pembeli->name:null) }}
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width: 26%;"><b>No KP : </b></td>
                        <td style="width: 45%;" >{{ $sales->ic_number }}</td>
                        <td style="width: 30%;"></td>
                    </tr>
                    <tr>
                        <td><b>Status Pembeli : </b></td>
                        <td >{{ ($sales->buyer_status == 1)? 'Pemilik Syarikat':'Wakil Syarikat' }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>No Tel (Pejabat) : </b></td>
                        <td >{{ $sales->company->phones[0]->phone_number ?? '' }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>No Tel (Bimbit) : </b></td>
                        <td >
                            @if($sales->buyer_status == 1 && !empty($sales->sale_owner) && $sales->sale_owner->count() > 0)
                                {{ (!empty($sales->sale_owner->where('company_id',$sales->company_id)->first()) && !empty($sales->sale_owner->where('company_id',$sales->company_id)->first()->phones[0]))? $sales->sale_owner->where('company_id',$sales->company_id)->first()->phones[0]->phone_number:null }}
                            @elseif($sales->buyer_status == 2 && !empty($sales->sale_agent) && $sales->sale_agent->count() > 0)
                                {{ $sales->sale_agent->pluck('telephone_num')->first() }}
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>No Faks : </b></td>
                        <td >{{ $sales->company->phones[2]->phone_number ?? '' }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Tandatangan Pembeli : </b></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div><br><br><br><br></div>
            <div >
                <table style="width: 100%; padding: 4px;" align="center">
                    <tr>
                        <td style="width: 26%;"><b>Petugas Kaunter : </b></td>
                        <td colspan="2" >{{ (!empty($officer))? $officer->name:null }}</td>
                    </tr>
                    <tr>
                        <td style="width: 26%;" valign="top"><b>Tandatangan Petugas : </b></td>
                        <td style="width: 44%;"><br><br><br><br><br><br></td>
                        <td style="width: 30%;" valign="top">
                            <table width="100%">
                                <tr>
                                    <td style="width: 40%;"><b>No Resit : </b></td>
                                    <td style="width: 60%;" >{{ $sales->receipt_no ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 40%;"><b>No Siri : </b></td>
                                    <td style="width: 60%;">{{ $sales->series_no }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Tarikh : </b>{{ \Carbon::intlFormat(now()) }}</td>
                        <td colspan="2"></td>
                    </tr>
                </table>
            </div>
            <div style="height: 5px;"></div>
            <div>
                <table style="width: 95%;border-collapse: collapse;padding: 4px;" align="left">
                    <tr>
                        <td colspan="4">
                            <span >*Kadar 0% SST adalah daripada harga jualan dokumen Tender/ Sebut Harga / RFP</span>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><span >Tandakan (/) jika maklumat tidak lengkap : </span></td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <input type="checkbox" />
                        </td>
                        <td valign="top" colspan="2">
                            <span style="color: black;">Dokumen Belum Diserah</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="5%" colspan="2">
                            <input type="checkbox" />
                        </td>
                        <td valign="top" width="95%" colspan="2">
                            <span style="color: black;">Salinan Sijil</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <input type="checkbox" />
                        </td>
                        <td valign="top" colspan="2">
                            <span style="color: black;">Surat Perwakilan Kuasa</span>
                        </td>
                    </tr>
                    @if(!empty($check_lepas) && $check_lepas == true)
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td width="20%"><b>Petugas : </b></td>
                        <td width="20%">{{ (!empty($officer))? $officer->name:null }}</td>
                        <td width="20%" align="right"><b>Sebab : </b></td>
                        <td width="20%">{{ (!empty($sales->reason_buy))? $sales->reason_buy:null }}</td>
                    </tr>
                    
                    @endif
                </table>
            </div>
        </td>
    </tr>
</table>

