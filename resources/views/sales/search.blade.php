@extends('layouts.admin')
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush
@push('scripts')
@include('components.forms.assets.select2')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script>
        
        jQuery(document).ready(function($) {
            $('.select2').select2();
            $("#table_owner").DataTable();
            $("#ic_number").val("{{$ic_number}}")
            $("#name").val("{{ $owner->pluck('name')->first() ??  ($agent->pluck('name')->first() ?? '') }}");
            $("#telephone_num").val("{{ $owner->pluck('telephone_num')->first() ?? ( $agent->pluck('telephone_num')->first() ?? '' ) }}");
            
        });

    </script>
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @slot('card_title')
                    Senarai Syarikat yang diwakili
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalTambahWakil">
                        @icon('fe fe-plus') {{ __('Wakil Syarikat') }}
                    </button>
                @endslot
                @slot('card_body')
                <div class="table-responsive">
                    <table id="table_owner" class="table table-sm table-transparent table-hover">
                        <thead>
                            <th>Nama Syarikat</th>
                            <th>Nama Pemilik</th>
                            <th>Status Pemilik</th>
                        </thead>
                        <tbody>
                            @if(!empty($owner))
                                @foreach($owner as $own)
                                    <tr>
                                        <td>{{$own->company->company_name}}</td>
                                        <td>{{$own->name}}</td>
                                        <td>Pemilik Syarikat</td>
                                    </tr>
                                @endforeach
                            @endif
                            @if(!empty($owner))
                                @foreach($agent as $agen)
                                    <tr>
                                        <td>{{$agen->company->company_name}}</td>
                                        <td>{{$agen->name}}</td>
                                        <td>Wakil Syarikat</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @endslot

                @slot('card_footer')
                     <div class="row">
                        <div class="col">
                            <div class="btn-group float-right">
                                {{ html()->a(route('sale.index'), __('Kembali'))->class('btn btn-default border-primary') }}
                                <a class="btn btn-primary float-right" href="{{ route('sale.show',$ic_number) }}">
                                    {{ __('Jualan') }}@icon('fe fe-arrow-right')
                                </a>
                            </div>
                        </div>
                    </div>
                @endslot
                
            @endcomponent
        </div>
    </div>

    <div class="modal fade" id="modalTambahWakil" role="dialog" aria-labelledby="modalTambahWakilModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTambahWakilModalLongTitle">Tambah Syarikat yang diwakilkan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form class="col"  method="POST" action="{{ route('sale.wakil') }}" files="true" enctype="multipart/form-data">
                            @csrf
                            @include('components.forms.input', [
                                'input_label' => __('No Kad Pengenalan'),
                                'id' => 'ic_number',
                                'name' => 'ic_number',
                                'readonly' => 'readonly'
                            ])
                            @include('components.forms.input', [
                                'input_label' => __('Nama'),
                                'id' => 'name',
                                'name' => 'name',
                            ])
                            @include('components.forms.input', [
                                'input_label' => __('No Telefon'),
                                'id' => 'telephone_num',
                                'name' => 'telephone_num',
                            ])
                            @include('components.forms.select-multiple', [
                                'input_label' => __('Syarikat'),
                                'options' => $company->pluck('company_name', 'id'),
                                'id' => 'company_id',
                                'name' => 'company_id',
                            ])
                            <div class="btn-group float-right">
                                <button type="submit" class="btn btn-primary btn-group float-right">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection