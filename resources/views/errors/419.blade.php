@component('layouts.error', ['title' => __('Laman telah tamat tempoh kerana tidak aktif')])
	@slot('message')
		{{ __('Sila log masuk untuk meneruskan') }} 
		<a href="{{ url('/') }}" class="btn btn-primary ml-2 mr-2">{{ __('Log Masuk') }}</a>
	@endslot
@endcomponent