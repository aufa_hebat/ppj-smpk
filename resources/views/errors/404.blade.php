@extends('layouts.error', [
	'title' => __('Laman tidak dijumpai'),
	'message' => __('Maaf, laman yang anda cari tidak dapat dijumpai.'),
])