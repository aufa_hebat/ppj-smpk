@extends('layouts.error', [
	'title' => __('Percubaan Akses Terlarang'),
	'message' => __('Anda tidak dibenarkan mengakses kawasan ini.'),
])