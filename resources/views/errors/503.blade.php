@extends('layouts.error', [
	'title' => __('Penyelenggaraan'),
	'message' => __('Kami menghadapi masalah sekarang. Kami akan kembali lagi. Terima kasih!'),
])