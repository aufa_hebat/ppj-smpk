@extends('layouts.error', [
	'title' => __('Rekod tidak dijumpai'),
	'message' => __('Maaf, rekod yang anda cari tidak dapat dijumpai.'),
])