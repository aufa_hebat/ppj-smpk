@extends('layouts.app')

@section('body')
	@if(finance_users()->count() == 0)
		<div class="alert alert-danger text-center">
			@icon('fe fe-alert-octagon') {{ config('app.name') }} masih belum mempunyai pengguna Jabatan Kewangan, Gred 41 dan ke atas.
			Klik <a href="{{ route('manage.users.index') }}">di sini</a> untuk menjana pengguna Jabatan Kewangan. @icon('fe fe-alert-octagon') 
		</div>
	@endif
	<div class="container">
		<div class="row">
			<div class="col">
                            @if($tag == '6MonthToEnd')
                            <h1>Senarai Kontrak akan Tamat dalam Tempoh 6 Bulan</h1>
                            @elseif($tag == 'ContractOnGoing')                          
                            <h1>Senarai Kontrak Sedang Berjalan</h1>                             
                            @elseif($tag == 'contractsNotSignedYet')                          
                            <h1>Senarai Kontrak Belum Di Tandatangan</h1>                          
                            @elseif($tag == 'contractsDoneButNotSofaYet')
                            <h1>Senarai Kerja Telah Siap Tetapi Belum Di SOFA</h1>                          
                            @else
                              {{ Breadcrumbs::render() }}
                            @endif

                       
				@yield('content')
			</div>
		</div>
	</div><!-- /.container -->
@endsection