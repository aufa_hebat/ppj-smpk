
    <div id="app" class="row justify-content-center">
        <main class="py-4 mt-5 col-4">
            <div class="text-center mt-5 mb-5">
                @include('components.logo', ['height' => '128px'])
            </div>
            @component('components.card')
                @slot('card_body')
                    <div class="text-center">
                        @icon('fe fa-2x fe-alert-triangle text-danger')
                        <p class="font-weight-light">
                            {{ $message }}
                        </p>
                    </div>
                @endslot
            @endcomponent
        </main>
    </div>
