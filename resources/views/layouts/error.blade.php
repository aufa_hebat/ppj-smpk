<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('components.meta')

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    
    <link href="{{ theme('url', 'assets/css/dashboard.css', 'tabler') }}" rel="stylesheet" />
    <link href="{{ asset('css/balloon.css') }}" rel="stylesheet">
    <link href="{{ asset('css/layout1.css') }}" rel="stylesheet">

    @stack('styles')
</head>
<body>
    <div id="app" class="row justify-content-center">
        <main class="py-4 mt-5 col-4">
            <div class="text-center mt-5 mb-5">
                @include('components.logo', ['height' => '128px'])
            </div>
            @component('components.card')
                @slot('card_body')
                    <div class="text-center">
                        @icon('fe fa-2x fe-alert-triangle text-danger')
                        <p class="font-weight-light">
                            {{ $message }}
                        </p>
                    </div>
                @endslot
            @endcomponent
        </main>
    </div>
    @include('components.preloader')
    @include('components.scripts')
    <script type="text/javascript">function redirect(url){window.location = url;}</script>
</body>
</html>