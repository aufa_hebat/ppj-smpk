@extends('layouts.app')

@section('body')
	@if(finance_users()->count() == 0)
		<div class="alert alert-danger text-center">
			@icon('fe fe-alert-octagon') {{ config('app.name') }} masih belum mempunyai pengguna Jabatan Kewangan, Gred 41 dan ke atas.
			Klik <a href="{{ route('manage.users.index') }}">di sini</a> untuk menjana pengguna Jabatan Kewangan. @icon('fe fe-alert-octagon') 
		</div>
	@endif
	<div class="container">
		<div class="row">
			<div class="col">
				{{ Breadcrumbs::render() }}
				@yield('content')
			</div>
		</div>
	</div><!-- /.container -->
@endsection