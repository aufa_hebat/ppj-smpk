<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    
    <style type="text/css">
    /* Loader */
/* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript, 
if it's not present, don't show loader */

/*.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url("/img/Preloader_3.gif") center no-repeat #fff;
}
*/
</style>
    
    
    <!-- DIN: try to avoid back button -->
    <script type="text/javascript">
        
        
         document.addEventListener("keyup", function (e) {
    var keyCode = e.keyCode ? e.keyCode : e.which;
            if (keyCode == 44) {
                stopPrntScr();
            }
        });
function stopPrntScr() {

            var inpFld = document.createElement("input");
            inpFld.setAttribute("value", ".");
            inpFld.setAttribute("width", "0");
            inpFld.style.height = "0px";
            inpFld.style.width = "0px";
            inpFld.style.border = "0px";
            document.body.appendChild(inpFld);
            inpFld.select();
            document.execCommand("copy");
            inpFld.remove(inpFld);
        }
       function AccessClipboardData() {
            try {
                window.clipboardData.setData('text', "Access   Restricted");
            } catch (err) {
            }
        }
        setInterval("AccessClipboardData()", 300);
        
        
        
        
        
        function preventBack() { 
            //alert('prevent back');
            
            //
            
            var urlx = window.location.href;
            if(urlx.includes('ipc_invoice_edit')){
            
             //nothing
           }else{
              // alert('windows prevent foward!');
                window.history.forward(); 
           }
        
    }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>
    
     {{--  @include('components.preloader')
    @include('components.scripts')
    <script type="text/javascript">
$( document ).ready(function() {
	// Animate loader off screen
	$(".se-pre-con").fadeOut("slow");;
        
        

        
});



</script>  --}}
    
    @include('components.meta')

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    
    <link href="{{ theme('url', 'assets/css/dashboard.css', 'tabler') }}" rel="stylesheet" />
    <link href="{{ asset('css/balloon.css') }}" rel="stylesheet">
    <link href="{{ asset('css/layout1.css') }}" rel="stylesheet">

    @stack('styles')
</head>
<body onchange="preventBack()">
    <div class="se-pre-con"></div>
    <div id="app">
        @include('components.navigations.nav')

        <main class="py-4">
            @yield('body')
        </main>
    </div>
    @include('components.preloader')
    @include('components.scripts')
    <script type="text/javascript">function redirect(url){window.location = url;}</script>
</body>
</html>