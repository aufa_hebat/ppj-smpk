@extends('layouts.public.base1')

@section('body')
    {{--  <div class="container">  --}}
    <div>
        <div class="row">
            <div class="col mt-1">
                @yield('content')
            </div>
        </div>
    </div><!-- /.container -->
@endsection