@extends('layouts.public.base')

@section('body')
    <div>
        <div class="row">
            {{--  <div class="col mt-5">  --}}
            <div class="col mt-1">                
                @yield('content')
            </div>
        </div>
    </div><!-- /.container -->
@endsection