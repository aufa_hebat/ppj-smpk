<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('components.meta')

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
        
        <link href="{{ theme('url', 'assets/css/dashboard.css', 'tabler') }}" rel="stylesheet" />
        <link href="{{ asset('css/balloon.css') }}" rel="stylesheet">
        <link href="{{ asset('css/layout1.css') }}" rel="stylesheet">

        @stack('styles')
    </head>
    <body class="body-bg">
        <div id="app">
            @include('components.navigations.nav')

            <main class="content-bg py-4">
                @yield('body')
            </main>
        </div>
        @include('components.preloader')
        @include('components.scripts')
        <script type="text/javascript">function redirect(url){window.location = url;}</script>
    </body>
</html>