@extends('layouts.admin')

@section('content')
	<div class="row justify-content-center">
		<div class="col">
			@include('contract.pre.partials.scripts')
			@component('components.forms.hidden-form', 
				[
					'id' => 'destroy-record-form',
					'action' => route('sales.destroy', 'dummy')
				])
				@slot('inputs')
					@method('DELETE')
				@endslot
			@endcomponent
			@component('components.card')
				@slot('card_title')
					<a href="{{ route('sales.create') }}" class="btn btn-primary float-right">
						@icon('fe fe-plus') {{ __('Jualan Baru') }}
					</a>
				@endslot
			@endcomponent
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'contract-pre-management',
							'route_name' => 'api.datatable.contract.sale',
							'columns' => [
								['data' => 'series_no', 'title' => __('No. Siri'), 'defaultContent' => '-'],
								['data' => 'receipt_no', 'title' => __('No. Resit'), 'defaultContent' => '-'],
								['data' => 'sales_date', 'title' => __('Tarikh Penjualan'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('No. Siri'), __('No. Resit'),__('Tarikh Penjualan'), __('table.action')
							],
							'actions' => minify(view('contract.pre.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection