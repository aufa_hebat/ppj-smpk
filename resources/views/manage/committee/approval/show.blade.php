@extends('layouts.admin')

@include('components.forms.assets.datetimepicker')

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            
            $('#appointment_date').val("{{ !empty($officer1) ? optional($officer1->appointment_date)->format('d/m/Y') : optional($approval->closed_at)->format('d/m/Y') }}");
            $(".addmore").on('click',function(){
                var $tr = $("#tableJKPembuka").find('tr[id^="klon"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
    //              $klon.find('#bil_'+currnum).attr('id','bil_'+num).attr('name','baru['+num+'][bil]').val('');
                  $klon.find('#appointment_date_'+currnum).attr('id','appointment_date_'+num).attr('name','baru['+num+'][appointment_date]').val('');
                  $klon.find('#name_'+currnum).attr('id','name_'+num).attr('name','baru['+num+'][name]').val('');
                $('.addmore').parents('table').append($klon);
            });
            $('.buang').click(function () {
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        $(this).parents('tr').detach();
                    }
                });
            });

            $('select').click(function() {
                var myOpt = [];
                $("select").each(function () {
                    myOpt.push($(this).val());
                });
                $("select").each(function () {
                    $(this).find("option").prop('hidden', false);
                    var sel = $(this);
                    $.each(myOpt, function(key, value) {
                        if((value != "") && (value != sel.val())) {
                            sel.find("option").filter('[value="' + value +'"]').prop('hidden', true);
                        }
                    });
                });
            });

        });


    </script>
@endpush

@section('content')
@component('components.forms.hidden-form', 
    [
        'id' => 'destroy-record-form',
        'action' => route('manage.committee.approvals.destroy', 'dummy')
    ])
    @slot('inputs')
        @method('DELETE')
    @endslot
@endcomponent
@include('manage.committee.approval.partials.scripts')
@include('manage.committee.approval.partials.styles')
<div class="row">
    <div class="col-12">
        @component('components.card')
            @slot('card_body')
                <h4>Maklumat Perolehan</h4>
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="text-muted">No Sebut Harga</div>
                                <div>{!! $approval->reference !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="text-muted">Tajuk</div>
                                <div>{!! $approval->title !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endslot
        @endcomponent
        @component('components.card')
            @slot('card_body')
            <form class="form-horizontal" method="POST" action="{{route('manage.committee.approvals.store')}}" files="true" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="text" name="acqID" id="acqID" value="{{$approval->id}}" hidden>
                <input type="text" name="hashslug" id="hashslug" value="{{$approval->hashslug}}" hidden>
                <h4>Maklumat Ahli Jawatankuasa Pembuka</h4>
                <div class="row">
                    
                    <!-- If admin or developer, no have on key press -->
                     @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                       
                      <div class="col-3">
                        @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Pelantikan'),
                            'id' => 'appointment_date',
                            'name' => 'appointment_date',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ],
                            'onkeypress' => 'return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))'
                        ])
                    </div>
                     @else
                      <div class="col-3">
                        @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Pelantikan'),
                            'id' => 'appointment_date',
                            'name' => 'appointment_date',
                            'config' => [
                                'format' => config('datetime.display.date'),
                            ]
                        ])
                    </div>
                     @endif
                    
                   
                    
                    
                    
                </div><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                            <button type="button" id="addDetail href="#" class="btn btn-primary addmore">
                                @icon('fe fe-plus') {{ __('Ahli Baru') }}
                            </button>
                             @endif
                        </div>
                        <div class="form-group">
                            <div id="tableJKPembuka" class="table table-responsive">
                                <table class="table table-sm table-transparent">
                                    <tr>
                                        <th width="30%"><b>Ahli Jawatankuasa Pembuka</b></th>
                                        <th width="5%"><b><span class="addmore">HAPUS</span></b></th>
                                    </tr>
                                    @if(!empty($officer))
                                    @foreach($officer as $jk)
                                    <tr>
                                        <td>
                                            <input type="hidden" name="exist[{{$jk->id}}][id]" value="{{$jk->id}}">
                                            <span>{{$jk->name}}</span>
                                        </td>
                                        <td>
                                            @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                                            <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    <tr id="klon0" class="d-none">
                                        <td>
                                            <select class="form-control input-lg" id="name_0" name="baru[0][name]" style="width: 100%;">
                                                <option value=""></option>
                                                @foreach($user as $users)
                                                    <option value="{{$users->name}}">{{$users->name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                         @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                                        <td>
                                            <button type="button" class="remove btn btn-danger buang" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button>
                                        </td>
                                         @endif
                                    </tr>
                               </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-group pull-right">
                    <a href="{{ route('manage.committee.approvals.index') }}" 
                            class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                    </a>
                     @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                    <button type="submit" class="btn btn-primary float-right">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                     @endif
                </div>
            </form>
            @endslot
        @endcomponent
    </div>
</div>

@endsection