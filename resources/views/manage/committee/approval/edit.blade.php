@extends('layouts.admin')

@push('scripts')
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/datetimepicker.js') }}"></script>
    
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(function () {

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            })

        })
    </script>
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#approval').val('{{ $officer->approval }}');

            $('#name').val('{{ $officer->name }}');
            $('#department_ids').val('{{ $officer->department->name }}');
            $('#position_ids').val('{{ $officer->position->name }}');
            $('#role').val('{{ $officer->role }}');

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Syarikat') !!}', response.data.message, 'success');
                });
            });
        });
    </script>
    <script>
        $('#name').change(function() {
            // alert(this.value);
            @foreach($user as $users)
            // alert('{{$users->id}}');
                    if(this.value == '{{$users->name}}'){
                        $('#department_ids').val('{{ $users->department->name }}');
                        $('#department_idss').val('{{ $users->department_id }}');
                        $('#position_ids').val('{{ $users->position->name }}');
                        $('#position_idss').val('{{ $users->position_id }}');
                    }
                    else if(this.value == ""){
                        $('#department_ids').val('');
                        $('#position_ids').val('');
                    }
                    @endforeach
            });
    </script>
@endpush

@section('content')
<form method="POST" action="{{ route('manage.committee.approvals.update',$officer->id) }}" files="true" enctype="multipart/form-data">
    @csrf
    @method('PUT')
	<div class="row">
        @component('components.card', ['card_classes' => 'col-12'])
            @slot('card_body')

                @include('components.forms.hidden', [
                    'id' => 'acquisition_id',
                    'name' => 'acquisition_id',
                    'value' => $officer->acquisition_id
                ])

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="datePicker" class="control-label">Tarikh Perlantikan</label>
                            <input type="text" class="form-control input-lg datepicker" id="appointment_date" name="appointment_date" format='dd/mm/yyyy' value="@if(!empty($officer->appointment_date)){!! date('d/m/Y', strtotime($officer->appointment_date)) !!}@endif"
                                onkeypress="return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))"
                            >
                        </div>
                        
                        @include('components.forms.input', [
                            'input_label' => 'Jabatan',
                            'name' => '',
                            'id' => 'department_ids'
                        ])

                        @include('components.forms.hidden', [
                            'name' => 'department_id',
                            'id' => 'department_idss',
                            'value' => ''
                        ])

                        @include('components.forms.select', [
                            'input_label' => __('Kelulusan'),
                            'options' => $kelulusan->pluck('description','description'),
                            'name' => 'approval',
                            'id' => 'approval',
                        ])
                    </div>
                    <div class="col-6" style="margin-top: -10px;">
                        @include('components.forms.select', [
                            'input_label' => __('Nama Ahli'),
                            'options' => $user->where('department_id','!=',NULL)->pluck('name','name') ,
                            'name' => 'name',
                            'id' => 'name',
                        ])

                        @include('components.forms.input', [
                            'input_label' => 'Jawatan',
                            'name' => '',
                            'id' => 'position_ids'
                        ])

                        @include('components.forms.hidden', [
                            'name' => 'position_id',
                            'id' => 'position_idss',
                            'value' => ''
                        ])

                        @include('components.forms.select', [
                            'input_label' => __('Peranan'),
                            'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                            'name' => 'role',
                            'id' => 'role',
                        ])
                    </div>
                </div>

                <button type="submit" class="btn btn-primary float-right">
                    @icon('fe fe-save') {{ __('Simpan') }}
                </button>
        @endslot
    @endcomponent
</div>
</form>
@endsection