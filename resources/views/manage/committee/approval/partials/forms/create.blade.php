<form method="POST" action="{{ route('manage.committee.approvals.store') }}" files="true" enctype="multipart/form-data">
	@csrf
	@component('components.card')
        @slot('card_body')
			<div class="row">
				<div class="col-6">
					@include('components.forms.hidden', [
						'id' => 'id',
						'name' => 'id',
						'value' => ''
					])

					@include('components.forms.hidden', [
						'id' => 'acquisition_id',
						'name' => 'acquisition_id',
						'value' => $approval->id
					])

					<label for="datePicker" class="control-label">Tarikh Perlantikan</label>
                    <input type="text" class="form-control input-lg datepicker" id="appointment_date" name="appointment_date">

					@include('components.forms.input', [
                        'input_label' => 'Jabatan',
                        'name' => '',
                        'id' => 'department_ids'
                    ])

                    @include('components.forms.hidden', [
                        'name' => 'department_id',
                        'id' => 'department_idss',
                        'value' => ''
                    ])

			        @include('components.forms.select', [
			            'input_label' => __('Kelulusan'),
			            'options' => $kelulusan->pluck('description','description'),
			            'name' => 'approval',
			            'id' => 'role',
			        ])
				</div>
				<div class="col-6" style="margin-top: -9px;">
					@include('components.forms.select', [
                        'input_label' => __('Nama Ahli'),
                        'options' => $user->where('department_id','!=',NULL)->pluck('name','name') ,
                        'name' => 'name',
                        'id' => 'name',
                    ])

                    <div style="margin-top: -9px;">
						@include('components.forms.input', [
	                        'input_label' => 'Jawatan',
	                        'name' => '',
	                        'id' => 'position_ids'
	                    ])
	                </div>

                    @include('components.forms.hidden', [
                        'name' => 'position_id',
                        'id' => 'position_idss',
                        'value' => ''
                    ])

			        @include('components.forms.select', [
			            'input_label' => __('Peranan'),
			            'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
			            'name' => 'role',
			            'id' => 'role',
			        ])
				</div>
			</div>
		@endslot
		@slot('card_footer')
			<div class="btn-group float-right">
		        <button type="submit" class="btn btn-primary btn-group float-right">
					@icon('fe fe-save') {{ __('Simpan') }}
				</button>
		    </div>
    	@endslot
	@endcomponent
</form>