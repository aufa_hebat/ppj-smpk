<button type="submit" class="btn btn-primary float-right submit-action-btn" 
	data-hashslug="{{ $hashslug }}"
	data-route="{{ $route_name }}"
	data-form="{{ $form }}">
    @icon('fe fe-save') {{ __('Simpan') }}
</button>
