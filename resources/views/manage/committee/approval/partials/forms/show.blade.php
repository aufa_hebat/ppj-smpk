@component('components.card')
    @slot('card_body')
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    @if(($committee->category) == 1)
                        <div class="form-group">
                            <div class="text-muted">Status Penilai</div>
                            <div>Penilai Dalam</div>
                        </div>
                    @elseif(($committee->category) == 2)
                        <div class="form-group">
                            <div class="text-muted">Status Penilai</div>
                            <div>Penilai Luar</div>
                        </div>
                    @endif
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <div class="text-muted">Tarikh Pelantikan</div>
                        <div>@if(!empty($committee->appointment_date)){!! date('d/m/Y', strtotime($committee->appointment_date)) !!}@endif</div>
                    </div>

                    <div class="form-group">
                        <div class="text-muted">Jabatan</div>
                        <div>{!! $committee->department->name !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="text-muted">Kelulusan</div>
                        <div>{!! $committee->approval !!}</div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <div class="text-muted">Nama Ahli</div>
                        <div>{!! $committee->name !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="text-muted">Jabatan</div>
                        <div>{!! $committee->position->name !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="text-muted">Peranan</div>
                        <div>{!! $committee->role !!}</div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
@endcomponent