<h4>Pembuka</h4>
<div class="row">
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('Nama'),
            'id' => 'name',
            'name' => 'name',
        ])
    </div>
    <div class="col-6">
        @include('components.forms.input', [
            'input_label' => __('E-Mel'),
            'id' => 'email',
            'name' => 'email',
        ])
    </div>
</div>
<div class="row">
    <div class="col-6">
        Kumpulan
        @foreach($user['roles'] as $users)
            @include('components.forms.input', [
                'input_label' => '',
                'id' => 'group_'.$users->id,
                'name' => 'group_'.$users->id,
            ])
        @endforeach
    </div>
    <div class="col-6">
        
    </div>
</div>