@extends('layouts.admin')

@section('content')
	@include('manage.committee.approval.partials.styles')
	@include('manage.committee.approval.partials.scripts', [
		'table_id' => 'user-management',
		'primary_key' => 'hashslug',
		'routes' => [
			'show' => 'api.manage.committee.show',
			'store' => 'api.manage.committee.store',
			'update' => 'api.manage.committee.update',
			'destroy' => 'api.manage.committee.destroy',
		],
		'columns' => [
			'name' => __('table.name'), 
			'email' => __('table.email'), 
			'created_at' => __('table.created_at'),
			'updated_at' => __('table.updated_at')
		],
		'forms' => [
			'create' => 'user-form',
			'edit' => 'user-form',
		], 
		'disabled' => [
			'email'
		]
	])
	<div class="row justify-content-center">
		<div class="col">
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'user-management',
							'route_name' => 'api.datatable.manage.approval',
							'columns' => [
								['data' => 'reference', 'title' => __('No Rujukan'), 'defaultContent' => '-'],
								['data' => 'title', 'title' => __('Tajuk'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false],
							],
							'headers' => [
								__('No Rujukan'), __('Tajuk'), __('table.action')
							],
							'actions' => minify(view('manage.committee.approval.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection