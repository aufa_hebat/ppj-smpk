@extends('layouts.admin')

@push('scripts')
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/datetimepicker.js') }}"></script>
    
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(function () {

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            })

        })
    </script>
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#approval').val('{{ $assessment->approval }}');

            @if($assessment->category == 1)
            $('#name1').val('{{ $assessment->name }}');
            $('#department_ids').val('{{ $assessment->department->name }}');
            $('#department_idss').val('{{ $assessment->department_id }}');
            $('#position_ids').val('{{ $assessment->position->name }}');
            $('#position_idss').val('{{ $assessment->position_id }}');
            $('#role1').val('{{ $assessment->role }}');
            @elseif($assessment->category == 2)
            $('#name').val('{{ $assessment->name }}');
            $('#department_id').val('{{ $assessment->department_id }}');
            $('#position_id').val('{{ $assessment->position_id }}');
            $('#role').val('{{ $assessment->role }}');
            @endif

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Syarikat') !!}', response.data.message, 'success');
                });
            });
        });
    </script>
    <script>
        function getKategori2(obj) {
            var dalam = document.getElementById('penilaidalam');
            var luar = document.getElementById('penilailuar');

            if(obj.value == '1'){
                dalam.hidden = false;
                luar.hidden = true;
            }
            else if(obj.value == '2'){
                dalam.hidden = true;
                luar.hidden = false;
            }
        }

        $('#name').change(function() {
             alert(this.value);
             if(this.value !== ""){
                 //alert('hi');
                //change to axios here
             var route_name = 'api.manage.finduserbyid'; 
                       axios.get(route(route_name, this.value)).then(response => {
                               try { 
                                  
                                  //alert(response.data.usersById.name);
                                
                                 //alert('hi');
                                  $('#department_ids').val(response.data.department_ids);
                                  $('#department_idss').val(response.data.department_idss);
                                  $('#position_ids').val(response.data.position_ids);
                                  $('#position_idss').val(response.data.position_idss);
                                  
                               }catch(err) {}             
                       });
                   }
            });
            
            
            $('#name1').change(function() {
             alert(this.value);
             if(this.value !== ""){
                 //alert('hi');
                //change to axios here
             var route_name = 'api.manage.finduserbyid'; 
                       axios.get(route(route_name, this.value)).then(response => {
                               try { 
                                  
                                  //alert(response.data.usersById.name);
                                
                                 //alert('hi');
                                  $('#department_ids').val(response.data.department_ids);
                                  $('#department_idss').val(response.data.department_idss);
                                  $('#position_ids').val(response.data.position_ids);
                                  $('#position_idss').val(response.data.position_idss);
                                  
                               }catch(err) {}             
                       });
                   }
            });
            
            
    </script>
@endpush

@section('content')
<form method="POST" action="{{ route('manage.committee.assessments.update',$assessment->id) }}" files="true" enctype="multipart/form-data">
    @csrf
    @method('PUT')
	<div class="row">
        @component('components.card', ['card_classes' => 'col-12'])
            @slot('card_body')
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="datePicker" class="control-label">Tarikh Perlantikan</label>
                            <input type="text" class="form-control input-lg datepicker" id="appointment_date" name="appointment_date" format='dd/mm/yyyy' value="@if(!empty($assessment->appointment_date)){!! date('d/m/Y', strtotime($assessment->appointment_date)) !!}@endif">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="row" >
                            <div class="col-sm-12">
                                <label class="control-label" style="font-size: 15px" >Status Penilai</label><br>
                                <div class="selectgroup w-25 grade-container">
                                    <label class="selectgroup-item">
                                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input"  onclick="getKategori2(this)" @if($assessment->category == old('category',1))
                                        checked="checked"
                                        @endif>
                                            <span class="selectgroup-button">{{ __('PPj') }}</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)" @if($assessment->category == old('category',2))
                                        checked="checked"
                                        @endif>
                                            <span class="selectgroup-button">{{ __('Agensi') }}</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6" style="margin-top: -8px;">
                        <!-- @include('components.forms.select', [
                            'input_label' => __('Kelulusan'),
                            'options' => $kelulusan->pluck('description','description'),
                            'name' => 'approval',
                            'id' => 'approval',
                        ]) -->
                        @include('components.forms.select', [
                            'input_label' => __('Kelulusan'),
                            'options' => ['Naib Presiden Kewangan' => 'Naib Presiden Kewangan', 'Pengarah BPUB' => 'Pengarah BPUB'],
                            'name' => 'approval',
                            'id' => 'approval',
                        ])                        
                    </div>
                </div>
                @include('components.forms.hidden', [
                    'id' => 'acquisition_id',
                    'name' => 'acquisition_id',
                    'value' => $assessment->acquisition_id
                ])
                @if($assessment->category == 1)
                <div id="penilaidalam">
                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.select', [
                                'input_label' => __('Nama Ahli'),
                                'options' => $user->pluck('name','id') ,
                                'value' => $assessment->name,
                                'name' => 'name1',
                                'id' => 'name1',
                            ])

                            @include('components.forms.hidden', [
                                'name' => '',
                                'id' => 'department_ids',
                                'value' => ''
                            ])

                            @include('components.forms.input', [
                                'input_label' => 'Jabatan',
                                'name' => 'department_id1',
                                'id' => 'department_idss',
                            ])
                        </div>
                        <div class="col-6">
                            @include('components.forms.hidden', [
                                'name' => '',
                                'id' => 'position_ids',
                                'value' => ''

                            ])

                            @include('components.forms.input', [
                                'input_label' => 'Jawatan',
                                'name' => 'position_id1',
                                'id' => 'position_idss'                            ])

                            @include('components.forms.select', [
                                'input_label' => __('Peranan'),
                                'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                                'name' => 'role1',
                                'id' => 'role1',
                            ])
                        </div>
                    </div>
                </div>
                @elseif($assessment->category == 2)
                <div id="penilaidalam" hidden>
                    
                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.select', [
                                'input_label' => __('Nama Ahli'),
                                'options' => $user->pluck('name','id') ,
                                'value' => $assessment->name,
                                'name' => 'name1',
                                'id' => 'name1',
                            ])

                            @include('components.forms.hidden', [
                                'name' => '',
                                'id' => 'department_ids',
                                'value' => ''
                            ])

                            @include('components.forms.input', [
                                'input_label' => 'Jabatan',
                                'name' => 'department_id1',
                                'id' => 'department_idss',
                            ])
                        </div>
                        <div class="col-6">
                            @include('components.forms.hidden', [
                                'name' => '',
                                'id' => 'position_ids',
                                'value' => ''

                            ])

                            @include('components.forms.input', [
                                'input_label' => 'Jawatan',
                                'name' => 'position_id1',
                                'id' => 'position_idss'                            ])

                            @include('components.forms.select', [
                                'input_label' => __('Peranan'),
                                'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                                'name' => 'role1',
                                'id' => 'role1',
                            ])
                        </div>
                    </div>
                </div>
                @endif
                @if($assessment->category == 1)
                <div id="penilailuar" hidden>
                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.input', [
                                'input_label' => 'Nama Ahli',
                                'options' => $user->pluck('name','id') ,
                                'value' => $assessment->name,
                                'id' => 'name',
                                'name' => 'name'
                            ])
                            @include('components.forms.select', [
                                'input_label' => __('Jabatan'),
                                'options' => $JabatanPelaksana->pluck('name','id'),
                                'name' => 'department_id',
                                'id' => 'department_id',
                            ])
                        </div>
                        <div class="col-6">

                            @include('components.forms.select', [
                                'input_label' => __('Jawatan'),
                                'options' => $listJawatan->pluck('name','id') ,
                                'name' => 'position_id',
                                'id' => 'position_id',
                            ])                            

                            @include('components.forms.select', [
                                'input_label' => __('Peranan'),
                                'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                                'name' => 'role',
                                'id' => 'role',
                            ])
                        </div>
                    </div>
                </div>
                @elseif($assessment->category == 2)
                    <div id="penilailuar">
                    <div class="row">
                        <div class="col-6">
                            @include('components.forms.input', [
                                'input_label' => 'Nama Ahli',
                                'options' => $user->pluck('name','id') ,
                                'value' => $assessment->name,
                                'id' => 'name',
                                'name' => 'name'
                            ])

                            @include('components.forms.select', [
                                'input_label' => __('Jawatan'),
                                'options' => $listJawatan->pluck('name','id') ,
                                'name' => 'position_id',
                                'id' => 'position_id',
                            ])
                        </div>
                        <div class="col-6">
                            @include('components.forms.select', [
                                'input_label' => __('Jabatan'),
                                'options' => $JabatanPelaksana->pluck('name','id'),
                                'name' => 'department_id',
                                'id' => 'department_id',
                            ])

                            @include('components.forms.select', [
                                'input_label' => __('Peranan'),
                                'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                                'name' => 'role',
                                'id' => 'role',
                            ])
                        </div>
                    </div>
                </div>
                @endif
                <div class="btn-group pull-right">
                    <a href="{{ route('manage.committee.assessments.show', ['hashslug'=>$assessment->acquisition->hashslug]) }}" 
                            class="btn btn-default border-primary">
                            {{ __('Kembali') }}
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
        @endslot
    @endcomponent
</div>
</form>
@endsection