@extends('layouts.admin')

@push('scripts')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/datetimepicker.js') }}"></script>
@endpush

@section('content')
<form class=""  method="POST" action="{{ route('manage.committee.assessments.store') }}" files="true" enctype="multipart/form-data">
	@csrf
	@component('components.card')
        @slot('card_body')
			<div class="row">
				<div class="col-6">
					@include('components.forms.hidden', [
						'id' => 'id',
						'name' => 'id',
						'value' => ''
					])

					@include('components.forms.datetimepicker', [
					    'input_label' => __('Tarikh pelantikan'),
					    'id' => 'appointment_date',
					    'name' => 'appointment_date',
					])

					@include('components.forms.select', [
			            'input_label' => __('Jabatan'),
			            'options' => $JabatanPelaksana->pluck('name','id'),
			            'name' => 'department_id',
			            'id' => 'department_id',
			        ])

			        @include('components.forms.select', [
			            'input_label' => __('Kelulusan'),
			            'options' => $JabatanPelaksana->pluck('description','description'),
			            'name' => 'approval',
			            'id' => 'approval',
			        ])
				</div>
				<div class="col-6">
					@include('components.forms.input', [
						'input_label' => 'Nama Ahli',
						'name' => 'name'
					])

					@include('components.forms.select', [
			            'input_label' => __('Jawatan'),
			            'options' => $listJawatan->pluck('name','id') ,
			            'name' => 'position_id',
			            'id' => 'position_id',
			        ])

			        @include('components.forms.select', [
			            'input_label' => __('Peranan'),
			            'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
			            'name' => 'role',
			            'id' => 'role',
			        ])
				</div>
			</div>
		@endslot
		@slot('card_footer')
			<div class="btn-group float-right">
		        <a href="{{ route('manage.committee.assessments.show',$id) }}" 
		                class="btn btn-default border-primary">
		                {{ __('Kembali') }}
		        </a>
		        <button type="submit" class="btn btn-primary btn-group float-right">
					@icon('fe fe-save') {{ __('Simpan') }}
				</button>
		    </div>
    	@endslot
	@endcomponent
</form>
@endsection