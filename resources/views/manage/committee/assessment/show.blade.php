@extends('layouts.admin')

@push('scripts')
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
@endpush

@push('scripts')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/datetimepicker.js') }}"></script>
    
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(function () {

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            })

        })
    </script>
    <script>
        function getKategori2(obj) {
            var dalam = document.getElementById('penilaidalam');
            var luar = document.getElementById('penilailuar');

            if(obj.value == '1'){
                dalam.hidden = false;
                luar.hidden = true;
            }
            else if(obj.value == '2'){
                dalam.hidden = true;
                luar.hidden = false;
            }
        }

        // din note : this looping got performance issue, please fix it ,
        // then dont find by name, because maybe name will have a same
        $('#name').change(function() {
             //alert(this.value);
             if(this.value !== ""){
                 //alert('hi');
                //change to axios here
             var route_name = 'api.manage.finduserbyid'; 
                       axios.get(route(route_name, this.value)).then(response => {
                               try { 
                                  
                                  //alert(response.data.usersById.name);
                                
                                 //alert('hi');
                                  $('#department_ids').val(response.data.department_ids);
                                  $('#department_idss').val(response.data.department_idss);
                                  $('#position_ids').val(response.data.position_ids);
                                  $('#position_idss').val(response.data.position_idss);
                                  
                               }catch(err) {}             
                       });
                   }
             /*
                  if(this.value == ''){
                       
                    }
                    else if(this.value == ""){
                        $('#department_ids').val('');
                        $('#position_ids').val('');
                    } */
             
             
           
            });
            
            
             function getUser(value) {
        
                var ddl = value;
                alert(ddl);
        }

            
    </script>
@endpush

@section('content')
@component('components.forms.hidden-form', 
    [
        'id' => 'destroy-record-form',
        'action' => route('manage.committee.assessments.destroy', 'dummy')
    ])
    @slot('inputs')
        @method('DELETE')
    @endslot
@endcomponent
@include('manage.committee.assessment.partials.scripts')
@include('manage.committee.assessment.partials.styles')
<div class="row">
    <div class="col-12">
        @component('components.card')
            @slot('card_body')
                <h4>Maklumat Perolehan</h4>
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="text-muted">No Sebut Harga</div>
                                <div>{!! $approval->reference !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="text-muted">Tajuk</div>
                                <div>{!! $approval->title !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endslot
        @endcomponent
        @include('manage.committee.assessment.partials.modals.form')
        @component('components.card')
            @slot('card_title')
            @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalTambahJKPenilai">
                    @icon('fe fe-plus') {{ __('Ahli Baru') }}
                </button>
            @endif 
            @endslot
            @slot('card_body')
                <h4>Maklumat Ahli Jawatankuasa Penilai</h4>
                <table class="table" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 10px">Bil</th>
                        <th >Nama Ahli</th>
                        <th>Jawatan</th>
                        <th>Jabatan</th>
                        <th>Peranan</th>
                        <th>Kategori</th>
                        <th>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($assessment as $bil => $committee)
                        <tr>
                            <td>{{$bil + 1}}</td>
                            @if(!empty($committee->get_user()) && $committee->category == '1')
                            <td>{{$committee->get_user()->name}}</td>
                            @else
                            <td>{{$committee->name}}</td>
                            @endif
                            <td>{{$committee->position_id}}</td>
                            <td>{!!$committee->department_id!!}</td>
                            <td>{!!$committee->role!!}</td>
                            <td>
                                @if($committee->category == 1)
                                    Penilai Dalam
                                @elseif($committee->category == 2)
                                    Penilai Luar
                                @endif
                            </td>
                            <td><div class="text-center">
                                    <div class="item-action dropdown">
                                      <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
                                        <i class="fe fe-more-vertical"></i>
                                      </a>
                                      <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
                                        style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item"  data-toggle="modal" data-target="#modalPaparJKPenilai{{$committee->id}}">
                                            <i class="fe fe-eye text-success"></i> {{ __('Butiran') }}
                                        </a>
                                          <!-- Din ,Hidden dulu sbb ada kena enhance 
                                        <a class="dropdown-item edit-action-btn" href="{{ route('manage.committee.assessments.edit', $committee->id) }}">
                                            <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
                                        </a> -->
                                         
                                        <div class="dropdown-divider"></div>
                                         @if(user()->current_role_login != 'administrator'  && user()->current_role_login != 'developer')
                                        <a class="dropdown-item destroy-action-btn{{$committee->id}}"
                                            data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
                                            <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
                                        </a>
                                        @endif
                                      </div>
                                    </div>
                                </div></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="btn-group pull-right">
                    <a href="{{ route('manage.committee.assessments.index') }}" 
                            class="btn btn-default border-primary float-right">
                            {{ __('Kembali') }}
                    </a>
                </div>
            @endslot
        @endcomponent
    </div>
</div>

<div class="modal fade" id="modalTambahJKPenilai" role="dialog" aria-labelledby="modalTambahJKPenilaiModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahJKPenilaiModalLongTitle">Tambah Ahli Jawatankuasa Penilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <form method="POST" action="{{ route('manage.committee.assessments.store') }}" files="true" enctype="multipart/form-data">
                @csrf
                @component('components.card')
                    @slot('card_body')
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <!-- <label for="datePicker" class="control-label">Tarikh Perlantikan</label> -->
                                    <!-- <input type="text" class="form-control input-lg datepicker" id="appointment_date" name="appointment_date"> -->
                                    @include('components.forms.datetimepicker', [
                                        'input_label'   => __('Tarikh Perlantikan'),
                                        'id'            => 'appointment_date',
                                        'name'          => 'appointment_date',
                                        'config'        => [
                                                            'format' => config('datetime.display.date'),
                                        ],
                                        'type'          => 'text',
                                        'maxlength'     => '10',
                                        'onkeypress'    => 'return (event.charCode === 0 )||  /[0-9/]/.test(String.fromCharCode(event.charCode))'
                                    ])                                    
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row" >
                                    <div class="col-sm-12">
                                        <label class="control-label" style="font-size: 15px" >Status Penilai</label><br>
                                        <div class="selectgroup w-25 grade-container">
                                          <label class="selectgroup-item">
                                            <input type="radio" id="category1" name="category" value="1" class="selectgroup-input"  onclick="getKategori2(this)">
                                            <span class="selectgroup-button">{{ __('PPj') }}</span>
                                          </label>
                                          <label class="selectgroup-item">
                                            <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                                            <span class="selectgroup-button">{{ __('Agensi') }}</span>
                                          </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6" style="margin-top: -8px;">  
                                <!-- @include('components.forms.select', [
                                    'input_label' => __('Kelulusan'),
                                    'options' => $kelulusan->pluck('description','description'),
                                    'name' => 'approval',
                                    'id' => 'role',
                                ]) -->
                                @include('components.forms.select', [
                                    'input_label' => __('Kelulusan'),
                                    'options' => ['Naib Presiden Kewangan' => 'Naib Presiden Kewangan', 'Pengarah BPUB' => 'Pengarah BPUB'],
                                    'name' => 'approval',
                                    'id' => 'role',
                                ])
                            </div>
                        </div>

                        @include('components.forms.hidden', [
                            'id' => 'id',
                            'name' => 'id',
                            'value' => ''
                        ])

                        @include('components.forms.hidden', [
                            'id' => 'acquisition_id',
                            'name' => 'acquisition_id',
                            'value' => $approval->id
                        ])

                        <div id="penilaidalam" hidden>
                            <div class="row">
                                <div class="col-6">
                                    @include('components.forms.select', [
                                        'input_label' => __('Nama Ahli'),
                                        'options' => $user->pluck('name','id') ,
                                        'name' => 'name1',
                                        'id' => 'name',
                                        'placeholder' => ''
                                    ])

                                    @include('components.forms.input', [
                                        'input_label' => 'Jabatan',
                                        'name' => 'department_id1',
                                        'id' => 'department_ids'
                                    ])
                                </div>
                                <div class="col-6">
                                    @include('components.forms.input', [
                                        'input_label' => 'Jawatan',
                                        'name' => 'position_id1',
                                        'id' => 'position_ids'
                                    ])

                                    @include('components.forms.select', [
                                        'input_label' => __('Peranan'),
                                        'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                                        'name' => 'role1',
                                        'id' => 'role',
                                    ])
                                </div>
                            </div>
                        </div>
                        <div id="penilailuar" hidden>
                            <div class="row">
                                <div class="col-6">
                                    @include('components.forms.input', [
                                        'input_label' => 'Nama Ahli',
                                        'name' => 'name'
                                    ])

                                    {{--@include('components.forms.select', [
                                        'input_label' => __('Jabatan'),
                                        'options' => $JabatanPelaksana->pluck('name','id'),
                                        'name' => 'department_id',
                                        'id' => 'role',
                                    ])--}}
                                    @include('components.forms.input', [
                                        'input_label' => 'Jabatan',
                                        'name' => 'department_id'
                                    ])
                                </div>
                                <div class="col-6">
                                {{--@include('components.forms.select', [
                                        'input_label' => __('Jawatan'),
                                        'options' => $listJawatan->pluck('name','id') ,
                                        'name' => 'position_id',
                                        'id' => 'role',
                                    ])--}}
                                    @include('components.forms.input', [
                                        'input_label' => 'Jawatan',
                                        'name' => 'position_id'
                                    ])                                

                                    @include('components.forms.select', [
                                        'input_label' => __('Peranan'),
                                        'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
                                        'name' => 'role',
                                        'id' => 'role',
                                    ])
                                </div>
                            </div>
                        </div>
                        @endslot
                        @slot('card_footer')
                            <div class="btn-group float-right">
                                <button type="submit" class="btn btn-primary btn-group float-right">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                                </button>
                            </div>
                        @endslot
                    @endcomponent
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@foreach($assessment as $bil => $committee)
<div class="modal fade" id="modalPaparJKPenilai{{$committee->id}}" role="dialog" aria-labelledby="modalTambahJKPenilaiModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahJKPenilaiModalLongTitle">Butiran Ahli Jawatankuasa Penilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                @include('manage.committee.assessment.partials.forms.show')
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection