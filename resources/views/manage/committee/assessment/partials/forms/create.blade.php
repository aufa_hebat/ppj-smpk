<form class="card"  method="POST" action="{{ route('manage.committee.assessments.store') }}" files="true" enctype="multipart/form-data">
	@csrf
	@component('components.card')
        @slot('card_body')
	        <div class="row">
	        	<div class="col-6">
	        		<div class="form-group">
                        <label for="datePicker" class="control-label">Tarikh Perlantikan</label>
                        <input type="text" class="form-control input-lg datepicker" id="appointment_date" name="appointment_date">
                    </div>
	        	</div>
				<div class="col-6">
			        <label class="control-label" style="font-size: 15px" >Penilai</label>
                    <div class="radio radio-primary justify-content-end">
                        <label><input type="radio" name="category" id="category1" value="1" onclick="getKategori1(this)">Dalam</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="category" id="category2" value="2" onclick="getKategori1(this)">Luar</label>&nbsp;&nbsp;&nbsp;
                    </div>
			    </div>
			</div>
			<div class="row">
				<div class="col-6" id="dalam" hidden>
					@include('components.forms.hidden', [
						'id' => 'id',
						'name' => 'id',
						'value' => ''
					])

					@include('components.forms.hidden', [
						'id' => 'acquisition_id',
						'name' => 'acquisition_id',
						'value' => $approval->id
					])

					@include('components.forms.select', [
			            'input_label' => __('Jabatan'),
			            'options' => $JabatanPelaksana->pluck('name','id'),
			            'name' => 'department_id',
			            'id' => 'department_id',
			        ])

			        @include('components.forms.select', [
			            'input_label' => __('Kelulusan'),
			            'options' => $kelulusan->pluck('description','description'),
			            'name' => 'approval',
			            'id' => 'approval',
			        ])
				</div>
				<div class="col-6" id="luar" hidden>
					@include('components.forms.input', [
						'input_label' => 'Nama Ahli',
						'name' => 'name'
					])

					@include('components.forms.select', [
			            'input_label' => __('Jawatan'),
			            'options' => $listJawatan->pluck('name','id') ,
			            'name' => 'position_id',
			            'id' => 'position_id',
			        ])
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					@include('components.forms.select', [
			            'input_label' => __('Peranan'),
			            'options' => ['Ahli' => 'Ahli', 'Pengerusi' => 'Pengerusi'],
			            'name' => 'role',
			            'id' => 'role',
			        ])
				</div>
				<div class="col-6">
					
				</div>
			</div>
		@endslot
		@slot('card_footer')
			<div class="btn-group float-right">
		        <button type="submit" class="btn btn-primary btn-group float-right">
					@icon('fe fe-save') {{ __('Simpan') }}
				</button>
		    </div>
    	@endslot
	@endcomponent
</form>