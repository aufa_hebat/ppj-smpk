<div class="btn-group float-right">
    
    {{ html()->a(route('manage.document_acceptances.index'), __('Kembali'))->class('btn btn-default border-primary') }}
    
    {{-- @if(user()->current_role_login != 'administrator' && user()->current_role_login != 'developer') --}}
        @isset($route_name)
        <button type="submit" class="btn btn-primary float-middle submit-action-btn" 
            data-route="{{ $route_name }}"
            data-form="{{ $form }}">
            @icon('fe fe-save') {{ __('Simpan') }}
        </button>
        {{-- @endisset --}}
        {{-- @isset($route_send) --}}
        {{-- <button type="submit" class="btn btn-success float-middle border-default confirm-action-btn" 
            data-route="{{ $route_name }}"
            data-form="{{ $form }}">
            @icon('fe fe-check') {{ __('Selesai') }}
        </button> --}}
        @endisset
    {{-- @endif --}}
    
</div>