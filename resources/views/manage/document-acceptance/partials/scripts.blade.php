@push('scripts')
    <script type="text/javascript">
    	jQuery(document).ready(function($) {
            $(document).on('click', '.show-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                redirect(route('manage.document_acceptances.shows', id));
            });
            $(document).on('click', '.edit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                redirect(route('manage.document_acceptances.edits', id));
            });
            $(document).on('click', '.list-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                redirect(route('manage.document_acceptances.list', id));
            });
            $(document).on('click', '.create-action-btn', function(event) {
                /* Handle Method Spoofing */
                $("[name='_method']").val('POST');
                /* Handle primary key */
                $("[name='id']").val(null);
                /* Enable disabled inputs defined */
                // $.each(disabled, function(index, val) {
                //      $("[name='" + val + "']").prop('readonly', false);
                // });
                $('#document-acceptance-modal').modal('show');
            });
    	});
    </script>
@endpush