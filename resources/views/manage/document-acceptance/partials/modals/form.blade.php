@component('components.modals.base', [
	'id' => 'document-acceptance-modal',
	'tooltip' => __('Tambah Penerimaan Dokumen Baru'),
	'modal_title' => __('Tambah Penerimaan Dokumen Baru'),
	])
	@slot('modal_body')
		@include('manage.document-acceptance.partials.modals.create')
	@endslot
	@slot('modal_footer')
        @include('manage.document-acceptance.partials.submit', [
            'route_name' => 'api.manage.document_acceptances.store',
            'form' => 'acceptance-document-form',
        ])
	@endslot
</form>
@endcomponent
