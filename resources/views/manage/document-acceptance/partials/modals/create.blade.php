@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript">

        $('#datetimepicker-document_acceptance_at').datetimepicker({ format:'DD/MM/YYYY hh:mm:ss A',inline:true, sideBySide: true, useCurrent : false  });

        $(document).on('click', '.submit-action-btn', function(event) {
            event.preventDefault();

            var route_name = $(this).data('route');
            var form_id = $(this).data('form');

            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', '');

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ '= ' + pair[1]);
            }

            axios.post(route(route_name), data ).then( (response) => {
                swal('{!! __('Penerimaan Dokumen') !!}', response.data.message, 'success');
                // $('#cpo_print').show();
                location.reload();
            }).catch((error)=>{
                    console.log(error.response);
            });
        });

        /* DOCUMENT UPLOAD DA*/
        var tDA = $('#tblUploadDA').DataTable({
            searching: false,
            ordering: false,
            paging: false,
            info:false
        });

        var counterDA = 1;

        $('#addRowDA').on( 'click', function () {
            tDA.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input id="subFileDA'+ counterDA +'" type="text"  class="form-control" readonly>\n' +
                '       <label class="input-group-text" for="uploadFileDA'+ counterDA +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFileDA" id="uploadFileDA'+ counterDA +'" name="uploadFileDA[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterDA +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="removeDA'+ counterDA +'" class="removeDA btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counterDA++;
        } );

        // Automatically add a first row of data
        $('#addRowDA').click();

        $("#tblUploadDA").on('click','.removeDA',function(){
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        tDA.row($(this).closest("tr")).remove().draw(false);
                    }
                });

            });

            $("#tblUploadDA").on('change','.uploadFileDA',function(){
                var noDA = $(this).data('counter');
                $('#subFileDA' + noDA).val($(this).val().split('\\').pop());
            });
            /* END DOCUMENT UPLOAD DA */
    </script>
@endpush
<form id="acceptance-document-form" files = "true" enctype="multipart/form-data" method="POST">
	@csrf
	<div class="row">
		<div class="col-12">
			
            @include('components.forms.datetimepicker', [
                'input_label' => __('Tarikh Penerimaan Dokumen'),
                'id' => 'document_acceptance_at',
                'name' => 'document_acceptance_at',
                'input_classes' => 'briefing',
                'config' => [
                    'format' => config('datetime.display.date_time'),
                    'sideBySide' => true,
                    'inline'    => true,
                ]
            ])

            <input type="hidden" name="sst_id" value="{{$sst->id}}">

            @include('components.forms.textarea', [
                'input_label' => __('Catatan'),
                'id' => 'remarks',
                'name' => 'remarks'
            ])

            <div id="uploadsDA">
                <hr>
                <h4>Muat Naik Dokumen</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="float-right">
                            <button type="button" id="addRowDA" href="#" class="btn btn-primary">
                                @icon('fe fe-plus')
                                {{ __('Muat Naik Dokumen') }}
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table id="tblUploadDA" class="table table-sm table-transparent">
                                <thead>
                                <tr>
                                    <th>Muat Naik Dokumen</th>
                                    <th width="70px">Hapus</th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

		</div>
	</div>