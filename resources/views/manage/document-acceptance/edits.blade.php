@extends('layouts.admin')
@push('scripts')
    @include('components.forms.assets.datetimepicker')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript">

        $('#datetimepicker-document_acceptance_at').datetimepicker({ format:'DD/MM/YYYY hh:mm:ss A',inline:true, sideBySide: true, useCurrent : false  });

        $(document).on('click', '.submit-action-btn', function(event) {
            event.preventDefault();

            var id = '{{ $document->hashslug }}';
            var route_name = $(this).data('route');
            var form_id = $(this).data('form');

            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', '');

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ '= ' + pair[1]);
            }

            axios.post(route(route_name, id), data ).then( (response) => {
                swal('{!! __('Penerimaan Dokumen') !!}', response.data.message, 'success');
                // $('#cpo_print').show();
                location.reload();
            }).catch((error)=>{
                    console.log(error.response);
            });
        });

        $(document).on('click', '.confirm-action-btn', function(event) {
            event.preventDefault();

            var id = '{{ $document->hashslug }}';
            var route_name = $(this).data('route');
            var form_id = $(this).data('form');

            var form = document.forms[form_id];
            var data = new FormData(form);
            data.append('action', 'submit');

            // Display the key/value pairs
            for (var pair of data.entries()) {
                console.log(pair[0]+ '= ' + pair[1]);
            }

            axios.post(route(route_name, id), data ).then( (response) => {
                swal('{!! __('Penerimaan Dokumen') !!}', response.data.message, 'success');
                // $('#cpo_print').show();
                location.reload();
                redirect(route('manage.document_acceptances.index'));
            }).catch((error)=>{
                    console.log(error.response);
            });
        });

        @if(!empty($document->document_acceptance_at))
            $('#document_acceptance_at').val('{{ Carbon::createFromFormat('Y-m-d G:i:s', $document->document_acceptance_at)->format("d/m/Y G:i A") }}');
        @endif
        @if(!empty($document->remarks))
            $('#remarks').val('{{ $document->remarks }}');
        @endif

        /* DOCUMENT UPLOAD DOCUMENT ACCEPTANCE*/
        var tda = $('#tblUploadDA').DataTable({
            searching: false,
            ordering: false,
            paging: false,
            info:false
        });

        var counterDA = 1;

        $('#addRowDA').on( 'click', function () {
            tda.row.add( [
                '<div class="form-group">\n' +
                '   <div class="col input-group">\n' +
                '       <input id="subFileDA'+ counterDA +'" type="text"  class="form-control" readonly>\n' +
                '       <label class="input-group-text" for="uploadFileDA'+ counterDA +'"><i class="fe fe-upload" ></i></label>\n' +
                '       <input type="file" class="form-control uploadFileDA" id="uploadFileDA'+ counterDA +'" name="uploadFileDA[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterDA +'">\n' +
                '   </div>\n' +
                '</div>',
                '<div class="form-group"><button type="button" id="remove'+ counterDA +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
            ] ).draw( false );

            counterDA++;
        } );

        @if(!empty($document) && ($document->doc_acceptance->count() > 0))
            @foreach($document->doc_acceptance as $doc)
                tda.row.add( [
                    '<div class="form-group">\n' +
                    '   <div class="col input-group">\n' +
                    '       <input type="text" id="subFileDA'+ counterDA +'" class="form-control" value="{{ $doc->document_name }}" readonly>\n' +
                    '@if(!isset($eot->eot_approve->id))' +
                    '       <input type="hidden" id="hDocumentIdDA'+ counterDA +'" name="hDocumentIdDA[]" value="{{ $doc->id }}">\n' +
                    '       <label class="input-group-text" for="uploadFileDA'+ counterDA +'"><i class="fe fe-upload" ></i></label>\n' +
                    '       <input type="file" class="form-control uploadFileDA" id="uploadFileDA'+ counterDA +'" name="uploadFileDA[]" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg" data-counter="'+ counterDA +'">\n' +
                    '@endif' +
                    '       <label class="input-group-text" for="uploadFileDA'+ counterDA +'"><a href="/download/{{$doc->document_path}}/{{ $doc->document_name }}" target="_blank"><i class="fas fa-download"></i></a></label>\n' +
                    '   </div>\n' +
                    '</div>',
                    '<div class="form-group"><button type="button" id="remove'+ counterDA +'" class="remove btn btn-danger" href="#">@icon("fe fe-trash") {{__("Hapus")}}</button></div>'
                ] ).draw( false );

                counterDA++;
            @endforeach
        @else
        // Automatically add a first row of data
        $('#addRowDA').click();
        @endif

        $("#tblUploadDA").on('click','.remove',function(){
            swal({
                title: '{!! __('Amaran') !!}',
                text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '{!! __('Ya') !!}',
                cancelButtonText: '{!! __('Batal') !!}'
            }).then((result) => {
                if (result.value) {
                tda.row($(this).closest("tr")).remove().draw(false);
                }
            });
        });

        $("#tblUploadDA").on('change','.uploadFileDA',function(){
            var no = $(this).data('counter');
            $('#subFileDA' + no).val($(this).val().split('\\').pop());
        });
        /* END DOCUMENT UPLOAD DOCUMENT ACCEPTANCE */
    </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{!! $sst->acquisition->title !!}
        <br>
        <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
        {{-- <br> --}}
        {{-- <span class="font-weight-bold"> No Sijil Interim: </span> @if($ipc->count() > 0) @foreach($ipc as $ip) {{$ip->ipc_no}}, @endforeach @endif --}}
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')

                    <form id="acceptance-document-form" files = "true" enctype="multipart/form-data" method="POST">

                        @csrf
                        @method('put')
                        @include('components.forms.datetimepicker', [
                            'input_label' => __('Tarikh Penerimaan Dokumen'),
                            'id' => 'document_acceptance_at',
                            'name' => 'document_acceptance_at',
                            'input_classes' => 'briefing',
                            'config' => [
                                'format' => config('datetime.display.date_time'),
                                'sideBySide' => true,
                                'inline'    => true,
                            ]
                        ])

                        @include('components.forms.textarea', [
                            'input_label' => __('Catatan'),
                            'id' => 'remarks',
                            'name' => 'remarks'
                        ])

                        <input type="hidden" name="sst_id" value="{{$sst->id}}">

                        <div id="uploadsDA">
                            <hr>
                            <h4>Muat Naik Dokumen</h4>
                            <div class="row">
                                <div class="col-12">
                                    <div class="float-right">
                                        <button type="button" id="addRowDA" href="#" class="btn btn-primary">
                                            @icon('fe fe-plus')
                                            {{ __('Muat Naik Dokumen') }}
                                        </button>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="tblUploadDA" class="table table-sm table-transparent">
                                            <thead>
                                            <tr>
                                                <th>Muat Naik Dokumen</th>
                                                <th width="70px">Hapus</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </form>
                    
                    @include('manage.document-acceptance.partials.button', [
                        'hashslug'=> $sst->hashslug,
                        'route_name' => 'api.manage.document_acceptances.update',
                        'form' => 'acceptance-document-form',
                    ])

                @endslot
            @endcomponent
        </div>
    </div>
@endsection