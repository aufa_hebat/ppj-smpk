@extends('layouts.admin')
@section('content')
    @include('manage.document-acceptance.partials.scripts')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{!! $sst->acquisition->title !!}
        <br>
        <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
        {{-- <br> --}}
        {{-- <span class="font-weight-bold"> No Sijil Interim: </span> @if($ipc->count() > 0) @foreach($ipc as $ip) {{$ip->ipc_no}}, @endforeach @endif --}}
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-12">

            @include('manage.document-acceptance.partials.modals.form')

            @component('components.card')
                @slot('card_title')
                    @include('components.modals.button', [
                        'modal_btn_classes' => 'create-action-btn float-right',
                        'label' => __('Penerimaan Dokumen Baru'),
                        'icon' => 'fe fe-plus'
                    ])
                @endslot
                @slot('card_body')
                    <div class="row">
                        <div class="col">
                            @component('components.datatable', 
                            [
                                'table_id' => 'document-acceptance-table',
                                'route_name' => 'api.datatable.manage.document_acceptances_list',
                                'param' => [
                                    'hashslug' => $sst->hashslug,
                                ],
                                'columns' => [
                                    ['data' => 'document_acceptance_at', 'title' => __('Tarikh Penerimaan Dokumen'), 'defaultContent' => '-'],
                                    ['data' => 'remarks', 'title' => __('Catatan'), 'defaultContent' => '-'],
                                    ['data' => 'user', 'title' => __('Diterima Oleh'), 'defaultContent' => '-'],
                                    ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
                                ],
                                'headers' => [
                                   __('Tarikh Penerimaan Dokumen'),__('Catatan'),__('Diterima Oleh'), __('table.action')
                                ],
                                'actions' => minify(view('manage.document-acceptance.partials.action')->render()),
                            ]
                        )
                        @endcomponent
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="pull-right btn-group">
                                <a href="{{ route('manage.document_acceptances.index') }}"
                                   class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>
                            </div>
                        </div>
                    </div>
                @endslot
            @endcomponent

        </div>
    </div>
@endsection