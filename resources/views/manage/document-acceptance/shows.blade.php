@extends('layouts.admin')
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
    <script type="text/javascript">
        @if(!empty($document->document_acceptance_at))
            $('#document_acceptance_at').val('{{ Carbon::createFromFormat('Y-m-d G:i:s', $document->document_acceptance_at)->format("d/m/Y G:i A") }}');
        @endif
        @if(!empty($document->remarks))
            $('#remarks').val('{{ $document->remarks }}');
        @endif

        @if($document->doc_acceptance->count() > 0)
            var t_document_acceptance = $('#tblUpload_document_acceptance').DataTable({
            searching: false,
            ordering: false,
            paging: false,
            info:false
            });

            var counter_document_acceptance = 1;
            @foreach($document->doc_acceptance as $doc)
                t_document_acceptance.row.add( [
                    counter_document_acceptance,
                    '<a href="/download/{{ $doc->document_path .'/'.$doc->document_name }}" target="_blank"> {{ $doc->document_name }}</a>'
                ] ).draw( false );

                counter_document_acceptance++;
            @endforeach
        @endif
    </script>
@endpush
@section('content')
    @component('components.pages.title-sub')
        @slot('title_sub_content')
        <span class="font-weight-bold">Tajuk : </span>{!! $sst->acquisition->title !!}
        <br>
        <span class="font-weight-bold">No. Kontrak : </span>{{ $sst->contract_no }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nama Kontraktor : </span>{{ $sst->company->company_name }}
        <br>
        <span class="font-weight-bold">Tarikh Mula Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->start_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tempoh : </span>{{ $sst->period." ".$sst->period_type->name }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Tarikh Siap Kerja : </span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sst->end_working_date)->format('d/m/Y') }}&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="font-weight-bold">Nilai Kontrak : </span>RM {{ money()->toCommon($appointed->offered_price ?? "0" , 2) }}&nbsp;&nbsp;&nbsp;&nbsp;
        {{-- <br> --}}
        {{-- <span class="font-weight-bold"> No Sijil Interim: </span> @if($ipc->count() > 0) @foreach($ipc as $ip) {{$ip->ipc_no}}, @endforeach @endif --}}
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-12">
            @component('components.card')
                @slot('card_body')
                    @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh Penerimaan Dokumen'),
                        'id' => 'document_acceptance_at',
                        'name' => 'document_acceptance_at',
                        'readonly' => true,
                    ])

                    @include('components.forms.textarea', [
                        'input_label' => __('Catatan'),
                        'id' => 'remarks',
                        'name' => 'remarks',
                        'readonly' => true,
                    ])

                    <div id="uploads_document_acceptance">
                        <hr>
                        <h4>Muat Naik Dokumen</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table id="tblUpload_document_acceptance" class="table table-sm table-transparent">
                                        <thead>
                                        <tr>
                                            <th width="10px">No</th>
                                            <th >Nama Dokumen</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>

                @endslot
            @endcomponent
        </div>
    </div>
@endsection