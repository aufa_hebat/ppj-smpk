@extends('layouts.admin')
@push('scripts')
    <script>
    	jQuery(document).ready(function($) {
    	});
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#owner" role="tab" aria-controls="owner" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Pemilik Syarikat') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'owner', 'active' => true])
                            @slot('content')
                                @include('manage.owner.partials.shows.owner', ['owner' => $owner])
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('manage.companies.edit',['company_id' => $owner->company->hashslug]) }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection