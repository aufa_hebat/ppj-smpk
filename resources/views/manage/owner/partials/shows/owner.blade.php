<h4>Maklumat Pemilik Syarikat</h4>
<br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Nama Pemilik</div>
        <p> {!! $owner->name !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">No. Kad Pengenalan</div>
        <p>{!! $owner->ic_number !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">E-Mel</div>
        <p>{!! $owner->email !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">No. Telefon</div>
        <p>@if(!empty($owner->phones[0])){!! $owner->phones[0]->phone_number !!}@endif</p>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($owner->addresses[0]->primary))
            <p>
                @if(!empty($owner->addresses[0])){!! $owner->addresses[0]->primary !!}<br>{!! $owner->addresses[0]->secondary !!}<br>{!! $owner->addresses[0]->postcode !!}, {!! $owner->addresses[0]->state !!}, Malaysia @endif
            </p>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Pemilik Sijil Pendaftaran bagi:</div>
        @if($owner->cidb_cert == 1)
            <div>CIDB</div>
        @endif
        @if($owner->mof_cert == 1)
            <div>MOF</div>
        @endif
        @if($owner->kdn_cert == 1)
            <div>KDN</div>
        @endif
        @if(($owner->cidb_cert == NULL) && ($owner->mof_cert == NULL) && ($owner->kdn_cert == NULL))
            <div>Tiada</div>
        @endif
    </div>
</div>