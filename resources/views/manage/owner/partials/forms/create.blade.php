@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#company_id').val('{{ $owner->id }}');
        });
    </script>
@endpush
<form method="POST" action="{{ route('manage.owners.store') }}">
	@csrf
<div class="row">
	<div class="col-6">
		<input type="text" name="hashslug" id="hashslug" value="{{$hashslug}}" hidden/>
		@include('components.forms.hidden', [
			'id' => 'id',
			'name' => 'id',
			'value' => '',
		])

		@include('components.forms.hidden', [
			'id' => 'company_id',
			'name' => 'company_id',
			'value' => ''
		])

		@include('components.forms.input', [
			'input_label' => 'Nama Pemilik',
			'name' => 'name',
			'required' => 'true'
		])
	</div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Kad Pengenalan',
			'name' => 'ic_number',
            'input_classes' => 'allownumericonly',
			'required' => 'true'
		])
	</div>
	
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'E-Mel',
			'name' => 'email'
		])
	</div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'No. Telefon',
			'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
	      	'input_classes' => 'allownumericonly',
			'required' => 'true'
		])
	</div>
</div>

@include('components.forms.input', [
    'input_label' => 'Alamat',
  	'name' => 'primary',
  	'id' => 'primary',
    'placeholder' => 'Alamat (Baris Pertama)',
	'required' => 'true'
])
<div style="margin-top: -10px;">
    @include('components.forms.input', [
        'input_label' => '',
    	'name' => 'secondary',
    	'id' => 'secondary',
        'placeholder' => 'Alamat (Baris Kedua)'
    ])
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'postcode',
	      	'input_classes' => 'allownumericonly', 
	      	'maxlength' => 5,
			'required' => 'true'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
			'input_label' => 'Negeri',
			'name' => 'state',
			'options' => $std_cd_state->pluck('description','description'),
			'required' => 'true'
		])
	</div>
</div>

<div class="form-group row" style="margin-top: 20px;margin-left: 0px;">
  	<label class="control-label" for="inputLarge" style="font-size: 15px;">Pemilik Sijil Pendaftaran bagi:</label>
	<div class="col-lg-10">
		<div class="checkbox form-control">
			<label class="custom-control custom-checkbox">
			<input type="checkbox" value="1" name="cidb_cert" class="custom-control-input" > <span class="custom-control-label">CIDB</span>
			</label>
			<label class="custom-control custom-checkbox">
			<input type="checkbox" value="1" name="mof_cert" class="custom-control-input" > <span class="custom-control-label">MOF</span>
			</label>
			<label class="custom-control custom-checkbox">
			<input type="checkbox" value="1" name="kdn_cert" class="custom-control-input" > <span class="custom-control-label">KDN</span>
			</label>
		</div>
	</div>
</div>