@component('components.modals.base', [
    'id' => 'view-company-modal',
    'modal_title' => 'Company Details',
    ])
    @slot('modal_body')
        @include('components.table', ['table_id' => 'company-details'])
    @endslot
    @slot('modal_footer')
        <a href="#" id="edit-user-link"
            @include('components.tooltip', ['tooltip' => 'Edit'])
            class="float-right btn btn-primary">
            <i class="fe fe-edit text-primary"></i> Edit
        </a>
    @endslot
@endcomponent