@component('components.modals.base', [
	'id' => 'owner-modal',
	'tooltip' => __('Pemilik Syarikat'),
	'modal_title' => __('Pemilik Syarikat'),
	])
	@slot('modal_body')
		@include('manage.owner.partials.forms.create')
	@endslot
	@slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right">
			@icon('fe fe-save') {{ __('Simpan') }}
		</button>
	@endslot
</form>
@endcomponent
