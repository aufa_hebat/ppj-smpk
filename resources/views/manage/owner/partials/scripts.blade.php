@push('scripts')
	<script src="{{ asset('js/select2.js') }}"></script>
	<script type="text/javascript">
		var table_id = '#{{ $table_id }}';
		var primary_key = '{{ $primary_key or 'hashslug' }}';
		var routes = @json($routes);
		var columns = @json($columns);
		var forms = @json($forms);
		var disabled = @json($disabled);
		jQuery(document).ready(function($) {
			/* Initialisation */
			$('.select2').select2();

			/* Actions */
			$(document).on('click', '.create-action-btn', function(event) {
				/* Handle Method Spoofing */
				$("[name='_method']").val('POST');
				/* Handle primary key */
				$("[name='id']").val(null);
				/* Enable disabled inputs defined */
				$.each(disabled, function(index, val) {
					 $("[name='" + val + "']").prop('readonly', false);
				});
				$('#owner-modal').modal('show');
			});

			$(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('hashslug');
				redirect(route('manage.owners.edit', id));
			});

			$(document).on('click', '.show-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('hashslug');
				redirect(route('manage.owners.show', id));
			});

			$(document).on('click', '.destroy-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('hashslug');
				swal({
				  title: '{!! __('Amaran') !!}',
				  text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonText: '{!! __('Ya') !!}',
				  cancelButtonText: '{!! __('Batal') !!}'
				}).then((result) => {
				  if (result.value) {
                    axios.delete(route('manage.owners.destroy', id))
                    .then(response => {
                        swal('{!! __('Pemilik Syarikat') !!}', response.data.message, 'success');
                        location.reload();
                    })
				  }
				});
			});
		});
	</script>
@endpush