@include('manage.owner.partials.scripts', [
	'table_id' => 'owner-management',
	'primary_key' => 'hashslug',
	'param' => 'hashslug=' . $hashslug,
	'routes' => [
		'show' => 'api.manage.owners.show',
		'store' => 'api.manage.owners.store',
		'update' => 'api.manage.owners.update',
		'destroy' => 'api.manage.owners.destroy',
	],
	'columns' => [
		'name' => __('table.name'), 
		'ic_number' => __('table.ic_number'), 
		'email' => __('table.email'), 
		'created_at' => __('table.created_at'),
		'updated_at' => __('table.updated_at')
	],
	'forms' => [
		'create' => 'owner-form',
		'edit' => 'owner-form',
	], 
	'disabled' => [
		'email'
	]
])
@component('components.forms.hidden-form', 
	[
		'id' => 'destroy-record-form',
		'action' => route('manage.owners.destroy', 'dummy')
	])
	@slot('inputs')
		@method('DELETE')
	@endslot
@endcomponent

@include('manage.owner.partials.modals.form')
@include('manage.owner.partials.modals.show')
	
@component('components.card')
	@slot('card_title')
		@include('components.modals.button', [
			'modal_btn_classes' => 'create-action-btn float-right',
			'label' => __('Pemilik Syarikat Baru'),
			'icon' => 'fe fe-plus'
		])
	@endslot
	@slot('card_body')
		<div class="row">
			<div class="col">
				@component('components.datatable', 
					[
						'table_id' => 'owner-management',
						'param' => 'hashslug=' . $hashslug,
						'route_name' => 'api.datatable.manage.owner',
						'columns' => [
							['data' => 'name', 'title' => __('Nama Pemilik Syarikat'), 'defaultContent' => '-'], 
							['data' => 'ic_number', 'title' => __('No Kad Pengenalan'), 'defaultContent' => '-'],
							['data' => 'email', 'title' => __('E-Mel'), 'defaultContent' => '-'],
							['data' => 'type', 'title' => __('Sijil'), 'defaultContent' => '-'],
							['data' => null , 'name' => null, 'searchable' => false],
						],
						'headers' => [
							__('Nama Pemilik Syarikat'), __('No Kad Pengenalan'), __('E-mel'), __('Sijil'), __('table.action')
						],
						'actions' => minify(view('manage.owner.partials.actions')->render())
					]
				)
				@endcomponent 
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="pull-right btn-group">
		            <a href="{{ route('manage.companies.index') }}"
		               class="btn btn-default border-primary">
		                {{ __('Kembali') }}
		            </a>
		        </div>
			</div>
		</div>
	@endslot
@endcomponent