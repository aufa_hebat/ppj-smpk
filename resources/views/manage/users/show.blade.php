@extends('layouts.admin')
@push('scripts')
    <script>
    	jQuery(document).ready(function($) {
    	});
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#user-details" role="tab" aria-controls="user-details" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Pengguna') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'user-details', 'active' => true])
                            @slot('content')
                                @include('manage.users.partials.shows.user-details', ['user' => $user])
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('manage.users.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection