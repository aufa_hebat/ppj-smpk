@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('js/select2.js') }}"></script>
<script type="text/javascript">
	$(document).ready(
	    function () {
	        $("#roles").select2();
	    }
	);
</script>
<script type="text/javascript">
    function getBahagian(value) {

        var ddl = value;
        var selectedOption = ddl.options[ddl.selectedIndex];

        var mailValue = selectedOption.getAttribute("data-mail");
        var textBoxBahagian = document.getElementById("Bahagian");
        var textBoxSeksyen = document.getElementById("Seksyen");

        /* remove all select options */
        textBoxBahagian.options.length = 0;
        textBoxSeksyen.options.length = 0;
        /* add Sila Pilih options */
        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "";
        textBoxBahagian.appendChild(opt);
       // alert(mailValue);

        /* add other options */
        @foreach($Seksyen as $bhgn)
        if (mailValue == "{{ $bhgn->category}}") {

            var opt = document.createElement('option');
            opt.value = "{{ $bhgn->id }}";
            opt.innerHTML = "{{$bhgn->name}}";
            opt.setAttribute('data-mail', '{{ $bhgn->code }}');
            textBoxBahagian.add(opt);
        }
        @endforeach

    }

</script>
<script type="text/javascript">
    function getSeksyen(value) {
        var dd2 = value;
        var selectedOption = dd2.options[dd2.selectedIndex];
        var mailValue1 = selectedOption.getAttribute("data-mail");
        var textBoxSeksyen = document.getElementById("Seksyen");

        /* remove all select options */
        textBoxSeksyen.options.length = 0;

        /* add options */
        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "";
        textBoxSeksyen.appendChild(opt);


        /* add other options */
        @foreach($Seksyen as $sksyn)
        if (mailValue1 == "{{ $sksyn->category}}") {
            var opt = document.createElement('option');
            opt.value = "{{ $sksyn->id }}";
            opt.innerHTML = "{{$sksyn->name}}";
            opt.setAttribute('data-mail', '{{ $sksyn->code }}');
            textBoxSeksyen.appendChild(opt);
        }
        @endforeach

    }
</script>
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
			
        	@if(!empty($user->phones[0]))
            $('#phone_number').val('{{ $user->phones[0]->phone_number }}');
            @endif
            @if(!empty($user->roles()->id))
            $('#roles').val('{{ $user->roles()->id }}');
            @endif
            @if(!empty($user->phone_no))
            $('#phone_no').val('{{ $user->phone_no }}');
            @endif

            $(document).on('click', '.submit-action-btn', function(event) {

                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();

				console.log(data);

                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Pengguna') !!}', response.data.message, 'success');
                });
            });


            $(".allownumericonly").on("keypress keyup blur",function (event) {    
               $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        });
    </script>
@endpush

@section('content')
@can('urus-pengguna_update')
	<div class="my-3 my-md-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="card col-xs-12 col-12">
					<div class="card-body">
						<form method="POST" action="{{ route('manage.users.update',$user->hashslug) }}">
						@csrf
    					@method('PUT')
						<div class="row">
							<div class="col-6">
								<div class="form-group">
                                                                {{ html()->label(__('Nama')) }}
			        				{{ html()->input('text')
			        					->name('name')
			        					->class('form-control')
			        					->value($user->name) }}
                                                                </div>

                                    @include('components.forms.input', [
                                        'input_label' => 'No. Telefon',
                                        'id' => 'phone_no',
                                        'name' => 'phone_no',
                                        'input_classes' => 'allownumericonly', 
                                        'maxlength' => 15,
                                        'type' => 'text'
                                    ])

		                        <!--Jabatan Pelaksana -->
			                    <div class="form-group {{ $errors->has('Jabatan') ? ' has-error' : '' }}">
		                            <label for="Jabatan" class="control-label">Jabatan</label>
	                                <select class="form-control input-lg" onchange="getBahagian(this);" name="department_id" id="JabatanPelaksana" style="width: 100%;">
	                                    <option value=""></option>
	                                    @foreach($JabatanPelaksana as $jp)
	                                        <option value="{{$jp->id}}" data-mail="{{$jp->code}}"
                                            @if ($jp->id == old('department_id', $user->department_id))
                                            selected="selected"
                                            @endif
	                                        >{{$jp->name}}</option>
	                                    @endforeach
	                                </select>
			                    </div>
                                        
			                    <!--Seksyen -->
			                    <div class="form-group {{ $errors->has('Seksyen') ? ' has-error' : '' }}">
			                        <label for="Seksyen" class="control-label">Pilih Seksyen</label>
			                        <select class="form-control input-lg" name="section_id" id="Seksyen" style="width: 100%;">
			                            <option value=""></option>
			                            @foreach($Seksyen->where('id','=',$user->section_id) as $skyn)
			                                <option value="{{$skyn->id}}" data-mail="{{$skyn->code}}"
		                                        @if ($skyn->id == old('section_id', $user->section_id))
		                                        selected="selected"
		                                        @endif
			                                >{{$skyn->name}}</option>
			                            @endforeach
			                        </select>
			                    </div>
							</div>
							<div class="col-6">
                                                                <div class="form-group">
			        				{{ html()->label(__('Gelaran')) }}
			        				{{ html()->input('text')
			        					->name('honourary')
			        					->class('form-control')
			        					->value($user->honourary) }}
                                                                </div>
								<div class="form-group">
			        				{{ html()->label(__('Emel')) }}
			        				{{ html()->input('text')
			        					->name('email')
			        					->class('form-control')
			        					->value($user->email) }}
                                                                </div>
                                                            <!--Bahagian -->
                                                        <div class="form-group {{ $errors->has('Bahagian') ? ' has-error' : '' }} ">
                                                            <label for="Bahagian" class="control-label">Pilih Bahagian</label>
                                                            <select class="form-control input-lg" name="executor_department_id" id="Bahagian" style="width: 100%;" onchange="getSeksyen(this)">
                                                                <option value=""></option>										
                                                                @foreach($Seksyen->where('id','=',$user->executor_department_id) as $bhgn)
                                                                    <option value="{{$bhgn->id}}" data-mail="{{$bhgn->code}}"
                                                                    @if ($bhgn->id == old('executor_department_id', $user->executor_department_id))
                                                                    selected="selected"
                                                                    @endif
                                                                    >{{$bhgn->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
							    <div class="selectRow">
							    	<label for="roles" 
										class="col-form-label">
										Kumpulan
									</label>
							        <!-- Using data-placeholder below to set place holder value versus putting in configuration -->
							        <select id="roles" data-placeholder="Select an option" name="roles[]" style="width: 100%" multiple>
							        	@foreach(roles()->where('guard_name', 'web') as $roles)
							            <option value="{{$roles->id}}"
							            	@foreach($user->roles as $users)
							            	@if ($roles->id == old('roles[]', $users->id))
		                                        selected="selected"
		                                        @endif
		                                        @endforeach
							            	>{{$roles->name}}</option>
							            @endforeach
							        </select>
							    </div><br>
                                        
		                        
							</div>
						</div>

						<div class="row">
							<div class="col-8">
								<!--Jawatan -->
			                    <div class="form-group {{ $errors->has('Jawatan') ? ' has-error' : '' }}">
			                        <label for="Jawatan" class="control-label">Jawatan</label>
			                        <select class="form-control input-lg" name="position_id" id="Jawatan" style="width: 100%;">
			                            <option value=""></option>
			                            @foreach($listJawatan as $jawatan)
			                                <option value="{{$jawatan->id}}"
		                                        @if ($jawatan->id == old('position_id', $user->position_id))
		                                        selected
		                                        @endif
			                                >{{$jawatan->name}}</option>
			                            @endforeach
			                        </select>
			                    </div>
							</div>
							<div class="col-2">
								<!--Skim -->
			                    <div class="form-group {{ $errors->has('Skim') ? ' has-error' : '' }} ">
			                        <label for="Skim" class="control-label">Skim</label>
			                        <select class="form-control input-lg" name="scheme_id" id="Skim" style="width: 100%;">
			                            <option value=""></option>
			                            @foreach($listSkim as $skim)
			                                <option value="{{$skim->id}}"
		                                        @if ($skim->id == old('scheme_id', $user->scheme_id))
		                                        selected
		                                        @endif
			                                >{{$skim->name}}</option>
			                            @endforeach
			                        </select>
			                    </div>
							</div>
							<div class="col-2">
								<!--Gred -->
			                    <div class="form-group {{ $errors->has('Gred') ? ' has-error' : '' }}">
			                        <label for="Gred" class="control-label">Gred</label>
			                        <select class="form-control input-lg" name="grade_id" id="Gred" style="width: 100%;">
			                            <option value=""></option>
			                            @foreach($listGred as $gred)
			                                <option value="{{$gred->id}}"
		                                        @if ($gred->id == old('grade_id', $user->grade_id))
		                                        selected
		                                        @endif
			                                >{{$gred->name}}</option>
			                            @endforeach
			                        </select>
			                    </div>
							</div>
						</div>
				  
	                    <!--Pegawai Atasan-->
	                    <div class="form-group{{ $errors->has('PegawaiAtasan') ? ' has-error' : '' }}">
	                        <label for="PegawaiAtasan" class="control-label">Pegawai Atasan</label><br>
	                        <select class="form-control input-lg" name="supervisor_id" id="PegawaiAtasan" style="width: 100%;">
	                            <option value="" ></option>
	                            @foreach($PegawaiAtasan as $pegawai)
	                                <option value="{{$pegawai->id}}"
	                                        @if ($pegawai->id == old('supervisor_id', $user->supervisor_id))
	                                        selected="selected"
	                                        @endif
	                                >{{$pegawai->name}}</option>
	                            @endforeach

	                        </select>
	                    </div>

						<button type="submit" class="btn btn-primary float-right">
		                    @icon('fe fe-save') {{ __('Simpan') }}
		                </button>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@else

@include('layouts.error_acl', [
	'title' => __('Percubaan Akses Terlarang'),
	'message' => __('Anda tidak dibenarkan mengakses kawasan ini.'),
])

@endcan
@endsection