@include('components.forms.hidden', [
	'id' => 'id',
	'name' => 'id',
	'value' => ''
])

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Nama',
			'name' => 'name'
		])
        </div>
        <div class="col-6">
                @include('components.forms.input', [
			'input_label' => 'Gelaran',
			'name' => 'honourary'
		])
		
	</div>
        <div class="col-6">
            @include('components.forms.input', [
                    'input_label' => 'No. Telefon',
                    'name' => 'phone_no',
                    'input_classes' => 'allownumericonly', 
                    'maxlength' => 15,
                    'type' => 'text'
            ])
        </div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'E-Mel',
			'name' => 'email',
			'type' => 'email'
		])

	</div>
</div>

<div class="row">
	<div class="col-6">
		<!--Jabatan Pelaksana -->
		<div class="form-group{{ $errors->has('Jabatan') ? ' has-error' : '' }}">
		    <label for="Jabatan" class="control-label">Jabatan</label><br>
		       <select class="form-control input-lg"  onchange="getBahagian(this);"  name="department_id" id="JabatanPelaksana" style="width: 100%;">
		        <option value="" ></option>
		                @foreach($JabatanPelaksana as $jp)
		        <option value="{{$jp->id}}" data-mail="{{$jp->code}}">{{$jp->name}}</option>
		                @endforeach
		    </select>
		</div>

		<!--Seksyen -->
		<div class="form-group{{ $errors->has('Seksyen') ? ' has-error' : '' }}">
		    <label for="Seksyen" class="control-label">Seksyen </label><br>
		    <select class="form-control input-lg" name="section_id" id="Seksyen" style="width: 100%;" >
		        <option value=""></option>
		    </select>
		</div>
	</div>
	<div class="col-6">
		<!--Bahagian -->
		<div class="form-group{{ $errors->has('Bahagian') ? ' has-error' : '' }}">
		    <label for="Bahagian" class="control-label">Bahagian </label><br>
		    <select class="form-control input-lg" name="executor_department_id" id="Bahagian" style="width: 100%;" onchange="getSeksyen(this)">
		        <option value=""></option>
		    </select>
		</div>
                @include('components.forms.select-multiple', [
			'input_label' => 'Kumpulan',
			'name' => 'roles',
			'options' => roles()->where('guard_name', 'web')->pluck('name', 'id')
				->transform(function($name, $id) {
					return title_case($name);
				})
		])
	</div>
</div>

<div class="row">
	<div class="col-8">
		<!--Jawatan -->
		<div class="form-group {{ $errors->has('Jawatan') ? ' has-error' : '' }}">
		    <label for="Jawatan" class="control-label">Jawatan</label>
		    <select class="form-control input-lg" name="position_id" id="Jawatan" style="width: 100%;">
		        <option value=""></option>
		        @foreach($listJawatan as $jawatan)
		            <option value="{{$jawatan->id}}">{{$jawatan->name}}</option>
		        @endforeach
		    </select>
		</div>		
	</div>
	<div class="col-2">
		<!--Skim -->
		<div class="form-group {{ $errors->has('Skim') ? ' has-error' : '' }} ">
		    <label for="Skim" class="control-label">Skim</label>
		    <select class="form-control input-lg" name="scheme_id" id="Skim" style="width: 100%;">
		        <option value=""></option>
		        @foreach($listSkim as $skim)
		            <option value="{{$skim->id}}">{{$skim->name}}</option>
		        @endforeach
		    </select>
		</div>		
	</div>
	<div class="col-2">
		<!--Gred -->
		<div class="form-group {{ $errors->has('Gred') ? ' has-error' : '' }}">
		    <label for="Gred" class="control-label">Gred</label>
		    <select class="form-control input-lg" name="grade_id" id="Gred" style="width: 100%;">
		        <option value=""></option>
		        @foreach($listGred as $gred)
		            <option value="{{$gred->id}}">{{$gred->name}}</option>
		        @endforeach
		    </select>
		</div>		
	</div>
</div>

<!--Pegawai Atasan-->
<div class="form-group{{ $errors->has('PegawaiAtasan') ? ' has-error' : '' }}">
    <label for="PegawaiAtasan" class="control-label">Pegawai Atasan</label><br>
    <select class="form-control input-lg" name="supervisor_id" id="PegawaiAtasan" style="width: 100%;">
        <option value="" ></option>
        @foreach($PegawaiAtasan as $user)
            <option value="{{$user->id}}" data-mail="{{$user->id}}">{{$user->name}}</option>
        @endforeach
    </select>
</div>

{{-- <div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Kata Laluan',
			'name' => 'password',
			'type' => 'password'
		])
	</div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Kenal Pasti Kata Laluan',
			'name' => 'password_confirmation',
			'type' => 'password'
		])
	</div>
</div> --}}