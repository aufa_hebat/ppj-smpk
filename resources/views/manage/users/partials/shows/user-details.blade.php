<h4>Pengguna</h4>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Nama</div>
        <div>{!! $user->name !!}</div> 
    </div>
    <div class="col-6">
        <div class="text-muted">Gelaran</div>
        <div>{!! $user->honourary !!}</div> 
    </div>
</div><br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">No. Telefon</div>
        <div>{!! $user->phone_no !!}</div> 
    </div>
    <div class="col-6">
        <div class="text-muted">E-Mel</div>
        <div>{!! $user->email !!}</div> 
    </div>
</div><br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Jabatan</div>
        <div>{!! $user->department->name !!}</div> 
    </div>
    <div class="col-6">
        <div class="text-muted">Bahagian</div>
        <div>
            @foreach($Seksyen->where('id','=',$user->executor_department_id) as $bhgn)
                @if ($bhgn->id == $user->executor_department_id)
                    {{$bhgn->name}}
                @endif
            @endforeach        
        </div>
    </div>
</div><br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Seksyen</div>
        <div>{!! $user->section->name !!}</div> 
    </div>
    <div class="col-6">
        <div class="text-muted">Kumpulan</div>
        @foreach($user->roles as $role)
            <div>{!! $role->name !!}</div> 
        @endforeach
    </div>             
</div><br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Jawatan</div>
        <div>{!! $user->position->name !!}</div> 
    </div>             
</div><br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Gred</div>
        <div>{!! $user->grade->name !!}</div> 
    </div>
    <div class="col-6">
        <div class="text-muted">Skim</div>
        <div>{!! $user->scheme->name !!}</div>     
    </div>
</div><br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Pegawai Atasan</div>
        <div>{!! $user->supervisor->name !!}</div> 
    </div>             
</div><br>

