@component('components.modals.base', [
	'id' => 'user-modal',
	'tooltip' => __('Pengguna'),
	'modal_title' => __('Pengguna'),
	])
	@slot('modal_body')
		<div class="row">
			<div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		    <div class="col-md-12 col-lg-12 col-xl-12">
		      	{{ html()->form('POST', '#')->id('user-form')->open() }}
					@method('POST')
					@include('manage.users.partials.forms.create')
				{{ html()->form()->close() }}
		    </div>
		    <div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		</div>
	@endslot
	@slot('modal_footer')
		<button class="btn btn-primary form-btn">{{ __('Simpan') }}</button> 
	@endslot
@endcomponent
