@extends('layouts.admin')

@push('scripts')
<script type="text/javascript">
    function getBahagian(value) {

        var ddl = value;
        var selectedOption = ddl.options[ddl.selectedIndex];

        var mailValue = selectedOption.getAttribute("data-mail");
        var textBoxBahagian = document.getElementById("Bahagian");
        var textBoxSeksyen = document.getElementById("Seksyen");

        /* remove all select options */
        textBoxBahagian.options.length = 0;
        textBoxSeksyen.options.length = 0;
        /* add Sila Pilih options */
        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "";
        textBoxBahagian.appendChild(opt);
       //alert(mailValue);

        /* add other options */
        @foreach($Seksyen as $bhgn)
        if (mailValue == "{{ $bhgn->category}}") {

            var opt = document.createElement('option');
            opt.value = "{{ $bhgn->id }}";
            opt.innerHTML = "{{$bhgn->name}}";
            opt.setAttribute('data-mail', '{{ $bhgn->code }}');
            textBoxBahagian.add(opt);
        }
        @endforeach

    }

</script>
<script type="text/javascript">
    function getSeksyen(value) {
        var dd2 = value;
        var selectedOption = dd2.options[dd2.selectedIndex];
        var mailValue1 = selectedOption.getAttribute("data-mail");
        var textBoxSeksyen = document.getElementById("Seksyen");

        /* remove all select options */
        textBoxSeksyen.options.length = 0;

        /* add options */
        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "";
        textBoxSeksyen.appendChild(opt);


        /* add other options */
        @foreach($Seksyen as $sksyn)
        if (mailValue1 == "{{ $sksyn->category}}") {
            var opt = document.createElement('option');
            opt.value = "{{ $sksyn->id }}";
            opt.innerHTML = "{{$sksyn->name}}";
            opt.setAttribute('data-mail', '{{ $sksyn->code }}');
            textBoxSeksyen.appendChild(opt);
        }
        @endforeach

    }

    $(".allownumericonly").on("keypress keyup blur",function (event) {    
       $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
</script>
@endpush

@section('content')
	@include('manage.users.partials.styles')
	@include('manage.users.partials.scripts', [
		'table_id' => 'user-management',
		'primary_key' => 'hashslug',
		'routes' => [
			'show' => 'api.manage.users.show',
			'store' => 'api.manage.users.store',
			'update' => 'api.manage.users.update',
			'destroy' => 'api.manage.users.destroy',
		],
		'columns' => [
			'name' => __('table.name'), 
			'email' => __('table.email'), 
			'roles_to_string' => __('table.role'), 
			'roles' => __('table.role'), 
			'created_at' => __('table.created_at'),
			'updated_at' => __('table.updated_at')
		],
		'forms' => [
			'create' => 'user-form',
			'edit' => 'user-form',
		], 
		'disabled' => [
			'email'
		]
	])
	@include('manage.users.partials.modals.form')
	@include('manage.users.partials.modals.show')
	@include('manage.users.partials.dashboard')
	<div class="row justify-content-center">
		<div class="col">
			@component('components.card')
				@slot('card_title')
					@include('components.modals.button', [
						'modal_btn_classes' => 'create-action-btn float-right',
						'label' => __('Pengguna Baru'),
						'icon' => 'fe fe-plus'
					])
				@endslot
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'user-management',
							'route_name' => 'api.datatable.manage.user',
							'columns' => [
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
								['data' => 'email', 'title' => __('Email'), 'defaultContent' => '-'],
                                                                ['data' => 'org_atasan', 'title' => __('Orang Atasan'), 'searchable' => false, 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Nama'), __('Email'),__('Orang Atasan'), __('table.action')
							],
							'actions' => minify(view('components.acl.actions_user')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection