<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (CIDB)'),
		    'id' => 'cidb_no',
		    'name' => 'cidb_no'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'cidb_start_date',
		    'name' => 'cidb_start_date',
        'config' => [
            'format' => config('datetime.display.date'),
        ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'cidb_end_date',
		    'name' => 'cidb_end_date',
        'config' => [
            'format' => config('datetime.display.date'),
        ]
		])
	</div>
</div>

@include('components.forms.input', [
	'input_label' => 'Alamat (Baris Pertama)',
	'name' => 'cidb_primary',
	'id' => 'cidb_primary'
])

@include('components.forms.input', [
	'input_label' => 'Alamat (Baris Kedua)',
	'name' => 'cidb_secondary',
	'id' => 'cidb_secondary'
])

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'cidb_postcode',
			'type' => 'number',
			'id' => 'cidb_postcode'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'cidb_state',
            'id' => 'cidb_state',
        ])
	</div>
</div>

<div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
    <div class="mb-0 card card-primary">
      	<div class="card-header" role="tab" id="headingOne2">
            <h4 class="card-title">
              	<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                	GRED
              	</a>
            </h4>
      	</div>
        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
          	<div class="card-body">
                <div id="tablegred">
                	<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
            		<table class="table" width="100%" cellspacing="0">
              			<thead>
           				 	<tr>
                  				<td width="5%"></td>
                  				<td>GRED</td>
                			</tr>
              			</thead>
              			<tbody class="list">
              			@foreach($gred as $bil => $row2)
               				<tr>
                  				<td>
                    				<div class="form-group row justify-content-end row" style="margin-top: 0;">
                      					<div class="col-lg-10">
                        					<div class="checkbox">
                          						<label>
                            					<input type="checkbox" id="Gred{{$bil}}" name="Gred[]" value="{{$row2->code}}"
                              					@if(!empty($company->syarikatCIDBGreds)){
                                				@foreach($company->syarikatCIDBGreds as $gred)
                                  				@if ($row2->code == old('Gred[]',$gred->grades))
                                    			checked="checked"
                                  				@endif
                                				@endforeach
                              					@endif
                            					/>
                          						</label>
                        					</div>
                      					</div>
                    				</div>
                  				</td>
                  				<td class="gred">{{$row2->name}}</td>
                			</tr>
          				@endforeach
              			</tbody>
            		</table>
          		</div>
        	</div>
      	</div>

      	<div class="mb-0 card card-primary">
        	<div class="card-header" role="tab" id="headingTwo2">
          		<h4 class="card-title">
            		<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
              			KATEGORI
            		</a>
          		</h4>
        	</div>
        	<div id="collapseTwo2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
          		<div class="card-body">
            		<body onload="hideShowAllKhusus()">
              			<div id="tablekat">
            				<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
        					<table class="table" width="100%" cellspacing="0">
          						<thead>
            						<tr>
              							<td width="5%"></td>
              							<td>KATEGORI</td>
            						</tr>
          						</thead>
              					<tbody class="list">
          						@foreach($kategori as $bil => $row2)
                					<tr>
              							<td>
                							<div class="form-group row justify-content-end row" style="margin-top: 0;">
                      							<div class="col-lg-10">
                            						<div class="checkbox">
                              							<label>
                                							<input type="checkbox" id="Kategori{{$bil}}" name="Kategori[]" data-kategori="{{$row2->code}}" value="{{$row2->code}}"
                                  							@foreach($company->syarikatCIDBKategori as $kat)
                                							@if ($row2->code == old('Kategori[]',$kat->categories))
                                      						checked="checked"
                                    						@endif
                                  							@endforeach
                                							>
                              							</label>
                    								</div>
                  								</div>
                							</div>
              							</td>
              							<td class="kat">{{$row2->name}}</td>
            						</tr>
          						@endforeach
          						</tbody>
    						</table>
      					</div>
        			</body>
      			</div>
    		</div>
  		</div>

      	<div id="KategoriB">
        	<div class="mb-0 card card-primary">
              	<div class="card-header" role="tab" id="heading3">
                	<h4 class="card-title">
                  		<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                    		PENGKHUSUSAN B
                  		</a>
            		</h4>
              	</div>
              	<div id="collapse3" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                	<div class="card-body">
                  	<!--Kategori B-->
                  		<div id="tablekusb">
                			<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
              				<table class="table"  width="100%" cellspacing="0">
                				<thead>
                  					<tr>
                    					<td width="5%"></td>
                    					<td>PENGKHUSUSAN</td>
                  					</tr>
                				</thead>
                				<tbody class="list">
            					@foreach($pengkhususan->where('category', 'B') as $bil => $row2)
                  					<tr>
                    					<td>
                      						<div class="form-group row justify-content-end row" style="margin-top: 0;">
                            					<div class="col-lg-10">
                              						<div class="checkbox">
                                						<label>
                              							<input type="checkbox" id="Pengkhususan" name="PengkhususanB[]" value="{{$row2->code}}"
                            							@foreach($company->syarikatCIDBKategori->where('categories','=','B') as $khusus)
                                      					@foreach($khusus->syarikatCIDBKhusus as $khu)
                                        				@if ($row2->code == old('PengkhususanB[]',$khu->khusus))
                                          				checked="checked"
                                        				@endif
                                      					@endforeach
                                    					@endforeach
                                  						>
                                						</label>
                              						</div>
                            					</div>
                          					</div>
                        				</td>
                        				<td class="kusb">{{$row2->name}}</td>
                      				</tr>
                				@endforeach
                    			</tbody>
                  			</table>
                		</div>
                	</div>
              	</div>
            </div>
      	</div>

      	<div id="KategoriCE">
            <div class="mb-0 card card-primary">
              	<div class="card-header" role="tab" id="heading4">
                	<h4 class="card-title">
                  		<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                   	 		PENGKHUSUSAN CE
                  		</a>
            		</h4>
              	</div>
              	<div id="collapse4" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                	<div class="card-body">
                  	<!--Kategori CE-->
                  		<div id="tablekusce">
                			<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  			<table class="table"  width="100%" cellspacing="0">
                    			<thead>
                      				<tr>
                        				<td width="5%"></td>
                        				<td>PENGKHUSUSAN</td>
                      				</tr>
                    			</thead>
                    			<tbody class="list">
                    			@foreach($pengkhususan->where('category', 'CE') as $bil => $row2)
                      				<tr>
                        				<td>
                          					<div class="form-group row justify-content-end row" style="margin-top: 0;">
                            					<div class="col-lg-10">
                              						<div class="checkbox">
                                						<label>
                                  						<input type="checkbox" id="Pengkhususan" name="PengkhususanCE[]" value="{{$row2->code}}"
                                    					@foreach($company->syarikatCIDBKategori->where('categories','=','CE') as $khusus)
                                      					@foreach($khusus->syarikatCIDBKhusus as $khu)
                                        				@if ($row2->code == old('PengkhususanCE[]',$khu->khusus))
                                          				checked="checked"
                                        				@endif
                                      					@endforeach
                                    					@endforeach
                              							>
                                						</label>
                              						</div>
                            					</div>
                          					</div>
                        				</td>
                        				<td class="kusce">{{$row2->name}}</td>
                      				</tr>
                    			@endforeach
	                    		</tbody>
	                  		</table>
                		</div>
              		</div>
        		</div>
          	</div>
      	</div>

      	<div id="KategoriM">
            <div class="mb-0 card card-primary">
              	<div class="card-header" role="tab" id="heading5">
                	<h4 class="card-title">
                  		<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                    		PENGKHUSUSAN M
                  		</a>
                	</h4>
              	</div>
              	<div id="collapse5" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                	<div class="card-body">
                  	<!--Kategori M-->
                  		<div id="tablekusm">
                			<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  			<table class="table"  width="100%" cellspacing="0">
                    			<thead>
                      				<tr>
                        				<td width="5%"></td>
                        				<td>PENGKHUSUSAN</td>
                      				</tr>
                    			</thead>
                    			<tbody class="list">
                    			@foreach($pengkhususan->where('category', 'M') as $bil => $row2)
                      				<tr>
                        				<td>
                          					<div class="form-group row justify-content-end row" style="margin-top: 0;">
                            					<div class="col-lg-10">
                              						<div class="checkbox">
                                						<label>
                                  							<input type="checkbox" id="Pengkhususan" name="PengkhususanM[]" value="{{$row2->code}}"
                                    						@foreach($company->syarikatCIDBKategori->where('categories','=','M') as $khusus)
                                      						@foreach($khusus->syarikatCIDBKhusus as $khu)
                                        					@if ($row2->code == old('PengkhususanM[]',$khu->khusus))
                                          					checked="checked"
                                        					@endif
                                      						@endforeach
                                    						@endforeach
                                  							>
                                						</label>
                              						</div>
                            					</div>
                          					</div>
                        				</td>
                        				<td class="kusm">{{$row2->name}}</td>
                      				</tr>
                    			@endforeach
                    			</tbody>
                  			</table>
                		</div>
              		</div>
        		</div>
          	</div>
      	</div>

      	<div id="KategoriE">
            <div class="mb-0 card card-primary">
              	<div class="card-header" role="tab" id="heading6">
                	<h4 class="card-title">
                  		<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                    		PENGKHUSUSAN E
                  		</a>
                	</h4>
              	</div>
              	<div id="collapse6" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                	<div class="card-body">
                  	<!--Kategori E-->
                  		<div id="tablekuse">
                			<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
              				<table class="table"  width="100%" cellspacing="0">
                    			<thead>
                      				<tr>
                        				<td width="5%"></td>
                        				<td>PENGKHUSUSAN</td>
                      				</tr>
                    			</thead>
                    			<tbody class="list">
                    			@foreach($pengkhususan->where('category', 'E') as $bil => $row2)
                      				<tr>
                        				<td>
                          					<div class="form-group row justify-content-end row" style="margin-top: 0;">
                            					<div class="col-lg-10">
                              						<div class="checkbox">
                                						<label>
                                  						<input type="checkbox" id="Pengkhususan" name="PengkhususanE[]" value="{{$row2->code}}"
                                    					@foreach($company->syarikatCIDBKategori->where('categories','=','E') as $khusus)
                                      					@foreach($khusus->syarikatCIDBKhusus as $khu)
                                        				@if ($row2->code == old('PengkhususanE[]',$khu->khusus))
                                          				checked="checked"
                                        				@endif
                                      					@endforeach
                                    					@endforeach
                                  						>
                                						</label>
                              						</div>
                            					</div>
                          					</div>
                        				</td>
                        				<td class="kuse">{{$row2->name}}</td>
                      				</tr>
                    			@endforeach
                    			</tbody>
                  			</table>
                		</div>
              		</div>
            	</div>
          	</div> 
      	</div>
  	</div>
</div><br>

<div class="row">
	<div class="col-5">
		<label>Sijil CIDB</label>
		@foreach($cidb_doc as $doc)
			@if(!($doc->document_name) == NULL)
			<br>
			<input type="text" name="" class="form-control" readonly="readonly" value="{{$doc->document_name}}">
			
	</div>
	<div class="col-1" style="margin-top: 30px;">
        <div><a class="newwindow{{$doc->document_id}} btn btn-primary" tt="link" href="#"><i class="fe fe-file"></i></a></div>
    </div>
	<div class="col-6">
			@endif
		@endforeach
		<br>
		<input type="file" name="cidb" class="form-control" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
	</div>
</div><br>

<div class="float-right">
@include('components.forms.switch', [
    'id' => 'cidb_blacklist', 
    'name' => 'cidb_blacklist', 
    'label' => 'Senarai Hitam',
    'checked' => old('cidb_blacklist', (!empty($company->cidb_blacklist))? $company->cidb_blacklist: false)
])
</div>
<div class="cidbblacklist">
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Tamat Senarai Hitam'),
    'id' => 'cidb_blacklist_end_date',
    'name' => 'cidb_blacklist_end_date',
    'config' => [
        'format' => config('datetime.display.date'),
    ]
])
</div>