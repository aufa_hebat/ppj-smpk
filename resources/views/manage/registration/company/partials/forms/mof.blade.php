<div class="table-responsive">
    <table class="table" width="100%" cellspacing="0">
    	<thead>
			<tr>
				<th>No.Rujukan Pendaftaran (MOF)</th>
				<th>Tarikh Sah Sijil</th>
				<th>Bidang</th>
              	<th>Pengkhususan Berdaftar</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$company->mof_no}}</td>
				<td>@if(!empty($company->mof_start_date) && !empty($company->mof_end_date)){{date('d/m/Y', strtotime($company->mof_start_date))}} hingga {{date('d/m/Y', strtotime($company->mof_end_date))}}@endif</td>
				<td>
                    @if(!empty($company->syarikatMOFBidang)) 
                      	@foreach($company->syarikatMOFBidang as $bidang)
                            {{$bidang->areas}} 
                      	@endforeach 
                    @endif
              	</td>
				<td>
					@if(!empty($company->syarikatMOFBidang)) 
                      	@foreach($company->syarikatMOFBidang as $kategori) 
                            @if(!empty($kategori->syarikatMOFKhusus)) 
                              	@foreach($kategori->syarikatMOFKhusus as $khusus)
                                    {{$khusus->khusus}} 
                              	@endforeach 
                            @endif 
                      	@endforeach 
                    @endif
				</td>
			</tr>
		</tbody>
	</table>
</div>