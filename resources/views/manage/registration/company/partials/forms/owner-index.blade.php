
	@include('manage.registration.owner.partials.scripts', [
		'table_id' => 'owner-management',
		'primary_key' => 'hashslug',
		'param' => 'hashslug=' . $hashslug,
		'routes' => [
			'show' => 'api.manage.owners.show',
			'store' => 'api.manage.owners.store',
			'update' => 'api.manage.owners.update',
			'destroy' => 'api.manage.owners.destroy',
		],
		'columns' => [
			'name' => __('table.name'), 
			'ic_number' => __('table.ic_number'), 
			'email' => __('table.email'), 
			'created_at' => __('table.created_at'),
			'updated_at' => __('table.updated_at')
		],
		'forms' => [
			'create' => 'owner-form',
			'edit' => 'owner-form',
		], 
		'disabled' => [
			'email'
		]
	])
	@component('components.forms.hidden-form', 
		[
			'id' => 'destroy-record-form',
			'action' => route('manage.owners.destroy', 'dummy')
		])
		@slot('inputs')
			@method('DELETE')
		@endslot
	@endcomponent
	<div class="row justify-content-center">
		<div class="col-12">
			@component('components.card')
				@slot('card_title')
					<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalTambahPemilikSyarikat">
				        @icon('fe fe-plus') {{ __('Pemilik Syarikat Baru') }}
				    </button>
				@endslot
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'owner-management',
							'param' => 'hashslug=' . $hashslug,
							'route_name' => 'api.datatable.manage.owner',
							'columns' => [
								['data' => 'name', 'title' => __('Nama Pemilik Syarikat'), 'defaultContent' => '-'], 
								['data' => 'ic_number', 'title' => __('No Kad Pengenalan'), 'defaultContent' => '-'],
								['data' => 'email', 'title' => __('E-Mel'), 'defaultContent' => '-'],
								['data' => 'type', 'title' => __('Sijil'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false],
							],
							'headers' => [
								__('Nama Pemilik Syarikat'), __('No Kad Pengenalan'), __('E-mel'), __('Sijil'), __('table.action')
							],
							'actions' => minify(view('manage.registration.owner.partials.actions')->render())
						]
					)
					@endcomponent
					<br>

                    <div class="pull-right btn-group">
                        @if(!empty($company->company_name) && !empty($company->ssm_start_date) && !empty($company->owners->first()->name))
	                        <form id="box-form"  method="POST" action="{{ route('registration.companies.update',$company->id) }}" files="true" enctype="multipart/form-data">
								@csrf
								@method('PUT')
	                            <input id="confirmation" name="confirmation" type="hidden"/>
	                            <input type="hidden" name="ssm_no" style="text-transform: uppercase;" value="{{ $company->ssm_no }}">
	                            <input type="hidden" name="company_name" value="{{ $company->company_name }}">
	                            <input type="hidden" name="primary" value="{{ $company->addresses[0]->primary }}">
	                            <input type="hidden" name="secondary" value="{{ $company->addresses[0]->secondary }}">
	                            <input type="hidden" name="state" value="{{ $company->addresses[0]->state }}">
	                            <input type="hidden" name="postcode" value="{{ $company->addresses[0]->postcode }}">
	                            <input type="hidden" name="phone_number3" value="{{ $company->phones[0]->phone_number }}">
	                            <input type="hidden" name="phone_number2" value="{{ $company->phones[1]->phone_number }}">
	                            <input type="hidden" name="phone_number4" value="@if(!empty($company->phones[2]->phone_number)){{ $company->phones[2]->phone_number }}@endif">
	                            <input type="hidden" name="email" value="{{ $company->email }}">
	                            <input type="hidden" name="gst_registered_no" value="@if(!empty($company->gst_registered_no)){{ $company->gst_registered_no }}@endif">
	                            <input type="hidden" name="gst_start_date" value="@if(!empty($company->gst_start_date)){{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->gst_start_date)->format('d/m/Y') }}@endif">
	                            <input type="hidden" name="bank_name" value="@if(!empty($company->bank_name)){{ $company->bank_name }}@endif">
	                            <input type="hidden" name="account_bank_no" value="@if(!empty($company->account_bank_no)){{ $company->account_bank_no }}@endif">
	                            <button type="button" id="selesai_submit_button" class="btn btn-success float-middle border-default confirm">
	                                @icon('fe fe-check') {{ __('Selesai') }}
	                            </button>
	                        </form>
                        @endif
                    </div>
				@endslot
			@endcomponent
		</div>
	</div>

<div class="modal fade" id="modalTambahPemilikSyarikat" role="dialog" aria-labelledby="modalTambahPemilikSyarikatModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahPemilikSyarikatModalLongTitle">Tambah Pemilik Syarikat Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Tutup') }}">
          <span aria-hidden="true"></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                @include('manage.registration.owner.create')
            </div>
        </div>
      </div>
    </div>
  </div>
</div>