<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (BPKU)'),
		    'id' => 'bpku_no',
		    'name' => 'bpku_no'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'bpku_start_date',
		    'name' => 'bpku_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'bpku_end_date',
		    'name' => 'bpku_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
	<div class="col-5">
		<label>Sijil BPKU</label>
		@foreach($bpku_doc as $doc)
			@if(!($doc->document_name) == NULL)
			<br>
			<input type="text" name="" class="form-control" readonly="readonly" value="{{$doc->document_name}}">
			
	</div>
	<div class="col-1" style="margin-top: 30px;">
        <div><a class="newwindow{{$doc->document_id}} btn btn-primary" tt="link" href="#"><i class="fe fe-file"></i></a></div>
    </div>
	<div class="col-6">
			@endif
		@endforeach
		<br>
		<input type="file" name="bpku" class="form-control" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
	</div>
</div>

<div class="float-right">
@include('components.forms.switch', [
    'id' => 'bpku_blacklist', 
    'name' => 'bpku_blacklist', 
    'label' => 'Senarai Hitam',
    'checked' => old('bpku_blacklist', (!empty($company->bpku_blacklist))? $company->bpku_blacklist: false)
])
</div>
<div class="bpkublacklist">
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Tamat Senarai Hitam'),
    'id' => 'bpku_blacklist_end_date',
    'name' => 'bpku_blacklist_end_date',
    'config' => [
        'format' => config('datetime.display.date'),
    ]
])
</div>