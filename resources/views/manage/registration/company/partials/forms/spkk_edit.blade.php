<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (SPKK)'),
		    'id' => 'spkk_no',
		    'name' => 'spkk_no'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'spkk_start_date',
		    'name' => 'spkk_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'spkk_end_date',
		    'name' => 'spkk_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
	<div class="col-5">
		<label>Sijil SPKK</label>
		@foreach($spkk_doc as $doc)
			@if(!($doc->document_name) == NULL)
			<br>
			<input type="text" name="" class="form-control" readonly="readonly" value="{{$doc->document_name}}">
			
	</div>
	<div class="col-1" style="margin-top: 30px;">
        <div><a class="newwindow{{$doc->document_id}} btn btn-primary" tt="link" href="#"><i class="fe fe-file"></i></a></div>
    </div>
	<div class="col-6">
			@endif
		@endforeach
		<br>
		<input type="file" name="spkk" class="form-control" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
	</div>
</div>

<div class="float-right">
@include('components.forms.switch', [
    'id' => 'spkk_blacklist', 
    'name' => 'spkk_blacklist', 
    'label' => 'Senarai Hitam',
    'checked' => old('spkk_blacklist', (!empty($company->spkk_blacklist))? $company->spkk_blacklist: false)
])
</div>
<div class="spkkblacklist">
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Tamat Senarai Hitam'),
    'id' => 'spkk_blacklist_end_date',
    'name' => 'spkk_blacklist_end_date',
    'config' => [
        'format' => config('datetime.display.date'),
    ]
])
</div>