<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (SSM)'),
		    'id' => 'ssm_no1',
		    'name' => 'ssm_no'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'ssm_start_date',
		    'name' => 'ssm_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'ssm_end_date',
		    'name' => 'ssm_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

@include('components.forms.input', [
	'input_label' => 'Alamat (Baris Pertama)',
	'name' => 'ssm_primary',
	'id' => 'ssm_primary'
])

@include('components.forms.input', [
	'input_label' => 'Alamat (Baris Kedua)',
	'name' => 'ssm_secondary',
	'id' => 'ssm_secondary'
])

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'ssm_postcode',
			'type' => 'number',
			'id' => 'ssm_postcode'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'ssm_state',
            'id' => 'ssm_state',
        ])
	</div>
</div>

<div class="row">
	<div class="col-5">
		<label>Sijil SSM</label>
		@foreach($ssm_doc as $doc)
			@if(!($doc->document_name) == NULL)
			<br>
			<input type="text" name="" class="form-control" readonly="readonly" value="{{$doc->document_name}}">
			
	</div>
	<div class="col-1" style="margin-top: 30px;">
        <div><a class="newwindow{{$doc->document_id}} btn btn-primary" tt="link" href="#"><i class="fe fe-file"></i></a></div>
    </div>
	<div class="col-6">
			@endif
		@endforeach
		<br>
		<input type="file" name="ssm" class="form-control" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
	</div>
</div>