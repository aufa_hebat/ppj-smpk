<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (KDN)'),
		    'id' => 'kdn_no',
		    'name' => 'kdn_no'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'kdn_start_date',
		    'name' => 'kdn_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'kdn_end_date',
		    'name' => 'kdn_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
	<div class="col-5">
		<label>Sijil KDN</label>
		@foreach($kdn_doc as $doc)
			@if(!($doc->document_name) == NULL)
			<br>
			<input type="text" name="" class="form-control" readonly="readonly" value="{{$doc->document_name}}">
			
	</div>
	<div class="col-1" style="margin-top: 30px;">
        <div><a class="newwindow{{$doc->document_id}} btn btn-primary" tt="link" href="#"><i class="fe fe-file"></i></a></div>
    </div>
	<div class="col-6">
			@endif
		@endforeach
		<br>
		<input type="file" name="kdn" class="form-control" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
	</div>
</div>

<div class="float-right">
@include('components.forms.switch', [
    'id' => 'kdn_blacklist', 
    'name' => 'kdn_blacklist', 
    'label' => 'Senarai Hitam',
    'checked' => old('kdn_blacklist', (!empty($company->kdn_blacklist))? $company->kdn_blacklist: false)
])
</div>
<div class="kdnblacklist">
@include('components.forms.datetimepicker', [
    'input_label' => __('Tarikh Tamat Senarai Hitam'),
    'id' => 'kdn_blacklist_end_date',
    'name' => 'kdn_blacklist_end_date',
    'config' => [
        'format' => config('datetime.display.date'),
    ]
])
</div>