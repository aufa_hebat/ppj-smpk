@extends('layouts.admin')

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#hashslug').val('{{ $owner->hashslug }}');
            $('#name').val('{{ $owner->name }}');
            $('#ic_number').val('{{ $owner->ic_number }}');
            $('#email').val('{{ $owner->email }}');
            $('#primary').val('{{ str_replace(["\r\n","\r","\n"],"\\n ",$owner->addresses[0]->primary) }}');
            $('#secondary').val('{{ $owner->addresses[0]->secondary }}');
            $('#postcode').val('{{ $owner->addresses[0]->postcode }}');
            $('#state').val('{{ $owner->addresses[0]->state }}');
            $('#phone_number').val('{{ $owner->phones[0]->phone_number }}');

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Pemilik Syarikat') !!}', response.data.message, 'success');
                });
            });
        });
    </script>
@endpush

@section('content')
<form method="POST" action="{{ route('manage.owners.update',$owner->id) }}">
    @csrf
    @method('PUT')
	<div class="row">
        @component('components.card', ['card_classes' => 'col-10'])
            @slot('card_body')
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'id' => 'hashslug',
                            'name' => 'hashslug',
                            'value' => ''
                        ])
                        @include('components.forms.input', [
                            'input_label' => 'Nama Pemilik Syarikat',
                            'id' => 'name',
                            'name' => 'name'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kad Pengenalan',
                            'id' => 'ic_number',
                            'name' => 'ic_number'
                        ])
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'E-Mel',
                            'id' => 'email',
                            'name' => 'email'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'No. Telefon',
                            'id' => 'phone_number',
                            'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
                            'type' => 'number'
                        ])
                    </div>
                </div>

                @include('components.forms.input', [
                    'input_label' => 'Alamat (Baris Pertama)',
                    'id' => 'primary',
                    'name' => 'primary'
                ])

                @include('components.forms.input', [
                    'input_label' => 'Alamat (Baris Kedua)',
                    'id' => 'secondary',
                    'name' => 'secondary'
                ])

                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Poskod',
                            'id' => 'postcode',
                            'name' => 'postcode',
                            'type' => 'number'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.select', [
                            'input_label' => 'Negeri',
                            'id' => 'state',
                            'name' => 'state',
                            'options' => $std_cd_state->pluck('description','description'),
                        ])
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 20px;margin-left: 0px;">
                    <label class="control-label" for="inputLarge" style="font-size: 15px;">Pemilik Sijil Kelayakan</label>
                        <div class="col-lg-10">
                            <div class="checkbox">
                                <label>
                                    <input type="hidden" name="cidb_cert" value="0">
                                    <input type="checkbox" value="1" name="cidb_cert"
                                        @if(!empty($owner->cidb_cert))
                                            checked="checked"
                                        @endif> CIDB
                                </label>
                                <label>
                                    <input type="hidden" name="mof_cert" value="0">
                                    <input type="checkbox" value="1" name="mof_cert"    @if(!empty($owner->mof_cert))
                                            checked="checked"
                                        @endif> MOF
                                </label>
                                <label>
                                    <input type="hidden" name="kdn_cert" value="0">
                                    <input type="checkbox" value="1" name="kdn_cert" @if(!empty($owner->kdn_cert))
                                        checked="checked"
                                    @endif> KDN
                                </label>
                            </div>
                        </div>
                    </div>
                <div class="pull-right btn-group">
                    <a href="{{ route('manage.companies.edit',['company_id' => $owner->company->hashslug]) }}"
                       class="btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>
                    <button type="submit" class="btn btn-primary float-right">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
        @endslot
	@endcomponent
</div>
</form>
@endsection