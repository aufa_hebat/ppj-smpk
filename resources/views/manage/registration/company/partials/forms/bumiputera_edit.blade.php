<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (Bumiputera)'),
		    'id' => 'bumiputra_no',
		    'name' => 'bumiputra_no'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'bumiputra_start_date',
		    'name' => 'bumiputra_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'bumiputra_end_date',
		    'name' => 'bumiputra_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
	<div class="col-5">
		<label>Sijil Bumiputra</label>
		@foreach($bumi_doc as $doc)
			@if(!($doc->document_name) == NULL)
			<br>
			<input type="text" name="" class="form-control" readonly="readonly" value="{{$doc->document_name}}">
			
	</div>
	<div class="col-1" style="margin-top: 30px;">
        <div><a class="newwindow{{$doc->document_id}} btn btn-primary" tt="link" href="#"><i class="fe fe-file"></i></a></div>
    </div>
	<div class="col-6">
			@endif
		@endforeach
		<br>
		<input type="file" name="bumiputra" class="form-control" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
	</div>
</div>