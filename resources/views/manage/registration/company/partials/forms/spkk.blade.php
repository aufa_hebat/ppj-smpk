<div class="table-responsive">
    <table class="table" width="100%" cellspacing="0">
    	<thead>
			<tr>
				<th>No.Rujukan Pendaftaran (SPKK)</th>
				<th>Tarikh Sah Sijil</th>
				<th>Gred</th>
              	<th>Kategori</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$company->spkk_no}}</td>
				<td>@if(!empty($company->spkk_start_date) && !empty($company->spkk_end_date)){{date('d/m/Y', strtotime($company->spkk_start_date))}} hingga {{date('d/m/Y', strtotime($company->spkk_end_date))}}@endif</td>
				<td>
					@if(!empty($company->syarikatCIDBgreds)) 
                      	@foreach($company->syarikatCIDBgreds as $gred)
	                        {{$gred->grades}} 
                      	@endforeach 
                    @endif
				</td>
				<td>
                    @if(!empty($company->syarikatCIDBkategori)) 
                      	@foreach($company->syarikatCIDBkategori as $kategori)
                            {{$kategori->categories}} 
                      	@endforeach 
                    @endif
              	</td>
			</tr>
		</tbody>
	</table>
</div>