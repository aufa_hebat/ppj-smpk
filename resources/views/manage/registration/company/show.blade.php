{{--  @extends('layouts.app')  --}}
@extends('layouts.public.app2')

@push('scripts')
  <script src="{{ asset('js/select2.js') }}"></script>
  <script>
      $(function () {
          $('.select2').select2();
      })
      $(document).ready(function() {
        $('#tokens').select2({
          width: '100%',
          placeholder: '',
          language: {
            noResults: function() {
              return 'Tiada Rekod. Sila klik pada butang "+ Syarikat Baru" untuk pendaftaran. <div class="float-right"><button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalDisclaimer">@icon('fe fe-plus'){{ __('Syarikat Baru') }}</button></div>';
            },
          },
          escapeMarkup: function(markup) {
            return markup;
          },
        });
      });
  </script>
@endpush

@push('styles')
  <link href="{{ asset('css/select2.css') }}" rel="stylesheet">
@endpush

@section('content')

  @component('components.card')
    @slot('card_title')
      <h3>Pendaftaran Syarikat</h3>
    @endslot

    @slot('card_body')
      <p><h4>Sila masukkan Nama atau nombor pendaftaran syarikat anda untuk semakan rekod pendaftaran.</h4></p>
      <div class="row">
        <div class="col-12">
          <select id="tokens" class="form-control select2" title="Carian Syarikat ..." multiple data-live-search="true">
            @foreach($syarikat as $company)
              <option data-tokens="{{ $company->company_name }}" disabled="true">{{ $company->company_name }} ({{ $company->ssm_no }})</option>
            @endforeach
          </select>
        </div>
        {{-- <div class="col-2">
          <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalDisclaimer">
            @icon('fe fe-plus') {{ __('Syarikat Baru') }}
          </button>
        </div> --}}
      </div>
      <br><br>
      <p><h4><font color="red">*</font> Pendaftaran syarikat baru bagi memasuki tender / sebut harga yang ditawarkan oleh Perbadanan Putrajaya.</h4></p>
      <p><h4><font color="red">*</font> Setelah pendaftaran syarikat dibuat melalui sistem, sekiranya terdapat sebarang perubahan atau kemaskini butiran syarikat boleh dibuat di Kaunter Jualan, Bahagian Perolehan & Ukur Bahan, Blok C, Aras 1, Kompleks Perbadanan Putrajaya, 24, Persiaran Perdana, Presint 3, 62675, Wilayah Persekutuan Putrajaya.</h4></p>

    @endslot
  @endcomponent

  <div class="modal fade" id="modalDisclaimer" style="z-index: 10000" role="dialog" aria-labelledby="modalDisclaimerModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="modalDisclaimerModalLongTitle">Terma dan Syarat</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
              <i><b>Pendedahan Maklumat </b></i>
              <br><br>Perbadanan Putrajaya tidak akan mendedahkan kepada pihak ketiga maklumat peribadi dan / atau demografi anda kecuali seperti yang dinyatakan dalam dua subperenggan berikut.
              <ol><br>
                <li>Bagi membantu tujuan-tujuan yang dinyatakan di atas dan penyediaan produk dan perkhidmatan kepada anda, kami mungkin akan mendedahkan Maklumat Peribadi anda kepada pihak ketiga mengikut pengkelasan seperti berikut :</li>
                <ul>
                  <li style="list-style-type:disc;">Gabungan syarikat-syarikat kami dan anak syarikat kami di dalam atau luar Malaysia;</li>
                  <li style="list-style-type:disc;">Sebarang institusi kewangan dan syarikat-syarikat yang mengeluarkan kad caj atau kad kredit.</li>
                  <li style="list-style-type:disc;">Penasihat, juruaudit, perunding, kontraktor, pembekal produk dan perkhidmatan kami sekadar makluman yang diperlukan sahaja;</li>
                  <li style="list-style-type:disc;">Pihak berkuasakawal selia, agensi-agensi penguatkuasaan dan mana-mana pihak yang berkaitan dengan prosiding undang-undang;</li>
                  <li style="list-style-type:disc;">Mana-mana pemegang serah hak atau bakal penerima pindah milik atau pemeroleh syarikat atau perniagaan kami atau berkenaan dengan perlaksanaan korporat.</li>
                </ul><br>
                  <li>Perbadanan Putrajaya mungkin mendedahkan maklumat seperti itu kepada syarikat-syarikat dan individu-individu yang bekerja dengan kami untuk menjalankan tugas sebagai wakil kami. Contohnya termasuk menjadi hos pelayan Web kami, menganalisis data, memproses bayaran kad kredit, dan memberi perkhidmatan kepada pelanggan. Syarikat-syarikat ini dan individu-individu akan mempunyai akses kepada maklumat peribadi anda sebagai keperluan bagi melaksanakan tugas mereka, tetapi mereka tidak boleh berkongsi maklumat tersebut dengan mana-mana pihak ketiga lain.</li><br>
                  <li>Perbadanan Putrajaya mungkin mendedahkan maklumat seperti itu jika dikehendaki oleh undang-undang dan, jika diminta berbuat demikian oleh satu entiti kerajaan atau jika kita meyakini dengan niat baik tindakan seumpama itu adalah perlu untuk:</li>
                <ul>
                  <li style="list-style-type:disc;">mematuhi keperluan-keperluan undang-undang atau mematuhi proses undang-undang;</li>
                  <li style="list-style-type:disc;">melindungi hak atau harta Perbadanan Putrajaya;</li>
                  <li style="list-style-type:disc;">menghalang satu jenayah atau mempertahankan keselamatan negara; atau</li>
                  <li style="list-style-type:disc;">melindungi keselamatan diri pengguna-pengguna atau orang awam.</li>
                </ul>
              </ol><br>
              <i><b>Akses, pembetulan atau menghadkan proses-proses Maklumat Peribadi Anda.</b></i>
              <ol> <br>
                <li>Pada bila-bila masa anda boleh mengemukakan permintaan untuk akses, membuat pembetulan, mengemaskini atau menghadkan proses-proses maklumat peribadi anda dengan memaklumkan kepada kami secara bertulis kepada butir-butir hubungan seperti yang tertera di bawah :</li>
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Perbadanan Putrajaya, 
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kompleks Perbadanan Putrajaya,
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;24, Persiaran Perdana, Presint 3, 
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;62675 Putrajaya. 
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Emel : ppjonline@ppj.gov.my 
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telefon : 03-8000 8000 
                  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Faksimili : 03-88875000
                  <br><br>
                <li>Sekiranya anda tidak bersetuju dengan kami untuk memproses Maklumat Peribadi anda seperti di atas, kami mungkin tidak dapat :</li>
                <ul>
                  <li style="list-style-type:disc;">Memproses permohonan anda;</li>
                  <li style="list-style-type:disc;">Memberikan maklumat - maklumat terkini, produk-produk atau perkhidmatan yang diminta oleh anda yang berkenaan dengan produk dan perkhidmatan kami; atau</li>
                  <li style="list-style-type:disc;">Memproses atau melaksanakan transaksi perniagaan yang releven.</li>
                </ul>
              </ol>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="float-right">
              <div class=" btn-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                  {{ __('Tidak Setuju') }}
                </button>
                <a class="btn btn-primary" href="{{ route('registration.companies.create') }}">
                  {{ __('Setuju') }}
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection