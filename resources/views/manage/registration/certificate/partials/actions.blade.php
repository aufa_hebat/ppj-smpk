@component('components.actions')
	@slot('append_action')
		<a class="dropdown-item certificate-action-btn"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			@icon('fe fe-file') {{ __('Sijil Kelayakan') }}
		</a>
		<a class="dropdown-item owner-action-btn"
			data-{{ $primary_key or 'hashslug' }}="' + data.{{ $primary_key or 'hashslug' }} + '">
			@icon('fe fe-user') {{ __('Pemilik Syarikat') }}
		</a>
	@endslot
@endcomponent