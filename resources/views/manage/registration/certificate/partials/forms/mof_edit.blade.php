<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (MOF)'),
		    'id' => 'mof_no',
		    'name' => 'mof_no',
        'style' => 'text-transform:uppercase', 
        'maxlength' => 20 
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'mof_start_date',
		    'name' => 'mof_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'mof_end_date',
		    'name' => 'mof_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

@include('components.forms.switch', [
    'id' => 'mof_company_address', 
    'name' => 'mof_company_address', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan alamat yang sama dengan alamat SSM.'
])

@include('components.forms.input', [
    'input_label' => 'Alamat',
  'name' => 'mof_primary',
  'id' => 'mof_primary',
    'placeholder' => 'Alamat (Baris Pertama)',
])
<div style="margin-top: -10px;">
    @include('components.forms.input', [
        'input_label' => '',
    'name' => 'mof_secondary',
    'id' => 'mof_secondary',
        'placeholder' => 'Alamat (Baris Kedua)',
    ])
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'mof_postcode',
      'input_classes' => 'allownumericonly', 
      'maxlength' => 5,
			'id' => 'mof_postcode'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'mof_state',
            'id' => 'mof_state',
        ])
	</div>
</div>

<div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
    <div class="mb-0 card card-primary">
      	<div class="card-header" role="tab" id="headingOne2">
            <h4 class="card-title">
              	<a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2m" aria-expanded="false" aria-controls="collapseOne2m">
                    BIDANG
              	</a>
            </h4>
      	</div>
        <div id="collapseOne2m" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
          	<div class="card-body">
              <!-- <body onload="hideShowAllBidang()"> -->
            	<div id="tablebidang">
                	<div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                	<table class="table" width="100%" cellspacing="0">
                  		<thead>
                    		<tr>
                      			<td width="5%"></td>
                      			<td>BIDANG</td>
                    		</tr>
                  		</thead>
                  		<tbody class="list">
                  		@foreach($kategori_mof as $bil => $row2)
                    		<tr>
                      			<td>
                        			<div class="form-group row justify-content-end row" style="margin-top: 0;">
                          				<div class="col-lg-10">
                            				<div class="checkbox">
                              					<label class="custom-control custom-checkbox">
                                				<input type="checkbox" id="Bidang{{$row2->code}}" name="Bidang[]" class="custom-control-input" value="{{$row2->code}}"
                              					@if(!empty($company->syarikatMOFBidang))
                                    			@foreach($company->syarikatMOFBidang as $bidang)
                                      			@if ($row2->code == old('Bidang[]',$bidang->areas))
                                        		checked="checked"
                                      			@endif
                                    			@endforeach
                                  				@endif/>
                              					<span class="custom-control-label"></span></label>
                            				</div>
                          				</div>
                        			</div>
                      			</td>
                      			<td class="bidang">{{$row2->name}}</td>
                    		</tr>
                  		@endforeach
	                  	</tbody>
	                </table>
	          	</div>
            <!-- </body> -->
	        </div>
	  	</div>
  	</div>

    @foreach(mof_codes('category') as $category) 
      <div class="bidang{{$category->code}}">
        <div class="mb-0 card card-primary">
          <div class="card-header" role="tab" id="heading3">
            <h4 class="card-title">
              <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse{{$category->code}}" aria-expanded="false" aria-controls="collapse{{$category->code}}">
                {{$category->name}}
              </a>
            </h4>
          </div>
          <div id="collapse{{$category->code}}" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
            <div class="card-body">
              <!--Kategori B-->
              <div id="tablebidang{{$category->code}}">
                <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                <table class="table"  width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <td width="5%"></td>
                      <td>PENGKHUSUSAN</td>
                    </tr>
                  </thead>
                  <tbody class="list">
                    @foreach(mof_codes('khusus') as $khusus) 
                      @if($khusus->category == $category->code)
                      <tr>
                        <td>
                          <div class="form-group row justify-content-end row" style="margin-top: 0;">
                            <div class="col-lg-10">
                              <div class="checkbox">
                                <label class="custom-control custom-checkbox">
                                <input type="checkbox" id="Pengkhususan" name="Pengkhususan{{$category->code}}[]" class="custom-control-input" value="{{$khusus->code}}"
                                  @foreach($company->syarikatMOFBidang->where('areas','=',$category->code) as $khususes)
                                    @foreach($khususes->syarikatMOFKhusus as $khu)
                                      @if ($khusus->code == old('Pengkhususan{{$khusus->code}}[]',$khu->khusus))
                                        checked="checked"
                                      @endif
                                    @endforeach
                                  @endforeach
                                >
                                  <span class="custom-control-label"></span></label>
                              </div>
                            </div>
                          </div>
                        </td>
                        <td class="bidang{{$category->code}}">{{$khusus->name}}</td>
                      </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    {{-- <div class="bidang010000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse30" aria-expanded="false" aria-controls="collapse30">
                    PENGKHUSUSAN PENERBITAN
                  </a>
            </h4>
            </div>
            <div id="collapse30" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang10000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db2->where('code', 'MOF_Khusus_10000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan10000[]" class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','10000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan10000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang10000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang020000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse31" aria-expanded="false" aria-controls="collapse31">
                    PENGKHUSUSAN PERABOT, PERALATAN PEJABAT, HIASAN DALAMAN DAN DOMESTIK
                  </a>
            </h4>
            </div>
            <div id="collapse31" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang20000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db3->where('code', 'MOF_Khusus_20000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan20000[]" class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','20000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan20000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang20000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang030000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse32" aria-expanded="false" aria-controls="collapse32">
                    PENGKHUSUSAN SUKAN, REKREASI DAN ALAT MUZIK
                  </a>
            </h4>
            </div>
            <div id="collapse32" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang30000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db4->where('code', 'MOF_Khusus_30000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan30000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','30000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan30000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang30000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang040000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse33" aria-expanded="false" aria-controls="collapse33">
                    PENGKHUSUSAN MAKANAN,MINUMAN DAN BAHAN MENTAH
                  </a>
            </h4>
            </div>
            <div id="collapse33" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang40000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db5->where('code', 'MOF_Khusus_40000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan40000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','40000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan40000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang40000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang050000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse34" aria-expanded="false" aria-controls="collapse34">
                    PENGKHUSUSAN PERALATAN HOSPITAL,PERUBATAN UBAT-UBATAN DAN FARMASEUTIKAL
                  </a>
            </h4>
            </div>
            <div id="collapse34" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang50000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db6->where('code', 'MOF_Khusus_50000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan50000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','50000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan50000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang50000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang060000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse35" aria-expanded="false" aria-controls="collapse35">
                    PENGKHUSUSAN KIMIA,BAHAN KIMIA DAN PERALATAN MAKMAL
                  </a>
            </h4>
            </div>
            <div id="collapse35" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang60000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db7->where('code', 'MOF_Khusus_60000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan60000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','60000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan60000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang60000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang070000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse36" aria-expanded="false" aria-controls="collapse36">
                    PENGKHUSUSAN PERTANIAN,PERHUTANAN DAN TERNAKAN
                  </a>
            </h4>
            </div>
            <div id="collapse36" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang70000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db8->where('code', 'MOF_Khusus_70000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan70000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','70000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan70000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang70000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang080000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse37" aria-expanded="false" aria-controls="collapse37">
                    PENGKHUSUSAN KEJURUTERAAN AWAM, BINAAN DAN KELENGKAPAN KEMUDAHAN AWAM
                  </a>
            </h4>
            </div>
            <div id="collapse37" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang80000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db9->where('code', 'MOF_Khusus_80000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan80000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','80000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan80000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang80000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang090000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse38" aria-expanded="false" aria-controls="collapse38">
                    PENGKHUSUSAN BAHAN BINAAN DAN PERALATAN KESELAMATAN JALAN RAYA
                  </a>
            </h4>
            </div>
            <div id="collapse38" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang90000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db10->where('code', 'MOF_Khusus_90000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan90000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','90000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan90000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang90000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang100000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse39" aria-expanded="false" aria-controls="collapse39">
                    PENGKHUSUSAN PERALATAN SUKATAN DAN UKURAN
                  </a>
            </h4>
            </div>
            <div id="collapse39" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang100000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db11->where('code', 'MOF_Khusus_100000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan100000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','100000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan100000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang100000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang210000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse40" aria-expanded="false" aria-controls="collapse40">
                    PENGKHUSUSAN ICT
                  </a>
            </h4>
            </div>
            <div id="collapse40" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang210000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db12->where('code', 'MOF_Khusus_210000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan210000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','210000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan210000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang210000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="bidang220000">
      <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                  <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse41" aria-expanded="false" aria-controls="collapse41">
                    PENGKHUSUSAN PERKHIDMATAN
                  </a>
            </h4>
            </div>
            <div id="collapse41" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                  <div id="tablebidang220000">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <td width="5%"></td>
                          <td>PENGKHUSUSAN</td>
                        </tr>
                    </thead>
                    <tbody class="list">
                  @foreach($db13->where('code', 'MOF_Khusus_220000') as $bil => $row2)
                        <tr>
                          <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                  <div class="col-lg-10">
                                      <div class="checkbox">
                                        <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="Pengkhususan" name="Pengkhususan220000[]"  class="custom-control-input" value="{{$row2->code_short}}"
                                      @foreach($company->syarikatMOFBidang->where('areas','=','220000') as $khusus)
                                            @foreach($khusus->syarikatMOFKhusus as $khu)
                                            @if ($row2->code_short == old('Pengkhususan220000[]',$khu->khusus))
                                              checked="checked"
                                            @endif
                                            @endforeach
                                          @endforeach
                                          >
                                        <span class="custom-control-label"></span></label>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td class="bidang220000">{{$row2->code_short}}-{{$row2->description}}</td>
                          </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div> --}}
</div><br>

<div class="row">
    <div class="col-12">
        <label>Sijil MOF</label><br>
        <div class="input-group">
            <input id="subFile_mof" type="text"  class="form-control" name="mof_cert" value="@foreach($mof_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_mof"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_mof" name="mof" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($mof_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowmof{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>

                <label class="input-group-text" for="deleteFileMOF">
                    <input type="input" id="deleteFileMOF" class="buang-upload-mof" value="{{ $db->hashslug }}" hidden>
                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                </label>
              @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div><br>