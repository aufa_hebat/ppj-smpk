@include('components.forms.switch', [
    'id' => 'spkks', 
    'name' => 'spkks', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan nombor sijil yang sama dengan nombor sijil CIDB.'
])

<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (SPKK)'),
		    'id' => 'spkk_no',
		    'name' => 'spkk_no',
            'style' => 'text-transform:uppercase', 
            'maxlength' => 20 
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'spkk_start_date',
		    'name' => 'spkk_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'spkk_end_date',
		    'name' => 'spkk_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
    <div class="col-12">
        <label>Sijil SPKK</label><br>
        <div class="input-group">
            <input id="subFile_spkk" type="text"  class="form-control" name="spkk_cert" value="@foreach($spkk_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_spkk"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_spkk" name="spkk" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($spkk_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowspkk{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>

                <label class="input-group-text" for="deleteFileSPKK">
                    <input type="input" id="deleteFileSPKK" class="buang-upload-spkk" value="{{ $db->hashslug }}" hidden>
                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                </label>
              @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div><br>