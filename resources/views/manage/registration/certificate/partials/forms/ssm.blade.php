<div class="table-responsive">
    <table class="table" width="100%" cellspacing="0">
    	<thead>
			<tr>
				<th>No.Rujukan Pendaftaran (SSM)</th>
				<th>Tarikh Sah Sijil</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$company->ssm_no}}</td>
				<td>@if(!empty($company->ssm_start_date) && !empty($company->ssm_end_date)){{date('d/m/Y', strtotime($company->ssm_start_date))}} hingga {{date('d/m/Y', strtotime($company->ssm_end_date))}}@endif</td>
			</tr>
		</tbody>
	</table>
</div>