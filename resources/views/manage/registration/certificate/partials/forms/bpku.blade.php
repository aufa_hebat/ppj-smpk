<div class="table-responsive">
    <table class="table" width="100%" cellspacing="0">
    	<thead>
			<tr>
				<th>No.Rujukan Pendaftaran (BPKU)</th>
				<th>Tarikh Sah Sijil</th>
				<th>Gred</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$company->bpku_no}}</td>
				<td>@if(!empty($company->bpku_start_date) && !empty($company->bpku_end_date)){{date('d/m/Y', strtotime($company->bpku_start_date))}} hingga {{date('d/m/Y', strtotime($company->bpku_end_date))}}@endif</td>
				<td>
					@if(!empty($company->syarikatCIDBgreds)) 
                      	@foreach($company->syarikatCIDBgreds as $gred)
	                        {{$gred->grades}} 
                      	@endforeach 
                    @endif
				</td>
			</tr>
		</tbody>
	</table>
</div>