<div class="table-responsive">
    <table class="table" width="100%" cellspacing="0">
    	<thead>
			<tr>
				<th>No.Rujukan Pendaftaran (CIDB)</th>
				<th>Tarikh Sah Sijil</th>
				<th>Gred</th>
              	<th>Kategori</th>
              	<th>Pengkhususan Berdaftar</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$company->cidb_no}}</td>
				<td>@if(!empty($company->cidb_start_date) && !empty($company->cidb_end_date)){{date('d/m/Y', strtotime($company->cidb_start_date))}} hingga {{date('d/m/Y', strtotime($company->cidb_end_date))}}@endif</td>
				<td>
					@if(!empty($company->syarikatCIDBgreds)) 
                      	@foreach($company->syarikatCIDBgreds as $gred)
	                        {{$gred->grades}} 
                      	@endforeach 
                    @endif
				</td>
				<td>
                    @if(!empty($company->syarikatCIDBkategori)) 
                      	@foreach($company->syarikatCIDBkategori as $kategori)
                            {{$kategori->categories}} 
                      	@endforeach 
                    @endif
              	</td>
              	<td>
                    @if(!empty($company->syarikatCIDBkategori)) 
                      	@foreach($company->syarikatCIDBkategori as $kategori) 
                            @if(!empty($kategori->syarikatCIDBKhusus)) 
                              	@foreach($kategori->syarikatCIDBKhusus as $khusus)
                                    {{$khusus->khusus}} 
                              	@endforeach 
                            @endif 
                      	@endforeach 
                    @endif
              	</td>
			</tr>
		</tbody>
	</table>
</div>