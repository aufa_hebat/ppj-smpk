<div class="table-responsive">
    <table class="table" width="100%" cellspacing="0">
    	<thead>
			<tr>
				<th>No.Rujukan Pendaftaran (Bumiputera)</th>
				<th>Tarikh Sah Sijil</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$company->bumiputra_no}}</td>
				<td>@if(!empty($company->bumiputra_start_date) && !empty($company->bumiputra_end_date)){{date('d/m/Y', strtotime($company->bumiputra_start_date))}} hingga {{date('d/m/Y', strtotime($company->bumiputra_end_date))}}@endif</td>
			</tr>
		</tbody>
	</table>
</div>