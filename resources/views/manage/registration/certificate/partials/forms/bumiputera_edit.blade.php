@include('components.forms.switch', [
    'id' => 'bumiputras', 
    'name' => 'bumiputras', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan nombor sijil, Tarikh Sijil Sah dan Tarikh Sijil Tamat yang sama dengan MOF.'
])

<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (Bumiputera)'),
		    'id' => 'bumiputra_no',
		    'name' => 'bumiputra_no',
            'style' => 'text-transform:uppercase', 
            'maxlength' => 20 
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'bumiputra_start_date',
		    'name' => 'bumiputra_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'bumiputra_end_date',
		    'name' => 'bumiputra_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
    <div class="col-12">
        <label>Sijil Bumiputra</label><br>
        <div class="input-group">
            <input id="subFile_bumi" type="text"  class="form-control" name="bumiputra_cert" value="@foreach($bumi_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_bumi"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_bumi" name="bumiputra" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($bumi_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowbumi{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>

                <label class="input-group-text" for="deleteFileBumiputra">
                    <input type="input" id="deleteFileBumiputra" class="buang-upload-bumi" value="{{ $db->hashslug }}" hidden>
                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                </label>
              @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div><br>