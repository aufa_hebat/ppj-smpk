@component('components.modals.base', [
	'id' => 'company-modal',
	'tooltip' => __('Syarikat'),
	'modal_title' => __('Syarikat'),
	])
	@slot('modal_body')
		<div class="row">
			<div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		    <div class="col-md-12 col-lg-12 col-xl-12">
				@include('manage.company.partials.forms.create')
		    </div>
		    <div class="col-md-3 col-lg-3 col-xl-3">
		      
		    </div>
		</div>
	@endslot
	@slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right">
			@icon('fe fe-save') {{ __('Simpan') }}
		</button>
	@endslot
</form>
@endcomponent
