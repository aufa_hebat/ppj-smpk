@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#company_id').val('{{ $owner->id }}');
        });
    </script>
@endpush
<form method="POST" action="{{ route('manage.owners.store') }}">
	@csrf
<div class="row">
	<div class="col-6">
		<input type="text" name="hashslug" id="hashslug" value="{{$hashslug}}" hidden/>
		@include('components.forms.hidden', [
			'id' => 'id',
			'name' => 'id',
			'value' => ''
		])

		@include('components.forms.hidden', [
			'id' => 'company_id',
			'name' => 'company_id',
			'value' => ''
		])

		@include('components.forms.input', [
			'input_label' => 'Nama Pemilik',
			'name' => 'name'
		])
	</div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Kad Pengenalan',
			'name' => 'ic_number',
			'type' => 'number',
            'input_classes' => 'allownumericonly',
		])
	</div>
	
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'E-Mel',
			'name' => 'email'
		])
	</div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'No. Telefon',
			'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
			'type' => 'number'
		])
	</div>
</div>

@include('components.forms.input', [
	'input_label' => 'Alamat (Baris Pertama)',
	'name' => 'primary'
])

@include('components.forms.input', [
	'input_label' => 'Alamat (Baris Kedua)',
	'name' => 'secondary'
])

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'postcode',
			'type' => 'number'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
			'input_label' => 'Negeri',
			'name' => 'state',
			'options' => $std_cd_state->pluck('description','description'),
		])
	</div>
</div>

<div class="form-group row" style="margin-top: 20px;margin-left: 0px;">
  	<label class="control-label" for="inputLarge" style="font-size: 15px;">Pemilik Sijil Kelayakan</label>
	<div class="col-lg-10">
		<div class="checkbox form-control">
			<label>
			<input type="checkbox" value="1" name="cidb_cert"> CIDB
			</label>
			<label>
			<input type="checkbox" value="1" name="mof_cert"> MOF
			</label>
			<label>
			<input type="checkbox" value="1" name="kdn_cert"> KDN
			</label>
		</div>
	</div>
</div>