@extends('layouts.app')

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#hashslug').val('{{ $owner->hashslug }}');
            $('#name').val('{{ $owner->name }}');
            $('#ic_number').val('{{ $owner->ic_number }}');
            $('#email').val('{{ $owner->email }}');
            @if(!empty($owner->addresses[0]))
            $('#primary').val('{{ str_replace(["\r\n","\r","\n"],"\\n ",$owner->addresses[0]->primary) }}');
            $('#secondary').val('{{ $owner->addresses[0]->secondary }}');
            $('#postcode').val('{{ $owner->addresses[0]->postcode }}');
            $('#state').val('{{ $owner->addresses[0]->state }}');
            @endif
            @if(!empty($owner->phones[0]))
            $('#phone_number').val('{{ $owner->phones[0]->phone_number }}');
            @endif

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Pemilik Syarikat') !!}', response.data.message, 'success');
                });
            });
        });
    </script>
@endpush

@section('content')
<form method="POST" action="{{ route('registration.owners.update',$owner->id) }}">
    @csrf
    @method('PUT')
	<div class="row">
        @component('components.card', ['card_classes' => 'col-12'])
            @slot('card_body')
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'id' => 'hashslug',
                            'name' => 'hashslug',
                            'value' => ''
                        ])
                        @include('components.forms.input', [
                            'input_label' => 'Nama Pemilik',
                            'id' => 'name',
                            'name' => 'name',
                            'required' => 'true'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kad Pengenalan',
                            'id' => 'ic_number',
                            'name' => 'ic_number',
                            'input_classes' => 'allownumericonly',
                            'required' => 'true'
                        ])
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'E-Mel',
                            'id' => 'email',
                            'name' => 'email'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'No. Telefon',
                            'id' => 'phone_number',
                            'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
                            'input_classes' => 'allownumericonly',
                            'required' => 'true'
                        ])
                    </div>
                </div>

                @include('components.forms.input', [
                    'input_label' => 'Alamat',
                    'name' => 'primary',
                    'id' => 'primary',
                    'placeholder' => 'Alamat (Baris Pertama)',
                    'required' => 'true'
                ])
                <div style="margin-top: -10px;">
                    @include('components.forms.input', [
                        'input_label' => '',
                        'name' => 'secondary',
                        'id' => 'secondary',
                        'placeholder' => 'Alamat (Baris Kedua)'
                    ])
                </div>

                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Poskod',
                            'id' => 'postcode',
                            'name' => 'postcode',
                            'input_classes' => 'allownumericonly',
                            'maxlength' => 5,
                            'required' => 'true'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.select', [
                            'input_label' => 'Negeri',
                            'id' => 'state',
                            'name' => 'state',
                            'options' => $std_cd_state->pluck('description','description'),
                            'required' => 'true'
                        ])
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 20px;margin-left: 0px;">
                    <label class="control-label" for="inputLarge" style="font-size: 15px;">Pemilik Sijil Pendaftaran bagi:</label>
                        <div class="col-lg-10">
                            <div class="checkbox">
                                <label class="custom-control custom-checkbox">
                                    <input type="hidden" name="cidb_cert" value="0">
                                    <input type="checkbox" value="1" name="cidb_cert" class="custom-control-input"
                                        @if(!empty($owner->cidb_cert))
                                            checked="checked"
                                        @endif> <span class="custom-control-label">CIDB</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="hidden" name="mof_cert" value="0">
                                    <input type="checkbox" value="1" name="mof_cert" class="custom-control-input"    
                                        @if(!empty($owner->mof_cert))
                                            checked="checked"
                                        @endif> <span class="custom-control-label">MOF</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="hidden" name="kdn_cert" value="0">
                                    <input type="checkbox" value="1" name="kdn_cert" class="custom-control-input"
                                    @if(!empty($owner->kdn_cert))
                                        checked="checked"
                                    @endif> <span class="custom-control-label">KDN</span>
                                </label>
                            </div>
                        </div>
                    </div>
                <div class="pull-right btn-group">
                    <a href="{{ route('registration.companies.edit',['company_id' => $owner->company->hashslug]) }}"
                       class="btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>
                    <button type="submit" class="btn btn-primary float-right">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
        @endslot
	@endcomponent
</div>
</form>
@endsection