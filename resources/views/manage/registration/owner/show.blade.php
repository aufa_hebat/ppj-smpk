@extends('layouts.app')

@section('content')
<div class="row card">
    <div class="col-12">
        @component('components.card')
            @slot('card_body')
<h4>Maklumat Pemilik Syarikat</h4>
<br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Nama Pemilik</div>
        <p> {!! $owner->name !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">No. Kad Pengenalan</div>
        <p>{!! $owner->ic_number !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">E-Mel</div>
        <p>{!! $owner->email !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">No. Telefon</div>
        <p>{!! $owner->phones[0]->phone_number !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($owner->addresses[0]->primary))
            <p>
                {!! $owner->addresses[0]->primary !!}<br>{!! $owner->addresses[0]->secondary !!}<br>{!! $owner->addresses[0]->postcode !!}, {!! $owner->addresses[0]->state !!}, Malaysia
            </p>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Pemilik Sijil Pendaftaran bagi:</div>
        @if($owner->cidb_cert == 1)
            <div>CIDB</div>
        @endif
        @if($owner->mof_cert == 1)
            <div>MOF</div>
        @endif
        @if($owner->kdn_cert == 1)
            <div>KDN</div>
        @endif
        @if(($owner->cidb_cert == NULL) && ($owner->mof_cert == NULL) && ($owner->kdn_cert == NULL))
            <div>Tiada</div>
        @endif
    </div>
</div>
<div class="modal-footer">
    <a href="{{ route('PendaftaranSyarikat.edit',$owner->company->hashslug) }}" class="btn btn-default border-primary">
        {{ __('Kembali') }}
    </a>
</div>

@endslot
        @endcomponent
    </div>
</div>
@endsection