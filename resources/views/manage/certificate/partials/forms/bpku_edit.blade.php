@include('components.forms.switch', [
    'id' => 'bpkus', 
    'name' => 'bpkus', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan nombor sijil yang sama dengan nombor sijil CIDB.'
])

<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (BPKU)'),
		    'id' => 'bpku_no',
		    'name' => 'bpku_no',
            'style' => 'text-transform:uppercase', 
            'maxlength' => 20 
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Sah'),
		    'id' => 'bpku_start_date',
		    'name' => 'bpku_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Sijil Tamat'),
		    'id' => 'bpku_end_date',
		    'name' => 'bpku_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

<div class="row">
    <div class="col-12">
        <label>Sijil BPKU</label><br>
        <div class="input-group">
            <input id="subFile_bpku" type="text"  class="form-control" name="bpku_cert" value="@foreach($bpku_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_bpku"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_bpku" name="bpku" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($bpku_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowbpku{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>

                <label class="input-group-text" for="deleteFileBPKU">
                    <input type="input" id="deleteFileBPKU" class="buang-upload-bpku" value="{{ $db->hashslug }}" hidden>
                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                </label>
              @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div><br>

@if(!empty(user()) && user()->current_role_login == 'urusetia')
	<div class="float-right">
		@include('components.forms.switch', [
		    'id' => 'bpku_blacklist', 
		    'name' => 'bpku_blacklist', 
		    'label' => 'Senarai Hitam',
		    'checked' => old('bpku_blacklist', (!empty($company->bpku_blacklist))? $company->bpku_blacklist: false)
		])
	</div>
	<div class="bpkublacklist">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Tamat Senarai Hitam'),
		    'id' => 'bpku_blacklist_end_date',
		    'name' => 'bpku_blacklist_end_date',
		    'config' => [
		        'format' => config('datetime.display.date'),
		    ]
		])
	</div>
@endif