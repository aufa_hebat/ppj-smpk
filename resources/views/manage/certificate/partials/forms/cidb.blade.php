<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (CIDB)</div>
        <p style="text-transform: uppercase;">{!! $company->cidb_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Berkuatkuasa</div>
        <p>{!! !empty($company->cidb_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_start_date)->format('d/m/Y') : null !!} @if(!empty($company->cidb_start_date)) hingga @endif {!! !empty($company->cidb_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($cidb1->addresses[0]->primary))
          <p>
              {!! !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->primary : null !!}<br>{!! !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->secondary : null !!}<br>{!! !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->postcode : null !!}, {!! !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->state : null !!}, Malaysia
          </p>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-4">
        <div class="text-muted">Gred</div>
        <p>
          @if(!empty($company->syarikatCIDBgreds)) 
            @foreach($company->syarikatCIDBgreds as $gred)
              {{$gred->grades}} 
            @endforeach 
          @endif
        </p>
    </div>
    <div class="col-4">
        <div class="text-muted">Kategori</div>
        <p>
          @if(!empty($company->syarikatCIDBkategori)) 
            @foreach($company->syarikatCIDBkategori as $kategori)
              {{$kategori->categories}} 
            @endforeach 
          @endif
        </p>
    </div>
    <div class="col-4">
        <div class="text-muted">Pengkhususan Berdaftar</div>
        <p>
          @if(!empty($company->syarikatCIDBkategori)) 
            @foreach($company->syarikatCIDBkategori as $kategori) 
              @if(!empty($kategori->syarikatCIDBKhusus)) 
                @foreach($kategori->syarikatCIDBKhusus as $khusus)
                  {{$khusus->khusus}} 
                @endforeach 
              @endif 
            @endforeach 
          @endif
        </p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil CIDB</div>
        <div class="input-group">
            <input id="subFile_cidb" type="text"  class="form-control" name="cidb_cert" value="@foreach($cidb_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($cidb_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowcidb{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>
              @endif
            @endforeach
        </div>
    </div>
</div>

@if(!empty($company->cidb_blacklist_end_date))
  <div class="row">
    <div class="col-12">
      <div class="text-muted">Tarikh Disenarai Hitam</div>
      <p>{!! !empty($company->cidb_blacklist_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_blacklist_start_date)->format('d/m/Y') : null !!} hingga {!! !empty($company->cidb_blacklist_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_blacklist_end_date)->format('d/m/Y') : null !!}</p>
    </div>
  </div>
@endif
<br><br>