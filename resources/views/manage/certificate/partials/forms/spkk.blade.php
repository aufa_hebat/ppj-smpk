<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (SPKK)</div>
        <p style="text-transform: uppercase;">{!! $company->spkk_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Sijil Sah</div>
        <p>{!! !empty($company->spkk_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_start_date)->format('d/m/Y') : null !!}  @if(!empty($company->spkk_start_date)) hingga @endif {!! !empty($company->spkk_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil SPKK</div>
        <div class="input-group">
            <input id="subFile_spkk" type="text"  class="form-control" name="spkk_cert" value="@foreach($spkk_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($spkk_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowspkk{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>
              @endif
            @endforeach
        </div>
    </div>
</div>

@if(!empty($company->spkk_blacklist_end_date))
  <div class="row">
    <div class="col-12">
      <div class="text-muted">Tarikh Disenarai Hitam</div>
      <p>{!! !empty($company->spkk_blacklist_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_blacklist_start_date)->format('d/m/Y') : null !!} hingga {!! !empty($company->spkk_blacklist_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_blacklist_end_date)->format('d/m/Y') : null !!}</p>
    </div>
  </div>
@endif
<br><br>