<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (Bumiputra)</div>
        <p style="text-transform: uppercase;">{!! $company->bumiputra_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Sijil Sah</div>
        <p>{!! !empty($company->bumiputra_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_start_date)->format('d/m/Y') : null !!} @if(!empty($company->bumiputra_start_date)) hingga @endif {!! !empty($company->bumiputra_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil Bumiputra</div>
        <div class="input-group">
            <input id="subFile_bumi" type="text"  class="form-control" name="bumi_cert" value="@foreach($bumi_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($bumi_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowbumi{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>
              @endif
            @endforeach
        </div>
    </div>
</div>
<br><br>