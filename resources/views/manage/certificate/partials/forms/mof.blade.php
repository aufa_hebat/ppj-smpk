<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (MOF)</div>
        <p style="text-transform: uppercase;">{!! $company->mof_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Sijil Sah</div>
        <p>{!! !empty($company->mof_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_start_date)->format('d/m/Y') : null !!} @if(!empty($company->mof_start_date)) hingga @endif {!! !empty($company->mof_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($mof1->addresses[0]->primary))
          <p>
              {!! !empty($mof1->addresses[0]) ? $mof1->addresses[0]->primary : null !!}<br>{!! !empty($mof1->addresses[0]) ? $mof1->addresses[0]->secondary : null !!}<br>{!! !empty($mof1->addresses[0]) ? $mof1->addresses[0]->postcode : null !!}, {!! !empty($mof1->addresses[0]) ? $mof1->addresses[0]->state : null !!}, Malaysia
          </p>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="text-muted">Bidang</div>
        <p>
          	@if(!empty($company->syarikatMOFBidang)) 
              	@foreach($company->syarikatMOFBidang as $bidang)
                    {{$bidang->areas}} 
              	@endforeach 
            @endif
        </p>
    </div>
    <div class="col-6">
        <div class="text-muted">Pengkhususan Berdaftar</div>
        <p>
          	@if(!empty($company->syarikatMOFBidang)) 
              	@foreach($company->syarikatMOFBidang as $kategori) 
                    @if(!empty($kategori->syarikatMOFKhusus)) 
                      	@foreach($kategori->syarikatMOFKhusus as $khusus)
                            {{$khusus->khusus}} 
                      	@endforeach 
                    @endif 
              	@endforeach 
            @endif
        </p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil MOF</div>
        <div class="input-group">
            <input id="subFile_mof" type="text"  class="form-control" name="mof_cert" value="@foreach($mof_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($mof_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowmof{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>
              @endif
            @endforeach
        </div>
    </div>
</div>

@if(!empty($company->mof_blacklist_end_date))
  <div class="row">
    <div class="col-12">
      <div class="text-muted">Tarikh Disenarai Hitam</div>
      <p>{!! !empty($company->mof_blacklist_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_blacklist_start_date)->format('d/m/Y') : null !!} hingga {!! !empty($company->mof_blacklist_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_blacklist_end_date)->format('d/m/Y') : null !!}</p>
    </div>
  </div>
@endif
<br><br>