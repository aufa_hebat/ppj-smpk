<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (SSM)</div>
        <p style="text-transform: uppercase;">{!! $company->ssm_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Pendaftaran</div>
        <p>{!! !empty($company->ssm_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->ssm_start_date)->format('d/m/Y') : null !!} @if(!empty($company->ssm_start_date)) hingga @endif {!! !empty($company->ssm_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->ssm_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($ssm1->addresses[0]->primary))
            <p>
                {!! !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->primary : null !!}<br>{!! !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->secondary : null !!}<br>{!! !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->postcode : null !!}, {!! !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->state : null !!}, Malaysia
            </p>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil SSM</div>
        <div class="input-group">
            <input id="subFile_ssm" type="text"  class="form-control" name="ssm_cert" value="@foreach($ssm_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($ssm_doc as $db)
	            @if(!empty($db))
		            <label class="input-group-text" for="downloadFile">
		                <a class="newwindowssm{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
		            </label>
	            @endif
            @endforeach
        </div>
    </div>
</div>