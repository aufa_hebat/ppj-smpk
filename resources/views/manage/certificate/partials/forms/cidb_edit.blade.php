<div class="row">
  <div class="col-12">
    @include('components.forms.input', [
        'input_label' => __('No Rujukan Pendaftaran (CIDB)'),
        'id' => 'cidb_no',
        'name' => 'cidb_no',
        'style' => 'text-transform:uppercase', 
        'maxlength' => 20 
    ])
  </div>
</div>

<div class="row">
  <div class="col-6">
    @include('components.forms.datetimepicker', [
        'input_label' => __('Tarikh Mula Berkuatkuasa'),
        'id' => 'cidb_start_date',
        'name' => 'cidb_start_date',
        'config' => [
            'format' => config('datetime.display.date'),
        ]
    ])
  </div>
  <div class="col-6">
    @include('components.forms.datetimepicker', [
        'input_label' => __('Tarikh Habis Tempoh Perakuan'),
        'id' => 'cidb_end_date',
        'name' => 'cidb_end_date',
        'config' => [
            'format' => config('datetime.display.date'),
        ]
    ])
  </div>
</div>

@include('components.forms.switch', [
    'id' => 'cidb_company_address', 
    'name' => 'cidb_company_address', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan alamat yang sama dengan alamat SSM.'
])

@include('components.forms.input', [
    'input_label' => 'Alamat',
  'name' => 'cidb_primary',
  'id' => 'cidb_primary',
    'placeholder' => 'Alamat (Baris Pertama)',
])
<div style="margin-top: -10px;">
    @include('components.forms.input', [
        'input_label' => '',
    'name' => 'cidb_secondary',
    'id' => 'cidb_secondary',
        'placeholder' => 'Alamat (Baris Kedua)',
    ])
</div>

<div class="row">
  <div class="col-6">
    @include('components.forms.input', [
      'input_label' => 'Poskod',
      'name' => 'cidb_postcode',
      'input_classes' => 'allownumericonly', 
      'maxlength' => 5,
      'id' => 'cidb_postcode'
    ])
  </div>
  <div class="col-6">
    @include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'cidb_state',
            'id' => 'cidb_state',
        ])
  </div>
</div>

<div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
    <div class="mb-0 card card-primary">
        <div class="card-header" role="tab" id="headingOne2">
            <h4 class="card-title">
                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                  GRED
                </a>
            </h4>
        </div>
        <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
            <div class="card-body">
                <div id="tablegred">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                <table class="table" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                          <td width="5%"></td>
                          <td>GRED</td>
                      </tr>
                    </thead>
                    <tbody class="list">
                    @foreach($gred as $bil => $row2)
                      <tr>
                          <td>
                            <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                <div class="col-lg-10">
                                      <label class="custom-control custom-checkbox">
                                      <input type="checkbox" id="Gred{{$bil}}" name="Gred[]" class="custom-control-input" value="{{$row2->code}}"
                                        @if(!empty($company->syarikatCIDBGreds)){
                                        @foreach($company->syarikatCIDBGreds as $gred)
                                          @if ($row2->code == old('Gred[]',$gred->grades))
                                          checked="checked"
                                          @endif
                                        @endforeach
                                        @endif
                                      /><span class="custom-control-label"></span>
                                      </label>
                                </div>
                            </div>
                          </td>
                          <td class="gred">{{$row2->code}}</td>
                      </tr>
                  @endforeach
                    </tbody>
                </table>
              </div>
          </div>
        </div>

        <div class="mb-0 card card-primary">
          <div class="card-header" role="tab" id="headingTwo2">
              <h4 class="card-title">
                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                    KATEGORI
                </a>
              </h4>
          </div>
          <div id="collapseTwo2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
              <div class="card-body">
                <body onload="hideShowAllKhusus()">
                    <div id="tablekat">
                    <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                            <td width="5%"></td>
                            <td>KATEGORI</td>
                        </tr>
                      </thead>
                        <tbody class="list">
                      @foreach($kategori as $bil => $row2)
                          <tr>
                            <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                    <div class="col-lg-10">
                                        <div class="checkbox">
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" id="Kategori{{$bil}}" name="Kategori[]" class="custom-control-input" data-kategori="{{$row2->code}}" value="{{$row2->code}}"
                                                @foreach($company->syarikatCIDBKategori as $kat)
                                              @if ($row2->code == old('Kategori[]',$kat->categories))
                                                  checked="checked"
                                                @endif
                                                @endforeach
                                              ><span class="custom-control-label"></span>
                                            </label>
                                    </div>
                                  </div>
                              </div>
                            </td>
                            <td class="kat">{{$row2->name}}</td>
                        </tr>
                      @endforeach
                      </tbody>
                </table>
                </div>
              </body>
            </div>
        </div>
      </div>

      @foreach(cidb_codes('category') as $category)
        <div id="Kategori{{$category->code}}">
          <div class="mb-0 card card-primary">
            <div class="card-header" role="tab" id="heading3">
              <h4 class="card-title">
                <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse{{$category->code}}" aria-expanded="false" aria-controls="collapse3">
                  PENGKHUSUSAN {{$category->code}}
                </a>
              </h4>
            </div>
            <div id="collapse{{$category->code}}" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
              <div class="card-body">
                <!--Kategori B-->
                <div id="tablekus{{$category->code}}">
                  <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                  <table class="table"  width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <td width="5%"></td>
                        <td>PENGKHUSUSAN</td>
                      </tr>
                    </thead>
                    <tbody class="list">
                      @foreach(cidb_codes('khusus') as $khusus) 
                        @if($khusus->category == $category->code)
                          <tr>
                            <td>
                              <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                <div class="col-lg-10">
                                  <div class="checkbox">
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" id="Pengkhususan" name="Pengkhususan{{$category->code}}[]"class="custom-control-input" value="{{$khusus->code}}"
                                      @foreach($company->syarikatCIDBKategori->where('categories','=',$category->code) as $khususes)
                                        @foreach($khususes->syarikatCIDBKhusus as $khu)
                                          @if ($khusus->code == old('Pengkhususan{{$category->code}}[]',$khu->khusus))
                                            checked="checked"
                                          @endif
                                        @endforeach
                                      @endforeach
                                    ><span class="custom-control-label"></span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td class="kus{{$category->code}}">{{$khusus->name}}</td>
                          </tr>
                        @endif
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach

      {{-- <div id="KategoriB">
        <div class="mb-0 card card-primary">
              <div class="card-header" role="tab" id="heading3">
                <h4 class="card-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                      PENGKHUSUSAN B
                    </a>
              </h4>
              </div>
              <div id="collapse3" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                <div class="card-body">
                  <!--Kategori B-->
                    <div id="tablekusB">
                    <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                    <table class="table"  width="100%" cellspacing="0">
                      <thead>
                          <tr>
                            <td width="5%"></td>
                            <td>PENGKHUSUSAN</td>
                          </tr>
                      </thead>
                      <tbody class="list">
                    @foreach($pengkhususan->where('category', 'B') as $bil => $row2)
                          <tr>
                            <td>
                                <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                    <div class="col-lg-10">
                                        <div class="checkbox">
                                          <label class="custom-control custom-checkbox">
                                          <input type="checkbox" id="Pengkhususan" name="PengkhususanB[]"class="custom-control-input" value="{{$row2->code}}"
                                        @foreach($company->syarikatCIDBKategori->where('categories','=','B') as $khusus)
                                              @foreach($khusus->syarikatCIDBKhusus as $khu)
                                              @if ($row2->code == old('PengkhususanB[]',$khu->khusus))
                                                checked="checked"
                                              @endif
                                              @endforeach
                                            @endforeach
                                            ><span class="custom-control-label"></span>
                                          </label>
                                        </div>
                                    </div>
                                  </div>
                              </td>
                              <td class="kusB">{{$row2->name}}</td>
                            </tr>
                      @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
          </div>
      </div>

      <div id="KategoriCE">
          <div class="mb-0 card card-primary">
              <div class="card-header" role="tab" id="heading4">
                <h4 class="card-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                      PENGKHUSUSAN CE
                    </a>
              </h4>
              </div>
              <div id="collapse4" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                <div class="card-body">
                  <!--Kategori CE-->
                    <div id="tablekusCE">
                    <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                      <table class="table"  width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <td width="5%"></td>
                              <td>PENGKHUSUSAN</td>
                            </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($pengkhususan->where('category', 'CE') as $bil => $row2)
                            <tr>
                              <td>
                                  <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                    <div class="col-lg-10">
                                        <div class="checkbox">
                                          <label class="custom-control custom-checkbox">
                                            <input type="checkbox" id="Pengkhususan" name="PengkhususanCE[]" class="custom-control-input" value="{{$row2->code}}"
                                            @foreach($company->syarikatCIDBKategori->where('categories','=','CE') as $khusus)
                                              @foreach($khusus->syarikatCIDBKhusus as $khu)
                                              @if ($row2->code == old('PengkhususanCE[]',$khu->khusus))
                                                checked="checked"
                                              @endif
                                              @endforeach
                                            @endforeach
                                          ><span class="custom-control-label"></span>
                                          </label>
                                        </div>
                                    </div>
                                  </div>
                              </td>
                              <td class="kusCE">{{$row2->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
          </div>
          </div>
      </div>

      <div id="KategoriM">
          <div class="mb-0 card card-primary">
              <div class="card-header" role="tab" id="heading5">
                <h4 class="card-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                      PENGKHUSUSAN M
                    </a>
                </h4>
              </div>
              <div id="collapse5" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                <div class="card-body">
                  <!--Kategori M-->
                    <div id="tablekusM">
                    <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                      <table class="table"  width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <td width="5%"></td>
                              <td>PENGKHUSUSAN</td>
                            </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($pengkhususan->where('category', 'M') as $bil => $row2)
                            <tr>
                              <td>
                                  <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                    <div class="col-lg-10">
                                        <div class="checkbox">
                                          <label class="custom-control custom-checkbox">
                                              <input type="checkbox" id="Pengkhususan" name="PengkhususanM[]" class="custom-control-input" value="{{$row2->code}}"
                                              @foreach($company->syarikatCIDBKategori->where('categories','=','M') as $khusus)
                                                @foreach($khusus->syarikatCIDBKhusus as $khu)
                                                @if ($row2->code == old('PengkhususanM[]',$khu->khusus))
                                                  checked="checked"
                                                @endif
                                                @endforeach
                                              @endforeach
                                              ><span class="custom-control-label"></span>
                                          </label>
                                        </div>
                                    </div>
                                  </div>
                              </td>
                              <td class="kusM">{{$row2->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
          </div>
          </div>
      </div>

      <div id="KategoriE">
          <div class="mb-0 card card-primary">
              <div class="card-header" role="tab" id="heading6">
                <h4 class="card-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                      PENGKHUSUSAN E
                    </a>
                </h4>
              </div>
              <div id="collapse6" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                <div class="card-body">
                  <!--Kategori E-->
                    <div id="tablekusE">
                    <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                    <table class="table"  width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <td width="5%"></td>
                              <td>PENGKHUSUSAN</td>
                            </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($pengkhususan->where('category', 'E') as $bil => $row2)
                            <tr>
                              <td>
                                  <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                    <div class="col-lg-10">
                                        <div class="checkbox">
                                          <label class="custom-control custom-checkbox">
                                            <input type="checkbox" id="Pengkhususan" name="PengkhususanE[]" class="custom-control-input" value="{{$row2->code}}"
                                            @foreach($company->syarikatCIDBKategori->where('categories','=','E') as $khusus)
                                              @foreach($khusus->syarikatCIDBKhusus as $khu)
                                              @if ($row2->code == old('PengkhususanE[]',$khu->khusus))
                                                checked="checked"
                                              @endif
                                              @endforeach
                                            @endforeach
                                            ><span class="custom-control-label"></span>
                                          </label>
                                        </div>
                                    </div>
                                  </div>
                              </td>
                              <td class="kusE">{{$row2->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
            </div>
          </div> 
      </div>

      <div id="KategoriF">
          <div class="mb-0 card card-primary">
              <div class="card-header" role="tab" id="heading6">
                <h4 class="card-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse7" aria-expanded="false" aria-controls="collapse6">
                      PENGKHUSUSAN F
                    </a>
                </h4>
              </div>
              <div id="collapse7" class="card-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                <div class="card-body">
                  <!--Kategori E-->
                    <div id="tablekusF">
                    <div class="pull-right"><input class="search roundedInput" placeholder="Cari..." /></div>
                    <table class="table"  width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <td width="5%"></td>
                              <td>PENGKHUSUSAN</td>
                            </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($pengkhususan->where('category', 'F') as $bil => $row2)
                            <tr>
                              <td>
                                  <div class="form-group row justify-content-end row" style="margin-top: 0;">
                                    <div class="col-lg-10">
                                        <div class="checkbox">
                                          <label class="custom-control custom-checkbox">
                                            <input type="checkbox" id="Pengkhususan" name="PengkhususanF[]" class="custom-control-input" value="{{$row2->code}}"
                                            @foreach($company->syarikatCIDBKategori->where('categories','=','F') as $khusus)
                                              @foreach($khusus->syarikatCIDBKhusus as $khu)
                                              @if ($row2->code == old('PengkhususanF[]',$khu->khusus))
                                                checked="checked"
                                              @endif
                                              @endforeach
                                            @endforeach
                                            ><span class="custom-control-label"></span>
                                          </label>
                                        </div>
                                    </div>
                                  </div>
                              </td>
                              <td class="kusF">{{$row2->name}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
            </div>
          </div> 
      </div> --}}
    </div>
</div><br>

<div class="row">
    <div class="col-12">
        <label>Sijil CIDB</label><br>
        <div class="input-group">
            <input id="subFile_cidb" type="text"  class="form-control" name="cidb_cert" value="@foreach($cidb_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_cidb"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_cidb" name="cidb" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($cidb_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowcidb{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>

                <label class="input-group-text" for="deleteFileCIDB">
                    <input type="input" id="deleteFileCIDB" class="buang-upload-cidb" value="{{ $db->hashslug }}" hidden>
                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                </label>
              @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div><br>

@if(!empty(user()) && user()->current_role_login == 'urusetia')
  <div class="float-right">
    @include('components.forms.switch', [
        'id' => 'cidb_blacklist', 
        'name' => 'cidb_blacklist', 
        'label' => 'Senarai Hitam',
        'checked' => old('cidb_blacklist', (!empty($company->cidb_blacklist))? $company->cidb_blacklist: false)
    ])
  </div>
  <div class="cidbblacklist">
    @include('components.forms.datetimepicker', [
        'input_label' => __('Tarikh Tamat Senarai Hitam'),
        'id' => 'cidb_blacklist_end_date',
        'name' => 'cidb_blacklist_end_date',
        'config' => [
            'format' => config('datetime.display.date'),
        ]
    ])
  </div>
@endif