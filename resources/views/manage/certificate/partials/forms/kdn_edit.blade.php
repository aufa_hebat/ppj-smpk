<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (KDN)'),
		    'id' => 'kdn_no',
		    'name' => 'kdn_no',
            'style' => 'text-transform:uppercase', 
            'maxlength' => 20 
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Mula Lesen'),
		    'id' => 'kdn_start_date',
		    'name' => 'kdn_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Tamat Lesen'),
		    'id' => 'kdn_end_date',
		    'name' => 'kdn_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

@include('components.forms.switch', [
    'id' => 'kdn_company_address', 
    'name' => 'kdn_company_address', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan alamat yang sama dengan alamat MOF.'
])

@include('components.forms.input', [
    'input_label' => 'Alamat',
  'name' => 'kdn_primary',
  'id' => 'kdn_primary',
    'placeholder' => 'Alamat (Baris Pertama)',
])
<div style="margin-top: -10px;">
    @include('components.forms.input', [
        'input_label' => '',
    'name' => 'kdn_secondary',
    'id' => 'kdn_secondary',
        'placeholder' => 'Alamat (Baris Kedua)',
    ])
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'kdn_postcode',
      'input_classes' => 'allownumericonly', 
      'maxlength' => 5,
			'id' => 'kdn_postcode'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'kdn_state',
            'id' => 'kdn_state',
        ])
	</div>
</div>

<div class="row">
    <div class="col-12">
        <label>Sijil KDN</label><br>
        <div class="input-group">
            <input id="subFile_kdn" type="text"  class="form-control" name="kdn_cert" value="@foreach($kdn_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_kdn"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_kdn" name="kdn" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($kdn_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowkdn{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>

                <label class="input-group-text" for="deleteFileKDN">
                    <input type="input" id="deleteFileKDN" class="buang-upload-kdn" value="{{ $db->hashslug }}" hidden>
                    <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                </label>
              @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div><br>

<div class="row">
	<div class="col-2">
		@include('components.forms.input', [
		    'input_label' => __('No Lesen'),
		    'id' => 'license_no',
		    'name' => 'license_no'
		])
	</div>
	<div class="col-4">
		@include('components.forms.select', [
            'input_label' => __('Kawasan Yang Dibenarkan'),
            'options' => $std_cd_states->pluck('description','description'),
            'name' => 'kdn_states',
            'id' => 'kdn_states',
        ])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Persatuan Perkhidmatan Kawalan Keselamatan Malaysia (PPKKM)'),
            'options' => ['Ada','Tiada'],
            'name' => 'ppkkm',
            'id' => 'ppkkm',
        ])
	</div>
</div>

@if(!empty(user()) && user()->current_role_login == 'urusetia')
	<div class="float-right">
		@include('components.forms.switch', [
		    'id' => 'kdn_blacklist', 
		    'name' => 'kdn_blacklist', 
		    'label' => 'Senarai Hitam',
		    'checked' => old('kdn_blacklist', (!empty($company->kdn_blacklist))? $company->kdn_blacklist: false)
		])
	</div>
	<div class="kdnblacklist">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Tamat Senarai Hitam'),
		    'id' => 'kdn_blacklist_end_date',
		    'name' => 'kdn_blacklist_end_date',
		    'config' => [
		        'format' => config('datetime.display.date'),
		    ]
		])
	</div>
@endif