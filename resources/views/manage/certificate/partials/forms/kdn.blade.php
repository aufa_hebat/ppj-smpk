<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (KDN)</div>
        <p style="text-transform: uppercase;">{!! $company->kdn_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Lesen</div>
        <p>{!! !empty($company->kdn_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_start_date)->format('d/m/Y') : null !!} @if(!empty($company->kdn_start_date)) hingga @endif {!! !empty($company->kdn_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($kdn1->addresses[0]->primary))
            <p>
                {!! !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->primary : null !!}<br>{!! !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->secondary : null !!}<br>{!! !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->postcode : null !!}, {!! !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->state : null !!}, Malaysia
            </p>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil KDN</div>
        <div class="input-group">
            <input id="subFile_kdn" type="text"  class="form-control" name="kdn_cert" value="@foreach($kdn_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($kdn_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowkdn{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>
              @endif
            @endforeach
        </div>
    </div>
</div><br>

<div class="row">
    <div class="col-2">
        <div class="text-muted">No Lesen</div>
        <p>{!! $company->license_no !!}</p>
    </div>
    <div class="col-4">
        <div class="text-muted">Kawasan Yang Dibenarkan</div>
        <p>{!! $company->kdn_state !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Persatuan Perkhidmatan Kawalan Keselamatan Malaysia (PPKKM)</div>
        @if(!empty($company->license_no))
            <p>
            	@if($company->ppkkm == 0)
            		ADA
        		@elseif($company->ppkkm == 1)
        			TIADA
    			@endif
            </p>
        @endif
    </div>
</div>

@if(!empty($company->kdn_blacklist_end_date))
  <div class="row">
    <div class="col-12">
      <div class="text-muted">Tarikh Disenarai Hitam</div>
      <p>{!! !empty($company->kdn_blacklist_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_blacklist_start_date)->format('d/m/Y') : null !!} hingga {!! !empty($company->kdn_blacklist_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_blacklist_end_date)->format('d/m/Y') : null !!}</p>
    </div>
  </div>
@endif