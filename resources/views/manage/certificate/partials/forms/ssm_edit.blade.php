<div class="row">
	<div class="col-12">
		@include('components.forms.input', [
		    'input_label' => __('No Rujukan Pendaftaran (SSM)'),
		    'id' => 'ssm_no1',
		    'name' => 'ssm_no',
            'style' => 'text-transform:uppercase', 
            'maxlength' => 20,
            'required' => 'true'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Pendaftaran'),
		    'id' => 'ssm_start_date',
		    'name' => 'ssm_start_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ],
            'required' => 'true'
		])
	</div>
	<div class="col-6">
		@include('components.forms.datetimepicker', [
		    'input_label' => __('Tarikh Luput Pendaftaran'),
		    'id' => 'ssm_end_date',
		    'name' => 'ssm_end_date',
            'config' => [
                'format' => config('datetime.display.date'),
            ]
		])
	</div>
</div>

@include('components.forms.switch', [
    'id' => 'company_address', 
    'name' => 'company_address', 
    'checked' => '',
    'label' => 'Sila tekan jika ingin menggunakan alamat yang sama dengan alamat syarikat.'
])

@include('components.forms.input', [
    'input_label' => 'Alamat',
	'name' => 'ssm_primary',
	'id' => 'ssm_primary',
    'placeholder' => 'Alamat (Baris Pertama)',
    'required' => 'true',
    'mandatory' => 'true'
])
<div style="margin-top: -10px;">
    @include('components.forms.input', [
        'input_label' => '',
		'name' => 'ssm_secondary',
		'id' => 'ssm_secondary',
        'placeholder' => 'Alamat (Baris Kedua)',
        'required' => 'true',
        'mandatory' => 'true'
    ])
</div>


<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'ssm_postcode',
            'input_classes' => 'allownumericonly',
            'maxlength' => 5, 
			'id' => 'ssm_postcode',
            'required' => 'true'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'ssm_state',
            'id' => 'ssm_state',
            'required' => 'true'
        ])
	</div>
</div>

<div class="row">
    <div class="col-12">
        <label>Sijil SSM</label><br>
        <div class="input-group">
            <input id="subFile_ssm" type="text"  class="form-control" name="ssm_cert" value="@foreach($ssm_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            <label class="input-group-text" for="uploadFile_ssm"><i class="fe fe-upload" ></i></label>
            <input type="file" class="form-control" id="uploadFile_ssm" name="ssm" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
            @foreach($ssm_doc as $db)
	            @if(!empty($db))
		            <label class="input-group-text" for="downloadFile">
		                <a class="newwindowssm{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
		            </label>

                    <label class="input-group-text" for="deleteFileSSM">
                        <input type="input" id="deleteFileSSM" class="buang-upload-ssm" value="{{ $db->hashslug }}" hidden>
                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                    </label>
	            @endif
            @endforeach
        </div>
        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
    </div>
</div>