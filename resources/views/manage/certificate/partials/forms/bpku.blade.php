<div class="row">
    <div class="col-6">
        <div class="text-muted">No Rujukan Pendaftaran (BPKU)</div>
        <p style="text-transform: uppercase;">{!! $company->bpku_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh Sijil Sah</div>
        <p>{!! !empty($company->bpku_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_start_date)->format('d/m/Y') : null !!} @if(!empty($company->bpku_start_date)) hingga @endif {!! !empty($company->bpku_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_end_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil BPKU</div>
        <div class="input-group">
            <input id="subFile_bpku" type="text"  class="form-control" name="bpku_cert" value="@foreach($bpku_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($bpku_doc as $db)
              @if(!empty($db))
                <label class="input-group-text" for="downloadFile">
                    <a class="newwindowbpku{{$db->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                </label>
              @endif
            @endforeach
        </div>
    </div>
</div>

@if(!empty($company->bpku_blacklist_end_date))
  <div class="row">
    <div class="col-12">
      <div class="text-muted">Tarikh Disenarai Hitam</div>
      <p>{!! !empty($company->bpku_blacklist_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_blacklist_start_date)->format('d/m/Y') : null !!} hingga {!! !empty($company->bpku_blacklist_end_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_blacklist_end_date)->format('d/m/Y') : null !!}</p>
    </div>
  </div>
@endif
<br><br>