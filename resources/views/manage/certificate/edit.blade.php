@extends('layouts.admin')

@push('scripts')
@include('components.forms.assets.datetimepicker')
    {{--CUSTOM SEARCH--}}
    <script src="{{asset('js/list.js')}}"></script>
    <script type="text/javascript">
        var options = {
            valueNames: [ 'gred' ]
        };
        var optionskat = {
            valueNames: [ 'kat' ]
        };
        var optionskusb = {
            valueNames: [ 'kusb' ]
        };
        var optionskusce = {
            valueNames: [ 'kusce' ]
        };
        var optionskusm = {
            valueNames: [ 'kusm' ]
        };
        var optionskuse = {
            valueNames: [ 'kuse' ]
        };


        var bukaPetiList = new List('tablegred', options);
        var bukaPetiList = new List('tablekat', optionskat);
        var bukaPetiList = new List('tablekusb', optionskusb);
        var bukaPetiList = new List('tablekusce', optionskusce);
        var bukaPetiList = new List('tablekusm', optionskusm);
        var bukaPetiList = new List('tablekuse', optionskuse);
    </script>

    <script type="text/javascript">
        var optionsmof = {
            valueNames: [ 'bidang' ]
        };
        var optionsmof10000 = {
            valueNames: [ 'bidang10000' ]
        };
        var optionsmof20000 = {
            valueNames: [ 'bidang20000' ]
        };
        var optionsmof30000 = {
            valueNames: [ 'bidang30000' ]
        };
        var optionsmof40000 = {
            valueNames: [ 'bidang40000' ]
        };
        var optionsmof50000 = {
            valueNames: [ 'bidang50000' ]
        };
        var optionsmof60000 = {
            valueNames: [ 'bidang60000' ]
        };
        var optionsmof70000 = {
            valueNames: [ 'bidang70000' ]
        };
        var optionsmof80000 = {
            valueNames: [ 'bidang80000' ]
        };
        var optionsmof90000 = {
            valueNames: [ 'bidang90000' ]
        };
        var optionsmof100000 = {
            valueNames: [ 'bidang100000' ]
        };
        var optionsmof210000 = {
            valueNames: [ 'bidang210000' ]
        };
        var optionsmof220000 = {
            valueNames: [ 'bidang220000' ]
        };

        var bukaPetiList = new List('tablebidang', optionsmof);
        var bukaPetiList = new List('tablebidang10000', optionsmof10000);
        var bukaPetiList = new List('tablebidang20000', optionsmof20000);
        var bukaPetiList = new List('tablebidang30000', optionsmof30000);
        var bukaPetiList = new List('tablebidang40000', optionsmof40000);
        var bukaPetiList = new List('tablebidang50000', optionsmof50000);
        var bukaPetiList = new List('tablebidang60000', optionsmof60000);
        var bukaPetiList = new List('tablebidang70000', optionsmof70000);
        var bukaPetiList = new List('tablebidang80000', optionsmof80000);
        var bukaPetiList = new List('tablebidang90000', optionsmof90000);
        var bukaPetiList = new List('tablebidang100000', optionsmof100000);
        var bukaPetiList = new List('tablebidang210000', optionsmof210000);
        var bukaPetiList = new List('tablebidang220000', optionsmof220000);
    </script>
    <script type="text/javascript">
        @foreach($ssm_doc as $ssm)
            var url = "/uploads/{{$ssm->document_name}}";

            $('.newwindow{{$ssm->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($cidb_doc as $cidb)
            var url = "/uploads/{{$cidb->document_name}}";

            $('.newwindow{{$cidb->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($bpku_doc as $bpku)
            var url = "/uploads/{{$bpku->document_name}}";

            $('.newwindow{{$bpku->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($spkk_doc as $spkk)
            var url = "/uploads/{{$spkk->document_name}}";

            $('.newwindow{{$spkk->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($mof_doc as $mof)
            var url = "/uploads/{{$mof->document_name}}";

            $('.newwindow{{$mof->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($bumi_doc as $bumi)
            var url = "/uploads/{{$bumi->document_name}}";

            $('.newwindow{{$bumi->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($kdn_doc as $kdn)
            var url = "/uploads/{{$kdn->document_name}}";

            $('.newwindow{{$kdn->document_id}}').click(function () {

                var params = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindow = window.open(url, 'website', params);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach
    </script>
@endpush

@push('styles')
    <style type="text/css">
        .roundedInput { border-radius: 25px; padding: 7px 14px; background-color: transparent; border: solid 1px rgba(0, 0, 0, 0.2); width: 200px; box-sizing: border-box; color: #2e2e2e; margin-bottom: 5px; }
    </style>
@endpush

@push('scripts')
    <script>
        jQuery(document).ready(function($) {
            $('#ssm_no').val('{{ $company->ssm_no }}');
            @if($company->ssm_start_date)
                $('#ssm_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->ssm_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->ssm_end_date)
                $('#ssm_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->ssm_end_date)->format('d/m/Y')  }}');
            @endif
            $('#ssm_primary').val('{{ !empty($ssm1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$ssm1->addresses[0]->primary) : null}}');
            $('#ssm_secondary').val('{{ !empty($ssm1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$ssm1->addresses[0]->secondary) : null}}');
            $('#ssm_postcode').val('{{ !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->postcode : null}}');
            $('#ssm_state').val('{{ !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->state : null}}');
            $('#cidb_no').val('{{ $company->cidb_no }}');
            @if($company->cidb_start_date)
                $('#cidb_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->cidb_end_date)
                $('#cidb_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_end_date)->format('d/m/Y')  }}');
            @endif
            $('#cidb_primary').val('{{ !empty($cidb1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$cidb1->addresses[0]->primary) : null}}');
            $('#cidb_secondary').val('{{ !empty($cidb1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$cidb1->addresses[0]->secondary) : null}}');
            $('#cidb_postcode').val('{{ !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->postcode : null}}');
            $('#cidb_state').val('{{ !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->state : null}}');
            $('#cidb_blacklist').val('{{ $company->cidb_blacklist }}');
            @if($company->cidb_blacklist_end_date)
                $('#cidb_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#bpku_no').val('{{ $company->bpku_no }}');
            @if($company->bpku_start_date)
                $('#bpku_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->bpku_end_date)
                $('#bpku_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_end_date)->format('d/m/Y')  }}');
            @endif
            $('#bpku_blacklist').val('{{ $company->bpku_blacklist }}');
            @if($company->bpku_blacklist_end_date)
                $('#bpku_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#spkk_no').val('{{ $company->spkk_no }}');
            @if($company->spkk_start_date)
                $('#spkk_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->spkk_end_date)
                $('#spkk_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_end_date)->format('d/m/Y')  }}');
            @endif
            $('#spkk_blacklist').val('{{ $company->spkk_blacklist }}');
            @if($company->spkk_blacklist_end_date)
                $('#spkk_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#mof_no').val('{{ $company->mof_no }}');
            @if($company->mof_start_date)
                $('#mof_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->mof_end_date)
                $('#mof_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_end_date)->format('d/m/Y')  }}');
            @endif
            $('#mof_primary').val('{{ !empty($mof1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$mof1->addresses[0]->primary) : null}}');
            $('#mof_secondary').val('{{ !empty($mof1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$mof1->addresses[0]->secondary) : null}}');
            $('#mof_postcode').val('{{ !empty($mof1->addresses[0]) ? $mof1->addresses[0]->postcode : null}}');
            $('#mof_state').val('{{ !empty($mof1->addresses[0]) ? $mof1->addresses[0]->state : null}}');
            $('#mof_blacklist').val('{{ $company->mof_blacklist }}');
            @if($company->mof_blacklist_end_date)
                $('#mof_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#bumiputra_no').val('{{ $company->bumiputra_no }}');
            @if($company->bumiputra_start_date)
                $('#bumiputra_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->bumiputra_end_date)
                $('#bumiputra_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_end_date)->format('d/m/Y')  }}');
            @endif
            $('#kdn_no').val('{{ $company->kdn_no }}');
            @if($company->kdn_start_date)
                $('#kdn_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->kdn_end_date)
                $('#kdn_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_end_date)->format('d/m/Y')  }}');
            @endif
            $('#kdn_blacklist').val('{{ $company->kdn_blacklist }}');
            @if($company->kdn_blacklist_end_date)
                $('#kdn_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#license_no').val('{{ $company->license_no }}');
            $('#kdn_state').val('{{ $company->kdn_state }}');
            $('#ppkkm').val('{{ $company->ppkkm }}');

            $(document).on('change', '#cidb_blacklist', function(event) {
                event.preventDefault();
                $('.cidbblacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#bpku_blacklist', function(event) {
                event.preventDefault();
                $('.bpkublacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#spkk_blacklist', function(event) {
                event.preventDefault();
                $('.spkkblacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#mof_blacklist', function(event) {
                event.preventDefault();
                $('.mofblacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#kdn_blacklist', function(event) {
                event.preventDefault();
                $('.kdnblacklist').prop('hidden', !this.checked);
            });

            $('#cidb_blacklist').trigger('change');
            $('#bpku_blacklist').trigger('change');
            $('#spkk_blacklist').trigger('change');
            $('#mof_blacklist').trigger('change');
            $('#kdn_blacklist').trigger('change');

            $(document).on('change', '#Bidang10000', function(event) {
                event.preventDefault();
                $('.bidang10000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang20000', function(event) {
                event.preventDefault();
                $('.bidang20000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang30000', function(event) {
                event.preventDefault();
                $('.bidang30000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang40000', function(event) {
                event.preventDefault();
                $('.bidang40000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang50000', function(event) {
                event.preventDefault();
                $('.bidang50000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang60000', function(event) {
                event.preventDefault();
                $('.bidang60000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang70000', function(event) {
                event.preventDefault();
                $('.bidang70000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang80000', function(event) {
                event.preventDefault();
                $('.bidang80000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang90000', function(event) {
                event.preventDefault();
                $('.bidang90000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang100000', function(event) {
                event.preventDefault();
                $('.bidang100000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang210000', function(event) {
                event.preventDefault();
                $('.bidang210000').prop('hidden', !this.checked);
            });
            $(document).on('change', '#Bidang220000', function(event) {
                event.preventDefault();
                $('.bidang220000').prop('hidden', !this.checked);
            });

            $('#Bidang10000').trigger('change');
            $('#Bidang20000').trigger('change');
            $('#Bidang30000').trigger('change');
            $('#Bidang40000').trigger('change');
            $('#Bidang50000').trigger('change');
            $('#Bidang60000').trigger('change');
            $('#Bidang70000').trigger('change');
            $('#Bidang80000').trigger('change');
            $('#Bidang90000').trigger('change');
            $('#Bidang100000').trigger('change');
            $('#Bidang210000').trigger('change');
            $('#Bidang220000').trigger('change');

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Sijil Kelayakan') !!}', response.data.message, 'success');
                });
            });

            @foreach($kategori as $bil => $row2)
                $('#Kategori{{$bil}}').change(function() {
                  if($(this).is(":checked")) {
                    document.getElementById('Kategori{{$row2->code}}').style.display = "block";
                  }
                  else{
                    document.getElementById('Kategori{{$row2->code}}').style.display = "none";
                  }
                });
              @endforeach

        });
    </script>

    <script type="text/javascript">
            function hideShowAllKhusus(){
              document.getElementById('KategoriB').style.display = "none";
              document.getElementById('KategoriCE').style.display = "none";
              document.getElementById('KategoriM').style.display = "none";
              document.getElementById('KategoriE').style.display = "none";

              @foreach($company->syarikatCIDBKategori as $kat)
                @if(!empty($kat->categories == 'B'))
                  document.getElementById('KategoriB').style.display = "block";

                @elseif(!empty($kat->categories == 'CE'))
                  document.getElementById('KategoriCE').style.display = "block";

                @elseif(!empty($kat->categories == 'M'))
                  document.getElementById('KategoriM').style.display = "block";

                @elseif(!empty($kat->categories == 'E'))
                  document.getElementById('KategoriE').style.display = "block";
                @endif

              @endforeach
            }

          </script>
@endpush

@section('content')
<form method="POST" action="{{ route('manage.certificates.update',$company->hashslug) }}" files="true" enctype="multipart/form-data">
    @csrf
    @method('PUT')
	<div class="row">
        <div class="col-2 bg-transparent">
            <ul class="list-group list-group-transparent mb-0" id="certificate-tab-content" role="tablist">
                <li class="list-group-item">
                    <a id="ssm-tab" class="list-group-item-action active" data-toggle="tab" href="#ssm" role="tab" aria-controls="ssm" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('SSM') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a id="cidb-tab" class="list-group-item-action" data-toggle="tab" href="#cidb" role="tab" aria-controls="cidb" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('CIDB') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a id="mof-tab" class="list-group-item-action" data-toggle="tab" href="#mof" role="tab" aria-controls="mof" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('MOF') }}
                    </a>
                </li>
                <li class="list-group-item">
                    <a id="kdn-tab" class="list-group-item-action" data-toggle="tab" href="#kdn" role="tab" aria-controls="kdn" aria-selected="false">
                        @icon('fe fe-file-text')&nbsp;{{ __('KDN') }}
                    </a>
                </li>
            </ul>
        </div>
        @component('components.card', ['card_classes' => 'col-10'])
            @slot('card_body')
                <div class="tab-content" id="certificate-tab-content">
                    <div class="tab-pane fade show active" id="ssm" role="tabpanel" aria-labelledby="ssm-tab">
                        <h4>Pendaftaran Suruhanjaya Syarikat Malaysia (SSM)</h4>
                        
                            @include('manage.certificate.partials.forms.ssm_edit')
                        
                    </div>
                    <div class="tab-pane fade" id="cidb" role="tabpanel" aria-labelledby="cidb-tab">
                        <h4>Pendaftaran Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM/ CIDB)</h4>
                        
                            @include('manage.certificate.partials.forms.cidb_edit')
                        <br>
                        <h4>Pendaftaran Bahagian Pembangunan Kontraktor Dan Usahawanan (BPKU)</h4>
                        
                            @include('manage.certificate.partials.forms.bpku_edit')
                        <br>
                        <h4>Pendaftaran Sijil Perolehan Kerja Kerajaan (SPKK)</h4>
                        
                            @include('manage.certificate.partials.forms.spkk_edit')
                        
                    </div>

                    <div class="tab-pane fade" id="mof" role="tabpanel" aria-labelledby="mof-tab">
                        <h4>Pendaftaran Kementerian Kewangan (MOF)</h4>
                        
                            @include('manage.certificate.partials.forms.mof_edit')
                        <br>
                        <h4>Pendaftaran Sijil Akuan Pendaftaran Syarikat Bumiputera</h4>
                        
                            @include('manage.certificate.partials.forms.bumiputera_edit')
                        
                    </div>
                    <div class="tab-pane fade" id="kdn" role="tabpanel" aria-labelledby="kdn-tab">
                        <h4>Pendaftaran Kementerian Dalam Negeri (KDN)</h4>
                        
                            @include('manage.certificate.partials.forms.kdn_edit')
                        
                    </div>
                </div><br><br>
                <div class="pull-right btn-group">
                    <a href="{{ url()->previous() }}"
                       class="btn btn-default border-primary">
                        {{ __('Kembali') }}
                    </a>
                    <button type="submit" class="btn btn-primary float-right">
                        @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                </div>
            @endslot
        @endcomponent
    </div>
</form>
@endsection