<div class="container">
    <div class="row" >
        <div class="col-sm-12">
            <div class="selectgroup w-100 grade-container">
                {{-- ssm --}}
                @if(!empty($ssm_active_date))
                    <label class="selectgroup-item" style="background: #5eba00">
                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black"><b>{{ __('SSM') }}</b></span>
                    </label>
                @elseif(!empty($ssm_unlimited_active_date))
                    <label class="selectgroup-item" style="background: #5eba00">
                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black"><b>{{ __('SSM') }}</b></span>
                    </label>
                @elseif(!empty($ssm_inactive_date))
                    <label class="selectgroup-item" style="background: #f1c40f">
                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black"><b>{{ __('SSM') }}</b></span>
                    </label>
                @else
                    <label class="selectgroup-item">
                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('SSM') }}</span>
                    </label>
                @endif

                {{-- cidb --}}
                @if(!empty($cidb_blacklist_date))
                    <label class="selectgroup-item bg-dark">
                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('CIDB') }}</span>
                    </label>
                @elseif(!empty($cidb_active_date))
                    <label class="selectgroup-item" style="background: #5eba00">
                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black">{{ __('CIDB') }}</span>
                    </label>
                @elseif(!empty($cidb_inactive_date))
                    <label class="selectgroup-item" style="background: #f1c40f">
                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black">{{ __('CIDB') }}</span>
                    </label>
                @else
                    <label class="selectgroup-item">
                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('CIDB') }}</span>
                    </label>
                @endif

                {{-- mof --}}
                @if(!empty($mof_blacklist_date))
                    <label class="selectgroup-item bg-dark">
                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('MOF') }}</span>
                    </label>
                @elseif(!empty($mof_active_date))
                    <label class="selectgroup-item" style="background: #5eba00">
                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black">{{ __('MOF') }}</span>
                    </label>
                @elseif(!empty($mof_inactive_date))
                    <label class="selectgroup-item" style="background: #f1c40f">
                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black">{{ __('MOF') }}</span>
                    </label>
                @else
                    <label class="selectgroup-item">
                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('MOF') }}</span>
                    </label>
                @endif

                {{-- kdn --}}
                @if(!empty($kdn_blacklist_date))
                    <label class="selectgroup-item bg-dark">
                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('KDN') }}</span>
                    </label>
                @elseif(!empty($kdn_active_date))
                    <label class="selectgroup-item" style="background: #5eba00">
                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black">{{ __('KDN') }}</span>
                    </label>
                @elseif(!empty($kdn_inactive_date))
                    <label class="selectgroup-item" style="background: #f1c40f">
                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button" style="color: black">{{ __('KDN') }}</span>
                    </label>
                @else
                    <label class="selectgroup-item">
                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                            <span class="selectgroup-button">{{ __('KDN') }}</span>
                    </label>
                @endif

            </div>
        </div>
    </div>
    <div id='ssm'>
        <h4>Pendaftaran Suruhanjaya Syarikat Malaysia (SSM)</h4>
        @include('manage.certificate.partials.forms.ssm_edit')
    </div>
    <div id='cidb' hidden>
        <h4>Pendaftaran Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM/ CIDB)</h4>
        @include('manage.certificate.partials.forms.cidb_edit')
        <br>
        <h4>Pendaftaran Bahagian Pembangunan Kontraktor Dan Usahawanan (BPKU)</h4>
        @include('manage.certificate.partials.forms.bpku_edit')
        <br>
        <h4>Pendaftaran Sijil Perolehan Kerja Kerajaan (SPKK)</h4>
        @include('manage.certificate.partials.forms.spkk_edit')
    </div>
    <div id='mof' hidden>
        <h4>Pendaftaran Kementerian Kewangan (MOF)</h4>
        @include('manage.certificate.partials.forms.mof_edit')
        <br>
        <h4>Pendaftaran Sijil Akuan Pendaftaran Syarikat Bumiputera</h4>
        @include('manage.certificate.partials.forms.bumiputera_edit')
    </div>
    <div id='kdn' hidden>
        <h4>Pendaftaran Kementerian Dalam Negeri (KDN)</h4>
        @include('manage.certificate.partials.forms.kdn_edit')
    </div>
</div>