@extends('layouts.admin')
@push('scripts')
	<script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
	<script>
		jQuery(document).ready(function($) {

		});
	</script>
@endpush

@section('content')
	@include('manage.company.partials.scripts', [
		'table_id' => 'user-management',
		'primary_key' => 'hashslug',
		'routes' => [
			'show' => 'api.manage.companies.show',
			'store' => 'api.manage.companies.store',
			'update' => 'api.manage.companies.update',
			'destroy' => 'api.manage.companies.destroy',
		],
		'columns' => [
			'company_name' => __('table.company_name'), 
			'email' => __('table.email'), 
			'created_at' => __('table.created_at'),
			'updated_at' => __('table.updated_at')
		],
		'forms' => [
			'create' => 'company-form',
			'edit' => 'company-form',
		], 
		'disabled' => [
			'email'
		]
	])
	@component('components.forms.hidden-form', 
		[
			'id' => 'destroy-record-form',
			'action' => route('manage.companies.destroy', 'dummy')
		])
		@slot('inputs')
			@method('DELETE')
		@endslot
	@endcomponent
	@include('manage.company.partials.styles')
	@include('manage.company.partials.modals.form')
	@include('manage.company.partials.modals.show')
	@include('manage.company.partials.dashboard')
	<div class="row justify-content-center">
		<div class="col">
			@component('components.card')
				@slot('card_title')
		            <a class="btn btn-primary float-right" href="{{ route('manage.companies.create') }}">
	                  @icon('fe fe-plus') {{ __('Syarikat Baru') }}
	                </a>
					{{-- <form class="card"  method="POST" action="{{ route('manage.companies.store') }}" files="true" enctype="multipart/form-data">
			            @csrf
			            <button type="submit" class="btn btn-primary float-right">
			              @icon('fe fe-plus') {{ __('Syarikat Baru') }}
			            </button>
			            <div class="row d-none">
			              	<div class="col-4">
				                @include('components.forms.input', [
				                  'input_label' => 'No. Telefon (Pejabat)',
				                  'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::OFFICE.']',
				                  'type' => 'number'
				                ])
			              	</div>
			              	<div class="col-4">
				                @include('components.forms.input', [
				                  'input_label' => 'No. Telefon (Bimbit)',
				                  'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
				                  'type' => 'number'
				                ])
			              	</div>
			              	<div class="col-4">
				                @include('components.forms.input', [
				                  'input_label' => 'No. Faks',
				                  'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::OTHER.']',
				                  'type' => 'number'
				                ])
			              	</div>
			            </div>
			        </form> --}}
				@endslot
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'user-management',
							'route_name' => 'api.datatable.manage.company',
							'columns' => [
								['data' => 'company_name', 'title' => __('Nama Syarikat'), 'defaultContent' => '-'],
								['data' => 'email', 'title' => __('E-Mel'), 'defaultContent' => '-'],
								['data' => 'status_company', 'title' => __('Status'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false],
							],
							'headers' => [
								__('Nama Syarikat'), __('E-mel'),  __('Status'), __('table.action')
							],
							'actions' => minify(view('manage.company.partials.actions')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	</div>
@endsection