@extends('layouts.admin')

@push('scripts')
    <script>
        jQuery(document).ready(function($) {

            //tab remain stay after refresh
            $('#company-tab-content a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.list-group-transparent > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#company-tab-content a[href="#{{ old('tab') }}"]').tab('show');
            //tab remain stay after refresh

            $('#ssm_no').val('{{ $company->ssm_no }}');
            $('#company_name').val('{!! $company->company_name !!}');
            $('#email').val('{!! $company->email !!}');
            $('#gst_registered_no').val('{{ $company->gst_registered_no }}');
            @if($company->gst_start_date)
                $('#gst_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->gst_start_date)->format('d/m/Y')  }}');
            @endif
            $('#account_bank_no').val('{{ $company->account_bank_no }}');
            $('#bank_name').val('{{ $company->bank_name }}');
            @if(!empty($company->addresses[0]))
                $('#primary').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$company->addresses[0]->primary) !!}');
                $('#secondary').val('{!! str_replace(["\r\n","\r","\n"],"\\n ",$company->addresses[0]->secondary) !!}');
                $('#postcode').val('{{ $company->addresses[0]->postcode }}');
                $('#state').val('{{ $company->addresses[0]->state }}');
            @endif
            @if(!empty($company->phones[0]))
                $('#phone_number1').val('{{ $company->phones[0]->phone_number }}');
            @endif
            @if(!empty($company->phones[1]))
                $('#phone_number2').val('{{ $company->phones[1]->phone_number }}');
            @endif
            @if(!empty($company->phones[2]))
                $('#phone_number3').val('{{ $company->phones[2]->phone_number }}');
            @endif

            $('#uploadFile_status').change(function(){
                $('#subFile_status').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_ssm').change(function(){
                $('#subFile_ssm').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_cidb').change(function(){
                $('#subFile_cidb').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_bpku').change(function(){
                $('#subFile_bpku').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_spkk').change(function(){
                $('#subFile_spkk').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_mof').change(function(){
                $('#subFile_mof').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_bumi').change(function(){
                $('#subFile_bumi').val($(this).val().split('\\').pop());
            });

            $('#uploadFile_kdn').change(function(){
                $('#subFile_kdn').val($(this).val().split('\\').pop());
            });

            $(".allownumericonly").on("keypress keyup blur",function (event) {    
               $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini asdSyarikat') !!}', response.data.message, 'success');
                });
            });$(document).on('click', '.buang-upload', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran GST ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.companies.delete', id))
                        .then(response => {
                            swal('GST / SST', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-ssm', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran SSM ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_ssm', id))
                        .then(response => {
                            swal('SSM', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-cidb', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran CIDB ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_cidb', id))
                        .then(response => {
                            swal('CIDB', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-bpku', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran BPKU ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_bpku', id))
                        .then(response => {
                            swal('BPKU', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-spkk', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran SPKK ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_spkk', id))
                        .then(response => {
                            swal('SPKK', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-mof', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran MOF ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_mof', id))
                        .then(response => {
                            swal('MOF', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-bumi', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran Bumiputra ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_bumi', id))
                        .then(response => {
                            swal('BUMIPUTRA', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });

            $(document).on('click', '.buang-upload-kdn', function(event) {
                event.preventDefault();
                var id = $(this).val();
                swal({
                  title: '{!! __('Amaran') !!}',
                  text: '{!! __('Adakah anda pasti mahu memadamkan lampiran KDN ini?') !!}',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: '{!! __('Ya') !!}',
                  cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                  if (result.value) {
                        axios.delete(route('manage.certificates.delete_kdn', id))
                        .then(response => {
                            swal('KDN', response.data.message, 'success');
                            location.reload();
                        })
                  }
                });
            });
        });

        function getKategori2(obj) {
            var ssm = document.getElementById('ssm');
            var cidb = document.getElementById('cidb');
            var mof = document.getElementById('mof');
            var kdn = document.getElementById('kdn');

            if(obj.value == '1'){
                ssm.hidden = false;
                cidb.hidden = true;
                mof.hidden = true;
                kdn.hidden = true;
            }
            else if(obj.value == '2'){
                ssm.hidden = true;
                cidb.hidden = false;
                mof.hidden = true;
                kdn.hidden = true;
            }
            else if(obj.value == '3'){
                ssm.hidden = true;
                cidb.hidden = true;
                mof.hidden = false;
                kdn.hidden = true;
            }
            else if(obj.value == '4'){
                ssm.hidden = true;
                cidb.hidden = true;
                mof.hidden = true;
                kdn.hidden = false;
            }
        }

        @foreach($gst_doc as $db)
            var urlgst = "/company/{{$db->document_name}}";

            $('.newwindowgst{{$db->document_id}}').click(function () {

                var paramsgst = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowgst = window.open(urlgst, 'website', paramsgst);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach
    </script>
    {{--CUSTOM SEARCH--}}
    <script src="{{asset('js/list.js')}}"></script>
    <script type="text/javascript">

        @foreach(cidb_codes('category') as $cat)
            var optionskus{{$cat->code}} = {
                valueNames: [ 'kus{{$cat->code}}' ]
            };
            var bukaPetiList = new List('tablekus{{$cat->code}}', optionskus{{$cat->code}});
        @endforeach
        var options = {
            valueNames: [ 'gred' ]
        };
        var optionskat = {
            valueNames: [ 'kat' ]
        };
        // var optionskusb = {
        //     valueNames: [ 'kusb' ]
        // };
        // var optionskusce = {
        //     valueNames: [ 'kusce' ]
        // };
        // var optionskusm = {
        //     valueNames: [ 'kusm' ]
        // };
        // var optionskuse = {
        //     valueNames: [ 'kuse' ]
        // };
        // var optionskusf = {
        //     valueNames: [ 'kusf' ]
        // };


        var bukaPetiList = new List('tablegred', options);
        var bukaPetiList = new List('tablekat', optionskat);
        // var bukaPetiList = new List('tablekusb', optionskusb);
        // var bukaPetiList = new List('tablekusce', optionskusce);
        // var bukaPetiList = new List('tablekusm', optionskusm);
        // var bukaPetiList = new List('tablekuse', optionskuse);
        // var bukaPetiList = new List('tablekusf', optionskusf);
    </script>

    <script type="text/javascript">
        var optionsmof = {
            valueNames: [ 'bidang' ]
        };
        @foreach(mof_codes('category') as $cat)
            var optionsmof{{$cat->code}} = {
                valueNames: [ 'bidang{{$cat->code}}' ]
            };
            var bukaPetiList = new List('tablebidang{{$cat->code}}', optionsmof{{$cat->code}});
        @endforeach
        // var optionsmof10000 = {
        //     valueNames: [ 'bidang010000' ]
        // };
        // var optionsmof20000 = {
        //     valueNames: [ 'bidang20000' ]
        // };
        // var optionsmof30000 = {
        //     valueNames: [ 'bidang30000' ]
        // };
        // var optionsmof40000 = {
        //     valueNames: [ 'bidang40000' ]
        // };
        // var optionsmof50000 = {
        //     valueNames: [ 'bidang50000' ]
        // };
        // var optionsmof60000 = {
        //     valueNames: [ 'bidang60000' ]
        // };
        // var optionsmof70000 = {
        //     valueNames: [ 'bidang70000' ]
        // };
        // var optionsmof80000 = {
        //     valueNames: [ 'bidang80000' ]
        // };
        // var optionsmof90000 = {
        //     valueNames: [ 'bidang90000' ]
        // };
        // var optionsmof100000 = {
        //     valueNames: [ 'bidang100000' ]
        // };
        // var optionsmof210000 = {
        //     valueNames: [ 'bidang210000' ]
        // };
        // var optionsmof220000 = {
        //     valueNames: [ 'bidang220000' ]
        // };

        var bukaPetiList = new List('tablebidang', optionsmof);
        // var bukaPetiList = new List('tablebidang10000', optionsmof10000);
        // var bukaPetiList = new List('tablebidang20000', optionsmof20000);
        // var bukaPetiList = new List('tablebidang30000', optionsmof30000);
        // var bukaPetiList = new List('tablebidang40000', optionsmof40000);
        // var bukaPetiList = new List('tablebidang50000', optionsmof50000);
        // var bukaPetiList = new List('tablebidang60000', optionsmof60000);
        // var bukaPetiList = new List('tablebidang70000', optionsmof70000);
        // var bukaPetiList = new List('tablebidang80000', optionsmof80000);
        // var bukaPetiList = new List('tablebidang90000', optionsmof90000);
        // var bukaPetiList = new List('tablebidang100000', optionsmof100000);
        // var bukaPetiList = new List('tablebidang210000', optionsmof210000);
        // var bukaPetiList = new List('tablebidang220000', optionsmof220000);
    </script>
    <script type="text/javascript">
        @foreach($ssm_doc as $ssm)
            var urlssm = "/company/{{$ssm->document_name}}";

            $('.newwindowssm{{$ssm->document_id}}').click(function () {

                var paramsssm = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowssm = window.open(urlssm, 'website', paramsssm);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($cidb_doc as $cidb)
            var urlcidb = "/company/{{$cidb->document_name}}";

            $('.newwindowcidb{{$cidb->document_id}}').click(function () {

                var paramscidb = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowcidb = window.open(urlcidb, 'website', paramscidb);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($bpku_doc as $bpku)
            var urlbpku = "/company/{{$bpku->document_name}}";

            $('.newwindowbpku{{$bpku->document_id}}').click(function () {

                var paramsbpku = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowbpku = window.open(urlbpku, 'website', paramsbpku);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($spkk_doc as $spkk)
            var urlspkk = "/company/{{$spkk->document_name}}";

            $('.newwindowspkk{{$spkk->document_id}}').click(function () {

                var paramsspkk = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowspkk = window.open(urlspkk, 'website', paramsspkk);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($mof_doc as $mof)
            var mof = "/company/{{$mof->document_name}}";

            $('.newwindowmof{{$mof->document_id}}').click(function () {

                var paramsmof = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowmof = window.open(mof, 'website', paramsmof);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($bumi_doc as $bumi)
            var urlbumi = "/company/{{$bumi->document_name}}";

            $('.newwindowbumi{{$bumi->document_id}}').click(function () {

                var paramsbumi = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowbumi = window.open(urlbumi, 'website', paramsbumi);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach

        @foreach($kdn_doc as $kdn)
            var urlkdn = "/company/{{$kdn->document_name}}";

            $('.newwindowkdn{{$kdn->document_id}}').click(function () {

                var paramskdn = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                try {
                    var newwindowkdn = window.open(urlkdn, 'website', paramskdn);
                } catch (err) {
                    $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                }
             });
        @endforeach
    </script>
@endpush

@push('styles')
    <style type="text/css">
        .roundedInput { border-radius: 25px; padding: 7px 14px; background-color: transparent; border: solid 1px rgba(0, 0, 0, 0.2); width: 200px; box-sizing: border-box; color: #2e2e2e; margin-bottom: 5px; }
    </style>
@endpush

@push('scripts')
@include('components.forms.assets.datetimepicker')
    <script>
        jQuery(document).ready(function($) {
            $('#ssm_no1').val('{{ $company->ssm_no }}');
            @if($company->ssm_start_date)
                $('#ssm_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->ssm_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->ssm_end_date)
                $('#ssm_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->ssm_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#ssm_primary').val('{!! !empty($ssm1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$ssm1->addresses[0]->primary) : null!!}');
                $('#ssm_secondary').val('{!! !empty($ssm1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$ssm1->addresses[0]->secondary) : null!!}');
                $('#ssm_postcode').val('{{ !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->postcode : null}}');
                $('#ssm_state').val('{{ !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->state : null}}');
                $('#ssm_state').trigger('change');
                var ckbox = $('#company_address');
                $('#company_address').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#ssm_primary').val($('#primary').val());
                        $('#ssm_secondary').val($('#secondary').val());
                        $('#ssm_postcode').val($('#postcode').val());
                        $('#ssm_state').val($('#state').val());
                        
                    } else {
                        $('#ssm_primary').val('{!! !empty($ssm1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$ssm1->addresses[0]->primary) : null!!}');
                        $('#ssm_secondary').val('{!! !empty($ssm1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$ssm1->addresses[0]->secondary) : null!!}');
                        $('#ssm_postcode').val('{{ !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->postcode : null}}');
                        $('#ssm_state').val('{{ !empty($ssm1->addresses[0]) ? $ssm1->addresses[0]->state : null}}');
                        
                    }
                    $('#ssm_state').trigger('change');
                });
            });
            $('#cidb_no').val('{{ $company->cidb_no }}');
            @if($company->cidb_start_date)
                $('#cidb_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->cidb_end_date)
                $('#cidb_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#cidb_primary').val('{!! !empty($cidb1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$cidb1->addresses[0]->primary) : null!!}');
                $('#cidb_secondary').val('{!! !empty($cidb1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$cidb1->addresses[0]->secondary) : null!!}');
                $('#cidb_postcode').val('{{ !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->postcode : null}}');
                $('#cidb_state').val('{{ !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->state : null}}');
                $('#cidb_state').trigger('change');
                var ckbox = $('#cidb_company_address');
                $('#cidb_company_address').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#cidb_primary').val($('#ssm_primary').val());
                        $('#cidb_secondary').val($('#ssm_secondary').val());
                        $('#cidb_postcode').val($('#ssm_postcode').val());
                        $('#cidb_state').val($('#ssm_state').val());
                        
                    } else {
                        $('#cidb_primary').val('{!! !empty($cidb1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$cidb1->addresses[0]->primary) : null!!}');
                        $('#cidb_secondary').val('{!! !empty($cidb1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$cidb1->addresses[0]->secondary) : null!!}');
                        $('#cidb_postcode').val('{{ !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->postcode : null}}');
                        $('#cidb_state').val('{{ !empty($cidb1->addresses[0]) ? $cidb1->addresses[0]->state : null}}');
                        
                    }
                    $('#cidb_state').trigger('change');
                });
            });

            $(document).ready(function () {
                $("#cidb_no").keyup(function(){
                    if($(this).val() == "") {
                        $("#cidb_start_date").prop('required',false);
                        $("#cidb_end_date").prop('required',false);
                        $("#cidb_primary").prop('required',false);
                        $("#cidb_secondary").prop('required',false);
                        $("#cidb_postcode").prop('required',false);
                        $("#cidb_state").prop('required',false);
                    } else {
                        $("#cidb_start_date").prop('required',true);
                        $("#cidb_end_date").prop('required',true);
                        $("#cidb_primary").prop('required',true);
                        $("#cidb_secondary").prop('required',true);
                        $("#cidb_postcode").prop('required',true);
                        $("#cidb_state").prop('required',true);
                    }
                });
            });

            $('#cidb_blacklist').val('{{ $company->cidb_blacklist }}');
            @if($company->cidb_blacklist_end_date)
                $('#cidb_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->cidb_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#bpku_no').val('{{ $company->bpku_no }}');
                var ckbox = $('#bpkus');
                $('#bpkus').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#bpku_no').val($('#cidb_no').val());
                    } else {
                        $('#bpku_no').val('{{ $company->bpku_no }}');
                    }
                });
            });

            $(document).ready(function () {
                $("#bpku_no").keyup(function(){
                    if($(this).val() == "") {
                        $("#bpku_start_date").prop('required',false);
                        $("#bpku_end_date").prop('required',false);
                    } else {
                        $("#bpku_start_date").prop('required',true);
                        $("#bpku_end_date").prop('required',true);
                    }
                });
            });

            @if($company->bpku_start_date)
                $('#bpku_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->bpku_end_date)
                $('#bpku_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_end_date)->format('d/m/Y')  }}');
            @endif
            $('#bpku_blacklist').val('{{ $company->bpku_blacklist }}');
            @if($company->bpku_blacklist_end_date)
                $('#bpku_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bpku_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#spkk_no').val('{{ $company->spkk_no }}');
                var ckbox = $('#spkks');
                $('#spkks').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#spkk_no').val($('#cidb_no').val());
                    } else {
                        $('#spkk_no').val('{{ $company->spkk_no }}');
                    }
                });
            });

            $(document).ready(function () {
                $("#spkk_no").keyup(function(){
                    if($(this).val() == "") {
                        $("#spkk_start_date").prop('required',false);
                        $("#spkk_end_date").prop('required',false);
                    } else {
                        $("#spkk_start_date").prop('required',true);
                        $("#spkk_end_date").prop('required',true);
                    }
                });
            });

            @if($company->spkk_start_date)
                $('#spkk_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->spkk_end_date)
                $('#spkk_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_end_date)->format('d/m/Y')  }}');
            @endif
            $('#spkk_blacklist').val('{{ $company->spkk_blacklist }}');
            @if($company->spkk_blacklist_end_date)
                $('#spkk_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->spkk_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#mof_no').val('{{ $company->mof_no }}');
            @if($company->mof_start_date)
                $('#mof_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->mof_end_date)
                $('#mof_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#mof_primary').val('{!! !empty($mof1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$mof1->addresses[0]->primary) : null!!}');
                $('#mof_secondary').val('{!! !empty($mof1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$mof1->addresses[0]->secondary) : null!!}');
                $('#mof_postcode').val('{{ !empty($mof1->addresses[0]) ? $mof1->addresses[0]->postcode : null}}');
                $('#mof_state').val('{{ !empty($mof1->addresses[0]) ? $mof1->addresses[0]->state : null}}');
                $('#mof_state').trigger('change');
                var ckbox = $('#mof_company_address');
                $('#mof_company_address').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#mof_primary').val($('#ssm_primary').val());
                        $('#mof_secondary').val($('#ssm_secondary').val());
                        $('#mof_postcode').val($('#ssm_postcode').val());
                        $('#mof_state').val($('#ssm_state').val());
                        
                    } else {
                        $('#mof_primary').val('{!! !empty($mof1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$mof1->addresses[0]->primary) : null!!}');
                        $('#mof_secondary').val('{!! !empty($mof1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$mof1->addresses[0]->secondary) : null!!}');
                        $('#mof_postcode').val('{{ !empty($mof1->addresses[0]) ? $mof1->addresses[0]->postcode : null}}');
                        $('#mof_state').val('{{ !empty($mof1->addresses[0]) ? $mof1->addresses[0]->state : null}}');
                        
                    }
                    $('#mof_state').trigger('change');
                });
            });

            $(document).ready(function () {
                $("#mof_no").keyup(function(){
                    if($(this).val() == "") {
                        $("#mof_start_date").prop('required',false);
                        $("#mof_end_date").prop('required',false);
                        $("#mof_primary").prop('required',false);
                        $("#mof_secondary").prop('required',false);
                        $("#mof_postcode").prop('required',false);
                        $("#mof_state").prop('required',false);
                    } else {
                        $("#mof_start_date").prop('required',true);
                        $("#mof_end_date").prop('required',true);
                        $("#mof_primary").prop('required',true);
                        $("#mof_secondary").prop('required',true);
                        $("#mof_postcode").prop('required',true);
                        $("#mof_state").prop('required',true);
                    }
                });
            });

            $('#mof_blacklist').val('{{ $company->mof_blacklist }}');
            @if($company->mof_blacklist_end_date)
                $('#mof_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->mof_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#bumiputra_no').val('{{ $company->bumiputra_no }}');
                @if($company->bumiputra_start_date)
                    $('#bumiputra_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_start_date)->format('d/m/Y')  }}');
                @endif
                @if($company->bumiputra_end_date)
                    $('#bumiputra_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_end_date)->format('d/m/Y')  }}');
                @endif
                var ckbox = $('#bumiputras');
                $('#bumiputras').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#bumiputra_no').val($('#mof_no').val());
                        $('#bumiputra_start_date').val($('#mof_start_date').val());
                        $('#bumiputra_end_date').val($('#mof_end_date').val());
                    } else {
                        $('#bumiputra_no').val('{{ $company->bumiputra_no }}');
                        @if($company->bumiputra_start_date)
                            $('#bumiputra_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_start_date)->format('d/m/Y')  }}');
                        @endif
                        @if($company->bumiputra_end_date)
                            $('#bumiputra_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->bumiputra_end_date)->format('d/m/Y')  }}');
                        @endif
                    }
                });
            });

            $(document).ready(function () {
                $("#bumiputra_no").keyup(function(){
                    if($(this).val() == "") {
                        $("#bumiputra_start_date").prop('required',false);
                        $("#bumiputra_end_date").prop('required',false);
                    } else {
                        $("#bumiputra_start_date").prop('required',true);
                        $("#bumiputra_end_date").prop('required',true);
                    }
                });
            });

            $('#kdn_no').val('{{ $company->kdn_no }}');
            @if($company->kdn_start_date)
                $('#kdn_start_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_start_date)->format('d/m/Y')  }}');
            @endif
            @if($company->kdn_end_date)
                $('#kdn_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_end_date)->format('d/m/Y')  }}');
            @endif
            $(document).ready(function () {
                $('#kdn_primary').val('{!! !empty($kdn1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$kdn1->addresses[0]->primary) : null!!}');
                $('#kdn_secondary').val('{!! !empty($kdn1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$kdn1->addresses[0]->secondary) : null!!}');
                $('#kdn_postcode').val('{{ !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->postcode : null}}');
                $('#kdn_state').val('{{ !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->state : null}}');
                $('#kdn_state').trigger('change');
                var ckbox = $('#kdn_company_address');
                $('#kdn_company_address').on('change',function () {
                    if (ckbox.is(':checked')) {
                        $('#kdn_primary').val($('#mof_primary').val());
                        $('#kdn_secondary').val($('#mof_secondary').val());
                        $('#kdn_postcode').val($('#mof_postcode').val());
                        $('#kdn_state').val($('#mof_state').val());
                        
                    } else {
                        $('#kdn_primary').val('{!! !empty($kdn1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$kdn1->addresses[0]->primary) : null!!}');
                        $('#kdn_secondary').val('{!! !empty($kdn1->addresses[0]) ? str_replace(["\r\n","\r","\n"],"\\n ",$kdn1->addresses[0]->secondary) : null!!}');
                        $('#kdn_postcode').val('{{ !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->postcode : null}}');
                        $('#kdn_state').val('{{ !empty($kdn1->addresses[0]) ? $kdn1->addresses[0]->state : null}}');
                        
                    }
                    $('#kdn_state').trigger('change');
                });
            });

            $(document).ready(function () {
                $("#kdn_no").keyup(function(){
                    if($(this).val() == "") {
                        $("#kdn_start_date").prop('required',false);
                        $("#kdn_end_date").prop('required',false);
                        $("#kdn_primary").prop('required',false);
                        $("#kdn_secondary").prop('required',false);
                        $("#kdn_postcode").prop('required',false);
                        $("#kdn_state").prop('required',false);
                        $("#license_no").prop('required',false);
                    } else {
                        $("#kdn_start_date").prop('required',true);
                        $("#kdn_end_date").prop('required',true);
                        $("#kdn_primary").prop('required',true);
                        $("#kdn_secondary").prop('required',true);
                        $("#kdn_postcode").prop('required',true);
                        $("#kdn_state").prop('required',true);
                        $("#license_no").prop('required',true);
                    }
                });
            });

            $('#kdn_blacklist').val('{{ $company->kdn_blacklist }}');
            @if($company->kdn_blacklist_end_date)
                $('#kdn_blacklist_end_date').val('{{ \Carbon\Carbon::createFromFormat('Y-m-d',$company->kdn_blacklist_end_date)->format('d/m/Y')  }}');
            @endif
            $('#license_no').val('{{ $company->license_no }}');
            $('#kdn_states').val('{{ $company->kdn_state }}');
            $('#ppkkm').val('{{ $company->ppkkm }}');

            $(document).on('change', '#cidb_blacklist', function(event) {
                event.preventDefault();
                $('.cidbblacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#bpku_blacklist', function(event) {
                event.preventDefault();
                $('.bpkublacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#spkk_blacklist', function(event) {
                event.preventDefault();
                $('.spkkblacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#mof_blacklist', function(event) {
                event.preventDefault();
                $('.mofblacklist').prop('hidden', !this.checked);
            });
            $(document).on('change', '#kdn_blacklist', function(event) {
                event.preventDefault();
                $('.kdnblacklist').prop('hidden', !this.checked);
            });

            $('#cidb_blacklist').trigger('change');
            $('#bpku_blacklist').trigger('change');
            $('#spkk_blacklist').trigger('change');
            $('#mof_blacklist').trigger('change');
            $('#kdn_blacklist').trigger('change');

            @foreach(mof_codes('category') as $cat)
                $(document).on('change', '#Bidang{{$cat->code}}', function(event) {
                    event.preventDefault();
                    $('.bidang{{$cat->code}}').prop('hidden', !this.checked);
                });
                $('#Bidang{{$cat->code}}').trigger('change');
            @endforeach
            // $(document).on('change', '#Bidang010000', function(event) {
            //     event.preventDefault();
            //     $('.bidang010000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang020000', function(event) {
            //     event.preventDefault();
            //     $('.bidang020000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang030000', function(event) {
            //     event.preventDefault();
            //     $('.bidang030000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang040000', function(event) {
            //     event.preventDefault();
            //     $('.bidang040000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang050000', function(event) {
            //     event.preventDefault();
            //     $('.bidang050000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang060000', function(event) {
            //     event.preventDefault();
            //     $('.bidang060000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang070000', function(event) {
            //     event.preventDefault();
            //     $('.bidang070000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang080000', function(event) {
            //     event.preventDefault();
            //     $('.bidang080000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang090000', function(event) {
            //     event.preventDefault();
            //     $('.bidang090000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang100000', function(event) {
            //     event.preventDefault();
            //     $('.bidang100000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang210000', function(event) {
            //     event.preventDefault();
            //     $('.bidang210000').prop('hidden', !this.checked);
            // });
            // $(document).on('change', '#Bidang220000', function(event) {
            //     event.preventDefault();
            //     $('.bidang220000').prop('hidden', !this.checked);
            // });

            // $('#Bidang010000').trigger('change');
            // $('#Bidang020000').trigger('change');
            // $('#Bidang030000').trigger('change');
            // $('#Bidang040000').trigger('change');
            // $('#Bidang050000').trigger('change');
            // $('#Bidang060000').trigger('change');
            // $('#Bidang070000').trigger('change');
            // $('#Bidang080000').trigger('change');
            // $('#Bidang090000').trigger('change');
            // $('#Bidang100000').trigger('change');
            // $('#Bidang210000').trigger('change');
            // $('#Bidang220000').trigger('change');

            $(document).on('click', '.submit-action-btn', function(event) {
                event.preventDefault();
                var id = $(this).data('hashslug');
                var route_name = $(this).data('route');
                var form_id = $(this).data('form');
                var data = $('#' + form_id).serialize();
                axios.put(route(route_name, id), data).then(response => {
                    swal('{!! __('Kemaskini Sijil Kelayakan') !!}', response.data.message, 'success');
                });
            });

            @foreach($kategori as $bil => $row2)
                $('#Kategori{{$bil}}').change(function() {
                  if($(this).is(":checked")) {
                    document.getElementById('Kategori{{$row2->code}}').style.display = "block";
                  }
                  else{
                    document.getElementById('Kategori{{$row2->code}}').style.display = "none";
                  }
                });
            @endforeach

        });
    </script>

    <script type="text/javascript">
        function hideShowAllKhusus(){
            @foreach(cidb_codes('category') as $cat)
                document.getElementById('Kategori{{$cat->code}}').style.display = "none";
                // document.getElementById('KategoriCE').style.display = "none";
                // document.getElementById('KategoriM').style.display = "none";
                // document.getElementById('KategoriE').style.display = "none";
                // document.getElementById('KategoriF').style.display = "none";

                @foreach($company->syarikatCIDBKategori as $kat)
                    @if(!empty($kat->categories == $cat->code))
                        document.getElementById('Kategori{{$cat->code}}').style.display = "block";
                    @endif
                @endforeach
            @endforeach
        }

    </script>
@endpush

@section('content')
@include('manage.company.partials.styles')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="company-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#company" role="tab" aria-controls="company" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Perincian') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#cert" role="tab" aria-controls="cert" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Sijil Kelayakan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#owner" role="tab" aria-controls="owner" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Pemilik Syarikat') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10"> 
        <div class="tab-content">
            <div class="tab-pane fade show active" id="company" role="tabpanel" aria-labelledby="company-tab">
                <form method="POST" action="{{ route('manage.companies.update',$company->id) }}" files="true" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                	<div class="row">
                        @component('components.card', ['card_classes' => 'col-12'])
                             @slot('card_body')
                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'Nama Syarikat',
                                            'id' => 'company_name',
                                            'name' => 'company_name',
                                            'required' => 'true'
                                        ])
                                    </div>
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'No Pendaftaran SSM',
                                            'id' => 'ssm_no',
                                            'name' => 'ssm_no',
                                            'style' => 'text-transform:uppercase', 
                                            'maxlength' => 20,
                                            'required' => 'true'
                                        ])
                                    </div>
                                </div>

                                @include('components.forms.input', [
                                    'input_label' => 'Alamat Syarikat',
                                    'id' => 'primary',
                                    'name' => 'primary',
                                    'placeholder' => 'Alamat Syarikat (Baris Pertama)',
                                    'required' => 'true'
                                ])
                                <div style="margin-top: -10px;">
                                    @include('components.forms.input', [
                                        'input_label' => '',
                                        'id' => 'secondary',
                                        'name' => 'secondary',
                                        'placeholder' => 'Alamat Syarikat (Baris Kedua)',
                                        'required' => 'true'
                                    ])
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'Poskod',
                                            'id' => 'postcode',
                                            'name' => 'postcode',
                                            'input_classes' => 'allownumericonly', 
                                            'maxlength' => 5,
                                            'required' => 'true'
                                        ])
                                    </div>
                                    <div class="col-6">
                                        @include('components.forms.select', [
                                            'input_label' => 'Negeri',
                                            'id' => 'state',
                                            'name' => 'state',
                                            'options' => $std_cd_state->pluck('description','description'),
                                            'required' => 'true'
                                        ])
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'No. Telefon (Pejabat)',
                                            'id' => 'phone_number1',
                                            'name' => 'phone_number3',
                                            'input_classes' => 'allownumericonly', 
                                            'maxlength' => 15,
                                            'required' => 'true'
                                        ])
                                    </div>
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'No. Telefon (Bimbit)',
                                            'id' => 'phone_number2',
                                            'name' => 'phone_number2',
                                            'input_classes' => 'allownumericonly', 
                                            'maxlength' => 15,
                                            'required' => 'true'
                                        ])
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'No. Faks',
                                            'id' => 'phone_number3',
                                            'name' => 'phone_number4',
                                            'input_classes' => 'allownumericonly', 
                                            'maxlength' => 15,
                                        ])
                                    </div>
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'E-Mel',
                                            'id' => 'email',
                                            'name' => 'email',
                                            'type' => 'email',
                                            'required' => 'true'
                                        ])
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'No. Pendaftaran GST / SST',
                                            'id' => 'gst_registered_no',
                                            'name' => 'gst_registered_no',
                                        ])
                                    </div>
                                    <div class="col-6" style="margin-top: 8px;">
                                      @include('components.forms.datetimepicker', [
                                        'input_label' => __('Tarikh GST / SST Berkuatkuasa'),
                                        'id' => 'gst_start_date',
                                        'name' => 'gst_start_date',
                                        'config' => [
                                            'format' => config('datetime.display.date'),
                                        ]
                                      ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <label>Sijil Pendaftaran GST / SST</label><br>
                                        <div class="input-group">
                                            <input id="subFile_status" type="text"  class="form-control" value="@foreach($gst_doc as $doc){{$doc->document_name}}@endforeach" readonly>
                                            <label class="input-group-text" for="uploadFile_status"><i class="fe fe-upload" ></i></label>
                                            <input type="file" class="form-control" id="uploadFile_status" name="gst" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                                            @foreach($gst_doc as $db_gst)
                                                @if(!empty($db_gst))
                                                    <label class="input-group-text" for="downloadFile">
                                                        <a class="newwindowgst{{$db_gst->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                                                    </label>

                                                    <label class="input-group-text" for="deleteFileGST">
                                                        <input type="input" id="deleteFileGST" class="buang-upload" value="{{ $db_gst->hashslug }}" hidden>
                                                        <a title="Buang Lampiran" ><i class="fas fa-times" style="color:red"></i></a>
                                                    </label>
                                                @endif
                                            @endforeach
                                        </div>
                                        <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        @include('components.forms.select', [
                                            'input_label' => 'Nama Bank',
                                            'id' => 'bank_name',
                                            'name' => 'bank_name',
                                            'options' => $std_cd_bank->pluck('name','name'),
                                            
                                        ])
                                    </div>
                                    <div class="col-6">
                                        @include('components.forms.input', [
                                            'input_label' => 'No. Akaun Bank',
                                            'id' => 'account_bank_no',
                                            'name' => 'account_bank_no',
                                            'input_classes' => 'allownumericonly',
                                            
                                        ])
                                    </div>
                                </div>
                                <div class="pull-right btn-group">
                                    <a href="{{ route('manage.companies.index') }}"
                                       class="btn btn-default border-primary">
                                        {{ __('Kembali') }}
                                    </a>
                                    <button type="submit" class="btn btn-primary float-right">
                                        @icon('fe fe-save') {{ __('Simpan') }}
                                    </button>
                                </div>
                            @endslot
                         @endcomponent
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="cert" role="tabpanel" aria-labelledby="cert-tab">
                <form method="POST" action="{{ route('manage.certificates.update',$company->hashslug) }}" files="true" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    @component('components.card')
                        @slot('card_body')
                            @include('manage.certificate.index')<br><br>
                            <div class="pull-right btn-group">
                                <a href="{{ route('manage.companies.index') }}"
                                   class="btn btn-default border-primary">
                                    {{ __('Kembali') }}
                                </a>
                                <button type="submit" class="btn btn-primary float-right">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                                </button>
                            </div>
                        @endslot
                    @endcomponent
                </form>
            </div>
            <div class="tab-pane fade" id="owner" role="tabpanel" aria-labelledby="owner-tab">
                <form id="detail-form">
                    @include('manage.owner.index')
                </form>
            </div>
        </div>
    </div>
</div>
@endsection