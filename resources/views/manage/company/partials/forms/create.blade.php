<form  method="POST" action="{{ route('manage.companies.store') }}" files="true" enctype="multipart/form-data">
	@csrf
<div class="row">
	<div class="col-4">
		@include('components.forms.hidden', [
			'id' => 'id',
			'name' => 'id',
			'value' => ''
		])

		@include('components.forms.input', [
			'input_label' => 'No Pendaftaran SSM',
			'name' => 'ssm_no'
		])
	</div>
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => 'No Pendaftaran GST / SST',
			'name' => 'gst_registered_no'
		])
	</div>
	<div class="col-4">
		@include('components.forms.file', [
			'input_label' => 'Sijil Pendaftaran GST / SST',
			'name' => 'gst'
		])
	</div>
</div>

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Nama Syarikat',
			'name' => 'company_name'
		])
	</div>
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'E-Mel',
			'name' => 'email',
			'type' => 'email'
		])
	</div>
</div>

<div class="row">
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => 'No. Telefon (Pejabat)',
			'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::OFFICE.']',
			'type' => 'number'
		])
	</div>
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => 'No. Telefon (Bimbit)',
			'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
			'type' => 'number'
		])
	</div>
	<div class="col-4">
		@include('components.forms.input', [
			'input_label' => 'No. Faks',
			'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::OTHER.']',
			'type' => 'number'
		])
	</div>
</div>

@include('components.forms.input', [
	'input_label' => 'Alamat Syarikat (Baris Pertama)',
	'name' => 'primary'
])

@include('components.forms.input', [
	'input_label' => 'Alamat Syarikat (Baris Kedua)',
	'name' => 'secondary'
])

<div class="row">
	<div class="col-6">
		@include('components.forms.input', [
			'input_label' => 'Poskod',
			'name' => 'postcode',
			'type' => 'number'
		])

		@include('components.forms.input', [
			'input_label' => 'No. Akaun Bank',
			'name' => 'account_bank_no',
			'type' => 'number'
		])
	</div>
	<div class="col-6">
		@include('components.forms.select', [
            'input_label' => __('Negeri'),
            'options' => $std_cd_state->pluck('description','description'),
            'name' => 'state',
            'id' => 'state',
        ])

		@include('components.forms.select', [
			'input_label' => 'Nama Bank',
			'name' => 'bank_name',
			'options' => $std_cd_bank->pluck('name','name'),
		])
	</div>
</div>