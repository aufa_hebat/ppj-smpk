<h4>Maklumat Syarikat</h4>
<br>
<div class="row">
    <div class="col-6">
        <div class="text-muted">Nama Syarikat</div>
        <p>{!! $data['company']->company_name !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">No. Pendaftaran SSM</div>
        <p style="text-transform: uppercase;">{!! $data['company']->ssm_no !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Alamat</div>
        @if(!empty($company->addresses[0]->primary))
            <p>
                {!! !empty($company->addresses[0]) ? $company->addresses[0]->primary : null !!}<br>{!! !empty($company->addresses[0]) ? $company->addresses[0]->secondary : null !!}<br>{!! !empty($company->addresses[0]) ? $company->addresses[0]->postcode : null !!}, {!! !empty($company->addresses[0]) ? $company->addresses[0]->state : null !!}, Malaysia
            </p>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">No. Telefon (Pejabat)</div>
        <p>{!! !empty($company->phones[0]) ? $company->phones[0]->phone_number : null !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">No. Telefon (Bimbit)</div>
        <p>{!! !empty($company->phones[1]) ? $company->phones[1]->phone_number : null !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">No. Telefon (Faks)</div>
        <p>{!! !empty($company->phones[2]) ? $company->phones[2]->phone_number : null !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">E-mel</div>
        <p>{!! $data['company']->email or '-' !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">No. Pendaftaran GST / SST</div>
        <p>{!! $data['company']->gst_registered_no or '-' !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Tarikh GST / SST Berkuatkuasa</div>
        <p>{!! !empty($company->gst_start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d',$company->gst_start_date)->format('d/m/Y') : null !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="text-muted">Sijil Pendaftaran GST / SST</div>
        <div class="input-group">
            <input id="subFile_status" type="text"  class="form-control" value="@foreach($gst_doc as $doc){{$doc->document_name}}@endforeach" readonly>
            @foreach($gst_doc as $db_gst)
                @if(!empty($db_gst))
                    <label class="input-group-text" for="downloadFile">
                        <a class="newwindowgst{{$db_gst->document_id}}" tt="link" href="#"><i class="fas fa-download"></i></a>
                    </label>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="text-muted">No. Akaun Bank</div>
        <p>{!! $data['company']->account_bank_no !!}</p>
    </div>
    <div class="col-6">
        <div class="text-muted">Nama Bank</div>
        <p>{!! $data['company']->bank_name !!}</p>
    </div>
</div>