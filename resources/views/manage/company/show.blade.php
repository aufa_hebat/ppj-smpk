@extends('layouts.admin')
@push('scripts')
    <script>
    	jQuery(document).ready(function($) {
            @foreach($gst_doc as $db)
                var urlgst = "/company/{{$db->document_name}}";

                $('.newwindowgst{{$db->document_id}}').click(function () {

                    var paramsgst = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowgst = window.open(urlgst, 'website', paramsgst);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach
            @foreach($ssm_doc as $ssm)
                var urlssm = "/company/{{$ssm->document_name}}";

                $('.newwindowssm{{$ssm->document_id}}').click(function () {

                    var paramsssm = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowssm = window.open(urlssm, 'website', paramsssm);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($cidb_doc as $cidb)
                var urlcidb = "/company/{{$cidb->document_name}}";

                $('.newwindowcidb{{$cidb->document_id}}').click(function () {

                    var paramscidb = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowcidb = window.open(urlcidb, 'website', paramscidb);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($bpku_doc as $bpku)
                var urlbpku = "/company/{{$bpku->document_name}}";

                $('.newwindowbpku{{$bpku->document_id}}').click(function () {

                    var paramsbpku = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowbpku = window.open(urlbpku, 'website', paramsbpku);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($spkk_doc as $spkk)
                var urlspkk = "/company/{{$spkk->document_name}}";

                $('.newwindowspkk{{$spkk->document_id}}').click(function () {

                    var paramsspkk = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowspkk = window.open(urlspkk, 'website', paramsspkk);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($mof_doc as $mof)
                var mof = "/company/{{$mof->document_name}}";

                $('.newwindowmof{{$mof->document_id}}').click(function () {

                    var paramsmof = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowmof = window.open(mof, 'website', paramsmof);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($bumi_doc as $bumi)
                var urlbumi = "/company/{{$bumi->document_name}}";

                $('.newwindowbumi{{$bumi->document_id}}').click(function () {

                    var paramsbumi = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowbumi = window.open(urlbumi, 'website', paramsbumi);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach

            @foreach($kdn_doc as $kdn)
                var urlkdn = "/company/{{$kdn->document_name}}";

                $('.newwindowkdn{{$kdn->document_id}}').click(function () {

                    var paramskdn = "menubar=no,titlebar=no,status=no,toolbar=no,location=no,resizable=no,scrollbars=no,height=1300,width=1600,left=1600,top=1300"

                    try {
                        var newwindowkdn = window.open(urlkdn, 'website', paramskdn);
                    } catch (err) {
                        $('#output').html($('#output').html() + "<br/> I clicked new Window " + $(this).attr('tt') + " " + err.message);
                    }
                 });
            @endforeach
    	});

        function getKategori2(obj) {
            var ssm = document.getElementById('ssm');
            var cidb = document.getElementById('cidb');
            var mof = document.getElementById('mof');
            var kdn = document.getElementById('kdn');

            if(obj.value == '1'){
                ssm.hidden = false;
                cidb.hidden = true;
                mof.hidden = true;
                kdn.hidden = true;
            }
            else if(obj.value == '2'){
                ssm.hidden = true;
                cidb.hidden = false;
                mof.hidden = true;
                kdn.hidden = true;
            }
            else if(obj.value == '3'){
                ssm.hidden = true;
                cidb.hidden = true;
                mof.hidden = false;
                kdn.hidden = true;
            }
            else if(obj.value == '4'){
                ssm.hidden = true;
                cidb.hidden = true;
                mof.hidden = true;
                kdn.hidden = false;
            }
        }
    </script>
@endpush
@section('content')
<div class="row">
    <div class="col-2 bg-transparent">
        <ul class="list-group list-group-transparent mb-0" id="document-tab-content" role="tablist">
            <li class="list-group-item">
                <a class="list-group-item-action active" data-toggle="tab" href="#company" role="tab" aria-controls="company" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Perincian') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#cert" role="tab" aria-controls="cert" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Sijil Kelayakan') }}
                </a>
            </li>
            <li class="list-group-item">
                <a class="list-group-item-action" data-toggle="tab" href="#owner" role="tab" aria-controls="owner" aria-selected="false">
                    @icon('fe fe-file')&nbsp;{{ __('Pemilik Syarikat') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-10">
        @component('components.card')
            @slot('card_body')
                @component('components.tab.container', ['id' => 'document'])
                    @slot('tabs')
                     	@component('components.tab.content', ['id' => 'company', 'active' => true])
                            @slot('content')
                                @include('manage.company.partials.shows.company', ['data' => $data])
                            @endslot
                        @endcomponent
                        @component('components.tab.content', ['id' => 'cert'])
                            @slot('content')
                                <div class="container">
                                    <div class="row" >
                                        <div class="col-sm-12">
                                            <div class="selectgroup w-100 grade-container">
                                                {{-- ssm --}}
                                                @if(!empty($ssm_active_date))
                                                    <label class="selectgroup-item" style="background: #5eba00">
                                                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black"><b>{{ __('SSM') }}</b></span>
                                                    </label>
                                                @elseif(!empty($ssm_inactive_date))
                                                    <label class="selectgroup-item" style="background: #f1c40f">
                                                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black"><b>{{ __('SSM') }}</b></span>
                                                    </label>
                                                @else
                                                    <label class="selectgroup-item">
                                                        <input type="radio" id="category1" name="category" value="1" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('SSM') }}</span>
                                                    </label>
                                                @endif

                                                {{-- cidb --}}
                                                @if(!empty($cidb_blacklist_date))
                                                    <label class="selectgroup-item bg-dark">
                                                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('CIDB') }}</span>
                                                    </label>
                                                @elseif(!empty($cidb_active_date))
                                                    <label class="selectgroup-item" style="background: #5eba00">
                                                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black">{{ __('CIDB') }}</span>
                                                    </label>
                                                @elseif(!empty($cidb_inactive_date))
                                                    <label class="selectgroup-item" style="background: #f1c40f">
                                                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black">{{ __('CIDB') }}</span>
                                                    </label>
                                                @else
                                                    <label class="selectgroup-item">
                                                        <input type="radio" id="category2" name="category" value="2" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('CIDB') }}</span>
                                                    </label>
                                                @endif

                                                {{-- mof --}}
                                                @if(!empty($mof_blacklist_date))
                                                    <label class="selectgroup-item bg-dark">
                                                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('MOF') }}</span>
                                                    </label>
                                                @elseif(!empty($mof_active_date))
                                                    <label class="selectgroup-item" style="background: #5eba00">
                                                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black">{{ __('MOF') }}</span>
                                                    </label>
                                                @elseif(!empty($mof_inactive_date))
                                                    <label class="selectgroup-item" style="background: #f1c40f">
                                                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black">{{ __('MOF') }}</span>
                                                    </label>
                                                @else
                                                    <label class="selectgroup-item">
                                                        <input type="radio" id="category3" name="category" value="3" class="selectgroup-input"  onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('MOF') }}</span>
                                                    </label>
                                                @endif

                                                {{-- kdn --}}
                                                @if(!empty($kdn_blacklist_date))
                                                    <label class="selectgroup-item bg-dark">
                                                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('KDN') }}</span>
                                                    </label>
                                                @elseif(!empty($kdn_active_date))
                                                    <label class="selectgroup-item" style="background: #5eba00">
                                                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black">{{ __('KDN') }}</span>
                                                    </label>
                                                @elseif(!empty($kdn_inactive_date))
                                                    <label class="selectgroup-item" style="background: #f1c40f">
                                                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button" style="color: black">{{ __('KDN') }}</span>
                                                    </label>
                                                @else
                                                    <label class="selectgroup-item">
                                                        <input type="radio" id="category4" name="category" value="4" class="selectgroup-input" onclick="getKategori2(this)">
                                                            <span class="selectgroup-button">{{ __('KDN') }}</span>
                                                    </label>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div id='ssm'>
                                        <h4>Pendaftaran Suruhanjaya Syarikat Malaysia (SSM)</h4>
                                        @include('manage.certificate.partials.forms.ssm')
                                    </div>
                                    <div id='cidb' hidden>
                                        <h4>Pendaftaran Lembaga Pembangunan Industri Pembinaan Malaysia (LPIPM/ CIDB)</h4>
                                        @include('manage.certificate.partials.forms.cidb')
                                        <br>
                                        <h4>Pendaftaran Bahagian Pembangunan Kontraktor Dan Usahawanan (BPKU)</h4>
                                        @include('manage.certificate.partials.forms.bpku')
                                        <br>
                                        <h4>Pendaftaran Sijil Perolehan Kerja Kerajaan (SPKK)</h4>
                                        @include('manage.certificate.partials.forms.spkk')
                                    </div>
                                    <div id='mof' hidden>
                                        <h4>Pendaftaran Kementerian Kewangan (MOF)</h4>
                                        @include('manage.certificate.partials.forms.mof')
                                        <br>
                                        <h4>Pendaftaran Sijil Akuan Pendaftaran Syarikat Bumiputera</h4>
                                        @include('manage.certificate.partials.forms.bumiputera')
                                    </div>
                                    <div id='kdn' hidden>
                                        <h4>Pendaftaran Kementerian Dalam Negeri (KDN)</h4>
                                        @include('manage.certificate.partials.forms.kdn')
                                    </div>
                                </div>
                            @endslot
                        @endcomponent
                        @component('components.tab.content', ['id' => 'owner'])
                            @slot('content')

                                @include('manage.owner.partials.scripts', [
                                    'table_id' => 'owner-management',
                                    'primary_key' => 'hashslug',
                                    'param' => 'hashslug=' . $hashslug,
                                    'routes' => [
                                        'show' => 'api.manage.owners.show',
                                        'store' => 'api.manage.owners.store',
                                        'update' => 'api.manage.owners.update',
                                        'destroy' => 'api.manage.owners.destroy',
                                    ],
                                    'columns' => [
                                        'name' => __('table.name'), 
                                        'ic_number' => __('table.ic_number'), 
                                        'email' => __('table.email'), 
                                        'created_at' => __('table.created_at'),
                                        'updated_at' => __('table.updated_at')
                                    ],
                                    'forms' => [
                                        'create' => 'owner-form',
                                        'edit' => 'owner-form',
                                    ], 
                                    'disabled' => [
                                        'email'
                                    ]
                                ])
                                                                
                                @component('components.card')
                                    @slot('card_body')
                                        <div class="row">
                                            <div class="col">
                                                @component('components.datatable', 
                                                    [
                                                        'table_id' => 'owner-management',
                                                        'param' => 'hashslug=' . $hashslug,
                                                        'route_name' => 'api.datatable.manage.owner',
                                                        'columns' => [
                                                            ['data' => 'name', 'title' => __('Nama Pemilik Syarikat'), 'defaultContent' => '-'], 
                                                            ['data' => 'ic_number', 'title' => __('No Kad Pengenalan'), 'defaultContent' => '-'],
                                                            ['data' => 'email', 'title' => __('E-Mel'), 'defaultContent' => '-'],
                                                            ['data' => 'type', 'title' => __('Sijil'), 'defaultContent' => '-'],
                                                            ['data' => null , 'name' => null, 'searchable' => false],
                                                        ],
                                                        'headers' => [
                                                            __('Nama Pemilik Syarikat'), __('No Kad Pengenalan'), __('E-mel'), __('Sijil'), __('table.action')
                                                        ],
                                                        'actions' => minify(view('manage.company.partials.shows.actions_owner')->render())
                                                    ]
                                                )
                                                @endcomponent 
                                            </div>
                                        </div>
                                    @endslot
                                @endcomponent

                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
                @slot('card_footer')
                    <div class="btn-group float-right">
                        <a href="{{ route('manage.companies.index') }}" 
                                class="btn btn-default border-primary">
                                {{ __('Kembali') }}
                        </a>
                    </div>
                @endslot
            @endslot
        @endcomponent
    </div>
</div>
@endsection