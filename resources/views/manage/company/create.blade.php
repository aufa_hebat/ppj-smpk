@extends('layouts.admin')

@push('scripts')
  @include('components.forms.assets.datetimepicker')
  <script>
    jQuery(document).ready(function($) {

      $(".allownumericonly").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
          event.preventDefault();
        }
      });

      $('#uploadFile').change(function(){
          $('#subFile').val($(this).val().split('\\').pop());
      });

      $(document).on('click', '.submit-action-btn', function(event) {
        event.preventDefault();
        var route_name = $(this).data('route');
        var form_id = $(this).data('form');
        // var data = $('#' + form_id).serialize();
        var form = document.forms[form_id];
        var data = new FormData(form);
        swal({
          title: '{!! __('Pendaftaran Syarikat') !!}',
          text: '{!! __('Adakah anda pasti?') !!}',
          type: 'warning',
          timer: 3000,
          showCancelButton: true,
          confirmButtonText: '{!! __('Ya') !!}',
          cancelButtonText: '{!! __('Batal') !!}'
        }).then((result) => {
          if (result.value) {
            axios.post(route(route_name), data).then(response => {
              redirect(route('manage.companies.edit', response.data.hashslug));                     
            });
          }
        });
      });
    });
  </script>
@endpush

@section('content')
  @include('manage.company.partials.styles')
  <div class="row">
    <div class="col-2 bg-transparent">
      <ul class="list-group list-group-transparent mb-0" id="company-tab-content" role="tablist">
        <li class="list-group-item">
          <a class="list-group-item-action active" data-toggle="tab" href="#company" role="tab" aria-controls="company" aria-selected="false">
            @icon('fe fe-file')&nbsp;{{ __('Perincian') }}
          </a>
        </li>
        <li class="list-group-item">
          <a class="disabled" style="color: grey">
            @icon('fe fe-file')&nbsp;{{ __('Sijil Kelayakan') }}
          </a>
        </li>
        <li class="list-group-item">
          <a class="disabled" style="color: grey">
            @icon('fe fe-file')&nbsp;{{ __('Pemilik Syarikat') }}
          </a>
        </li>
      </ul>
    </div>
    <div class="col-10"> 
      <div class="tab-content" id="company-tab-contents">
        <div class="tab-pane fade show active" id="company" role="tabpanel" aria-labelledby="company-tab">
          <form id="company_register" method="POST" files="true" enctype="multipart/form-data">
            @csrf
          	<div class="row">
              @component('components.card', ['card_classes' => 'col-12'])
                @slot('card_body')
                  <div class="row">
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'Nama Syarikat',
                        'id' => 'company_name',
                        'name' => 'company_name',
                        'required' => 'true'
                      ])
                    </div>
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'No Pendaftaran SSM',
                        'id' => 'ssm_no',
                        'name' => 'ssm_no',
                        'style' => 'text-transform:uppercase', 
                        'maxlength' => 20,
                        'required' => 'true'                                    
                      ])
                    </div>
                  </div>

                  @include('components.forms.input', [
                    'input_label' => 'Alamat Syarikat',
                    'id' => 'primary',
                    'name' => 'primary',
                    'placeholder' => 'Alamat Syarikat (Baris Pertama)',
                    'required' => 'true'
                  ])

                  <div style="margin-top: -10px;">
                    @include('components.forms.input', [
                      'input_label' => '',
                      'id' => 'secondary',
                      'name' => 'secondary',
                      'placeholder' => 'Alamat Syarikat (Baris Kedua)',
                      'required' => 'true'
                    ])
                  </div>

                  <div class="row">
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'Poskod',
                        'id' => 'postcode',
                        'name' => 'postcode',
                        'input_classes' => 'allownumericonly', 
                        'maxlength' => 5,
                        'required' => 'true'
                      ])
                    </div>
                    <div class="col-6">
                      @include('components.forms.select', [
                        'input_label' => 'Negeri',
                        'id' => 'state',
                        'name' => 'state',
                        'options' => $std_cd_state->pluck('description','description'),
                        'required' => 'true'
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'No. Telefon (Pejabat)',
                        'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::OFFICE.']',
                        'input_classes' => 'allownumericonly', 
                        'maxlength' => 15,
                        'required' => 'true'
                      ])
                    </div>
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'No. Telefon (Bimbit)',
                        'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::MOBILE.']',
                        'input_classes' => 'allownumericonly', 
                        'maxlength' => 15,
                        'required' => 'true'
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'No. Faks',
                        'name' => 'phone_number['.\CleaniqueCoders\Profile\Models\PhoneType::OTHER.']',
                        'input_classes' => 'allownumericonly', 
                        'maxlength' => 15,
                        'required' => 'true'
                      ])
                    </div>
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'E-Mel',
                        'id' => 'email',
                        'name' => 'email',
                        'type' => 'email',
                        'required' => 'true'
                      ])
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'No. Pendaftaran GST / SST',
                        'id' => 'gst_registered_no',
                        'name' => 'gst_registered_no',
                      ])
                    </div>
                    <div class="col-6" style="margin-top: 8px;">
                      @include('components.forms.datetimepicker', [
                        'input_label' => __('Tarikh GST / SST Berkuatkuasa'),
                        'id' => 'gst_start_date',
                        'name' => 'gst_start_date',
                        'config' => [
                            'format' => config('datetime.display.date'),
                        ]
                      ])
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <label>Sijil Pendaftaran GST / SST</label><br>
                      <div class="input-group">
                        <input id="subFile" type="text" class="form-control" readonly>
                        <label class="input-group-text" for="uploadFile"><i class="fe fe-upload" ></i></label>
                        <input type="file" class="form-control" id="uploadFile" name="gst" style="display: none" accept=".pdf,.doc,.docx,.txt,.xls,.xlsx,.jpeg,.png,.jpg">
                      </div>
                      <small class="float-right"><b>Format : .pdf, .doc, .docx, .txt, .jpeg, .jpg, .png &nbsp;&nbsp;&nbsp; Saiz Lampiran : 2MB</b></small>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6">
                      @include('components.forms.select', [
                          'input_label' => 'Nama Bank',
                          'id' => 'bank_name',
                          'name' => 'bank_name',
                          'options' => $std_cd_bank->pluck('name','name'),
                          
                      ])
                    </div>
                    <div class="col-6">
                      @include('components.forms.input', [
                        'input_label' => 'No. Akaun Bank',
                        'id' => 'account_bank_no',
                        'name' => 'account_bank_no',
                        'input_classes' => 'allownumericonly', 
                      ])
                    </div>
                  </div>
                  <div class="btn-group float-right">
                    <button type="submit" class="btn btn-primary float-right submit-action-btn"
                      data-route="manage.companies.store"
                      data-form="company_register">
                      @icon('fe fe-save') {{ __('Simpan') }}
                    </button>
                  </div>
                @endslot
              @endcomponent
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection