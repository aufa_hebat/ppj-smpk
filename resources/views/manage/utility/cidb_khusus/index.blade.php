@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.cidb_khusus.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-cidb_khusus" id="add-cidb_khusus"
        				@include('components.tooltip', ['tooltip' => __('CIDB:Kategori Baru')])
        				data-toggle="modal" data-target="#add-cidb_khusus-modal">
        				<i class="fe fe-plus"></i>
        				CIDB:Kategori Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-cidb_khusus',
							'route_name' => 'api.datatable.utility.cidb_khusus',
							'columns' => [
                                ['data' => 'category', 'title' => __('Kategori'), 'defaultContent' => '-'],
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
                                ['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('Kategori'), __('Kod'), __('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.cidb_khusus.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-cidb_khusus-modal',
            'tooltip' => __('Tambah CIDB:Kategori'),
            'modal_title' => __('Tambah CIDB:Kategori'),
        ])
        @slot('modal_body')
            <form id="add-cidb_khusus-form-store">
                <div class="row">
                    <div class="col-2">
                        @include('components.forms.select', [
                            'input_label' => __('Kategori'),
                            'options' => cidb_codes('category')->pluck('code','code'),
                            'name' => 'category',
                        ])
                    </div>
                    <div class="col-2">
                        @include('components.forms.input', [
                            'input_label'   => 'Kod',
                            'name'          => 'code',
                        ])
                    </div>
                    <div class="col-8">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-cidb_khusus', function(event) {
                        });

                        $(document).on('click', '.submit-add-cidb_khusus-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-cidb_khusus-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('CIDB:Kategori') !!}', response.data.message, 'success');
                            
                                $('#add-cidb_khusus-modal').modal('hide');

                                $('#utility-cidb_khusus').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-cidb_khusus-action-btn" 
                data-route="api.utility.cidb_khusus.store"
                data-form="add-cidb_khusus-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
            'id' => 'edit-cidb_khusus-modal',
            'tooltip' => __('Kemaskini CIDB:Kategori'),
            'modal_title' => __('Kemaskini CIDB:Kategori'),
        ])
        @slot('modal_body')
            <form id="edit-cidb_khusus-form-store">
                <div class="row">
                    <div class="col-2">  
                        @include('components.forms.select', [
                            'input_label' => __('Kategori'),
                            'options' => cidb_codes('category')->pluck('code','code'),
                            'name' => 'category',
                            'id' => 'category',
                        ])           
                    </div>
                    <div class="col-2">                 
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
                            'id' => 'code'
                        ])             
                    </div>
                    <div class="col-8">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                    @include('components.forms.hidden', [
                        'name' => 'id',
                        'id' => 'id',
                        'value' => '',
                    ])                     
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-cidb_khusus-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-cidb_khusus-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('CIDB:Kategori') !!}', response.data.message, 'success');
                            
                                $('#edit-cidb_khusus-modal').modal('hide');

                                $('#utility-cidb_khusus').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-cidb_khusus-action-btn" 
                data-route="api.utility.cidb_khusus.update"
                data-form="edit-cidb_khusus-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection