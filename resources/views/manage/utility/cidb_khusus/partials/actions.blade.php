<div class="text-center">
	<div class="item-action dropdown">
	  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
	  	<i class="fe fe-more-vertical"></i>
	  </a>
	  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
	  	style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
	  	{{ $prepend_action or '' }}
		<a class="dropdown-item edit-action-btn" style="cursor: pointer;"
		        data-toggle="modal" data-target="#edit-cidb_khusus-modal"
		        data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
		        data-{{ 'name' }}="' + data.{{ 'name' }} + '"
		        data-{{ 'code' }}="' + data.{{ 'code' }} + '"
		        data-{{ 'category' }}="' + data.{{ 'category' }} + '">
		    <i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
		</a>
                
		{{ $append_action or '' }}
		<a class="dropdown-item destroy-action-btn" style="cursor: pointer;" data-{{ 'id' }}="' + data.{{ 'id' }} + '" >
		    <i class="fe fe-trash text-danger"></i> {{ __('Hapus') }}
		</a>
		{{ $append_footer_action or '' }}
	  </div>
	</div>
</div>
