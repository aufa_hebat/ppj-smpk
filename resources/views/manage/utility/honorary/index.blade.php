@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.honorary.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-honorary" id="add-honorary"
        				@include('components.tooltip', ['tooltip' => __('Gelaran Baru')])
        				data-toggle="modal" data-target="#add-honorary-modal">
        				<i class="fe fe-plus"></i>
        				Gelaran Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-honorary',
							'route_name' => 'api.datatable.utility.honorary',
							'columns' => [
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.honorary.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-honorary-modal',
        'tooltip' => __('Tambah Gelaran'),
        'modal_title' => __('Tambah Gelaran'),
    ])
        @slot('modal_body')
            <form id="add-honorary-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-honorary', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-honorary-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-honorary-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Gelaran') !!}', response.data.message, 'success');
					
						        $('#add-honorary-modal').modal('hide');

						        $('#utility-honorary').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-honorary-action-btn" 
			    data-route="api.utility.honorary.store"
			    data-form="add-honorary-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-honorary-modal',
        'tooltip' => __('Kemaskini Gelaran'),
        'modal_title' => __('Kemaskini Gelaran'),
    ])
        @slot('modal_body')
            <form id="edit-honorary-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                    
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-honorary-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-honorary-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Gelaran') !!}', response.data.message, 'success');
					
						        $('#edit-honorary-modal').modal('hide');

						        $('#utility-honorary').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-honorary-action-btn" 
			    data-route="api.utility.honorary.update"
			    data-form="edit-honorary-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection