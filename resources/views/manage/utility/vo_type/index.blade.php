@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.vo_type.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-vo_type" id="add-vo_type"
        				@include('components.tooltip', ['tooltip' => __('Jenis Perubahan Kerja Baru')])
        				data-toggle="modal" data-target="#add-vo_type-modal">
        				<i class="fe fe-plus"></i>
        				Jenis Perubahan Kerja Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-vo_type',
							'route_name' => 'api.datatable.utility.vo_type',
							'columns' => [
                                ['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.vo_type.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-vo_type-modal',
            'tooltip' => __('Tambah Jenis Perubahan Kerja'),
            'modal_title' => __('Tambah Jenis Perubahan Kerja'),
        ])
        @slot('modal_body')
            <form id="add-vo_type-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                    <div class="col-6"></div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-vo_type', function(event) {
                        });

                        $(document).on('click', '.submit-add-vo_type-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-vo_type-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('Jenis Perubahan Kerja') !!}', response.data.message, 'success');
                            
                                $('#add-vo_type-modal').modal('hide');

                                $('#utility-vo_type').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-vo_type-action-btn" 
                data-route="api.utility.vo_type.store"
                data-form="add-vo_type-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
            'id' => 'edit-vo_type-modal',
            'tooltip' => __('Kemaskini Jenis Perubahan Kerja'),
            'modal_title' => __('Kemaskini Jenis Perubahan Kerja'),
        ])
        @slot('modal_body')
            <form id="edit-vo_type-form-store">
                <div class="row">
                    <div class="col-6">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
                            'id' => 'id',
                            'value'	=> '',
                        ])               
                    </div>                    
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-vo_type-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-vo_type-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('Jenis Perubahan Kerja') !!}', response.data.message, 'success');
                            
                                $('#edit-vo_type-modal').modal('hide');

                                $('#utility-vo_type').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-vo_type-action-btn" 
                data-route="api.utility.vo_type.update"
                data-form="edit-vo_type-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection