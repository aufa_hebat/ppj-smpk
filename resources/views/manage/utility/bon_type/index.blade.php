@extends('layouts.admin')

@push('scripts')
<script type="text/javascript">


</script>

@endpush

@section('content')

	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.bon_type.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-bon_type" id="add-bon_type"
        				@include('components.tooltip', ['tooltip' => __('Jenis Bon Baru')])
        				data-toggle="modal" data-target="#add-bon_type-modal">
        				<i class="fe fe-plus"></i>
        				Jenis Bon Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-bon_type',
							'route_name' => 'api.datatable.utility.bon_type',
							'columns' => [
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.bon_type.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
        'id' => 'add-bon_type-modal',
        'tooltip' => __('Tambah Jenis Bon'),
        'modal_title' => __('Tambah Jenis Bon'),
    ])
    @slot('modal_body')
        <form id="add-bon_type-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label'   => 'Kod',
                    'name'          => 'code',
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label'   => 'Nama',
                    'name'          => 'name',
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.add-bon_type', function(event) {
					// $('#select2-business_area-container').html("");
					// $('#select2-cost_center-container').html("");
					// $('#select2-fund-container').html("");
					// $('#select2-functional_area-container').html("");
					// $('#select2-funded_programme-container').html("");
					// $('#select2-gl_account-container').html("");
	            });

				$(document).on('click', '.submit-add-bon_type-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#add-bon_type-form-store').serialize();
	                var route_name = $(this).data('route');

	                axios.post(route(route_name), data).then(response => {
	                    swal('{!! __('Jenis Bon') !!}', response.data.message, 'success');
					
						$('#add-bon_type-modal').modal('hide');

						$('#utility-bon_type').DataTable().ajax.reload();
	                });
					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-add-bon_type-action-btn" 
			data-route="api.utility.bon_type.store"
			data-form="add-bon_type-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@component('components.modals.base', [
        'id' => 'edit-bon_type-modal',
        'tooltip' => __('Kemaskini Jenis Bon'),
        'modal_title' => __('Kemaskini Jenis Bon'),
    ])
    @slot('modal_body')
        <form id="edit-bon_type-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.hidden', [
                    'name' => 'id',
					'id' => 'id',
					'value'	=> '',
                ])
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'code',
					'id' => 'code'
                ])                
            </div>
            <div class="col-6">	            
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name',
					'id' => 'name'
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                @include('components.forms.switch', [
                    'id' => 'is_active', 
                    'name' => 'is_active', 
                    'label' => 'Status',
                    'checked' => ''
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.submit-edit-bon_type-action-btn', function(event) {
	                event.preventDefault();
					var id = $('#id').val();

	                var data = $('#edit-bon_type-form-store').serialize();
	                var route_name = $(this).data('route');

					console.log(data);

	                axios.put(route(route_name, id), data).then(response => {
	                    swal('{!! __('Jenis Bon') !!}', response.data.message, 'success');
					
						$('#edit-bon_type-modal').modal('hide');

						$('#utility-bon_type').DataTable().ajax.reload();
	                });					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-edit-bon_type-action-btn" 
			data-route="api.utility.bon_type.update"
			data-form="edit-bon_type-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent

@endsection