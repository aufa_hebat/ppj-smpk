@extends('layouts.admin')

@push('scripts')
<script type="text/javascript">


</script>

@endpush

@section('content')


	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.authorities.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-authority" id="add-authority"
        				@include('components.tooltip', ['tooltip' => __('Authority Baru')])
        				data-toggle="modal" data-target="#add-authority-modal">
        				<i class="fe fe-plus"></i>
        				Authority Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-authorities',
							'route_name' => 'api.datatable.utility.authorities',
							'columns' => [
								['data' => 'code', 'title' => __('kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('kod'), __('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.authorities.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
        'id' => 'add-authority-modal',
        'tooltip' => __('Tambah Authority'),
        'modal_title' => __('Tambah Authority'),
    ])
    @slot('modal_body')
        <form id="add-authority-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'code'
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.add-authority', function(event) {
					// $('#select2-business_area-container').html("");
					// $('#select2-cost_center-container').html("");
					// $('#select2-fund-container').html("");
					// $('#select2-functional_area-container').html("");
					// $('#select2-funded_programme-container').html("");
					// $('#select2-gl_account-container').html("");
	            });

				$(document).on('click', '.submit-add-authority-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#add-authority-form-store').serialize();
	                var route_name = $(this).data('route');

	                axios.post(route(route_name), data).then(response => {
	                    swal('{!! __('Kod Authority') !!}', response.data.message, 'success');
					
						$('#add-authority-modal').modal('hide');

						$('#utility-authorities').DataTable().ajax.reload();
	                });
					// location.reload();
					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-add-authority-action-btn" 
			data-route="api.utility.authorities.store"
			data-form="add-authority-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@component('components.modals.base', [
        'id' => 'edit-authority-modal',
        'tooltip' => __('Kemaskini Authority'),
        'modal_title' => __('Kemaskini Authority'),
    ])
    @slot('modal_body')
        <form id="edit-authority-form-store">
        <div class="row">
            <div class="col-6">
				@include('components.forms.hidden', [
                    'name' => 'id',
					'id' => 'id',
					'value'	=> '',
                ])			
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'code',
					'id' => 'code'
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name',
					'id' => 'name'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.submit-edit-authority-action-btn', function(event) {
	                event.preventDefault();
					var id = $('#id').val();

	                var data = $('#edit-authority-form-store').serialize();
	                var route_name = $(this).data('route');

					console.log(data);

	                axios.put(route(route_name, id), data).then(response => {
	                    swal('{!! __('Kod Authority') !!}', response.data.message, 'success');
					
						$('#edit-authority-modal').modal('hide');

						$('#utility-authorities').DataTable().ajax.reload();
	                });					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-edit-authority-action-btn" 
			data-route="api.utility.authorities.update"
			data-form="edit-authority-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@endsection