<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-authority-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'code' }}="' + data.{{ 'code' }} + '"
    data-{{ 'name' }}="' + data.{{ 'name' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
</a>
