@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.division.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-division" id="add-division"
        				@include('components.tooltip', ['tooltip' => __('Bahagian Baru')])
        				data-toggle="modal" data-target="#add-division-modal">
        				<i class="fe fe-plus"></i>
        				Bahagian Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-division',
							'route_name' => 'api.datatable.utility.division',
							'columns' => [
                                ['data' => 'category', 'title' => __('Kategori'), 'defaultContent' => '-'],
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],                                
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('Kategori'), __('Kod'), __('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.division.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-division-modal',
        'tooltip' => __('Tambah Bahagian'),
        'modal_title' => __('Tambah Bahagian'),
    ])
        @slot('modal_body')
            <form id="add-division-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.select', [
                            'input_label' => 'Jabatan',
                            'name' => 'category',
                            'options' => $department->pluck('name','code'),
                        ])
                    </div>
                </div>             
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>               
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-division', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-division-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-division-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Jabatan') !!}', response.data.message, 'success');
					
						        $('#add-division-modal').modal('hide');

						        $('#utility-division').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-division-action-btn" 
			    data-route="api.utility.division.store"
			    data-form="add-division-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-division-modal',
        'tooltip' => __('Kemaskini Jabatan'),
        'modal_title' => __('Kemaskini Jabatan'),
    ])
        @slot('modal_body')
            <form id="edit-division-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])                    
                        @include('components.forms.select', [
                            'input_label' => 'Jabatan',
                            'name' => 'category',
                            'id'    => 'category',
                            'options' => $department->pluck('name','code'),
                        ])
                    </div>
                </div>             
                <div class="row">
                    <div class="col-6">
			
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
					        'id' => 'code'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-division-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-division-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Jabatan') !!}', response.data.message, 'success');
					
						        $('#edit-division-modal').modal('hide');

						        $('#utility-division').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-division-action-btn" 
			    data-route="api.utility.division.update"
			    data-form="edit-division-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection