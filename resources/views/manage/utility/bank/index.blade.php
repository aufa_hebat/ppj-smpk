@extends('layouts.admin')

@push('scripts')
<script type="text/javascript">


</script>

@endpush

@section('content')

	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.bank.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-bank" id="add-bank"
        				@include('components.tooltip', ['tooltip' => __('Bank Baru')])
        				data-toggle="modal" data-target="#add-bank-modal">
        				<i class="fe fe-plus"></i>
        				Bank Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-bank',
							'route_name' => 'api.datatable.utility.bank',
							'columns' => [
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.bank.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
        'id' => 'add-bank-modal',
        'tooltip' => __('Tambah Bank'),
        'modal_title' => __('Tambah Bank'),
    ])
    @slot('modal_body')
        <form id="add-bank-form-store">
        <div class="row">
            <div class="col-12">
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.add-bank', function(event) {
					// $('#select2-business_area-container').html("");
					// $('#select2-cost_center-container').html("");
					// $('#select2-fund-container').html("");
					// $('#select2-functional_area-container').html("");
					// $('#select2-funded_programme-container').html("");
					// $('#select2-gl_account-container').html("");
	            });

				$(document).on('click', '.submit-add-bank-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#add-bank-form-store').serialize();
	                var route_name = $(this).data('route');

	                axios.post(route(route_name), data).then(response => {
	                    swal('{!! __('Senarai Bank') !!}', response.data.message, 'success');
					
						$('#add-bank-modal').modal('hide');

						$('#utility-bank').DataTable().ajax.reload();
	                });
					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-add-bank-action-btn" 
			data-route="api.utility.bank.store"
			data-form="add-bank-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@component('components.modals.base', [
        'id' => 'edit-bank-modal',
        'tooltip' => __('Kemaskini Bank'),
        'modal_title' => __('Kemaskini Bank'),
    ])
    @slot('modal_body')
        <form id="edit-bank-form-store">
        <div class="row">
            <div class="col-12">
                @include('components.forms.hidden', [
                    'name' => 'id',
					'id' => 'id',
					'value'	=> '',
                ])	            
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name',
					'id' => 'name'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.submit-edit-bank-action-btn', function(event) {
	                event.preventDefault();
					var id = $('#id').val();

	                var data = $('#edit-bank-form-store').serialize();
	                var route_name = $(this).data('route');

					console.log(data);

	                axios.put(route(route_name, id), data).then(response => {
	                    swal('{!! __('Senarai Bank') !!}', response.data.message, 'success');
					
						$('#edit-bank-modal').modal('hide');

						$('#utility-bank').DataTable().ajax.reload();
	                });					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-edit-bank-action-btn" 
			data-route="api.utility.bank.update"
			data-form="edit-bank-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@endsection