@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.grade.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-grade" id="add-grade"
        				@include('components.tooltip', ['tooltip' => __('Gred Baru')])
        				data-toggle="modal" data-target="#add-grade-modal">
        				<i class="fe fe-plus"></i>
        				Gred Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-grade',
							'route_name' => 'api.datatable.utility.grade',
							'columns' => [
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.grade.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-grade-modal',
        'tooltip' => __('Tambah Gred'),
        'modal_title' => __('Tambah Gred'),
    ])
        @slot('modal_body')
            <form id="add-grade-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-grade', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-grade-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-grade-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Gred') !!}', response.data.message, 'success');
					
						        $('#add-grade-modal').modal('hide');

						        $('#utility-grade').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-grade-action-btn" 
			    data-route="api.utility.grade.store"
			    data-form="add-grade-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-grade-modal',
        'tooltip' => __('Kemaskini Gred'),
        'modal_title' => __('Kemaskini Gred'),
    ])
        @slot('modal_body')
            <form id="edit-grade-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                    
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-grade-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-grade-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Gred') !!}', response.data.message, 'success');
					
						        $('#edit-grade-modal').modal('hide');

						        $('#utility-grade').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-grade-action-btn" 
			    data-route="api.utility.grade.update"
			    data-form="edit-grade-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection