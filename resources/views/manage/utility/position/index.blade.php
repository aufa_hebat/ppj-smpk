@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.position.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-position" id="add-position"
        				@include('components.tooltip', ['tooltip' => __('Jawatan Baru')])
        				data-toggle="modal" data-target="#add-position-modal">
        				<i class="fe fe-plus"></i>
        				Jawatan Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-position',
							'route_name' => 'api.datatable.utility.position',
							'columns' => [
                                ['data' => 'grade', 'title' => __('Gred'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Gred'), __('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.position.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-position-modal',
        'tooltip' => __('Tambah Jawatan'),
        'modal_title' => __('Tambah Jawatan'),
    ])
        @slot('modal_body')
            <form id="add-position-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Gred',
                            'name' => 'grade'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-position', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-position-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-position-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Jawatan') !!}', response.data.message, 'success');
					
						        $('#add-position-modal').modal('hide');

						        $('#utility-position').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-position-action-btn" 
			    data-route="api.utility.position.store"
			    data-form="add-position-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-position-modal',
        'tooltip' => __('Kemaskini Jawatan'),
        'modal_title' => __('Kemaskini Jawatan'),
    ])
        @slot('modal_body')
            <form id="edit-position-form-store">
                <div class="row">
                    <div class="col-6">
				        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])			
                        @include('components.forms.input', [
                            'input_label' => 'Gred',
                            'name' => 'grade',
					        'id' => 'grade'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-position-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-position-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Jawatan') !!}', response.data.message, 'success');
					
						        $('#edit-position-modal').modal('hide');

						        $('#utility-position').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-position-action-btn" 
			    data-route="api.utility.position.update"
			    data-form="edit-position-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection