<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-position-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'grade' }}="' + data.{{ 'grade' }} + '"
    data-{{ 'name' }}="' + data.{{ 'name' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
</a>
