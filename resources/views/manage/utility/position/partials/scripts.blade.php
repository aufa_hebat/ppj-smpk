@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var grade = $(this).data('grade');
                var name = $(this).data('name');

                $('#id').val(id);
                $('#grade').val(grade);
                $('#name').val(name);

			});
        });
    </script>
@endpush
