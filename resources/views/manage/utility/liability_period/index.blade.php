@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.liability_period.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-liability_period" id="add-liability_period"
        				@include('components.tooltip', ['tooltip' => __('Tempoh Liabiliti Baru')])
        				data-toggle="modal" data-target="#add-liability_period-modal">
        				<i class="fe fe-plus"></i>
        				Liabiliti Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-liability_period',
							'route_name' => 'api.datatable.utility.liability_period',
							'columns' => [
                                ['data' => 'value', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],                                
                                ['data' => 'description', 'title' => __('Keterangan'), 'defaultContent' => '-'],
                                ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Kod'), __('Nama'), __('Keterangan'), __('table.action')
							],
							'actions' => minify(view('manage.utility.liability_period.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-liability_period-modal',
        'tooltip' => __('Tambah Tempoh Liabiliti'),
        'modal_title' => __('Tambah Liabiliti'),
    ])
        @slot('modal_body')
            <form id="add-liability_period-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'value'
                        ])
                    </div>                
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div> 
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Keterangan',
                            'name' => 'description'
                        ])
                    </div>
                </div>             
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-liability_period', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-liability_period-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-liability_period-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Tempoh Liabiliti') !!}', response.data.message, 'success');
					
						        $('#add-liability_period-modal').modal('hide');

						        $('#utility-liability_period').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-liability_period-action-btn" 
			    data-route="api.utility.liability_period.store"
			    data-form="add-liability_period-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-liability_period-modal',
        'tooltip' => __('Kemaskini Tempoh Liabiliti'),
        'modal_title' => __('Kemaskini Tempoh LIabiliti'),
    ])
        @slot('modal_body')
            <form id="edit-liability_period-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'value',
					        'id' => 'value'
                        ])

                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                         
                    </div>
                    <div class="col-6">                   
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Keterangan',
                            'name' => 'description',
					        'id' => 'description'
                        ])
                    </div>
                </div>                                
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-liability_period-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-liability_period-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Tempoh Liabiliti') !!}', response.data.message, 'success');
					
						        $('#edit-liability_period-modal').modal('hide');

						        $('#utility-liability_period').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-liability_period-action-btn" 
			    data-route="api.utility.liability_period.update"
			    data-form="edit-liability_period-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection