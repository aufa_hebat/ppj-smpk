@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var name = $(this).data('name');
                var value = $(this).data('value');
                var description = $(this).data('description');

                $('#id').val(id);
                $('#name').val(name);
                $('#value').val(value);
                $('#description').val(description);

			});
        });
    </script>
@endpush
