<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-liability_period-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'name' }}="' + data.{{ 'name' }} + '"
    data-{{ 'value' }}="' + data.{{ 'value' }} + '"
    data-{{ 'description' }}="' + data.{{ 'description' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
</a>
