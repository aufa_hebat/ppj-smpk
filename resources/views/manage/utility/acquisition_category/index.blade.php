@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.acquisition_category.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-acquisition_category" id="add-acquisition_category"
        				@include('components.tooltip', ['tooltip' => __('Kategori Perolehan Baru')])
        				data-toggle="modal" data-target="#add-acquisition_category-modal">
        				<i class="fe fe-plus"></i>
        				Kategori Perolehan Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-acquisition_category',
							'route_name' => 'api.datatable.utility.acquisition_category',
							'columns' => [
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.acquisition_category.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-acquisition_category-modal',
            'tooltip' => __('Tambah Kategori Perolehan'),
            'modal_title' => __('Tambah Kategori Perolehan'),
        ])
        @slot('modal_body')
            <form id="add-acquisition_category-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                    <div class="col-6"></div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-acquisition_category', function(event) {
                        });

                        $(document).on('click', '.submit-add-acquisition_category-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-acquisition_category-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('Kategori Perolehan') !!}', response.data.message, 'success');
                            
                                $('#add-acquisition_category-modal').modal('hide');

                                $('#utility-acquisition_category').DataTable().ajax.reload();
                            });
                            
                        });

                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-acquisition_category-action-btn" 
                data-route="api.utility.acquisition_category.store"
                data-form="add-acquisition_category-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

    @component('components.modals.base', [
            'id' => 'edit-acquisition_category-modal',
            'tooltip' => __('Kemaskini Kategori Perolehan'),
            'modal_title' => __('Kemaskini Kategori Perolehan'),
        ])
        @slot('modal_body')
            <form id="edit-acquisition_category-form-store">
                <div class="row">
                    <div class="col-6">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
                            'id' => 'id',
                            'value'	=> '',
                        ])             
                    </div>            
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-acquisition_category-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-acquisition_category-form-store').serialize();
                            var route_name = $(this).data('route');

                            console.log(data);

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('Kategori Perolehan') !!}', response.data.message, 'success');
                            
                                $('#edit-acquisition_category-modal').modal('hide');

                                $('#utility-acquisition_category').DataTable().ajax.reload();
                            });					
                        });

                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-acquisition_category-action-btn" 
                data-route="api.utility.acquisition_category.update"
                data-form="edit-acquisition_category-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection