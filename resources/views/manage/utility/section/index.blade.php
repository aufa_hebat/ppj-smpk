@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.section.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-section" id="add-section"
        				@include('components.tooltip', ['tooltip' => __('Seksyen Baru')])
        				data-toggle="modal" data-target="#add-section-modal">
        				<i class="fe fe-plus"></i>
        				Seksyen Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-section',
							'route_name' => 'api.datatable.utility.section',
							'columns' => [
                                ['data' => 'category', 'title' => __('Bahagian'), 'defaultContent' => '-'],
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],                                
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('Bahagian'), __('Kod'), __('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.section.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-section-modal',
        'tooltip' => __('Tambah Seksyen'),
        'modal_title' => __('Tambah Seksyen'),
    ])
        @slot('modal_body')
            <form id="add-section-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.select', [
                            'input_label' => 'Seksyen',
                            'name' => 'category',
                            'options' => $division->pluck('name','code'),
                        ])
                    </div>
                </div>             
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>               
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-section', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-section-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-section-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Seksyen') !!}', response.data.message, 'success');
					
						        $('#add-section-modal').modal('hide');

						        $('#utility-section').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-section-action-btn" 
			    data-route="api.utility.section.store"
			    data-form="add-section-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-section-modal',
        'tooltip' => __('Kemaskini Seksyen'),
        'modal_title' => __('Kemaskini Seksyen'),
    ])
        @slot('modal_body')
            <form id="edit-section-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])                    
                        @include('components.forms.select', [
                            'input_label' => 'Bahagian',
                            'name' => 'category',
                            'id'    => 'category',
                            'options' => $division->pluck('name','code'),
                        ])
                    </div>
                </div>             
                <div class="row">
                    <div class="col-6">
			
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
					        'id' => 'code'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-section-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-section-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Seksyen') !!}', response.data.message, 'success');
					
						        $('#edit-section-modal').modal('hide');

						        $('#utility-section').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-section-action-btn" 
			    data-route="api.utility.section.update"
			    data-form="edit-section-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection