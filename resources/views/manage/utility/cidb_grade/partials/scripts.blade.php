@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $(document).on('click', '.edit-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var dataname = $(this).data('name');
            var code = $(this).data('code');

            var name = dataname.split('- ');

            $('#id').val(id);
            $('#name').val(name[1]);
            $('#code').val(code);
        });

        $(document).on('click', '.destroy-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            swal({
                title: '{!! __('Amaran') !!}',
                text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '{!! __('Ya') !!}',
                cancelButtonText: '{!! __('Batal') !!}'
            }).then((result) => {
                if (result.value) {
                axios.delete(route('api.utility.cidb_grade.destroy', id))
                    .then(response => {
                        $('#utility-cidb_grade').DataTable().ajax.reload();
                    })
              }

            });
        });
    });
</script>
@endpush
