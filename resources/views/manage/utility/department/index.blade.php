@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.department.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-department" id="add-department"
        				@include('components.tooltip', ['tooltip' => __('Jabatan Baru')])
        				data-toggle="modal" data-target="#add-department-modal">
        				<i class="fe fe-plus"></i>
        				Jabatan Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-department',
							'route_name' => 'api.datatable.utility.department',
							'columns' => [
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                                ['data' => 'color', 'title' => __('Warna'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Kod'), __('Nama'), __('Warna'), __('table.action')
							],
							'actions' => minify(view('manage.utility.department.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-department-modal',
        'tooltip' => __('Tambah Jabatan'),
        'modal_title' => __('Tambah Jabatan'),
    ])
        @slot('modal_body')
            <form id="add-department-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Warna',
                            'name' => 'color',
                        ])
                    </div>
                </div>                
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-department', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-department-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-department-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Jabatan') !!}', response.data.message, 'success');
					
						        $('#add-department-modal').modal('hide');

						        $('#utility-department').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-department-action-btn" 
			    data-route="api.utility.department.store"
			    data-form="add-department-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-department-modal',
        'tooltip' => __('Kemaskini Jabatan'),
        'modal_title' => __('Kemaskini Jabatan'),
    ])
        @slot('modal_body')
            <form id="edit-department-form-store">
                <div class="row">
                    <div class="col-6">
				        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])			
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
					        'id' => 'code'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Warna',
                            'name' => 'color',
                            'id' => 'color'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-department-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-department-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Jabatan') !!}', response.data.message, 'success');
					
						        $('#edit-department-modal').modal('hide');

						        $('#utility-department').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-department-action-btn" 
			    data-route="api.utility.department.update"
			    data-form="edit-department-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection