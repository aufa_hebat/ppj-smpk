@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var code = $(this).data('code');
                var name = $(this).data('name');
                var color = $(this).data('color');

                $('#id').val(id);
                $('#code').val(code);
                $('#name').val(name);
                $('#color').val(color);

			});
        });
    </script>
@endpush
