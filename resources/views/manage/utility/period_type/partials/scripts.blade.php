@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var value = $(this).data('value');
                var name = $(this).data('name');

                $('#id').val(id);
                $('#value').val(value);
                $('#name').val(name);
			});
        });
    </script>
@endpush
