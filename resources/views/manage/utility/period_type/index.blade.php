@extends('layouts.admin')

@section('content')


	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.period_type.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-period_type" id="add-period_type"
        				@include('components.tooltip', ['tooltip' => __('Jenis Tempoh Baru')])
        				data-toggle="modal" data-target="#add-period_type-modal">
        				<i class="fe fe-plus"></i>
        				Jenis Tempoh Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-period_type',
							'route_name' => 'api.datatable.utility.period_type',
							'columns' => [
								['data' => 'value', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('table.name'), __('table.email'), __('table.action')
							],
							'actions' => minify(view('manage.utility.period_type.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
        'id' => 'add-period_type-modal',
        'tooltip' => __('Tambah Jenis Tempoh'),
        'modal_title' => __('Tambah Jenis Tempoh'),
    ])
    @slot('modal_body')
        <form id="add-period_type-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'value'
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.add-period_type', function(event) {
					// $('#select2-gl_account-container').html("");
	            });

				$(document).on('click', '.submit-add-period_type-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#add-period_type-form-store').serialize();
	                var route_name = $(this).data('route');

	                axios.post(route(route_name), data).then(response => {
	                    swal('{!! __('Jenis Tempoh') !!}', response.data.message, 'success');
					
						$('#add-period_type-modal').modal('hide');

						$('#utility-period_type').DataTable().ajax.reload();
	                });
					// location.reload();
					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-add-period_type-action-btn" 
			data-route="api.utility.period_type.store"
			data-form="add-period_type-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@component('components.modals.base', [
        'id' => 'edit-period_type-modal',
        'tooltip' => __('Kemaskini Jenis Tempoh'),
        'modal_title' => __('Kemaskini Jenis Tempoh'),
    ])
    @slot('modal_body')
        <form id="edit-period_type-form-store">
        <div class="row">
            <div class="col-6">
				@include('components.forms.hidden', [
                    'name' => 'id',
					'id' => 'id',
					'value'	=> '',
                ])			
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'value',
					'id' => 'value'
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Nama',
                    'name' => 'name',
					'id' => 'name'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.submit-edit-period_type-action-btn', function(event) {
	                event.preventDefault();
					var id = $('#id').val();

	                var data = $('#edit-period_type-form-store').serialize();
	                var route_name = $(this).data('route');

					console.log(data);

	                axios.put(route(route_name, id), data).then(response => {
	                    swal('{!! __('Jenis Tempoh') !!}', response.data.message, 'success');
					
						$('#edit-period_type-modal').modal('hide');

						$('#utility-period_type').DataTable().ajax.reload();
	                });					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-edit-period_type-action-btn" 
			data-route="api.utility.period_type.update"
			data-form="edit-period_type-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@endsection