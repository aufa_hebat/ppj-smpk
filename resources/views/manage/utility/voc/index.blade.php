@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            
            // JAWATANKUASA MESYUARAT
            $(".addmore").on('click',function(){
                var $tr = $("#tableVoc").find('tr[id^="klon"]:last');
                if($tr.prop("id") === undefined){
                    $tr = $("table.d-none");
                }
                var currnum = parseInt( $tr.prop("id").match(/\d+/g), 10 );
                var num = parseInt( $tr.prop("id").match(/\d+/g), 10 )+1;
                var $klon = $tr.clone(true).prop('id', 'klon'+num).removeClass('d-none');
                    
                $klon.find('#appointment_date_'+currnum).attr('id','appointment_date_'+num).attr('name','baru['+num+'][appointment_date]').val('');
                $klon.find('#name_'+currnum).attr('id','name_'+num).attr('name','baru['+num+'][name]').val('');
                $klon.find('#role_'+currnum).attr('id','role_'+num).attr('name','baru['+num+'][role]').val('');
                $('.addmore').parents('table').append($klon);
            });
            
            $('.buang').click(function () {
                swal({
                    title: '{!! __('Amaran') !!}',
                    text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{!! __('Ya') !!}',
                    cancelButtonText: '{!! __('Batal') !!}'
                }).then((result) => {
                    if (result.value) {
                        $(this).parents('tr').detach();
                    }
                });
            });
            // JAWATANKUASA MESYUARAT END

            // UPDATE JAWATANKUASA MEETING
            $(document).on('click', '.submit-voc-action-btn', function(event) {
                event.preventDefault();
                {{--var id = '{{ $vo->id }}';--}}
                {{--var hashslug = '{{ $vo->hashslug }}';--}}
                var route_name = 'api.utility.voc.store';
                var form_id = 'voc_form';
                var data = $('#' + form_id).serialize();
    
                console.log(data);

                axios.post(route(route_name), data).then(response => {
                    swal('Urusetia Jawatankuasa Perubahan Kerja', response.data.message, 'success');
                    redirect(route('manage.utility.voc.index'));
                });
            });           
        });
    </script>
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col">
                <div class="row">
                    <div class="col">
                        <h4>Urusetia Jawatankuasa Perubahan Kerja (VOC)</h4>                        
                    </div>
                </div>
                <form id="voc_form">
                    <div class="row">
                        <div class="col">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="table-responsive">
                                        <div id="tableVoc" class="table-editable table-bordered ">
                                            <table class="table">
                                                <tr>
                                                    <th width="30%"><b>Ahli Jawatankuasa Perubahan Kerja</b></th>
                                                    <th width="10%"><b>Peranan</b></th>
                                                    <th width="5%"><b><span class="fe fe-plus addmore"></span></b></th>
                                                </tr>
                                                @if(!empty($officer))
                                                    @foreach($officer as $jk)
                                                        <tr>
                                                            <td>
                                                                <input type="hidden" name="exist[{{$jk->id}}][id]" value="{{$jk->id}}">
                                                                <span>{{$jk->user->name}}</span>
                                                            </td>
                                                            <td><span>{{$jk->role}}</span></td>
                                                            <td>
                                                                <b><span class="fe fe-minus buang"></span></b></span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                <tr id="klon0" class="d-none">
                                                    <td>
                                                        <select class="form-control input-lg" id="name_0" name="baru[0][name]" style="width: 100%;">
                                                            <option value=""></option>
                                                            @foreach($user as $users)
                                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control input-lg" id="role_0" name="baru[0][role]" style="width: 100%;">
                                                            <option value="ahli">Ahli</option>
                                                            <option value="pengerusi">Pengerusi</option>
                                                            <option value="pengerusi ganti">Pengerusi Ganti</option>                                                                
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <b><span class="fe fe-minus buang"></b></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-group float-right">
                                <button type="submit" class="btn btn-primary submit-voc-action-btn">
                                    @icon('fe fe-save') {{ __('Simpan') }}
                                </button>
                            </div>
    
                        </div>
                    </div>
                </form>
        </div>
    </div>
@endsection