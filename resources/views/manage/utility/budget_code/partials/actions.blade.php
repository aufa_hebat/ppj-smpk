<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-budget_code-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'code' }}="' + data.{{ 'code' }} + '"
    data-{{ 'name' }}="' + data.{{ 'name' }} + '"
    data-{{ 'category' }}="' + data.{{ 'category' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }} 
</a>