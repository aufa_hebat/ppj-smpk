@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var code = $(this).data('code');
                var name = $(this).data('name');
                var category = $(this).data('category');

                $('#id').val(id);
                $('#code').val(code);
                $('#name').val(name);
                $('#category').val(category);
			});

        });
    </script>
@endpush
