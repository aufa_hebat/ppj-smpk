@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">


    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.budget_code.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-budget_code" id="add-budget_code"
        				@include('components.tooltip', ['tooltip' => __('Kod Bajet Baru')])
        				data-toggle="modal" data-target="#add-budget_code-modal">
        				<i class="fe fe-plus"></i>
        				Kod Bajet Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-budget_code',
							'route_name' => 'api.datatable.utility.budget_code',
							'columns' => [
                                ['data' => 'category', 'title' => __('kategori'), 'defaultContent' => '-'],
								['data' => 'code', 'title' => __('kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('kategori'), __('kod'), __('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.budget_code.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
        'id' => 'add-budget_code-modal',
        'tooltip' => __('Tambah Kod Bajet'),
        'modal_title' => __('Tambah Kod Bajet'),
    ])
    @slot('modal_body')
        <form id="add-budget_code-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.select', [
                    'input_label' => __('Kategori Kod Bajet'),
                    'name' => 'category',
                    'options' => [
                        'Business Area'             => 'Business Area', 
                        'Fund'                      => 'Fund',
                        'Functional Area'           => 'Functional Area',
                        'Funded Programme'          => 'Funded Programme',
                        'Cost Centre - Fun Centre'  => 'Cost Centre - Fun Centre',
                        'GL Account'                => 'GL Account'
                    ],
                
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label'   => 'Kod',
                    'name'          => 'code',
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label'   => 'Nama',
                    'name'          => 'name',
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.add-budget_code', function(event) {
					// $('#select2-business_area-container').html("");
					// $('#select2-cost_center-container').html("");
					// $('#select2-fund-container').html("");
					// $('#select2-functional_area-container').html("");
					// $('#select2-funded_programme-container').html("");
					// $('#select2-gl_account-container').html("");
	            });

				$(document).on('click', '.submit-add-budget_code-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#add-budget_code-form-store').serialize();
	                var route_name = $(this).data('route');

	                axios.post(route(route_name), data).then(response => {
	                    swal('{!! __('Kod Bajet') !!}', response.data.message, 'success');
					
						$('#add-budget_code-modal').modal('hide');

						$('#utility-budget_code').DataTable().ajax.reload();
	                });
					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-add-budget_code-action-btn" 
			data-route="api.utility.budget_code.store"
			data-form="add-budget_code-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-budget_code-modal',
        'tooltip' => __('Kemaskini Kod Bajet'),
        'modal_title' => __('Kemaskini Kod Bajet'),
    ])
        @slot('modal_body')
            <form id="edit-budget_code-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])                    
                        @include('components.forms.select', [
                            'input_label' => __('Kategori Kod Bajet'),
                            'id' => 'category',
                            'name' => 'category',
                            'options' => [
                                'Business Area'             => 'Business Area', 
                                'Fund'                      => 'Fund',
                                'Functional Area'           => 'Functional Area',
                                'Funded Programme'          => 'Funded Programme',
                                'Cost Centre - Fun Centre'  => 'Cost Centre - Fun Centre',
                                'GL Account'                => 'GL Account'
                            ],                                
                        ])
                    </div>
                </div>        
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
					        'id' => 'code'
                        ])                
                    </div>
                    <div class="col-6">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>    
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-budget_code-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();

        	                var data = $('#edit-budget_code-form-store').serialize();
	                        var route_name = $(this).data('route');

					        axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Kod Bajet') !!}', response.data.message, 'success');
					
						        $('#edit-budget_code-modal').modal('hide');
                                $('#utility-budget_code').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-budget_code-action-btn" 
			    data-route="api.utility.budget_code.update"
			    data-form="edit-budget_code-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent    
@endsection