@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.termination_type.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-termination_type" id="add-termination_type"
        				@include('components.tooltip', ['tooltip' => __('Jenis Penamatan Baru')])
        				data-toggle="modal" data-target="#add-termination_type-modal">
        				<i class="fe fe-plus"></i>
        				Jenis Penamatan Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-termination_type',
							'route_name' => 'api.datatable.utility.termination_type',
							'columns' => [
                                ['data' => 'code', 'title' => __('kod'), 'defaultContent' => '-'],
                                ['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
                                ['data' => 'description', 'title' => __('keterangan'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('kod'), __('table.name'), __('keterangan'), __('table.action')
							],
							'actions' => minify(view('manage.utility.termination_type.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-termination_type-modal',
            'tooltip' => __('Tambah Jenis Penamatan'),
            'modal_title' => __('Tambah Jenis Penamatan'),
        ])
        @slot('modal_body')
            <form id="add-termination_type-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Kod',
                            'name'          => 'code',
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @include('components.forms.input', [
                            'input_label'   => 'Keterangan',
                            'name'          => 'description',
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-termination_type', function(event) {
                        });

                        $(document).on('click', '.submit-add-termination_type-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-termination_type-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('Jenis Penamatan') !!}', response.data.message, 'success');
                            
                                $('#add-termination_type-modal').modal('hide');

                                $('#utility-termination_type').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-termination_type-action-btn" 
                data-route="api.utility.termination_type.store"
                data-form="add-termination_type-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
            'id' => 'edit-termination_type-modal',
            'tooltip' => __('Kemaskini Jenis Penamatan'),
            'modal_title' => __('Kemaskini Jenis Penamatan'),
        ])
        @slot('modal_body')
            <form id="edit-termination_type-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
                            'id' => 'id',
                            'value'	=> '',
                        ])
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
                            'id' => 'code'
                        ])                
                    </div>
                    <div class="col-6">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @include('components.forms.input', [
                            'input_label'   => 'Keterangan',
                            'name'          => 'description',
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-termination_type-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-termination_type-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('Jenis Penamatan') !!}', response.data.message, 'success');
                            
                                $('#edit-termination_type-modal').modal('hide');

                                $('#utility-termination_type').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-termination_type-action-btn" 
                data-route="api.utility.termination_type.update"
                data-form="edit-termination_type-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection