@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.scheme.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-scheme" id="add-scheme"
        				@include('components.tooltip', ['tooltip' => __('Skim Baru')])
        				data-toggle="modal" data-target="#add-scheme-modal">
        				<i class="fe fe-plus"></i>
        				Skim Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-scheme',
							'route_name' => 'api.datatable.utility.scheme',
							'columns' => [
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.scheme.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-scheme-modal',
        'tooltip' => __('Tambah Skim'),
        'modal_title' => __('Tambah Skim'),
    ])
        @slot('modal_body')
            <form id="add-scheme-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-scheme', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-scheme-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-scheme-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Skim') !!}', response.data.message, 'success');
					
						        $('#add-scheme-modal').modal('hide');

						        $('#utility-scheme').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-scheme-action-btn" 
			    data-route="api.utility.scheme.store"
			    data-form="add-scheme-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-scheme-modal',
        'tooltip' => __('Kemaskini Skim'),
        'modal_title' => __('Kemaskini Skim'),
    ])
        @slot('modal_body')
            <form id="edit-scheme-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                    
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-scheme-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-scheme-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Skim') !!}', response.data.message, 'success');
					
						        $('#edit-scheme-modal').modal('hide');

						        $('#utility-scheme').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-scheme-action-btn" 
			    data-route="api.utility.scheme.update"
			    data-form="edit-scheme-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection