<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-scheme-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'name' }}="' + data.{{ 'name' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
</a>
