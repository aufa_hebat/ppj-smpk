<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-location-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'name' }}="' + data.{{ 'name' }} + '"
    data-{{ 'code' }}="' + data.{{ 'code' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
</a>
