@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var name = $(this).data('name');
                var code = $(this).data('code');

                $('#id').val(id);
                $('#name').val(name);
                $('#code').val(code);

			});
        });
    </script>
@endpush
