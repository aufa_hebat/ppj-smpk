@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.location.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-location" id="add-location"
        				@include('components.tooltip', ['tooltip' => __('Lokasi Baru')])
        				data-toggle="modal" data-target="#add-location-modal">
        				<i class="fe fe-plus"></i>
        				Lokasi Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-location',
							'route_name' => 'api.datatable.utility.location',
							'columns' => [
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],                                
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('Kod'),	__('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.location.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-location-modal',
        'tooltip' => __('Tambah Lokasi'),
        'modal_title' => __('Tambah Lokasi'),
    ])
        @slot('modal_body')
            <form id="add-location-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code'
                        ])
                    </div>                
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-location', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-location-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-location-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Lokasi') !!}', response.data.message, 'success');
					
						        $('#add-location-modal').modal('hide');

						        $('#utility-location').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-location-action-btn" 
			    data-route="api.utility.location.store"
			    data-form="add-location-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-location-modal',
        'tooltip' => __('Kemaskini Lokasi'),
        'modal_title' => __('Kemaskini Lokasi'),
    ])
        @slot('modal_body')
            <form id="edit-location-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
                            'id'    => 'code'
                        ])
                    </div>                
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                    
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-location-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-location-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Lokasi') !!}', response.data.message, 'success');
					
						        $('#edit-location-modal').modal('hide');

						        $('#utility-location').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-location-action-btn" 
			    data-route="api.utility.location.update"
			    data-form="edit-location-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection