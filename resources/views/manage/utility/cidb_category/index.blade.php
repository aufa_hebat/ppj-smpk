@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.cidb_category.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-cidb_category" id="add-cidb_category"
        				@include('components.tooltip', ['tooltip' => __('CIDB:Kategori Baru')])
        				data-toggle="modal" data-target="#add-cidb_category-modal">
        				<i class="fe fe-plus"></i>
        				CIDB:Kategori Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-cidb_category',
							'route_name' => 'api.datatable.utility.cidb_category',
							'columns' => [
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
                                ['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('Kod'),__('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.cidb_category.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-cidb_category-modal',
            'tooltip' => __('Tambah CIDB:Kategori'),
            'modal_title' => __('Tambah CIDB:Kategori'),
        ])
        @slot('modal_body')
            <form id="add-cidb_category-form-store">
                <div class="row">
                    <div class="col-2">
                        @include('components.forms.input', [
                            'input_label'   => 'Kod',
                            'name'          => 'code',
                        ])
                    </div>
                    <div class="col-10">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-cidb_category', function(event) {
                        });

                        $(document).on('click', '.submit-add-cidb_category-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-cidb_category-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('CIDB:Kategori') !!}', response.data.message, 'success');
                            
                                $('#add-cidb_category-modal').modal('hide');

                                $('#utility-cidb_category').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-cidb_category-action-btn" 
                data-route="api.utility.cidb_category.store"
                data-form="add-cidb_category-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
            'id' => 'edit-cidb_category-modal',
            'tooltip' => __('Kemaskini CIDB:Kategori'),
            'modal_title' => __('Kemaskini CIDB:Kategori'),
        ])
        @slot('modal_body')
            <form id="edit-cidb_category-form-store">
                <div class="row">
                    <div class="col-2">          
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
                            'id' => 'code'
                        ])          
                    </div>
                    <div class="col-10">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>                    @include('components.forms.hidden', [
                        'name' => 'id',
                        'id' => 'id',
                        'value' => '',
                    ])                     
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-cidb_category-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-cidb_category-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('CIDB:Kategori') !!}', response.data.message, 'success');
                            
                                $('#edit-cidb_category-modal').modal('hide');

                                $('#utility-cidb_category').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-cidb_category-action-btn" 
                data-route="api.utility.cidb_category.update"
                data-form="edit-cidb_category-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection