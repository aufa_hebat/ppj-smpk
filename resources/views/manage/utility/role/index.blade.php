@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.role.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-role" id="add-role"
        				@include('components.tooltip', ['tooltip' => __('Kumpulan Baru')])
        				data-toggle="modal" data-target="#add-role-modal">
        				<i class="fe fe-plus"></i>
        				Kumpulan Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-role',
							'route_name' => 'api.datatable.utility.role',
							'columns' => [
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.role.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-role-modal',
        'tooltip' => __('Tambah Kumpulan'),
        'modal_title' => __('Tambah Kumpulan'),
    ])
        @slot('modal_body')
            <form id="add-role-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-role', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-role-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-role-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Kumpulan') !!}', response.data.message, 'success');
					
						        $('#add-role-modal').modal('hide');

						        $('#utility-role').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-role-action-btn" 
			    data-route="api.utility.role.store"
			    data-form="add-role-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-role-modal',
        'tooltip' => __('Kemaskini Kumpulan'),
        'modal_title' => __('Kemaskini Kumpulan'),
    ])
        @slot('modal_body')
            <form id="edit-role-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                    
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-role-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-role-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Kumpulan') !!}', response.data.message, 'success');
					
						        $('#edit-role-modal').modal('hide');

						        $('#utility-role').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-role-action-btn" 
			    data-route="api.utility.role.update"
			    data-form="edit-role-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection