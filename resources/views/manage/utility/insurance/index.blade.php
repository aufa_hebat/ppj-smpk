@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col">
            @component('components.card')
                @include('manage.utility.insurance.partials.scripts')
                @slot('card_title')
					<button class="btn btn-primary float-right add-insurance" id="add-insurance"
        				@include('components.tooltip', ['tooltip' => __('Insuran Baru')])
        				data-toggle="modal" data-target="#add-insurance-modal">
        				<i class="fe fe-plus"></i>
        				Insuran Baru
    				</button>
				@endslot            
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-insurance',
							'route_name' => 'api.datatable.utility.insurance',
							'columns' => [
								['data' => 'name', 'title' => __('Nama'), 'defaultContent' => '-'],
                               ['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Nama'), __('table.action')
							],
							'actions' => minify(view('manage.utility.insurance.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot   
            @endcomponent
        </div>
    </div>

    @component('components.modals.base', [
        'id' => 'add-insurance-modal',
        'tooltip' => __('Tambah Insuran'),
        'modal_title' => __('Tambah Insuran'),
    ])
        @slot('modal_body')
            <form id="add-insurance-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name'
                        ])
                    </div>
                </div>              
	        </form>
	        @push('scripts')
		        <script type="text/javascript">
			        jQuery(document).ready(function($) {

				        $(document).on('click', '.add-insurance', function(event) {
					        // $('#select2-business_area-container').html("");
	                    });

				        $(document).on('click', '.submit-add-insurance-action-btn', function(event) {
	                        event.preventDefault();
	                        var data = $('#add-insurance-form-store').serialize();
	                        var route_name = $(this).data('route');

	                        axios.post(route(route_name), data).then(response => {
	                            swal('{!! __('Insuran') !!}', response.data.message, 'success');
					
						        $('#add-insurance-modal').modal('hide');

						        $('#utility-insurance').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-add-insurance-action-btn" 
			    data-route="api.utility.insurance.store"
			    data-form="add-insurance-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
        'id' => 'edit-insurance-modal',
        'tooltip' => __('Kemaskini Insuran'),
        'modal_title' => __('Kemaskini Insuran'),
    ])
        @slot('modal_body')
            <form id="edit-insurance-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
					        'id' => 'id',
					        'value'	=> '',
                        ])	                    
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
					        'id' => 'name'
                        ])
                    </div>
                </div>                 
	        </form>
	    
            @push('scripts')
        		<script type="text/javascript">
		        	jQuery(document).ready(function($) {

				        $(document).on('click', '.submit-edit-insurance-action-btn', function(event) {
	                        event.preventDefault();
					        var id = $('#id').val();
        	                var data = $('#edit-insurance-form-store').serialize();
	                        var route_name = $(this).data('route');

        	                axios.put(route(route_name, id), data).then(response => {
	                            swal('{!! __('Insuran') !!}', response.data.message, 'success');
					
						        $('#edit-insurance-modal').modal('hide');

						        $('#utility-insurance').DataTable().ajax.reload();
	                        });					
	                    });
			        });
		        </script>
	        @endpush
        @endslot
        @slot('modal_footer')
		    <button type="submit" class="btn btn-primary float-right submit-edit-insurance-action-btn" 
			    data-route="api.utility.insurance.update"
			    data-form="edit-insurance-form-store">
		        @icon('fe fe-save') {{ __('Simpan') }}
		    </button>
        @endslot
    @endcomponent


@endsection