@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var name = $(this).data('name');

                $('#id').val(id);
                $('#name').val(name);

			});
        });
    </script>
@endpush
