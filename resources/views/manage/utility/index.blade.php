@extends('layouts.admin')

@push('scripts')
<script type="text/javascript">


</script>

@endpush

@section('content')

	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
                @slot('card_body')
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.authorities.index') }}">Authorities</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.department.index') }}">Jabatan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.vo_type.index') }}">Jenis VO</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.section.index') }}">Seksyen</a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.division.index') }}">Bahagian</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.position.index') }}">Jawatan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.acquisition_category.index') }}">Kategori Perolehan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.scheme.index') }}">Skim</a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.bank.index') }}">Bank</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.bon_type.index') }}">Jenis Bon</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.budget_code.index') }}">Kod Bajet</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.allocation_resource.index') }}">Sumber Peruntukan</a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.honorary.index') }}">Gelaran</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.termination_type.index') }}">Jenis Penamatan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.role.index') }}">Kumpulan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.liability_period.index') }}">Tempoh Liability</a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.grade.index') }}">Gred</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.acquisition_type.index') }}">Jenis Perolehan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.location.index') }}">Lokasi</a>
                        <a style="width: 25%" class="btn btn-default disabled"></a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.insurance.index') }}">Insuran</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.period_type.index') }}">Jenis Tempoh</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.state.index') }}">Negeri</a>
                        <a style="width: 25%" class="btn btn-default disabled"></a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.cidb_grade.index') }}">CIDB : Gred</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.cidb_category.index') }}">CIDB : Kategori</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.cidb_khusus.index') }}">CIDB : Pengkhususan</a>
                        <a style="width: 25%" class="btn btn-default disabled"></a>
                    </div>
                    <div class="btn-group" style="width: 100%" >
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.mof_area.index') }}">MOF : Bidang</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.mof_khusus.index') }}">MOF : Pengkhususan</a>
                        <a style="width: 25%" class="btn btn-default" href="{{ route('utility.voc.index') }}">Urusetia Perubahan Kerja (VOC)</a>
                        <a style="width: 25%" class="btn btn-default disabled"></a>
                    </div>
                    {{-- <table width="100%">
                        <tr>
                            <td><a href="{{ route('utility.authorities.index') }}">Authorities</a></td>   
                            <td><a href="{{ route('utility.department.index') }}">Jabatan</a></td>
                            <td><a href="{{ route('utility.vo_type.index') }}">Jenis VO</a></td>
                            <td><a href="{{ route('utility.section.index') }}">Seksyen</a></td>                            
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.division.index') }}">Bahagian</a></td>
                            <td><a href="{{ route('utility.position.index') }}">Jawatan</a></td>
                            <td><a href="{{ route('utility.acquisition_category.index') }}">Kategori Perolehan</a></td>
                            <td><a href="{{ route('utility.scheme.index') }}">Skim</a></td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.bank.index') }}">Bank</a></td>
                            <td><a href="{{ route('utility.bon_type.index') }}">Jenis Bon</a></td>
                            <td><a href="{{ route('utility.budget_code.index') }}">Kod Bajet</a></td>
                            <td><a href="{{ route('utility.allocation_resource.index') }}">Sumber Peruntukan</a></td>                            
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.honorary.index') }}">Gelaran</a></td>
                            <td><a href="{{ route('utility.termination_type.index') }}">Jenis Penamatan</a></td>
                            <td><a href="{{ route('utility.role.index') }}">Kumpulan</a></td>
                            <td><a href="{{ route('utility.liability_period.index') }}">Tempoh Liability</a></td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.grade.index') }}">Gred</a></td>
                            <td><a href="{{ route('utility.acquisition_type.index') }}">Jenis Perolehan</a></td>
                            <td><a href="{{ route('utility.location.index') }}">Lokasi</a></td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.insurance.index') }}">Insuran</a></td>
                            <td><a href="{{ route('utility.period_type.index') }}">Jenis Tempoh</a></td>
                            <td><a href="{{ route('utility.state.index') }}">Negeri</a></td>                          
                            <td></td>
                        </tr>           
                        <tr>
                            <td>Status</td>
                            <td></td>
                            <td></td>
                            <td></td>                            
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.cidb_grade.index') }}">CIDB : Gred</a></td>
                            <td><a href="{{ route('utility.cidb_category.index') }}">CIDB : Kategori</a></td>
                            <td><a href="{{ route('utility.cidb_khusus.index') }}">CIDB : Pengkhususan</a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('utility.mof_area.index') }}">MOF : Bidang</a></td>
                            <td><a href="{{ route('utility.mof_khusus.index') }}">MOF : Pengkhususan</a></td>
                            <td></td>
                            <td></td>
                        </tr>                                                                                     
                    </table> --}}
                @endslot                
            @endcomponent
		</div>
	</div>
@endsection