@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(document).on('click', '.edit-action-btn', function(event) {
				event.preventDefault();
				var id = $(this).data('id');
                var code = $(this).data('code');
                var code_short = $(this).data('code_short');
                var description = $(this).data('description');
                var status = $(this).data('status');

                $('#id').val(id);
                $('#code').val(code);
                $('#code_short').val(code_short);
                $('#description').val(description);
                $('#status').val(status);
			});
        });
    </script>
@endpush
