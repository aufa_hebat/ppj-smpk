<a class="btn btn-default edit-action-btn"
    data-toggle="modal" data-target="#edit-state-modal"
	data-{{ 'id' }}="' + data.{{ 'id' }} + '" 
    data-{{ 'code' }}="' + data.{{ 'code' }} + '"
    data-{{ 'code_short' }}="' + data.{{ 'code_short' }} + '"
    data-{{ 'description' }}="' + data.{{ 'description' }} + '"
    data-{{ 'status' }}="' + data.{{ 'status' }} + '"
    >
	<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
</a>
