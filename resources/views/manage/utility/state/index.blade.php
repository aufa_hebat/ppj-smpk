@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>

@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.state.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-state" id="add-state"
        				@include('components.tooltip', ['tooltip' => __('Negeri Baru')])
        				data-toggle="modal" data-target="#add-state-modal">
        				<i class="fe fe-plus"></i>
        				Negeri Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-state',
							'route_name' => 'api.datatable.utility.state',
							'columns' => [
								['data' => 'code', 'title' => __('Kategori'), 'defaultContent' => '-'],
                                ['data' => 'code_short', 'title' => __('Kod'), 'defaultContent' => '-'],
								['data' => 'description', 'title' => __('Keterangan'), 'defaultContent' => '-'],
                                ['data' => 'status', 'title' => __('Status'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('Kategori'), __('Kod'), __('Keterangan'), __('Status'), __('table.action')
							],
							'actions' => minify(view('manage.utility.state.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
        'id' => 'add-state-modal',
        'tooltip' => __('Tambah Negeri'),
        'modal_title' => __('Tambah Negeri'),
    ])
    @slot('modal_body')
        <form id="add-state-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Kategori',
                    'name' => 'code',
                    'value' => 'KOD_NEGERI',
                    'readonly'  =>  true,
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'code_short'
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Keterangan',
                    'name' => 'description'
                ])
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.add-state', function(event) {
					// $('#select2-business_area-container').html("");
	            });

				$(document).on('click', '.submit-add-state-action-btn', function(event) {
	                event.preventDefault();
	                var data = $('#add-state-form-store').serialize();
	                var route_name = $(this).data('route');

	                axios.post(route(route_name), data).then(response => {
	                    swal('{!! __('Negeri') !!}', response.data.message, 'success');
					
						$('#add-state-modal').modal('hide');

						$('#utility-state').DataTable().ajax.reload();
	                });					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-add-state-action-btn" 
			data-route="api.utility.state.store"
			data-form="add-state-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@component('components.modals.base', [
        'id' => 'edit-state-modal',
        'tooltip' => __('Kemaskini Negeri'),
        'modal_title' => __('Kemaskini Negeri'),
    ])
    @slot('modal_body')
        <form id="edit-state-form-store">
        <div class="row">
            <div class="col-6">
                @include('components.forms.hidden', [
                    'name' => 'id',
					'id' => 'id',
					'value'	=> '',
                ])	            
                @include('components.forms.input', [
                    'input_label' => 'Kategori',
                    'name' => 'code',
                    'id' => 'code',
                    'readonly'  =>  true,
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-6">		
                @include('components.forms.input', [
                    'input_label' => 'Kod',
                    'name' => 'code_short',
					'id' => 'code_short'
                ])
            </div>
            <div class="col-6">
                @include('components.forms.input', [
                    'input_label' => 'Keterangan',
                    'name' => 'description',
					'id' => 'description'
                ])
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label class="control-label" style="font-size: 15px; margin-top: 10px;" >Status</label><br>
                <div class="selectgroup w-25 grade-container">
                    <label class="selectgroup-item">
                        <input type="radio" id="status" name="status" value="0" class="selectgroup-input status">
                        <span class="selectgroup-button">{{ __('AKTIF') }}</span>
                    </label>
                    <label class="selectgroup-item">
                        <input type="radio" id="status" name="status" value="1" class="selectgroup-input status" >
                        <span class="selectgroup-button">{{ __('PASIF') }}</span>
                    </label>
                </div>
            </div>
        </div>
	</form>
	@push('scripts')
		<script type="text/javascript">
			jQuery(document).ready(function($) {

				$(document).on('click', '.submit-edit-state-action-btn', function(event) {
	                event.preventDefault();
					var id = $('#id').val();

	                var data = $('#edit-state-form-store').serialize();
	                var route_name = $(this).data('route');

                    console.log(data);

	                axios.put(route(route_name, id), data).then(response => {
	                    swal('{!! __('Negeri') !!}', response.data.message, 'success');
					
						$('#edit-state-modal').modal('hide');

						$('#utility-state').DataTable().ajax.reload();
	                });					
	            });

			});
		</script>
	@endpush
    @endslot
    @slot('modal_footer')
		<button type="submit" class="btn btn-primary float-right submit-edit-state-action-btn" 
			data-route="api.utility.state.update"
			data-form="edit-state-form-store">
		    @icon('fe fe-save') {{ __('Simpan') }}
		</button>
    @endslot
@endcomponent


@endsection