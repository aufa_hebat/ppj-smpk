@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.mof_khusus.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-mof_khusus" id="add-mof_khusus"
        				@include('components.tooltip', ['tooltip' => __('MOF:Pengkhususan Baru')])
        				data-toggle="modal" data-target="#add-mof_khusus-modal">
        				<i class="fe fe-plus"></i>
        				MOF:Pengkhususan Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-mof_khusus',
							'route_name' => 'api.datatable.utility.mof_khusus',
							'columns' => [
                                ['data' => 'category', 'title' => __('Bidang'), 'defaultContent' => '-'],
                                ['data' => 'code', 'title' => __('Kod'), 'defaultContent' => '-'],
                                ['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
                                __('Bidang'),__('Kod'),__('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.mof_khusus.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-mof_khusus-modal',
            'tooltip' => __('Tambah MOF:Pengkhususan'),
            'modal_title' => __('Tambah MOF:Pengkhususan'),
        ])
        @slot('modal_body')
            <form id="add-mof_khusus-form-store">
                <div class="row">
                    <div class="col-2">
                        @include('components.forms.select', [
                            'input_label' => __('Bidang'),
                            'options' => mof_codes('category')->pluck('code','code'),
                            'name' => 'category',
                        ])
                    </div>
                    <div class="col-2">
                        @include('components.forms.input', [
                            'input_label'   => 'Kod',
                            'name'          => 'code',
                        ])
                    </div>
                    <div class="col-8">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-mof_khusus', function(event) {
                        });

                        $(document).on('click', '.submit-add-mof_khusus-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-mof_khusus-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('MOF:Pengkhususan') !!}', response.data.message, 'success');
                            
                                $('#add-mof_khusus-modal').modal('hide');

                                $('#utility-mof_khusus').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-mof_khusus-action-btn" 
                data-route="api.utility.mof_khusus.store"
                data-form="add-mof_khusus-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
            'id' => 'edit-mof_khusus-modal',
            'tooltip' => __('Kemaskini MOF:Pengkhususan'),
            'modal_title' => __('Kemaskini MOF:Pengkhususan'),
        ])
        @slot('modal_body')
            <form id="edit-mof_khusus-form-store">
                <div class="row">
                    <div class="col-2">
                        @include('components.forms.select', [
                            'input_label' => __('Bidang'),
                            'options' => mof_codes('category')->pluck('code','code'),
                            'name' => 'category',
                            'id' => 'category',
                        ])
                    </div>
                    <div class="col-2">            
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
                            'id' => 'code'
                        ])            
                    </div>
                    <div class="col-8">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                    @include('components.forms.hidden', [
                        'name' => 'id',
                        'id' => 'id',
                        'value' => '',
                    ])                     
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-mof_khusus-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-mof_khusus-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('MOF:Pengkhususan') !!}', response.data.message, 'success');
                            
                                $('#edit-mof_khusus-modal').modal('hide');

                                $('#utility-mof_khusus').DataTable().ajax.reload();
                            });					
                        });
                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-mof_khusus-action-btn" 
                data-route="api.utility.mof_khusus.update"
                data-form="edit-mof_khusus-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection