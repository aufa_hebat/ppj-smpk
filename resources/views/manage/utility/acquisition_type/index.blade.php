@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.acquisition_type.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-acquisition_type" id="add-acquisition_type"
        				@include('components.tooltip', ['tooltip' => __('Jenis Perolehan Baru')])
        				data-toggle="modal" data-target="#add-acquisition_type-modal">
        				<i class="fe fe-plus"></i>
        				Jenis Perolehan Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-acquisition_type',
							'route_name' => 'api.datatable.utility.acquisition_type',
							'columns' => [
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.acquisition_type.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-acquisition_type-modal',
            'tooltip' => __('Tambah Jenis Perolehan'),
            'modal_title' => __('Tambah Jenis Perolehan'),
        ])
        @slot('modal_body')
            <form id="add-acquisition_type-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                    <div class="col-6"></div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-acquisition_type', function(event) {
                        });

                        $(document).on('click', '.submit-add-acquisition_type-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-acquisition_type-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('Jenis Perolehan') !!}', response.data.message, 'success');
                            
                                $('#add-acquisition_type-modal').modal('hide');

                                $('#utility-acquisition_type').DataTable().ajax.reload();
                            });
                            
                        });

                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-acquisition_type-action-btn" 
                data-route="api.utility.acquisition_type.store"
                data-form="add-acquisition_type-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

    @component('components.modals.base', [
            'id' => 'edit-acquisition_type-modal',
            'tooltip' => __('Kemaskini Jenis Perolehan'),
            'modal_title' => __('Kemaskini Jenis Perolehan'),
        ])
        @slot('modal_body')
            <form id="edit-acquisition_type-form-store">
                <div class="row">
                    <div class="col-6">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
                            'id' => 'id',
                            'value'	=> '',
                        ])             
                    </div>            
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-acquisition_type-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-acquisition_type-form-store').serialize();
                            var route_name = $(this).data('route');

                            console.log(data);

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('Jenis Perolehan') !!}', response.data.message, 'success');
                            
                                $('#edit-acquisition_type-modal').modal('hide');

                                $('#utility-acquisition_type').DataTable().ajax.reload();
                            });					
                        });

                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-acquisition_type-action-btn" 
                data-route="api.utility.acquisition_type.update"
                data-form="edit-acquisition_type-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection