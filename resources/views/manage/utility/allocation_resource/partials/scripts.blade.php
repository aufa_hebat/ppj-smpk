@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $(document).on('click', '.edit-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var code = $(this).data('code');
            var name = $(this).data('name');

            $('#id').val(id);
            $('#code').val(code);
            $('#name').val(name);
        });

        $(document).on('click', '.destroy-action-btn', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            swal({
                title: '{!! __('Amaran') !!}',
                text: '{!! __('Adakah anda pasti mahu memadamkan rekod ini?') !!}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '{!! __('Ya') !!}',
                cancelButtonText: '{!! __('Batal') !!}'
            }).then((result) => {
                if (result.value) {
                axios.delete(route('api.utility.allocation_resource.destroy', id))
                    .then(response => {
                        $('#utility-allocation_resource').DataTable().ajax.reload();
                    })
              }

            });
        });
    });
</script>
@endpush
