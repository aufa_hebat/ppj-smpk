@extends('layouts.admin')

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush

@section('content')
	<div class="row justify-content-center">
		<div class="col">
            @component('components.card')
				@include('manage.utility.allocation_resource.partials.scripts')
				@slot('card_title')
					<button class="btn btn-primary float-right add-allocation_resource" id="add-allocation_resource"
        				@include('components.tooltip', ['tooltip' => __('Sumber Peruntukan Baru')])
        				data-toggle="modal" data-target="#add-allocation_resource-modal">
        				<i class="fe fe-plus"></i>
        				Sumber Peruntukan Baru
    				</button>
				@endslot			
                @slot('card_body')
                    @component('components.datatable', 
						[
							'table_id' => 'utility-allocation_resource',
							'route_name' => 'api.datatable.utility.allocation_resource',
							'columns' => [
                                ['data' => 'code', 'title' => __('kod'), 'defaultContent' => '-'],
								['data' => 'name', 'title' => __('table.name'), 'defaultContent' => '-'],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],
							],
							'headers' => [
								__('kod'), __('table.name'), __('table.action')
							],
							'actions' => minify(view('manage.utility.allocation_resource.partials.actions')->render())
						]
					)
					@endcomponent
                @endslot                
            @endcomponent
		</div>
	</div>

    @component('components.modals.base', [
            'id' => 'add-allocation_resource-modal',
            'tooltip' => __('Tambah Sumber Peruntukan'),
            'modal_title' => __('Tambah Sumber Peruntukan'),
            ])
        @slot('modal_body')
            <form id="add-allocation_resource-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Kod',
                            'name'          => 'code',
                        ])
                    </div>
                    <div class="col-6">
                        @include('components.forms.input', [
                            'input_label'   => 'Nama',
                            'name'          => 'name',
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.add-allocation_resource', function(event) {
                        });

                        $(document).on('click', '.submit-add-allocation_resource-action-btn', function(event) {
                            event.preventDefault();
                            var data = $('#add-allocation_resource-form-store').serialize();
                            var route_name = $(this).data('route');

                            axios.post(route(route_name), data).then(response => {
                                swal('{!! __('Sumber Peruntukan') !!}', response.data.message, 'success');
                            
                                $('#add-allocation_resource-modal').modal('hide');

                                $('#utility-allocation_resource').DataTable().ajax.reload();
                            });
                            
                        });

                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-add-allocation_resource-action-btn" 
                data-route="api.utility.allocation_resource.store"
                data-form="add-allocation_resource-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent


    @component('components.modals.base', [
            'id' => 'edit-allocation_resource-modal',
            'tooltip' => __('Kemaskini Sumber Peruntukan'),
            'modal_title' => __('Kemaskini Sumber Peruntukan'),
        ])
        @slot('modal_body')
            <form id="edit-allocation_resource-form-store">
                <div class="row">
                    <div class="col-6">
                        @include('components.forms.hidden', [
                            'name' => 'id',
                            'id' => 'id',
                            'value'	=> '',
                        ])
                        @include('components.forms.input', [
                            'input_label' => 'Kod',
                            'name' => 'code',
                            'id' => 'code'
                        ])                
                    </div>
                    <div class="col-6">	            
                        @include('components.forms.input', [
                            'input_label' => 'Nama',
                            'name' => 'name',
                            'id' => 'name'
                        ])
                    </div>
                </div>
            </form>
            @push('scripts')
                <script type="text/javascript">
                    jQuery(document).ready(function($) {

                        $(document).on('click', '.submit-edit-allocation_resource-action-btn', function(event) {
                            event.preventDefault();
                            var id = $('#id').val();

                            var data = $('#edit-allocation_resource-form-store').serialize();
                            var route_name = $(this).data('route');

                            console.log(data);

                            axios.put(route(route_name, id), data).then(response => {
                                swal('{!! __('Sumber Peruntukan') !!}', response.data.message, 'success');
                            
                                $('#edit-allocation_resource-modal').modal('hide');

                                $('#utility-allocation_resource').DataTable().ajax.reload();
                            });					
                        });

                    });
                </script>
            @endpush
        @endslot
        @slot('modal_footer')
            <button type="submit" class="btn btn-primary float-right submit-edit-allocation_resource-action-btn" 
                data-route="api.utility.allocation_resource.update"
                data-form="edit-allocation_resource-form-store">
                @icon('fe fe-save') {{ __('Simpan') }}
            </button>
        @endslot
    @endcomponent

@endsection