
@if(!(user()->current_role_login == 'ketuajabatan' || user()->current_role_login == 'ketuabahagian' || user()->current_role_login == 'ketuaunit'|| user()->current_role_login == 'administrator'))
<h4>Senarai Tugasan</h4>
<div class="row justify-content-center">
      
    
	<div class="col">
		@component('components.card')
			@slot('card_body')
				@component('components.datatable', 
					[
						'table_id' => 'user-tasks',
						'route_name' => 'api.datatable.workflowtasks',
						'columns' => [ 
							['data' => 'flow_name', 'title' => __('Tugas'), 'defaultContent' => '-', 'searchable' => true, 'orderable' => true],
                            ['data' => 'flow_desc', 'title' => __('Arahan'), 'defaultContent' => '-', 'searchable' => true, 'orderable' => true],
							['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false],                                                        
						],
						'headers' => [
							__('Tugas'),__('Arahan'), __('table.action')
						],
                        'actions' => minify(view('users.usertasks')->render())
					]
				)
				@endcomponent
			@endslot
		@endcomponent
	</div>
</div>
@endif

@if(user()->current_role_login == 'urusetia' || user()->current_role_login == 'kaunter' || user()->current_role_login == 'urusetiavo')
{{-- @role('urusetia||kaunter||urusetiavo') --}}
	<h4>Senarai Tugasan Untuk Dipilih</h4>
	<div class="row justify-content-center">
	     <div class="col">
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'team-tasks',
							'route_name' => 'api.datatable.workflowtasksTeam',
							'columns' => [
								['data' => 'flow_name', 'title' => __('Tugas'), 'defaultContent' => '-', 'searchable' => false, 'orderable' => false],
	                            ['data' => 'flow_desc', 'title' => __('Arahan'), 'defaultContent' => '-', 'searchable' => true, 'orderable' => true],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false], 

							],
							'headers' => [
								__('Tugas'),__('Arahan'), __('Tindakan')
							],
	                        'actions' => minify(view('users.userteamtasks')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	    
	</div>
{{-- @endrole --}}
@else
	@if(user()->current_role_login != 'pengesah' && user()->department_id == 9 && user()->executor_department_id == 25)
		<h4>Senarai Tugasan Untuk Dipilih</h4>
		<div class="row justify-content-center">
		     <div class="col">
				@component('components.card')
					@slot('card_body')
						@component('components.datatable', 
							[
								'table_id' => 'team-tasks',
								'route_name' => 'api.datatable.workflowtasksTeam',
								'columns' => [
									['data' => 'flow_name', 'title' => __('Tugas'), 'defaultContent' => '-', 'searchable' => false, 'orderable' => false],
		                            ['data' => 'flow_desc', 'title' => __('Arahan'), 'defaultContent' => '-', 'searchable' => true, 'orderable' => true],
									['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false], 

								],
								'headers' => [
									__('Tugas'),__('Arahan'), __('Tindakan')
								],
		                        'actions' => minify(view('users.userteamtasks')->render())
							]
						)
						@endcomponent
					@endslot
				@endcomponent
			</div>
		    
		</div>
	@endif
@endif

@if(user()->current_role_login == 'penyemak' && (user()->position_id == '67' || user()->position_id == '2052' || user()->position_id == '2032') && user()->scheme_id == '21' && (user()->grade_id == '11' || user()->grade_id == '12' || user()->grade_id == '13' || user()->grade_id == '14' || user()->grade_id == '15' || user()->grade_id == '16'))
	<h4>Senarai Tugasan Untuk Dipilih</h4>
	<div class="row justify-content-center">
	     <div class="col">
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'team-tasks',
							'route_name' => 'api.datatable.workflowtasksTeam',
							'columns' => [
								['data' => 'flow_name', 'title' => __('Tugas'), 'defaultContent' => '-', 'searchable' => false, 'orderable' => false],
	                            ['data' => 'flow_desc', 'title' => __('Arahan'), 'defaultContent' => '-', 'searchable' => true, 'orderable' => true],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false], 

							],
							'headers' => [
								__('Tugas'),__('Arahan'), __('Tindakan')
							],
	                                                'actions' => minify(view('users.userteamtasks')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	    
	</div>
@endif

@if(user()->current_role_login == 'pengesah' && user()->executor_department_id == 25)
	<h4>Senarai Tugasan Untuk Dipilih</h4>
	<div class="row justify-content-center">
	     <div class="col">
			@component('components.card')
				@slot('card_body')
					@component('components.datatable', 
						[
							'table_id' => 'team-tasks',
							'route_name' => 'api.datatable.workflowtasksTeam',
							'columns' => [
								['data' => 'flow_name', 'title' => __('Tugas'), 'defaultContent' => '-', 'searchable' => false, 'orderable' => false],
	                            ['data' => 'flow_desc', 'title' => __('Arahan'), 'defaultContent' => '-', 'searchable' => true, 'orderable' => true],
								['data' => null , 'name' => null, 'searchable' => false, 'orderable' => false], 

							],
							'headers' => [
								__('Tugas'),__('Arahan'), __('Tindakan')
							],
	                                                'actions' => minify(view('users.userteamtasks')->render())
						]
					)
					@endcomponent
				@endslot
			@endcomponent
		</div>
	    
	</div>
@endif

