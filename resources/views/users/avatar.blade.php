@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center mt-5">
        <div class="col-4">
            @component('components.card', ['card_title' => __('Muatnaik Avatar')])
                @slot('card_body')
                    {{ html()->form('POST', route('user.avatar.store'))->attribute('enctype', 'multipart/form-data')->open() }}
                        <div class="form-group">
    						{{ html()->label(__('Sila pilih avatar anda')) }}
    						{{ html()->input('file')->name('avatar')->class('form-control') }}
                        </div>
                        @include('components.forms.submit', ['route' => 'user.show'])
					{{ html()->form()->close() }}
                @endslot
            @endcomponent
        </div>
    </div>
@endsection
