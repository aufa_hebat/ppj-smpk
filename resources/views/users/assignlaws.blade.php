<h4>Senarai Tugasan Pegawai Undang-Undang</h4>
<div class="row justify-content-center">
	<div class="col">
		@component('components.card')
			@slot('card_body')
				<table class="table" id="dataTable" width="100%" cellspacing="0">
                    <thead>
	                    <tr>
	                        <th style="width: 5%">Bil</th>
	                        <th style="width: 15%">No Rujukan</th>
	                        <th style="width: 40%">Tajuk</th>
	                        <th style="width: 25%">Modul</th>
	                        <th style="width: 15%">Tindakan</th>
	                    </tr>
                    </thead>
                    <tbody>
                    	@if(!empty($s6))
	                    	@foreach($s6 as $bil => $law)
		                        <tr>
		                        	<td>{{$bil + 1}}</td>
		                            <td>{{$law->acquisition->reference}}</td>
		                            <td>{{$law->acquisition->title}}</td>
		                            <td>{{$law->type}} @if(!empty($law->document_contract_type)): {{$law->document_contract_type}} @endif</td>
		                            <td>
										<form method="POST" action="{{ route('acquisition.lawreview.update',$law->acquisition->id) }}">
										    @csrf
										    @method('PUT')
		                            		<button type="submit" class="btn btn-primary">Pilih</button>
		                            	</form>
		                            </td>
		                        </tr>
	                        @endforeach
                        @endif
                    </tbody>
                </table>
			@endslot
		@endcomponent
	</div>
</div>