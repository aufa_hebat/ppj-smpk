
@push('scripts')
<script type="text/javascript">
        jQuery(document).ready(function($) {       
           $(document).on('click', '.team-edit-action-btn', function(event) {                   
                        event.preventDefault();
			var id_ = $(this).data('id');
                        var flow_name = $(this).data('flow_name');
			swal({
			  title: 'Pertanyaan',
			  text: 'Adakah anda pasti mahu mengambil tugas '+flow_name+'?',
			  type: 'question',
			  showCancelButton: true,
			  confirmButtonText: '{!! __('Ya') !!}',
			  cancelButtonText: '{!! __('Tidak') !!}'
			}).then((result) => {
			  if (result.value) {
                              
                                //redirect to controller send by id
                                
                              
			    axios.get(route('task.claim'), { params: { id: id_ } }).then(response => {
                                                      
                             //alert('Response:'+response.data);   
                            if(response.data == 'success'){
	                        //swal('Pemilihan Senarai Tugasan', 'Berjaya Dipilih!', 'success');
                                //redirect(''+url);
                                redirect(route('home'));
                                //
                                //go to
                            }else{
                                swal('Dukacita', 'Tidak Berjaya Dipilih!', 'error');
                            }
                                        						
	                     });
			  }
			});
		});       
        });
    </script>
@endpush

<div class="text-center">
	<div class="item-action dropdown">
	  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
	  	<i class="fe fe-more-vertical"></i>
	  </a>
	  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
	  	style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
	  	{{ $prepend_action or '' }}
	   
	    {{ $prepend_footer_action or '' }}
	    <a class="dropdown-item team-edit-action-btn" style="cursor: pointer;"
			data-{{'id'}}="' + data.{{'id'}} + '"
                        data-{{'flow_name'}}="' + data.{{'flow_name'}} + '"
			style="display: '+data.{{'display'}}+'"
			>
			<i class="fe fe-edit text-primary"></i> {{ __('Ambil') }}
		</a>
		{{ $append_footer_action or '' }}
	  </div>
	</div>
</div>