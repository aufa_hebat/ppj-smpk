@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center mt-5">
        <div class="col-4">
            @component('components.card', ['card_title' => __('Kata Laluan')])
                @slot('card_body')
					{{ html()->form('PUT', route('user.password.update'))->open() }}
						
						<div class="form-group">
							{{ html()->label(__('Kata Laluan'))->attribute('for', 'password') }}
							{{ html()->password()->name('password')->class('form-control') }}
						</div>
					
						<div class="form-group">
							{{ html()->label(__('Pengesahan Kata Laluan'))
								->attribute('for', 'password_confirmation') }}
							{{ html()->password()->name('password_confirmation')->class('form-control') }}
						</div>
							
						@include('components.forms.submit', ['route' => 'user.show'])
                        
					{{ html()->form()->close() }}
				@endslot
            @endcomponent
        </div>
    </div>
@endsection
