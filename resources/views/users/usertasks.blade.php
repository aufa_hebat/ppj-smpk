
@push('scripts')
<script type="text/javascript">
        jQuery(document).ready(function($) {       
           $(document).on('click', '.edit-action-btn', function(event) {                   
                        event.preventDefault();
			var url = $(this).data('url');
            //alert('ID'+url);
            // var flow_name = $(this).data('flow_name');
                        
			// swal({
			//   title: 'Pertanyaan',
			//   text: 'Adakah anda pasti mahu mengambil tindakan untuk '+flow_name+'?',
			//   type: 'question',
			//   showCancelButton: true,
			//   confirmButtonText: '{!! __('Ya') !!}',
			//   cancelButtonText: '{!! __('Tidak') !!}'
			// }).then((result) => {                                                  
			  // if (result.value) {                           
			  	redirect(''+url);
			  // }
			// });
		});       
        });
    </script>
@endpush

<div class="text-center">
	<div class="item-action dropdown">
	  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="true">
	  	<i class="fe fe-more-vertical"></i>
	  </a>
	  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" 
	  	style="position: absolute; transform: translate3d(-181px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">
	  	{{ $prepend_action or '' }}
	   
	    {{ $prepend_footer_action or '' }}
	    <a class="dropdown-item edit-action-btn" style="cursor: pointer;"
			data-{{ 'url' }}="' + data.{{ 'url' }} + '"
                        data-{{ 'flow_name' }}="' + data.{{ 'flow_name' }} + '"
			style="display: '+data.{{'display'}}+'"
			>
			<i class="fe fe-edit text-primary"></i> {{ __('Kemaskini') }}
		</a>
		{{ $append_footer_action or '' }}
	  </div>
	</div>
</div>