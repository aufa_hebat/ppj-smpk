@extends('layouts.admin')

@section('content')
	<div class="my-3 my-md-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-4">
						<h4>Profil</h4>
						<div class="card card-profile border-0">
							<div class="card-body">
								<div class="text-center">
									@if(user()->getLastMediaUrl('avatar', 'thumbnail_navbar'))
										<img src="{{ auth()->user()->getLastMediaUrl('avatar', 'thumbnail_navbar') }}"
											alt="avatar"
											class="card-profile-img">
									@else
										<span class="avatar">
											<i class="fe fe-user text-light"></i>
										</span>
									@endif
									<hr>
									<h3 class="mb-3">{{ user()->name }}</h3>
								</div>
								
								<p>
									<span class="text-dark font-weight-bold">{{ __('E-mel') }}</span><br>{{ user()->email }}
								</p>
								<p>
									<span class="text-dark font-weight-bold">{{ __('Jabatan') }}</span><br> 
									{{ user()->section->name }}, {{ user()->department->name }}.
								</p>
								<p>
									<span class="text-dark font-weight-bold">{{ __('Skim') }}</span><br>
									{{ user()->scheme->name }}
								</p>
								<p>
									<span class="text-dark font-weight-bold">{{ __('Gred') }}</span><br>
									{{ user()->grade->name }}
								</p>
							</div>
							
							<div class="card-footer">
								<a href="{{ route('manage.users.edit', user()->hashslug) }}" 
									class="btn float-right btn-primary">
									@icon('fe fe-edit text-primary') {{ __('Kemaskini') }}
								</a>
							</div>
						</div>
					</div>
					<div class="col">
						@include('users.tasks')
					</div>
				</div>
			</div>
		</div>
@endsection